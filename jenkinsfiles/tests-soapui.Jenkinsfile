pipeline {
   agent any
   tools {
      maven 'maven-3.9'
      jdk 'temurin-jdk17'
   }
   parameters {
      string(name: 'BRANCH', defaultValue: "main", description: 'Set branch')
      string(name: 'CREDENTIALS_ID', defaultValue: "xplanbox.lat-lon.de", description: 'Set id of Jenkins credentials to login to environment (BASIC AUTH)')
      string(name: 'SERVICES_API_KEY', defaultValue: "xplanbox", description: 'Set ApiKey to access /config of XPlanDienste')
      string(name: 'BASE_URL_VALIDATOR_API', defaultValue: "https://xplanbox.lat-lon.de", description: 'Set base URL of XPlanValidatorAPI')
      string(name: 'BASE_URL_MANAGER_API', defaultValue: "https://xplanbox.lat-lon.de", description: 'Set base URL of XPlanManagerAPI')
      string(name: 'BASE_URL_DOKUMENTE_API', defaultValue: "https://xplanbox.lat-lon.de", description: 'Set base URL of XPlanDokumenteAPI')
      string(name: 'BASE_URL_DIENSTE', defaultValue: "https://xplanbox.lat-lon.de", description: 'Set base URL of XPlanDienste')
      string(name: 'BASE_URL_INSPIRE_PLU', defaultValue: "https://xplanbox.lat-lon.de", description: 'Set base URL of INSPIRE PLU')
      string(name: 'BASE_URL_MAPSERVER', defaultValue: "https://xplanbox.lat-lon.de", description: 'Set base URL of MapServer')
      string(name: 'BASE_URL_MAPPROXY', defaultValue: "https://xplanbox.lat-lon.de", description: 'Set base URL of MapProxy')
      string(name: 'JDBC_URL', description: 'JDBC URL to access XPlanDB')
      string(name: 'JWT_URL', description: 'URL to retrieve a JSON Web Token')
      string(name: 'JWT_CLIENTID', defaultValue: "xplanbox-api", description: 'ID of the Client configured in Keycloak')
      string(name: 'SLACK_CHANNEL', defaultValue: "#installationen", description: 'Set slack channel')
      string(name: 'SLACK_TESTED_ENVIRONMENT', defaultValue: "https://xplanbox.lat-lon.de/", description: 'Set name of test environment which shall be printed in slack messages')
      string(name: 'SLACK_TEAM_DOMAIN', defaultValue: "xplanbox", description: 'Set slack team domain')
      string(name: 'SLACK_TOKEN_CREDENTIAL_ID', defaultValue: "slack-integration-id", description: 'Set slack token credential id')
   }
   stages {
      stage('Preparation') {
         steps {
            sh 'mvn --version'
         }
      }
      stage('SoapUI-Test XPlanManagerAPI') {
         steps {
            withCredentials([
               usernamePassword(credentialsId:"${CREDENTIALS_ID}", passwordVariable: 'Password', usernameVariable: 'Username'),
               usernamePassword(credentialsId:"${CREDENTIALS_ID}-dortmund", passwordVariable: 'PasswordDortmund', usernameVariable: 'UsernameDortmund'),
               usernamePassword(credentialsId:"${CREDENTIALS_ID}-gelsenkirchen", passwordVariable: 'PasswordNotDortmund', usernameVariable: 'UsernameNotDortmund')
            ]) {
               sh 'mvn test -pl :xplan-tests-soapui -Psystem-tests -DtestFileName=xplan-manager-api-soapui-project.xml \
                      -DbaseUrlManagerApi=${BASE_URL_MANAGER_API} -Dusername=$Username -Dpassword=$Password \
                      -DjdbcUrl=$JDBC_URL \
                      -DjwtUrl=$JWT_URL -DjwtClientId=$JWT_CLIENTID \
                      -DbaseUrlServices=${BASE_URL_DIENSTE} \
                      -DusernameDortmund=${UsernameDortmund} -DpasswordDortmund=${PasswordDortmund} -DusernameNotDortmund=${UsernameNotDortmund} -DpasswordNotDortmund=${PasswordNotDortmund}'
            }
         }
      }
      stage('SoapUI-Test XPlanValidatorAPI') {
         steps {
            withCredentials([
               usernamePassword(credentialsId:"${CREDENTIALS_ID}", passwordVariable: 'Password', usernameVariable: 'Username')
            ]) {
               sh 'mvn test -pl :xplan-tests-soapui -Psystem-tests -DtestFileName=xplan-validator-api-soapui-project.xml \
                       -DbaseUrlValidatorApi=${BASE_URL_VALIDATOR_API} -Dusername=$Username -Dpassword=$Password \
                       -DjwtUrl=$JWT_URL -DjwtClientId=$JWT_CLIENTID'
            }
         }
      }
      stage('SoapUI-Test XPlanDokumenteAPI') {
         steps {
            withCredentials([
               usernamePassword(credentialsId:"${CREDENTIALS_ID}", passwordVariable: 'Password', usernameVariable: 'Username')
            ]) {
               sh 'mvn test -pl :xplan-tests-soapui -Psystem-tests -DtestFileName=xplan-dokumente-api-soapui-project.xml \
                   -DbaseUrlManagerApi=${BASE_URL_MANAGER_API} -DusernameManagerApi=$Username -DpasswordManagerApi=$Password \
                   -DbaseUrlDokumenteApi=${BASE_URL_DOKUMENTE_API} -DusernameDokumenteApi=$Username -DpasswordDokumenteApi=$Password \
                   -DjwtUrl=$JWT_URL -DjwtClientId=$JWT_CLIENTID'
            }
         }
      }
      stage('SoapUI-Test XPlanDienste') {
         steps {
            withCredentials([
               usernamePassword(credentialsId:"${CREDENTIALS_ID}", passwordVariable: 'Password', usernameVariable: 'Username')
            ]) {
               sh 'mvn test -pl :xplan-tests-soapui -Psystem-tests -DtestFileName=xplan-webservices-soapui-project.xml \
                  -DbaseUrlServices=${BASE_URL_DIENSTE} -DusernameServices=$Username -DpasswordServices=$Password \
                  -DbaseUrlInspirePlu=${BASE_URL_INSPIRE_PLU} -DusernameInspirePlu=$Username -DpasswordInspirePlu=$Password \
                  -DbaseUrlManagerApi=${BASE_URL_MANAGER_API} -DusernameManagerApi=$Username -DpasswordManagerApi=$Password \
                  -DbaseUrlMapServer=${BASE_URL_MAPSERVER} -DusernameMapServer=$Username -DpasswordMapServer=$Password \
                  -DbaseUrlMapProxy=${BASE_URL_MAPPROXY} -DusernameMapProxy=$Username -DpasswordMapProxy=$Password \
                  -DapiKey=${SERVICES_API_KEY} \
                  -DjwtUrl=$JWT_URL -DjwtClientId=$JWT_CLIENTID'
            }
         }
      }
      stage('Results') {
         steps{
            junit '**/**/target/soapui/TEST-*.xml'
         }
      }
   }
   post {
      success {
         slackSend channel: "${SLACK_CHANNEL}", message: "Die SoapUI Tests (Branch: ${BRANCH}) wurden ohne Fehler gegen die Testumgebung ${SLACK_TESTED_ENVIRONMENT} ausgefuehrt.", teamDomain: "${SLACK_TEAM_DOMAIN}", tokenCredentialId: "${SLACK_TOKEN_CREDENTIAL_ID}"
      }
      failure {
         slackSend channel: "${SLACK_CHANNEL}", message: "ACHTUNG: Bei der Ausfuehrung der SoapUI Tests (Branch: ${BRANCH}) gegen die Testumgebung ${SLACK_TESTED_ENVIRONMENT} traten Fehler auf.", teamDomain: "${SLACK_TEAM_DOMAIN}", tokenCredentialId: "${SLACK_TOKEN_CREDENTIAL_ID}"
      }
      unstable {
         slackSend channel: "${SLACK_CHANNEL}", message: "ACHTUNG: Bei der Ausfuehrung der SoapUI Tests (Branch: ${BRANCH}) gegen die Testumgebung ${SLACK_TESTED_ENVIRONMENT} traten Fehler auf.", teamDomain: "${SLACK_TEAM_DOMAIN}", tokenCredentialId: "${SLACK_TOKEN_CREDENTIAL_ID}"
      }
      always {
          cleanWs notFailBuild: true
      }
   }
}
