pipeline {
   agent any
   tools {
      maven 'maven-3.9'
      jdk 'temurin-jdk17'
   }
   parameters {
      string(name: 'BRANCH', defaultValue: "main", description: 'Set branch')
      string(name: 'CREDENTIALS_ID', defaultValue: "xplanbox.lat-lon.de", description: 'Set id of Jenkins credentials to login to environment (BASIC AUTH)')
      string(name: 'BASE_URL_VALIDATOR_API', defaultValue: "https://xplanbox.lat-lon.de", description: 'Set base URL of XPlanValidatorAPI')
      string(name: 'BASE_URL_MANAGER_API', defaultValue: "https://xplanbox.lat-lon.de", description: 'Set base URL of XPlanManagerAPI')
      string(name: 'BASE_URL_DOKUMENTE_API', defaultValue: "https://xplanbox.lat-lon.de", description: 'Set base URL of XPlanDokumenteAPI')
      string(name: 'BASE_URL_VALIDATOR_WEB', defaultValue: "https://xplanbox.lat-lon.de/xplan-validator-web", description: 'Set base URL of XPlanValidatorWeb')
      string(name: 'SLACK_CHANNEL', defaultValue: "#installationen", description: 'Set slack channel')
      string(name: 'SLACK_TESTED_ENVIRONMENT', defaultValue: "https://xplanbox.lat-lon.de/", description: 'Set name of test environment which shall be printed in slack messages')
      string(name: 'SLACK_TEAM_DOMAIN', defaultValue: "xplanbox", description: 'Set slack team domain')
      string(name: 'SLACK_TOKEN_CREDENTIAL_ID', defaultValue: "slack-integration-id", description: 'Set slack token credential id')
   }
   stages {
      stage('Preparation') {
         steps {
            sh 'mvn --version'
         }
      }
      stage('Integrationstests') {
         steps {
            withCredentials([
               usernamePassword(credentialsId:"${CREDENTIALS_ID}", passwordVariable: 'Password', usernameVariable: 'Username')
            ]) {
				withEnv(['SKIP_RABBIT_TESTS=true']) {
	               sh 'mvn integration-test -pl :xplan-tests-integration -Psystem-tests \
	                   -DXPLAN_VALIDATORAPI_URL_PUBLIC=${BASE_URL_VALIDATOR_API}/xplan-validator-api \
	                   -DXPLAN_MANAGERAPI_URL_PUBLIC=${BASE_URL_MANAGER_API}/xplan-manager-api \
	                   -DXPLAN_DOKUMENTEAPI_URL_PUBLIC=${BASE_URL_DOKUMENTE_API}/xplan-dokumente-api \
	                   -DXPLAN_API_USERNAME=$Username -DXPLAN_API_PASSWORD=$Password \
	                   -DXPLAN_VALIDATOR_WEB_BASE_URL=${BASE_URL_VALIDATOR_WEB} \
	                   -DXPLAN_VALIDATOR_WEB_USERNAME=$Username -DXPLAN_VALIDATOR_WEB_PASSWORD=$Password'
				}
            }
         }
      }
      stage('Results') {
         steps{
            junit '**/**/target/failsafe-reports/TEST-*.xml'
         }
      }
   }
   post {
      success {
         slackSend channel: "${SLACK_CHANNEL}", message: "Die Integrationstests (Branch: ${BRANCH}) wurden ohne Fehler gegen die Testumgebung ${SLACK_TESTED_ENVIRONMENT} ausgefuehrt.", teamDomain: "${SLACK_TEAM_DOMAIN}", tokenCredentialId: "${SLACK_TOKEN_CREDENTIAL_ID}"
      }
      failure {
         slackSend channel: "${SLACK_CHANNEL}", message: "ACHTUNG: Bei der Ausfuehrung der Integrationstests (Branch: ${BRANCH}) gegen die Testumgebung ${SLACK_TESTED_ENVIRONMENT} traten Fehler auf.", teamDomain: "${SLACK_TEAM_DOMAIN}", tokenCredentialId: "${SLACK_TOKEN_CREDENTIAL_ID}"
      }
      unstable {
         slackSend channel: "${SLACK_CHANNEL}", message: "ACHTUNG: Bei der Ausfuehrung der Integrationstests (Branch: ${BRANCH}) gegen die Testumgebung ${SLACK_TESTED_ENVIRONMENT} traten Fehler auf.", teamDomain: "${SLACK_TEAM_DOMAIN}", tokenCredentialId: "${SLACK_TOKEN_CREDENTIAL_ID}"
      }
      always {
          cleanWs notFailBuild: true
      }
   }
}
