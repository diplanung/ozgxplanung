#!/bin/bash

echo "MapServer config initialization..."

if [ -z ${MS_MAPFILE+x} ]; then
	echo "[$(date -Iseconds)] ERROR: environment variable MS_MAPFILE not set!"
	exit 1
fi

if [ -f "$MS_MAPFILE" ]; then
	echo "[$(date -Iseconds)] Init already done ($MS_MAPFILE)"
	exec /usr/local/bin/start-server
	exit 0
fi

echo "[$(date -Iseconds)] Initializing mapserver config ..."

echo "[$(date -Iseconds)] MapProxy config initialization..."
defined_envs="$(printf '${%s} ' $(env | cut -d'=' -f1)) \${AWS_S3_ENDPOINT_WITHOUT_PROTOCOL} \${AWS_HTTPS} \${AWS_VIRTUAL_HOSTING}"
export AWS_S3_ENDPOINT_WITHOUT_PROTOCOL="$(echo $AWS_S3_ENDPOINT | sed s/'http[s]\?:\/\/'//)"

# set AWS_HTTPS=YES|NO
if [[ $AWS_S3_ENDPOINT == https:* ]]; then
    export AWS_HTTPS="YES"
else
    export AWS_HTTPS="NO"
fi

# set AWS_VIRTUAL_HOSTING=true|false
XPLAN_S3_PATHSTYLEACCESS_ENABLED="${XPLAN_S3_PATHSTYLEACCESS_ENABLED:-false}"
if [ $XPLAN_S3_PATHSTYLEACCESS_ENABLED = "true" ]
then
  echo "[$(date -Iseconds)] XPLAN_S3_PATHSTYLEACCESS_ENABLED is enabled"
  export AWS_VIRTUAL_HOSTING="false"
else
  export AWS_VIRTUAL_HOSTING="true"
fi

TARGET_DIR="$(dirname "${MS_MAPFILE}")"
for FILE in /xplan-mapserver-docker/xplan-mapserver-config/*; do
    FILENAME="$(basename "${FILE}")"
    envsubst "$defined_envs" < $FILE > $TARGET_DIR/$FILENAME;
done


XPLAN_SERVICES_DEFAULT_CRS_SRID="${XPLAN_SERVICES_DEFAULT_CRS_SRID:-25832}"
XPLAN_SERVICES_DEFAULT_CRS="${XPLAN_SERVICES_DEFAULT_CRS:-EPSG:25832}"
XPLAN_SERVICES_METADATA_URL="${XPLAN_SERVICES_METADATA_URL}"

######################################
# Update content of mapserver config #
######################################

echo "[$(date -Iseconds)] Default CRS/srid is $XPLAN_SERVICES_DEFAULT_CRS/$XPLAN_SERVICES_DEFAULT_CRS_SRID"
sed -i 's|EPSG:25832|'$XPLAN_SERVICES_DEFAULT_CRS'|g' $MS_MAPFILE
sed -i 's|25832|'$XPLAN_SERVICES_DEFAULT_CRS_SRID'|g' $MS_MAPFILE
echo "Metadata URL: $XPLAN_SERVICES_METADATA_URL"
if [ -z "$XPLAN_SERVICES_METADATA_URL" ]
  then
    echo "[$(date -Iseconds)] Remove metadata configuration"
    sed -i '/wms_metadataurl_list/d' $TARGET_FILE/layers.txt
    sed -i '/wms_metadataurl_xml_format/d' $TARGET_FILE/layers.txt
    sed -i '/wms_metadataurl_xml_type/d' $TARGET_FILE/layers.txt
    sed -i '/wms_metadataurl_xml_href/d' $TARGET_FILE/layers.txt
fi

echo "[$(date -Iseconds)] Start mapserver..."
exec /usr/local/bin/start-server