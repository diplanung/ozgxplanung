<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  xplan-webservices-workspaces - Modul zur Gruppierung aller Workspaces
  %%
  Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->

<FeatureTypeStyle xmlns="http://www.opengis.net/se" xmlns:ogc="http://www.opengis.net/ogc"
                  xmlns:xplan="http://www.deegree.org/xplanung/1/0"
                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                  xsi:schemaLocation="http://www.opengis.net/se http://schemas.opengis.net/se/1.1.0/FeatureStyle.xsd">
  <Name>vollflaechig</Name>
  <FeatureTypeName>xplan:SO_Grenze</FeatureTypeName>
  <!-- 1000 Bundesgrenze -->
  <Rule>
    <Name>so_grenze</Name>
    <Description>
      <Title>SO: Bundesgrenze</Title>
    </Description>
    <ogc:Filter>
      <ogc:PropertyIsEqualTo>
        <ogc:PropertyName>xplan:typ</ogc:PropertyName>
        <ogc:Literal>1000</ogc:Literal>
      </ogc:PropertyIsEqualTo>
    </ogc:Filter>
    <LineSymbolizer uom="meter">
      <Geometry>
        <ogc:PropertyName>xplan:position</ogc:PropertyName>
      </Geometry>
      <Stroke>
        <SvgParameter name="stroke">#ff0000</SvgParameter>
        <SvgParameter name="stroke-opacity">1.0</SvgParameter>
        <SvgParameter name="stroke-width">4.0</SvgParameter>
        <SvgParameter name="stroke-linecap">round</SvgParameter>
        <SvgParameter name="stroke-linejoin">round</SvgParameter>
        <SvgParameter name="stroke-dasharray">6 2 2 2</SvgParameter>
      </Stroke>
    </LineSymbolizer>
  </Rule>
  <!-- 1100 Landesgrenze -->
  <Rule>
    <Name>so_grenze</Name>
    <Description>
      <Title>SO: Landesgrenze</Title>
    </Description>
    <ogc:Filter>
      <ogc:PropertyIsEqualTo>
        <ogc:PropertyName>xplan:typ</ogc:PropertyName>
        <ogc:Literal>1100</ogc:Literal>
      </ogc:PropertyIsEqualTo>
    </ogc:Filter>
    <LineSymbolizer uom="meter">
      <Geometry>
        <ogc:PropertyName>xplan:position</ogc:PropertyName>
      </Geometry>
      <Stroke>
        <SvgParameter name="stroke">#e600a9</SvgParameter>
        <SvgParameter name="stroke-opacity">1.0</SvgParameter>
        <SvgParameter name="stroke-width">4.0</SvgParameter>
        <SvgParameter name="stroke-linecap">round</SvgParameter>
        <SvgParameter name="stroke-linejoin">round</SvgParameter>
        <SvgParameter name="stroke-dasharray">6 2 2 2</SvgParameter>
      </Stroke>
    </LineSymbolizer>
  </Rule>
  <!-- 1200 Regierungsbezirksgrenze -->
  <Rule>
    <Name>so_grenze</Name>
    <Description>
      <Title>SO: Regierungsbezirksgrenze</Title>
    </Description>
    <ogc:Filter>
      <ogc:PropertyIsEqualTo>
        <ogc:PropertyName>xplan:typ</ogc:PropertyName>
        <ogc:Literal>1200</ogc:Literal>
      </ogc:PropertyIsEqualTo>
    </ogc:Filter>
    <MaxScaleDenominator>25000</MaxScaleDenominator>
    <LineSymbolizer uom="meter">
      <Geometry>
        <ogc:PropertyName>xplan:position</ogc:PropertyName>
      </Geometry>
      <Stroke>
        <SvgParameter name="stroke">#000000</SvgParameter>
        <SvgParameter name="stroke-opacity">1.0</SvgParameter>
        <SvgParameter name="stroke-width">3.0</SvgParameter>
        <SvgParameter name="stroke-linecap">round</SvgParameter>
        <SvgParameter name="stroke-linejoin">round</SvgParameter>
        <SvgParameter name="stroke-dasharray">6 2 2 2</SvgParameter>
      </Stroke>
    </LineSymbolizer>
  </Rule>
  <!-- 1250 Bezirksgrenze -->
  <!-- 1300 Kreisgrenze -->
  <Rule>
    <Name>so_grenze</Name>
    <Description>
      <Title>SO: Kreisgrenze</Title>
    </Description>
    <ogc:Filter>
      <ogc:PropertyIsEqualTo>
        <ogc:PropertyName>xplan:typ</ogc:PropertyName>
        <ogc:Literal>1300</ogc:Literal>
      </ogc:PropertyIsEqualTo>
    </ogc:Filter>
    <MaxScaleDenominator>25000</MaxScaleDenominator>
    <LineSymbolizer uom="meter">
      <Geometry>
        <ogc:PropertyName>xplan:position</ogc:PropertyName>
      </Geometry>
      <Stroke>
        <SvgParameter name="stroke">#ff0000</SvgParameter>
        <SvgParameter name="stroke-opacity">1.0</SvgParameter>
        <SvgParameter name="stroke-width">3.0</SvgParameter>
        <SvgParameter name="stroke-linecap">round</SvgParameter>
        <SvgParameter name="stroke-linejoin">round</SvgParameter>
        <SvgParameter name="stroke-dasharray">4 2 1 2 1 2</SvgParameter>
      </Stroke>
    </LineSymbolizer>
  </Rule>
  <!-- 1400 Gemeindegrenze -->
  <Rule>
    <Name>so_grenze</Name>
    <Description>
      <Title>SO: Gemeindegrenze</Title>
    </Description>
    <ogc:Filter>
      <ogc:PropertyIsEqualTo>
        <ogc:PropertyName>xplan:typ</ogc:PropertyName>
        <ogc:Literal>1400</ogc:Literal>
      </ogc:PropertyIsEqualTo>
    </ogc:Filter>
    <MaxScaleDenominator>25000</MaxScaleDenominator>
    <LineSymbolizer uom="meter">
      <Geometry>
        <ogc:PropertyName>xplan:position</ogc:PropertyName>
      </Geometry>
      <Stroke>
        <SvgParameter name="stroke">#ffb2b2</SvgParameter>
        <SvgParameter name="stroke-opacity">1.0</SvgParameter>
        <SvgParameter name="stroke-width">2.0</SvgParameter>
        <SvgParameter name="stroke-linecap">round</SvgParameter>
        <SvgParameter name="stroke-linejoin">round</SvgParameter>
        <SvgParameter name="stroke-dasharray">4 2 1 2 1 2</SvgParameter>
      </Stroke>
    </LineSymbolizer>
  </Rule>
  <!-- 1500 Samtgemeindegrenze -->
  <Rule>
    <Name>so_grenze</Name>
    <Description>
      <Title>SO: Samtgemeindegrenze</Title>
    </Description>
    <ogc:Filter>
      <ogc:PropertyIsEqualTo>
        <ogc:PropertyName>xplan:typ</ogc:PropertyName>
        <ogc:Literal>1500</ogc:Literal>
      </ogc:PropertyIsEqualTo>
    </ogc:Filter>
    <MaxScaleDenominator>25000</MaxScaleDenominator>
    <LineSymbolizer uom="meter">
      <Geometry>
        <ogc:PropertyName>xplan:position</ogc:PropertyName>
      </Geometry>
      <Stroke>
        <SvgParameter name="stroke">#ffb2b2</SvgParameter>
        <SvgParameter name="stroke-opacity">1.0</SvgParameter>
        <SvgParameter name="stroke-width">1.0</SvgParameter>
        <SvgParameter name="stroke-linecap">round</SvgParameter>
        <SvgParameter name="stroke-linejoin">round</SvgParameter>
        <SvgParameter name="stroke-dasharray">4 2 1 2 1 2</SvgParameter>
      </Stroke>
    </LineSymbolizer>
  </Rule>
  <!-- 1510 Mitgliedsgemeindegrenze -->
  <!-- 1550 Amtsgrenze -->
  <!-- 1600 Stadtteilgrenze -->
  <Rule>
    <Name>so_grenze</Name>
    <Description>
      <Title>SO: Stadtteilgrenze</Title>
    </Description>
    <ogc:Filter>
      <ogc:PropertyIsEqualTo>
        <ogc:PropertyName>xplan:typ</ogc:PropertyName>
        <ogc:Literal>1600</ogc:Literal>
      </ogc:PropertyIsEqualTo>
    </ogc:Filter>
    <MaxScaleDenominator>25000</MaxScaleDenominator>
    <LineSymbolizer uom="meter">
      <Geometry>
        <ogc:PropertyName>xplan:position</ogc:PropertyName>
      </Geometry>
      <Stroke>
        <SvgParameter name="stroke">#ffb2b2</SvgParameter>
        <SvgParameter name="stroke-opacity">1.0</SvgParameter>
        <SvgParameter name="stroke-width">2.0</SvgParameter>
        <SvgParameter name="stroke-linecap">round</SvgParameter>
        <SvgParameter name="stroke-linejoin">round</SvgParameter>
        <SvgParameter name="stroke-dasharray">4 2 4 2 1 2</SvgParameter>
      </Stroke>
    </LineSymbolizer>
  </Rule>
  <!-- 2000 VorgeschlageneGrundstuecksgrenze -->
  <Rule>
    <Name>so_grenze</Name>
    <Description>
      <Title>SO: VorgeschlageneGrundstuecksgrenze</Title>
    </Description>
    <ogc:Filter>
      <ogc:PropertyIsEqualTo>
        <ogc:PropertyName>xplan:typ</ogc:PropertyName>
        <ogc:Literal>1600</ogc:Literal>
      </ogc:PropertyIsEqualTo>
    </ogc:Filter>
    <MaxScaleDenominator>25000</MaxScaleDenominator>
    <LineSymbolizer uom="meter">
      <Geometry>
        <ogc:PropertyName>xplan:position</ogc:PropertyName>
      </Geometry>
      <Stroke>
        <SvgParameter name="stroke">#000000</SvgParameter>
        <SvgParameter name="stroke-opacity">1.0</SvgParameter>
        <SvgParameter name="stroke-width">2.0</SvgParameter>
        <SvgParameter name="stroke-linecap">round</SvgParameter>
        <SvgParameter name="stroke-linejoin">round</SvgParameter>
        <SvgParameter name="stroke-dasharray">4 2 2 2 2 2 2 2</SvgParameter>
      </Stroke>
    </LineSymbolizer>
  </Rule>
  <!-- 2100 GrenzeBestehenderBebauungsplan -->
  <Rule>
    <Name>so_grenze</Name>
    <Description>
      <Title>SO: GrenzeBestehenderBebauungsplan</Title>
    </Description>
    <ogc:Filter>
      <ogc:PropertyIsEqualTo>
        <ogc:PropertyName>xplan:typ</ogc:PropertyName>
        <ogc:Literal>2100</ogc:Literal>
      </ogc:PropertyIsEqualTo>
    </ogc:Filter>
    <LineSymbolizer uom="meter">
      <Geometry>
        <ogc:PropertyName>xplan:position</ogc:PropertyName>
      </Geometry>
      <Stroke>
        <SvgParameter name="stroke">#ff0000</SvgParameter>
        <SvgParameter name="stroke-opacity">1.0</SvgParameter>
        <SvgParameter name="stroke-width">2.0</SvgParameter>
        <SvgParameter name="stroke-linecap">round</SvgParameter>
        <SvgParameter name="stroke-linejoin">round</SvgParameter>
        <SvgParameter name="stroke-dasharray">4 2 2 2 2 2 2 2</SvgParameter>
      </Stroke>
    </LineSymbolizer>
  </Rule>
  <!-- 1600 SonstGrenze -->
  <Rule>
    <Name>so_grenze</Name>
    <Description>
      <Title>SO: SonstGrenze</Title>
    </Description>
    <ogc:Filter>
      <ogc:PropertyIsEqualTo>
        <ogc:PropertyName>xplan:typ</ogc:PropertyName>
        <ogc:Literal>9999</ogc:Literal>
      </ogc:PropertyIsEqualTo>
    </ogc:Filter>
    <MaxScaleDenominator>25000</MaxScaleDenominator>
    <LineSymbolizer uom="meter">
      <Geometry>
        <ogc:PropertyName>xplan:position</ogc:PropertyName>
      </Geometry>
      <Stroke>
        <SvgParameter name="stroke">#000000</SvgParameter>
        <SvgParameter name="stroke-opacity">1.0</SvgParameter>
        <SvgParameter name="stroke-width">2.0</SvgParameter>
        <SvgParameter name="stroke-linecap">round</SvgParameter>
        <SvgParameter name="stroke-linejoin">round</SvgParameter>
        <SvgParameter name="stroke-dasharray">2 2</SvgParameter>
      </Stroke>
    </LineSymbolizer>
  </Rule>
  <!-- Alle anderen -->
  <Rule>
    <Name>so_grenze</Name>
    <Description>
      <Title>SO: Grenze</Title>
    </Description>
    <ogc:Filter>
      <ogc:Not>
        <ogc:And>
          <ogc:PropertyIsEqualTo>
            <ogc:PropertyName>xplan:typ</ogc:PropertyName>
            <ogc:Literal>1000</ogc:Literal>
          </ogc:PropertyIsEqualTo>
          <ogc:PropertyIsEqualTo>
            <ogc:PropertyName>xplan:typ</ogc:PropertyName>
            <ogc:Literal>1100</ogc:Literal>
          </ogc:PropertyIsEqualTo>
          <ogc:PropertyIsEqualTo>
            <ogc:PropertyName>xplan:typ</ogc:PropertyName>
            <ogc:Literal>1200</ogc:Literal>
          </ogc:PropertyIsEqualTo>
          <ogc:PropertyIsEqualTo>
            <ogc:PropertyName>xplan:typ</ogc:PropertyName>
            <ogc:Literal>1300</ogc:Literal>
          </ogc:PropertyIsEqualTo>
          <ogc:PropertyIsEqualTo>
            <ogc:PropertyName>xplan:typ</ogc:PropertyName>
            <ogc:Literal>1400</ogc:Literal>
          </ogc:PropertyIsEqualTo>
          <ogc:PropertyIsEqualTo>
            <ogc:PropertyName>xplan:typ</ogc:PropertyName>
            <ogc:Literal>1500</ogc:Literal>
          </ogc:PropertyIsEqualTo>
          <ogc:PropertyIsEqualTo>
            <ogc:PropertyName>xplan:typ</ogc:PropertyName>
            <ogc:Literal>1600</ogc:Literal>
          </ogc:PropertyIsEqualTo>
          <ogc:PropertyIsEqualTo>
            <ogc:PropertyName>xplan:typ</ogc:PropertyName>
            <ogc:Literal>2000</ogc:Literal>
          </ogc:PropertyIsEqualTo>
          <ogc:PropertyIsEqualTo>
            <ogc:PropertyName>xplan:typ</ogc:PropertyName>
            <ogc:Literal>2100</ogc:Literal>
          </ogc:PropertyIsEqualTo>
          <ogc:PropertyIsEqualTo>
            <ogc:PropertyName>xplan:typ</ogc:PropertyName>
            <ogc:Literal>9999</ogc:Literal>
          </ogc:PropertyIsEqualTo>
        </ogc:And>
      </ogc:Not>
    </ogc:Filter>
    <LineSymbolizer uom="meter">
      <Geometry>
        <ogc:PropertyName>xplan:position</ogc:PropertyName>
      </Geometry>
      <Stroke>
        <SvgParameter name="stroke">#000000</SvgParameter>
        <SvgParameter name="stroke-opacity">1.0</SvgParameter>
        <SvgParameter name="stroke-width">2.0</SvgParameter>
        <SvgParameter name="stroke-linecap">round</SvgParameter>
        <SvgParameter name="stroke-linejoin">round</SvgParameter>
      </Stroke>
    </LineSymbolizer>
  </Rule>
</FeatureTypeStyle>
