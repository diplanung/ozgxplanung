###
# #%L
# xplan-mapproxy-config - Modul zur Gruppierung aller Webservices
# %%
# Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# #L%
###
seeds:
  bpreseed_25832:
    caches: [bplan_raster_cache_25832,bplan_vektor_cache_25832]
    coverages: [bpchanges]
    refresh_before:
      days: 0
  fpreseed_25832:
    caches: [fplan_raster_cache_25832,fplan_vektor_cache_25832]
    coverages: [fpchanges]
    refresh_before:
      days: 0
  lpreseed_25832:
    caches: [lplan_raster_cache_25832,lplan_vektor_cache_25832]
    coverages: [lpchanges]
    refresh_before:
      days: 0
  rpreseed_25832:
    caches: [rplan_raster_cache_25832,rplan_vektor_cache_25832]
    coverages: [rpchanges]
    refresh_before:
      days: 0
  soreseed_25832:
    caches: [soplan_raster_cache_25832,soplan_vektor_cache_25832]
    coverages: [sochanges]
    refresh_before:
      days: 0
  bpreseed_3857:
    caches: [bplan_raster_cache_3857,bplan_vektor_cache_3857]
    coverages: [bpchanges]
    refresh_before:
      days: 0
  fpreseed_3857:
    caches: [fplan_raster_cache_3857,fplan_vektor_cache_3857]
    coverages: [fpchanges]
    refresh_before:
      days: 0
  lpreseed_3857:
    caches: [lplan_raster_cache_3857,lplan_vektor_cache_3857]
    coverages: [lpchanges]
    refresh_before:
      days: 0
  rpreseed_3857:
    caches: [rplan_raster_cache_3857,rplan_vektor_cache_3857]
    coverages: [rpchanges]
    refresh_before:
      days: 0
  soreseed_3857:
    caches: [soplan_raster_cache_3857,soplan_vektor_cache_3857]
    coverages: [sochanges]
    refresh_before:
      days: 0
  bp_fullseed_25832:
    caches: [bplan_raster_cache_25832,bplan_vektor_cache_25832]
    coverages: [bp_all]
    refresh_before:
      days: 0
  fp_fullseed_25832:
    caches: [fplan_raster_cache_25832,fplan_vektor_cache_25832]
    coverages: [fp_all]
    refresh_before:
      days: 0
  lp_fullseed_25832:
    caches: [lplan_raster_cache_25832,lplan_vektor_cache_25832]
    coverages: [lp_all]
    refresh_before:
      days: 0
  rp_fullseed_25832:
    caches: [rplan_raster_cache_25832,rplan_vektor_cache_25832]
    coverages: [rp_all]
    refresh_before:
      days: 0
  so_fullseed_25832:
    caches: [soplan_raster_cache_25832,soplan_vektor_cache_25832]
    coverages: [so_all]
    refresh_before:
      days: 0
  bp_fullseed_3857:
    caches: [bplan_raster_cache_3857,bplan_vektor_cache_3857]
    coverages: [bp_all]
    refresh_before:
      days: 0
  fp_fullseed_3857:
    caches: [fplan_raster_cache_3857,fplan_vektor_cache_3857]
    coverages: [fp_all]
    refresh_before:
      days: 0
  lp_fullseed_3857:
    caches: [lplan_raster_cache_3857,lplan_vektor_cache_3857]
    coverages: [lp_all]
    refresh_before:
      days: 0
  rp_fullseed_3857:
    caches: [rplan_raster_cache_3857,rplan_vektor_cache_3857]
    coverages: [rp_all]
    refresh_before:
      days: 0
  so_fullseed_3857:
    caches: [soplan_raster_cache_3857,soplan_vektor_cache_3857]
    coverages: [so_all]
    refresh_before:
      days: 0
coverages:
  bpchanges:
    # da die Connection mit PQconnectdb()/libpq gemacht wird, müssten alle Verbindungsparameter über ENVs gesetzt werden können: https://www.postgresql.org/docs/12/libpq-envars.html (ungetestet)
    datasource: "PG:"
    where: "select plan_id, ST_Union(bbox) as bbox, array_agg(operation) operations from xplanmgr.planslog where xp_type='BP_Plan' and (planstatus_new='Festgestellt' or planstatus_old='Festgestellt') and last_update_date > localtimestamp - interval '1 day' and plan_id not in (select plan_id from xplanmgr.planslog WHERE last_update_date > localtimestamp - interval '1 d' GROUP BY plan_id HAVING array_agg(operation) @>  ARRAY['INSERT'::varchar(6),'DELETE'::varchar(6)]) GROUP BY plan_id"
    srs: 'EPSG:4326'
  fpchanges:
    datasource: "PG:"
    where: "select plan_id, ST_Union(bbox) as bbox, array_agg(operation) operations from xplanmgr.planslog where xp_type='FP_Plan' and (planstatus_new='Festgestellt' or planstatus_old='Festgestellt') and last_update_date > localtimestamp - interval '1 day' and plan_id not in (select plan_id from xplanmgr.planslog WHERE last_update_date > localtimestamp - interval '1 d' GROUP BY plan_id HAVING array_agg(operation) @>  ARRAY['INSERT'::varchar(6),'DELETE'::varchar(6)]) GROUP BY plan_id"
    srs: 'EPSG:4326'
  lpchanges:
    datasource: "PG:"
    where: "select plan_id, ST_Union(bbox) as bbox, array_agg(operation) operations from xplanmgr.planslog where xp_type='LP_Plan' and (planstatus_new='Festgestellt' or planstatus_old='Festgestellt') and last_update_date > localtimestamp - interval '1 day' and plan_id not in (select plan_id from xplanmgr.planslog WHERE last_update_date > localtimestamp - interval '1 d' GROUP BY plan_id HAVING array_agg(operation) @>  ARRAY['INSERT'::varchar(6),'DELETE'::varchar(6)]) GROUP BY plan_id"
    srs: 'EPSG:4326'
  rpchanges:
    datasource: "PG:"
    where: "select plan_id, ST_Union(bbox) as bbox, array_agg(operation) operations from xplanmgr.planslog where xp_type='RP_Plan' and (planstatus_new='Festgestellt' or planstatus_old='Festgestellt') and last_update_date > localtimestamp - interval '1 day' and plan_id not in (select plan_id from xplanmgr.planslog WHERE last_update_date > localtimestamp - interval '1 d' GROUP BY plan_id HAVING array_agg(operation) @>  ARRAY['INSERT'::varchar(6),'DELETE'::varchar(6)]) GROUP BY plan_id"
    srs: 'EPSG:4326'
  sochanges:
    datasource: "PG:"
    where: "select plan_id, ST_Union(bbox) as bbox, array_agg(operation) operations from xplanmgr.planslog where xp_type='SO_Plan' and (planstatus_new='Festgestellt' or planstatus_old='Festgestellt') and last_update_date > localtimestamp - interval '1 day' and plan_id not in (select plan_id from xplanmgr.planslog WHERE last_update_date > localtimestamp - interval '1 d' GROUP BY plan_id HAVING array_agg(operation) @>  ARRAY['INSERT'::varchar(6),'DELETE'::varchar(6)]) GROUP BY plan_id"
    srs: 'EPSG:4326'
  bp_all:
    datasource: "PG:"
    where: "select ST_UNION(ARRAY(select bbox from xplanmgr.plans where planstatus='Festgestellt' and xp_type='BP_Plan'))"
    srs: 'EPSG:4326'
  fp_all:
    datasource: "PG:"
    where: "select ST_UNION(ARRAY(select bbox from xplanmgr.plans where planstatus='Festgestellt' and xp_type='FP_Plan'))"
    srs: 'EPSG:4326'
  lp_all:
    datasource: "PG:"
    where: "select ST_UNION(ARRAY(select bbox from xplanmgr.plans where planstatus='Festgestellt' and xp_type='LP_Plan'))"
    srs: 'EPSG:4326'
  rp_all:
    datasource: "PG:"
    where: "select ST_UNION(ARRAY(select bbox from xplanmgr.plans where planstatus='Festgestellt' and xp_type='RP_Plan'))"
    srs: 'EPSG:4326'
  so_all:
    datasource: "PG:"
    where: "select ST_UNION(ARRAY(select bbox from xplanmgr.plans where planstatus='Festgestellt' and xp_type='SO_Plan'))"
    srs: 'EPSG:4326'