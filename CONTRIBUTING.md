# Mitarbeit

Eine Mitarbeit an dem Projekt ist erwünscht und auf verschiedenen Wegen möglich. So können neben Fehlerberichten, Änderungswünschen auch Verbesserungen des Quellcodes beigesteuert werden. Bitte beachten Sie die für das Projekt geltenden Bedingungen aus der [Open Source Lizenz](LICENSE.txt).

## Fehler melden

Wenn Sie Fehler in der Anwendung finden, dann können Sie unter [Issues > Create Issue](../../issues/new) einen Fehlerbericht erstellen. Eine Übersicht aller bereits erstellten Fehlerberichte finden Sie ebenfalls unter [Issues](../../issues).

> **_Hinweis_**: Bitte beachten Sie, dass Fehler oder Probleme im Hinblick "klassische" Installationen und Betriebsarten mit dem Label "classic" versehen und nachrangig behandelt werden. Bitte beachten Sie dazu die Hinweise in der [README-Datei](README.md).

## Änderungen

Wenn Sie Verbesserungen oder Erweiterungen zum Projekt beisteuern möchten, dann können Sie Ihre Änderungen als [Pull bzw. Merge Request](../../merge_requests/) zur Verfügung stellen. Pull Requests (PRs) sind eine Möglichkeit zum Ändern, Überprüfen und Zusammenführen von Code in einem Git-Repository. PRs können aus Branches innerhalb dieses Repositories oder aus Branches in Forks auf der OpenCoDE-Plattform stammen.

### Voraussetzungen für die Übernahme von Änderungen

Der gesamte Code ist durch den Hersteller lat/lon erstellt worden, wenn nicht anders gekennzeichnet, und wurde speziell für die Verwendung im Rahmen des OZG-Kontexts einem umfangreichen Lizenz-Audit unterzogen. Es wurde kein Code aus unbekannten Quellen im Internet kopiert. Wenn Sie also eine Änderung vorschlagen möchten und einen PR als Proof-of-Concept beifügen, wäre das großartig. Seien Sie jedoch bitte nicht enttäuscht, wenn wir Ihren PR von Grund auf neu schreiben. Denn um die xPlanBox öffentlich zugänglich zu halten und sicherzustellen, dass der Code nicht mit proprietären oder lizenzierten Inhalten kontaminiert wird, akzeptiert das Projekt keine PR von Personen, die keine eidesstattliche Erklärung abgegeben haben, dass ihr Beitrag unter der [Lizenz](LICENSE.txt) öffentlich zugänglich gemacht werden kann. Da die Übernahme und die Pflege von Änderungen viel Zeit in Anspruch nehmen kann, ist eine verbindliche Erklärung der längerfristigen Übernahme der Code Ownership für die eingebrachten Änderungen ebenfalls erforderlich. 

###  Übernahme von Änderungen

Über die Übernahme eines PR in den Hauptentwicklungszweig (main branch) entscheiden die Mitglieder der Gruppe "Maintainer" dieses Repositories. Damit ein PR übernommen werden kann, müssen mindestens drei Mitglieder der Gruppe diesem zustimmen (6-Augen-Prinzip).
