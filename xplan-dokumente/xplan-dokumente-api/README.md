# XPlanDokumenteAPI

OpenAPI: #host#/xplan-dokumente-api/xdokumente/api/v1/

## Development

The application can be started locally with

```
mvn spring-boot:run -Dspring-boot.run.profiles=dev
```

(database xplan-db required on localhost:5432 with credentials xplanbox-app-user/postgres_app_password)

The application starts on port 8092:

- v1 API: http://localhost:8092/xplan-dokumente-api/xdokumente/api/v1/

## Technische Endpoints

- `/xplan-dokumente-api/actuator/health/liveness`: liveness check
    ```
    $> curl http://localhost:8092/xplan-dokumente-api/actuator/health/liveness
    {"status":"UP"}
    ```

- `/xplan-dokumente-api/actuator/info`: build infos
    ```
    $> curl http://localhost:8092/xplan-dokumente-api/actuator/info
    {"build":{"gitRevision":"08e35dc18b463715bbf0feab0c4c3a2e8c8a0904","key":""}}
    ```
