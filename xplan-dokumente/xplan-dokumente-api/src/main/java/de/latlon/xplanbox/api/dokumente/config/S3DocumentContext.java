/*-
 * #%L
 * xplan-dokumente-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.dokumente.config;

import static org.slf4j.LoggerFactory.getLogger;

import de.latlon.xplan.commons.s3.S3Storage;
import de.latlon.xplan.commons.s3.config.AmazonS3ReadOnlyContext;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import software.amazon.awssdk.services.s3.S3Client;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 7.0
 */
@Configuration
@Import(AmazonS3ReadOnlyContext.class)
public class S3DocumentContext {

	private static final Logger LOG = getLogger(S3DocumentContext.class);

	@Bean
	public S3Storage documentStorage(S3Client s3Client,
			@Value("${xplanbox.s3.bucketName:#{environment.XPLAN_S3_BUCKET_ATTACHMENTS}}") String bucketName) {
		LOG.info("Instantiate S3Storage to manage documents");
		return new S3Storage(s3Client, bucketName);
	}

}
