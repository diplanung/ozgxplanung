/*-
 * #%L
 * xplan-dokumente-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.dokumente.v1;

import static org.assertj.core.api.Assertions.assertThat;

import jakarta.ws.rs.core.Response;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 7.0
 */
class DokumentApiTest extends DokumenteApiJerseyTest<DokumentApi> {

	@Test
	void verifyThat_ListDokumente_Response_ContainsCorrectStatusCode() {
		final Response response = target("/dokument/1").request().get();
		assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
	}

	@Test
	void verifyThat_HeadDokument_Response_ContainsCorrectStatusCode() {
		final Response response = target("/dokument/1/test.png").request().head();
		assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
	}

	@Test
	void verifyThat_GetDokument_Response_ContainsCorrectStatusCode() {
		final Response response = target("/dokument/1/test.png").request().get();
		assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
	}

}
