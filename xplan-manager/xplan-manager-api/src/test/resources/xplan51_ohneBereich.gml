<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<!--Erzeugt mit KIT (www.kit.edu) GML-Toolbox, Erstellungsdatum: 10/15/18-->
<xplan:XPlanAuszug xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink"
                   xmlns:xplan="http://www.xplanung.de/xplangml/5/1"
                   gml:id="GML_597bdedb-0d50-41f9-9cd3-e021ee53e2e9">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25833">
      <gml:lowerCorner>417921.386 5715677.826</gml:lowerCorner>
      <gml:upperCorner>418523.944 5716033.278</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="FEATURE_2dabcaaf-f831-4d73-b9bb-04b78f52ed54">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25833">
          <gml:lowerCorner>417921.386 5715677.826</gml:lowerCorner>
          <gml:upperCorner>418523.944 5716033.278</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>"Heideweg"</xplan:name>
      <xplan:nummer>Nr.1</xplan:nummer>
      <xplan:technHerstellDatum>2001-08-27</xplan:technHerstellDatum>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:MultiSurface srsName="EPSG:25833" gml:id="GEOMETRY_454464fe-7b68-49c3-b648-bf62e49f2843">
          <gml:surfaceMember>
            <gml:Polygon srsName="EPSG:25833" gml:id="GML_02d4e636-4d91-47e7-8458-a6bed03e61dd">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsDimension="2" count="107">418355.941 5715848.133 418356.978 5715844.25 418364.584 5715813.894 418361.553 5715806.515 418384.129 5715709.723 418390.285 5715677.826 418430.4 5715687.627 418450.975 5715692.653 418460.249 5715694.026 418503.134 5715700.375 418496.968 5715724.624 418496.216 5715727.983 418487.652 5715735.774 418478.242 5715744.348 418469.148 5715752.631 418457.489 5715763.255 418447.696 5715772.183 418440.755 5715778.506 418431.606 5715786.842 418420.107 5715797.319 418414.567 5715811.579 418442.161 5715818.749 418470.2 5715826.034 418507.655 5715838.463 418493.482 5715888.664 418506.873 5715888.101 418508.013 5715898.578 418523.944 5715898.385 418523.778 5715913.592 418523.75 5715916.193 418522.52 5715918.071 418515.986 5715926.403 418513.199 5715929.655 418512.209 5715930.811 418498.02 5715947.369 418438.52 5715990.115 418416.866 5716005.666 418410.434 5716010.358 418378.954 5716033.278 418374.8 5716032.225 418370.339 5716031.093 418360.821 5716028.679 418340.836 5716023.61 418330.326 5716020.945 418321.922 5716018.813 418313.059 5716016.565 418282.381 5716008.785 418198.296 5715987.458 418157.999 5715977.238 418117.296 5715966.914 418105.754 5715963.987 418101.213 5715961.185 418081.185 5715948.824 418071.203 5715942.664 418052.049 5715943.912 418000.967 5715947.241 418000.433 5715948.123 418001.253 5715967.183 417992.271 5715961.587 417965.833 5715945.116 417931.624 5715923.804 417922.86 5715918.344 417929.061 5715906.241 417933.021 5715898.942 417935.068 5715895.206 417936.815 5715892.017 417937.932 5715889.889 417938.631 5715888.553 417939.602 5715886.696 417942.154 5715881.873 417964.77 5715839.128 417969.442 5715828.12 417968.157 5715821.377 417966.252 5715815.686 417963.973 5715808.906 417958.847 5715798.597 417947.867 5715782.944 417924.514 5715744.942 417921.386 5715738.578 417928.549 5715739.499 417955.467 5715742.959 417983.489 5715746.56 418025.436 5715751.952 418025.026 5715754.16 418023.457 5715762.607 418021.972 5715770.617 418018.864 5715787.393 418024.937 5715793.183 418048.723 5715772.8 418054.711 5715767.676 418064.177 5715760.796 418077.98 5715749.572 418084.362 5715745.492 418112.546 5715723.69 418127.141 5715712.402 418134.066 5715702.197 418145.135 5715685.478 418194.463 5715710.782 418155.678 5715780.293 418151.761 5715787.185 418151.714 5715787.268 418143.976 5715793.344 418136.707 5715824.698 418324.171 5715864.492 418350.514 5715870.045 418353.09 5715859.766 418355.941 5715848.133 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:externeReferenz>
        <xplan:XP_SpezExterneReferenz>
          <xplan:referenzName>B-Plan_Klingmuehl_Heideweg_Leg</xplan:referenzName>
          <xplan:referenzURL>B-Plan_Klingmuehl_Heideweg_Leg.pdf</xplan:referenzURL>
          <xplan:typ>1010</xplan:typ>
        </xplan:XP_SpezExterneReferenz>
      </xplan:externeReferenz>
      <xplan:externeReferenz>
        <xplan:XP_SpezExterneReferenz>
          <xplan:art>PlanMitGeoreferenz</xplan:art>
          <xplan:referenzName>B-Plan_Klingmuehl_Heideweg</xplan:referenzName>
          <xplan:referenzURL>B-Plan_Klingmuehl_Heideweg.tif</xplan:referenzURL>
          <xplan:typ>1030</xplan:typ>
        </xplan:XP_SpezExterneReferenz>
      </xplan:externeReferenz>
      <xplan:externeReferenz>
        <xplan:XP_SpezExterneReferenz>
          <xplan:art>Gruen</xplan:art>
          <xplan:referenzName>B-Plan_Klingmuehl_Heideweg_Gruen</xplan:referenzName>
          <xplan:georefURL>B-Plan_Klingmuehl_Heideweg_Gruen.pgw</xplan:georefURL>
          <xplan:referenzURL>B-Plan_Klingmuehl_Heideweg_Gruen.png</xplan:referenzURL>
          <xplan:typ>2300</xplan:typ>
        </xplan:XP_SpezExterneReferenz>
      </xplan:externeReferenz>
      <xplan:texte xlink:href="#FEATURE_0f870967-bd6f-4367-9150-8a255f0290ad" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>12062425</xplan:ags>
          <xplan:gemeindeName>Gemeinde Sallgast vertreten durch "Amt Kleine
            Elster"</xplan:gemeindeName>
          <xplan:ortsteilName>Klingmuehl</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:rechtsstand>4000</xplan:rechtsstand>
      <xplan:hoehenbezug>HN</xplan:hoehenbezug>
      <xplan:inkrafttretensDatum>2002-02-01</xplan:inkrafttretensDatum>
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
  <xplan:BP_TextAbschnitt gml:id="FEATURE_0f870967-bd6f-4367-9150-8a255f0290ad">
    <xplan:schluessel>key</xplan:schluessel>
    <xplan:gesetzlicheGrundlage>base</xplan:gesetzlicheGrundlage>
    <xplan:text>Beschreibungstext</xplan:text>
    <xplan:refText>
      <xplan:XP_ExterneReferenz>
        <xplan:referenzName>B-Plan_Klingmuehl_Heideweg_Text
        </xplan:referenzName>
        <xplan:referenzURL>B-Plan_Klingmuehl_Heideweg_Text.pdf</xplan:referenzURL>
      </xplan:XP_ExterneReferenz>
    </xplan:refText>
  </xplan:BP_TextAbschnitt>
  </gml:featureMember>
</xplan:XPlanAuszug>
