/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.v2;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.assertj.core.api.Assertions.assertThat;

import de.latlon.xplanbox.api.manager.ManagerApiJerseyTest;
import jakarta.ws.rs.core.Response;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:friebe@lat-lon.de">Torsten Friebe</a>
 */
class PlansApi2Test extends ManagerApiJerseyTest<PlansApi2> {

	@Test
	void verifyThat_GetPlansByName_ReturnCorrectStatus() {
		Response response = target("/plans").queryParam("planName", "bplan_41")
			.request()
			.accept(APPLICATION_JSON)
			.get();
		assertThat(response.getStatus()).isEqualTo(200);
		String responseEntity = response.readEntity(String.class);
		assertThat(responseEntity).contains(
				"{\"id\":123,\"type\":\"BP_Plan\",\"version\":\"XPLAN_41\",\"planStatus\":\"FESTGESTELLT\",\"raster\":false,\"importDate\":null,\"inspirePublished\":false,\"bbox\":null,");
		assertThat(responseEntity).doesNotContain("\"geltungsbereichWGS84\"");
	}

	@Test
	void verifyThat_GetPlansById_ReturnCorrectStatus() {
		Response response = target("/plans").queryParam("planId", 123)
			.queryParam("planId", 2)
			.request()
			.accept(APPLICATION_JSON)
			.get();
		assertThat(response.getStatus()).isEqualTo(200);
		String responseEntity = response.readEntity(String.class);
		assertThat(responseEntity).contains(
				"{\"id\":123,\"type\":\"BP_Plan\",\"version\":\"XPLAN_41\",\"planStatus\":\"FESTGESTELLT\",\"raster\":false,\"importDate\":null,\"inspirePublished\":false,\"bbox\":null,");
		assertThat(responseEntity).contains(
				"{\"id\":2,\"type\":\"BP_Plan\",\"version\":\"XPLAN_51\",\"planStatus\":\"FESTGESTELLT\",\"raster\":false,\"importDate\":null,\"inspirePublished\":false,\"bbox\":null,");
		assertThat(responseEntity).doesNotContain("\"geltungsbereichWGS84\"");
	}

	@Test
	void verifyThat_GetPlansById_includeGeltungsbereich() {
		Response response = target("/plans").queryParam("planId", 123)
			.queryParam("includeGeltungsbereich", "true")
			.request()
			.accept(APPLICATION_JSON)
			.get();
		assertThat(response.getStatus()).isEqualTo(200);
		String responseEntity = response.readEntity(String.class);
		assertThat(responseEntity).contains(
				"{\"id\":123,\"type\":\"BP_Plan\",\"version\":\"XPLAN_41\",\"planStatus\":\"FESTGESTELLT\",\"raster\":false,\"importDate\":null,\"inspirePublished\":false,\"bbox\":null,\"geltungsbereichWGS84\":{\"type\":\"Polygon\",\"bbox\":null,\"coordinates\":[[[0.0,0.0],[1.0,0.0],[1.0,1.0],[0.0,1.0],[0.0,0.0]]]},");
	}

	@Test
	void verifyThat_GetPlansByPlanStatus_ReturnCorrectStatus() {
		Response response = target("/plans").queryParam("planStatus", "FESTGESTELLT")
			.queryParam("planStatus", "ARCHIVIERT")
			.request()
			.accept(APPLICATION_JSON)
			.get();
		assertThat(response.getStatus()).isEqualTo(200);
	}

}
