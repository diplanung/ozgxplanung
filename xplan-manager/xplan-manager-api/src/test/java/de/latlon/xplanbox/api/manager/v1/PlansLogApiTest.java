package de.latlon.xplanbox.api.manager.v1;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.assertj.core.api.Assertions.assertThat;

import de.latlon.xplanbox.api.manager.ManagerApiJerseyTest;
import jakarta.ws.rs.core.Response;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.json.BasicJsonTester;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.1
 */
public class PlansLogApiTest extends ManagerApiJerseyTest<PlansLogApi> {

	@Test
	void verifyThat_withDays_planslogIsAvailable() {
		final Response response = target("/planslog").queryParam("offset", 0)
			.queryParam("limit", "5")
			.queryParam("days", "1")
			.request()
			.accept(APPLICATION_JSON)
			.get();
		assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_JSON);

		String responseBody = response.readEntity(String.class);

		BasicJsonTester json = new BasicJsonTester(getClass());
		assertThat(json.from(responseBody)).extractingJsonPathNumberValue("$[0].id").isEqualTo(1);
	}

	@Test
	void verifyThat_withoutDays_planslogIsAvailable() {
		final Response response = target("/planslog").queryParam("offset", 0)
			.queryParam("limit", "5")
			.request()
			.accept(APPLICATION_JSON)
			.get();
		assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_JSON);

		String responseBody = response.readEntity(String.class);

		BasicJsonTester json = new BasicJsonTester(getClass());
		assertThat(json.from(responseBody)).extractingJsonPathNumberValue("$[0].id").isEqualTo(2);
	}

	@Test
	void verifyThat_InvalidOffset_statusCode400() {
		final Response response = target("/planslog").queryParam("offset", -1)
			.queryParam("limit", "5")
			.queryParam("days", "1")
			.request()
			.accept(APPLICATION_JSON)
			.get();
		assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
	}

	@Test
	void verifyThat_InvalidLimit_statusCode400() {
		final Response response = target("/planslog").queryParam("offset", 0)
			.queryParam("limit", "0")
			.queryParam("days", "1")
			.request()
			.accept(APPLICATION_JSON)
			.get();
		assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
	}

	@Test
	void verifyThat_InvalidDays_statusCode400() {
		final Response response = target("/planslog").queryParam("offset", 0)
			.queryParam("limit", "5")
			.queryParam("days", "0")
			.request()
			.accept(APPLICATION_JSON)
			.get();
		assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
	}

	@Test
	void verifyThat_NonIntLimit_statusCode404() {
		final Response response = target("/planslog").queryParam("offset", 0)
			.queryParam("limit", "2147483648")
			.queryParam("days", "1")
			.request()
			.accept(APPLICATION_JSON)
			.get();
		assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
	}

	@Test
	void verifyThat_NonIntOffset_statusCode404() {
		final Response response = target("/planslog").queryParam("offset", "a")
			.queryParam("limit", "2")
			.queryParam("days", "1")
			.request()
			.accept(APPLICATION_JSON)
			.get();
		assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
	}

	@Test
	void verifyThat_NonIntDays_statusCode404() {
		final Response response = target("/planslog").queryParam("offset", "0")
			.queryParam("limit", "2")
			.queryParam("days", "invalid")
			.request()
			.accept(APPLICATION_JSON)
			.get();
		assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
	}

}
