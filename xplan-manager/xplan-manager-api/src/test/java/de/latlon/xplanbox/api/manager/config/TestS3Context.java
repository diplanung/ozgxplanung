/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.config;

import static software.amazon.awssdk.regions.Region.EU_CENTRAL_1;

import java.net.URI;
import java.net.URISyntaxException;

import io.findify.s3mock.S3Mock;
import jakarta.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.test.util.TestSocketUtils;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3Configuration;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;

/**
 * Spring Configuration to enable usage of mock objects for integration tests.
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@Configuration
public class TestS3Context {

	private final int port = TestSocketUtils.findAvailableTcpPort();

	private S3Client s3TestClient;

	private S3Mock s3Mock;

	@Bean
	@Profile("mock")
	public S3Mock s3Mock() {
		this.s3Mock = new S3Mock.Builder().withPort(port).withInMemoryBackend().build();
		s3Mock.start();
		return s3Mock;
	}

	@Bean
	@Primary
	@Profile("mock")
	public S3Client s3Client(S3Mock s3Mock, @Value("${xplanbox.s3.bucketName}") String bucketName)
			throws URISyntaxException {
		URI endpoint = new URI("http://localhost:" + port);
		S3Client client = S3Client.builder()
			.serviceConfiguration(S3Configuration.builder().pathStyleAccessEnabled(true).build())
			.endpointOverride(endpoint)
			.credentialsProvider(
					StaticCredentialsProvider.create(AwsBasicCredentials.create("accessKeyId", "secretKey")))
			.region(EU_CENTRAL_1)
			.build();
		client.createBucket(CreateBucketRequest.builder().bucket(bucketName).build());
		s3TestClient = client;
		return client;
	}

	@PreDestroy
	public void shutdown() {
		s3TestClient.close();
		if (s3Mock != null) {
			s3Mock.stop();
		}
	}

}
