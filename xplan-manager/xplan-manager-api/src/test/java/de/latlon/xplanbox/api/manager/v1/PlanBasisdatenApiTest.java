/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.v1;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import de.latlon.xplanbox.api.manager.ManagerApiJerseyTest;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
class PlanBasisdatenApiTest extends ManagerApiJerseyTest<PlanBasisdatenApi> {

	@Test
	void verifyThat_getBasisdaten_returnsCorrectStatusCodeForValidMediaType() {
		Response response = target("/plan/2/basisdaten").request(APPLICATION_JSON).get();

		assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_JSON);
	}

	@Test
	void verifyThat_replaceBasisdaten_returnsCorrectStatusCodeForValidMediaType()
			throws URISyntaxException, IOException {
		final byte[] data = Files.readAllBytes(Paths.get(getClass().getResource("basisdatenmodel.json").toURI()));

		Response response = target("/plan/2/basisdaten").request().put(Entity.entity(data, APPLICATION_JSON_TYPE));
		assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_JSON);
	}

	@Test
	void verifyThat_replaceBasisdaten_returnsCorrectStatusCodeForInvalidBeschreibung()
			throws URISyntaxException, IOException {
		final byte[] data = Files
			.readAllBytes(Paths.get(getClass().getResource("basisdatenmodel_invalidBeschreibungAndName.json").toURI()));

		Response response = target("/plan/2/basisdaten").request().put(Entity.entity(data, APPLICATION_JSON_TYPE));
		// Test fails in IntelliJ IDEA!
		assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
	}

}
