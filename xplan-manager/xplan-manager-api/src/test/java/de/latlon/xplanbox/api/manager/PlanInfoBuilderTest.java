/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import de.latlon.xplan.core.manager.db.model.Geltungsbereich;
import de.latlon.xplan.manager.web.shared.XPlan;
import de.latlon.xplanbox.api.manager.config.ManagerApiConfiguration;
import de.latlon.xplanbox.api.manager.v1.model.Bereich;
import de.latlon.xplanbox.api.manager.v1.model.Gemeinde;
import de.latlon.xplanbox.api.manager.v1.model.Link;
import de.latlon.xplanbox.api.manager.v1.model.PlanInfo;
import de.latlon.xplanbox.api.manager.v1.model.PlanInfoXplanModelData;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class PlanInfoBuilderTest {

	@Test
	public void verifyBuildV1() throws URISyntaxException {
		XPlan xplan = createPlan();
		List<Bereich> bereiche = Collections.singletonList(new Bereich().name("testbereich").nummer("0"));
		List<Gemeinde> gemeinden = Collections
			.singletonList(new Gemeinde().ags("87597649").gemeindeName("Mustergemeinde"));
		Geltungsbereich geltungsbereich = createGeltungsbereich();
		ManagerApiConfiguration managerApiConfiguration = createManagerApiConfiguration();
		PlanInfo planInfo = new PlanInfoBuilder(xplan, bereiche, gemeinden, geltungsbereich, managerApiConfiguration)
			.build();
		assertThat(planInfo.getId()).isEqualTo(1);
		assertThat(planInfo.getType()).isEqualTo("BP_Plan");

		PlanInfoXplanModelData xplanModelData = planInfo.getXplanModelData();
		assertThat(xplanModelData.getBereich().get(0).getName()).isEqualTo("testbereich");
		assertThat(xplanModelData.getBereich().get(0).getNummer()).isEqualTo("0");
	}

	@Test
	public void verifyBuildV2() throws URISyntaxException {
		XPlan xplan = createPlan();
		List<Bereich> bereiche = Collections.singletonList(new Bereich().name("testbereich").nummer("0"));
		List<Gemeinde> gemeinden = Collections
			.singletonList(new Gemeinde().ags("87597649").gemeindeName("Mustergemeinde"));
		Geltungsbereich geltungsbereich = createGeltungsbereich();
		ManagerApiConfiguration managerApiConfiguration = createManagerApiConfiguration();
		de.latlon.xplanbox.api.manager.v2.model.PlanInfo planInfo = new PlanInfoBuilder(xplan, bereiche, gemeinden,
				geltungsbereich, managerApiConfiguration)
			.buildV2();
		assertThat(planInfo.getId()).isEqualTo(1);
		assertThat(planInfo.getType()).isEqualTo("BP_Plan");
		Optional<Link> planwerkWms = planInfo.getLinks()
			.stream()
			.filter(l -> Link.RelEnum.PLANWERKWMS.equals(l.getRel()))
			.findFirst();
		assertThat(planwerkWms.isPresent()).isTrue();
		assertThat(planwerkWms.get().getHref().toString())
			.isEqualTo("http://xplan-wms/xplan-services-wms/services/planwerkwms/planname/plan");

		de.latlon.xplanbox.api.manager.v2.model.PlanInfoXplanModelData xplanModelData = planInfo.getXplanModelData();
		assertThat(xplanModelData.getBereich().get(0).getName()).isEqualTo("testbereich");
		assertThat(xplanModelData.getBereich().get(0).getNummer()).isEqualTo("0");
	}

	@Test
	public void verifyConvertToV1() throws URISyntaxException {
		XPlan xplan = createPlan();
		List<Bereich> bereiche = Collections.singletonList(new Bereich().name("testbereich").nummer("0"));
		List<Gemeinde> gemeinden = Collections
			.singletonList(new Gemeinde().ags("87597649").gemeindeName("Mustergemeinde"));
		Geltungsbereich geltungsbereich = createGeltungsbereich();
		ManagerApiConfiguration managerApiConfiguration = createManagerApiConfiguration();
		de.latlon.xplanbox.api.manager.v2.model.PlanInfo planInfo = new PlanInfoBuilder(xplan, bereiche, gemeinden,
				geltungsbereich, managerApiConfiguration)
			.buildV2();

		List<PlanInfo> planInfos = PlanInfoBuilder.convertToV1(Collections.singletonList(planInfo));
		assertThat(planInfos.size()).isEqualTo(1);

		PlanInfo convertedPlanInfo = planInfos.get(0);
		assertThat(convertedPlanInfo.getId()).isEqualTo(1);
		assertThat(convertedPlanInfo.getType()).isEqualTo("BP_Plan");

		PlanInfoXplanModelData xplanModelData = convertedPlanInfo.getXplanModelData();
		assertThat(xplanModelData.getBereich().get(0).getName()).isEqualTo("testbereich");
		assertThat(xplanModelData.getBereich().get(0).getNummer()).isEqualTo("0");
	}

	private Geltungsbereich createGeltungsbereich() {
		return null;
	}

	private static XPlan createPlan() {
		XPlan xplan = new XPlan("plan", "1", "BP_Plan");
		xplan.setImportDate(new Date());
		xplan.setVersion("XPLAN_60");
		return xplan;
	}

	private static ManagerApiConfiguration createManagerApiConfiguration() throws URISyntaxException {
		ManagerApiConfiguration managerApiConfiguration = mock(ManagerApiConfiguration.class);
		when(managerApiConfiguration.getApiUrl()).thenReturn(new URI("http://xplan-manager-api/xplan-manager-api"));
		when(managerApiConfiguration.getXPlanWmsUrl()).thenReturn(new URI("http://xplan-wms/xplan-services-wms"));
		return managerApiConfiguration;
	}

}
