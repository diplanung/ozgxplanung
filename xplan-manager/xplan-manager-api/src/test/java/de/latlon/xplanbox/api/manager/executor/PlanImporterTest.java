/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.executor;

import static de.latlon.core.validator.events.planimport.ImportRequestedEvent.EXTERNAL_REFERENCES_RESULT.AVAILABLE_AND_VALID;
import static de.latlon.core.validator.events.planimport.ImportRequestedEvent.VALIDATION_RESULT.VALID;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.IMPORT_FINISHED;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.IMPORT_STARTED;
import static de.latlon.xplanbox.validator.storage.StatusType.IMPORT_REQUESTED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

import de.latlon.core.validator.events.EventSender;
import de.latlon.core.validator.events.ValidationRequestedEvent;
import de.latlon.core.validator.events.planimport.ImportRequestedEvent;
import de.latlon.core.validator.events.v1.XPlanPublicV1Event;
import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplanbox.api.manager.config.ApplicationContext;
import de.latlon.xplanbox.api.manager.config.HsqlJpaContext;
import de.latlon.xplanbox.api.manager.config.ManagerApiConfiguration;
import de.latlon.xplanbox.api.manager.config.TestContext;
import de.latlon.xplanbox.api.manager.handler.PlanHandler;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.bean.override.mockito.MockitoSpyBean;
import org.xmlunit.assertj.XmlAssert;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@SpringBootTest(classes = { PlanImporter.class })
@ContextConfiguration(classes = { ApplicationContext.class, HsqlJpaContext.class, TestContext.class })
@TestPropertySource(value = "/s3Mock.properties", properties = "spring.main.allow-bean-definition-overriding=true")
@ActiveProfiles({ "mock", "test" })
public class PlanImporterTest {

	@Autowired
	private PlanImporter planImporter;

	@MockitoSpyBean
	private ValidationExecutionStorage validationExecutionStorage;

	@MockitoBean
	private PlanHandler planHandler;

	@MockitoBean
	private ManagerApiConfiguration managerApiConfiguration;

	@MockitoBean
	EventSender eventSender;

	@Captor
	ArgumentCaptor<XPlanPublicV1Event> publicEventCaptor;

	@Captor
	ArgumentCaptor<String> stringCaptor;

	@Captor
	ArgumentCaptor<String> internalIdCaptor;

	@Captor
	ArgumentCaptor<String> planStatusCaptor;

	@Captor
	ArgumentCaptor<Path> planInfoPathCaptor;

	@Test
	void verifyImportPlan() throws Exception {
		when(planHandler.importPlanWithoutValidation(any(XPlanArchive.class), anyString(), anyString()))
			.thenReturn(Collections.singletonList(1));
		when(managerApiConfiguration.getApiUrl()).thenReturn(new URI("http/localhost/manager/api/v2/"));

		Path pathToPlan = Paths.get(getClass().getResource("/xplan.gml").toURI());
		String uuid = validationExecutionStorage.addPlanToValidate(pathToPlan, IMPORT_REQUESTED);

		ImportRequestedEvent event = new ImportRequestedEvent(uuid, "validationName", "internalId", "FESTGESTELLT",
				ValidationRequestedEvent.OriginFile.GML, VALID, AVAILABLE_AND_VALID, true);
		planImporter.importPlan(event);

		verify(planHandler).importPlanWithoutValidation(any(XPlanArchive.class), internalIdCaptor.capture(),
				planStatusCaptor.capture());
		XmlAssert.assertThat(internalIdCaptor.getValue()).isEqualTo(event.getInternalId());
		XmlAssert.assertThat(planStatusCaptor.getValue()).isEqualTo(event.getPlanStatus());

		verify(validationExecutionStorage).cleanupAfterValidation(uuid);

		XPlanPublicV1Event expectedStartEvent = new XPlanPublicV1Event(IMPORT_STARTED, uuid);
		XPlanPublicV1Event expectedFinishedEvent = new XPlanPublicV1Event(IMPORT_FINISHED, uuid, true);
		verify(eventSender, times(2)).sendPublicEvent(publicEventCaptor.capture(), stringCaptor.capture());
		assertThat(publicEventCaptor.getAllValues()).containsExactly(expectedStartEvent, expectedFinishedEvent);
		assertThat(stringCaptor.getAllValues()).containsExactly(IMPORT_STARTED.routingKeySuffix(),
				IMPORT_FINISHED.routingKeySuffix());
	}

}
