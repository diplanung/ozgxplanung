/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.config;

import static de.latlon.xplanbox.validator.storage.StatusType.IMPORT_FINISHED;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import de.latlon.core.validator.events.ValidationRequestedEvent;
import de.latlon.xplanbox.api.manager.handler.AbstractAsyncImportWrapper;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class FakeAsyncImportWrapper extends AbstractAsyncImportWrapper {

	@Autowired
	private ValidationExecutionStorage validationExecutionStorage;

	protected FakeAsyncImportWrapper() {
		super(null);
	}

	@Override
	public boolean sendEvent(ValidationRequestedEvent event) {
		try {
			Map<ValidationExecutionStorage.ReportType, Path> reports = new HashMap<>();
			Path reportForNextValidateRequest = Paths.get(getClass().getResource("validation-report.json").toURI());
			reports.put(ValidationExecutionStorage.ReportType.JSON, reportForNextValidateRequest);
			validationExecutionStorage.saveValidationResult(event.getUuid(), reports);
			validationExecutionStorage.changeStatus(event.getUuid(), IMPORT_FINISHED, Collections.singletonList(1));
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		return true;
	}

}
