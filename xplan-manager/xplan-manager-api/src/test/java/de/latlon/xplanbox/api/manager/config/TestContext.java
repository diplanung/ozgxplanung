/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.config;

import static de.latlon.xplan.commons.XPlanType.BP_Plan;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_41;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_51;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_60;
import static de.latlon.xplan.core.manager.db.model.OperationType.INSERT;
import static de.latlon.xplan.manager.web.shared.PlanStatus.FESTGESTELLT;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.slf4j.LoggerFactory.getLogger;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import de.latlon.core.validator.events.EventSender;
import de.latlon.xplan.commons.XPlanSchemas;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.commons.configuration.SortConfiguration;
import de.latlon.xplan.commons.feature.SortPropertyReader;
import de.latlon.xplan.core.manager.db.model.Geltungsbereich;
import de.latlon.xplan.core.manager.db.model.PlansLogEntry;
import de.latlon.xplan.manager.XPlanManager;
import de.latlon.xplan.manager.configuration.ManagerConfiguration;
import de.latlon.xplan.manager.database.ManagerWorkspaceWrapper;
import de.latlon.xplan.manager.database.PlanNotFoundException;
import de.latlon.xplan.manager.database.XPlanDao;
import de.latlon.xplan.manager.synthesizer.XPlanSynthesizer;
import de.latlon.xplan.manager.transaction.XPlanDeleteManager;
import de.latlon.xplan.manager.transaction.XPlanEditManager;
import de.latlon.xplan.manager.transaction.XPlanInsertManager;
import de.latlon.xplan.manager.transaction.service.XPlanDeleteService;
import de.latlon.xplan.manager.transaction.service.XPlanEditService;
import de.latlon.xplan.manager.web.shared.PlanStatus;
import de.latlon.xplan.manager.web.shared.XPlan;
import de.latlon.xplan.manager.wmsconfig.raster.XPlanRasterManager;
import de.latlon.xplan.manager.wmsconfig.raster.storage.RasterStorage;
import de.latlon.xplan.manager.workspace.WorkspaceException;
import de.latlon.xplan.manager.workspace.WorkspaceReloader;
import de.latlon.xplanbox.core.raster.evaluation.XPlanRasterEvaluator;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.ServletContext;
import org.deegree.cs.coordinatesystems.ICRS;
import org.deegree.cs.exceptions.UnknownCRSException;
import org.deegree.cs.persistence.CRSManager;
import org.deegree.feature.persistence.FeatureStore;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.autoconfigure.quartz.QuartzAutoConfiguration;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;

/**
 * Creates mock objects for XPlanManager and underlying objects for in-memory unit tests.
 * Indented to register the JAX-RS resources within Spring Application Context. Resources
 * not configured automatically. Using Jersey <code>ResourceConfig</code> with profile
 * <code>jaxrs</code> instead.
 *
 * @author <a href="mailto:friebe@lat-lon.de">Torsten Friebe</a>
 */
@Configuration
@Import(TestS3Context.class)
@EnableAutoConfiguration(exclude = { QuartzAutoConfiguration.class, LiquibaseAutoConfiguration.class })
public class TestContext {

	private static final Logger LOG = getLogger(TestContext.class);

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	@Bean
	public FakeAsyncImportWrapper asyncImportWrapper() {
		return new FakeAsyncImportWrapper();
	}

	@Bean
	public EventSender eventSender() {
		return Mockito.mock(EventSender.class);
	}

	@Bean
	public ServletContext getServletContext() {
		ServletContext mockServletContext = Mockito.mock(ServletContext.class);
		Mockito.when(mockServletContext.getContextPath()).thenReturn("");
		return mockServletContext;
	}

	@Bean
	public XPlanManager xPlanManager(XPlanDao xPlanDao, XPlanArchiveCreator archiveCreator,
			ManagerConfiguration managerConfiguration, XPlanRasterEvaluator xPlanRasterEvaluator,
			XPlanInsertManager xPlanInsertManager, XPlanEditManager xPlanEditManager,
			XPlanDeleteManager xPlanDeleteManager) throws Exception {
		return new XPlanManager(xPlanDao, archiveCreator, managerConfiguration, xPlanRasterEvaluator, null,
				xPlanInsertManager, xPlanEditManager, xPlanDeleteManager);
	}

	@Bean
	@Primary
	public XPlanInsertManager xPlanInsertManager() throws Exception {
		XPlanInsertManager xplanInsertManager = mock(XPlanInsertManager.class);
		when(xplanInsertManager.importPlan(any(), anyBoolean(), anyBoolean(), nullable(String.class), any()))
			.thenReturn(Collections.singletonList(123));
		return xplanInsertManager;
	}

	@Primary
	@Bean
	public XPlanEditManager xPlanEditManager(XPlanSynthesizer xPlanSynthesizer, XPlanDao xPlanDao,
			ManagerConfiguration managerConfiguration, WorkspaceReloader workspaceReloader,
			XPlanRasterManager xPlanRasterManager, SortPropertyReader sortPropertyReader,
			XPlanEditService xPlanEditService, ICRS defaultCrs) {
		return new XPlanEditManager(xPlanSynthesizer, xPlanDao, xPlanRasterManager, workspaceReloader,
				managerConfiguration, sortPropertyReader, xPlanEditService, null, null, defaultCrs);
	}

	@Bean
	public ICRS defaultCrs(@Value("${xplanbox.services.crs}") String defaultCrs) throws UnknownCRSException {
		return CRSManager.lookup(defaultCrs);
	}

	@Primary
	@Bean
	public XPlanEditService xPlanEditService() {
		return mock(XPlanEditService.class);
	}

	@Bean
	public XPlanDeleteManager xPlanDeleteManager(WorkspaceReloader workspaceReloader,
			XPlanDeleteService xPlanDeleteService) {
		return new XPlanDeleteManager(workspaceReloader, xPlanDeleteService);
	}

	@Primary
	@Bean
	public XPlanDeleteService xPlanDeleteService() {
		return mock(XPlanDeleteService.class);
	}

	@Bean
	@Primary
	public ManagerApiConfiguration managerApiConfiguration() throws URISyntaxException {
		ManagerApiConfiguration managerApiConfiguration = mock(ManagerApiConfiguration.class);
		when(managerApiConfiguration.getApiUrl()).thenReturn(new URI("http://localhost:8080/xplan-manager-api/"));
		when(managerApiConfiguration.getDefaultValidationConfiguration())
			.thenReturn(new DefaultValidationConfiguration());
		return managerApiConfiguration;
	}

	@Bean
	@Primary
	public ManagerWorkspaceWrapper managerWorkspaceWrapper(ManagerConfiguration managerConfiguration)
			throws SQLException {
		ManagerWorkspaceWrapper managerWorkspaceWrapper = mock(ManagerWorkspaceWrapper.class);
		FeatureStore featureStore41 = mock(FeatureStore.class);
		when(featureStore41.getSchema()).thenReturn(XPlanSchemas.getInstance().getAppSchema(XPLAN_41));
		FeatureStore featureStore51 = mock(FeatureStore.class);
		when(featureStore51.getSchema()).thenReturn(XPlanSchemas.getInstance().getAppSchema(XPLAN_51));
		when(managerWorkspaceWrapper.lookupStore(eq(XPLAN_41), any(PlanStatus.class))).thenReturn(featureStore41);
		when(managerWorkspaceWrapper.lookupStore(eq(XPLAN_51), any(PlanStatus.class))).thenReturn(featureStore51);
		Connection connection = mockConnection();
		when(managerWorkspaceWrapper.openConnection()).thenReturn(connection);
		return managerWorkspaceWrapper;
	}

	private static Connection mockConnection() throws SQLException {
		Connection connection = mock(Connection.class);
		DatabaseMetaData connectionMetadata = mock(DatabaseMetaData.class);
		Statement statement = mock(Statement.class);
		when(connection.createStatement()).thenReturn(statement);
		when(statement.executeQuery(anyString())).thenReturn(mock(ResultSet.class));
		when(connection.getMetaData()).thenReturn(connectionMetadata);
		when(connectionMetadata.getURL()).thenReturn("jdbc:h2:mem:testdb");
		when(connectionMetadata.getDatabaseProductName()).thenReturn("H2");
		when(connectionMetadata.getSQLKeywords()).thenReturn("CREATE DROP");
		return connection;
	}

	@Bean
	@Primary
	public XPlanRasterManager xPlanRasterManager(RasterStorage rasterStorage) throws WorkspaceException {
		return new XPlanRasterManager(rasterStorage, applicationEventPublisher);
	}

	@Bean
	@Primary
	public XPlanDao xPlanDao(ManagerWorkspaceWrapper managerWorkspaceWrapper, ManagerConfiguration managerConfiguration)
			throws Exception {
		XPlanDao xplanDao = mock(XPlanDao.class);
		XPlan mockPlan_1 = new XPlan("bplan_41", "1", "BP_Plan", "XPLAN_41");
		XPlan mockPlan_123 = new XPlan("bplan_41", "123", "BP_Plan", "XPLAN_41");
		XPlan mockPlan_2 = new XPlan("bplan_51", "2", "BP_Plan", "XPLAN_51");
		XPlan mockPlan_3 = new XPlan("bplan_53", "3", "BP_Plan", "XPLAN_53");
		XPlan mockPlan_4 = new XPlan("fplan_51", "4", "FP_Plan", "XPLAN_51");
		XPlan mockPlan_5 = new XPlan("lplan_51", "5", "LP_Plan", "XPLAN_51");
		XPlan mockPlan_6 = new XPlan("soplan_40", "6", "SO_Plan", "XPLAN_40");
		XPlan mockPlan_7 = new XPlan("bplan_51", "7", "BP_Plan", "XPLAN_51");
		mockPlan_2.setInternalId("ID_2_II");
		mockPlan_123.setPlanStatus(FESTGESTELLT);
		mockPlan_2.setPlanStatus(FESTGESTELLT);
		mockPlan_3.setPlanStatus(FESTGESTELLT);
		when(xplanDao.getXPlanById(1)).thenReturn(mockPlan_1);
		when(xplanDao.getXPlanById(123)).thenReturn(mockPlan_123);
		when(xplanDao.getXPlanById(2)).thenReturn(mockPlan_2);
		when(xplanDao.getXPlanById(3)).thenReturn(mockPlan_3);
		when(xplanDao.getXPlanById(4)).thenReturn(mockPlan_4);
		when(xplanDao.getXPlanById(5)).thenReturn(mockPlan_5);
		when(xplanDao.getXPlanById(6)).thenReturn(mockPlan_6);
		when(xplanDao.getXPlanById(7)).thenReturn(mockPlan_7);
		when(xplanDao.retrieveXPlanArtefact("2")).thenReturn(getClass().getResourceAsStream("/xplan51.gml"))
			.thenReturn(getClass().getResourceAsStream("/xplan51.gml"))
			.thenReturn(getClass().getResourceAsStream("/xplan51-edited.gml"));
		when(xplanDao.retrieveXPlanArtefact("7")).thenReturn(getClass().getResourceAsStream("/xplan51_ohneBereich.gml"))
			.thenReturn(getClass().getResourceAsStream("/xplan51_ohneBereich.gml"));
		when(xplanDao.retrieveXPlanArtefact(2)).thenReturn(getClass().getResourceAsStream("/xplan51.gml"))
			.thenReturn(getClass().getResourceAsStream("/xplan51.gml"))
			.thenReturn(getClass().getResourceAsStream("/xplan51-edited.gml"));
		when(xplanDao.retrieveXPlanArtefact(7)).thenReturn(getClass().getResourceAsStream("/xplan51_ohneBereich.gml"))
			.thenReturn(getClass().getResourceAsStream("/xplan51_ohneBereich.gml"));
		when(xplanDao.existsPlan(123)).thenReturn(true);
		List<XPlan> mockList = Collections.singletonList(mockPlan_123);
		List<XPlan> allPlans = List.of(mockPlan_1, mockPlan_123, mockPlan_2, mockPlan_3, mockPlan_4, mockPlan_5,
				mockPlan_6, mockPlan_7);
		when(xplanDao.getXPlanByName("bplan_41")).thenReturn(mockList);
		when(xplanDao.getXPlansBySearchParams("bplan_41", null, Collections.emptyList())).thenReturn(mockList);
		when(xplanDao.getXPlansBySearchParams(null, "ID_2_II", Collections.emptyList()))
			.thenReturn(Collections.singletonList(mockPlan_2));
		when(xplanDao.getXPlansBySearchParams(null, null, Collections.singletonList(FESTGESTELLT)))
			.thenReturn(Collections.singletonList(mockPlan_3));
		when(xplanDao.getXPlansBySearchParams(null, null, Collections.emptyList())).thenReturn(allPlans);
		when(xplanDao.getXPlanList()).thenReturn(allPlans);
		when(xplanDao.retrieveAllXPlanArtefacts(anyString())).thenReturn(mock(List.class));
		when(xplanDao.retrieveAllXPlanArtefacts("42")).thenThrow(new PlanNotFoundException(42));
		when(xplanDao.retrieveGeltungsbereichOfPlanWithId("123")).thenReturn(geltungsbereichWGS84());

		when(xplanDao.retrievePlansLog(0, 5, 1)).thenReturn(Collections.singletonList(new PlansLogEntry().id(1)
			.planId(1)
			.version(XPLAN_60)
			.type(BP_Plan)
			.bbox(new WKTReader().read("POLYGON((0 0,1 0,1 1,0 1,0 0))"))
			.planstatusNew(FESTGESTELLT)
			.operation(INSERT)
			.lastUpdateDate(LocalDateTime.now())));

		when(xplanDao.retrievePlansLog(0, 5, -1)).thenReturn(Collections.singletonList(new PlansLogEntry().id(2)
			.planId(2)
			.version(XPLAN_60)
			.type(BP_Plan)
			.bbox(new WKTReader().read("POLYGON((0 0,1 0,1 1,0 1,0 0))"))
			.planstatusNew(FESTGESTELLT)
			.operation(INSERT)
			.lastUpdateDate(LocalDateTime.now())));
		return xplanDao;
	}

	@Bean
	@Primary
	public ManagerConfiguration managerConfiguration() {
		ManagerConfiguration mockedConfiguration = mock(ManagerConfiguration.class);
		when(mockedConfiguration.getSortConfiguration()).thenReturn(new SortConfiguration());
		return mockedConfiguration;
	}

	@Bean
	@Primary
	public WorkspaceReloader workspaceReloader() {
		WorkspaceReloader workspaceReloader = mock(WorkspaceReloader.class);
		when(workspaceReloader.reloadWorkspace(1)).thenReturn(true);
		return workspaceReloader;
	}

	@PostConstruct
	void initLoggingAdapter() {
		SLF4JBridgeHandler.removeHandlersForRootLogger();
		SLF4JBridgeHandler.install();
		LOG.trace("JUL logging enabled");
	}

	private Geltungsbereich geltungsbereichWGS84() throws ParseException {
		Geometry jtsGeom = new WKTReader().read("POLYGON((0 0,1 0,1 1,0 1,0 0))");
		return new Geltungsbereich().id(123).geltungsbereich(jtsGeom);
	}

}
