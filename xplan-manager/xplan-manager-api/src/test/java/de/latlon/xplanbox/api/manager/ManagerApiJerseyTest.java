/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager;

import java.lang.reflect.ParameterizedType;

import de.latlon.xplanbox.api.commons.exception.XPlanApiExceptionMapper;
import de.latlon.xplanbox.api.manager.config.ApplicationContext;
import de.latlon.xplanbox.api.manager.config.HsqlJpaContext;
import de.latlon.xplanbox.api.manager.config.TestContext;
import de.latlon.xplanbox.api.manager.exception.ValidationExceptionMapper;
import jakarta.ws.rs.core.Application;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public abstract class ManagerApiJerseyTest<T> extends JerseyTest {

	@Override
	protected Application configure() {
		enable(TestProperties.LOG_TRAFFIC);
		Class<T> actualTypeArgument = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
			.getActualTypeArguments()[0];
		final ResourceConfig resourceConfig = new ResourceConfig(actualTypeArgument);
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		PropertySourcesPlaceholderConfigurer pph = new PropertySourcesPlaceholderConfigurer();
		pph.setLocations(new ClassPathResource("/application.properties"), new ClassPathResource("/s3Mock.properties"));
		context.getEnvironment().setActiveProfiles("mock", "test");
		context.addBeanFactoryPostProcessor(pph);
		context.register(ApplicationContext.class, HsqlJpaContext.class, TestContext.class);
		context.refresh();
		resourceConfig.register(XPlanApiExceptionMapper.class);
		resourceConfig.register(MultiPartFeature.class);
		resourceConfig.register(ValidationExceptionMapper.class);
		resourceConfig.packages("org.glassfish.jersey.examples.multipart");
		resourceConfig.property("contextConfig", context);
		return resourceConfig;
	}

}
