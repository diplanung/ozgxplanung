/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.v2;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.net.URISyntaxException;

import de.latlon.xplanbox.api.manager.ManagerApiJerseyTest;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.apache.http.HttpHeaders;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
class PlanTextApi2Test extends ManagerApiJerseyTest<PlanTextApi2> {

	@Override
	protected void configureClient(ClientConfig config) {
		config.register(MultiPartFeature.class);
	}

	@Test
	void verifyThat_getTexte_returnsCorrectStatusCodeForValidMediaType() {
		Response response = target("/plan/2/text").request(APPLICATION_JSON).get();

		assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_JSON);
	}

	@Test
	void verifyThat_addText_returnsCorrectStatusCodeForValidMediaType() throws URISyntaxException {
		FileDataBodyPart textmodel = createFileDataBodyPart("textmodel", "textmodel.json",
				MediaType.APPLICATION_JSON_TYPE);
		FileDataBodyPart filePart = createFileDataBodyPart("datei", "datei.pdf", null);
		FormDataMultiPart multipart = (FormDataMultiPart) new FormDataMultiPart().bodyPart(filePart)
			.bodyPart(textmodel);

		Response response = target("/plan/2/text").request().post(Entity.entity(multipart, multipart.getMediaType()));
		assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_JSON);

	}

	@Test
	void verifyThat_getTextById_returnsCorrectStatusCodeForValidMediaType() {
		Response response = target("/plan/2/text/FEATURE_0f870967-bd6f-4367-9150-8a255f0290ad")
			.request(APPLICATION_JSON)
			.get();

		assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_JSON);
	}

	@Test
	void verifyThat_replaceTextById_returnsCorrectStatusCodeForValidMediaType() throws URISyntaxException {
		FileDataBodyPart textmodel = createFileDataBodyPart("textmodel", "textmodel.json",
				MediaType.APPLICATION_JSON_TYPE);
		FileDataBodyPart filePart = createFileDataBodyPart("datei", "datei.pdf", null);
		FormDataMultiPart multipart = (FormDataMultiPart) new FormDataMultiPart().bodyPart(filePart)
			.bodyPart(textmodel);

		Response response = target("/plan/2/text/FEATURE_0f870967-bd6f-4367-9150-8a255f0290ad").request()
			.put(Entity.entity(multipart, multipart.getMediaType()));
		assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_JSON);
	}

	@Test
	void verifyThat_replaceTextById_invalidDateTime_returnsCorrectStatusCodeForValidMediaType()
			throws URISyntaxException {
		FileDataBodyPart textmodel = createFileDataBodyPart("textmodel", "textmodel-datetime.json",
				MediaType.APPLICATION_JSON_TYPE);
		FileDataBodyPart filePart = createFileDataBodyPart("datei", "datei.pdf", null);
		FormDataMultiPart multipart = (FormDataMultiPart) new FormDataMultiPart().bodyPart(filePart)
			.bodyPart(textmodel);

		Response response = target("/plan/2/text/FEATURE_0f870967-bd6f-4367-9150-8a255f0290ad").request()
			.put(Entity.entity(multipart, multipart.getMediaType()));
		assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
	}

	private FileDataBodyPart createFileDataBodyPart(String name, String resource, MediaType mediaType)
			throws URISyntaxException {
		File datei = new File(getClass().getResource("../v2/" + resource).toURI());
		return new FileDataBodyPart(name, datei, mediaType);
	}

}
