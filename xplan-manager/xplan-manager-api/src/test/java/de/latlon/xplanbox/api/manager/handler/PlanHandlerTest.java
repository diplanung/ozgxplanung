/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.handler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Collections;
import java.util.List;

import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.manager.web.shared.XPlan;
import de.latlon.xplanbox.api.commons.exception.InvalidPlanId;
import de.latlon.xplanbox.api.commons.exception.InvalidPlanIdSyntax;
import de.latlon.xplanbox.api.manager.config.ApplicationContext;
import de.latlon.xplanbox.api.manager.config.HsqlJpaContext;
import de.latlon.xplanbox.api.manager.config.TestContext;
import de.latlon.xplanbox.api.manager.v1.model.PlanStatusEnum;
import de.latlon.xplanbox.api.manager.v1.model.StatusMessage;
import jakarta.ws.rs.core.StreamingOutput;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

/**
 * @author <a href="mailto:friebe@lat-lon.de">Torsten Friebe</a>
 */
@SpringBootTest
@ContextConfiguration(classes = { ApplicationContext.class, HsqlJpaContext.class, TestContext.class })
@TestPropertySource(value = "/s3Mock.properties", properties = "spring.main.allow-bean-definition-overriding=true")
@ActiveProfiles({ "test", "mock" })
class PlanHandlerTest {

	@Autowired
	private PlanHandler planHandler;

	@Autowired
	private XPlanArchiveCreator archiveCreator;

	@Test
	void verifyThat_deletePlan_IsNotThrowingException() throws Exception {
		StatusMessage statusMessage = planHandler.deletePlan("123");
		assertThat(statusMessage).isNotNull();
		assertThat(statusMessage.getMessage()).contains("123");
	}

	@Test
	void verifyThat_exportPlan() throws Exception {
		StreamingOutput planAsStream = planHandler.exportPlan("123");
		assertThat(planAsStream).isNotNull();
	}

	@Test
	void verifyThat_exportPlan_WithWrongIdThrowsException() {
		assertThrows(InvalidPlanId.class, () -> {
			StreamingOutput planAsStream = planHandler.exportPlan("42");
			assertThat(planAsStream).isNotNull();
		});
	}

	@Test
	void verifyThat_findPlanById() throws Exception {
		XPlan plan = planHandler.findPlanById("123");
		assertThat(plan.getId()).isEqualTo("123");
	}

	@Test
	void verifyThat_findPlanById_WithWrongIdFails() {
		assertThrows(InvalidPlanId.class, () -> {
			planHandler.findPlanById("42");
		});
	}

	@Test
	void verifyThat_findPlanByName() {
		List<XPlan> plans = planHandler.findPlansByName("bplan_41");
		assertThat(plans).hasSize(1);
		assertThat(plans.get(0).getId()).isEqualTo("123");
	}

	@Test
	void verifyThat_findPlansByName() {
		List<XPlan> planList = planHandler.findPlans("bplan_41", null, Collections.emptyList());
		assertThat(planList).contains(new XPlan("bplan_41", "123", "BP_Plan", "XPLAN_41"));
	}

	@Test
	void verifyThat_findPlansByInternalId() {
		List<XPlan> planList = planHandler.findPlans(null, "ID_2_II", Collections.emptyList());
		assertThat(planList).contains(new XPlan("bplan_51", "2", "BP_Plan", "XPLAN_51"));
	}

	@Test
	void verifyThat_findPlansByPlanstatus() {
		List<XPlan> planList = planHandler.findPlans(null, null,
				Collections.singletonList(PlanStatusEnum.FESTGESTELLT));
		assertThat(planList).contains(new XPlan("bplan_53", "3", "BP_Plan", "XPLAN_53"));
	}

	@Test
	void verifyThat_findPlans_ReturnsEmptyList() {
		List<XPlan> planList = planHandler.findPlans("xplan", null, Collections.emptyList());
		assertThat(planList).hasSize(0);
	}

	@Test
	void verifyThat_findPlansWithNullName() {
		List<XPlan> planList = planHandler.findPlans(null, null, Collections.emptyList());
		assertThat(planList).isNotEmpty();
	}

	@Test
	void verifyThat_deleteWithNonIntegerFails() {
		assertThrows(InvalidPlanIdSyntax.class, () -> planHandler.deletePlan("999999999999999999999"));
	}

}
