/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager;

import static de.latlon.xplanbox.api.commons.DateConverter.convertToDate;
import static de.latlon.xplanbox.api.commons.DateConverter.convertToLocalDate;
import static de.latlon.xplanbox.api.commons.DateConverter.convertToZonedDateTime;
import static de.latlon.xplanbox.api.manager.v1.model.Link.RelEnum.ALTERNATE;
import static de.latlon.xplanbox.api.manager.v1.model.Link.RelEnum.PLANWERKWMS;
import static de.latlon.xplanbox.api.manager.v1.model.Link.RelEnum.SELF;
import static org.slf4j.LoggerFactory.getLogger;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import de.latlon.xplan.core.manager.db.model.Geltungsbereich;
import de.latlon.xplan.manager.web.shared.PlanStatus;
import de.latlon.xplan.manager.web.shared.XPlan;
import de.latlon.xplan.validator.report.geojson.jts.JtsToGeoJsonGeometryBuilder;
import de.latlon.xplan.validator.report.geojson.model.Geometry;
import de.latlon.xplan.validator.web.shared.XPlanEnvelope;
import de.latlon.xplanbox.api.commons.v1.model.PlanInfoBbox;
import de.latlon.xplanbox.api.commons.v1.model.VersionEnum;
import de.latlon.xplanbox.api.manager.config.ManagerApiConfiguration;
import de.latlon.xplanbox.api.manager.openapi.v1.ApiV1Config;
import de.latlon.xplanbox.api.manager.openapi.v2.ApiV2Config;
import de.latlon.xplanbox.api.manager.v1.model.Bereich;
import de.latlon.xplanbox.api.manager.v1.model.Gemeinde;
import de.latlon.xplanbox.api.manager.v1.model.Link;
import de.latlon.xplanbox.api.manager.v1.model.PlanInfo;
import de.latlon.xplanbox.api.manager.v1.model.PlanInfoXplanModelData;
import de.latlon.xplanbox.api.manager.v1.model.PlanStatusEnum;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class PlanInfoBuilder {

	private static final Logger LOG = getLogger(PlanInfoBuilder.class);

	private final XPlan xPlan;

	private final List<Bereich> bereiche;

	private final List<Gemeinde> gemeinden;

	private final Geltungsbereich geltungsbereich;

	private final ManagerApiConfiguration managerApiConfiguration;

	private final List<String> alternateMediaTypes = new ArrayList<>();

	private String selfMediaType;

	public PlanInfoBuilder(XPlan xPlan, List<Bereich> bereiche, List<Gemeinde> gemeinden,
			Geltungsbereich geltungsbereich, ManagerApiConfiguration managerApiConfiguration) {
		this.xPlan = xPlan;
		this.bereiche = bereiche;
		this.gemeinden = gemeinden;
		this.geltungsbereich = geltungsbereich;
		this.managerApiConfiguration = managerApiConfiguration;
	}

	public static List<PlanInfo> convertToV1(List<de.latlon.xplanbox.api.manager.v2.model.PlanInfo> planInfos) {
		return planInfos.stream().map(planInfo -> {
			Date importDate = convertToDate(planInfo.getImportDate());
			return new PlanInfo().id(planInfo.getId())
				.importDate(importDate)
				.inspirePublished(planInfo.getInspirePublished())
				.raster(planInfo.getRaster())
				.version(planInfo.getVersion())
				.planStatus(planInfo.getplanStatus())
				.bbox(planInfo.getBbox())
				.links(planInfo.getLinks())
				.type(planInfo.getType())
				.xplanModelData(convertToV1(planInfo.getXplanModelData()));
		}).toList();
	}

	private static PlanInfoXplanModelData convertToV1(
			de.latlon.xplanbox.api.manager.v2.model.PlanInfoXplanModelData planInfoXplanModelData) {
		Date inkrafttretensDatum = convertToDate(planInfoXplanModelData.getInkrafttretensDatum());
		List<Gemeinde> gemeinden = planInfoXplanModelData.getGemeinden();
		return new PlanInfoXplanModelData().name(planInfoXplanModelData.getName())
			.nummer(planInfoXplanModelData.getNummer())
			.internalId(planInfoXplanModelData.getInternalId())
			.inkrafttretensDatum(inkrafttretensDatum)
			.rechtsstand(planInfoXplanModelData.getRechtsstand())
			.ags(!gemeinden.isEmpty() ? gemeinden.get(0).getAgs() : null)
			.gemeindeName(!gemeinden.isEmpty() ? gemeinden.get(0).getGemeindeName() : null)
			.gemeinden(gemeinden)
			.bereiche(planInfoXplanModelData.getBereich());
	}

	public PlanInfoBuilder selfMediaType(String selfMediaType) {
		this.selfMediaType = selfMediaType;
		return this;
	}

	public PlanInfoBuilder alternateMediaType(List<String> alternateMediaTypes) {
		if (alternateMediaTypes != null)
			this.alternateMediaTypes.addAll(alternateMediaTypes);
		return this;
	}

	public PlanInfo build() {
		return new PlanInfo().id(Integer.parseInt(xPlan.getId()))
			.importDate(xPlan.getImportDate())
			.inspirePublished(xPlan.isInspirePublished())
			.raster(xPlan.isRaster())
			.version(version())
			.planStatus(planStatus())
			.bbox(bbox())
			.geltungsbereichWGS84(geltungsbereichWgs84())
			.links(links(ApiV1Config.APP_PATH))
			.type(xPlan.getType())
			.xplanModelData(xPlanModelData());
	}

	public de.latlon.xplanbox.api.manager.v2.model.PlanInfo buildV2() {
		ZonedDateTime importDate = convertToZonedDateTime(xPlan.getImportDate());
		return new de.latlon.xplanbox.api.manager.v2.model.PlanInfo().id(Integer.parseInt(xPlan.getId()))
			.importDate(importDate)
			.inspirePublished(xPlan.isInspirePublished())
			.raster(xPlan.isRaster())
			.version(version())
			.planStatus(planStatus())
			.bbox(bbox())
			.geltungsbereichWGS84(geltungsbereichWgs84())
			.links(links(ApiV2Config.APP_PATH))
			.type(xPlan.getType())
			.xplanModelData(xPlanModelDataV2());
	}

	private PlanInfoXplanModelData xPlanModelData() {
		return new PlanInfoXplanModelData().name(xPlan.getName())
			.nummer(xPlan.getNumber())
			.internalId(xPlan.getInternalId())
			.inkrafttretensDatum(xPlan.getReleaseDate())
			.rechtsstand(xPlan.getLegislationStatus())
			.bereiche(bereiche)
			.ags(!gemeinden.isEmpty() ? gemeinden.get(0).getAgs() : null)
			.gemeindeName(!gemeinden.isEmpty() ? gemeinden.get(0).getGemeindeName() : null)
			.gemeinden(gemeinden);
	}

	private de.latlon.xplanbox.api.manager.v2.model.PlanInfoXplanModelData xPlanModelDataV2() {
		LocalDate inkrafttrensDatum = convertToLocalDate(xPlan.getReleaseDate());
		return new de.latlon.xplanbox.api.manager.v2.model.PlanInfoXplanModelData().name(xPlan.getName())
			.nummer(xPlan.getNumber())
			.internalId(xPlan.getInternalId())
			.inkrafttretensDatum(inkrafttrensDatum)
			.rechtsstand(xPlan.getLegislationStatus())
			.bereiche(bereiche)
			.gemeinden(gemeinden);
	}

	private PlanStatusEnum planStatus() {
		if (xPlan.getPlanStatus() != null) {
			PlanStatus planStatus = xPlan.getPlanStatus();
			return PlanStatusEnum.valueOf(planStatus.name());
		}
		return null;
	}

	private VersionEnum version() {
		return VersionEnum.fromValue(xPlan.getVersion());
	}

	private List<Link> links(String appPath) {
		List<Link> links = new ArrayList<>();
		URI selfRef = createSelfRef(appPath);
		if (selfRef != null) {
			Link selfLink = (Link) new Link().rel(SELF).href(selfRef).type(selfMediaType).title(xPlan.getName());
			links.add(selfLink);

			alternateMediaTypes.forEach(mediaType -> {
				Link alternateLink = (Link) new Link().rel(ALTERNATE)
					.href(selfRef)
					.type(mediaType)
					.title(xPlan.getName());
				links.add(alternateLink);
			});
		}

		if (managerApiConfiguration.getXPlanWmsUrl() != null) {
			Link planwerkWmsLink = createWmsEndpointUrl(managerApiConfiguration.getXPlanWmsUrl());
			if (planwerkWmsLink != null)
				links.add(planwerkWmsLink);
		}
		return links;
	}

	private URI createSelfRef(String appPath) {
		URI apiUrl = managerApiConfiguration.getApiUrl();
		URIBuilder uriBuilder = new URIBuilder(apiUrl);

		List<String> pathSegments = new ArrayList<>();
		if (apiUrl.getPath() != null && !apiUrl.getPath().isEmpty())
			pathSegments.addAll(Arrays.asList(apiUrl.getPath().split("/")));
		pathSegments.addAll(Arrays.asList(appPath.split("/")));
		pathSegments.add("plan");
		pathSegments.add(xPlan.getId());
		uriBuilder.setPathSegments(pathSegments.stream()
			.filter(pathSegment -> pathSegment != null && !pathSegment.isEmpty())
			.collect(Collectors.toList()));
		try {
			return uriBuilder.build();
		}
		catch (URISyntaxException e) {
			LOG.warn("Could not create self reference: " + e.getMessage(), e);
		}
		return null;
	}

	private Link createWmsEndpointUrl(URI xplanWmsUrl) {
		try {
			URIBuilder uriBuilder = new URIBuilder(xplanWmsUrl);
			List<String> pathSegments = new ArrayList<>();
			pathSegments.addAll(uriBuilder.getPathSegments());
			pathSegments.add("services");
			pathSegments.add(detectService());
			pathSegments.add("planname");
			pathSegments.add(xPlan.getName().replace("/", ""));
			pathSegments.remove("");
			uriBuilder.setPathSegments(pathSegments);
			URI planwerkWmsRef = uriBuilder.build();
			return (Link) new Link().rel(PLANWERKWMS).href(planwerkWmsRef).title(xPlan.getName());
		}
		catch (URISyntaxException e) {
			LOG.warn("Could not build XPlanwerkWMS url: " + e.getMessage(), e);
		}
		return null;
	}

	private PlanInfoBbox bbox() {
		XPlanEnvelope bbox = xPlan.getBbox();
		if (bbox != null)
			return new PlanInfoBbox().crs(bbox.getCrs())
				.minX(bbox.getMinX())
				.minY(bbox.getMinY())
				.maxX(bbox.getMaxX())
				.maxY(bbox.getMaxY());
		return null;
	}

	private Geometry geltungsbereichWgs84() {
		if (geltungsbereich != null) {
			org.locationtech.jts.geom.Geometry geltungsbereichGeom = geltungsbereich.getGeltungsbereichWGS84();
			return JtsToGeoJsonGeometryBuilder.createGeometry(geltungsbereichGeom);
		}
		return null;
	}

	private String detectService() {
		if (xPlan.getPlanStatus() != null)
			switch (xPlan.getPlanStatus()) {
				case ARCHIVIERT:
					return "planwerkwmsarchive";
				case IN_AUFSTELLUNG:
					return "planwerkwmspre";
			}
		return "planwerkwms";
	}

}
