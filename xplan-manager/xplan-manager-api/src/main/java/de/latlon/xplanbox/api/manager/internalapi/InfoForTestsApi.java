/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.internalapi;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST API for internal use.
 * 
 * Caution: don't expose sensitive information here.
 *
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
@Component
@RestController
@RequestMapping("/internal/api")
public class InfoForTestsApi {

	static record TraceInfo(String traceId, String spanId) {
	};

	@GetMapping("/traceInfo")
	public TraceInfo traceInfoFromLog4jThreadContext() {
		String traceId = ThreadContext.get("traceId");
		String spanId = ThreadContext.get("spanId");

		return new TraceInfo(traceId, spanId);
	}

}
