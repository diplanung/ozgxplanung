/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.v2.model;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class ManagerStatusNotification extends de.latlon.xplanbox.api.commons.v2.model.StatusNotification {

	private StatusEnum status;

	@JsonInclude(NON_EMPTY)
	private List<Integer> importedPlanIds;

	public ManagerStatusNotification() {
	}

	public ManagerStatusNotification(StatusEnum status) {
		this.status = status;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public ManagerStatusNotification status(StatusEnum status) {
		this.status = status;
		return this;
	}

	public List<Integer> getImportedPlanIds() {
		return importedPlanIds;
	}

	public void setImportedPlanIds(List<Integer> importedPlanIds) {
		this.importedPlanIds = importedPlanIds;
	}

	public ManagerStatusNotification importedPlanIds(List<Integer> importedPlanIds) {
		this.importedPlanIds = importedPlanIds;
		return this;
	}

}
