/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager;

import java.util.Optional;

import de.latlon.xplanbox.api.manager.config.ManagerApiConfiguration;
import de.latlon.xplanbox.api.manager.openapi.v1.ApiV1Config;
import de.latlon.xplanbox.api.manager.openapi.v2.ApiV2Config;
import de.latlon.xplanbox.security.openapi.BearerSecurityOpenApiWriter;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.core.Context;
import org.glassfish.jersey.servlet.ServletContainer;
import org.glassfish.jersey.servlet.ServletProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.autoconfigure.quartz.QuartzAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(
		basePackages = { "de.latlon.xplanbox.api.manager.config", "de.latlon.xplanbox.api.manager.messagingrabbitmq",
				"de.latlon.xplanbox.api.manager.executor", "de.latlon.xplanbox.api.manager.internalapi" })
@EnableAutoConfiguration(exclude = { QuartzAutoConfiguration.class, LiquibaseAutoConfiguration.class,
		SecurityAutoConfiguration.class, ManagementWebSecurityAutoConfiguration.class })
public class SpringBootApp extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootApp.class, args);
	}

	/**
	 * Runs the application as deployable WAR.
	 * @param application Spring application builder
	 * @return Spring application builder with this application configured
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SpringBootApp.class);
	}

	@Bean
	public ServletRegistrationBean v1config(@Context ServletContext servletContext,
			ManagerApiConfiguration managerApiConfiguration,
			Optional<BearerSecurityOpenApiWriter> bearerSecurityOpenApiWriter) {
		ServletRegistrationBean v1config = new ServletRegistrationBean(
				new ServletContainer(
						new ApiV1Config(servletContext, managerApiConfiguration, bearerSecurityOpenApiWriter)),
				"/xmanager/api/v1/*");
		v1config.addInitParameter(ServletProperties.JAXRS_APPLICATION_CLASS, ApiV1Config.class.getName());
		v1config.setName(ApiV1Config.class.getName());
		v1config.setLoadOnStartup(1);
		return v1config;
	}

	@Bean
	public ServletRegistrationBean v2config(@Context ServletContext servletContext,
			ManagerApiConfiguration managerApiConfiguration,
			Optional<BearerSecurityOpenApiWriter> bearerSecurityOpenApiWriter) {
		ServletRegistrationBean v2config = new ServletRegistrationBean(
				new ServletContainer(
						new ApiV2Config(servletContext, managerApiConfiguration, bearerSecurityOpenApiWriter)),
				"/api/v2/*");
		v2config.addInitParameter(ServletProperties.JAXRS_APPLICATION_CLASS, ApiV2Config.class.getName());
		v2config.setName(ApiV2Config.class.getName());
		v2config.setLoadOnStartup(2);
		return v2config;
	}

}
