/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.messagingrabbitmq;

import static de.latlon.core.validator.events.planimport.ImportFinishedEvent.ImportFinishedEventStatus.IMPORT_FAILED;
import static de.latlon.core.validator.events.planimport.ImportFinishedEvent.ImportFinishedEventStatus.IMPORT_SUCCEEDED;
import static org.slf4j.LoggerFactory.getLogger;

import de.latlon.core.validator.events.EventSender;
import de.latlon.core.validator.events.planimport.ImportFinishedEvent;
import de.latlon.core.validator.events.planimport.ImportRequestedEvent;
import de.latlon.xplanbox.api.manager.executor.PlanImporter;
import org.slf4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
@Component
@RabbitListener(queues = "#{importTaskQueue.name}", concurrency = "1")
public class Receiver {

	private static final Logger LOG = getLogger(Receiver.class);

	@Autowired
	private PlanImporter planImporter;

	@Autowired
	private EventSender eventSender;

	@RabbitHandler
	public void accept(ImportRequestedEvent event) {
		LOG.info("Received event: " + event);

		try {

			planImporter.importPlan(event);

			ImportFinishedEvent finishedEvent = new ImportFinishedEvent(event.getUuid(), IMPORT_SUCCEEDED);
			eventSender.sendPrivateEvent(finishedEvent);
		}
		catch (Exception e) {
			LOG.error("Failed to process event. Discarding it", e);

			ImportFinishedEvent finishedEvent = new ImportFinishedEvent(event.getUuid(), IMPORT_FAILED);
			eventSender.sendPrivateEvent(finishedEvent);
		}
	}

}
