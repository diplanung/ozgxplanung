/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.v2;

import static de.latlon.xplanbox.api.commons.v2.model.ResponseLink.RelEnum.PLANINFO;
import static de.latlon.xplanbox.api.commons.v2.model.ResponseLink.RelEnum.REPORT;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

import java.net.URI;
import java.net.URISyntaxException;

import de.latlon.xplan.commons.s3.StorageException;
import de.latlon.xplanbox.api.commons.exception.InvalidValidationUuid;
import de.latlon.xplanbox.api.commons.v2.model.ResponseLink;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReport;
import de.latlon.xplanbox.api.manager.config.ManagerApiConfiguration;
import de.latlon.xplanbox.api.manager.v2.model.ManagerStatusNotification;
import de.latlon.xplanbox.api.manager.v2.model.PlanInfo;
import de.latlon.xplanbox.api.manager.v2.model.StatusEnum;
import de.latlon.xplanbox.validator.storage.Status;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 8.0
 */
@Path("/status")
public class StatusApi {

	@Autowired
	private ManagerApiConfiguration managerApiConfiguration;

	@Autowired
	private ValidationExecutionStorage validationExecutionStorage;

	@GET
	@Produces({ "application/json" })
	@Path("/{uuid}")
	@Operation(summary = "Status of a import", description = "Returns the status of the import", tags = { "status" },
			responses = {
					@ApiResponse(responseCode = "200", description = "StatusNotification",
							content = { @Content(mediaType = APPLICATION_JSON,
									schema = @Schema(implementation = ManagerStatusNotification.class)) }),
					@ApiResponse(responseCode = "404",
							description = "Invalid uuid, no validation with the passed uuid found"),
					@ApiResponse(responseCode = "406", description = "Requested format is not available") })
	public Response status(
			@PathParam("uuid") @Parameter(description = "UUID of the validation",
					example = "0a0cedbc-bf3f-4f1f-bdaf-ea0e52075540") String uuid)
			throws InvalidValidationUuid, StorageException, URISyntaxException {
		try {
			Status status = validationExecutionStorage.retrieveStatus(uuid);
			return Response.ok(fromStatus(uuid, status)).build();
		}
		catch (StorageException e) {
			if (e.getStatusCode() == 404) {
				throw new InvalidValidationUuid(uuid);
			}
			throw e;
		}
	}

	public ManagerStatusNotification fromStatus(String uuid, Status status) {
		ManagerStatusNotification statusNotification = (ManagerStatusNotification) new ManagerStatusNotification()
			.status(StatusEnum.valueOf(status.getStatusType().name()))
			.importedPlanIds(status.getImportedPlanIds())
			.errorMsg(status.getErrorMsg());
		statusNotification.addLink((ResponseLink) new ResponseLink().rel(REPORT)
			.schema(createLinkToSchema(ValidationReport.class))
			.expirationTime(status.getExpirationTime())
			.type("application/json")
			.title("Validierungsreport")
			.href(createLinkToValidationReport(uuid)));
		statusNotification.addLink((ResponseLink) new ResponseLink().rel(REPORT)
			.expirationTime(status.getExpirationTime())
			.type("application/pdf")
			.title("Validierungsreport")
			.href(createLinkToValidationReport(uuid)));
		if (status.getImportedPlanIds() != null)
			status.getImportedPlanIds().forEach(planId -> {
				statusNotification.addLink((ResponseLink) new ResponseLink().rel(PLANINFO)
					.schema(createLinkToSchema(PlanInfo.class))
					.type("application/json")
					.title("PlanInfo " + planId)
					.href(createLinkToPlanInfo(planId)));
			});
		return statusNotification;
	}

	private URI createLinkToValidationReport(String uuid) {
		URI apiUrl = managerApiConfiguration.getApiUrl();
		if (apiUrl == null)
			return null;
		return apiUrl.resolve("api/v2/report/" + uuid);
	}

	private URI createLinkToPlanInfo(int planId) {
		URI apiUrl = managerApiConfiguration.getApiUrl();
		if (apiUrl == null)
			return null;
		return apiUrl.resolve("api/v2/plan/" + planId);
	}

	private URI createLinkToSchema(Class<?> schemaClass) {
		URI apiUrl = managerApiConfiguration.getApiUrl();
		if (apiUrl == null)
			return null;
		return apiUrl.resolve("api/v2#/components/schemas/" + schemaClass.getSimpleName());
	}

}
