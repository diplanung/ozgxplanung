/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.v2;

import static de.latlon.xplanbox.api.commons.XPlanMediaType.APPLICATION_PDF_TYPE;
import static de.latlon.xplanbox.validator.storage.ValidationExecutionStorage.ReportType.GEOJSON;
import static de.latlon.xplanbox.validator.storage.ValidationExecutionStorage.ReportType.JSON;
import static de.latlon.xplanbox.validator.storage.ValidationExecutionStorage.ReportType.PDF;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

import de.latlon.xplan.commons.s3.StorageException;
import de.latlon.xplan.validator.report.geojson.model.FeatureCollection;
import de.latlon.xplanbox.api.commons.exception.InvalidValidationUuid;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReport;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage;
import de.latlon.xplanbox.validator.storage.exception.EventExecutionException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Variant;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Controller class for handling access to the application info resource.
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @author <a href="mailto:friebe@lat-lon.de">Torsten Friebe</a>
 * @since 8.0
 */
@Path("/report")
public class ReportApi {

	@Autowired
	private ValidationExecutionStorage validationExecutionStorage;

	@GET
	@Path("/{uuid}")
	@Produces({ "application/json", "application/pdf" })
	@Operation(summary = "Return ValidationReport",
			description = "Returns the ValidationReport of the validation with {uuid}",
			responses = {
					@ApiResponse(responseCode = "200", description = "successful operation",
							content = @Content(schema = @Schema(implementation = ValidationReport.class))),
					@ApiResponse(responseCode = "404",
							description = "ValidationReport of the validation with {uuid} is not available or is expired"),
					@ApiResponse(responseCode = "406", description = "Requested format is not available") })
	public Response reportByUuid(@Context Request request,
			@PathParam("uuid") @Parameter(description = "UUID of the validation to be returned",
					example = "uuid") String uuid)
			throws EventExecutionException, StorageException, InvalidValidationUuid {
		try {
			MediaType mediaType = detectRequestedMediaType(request);
			byte[] report;
			if (APPLICATION_PDF_TYPE.equals(mediaType))
				report = validationExecutionStorage.retrieveReport(uuid, PDF);
			else
				report = validationExecutionStorage.retrieveReport(uuid, JSON);
			return Response.ok().entity(report).build();
		}
		catch (StorageException e) {
			if (e.getStatusCode() == 404) {
				throw new InvalidValidationUuid(uuid);
			}
			throw e;
		}
	}

	@GET
	@Path("/{uuid}/geomfindings")
	@Produces({ "application/geo+json" })
	@Operation(summary = "Return ValidationReport",
			description = "Returns the findings of the geometrical validation with {uuid} as GeoJSON",
			responses = {
					@ApiResponse(responseCode = "200", description = "successful operation",
							content = @Content(schema = @Schema(implementation = FeatureCollection.class))),
					@ApiResponse(responseCode = "404",
							description = "Findings of the geometrical validation with {uuid} is not available or is expired"),
					@ApiResponse(responseCode = "406", description = "Requested format is not available") })
	public Response geomfindingsByUuid(
			@PathParam("uuid") @Parameter(description = "UUID of the validation to be returned",
					example = "uuid") String uuid)
			throws EventExecutionException, StorageException, InvalidValidationUuid {
		return geomfindings(uuid);
	}

	@GET
	@Path("/{uuid}/geomfindings.json")
	@Produces({ "application/geo+json" })
	@Operation(summary = "Return ValidationReport",
			description = "Returns the findings of the geometrical validation with {uuid} as GeoJSON",
			responses = {
					@ApiResponse(responseCode = "200", description = "successful operation",
							content = @Content(schema = @Schema(implementation = FeatureCollection.class))),
					@ApiResponse(responseCode = "404",
							description = "Findings of the geometrical validation with {uuid} is not available or is expired"),
					@ApiResponse(responseCode = "406", description = "Requested format is not available") })
	public Response geomfindingsJsonByUuid(
			@PathParam("uuid") @Parameter(description = "UUID of the validation to be returned",
					example = "uuid") String uuid)
			throws EventExecutionException, StorageException, InvalidValidationUuid {
		return geomfindings(uuid);
	}

	private Response geomfindings(String uuid) throws EventExecutionException, InvalidValidationUuid, StorageException {
		try {
			byte[] report = validationExecutionStorage.retrieveReport(uuid, GEOJSON);
			return Response.ok().entity(report).build();
		}
		catch (StorageException e) {
			if (e.getStatusCode() == 404) {
				throw new InvalidValidationUuid(uuid);
			}
			throw e;
		}
	}

	private MediaType detectRequestedMediaType(Request request) {
		Variant.VariantListBuilder acceptedMediaTypes = Variant.mediaTypes(APPLICATION_JSON_TYPE, APPLICATION_PDF_TYPE);
		Variant selectVariant = request.selectVariant(acceptedMediaTypes.build());
		if (selectVariant == null)
			return APPLICATION_JSON_TYPE;
		return selectVariant.getMediaType();
	}

}
