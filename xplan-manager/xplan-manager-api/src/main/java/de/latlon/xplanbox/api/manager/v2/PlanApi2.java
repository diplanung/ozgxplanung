/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.v2;

import static de.latlon.xplan.commons.util.ContentTypeChecker.checkContentTypesOfXPlanArchiveOrGml;
import static de.latlon.xplan.commons.util.TextPatternConstants.INTERNALID_PATTERN;
import static de.latlon.xplan.commons.util.TextPatternConstants.SIMPLE_NAME_PATTERN;
import static de.latlon.xplanbox.api.commons.ValidatorConverter.createValidationSettings;
import static de.latlon.xplanbox.api.commons.ValidatorConverter.detectOrCreateValidationName;
import static de.latlon.xplanbox.api.commons.XPlanMediaType.APPLICATION_ZIP;
import static de.latlon.xplanbox.api.commons.v2.model.ResponseLink.RelEnum.STATUS;
import static de.latlon.xplanbox.validator.storage.StatusType.IMPORT_REQUESTED;
import static io.swagger.v3.oas.annotations.enums.Explode.FALSE;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

import java.io.File;
import java.net.URI;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.latlon.core.validator.events.EventSender;
import de.latlon.core.validator.events.ValidationRequestedEvent;
import de.latlon.core.validator.events.planimport.ImportValidationRequestedEvent;
import de.latlon.xplan.core.manager.db.model.Geltungsbereich;
import de.latlon.xplan.manager.web.shared.XPlan;
import de.latlon.xplan.validator.web.shared.ValidationSettings;
import de.latlon.xplanbox.api.commons.exception.UnsupportedParameterValue;
import de.latlon.xplanbox.api.commons.v2.model.ResponseLink;
import de.latlon.xplanbox.api.manager.PlanInfoBuilder;
import de.latlon.xplanbox.api.manager.config.DefaultValidationConfiguration;
import de.latlon.xplanbox.api.manager.config.ManagerApiConfiguration;
import de.latlon.xplanbox.api.manager.handler.PlanHandler;
import de.latlon.xplanbox.api.manager.v1.model.Bereich;
import de.latlon.xplanbox.api.manager.v1.model.Gemeinde;
import de.latlon.xplanbox.api.manager.v1.model.StatusMessage;
import de.latlon.xplanbox.api.manager.v2.model.ImportReceipt;
import de.latlon.xplanbox.api.manager.v2.model.PlanInfo;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.servlet.ServletContext;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.StreamingOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

/**
 * Controller class for handling access to a plan identified by it's id.
 *
 * @author <a href="mailto:friebe@lat-lon.de">Torsten Friebe</a>
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 4.0
 */
@Path("/plan")
@Component
public class PlanApi2 {

	private static final boolean WITH_SKIP_GEOMETRISCH_VALIDATION = false;

	private static final boolean WITH_SKIP_RASTER_EVALUATION = false;

	@Autowired
	private ValidationExecutionStorage validationExecutionStorage;

	@Autowired
	private EventSender eventSender;

	@Autowired
	private PlanHandler planHandler;

	@Context
	private ManagerApiConfiguration managerApiConfiguration;

	@Context
	private ServletContext servletContext;

	@PreAuthorize("hasRole('XPLANBOX_ADMIN') || @agsAuthorizer.hasAgs(#body, 'ZIP', authentication)")
	@POST
	@Consumes({ "application/octet-stream", "application/zip", "application/x-zip", "application/x-zip-compressed" })
	@Produces("application/json")
	@Operation(operationId = "import", summary = "Import a XPlanGML or XPlanArchive",
			description = "Imports a XPlanGML or XPlanArchive", tags = { "manage", },
			responses = {
					@ApiResponse(responseCode = "200", description = "ImportReceipt with uuid of the import",
							content = { @Content(mediaType = "application/json",
									schema = @Schema(implementation = ImportReceipt.class)) }),
					@ApiResponse(responseCode = "400", description = "Invalid input"),
					@ApiResponse(responseCode = "406", description = "Requested format is not available"),
					@ApiResponse(responseCode = "415",
							description = "Unsupported media type or content - only xml/gml, zip are accepted; all zip files entries must also match the supported content types for XPlanArchives") },
			requestBody = @RequestBody(
					content = {
							@Content(mediaType = "application/octet-stream",
									schema = @Schema(type = "string", format = "binary",
											description = "XPlanArchive (application/zip) file to upload")),
							@Content(mediaType = "application/zip",
									schema = @Schema(type = "string", format = "binary",
											description = "XPlanArchive (application/zip) file to upload")),
							@Content(mediaType = "application/x-zip",
									schema = @Schema(type = "string", format = "binary",
											description = "XPlanArchive (application/zip) file to upload")),
							@Content(mediaType = "application/x-zip-compressed",
									schema = @Schema(type = "string", format = "binary",
											description = "XPlanArchive (application/zip) file to upload")),
							@Content(mediaType = "text/xml",
									schema = @Schema(type = "string", format = "binary",
											description = "XPlanGML file to upload")),
							@Content(mediaType = "application/gml+xml", schema = @Schema(type = "string",
									format = "binary", description = "XPlanGML file to upload")) },
					required = true))
	public Response callImportZip(@Context Request request, @Valid File body,
			@HeaderParam("X-Filename") @Parameter(description = "Name of the file to be uploaded",
					example = "File names such as xplan.gml, xplan.xml, xplan.zip",
					schema = @Schema(pattern = SIMPLE_NAME_PATTERN)) String xFilename,
			@QueryParam("skipSemantisch") @DefaultValue("false") @Parameter(
					description = "skip semantische Validierung") Boolean skipSemantisch,
			@QueryParam("skipFlaechenschluss") @DefaultValue("false") @Parameter(
					description = "skip Flaechenschluss Ueberpruefung") Boolean skipFlaechenschluss,
			@QueryParam("skipGeltungsbereich") @DefaultValue("false") @Parameter(
					description = "skip Geltungsbereich Ueberpruefung") Boolean skipGeltungsbereich,
			@QueryParam("skipLaufrichtung") @DefaultValue("false") @Parameter(
					description = "skip Laufrichtung Ueberpruefung") Boolean skipLaufrichtung,
			@QueryParam("profiles") @Parameter(
					description = "Names of profiles which shall be additionaly used for validation",
					explode = FALSE) List<String> profiles,
			@QueryParam("internalId") @Parameter(description = "internalId links to VerfahrensId",
					schema = @Schema(pattern = INTERNALID_PATTERN)) String internalId,
			@QueryParam("planStatus") @Parameter(
					description = "target for data storage, overrides the default derived from xplan:rechtsstand",
					schema = @Schema(allowableValues = { "IN_AUFSTELLUNG", "FESTGESTELLT", "ARCHIVIERT" },
							example = "FESTGESTELLT")) String planStatus)
			throws Exception {
		checkContentTypesOfXPlanArchiveOrGml(body.toPath());
		return callImport(xFilename, skipSemantisch, skipFlaechenschluss, skipGeltungsbereich, skipLaufrichtung,
				profiles, internalId, planStatus, body, ValidationRequestedEvent.OriginFile.ZIP);
	}

	@PreAuthorize("hasRole('XPLANBOX_ADMIN') || @agsAuthorizer.hasAgs(#body, 'GML', authentication)")
	@POST
	@Consumes({ "text/xml", "application/gml+xml" })
	@Produces("application/json")
	@Hidden
	public Response callImportGml(@Context Request request, @Valid File body,
			@HeaderParam("X-Filename") @Parameter(schema = @Schema(pattern = SIMPLE_NAME_PATTERN)) String xFilename,
			@QueryParam("skipSemantisch") @DefaultValue("false") Boolean skipSemantisch,
			@QueryParam("skipFlaechenschluss") @DefaultValue("false") Boolean skipFlaechenschluss,
			@QueryParam("skipGeltungsbereich") @DefaultValue("false") Boolean skipGeltungsbereich,
			@QueryParam("skipLaufrichtung") @DefaultValue("false") Boolean skipLaufrichtung,
			@QueryParam("profiles") List<String> profiles,
			@QueryParam("internalId") @Parameter(schema = @Schema(pattern = INTERNALID_PATTERN)) String internalId,
			@QueryParam("planStatus") String planStatus) throws Exception {
		checkContentTypesOfXPlanArchiveOrGml(body.toPath());
		return callImport(xFilename, skipSemantisch, skipFlaechenschluss, skipGeltungsbereich, skipLaufrichtung,
				profiles, internalId, planStatus, body, ValidationRequestedEvent.OriginFile.GML);
	}

	@PreAuthorize("hasRole('XPLANBOX_ADMIN') || @agsAuthorizer.hasAgs(#planId, authentication)")
	@DELETE
	@Path("/{planId}")
	@Produces({ "application/json" })
	@Operation(summary = "Delete plan identified by the given planID",
			description = "Deletes an existing plan identified by the given planID", tags = { "manage", },
			responses = {
					@ApiResponse(responseCode = "200", description = "successful operation",
							content = @Content(schema = @Schema(implementation = StatusMessage.class))),
					@ApiResponse(responseCode = "400", description = "PlanID is not a valid int value"),
					@ApiResponse(responseCode = "404", description = "Invalid planID, plan not found"),
					@ApiResponse(responseCode = "406", description = "Requested format is not available") })
	public Response delete(@PathParam("planId") @Parameter(description = "ID of the plan to be removed",
			example = "123") String planId) throws Exception {
		StatusMessage statusMessage = planHandler.deletePlan(planId);
		return Response.ok().entity(statusMessage).build();
	}

	@GET
	@Path("/{planId}")
	@Produces({ "application/json" })
	@Operation(summary = "Get plan identified by the given planID",
			description = "Returns an existing plan identified by the given planID", tags = { "manage", "search", },
			responses = {
					@ApiResponse(responseCode = "200", description = "successful operation",
							content = { @Content(mediaType = "application/json",
									schema = @Schema(implementation = PlanInfo.class)) }),
					@ApiResponse(responseCode = "400", description = "PlanID is not a valid int value"),
					@ApiResponse(responseCode = "404", description = "Invalid planID, plan not found"),
					@ApiResponse(responseCode = "406", description = "Requested format is not available") })
	public Response getById(@Context Request request,
			@PathParam("planId") @Parameter(description = "ID of the plan to be returned",
					example = "123") String planId)
			throws Exception {
		XPlan planById = planHandler.findPlanById(planId);
		PlanInfo planInfo = createPlanInfo(planById);
		return Response.ok().entity(planInfo).build();
	}

	@GET
	@Path("/{planId}/archive")
	@Produces("application/zip")
	@Operation(summary = "Get plan as XPlanArchive identified by the given planID",
			description = "Returns the XPlanArchive of an existing plan identified by the given planID",
			tags = { "manage", "search", },
			responses = {
					@ApiResponse(responseCode = "200", description = "successful operation",
							content = { @Content(mediaType = "application/zip",
									schema = @Schema(type = "string", format = "binary")) }),
					@ApiResponse(responseCode = "400", description = "PlanID is not a valid int value"),
					@ApiResponse(responseCode = "404", description = "Invalid planID, plan not found"),
					@ApiResponse(responseCode = "406", description = "Requested format is not available") })
	public Response getArchiveById(@Context Request request,
			@PathParam("planId") @Parameter(description = "ID of the plan to be returned",
					example = "123") String planId)
			throws Exception {
		StreamingOutput plan = planHandler.exportPlan(planId);
		return Response.ok(plan)
			.type(APPLICATION_ZIP)
			.header("Content-Disposition", "attachment; filename=\"" + planId + ".zip\"")
			.build();
	}

	private Response callImport(String xFilename, Boolean skipSemantisch, Boolean skipFlaechenschluss,
			Boolean skipGeltungsbereich, Boolean skipLaufrichtung, List<String> profiles, String internalId,
			String planStatus, File file, ValidationRequestedEvent.OriginFile originFile) throws Exception {
		checkInternalId(internalId);

		String uuid = validationExecutionStorage.addPlanToValidate(file.toPath(), IMPORT_REQUESTED);

		String validationName = detectOrCreateValidationName(xFilename);
		DefaultValidationConfiguration validationConfig = managerApiConfiguration.getDefaultValidationConfiguration();
		ValidationSettings validationSettings = createValidationSettings(validationName,
				WITH_SKIP_GEOMETRISCH_VALIDATION,
				overwriteByRequest(skipSemantisch, validationConfig.isSkipSemantisch()),
				overwriteByRequest(skipFlaechenschluss, validationConfig.isSkipFlaechenschluss()),
				overwriteByRequest(skipGeltungsbereich, validationConfig.isSkipGeltungsbereich()),
				overwriteByRequest(skipLaufrichtung, validationConfig.isSkipLaufrichtung()), profiles,
				WITH_SKIP_RASTER_EVALUATION);

		eventSender.sendPrivateEvent(new ImportValidationRequestedEvent(uuid, validationSettings, xFilename, internalId,
				planStatus, ValidationRequestedEvent.MediaType.JSON, originFile, false));

		URI linkToStatus = createLinkToStatus(uuid);
		ResponseLink statusLink = (ResponseLink) new ResponseLink().rel(STATUS)
			.href(linkToStatus)
			.type("application/json");
		ImportReceipt importReceipt = new ImportReceipt(uuid, statusLink);
		return Response.ok().entity(importReceipt).build();
	}

	private PlanInfo createPlanInfo(XPlan planById) {
		List<Bereich> bereiche = planHandler.findBereiche(planById.getId());
		List<Gemeinde> gemeinden = planHandler.findGemeinden(planById.getId());
		Geltungsbereich geltungsbereich = planHandler.findGeltungsbereich(planById.getId());
		return new PlanInfoBuilder(planById, bereiche, gemeinden, geltungsbereich, managerApiConfiguration)
			.selfMediaType(APPLICATION_JSON)
			.buildV2();
	}

	private URI createLinkToStatus(String uuid) {
		URI apiUrl = managerApiConfiguration.getApiUrl();
		if (apiUrl == null)
			return null;
		return apiUrl.resolve(servletContext.getContextPath() + "/api/v2/status/" + uuid);
	}

	private boolean overwriteByRequest(Boolean requested, boolean configured) {
		if (requested != null)
			return requested;
		return configured;
	}

	private void checkInternalId(String internalId) throws UnsupportedParameterValue {
		if (internalId == null)
			return;
		Pattern pattern = Pattern.compile(INTERNALID_PATTERN);
		Matcher matcher = pattern.matcher(internalId);
		if (!matcher.matches()) {
			throw new UnsupportedParameterValue("internalId", internalId);
		}

	}

}
