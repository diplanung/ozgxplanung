package de.latlon.xplanbox.api.manager.v2.model;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import de.latlon.xplan.commons.XPlanType;
import de.latlon.xplan.commons.XPlanVersion;
import de.latlon.xplan.core.manager.db.model.OperationType;
import de.latlon.xplan.core.manager.db.model.PlansLogEntry;
import de.latlon.xplan.manager.web.shared.PlanStatus;
import de.latlon.xplanbox.api.commons.v1.model.PlanInfoBbox;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.1
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlanLog {

	private Integer id;

	private Integer planId;

	private XPlanVersion version;

	private XPlanType type;

	private PlanInfoBbox bbox;

	private PlanStatus planstatusNew;

	private PlanStatus planstatusOld;

	private OperationType operation;

	private LocalDateTime lastUpdateDate;

	public static PlanLog fromPlansLogEntry(PlansLogEntry plansLogEntry) {
		return new PlanLog().id(plansLogEntry.getId())
			.planId(plansLogEntry.getPlanId())
			.version(plansLogEntry.getVersion())
			.type(plansLogEntry.getType())
			.bbox(convertToXPlanEnvelope(plansLogEntry.getBbox()))
			.planstatusNew(plansLogEntry.getPlanstatusNew())
			.planstatusOld(plansLogEntry.getPlanstatusOld())
			.operation(plansLogEntry.getOperation())
			.lastUpdateDate(plansLogEntry.getLastUpdateDate());
	}

	private static PlanInfoBbox convertToXPlanEnvelope(Geometry bbox) {
		Coordinate[] coordinates = bbox.getCoordinates();
		double minx = 0, miny = 0, maxx = 0, maxy = 0;
		boolean first = true;
		for (Coordinate coordinate : coordinates) {
			if (first) {
				minx = coordinate.x;
				miny = coordinate.y;
				maxx = coordinate.x;
				maxy = coordinate.y;
			}
			else {
				minx = Math.min(coordinate.x, minx);
				miny = Math.min(coordinate.y, miny);
				maxx = Math.max(coordinate.x, maxx);
				maxy = Math.max(coordinate.y, maxy);
			}
			first = false;
		}
		return new PlanInfoBbox().minX(minx).minY(miny).maxX(maxx).maxY(maxy).crs("EPSG:4326");
	}

	public PlanLog id(Integer id) {
		this.id = id;
		return this;
	}

	@Schema(format = "int32", example = "123", description = "internal unique identifier of the log entry")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PlanLog planId(Integer planId) {
		this.planId = planId;
		return this;
	}

	@Schema(format = "int32", example = "123", description = "internal unique manager identifier")
	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public PlanLog version(XPlanVersion version) {
		this.version = version;
		return this;
	}

	@Schema(example = "XPLAN_51")
	public XPlanVersion getVersion() {
		return version;
	}

	public void setVersion(XPlanVersion version) {
		this.version = version;
	}

	public PlanLog type(XPlanType type) {
		this.type = type;
		return this;
	}

	@Schema(example = "BP_Plan")
	public XPlanType getType() {
		return type;
	}

	public void setType(XPlanType type) {
		this.type = type;
	}

	public PlanLog bbox(PlanInfoBbox bbox) {
		this.bbox = bbox;
		return this;
	}

	@Schema(description = "bbox of the plan")
	public PlanInfoBbox getBbox() {
		return bbox;
	}

	public void setBbox(PlanInfoBbox bbox) {
		this.bbox = bbox;
	}

	public PlanLog planstatusNew(PlanStatus planstatusNew) {
		this.planstatusNew = planstatusNew;
		return this;
	}

	@Schema(example = "Festgestellt")
	public PlanStatus getPlanstatusNew() {
		return planstatusNew;
	}

	public void setPlanstatusNew(PlanStatus planstatusNew) {
		this.planstatusNew = planstatusNew;
	}

	public PlanLog planstatusOld(PlanStatus planstatusOld) {
		this.planstatusOld = planstatusOld;
		return this;
	}

	@Schema(example = "Festgestellt")
	public PlanStatus getPlanstatusOld() {
		return planstatusOld;
	}

	public void setPlanstatusOld(PlanStatus planstatusOld) {
		this.planstatusOld = planstatusOld;
	}

	public PlanLog operation(OperationType operation) {
		this.operation = operation;
		return this;
	}

	@Schema(example = "Insert")
	public OperationType getOperation() {
		return operation;
	}

	public void setOperation(OperationType operation) {
		this.operation = operation;
	}

	public PlanLog lastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
		return this;
	}

	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

}
