/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.executor;

import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.IMPORT_FINISHED;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.IMPORT_STARTED;
import static de.latlon.xplanbox.validator.storage.StatusType.IMPORT_FAILED;
import static de.latlon.xplanbox.validator.storage.exception.ErrorType.INTERNAL_ERROR;
import static de.latlon.xplanbox.validator.storage.exception.ErrorType.INVALID_REQUEST;
import static de.latlon.xplanbox.validator.storage.exception.ErrorType.fromStatusCode;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import de.latlon.core.validator.events.EventSender;
import de.latlon.core.validator.events.ValidationRequestedEvent;
import de.latlon.core.validator.events.planimport.ImportRequestedEvent;
import de.latlon.core.validator.events.v1.XPlanPublicV1Event;
import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.commons.s3.StorageException;
import de.latlon.xplan.manager.transaction.UnsupportPlanException;
import de.latlon.xplanbox.api.commons.exception.InvalidXPlanGmlOrArchive;
import de.latlon.xplanbox.api.commons.exception.XPlanApiException;
import de.latlon.xplanbox.api.manager.handler.PlanHandler;
import de.latlon.xplanbox.validator.storage.StatusType;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 8.0
 */
@Component
public class PlanImporter {

	private static final Logger LOG = LoggerFactory.getLogger(PlanImporter.class);

	@Autowired
	private EventSender eventSender;

	@Autowired
	private PlanHandler planHandler;

	@Autowired
	private ValidationExecutionStorage validationExecutionStorage;

	private final XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();

	public void importPlan(ImportRequestedEvent event) throws Exception {
		if (event.getValidationResult() != ImportRequestedEvent.VALIDATION_RESULT.VALID) {
			validationExecutionStorage.changeStatus(event.getUuid(), StatusType.IMPORT_ABORTED,
					"Import aborted due to invalid XPlanGML");
			return;
		}
		if (event
			.getExternalReferencesResult() != ImportRequestedEvent.EXTERNAL_REFERENCES_RESULT.AVAILABLE_AND_VALID) {
			validationExecutionStorage.changeStatus(event.getUuid(), StatusType.IMPORT_ABORTED,
					"Import aborted due missing external References or invalid Rasterfiles");
			return;
		}
		doImportPlan(event);
	}

	private void doImportPlan(ImportRequestedEvent event) throws Exception {
		validationExecutionStorage.changeStatus(event.getUuid(), StatusType.IMPORT_STARTED);
		eventSender.sendPublicEvent(new XPlanPublicV1Event(IMPORT_STARTED, event.getUuid()),
				IMPORT_STARTED.routingKeySuffix());
		Path tmpPath = Files.createTempFile(event.getUuid(), ".rcv");
		if (Files.exists(tmpPath))
			Files.delete(tmpPath);
		try {
			XPlanArchive archive = createArchive(event, tmpPath);
			List<Integer> planIds = planHandler.importPlanWithoutValidation(archive, event.getInternalId(),
					event.getPlanStatus());
			LOG.info("Imported plans: {}", planIds.size());
			validationExecutionStorage.changeStatus(event.getUuid(), StatusType.IMPORT_FINISHED, planIds);
			validationExecutionStorage.cleanupAfterValidation(event.getUuid());
		}
		catch (XPlanApiException e) {
			validationExecutionStorage.changeStatus(event.getUuid(), e.getMessage(), fromStatusCode(e.getStatusCode()),
					IMPORT_FAILED);
			LOG.info("Import with uuid {} failed: {}", event.getUuid(), e.getMessage());
			throw e;
		}
		catch (UnsupportPlanException e) {
			validationExecutionStorage.changeStatus(event.getUuid(), e.getMessage(), INVALID_REQUEST, IMPORT_FAILED);
			LOG.info("Import with uuid {} failed: {}", event.getUuid(), e.getMessage());
			throw e;
		}
		catch (Exception e) {
			validationExecutionStorage.changeStatus(event.getUuid(), e.getMessage(), INTERNAL_ERROR, IMPORT_FAILED);
			LOG.info("Import with uuid {} failed: {}", event.getUuid(), e.getMessage());
			throw e;
		}
		finally {
			Files.delete(tmpPath);
			eventSender.sendPublicEvent(new XPlanPublicV1Event(IMPORT_FINISHED, event.getUuid()),
					IMPORT_FINISHED.routingKeySuffix());
		}
	}

	private XPlanArchive createArchive(ImportRequestedEvent event, Path tmpPath)
			throws IOException, StorageException, InvalidXPlanGmlOrArchive {
		validationExecutionStorage.writePlanToValidate(event.getUuid(), tmpPath);
		if (event.getOriginFile() == ValidationRequestedEvent.OriginFile.GML) {
			return createArchiveFromGml(tmpPath.toFile(), event.getValidationName());
		}
		return createArchiveFromZip(tmpPath.toFile(), event.getValidationName());
	}

	private XPlanArchive createArchiveFromZip(File uploadedPlan, String validationName)
			throws InvalidXPlanGmlOrArchive {
		try {
			return archiveCreator.createXPlanArchiveFromZip(validationName, new FileInputStream(uploadedPlan));
		}
		catch (Exception e) {
			throw new InvalidXPlanGmlOrArchive("Could not read attached file as XPlanArchive", e);
		}
	}

	private XPlanArchive createArchiveFromGml(File uploadedPlan, String validationName)
			throws InvalidXPlanGmlOrArchive {
		try {
			return archiveCreator.createXPlanArchiveFromGml(validationName, new FileInputStream(uploadedPlan));
		}
		catch (Exception e) {
			throw new InvalidXPlanGmlOrArchive("Could not read attached file as XPlanGML", e);
		}
	}

}
