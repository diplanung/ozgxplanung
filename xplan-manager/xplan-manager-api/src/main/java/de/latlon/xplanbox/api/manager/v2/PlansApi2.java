/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.v2;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.List;
import java.util.stream.Collectors;

import de.latlon.xplan.core.manager.db.model.Geltungsbereich;
import de.latlon.xplan.manager.web.shared.XPlan;
import de.latlon.xplanbox.api.manager.PlanInfoBuilder;
import de.latlon.xplanbox.api.manager.config.ManagerApiConfiguration;
import de.latlon.xplanbox.api.manager.exception.InvalidSearch;
import de.latlon.xplanbox.api.manager.handler.PlanHandler;
import de.latlon.xplanbox.api.manager.v1.model.Bereich;
import de.latlon.xplanbox.api.manager.v1.model.Gemeinde;
import de.latlon.xplanbox.api.manager.v1.model.PlanStatusEnum;
import de.latlon.xplanbox.api.manager.v2.model.PlanInfo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Controller class for handling search over all plans.
 *
 * @author <a href="mailto:friebe@lat-lon.de">Torsten Friebe</a>
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 4.0
 */
@Path("/plans")
public class PlansApi2 {

	@Autowired
	private PlanHandler planHandler;

	@Context
	private ManagerApiConfiguration managerApiConfiguration;

	@GET
	@Produces({ "application/json" })
	@Operation(summary = "Search for plan by name",
			description = "Returns a list of plans where the plan matches the passed query parameter",
			tags = { "search" },
			responses = {
					@ApiResponse(responseCode = "200", description = "OK",
							content = @Content(
									array = @ArraySchema(schema = @Schema(implementation = PlanInfo.class)))),
					@ApiResponse(responseCode = "406", description = "Requested format is not available") })
	public Response searchPlans(
			@QueryParam("planName") @Parameter(description = "The name of the plan to search for",
					example = "bplan_123") String planName,
			@QueryParam("internalId") @Parameter(description = "The internalId of the plan to search for",
					example = "ID_8ee57554-5f58-42ea-86a4-e9f4acd9d3c1") String internalId,
			@QueryParam("planStatus") @Parameter(description = "The planStatuses of the plan to search for",
					example = "FESTGESTELLT") List<PlanStatusEnum> planStatuses,
			@QueryParam("planId") @Parameter(description = "The ID of the plan to search for",
					example = "1, 2, 42") List<Integer> planIds,
			@QueryParam("includeGeltungsbereich") @DefaultValue("false") @Parameter(
					description = "true if the geltungsbereichWGS84 should be included in the response, false otherwise (default: false).") boolean includeGeltungsbereich)
			throws Exception {
		if ((planName != null || internalId != null || (planStatuses != null && !planStatuses.isEmpty()))
				&& !planIds.isEmpty())
			throw new InvalidSearch(
					"Searching by planName, internalId or planstatus and id within the same request is not supported!");
		List<XPlan> plans = searchPlans(planName, internalId, planStatuses, planIds);
		List<PlanInfo> planInfos = plans.stream().map(xPlan -> {
			List<Bereich> bereiche = planHandler.findBereiche(xPlan.getId());
			List<Gemeinde> gemeinden = planHandler.findGemeinden(xPlan.getId());
			Geltungsbereich geltungsbereich = includeGeltungsbereich ? planHandler.findGeltungsbereich(xPlan.getId())
					: null;
			return new PlanInfoBuilder(xPlan, bereiche, gemeinden, geltungsbereich, managerApiConfiguration)
				.selfMediaType(APPLICATION_JSON)
				.buildV2();
		}).collect(Collectors.toList());
		return Response.ok().entity(planInfos).build();
	}

	private List<XPlan> searchPlans(String planName, String internalId, List<PlanStatusEnum> planStatuses,
			List<Integer> planIds) throws Exception {
		if (!planIds.isEmpty()) {
			return planHandler.findPlansById(planIds);
		}
		return planHandler.findPlans(planName, internalId, planStatuses);
	}

}
