/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.v1.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class Gemeinde {

	private @Valid @JsonInclude(JsonInclude.Include.NON_NULL) String ags;

	private @Valid @JsonInclude(JsonInclude.Include.NON_NULL) String rs;

	private @Valid @JsonInclude(JsonInclude.Include.NON_NULL) String gemeindeName;

	private @Valid @JsonInclude(JsonInclude.Include.NON_NULL) String ortsteilName;

	public Gemeinde ags(String ags) {
		this.ags = ags;
		return this;
	}

	@Schema(example = "02000000")
	@JsonProperty("ags")
	public String getAgs() {
		return ags;
	}

	public void setAgs(String ags) {
		this.ags = ags;
	}

	public Gemeinde rs(String rs) {
		this.rs = rs;
		return this;
	}

	@Schema(example = "123765305201")
	@JsonProperty("rs")
	public String getRs() {
		return rs;
	}

	public void setRs(String rs) {
		this.rs = rs;
	}

	public Gemeinde gemeindeName(String gemeindeName) {
		this.gemeindeName = gemeindeName;
		return this;
	}

	@Schema(example = "Gemeindename")
	@JsonProperty("gemeindeName")
	public String getGemeindeName() {
		return gemeindeName;
	}

	public void setGemeindeName(String gemeindeName) {
		this.gemeindeName = gemeindeName;
	}

	public Gemeinde ortsteilName(String ortsteilName) {
		this.ortsteilName = ortsteilName;
		return this;
	}

	@Schema(example = "Ortsteilname")
	@JsonProperty("ortsteilName")
	public String getOrtsteilName() {
		return ortsteilName;
	}

	public void setOrtsteilName(String ortsteilName) {
		this.ortsteilName = ortsteilName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Gemeinde gemeinde = (Gemeinde) o;
		return Objects.equals(ags, gemeinde.ags) && Objects.equals(rs, gemeinde.rs)
				&& Objects.equals(gemeindeName, gemeinde.gemeindeName)
				&& Objects.equals(ortsteilName, gemeinde.ortsteilName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(ags, rs, gemeindeName, ortsteilName);
	}

	@Override
	public String toString() {
		return "Gemeinde{" + "ags='" + ags + '\'' + ", rs='" + rs + '\'' + ", gemeindeName='" + gemeindeName + '\''
				+ ", ortsteilName='" + ortsteilName + '\'' + '}';
	}

}
