package de.latlon.xplanbox.api.manager.handler;

import java.util.List;
import java.util.stream.StreamSupport;

import de.latlon.xplan.core.manager.db.model.PlansLogEntry;
import de.latlon.xplan.manager.database.XPlanDao;
import de.latlon.xplanbox.api.manager.v2.model.PlanLog;
import jakarta.inject.Singleton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.1
 */
@Component
@Singleton
public class PlansLogHandler {

	@Autowired
	private XPlanDao xPlanDao;

	public List<PlanLog> retrievePlanLog(int offset, int limit, int days) {
		Iterable<PlansLogEntry> plansLogEntries = xPlanDao.retrievePlansLog(offset, limit, days);
		return StreamSupport.stream(plansLogEntries.spliterator(), false).map(PlanLog::fromPlansLogEntry).toList();
	}

}
