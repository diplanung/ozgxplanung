/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.v2;

import java.util.List;

import de.latlon.xplanbox.api.commons.exception.UnsupportedParameterValue;
import de.latlon.xplanbox.api.manager.handler.PlansLogHandler;
import de.latlon.xplanbox.api.manager.v2.model.PlanLog;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 * Controller class for providing PlansLog.
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 8.1
 */
@Path("/planslog")
public class PlansLogApi2 {

	@Autowired
	private PlansLogHandler plansLogHandler;

	@PreAuthorize("hasRole('XPLANBOX_ADMIN')")
	@GET
	@Produces({ "application/json" })
	@Operation(summary = "Get planslog", description = "Returns the planslog describing imported/edited/deleted plans",
			responses = {
					@ApiResponse(responseCode = "200", description = "OK",
							content = @Content(array = @ArraySchema(schema = @Schema(implementation = PlanLog.class)))),
					@ApiResponse(responseCode = "400", description = "Unsupported query parameter"),
					@ApiResponse(responseCode = "404", description = "Invalid offset, limit or days parameter"),
					@ApiResponse(responseCode = "406", description = "Requested format is not available") })
	public List<PlanLog> planslog(
			@QueryParam("offset") @DefaultValue("0") @Parameter(description = "Number of entries to skip",
					schema = @Schema(minimum = "0")) int offset,
			@QueryParam("limit") @DefaultValue("50") @Parameter(description = "Number of entries to return",
					schema = @Schema(minimum = "1")) int limit,
			@QueryParam("days") @Parameter(description = "Filters the entries by the last update date beginning now",
					schema = @Schema(minimum = "1")) Integer days)
			throws UnsupportedParameterValue {
		if (offset < 0)
			throw new UnsupportedParameterValue("offset", Integer.toString(offset));
		if (limit < 1)
			throw new UnsupportedParameterValue("limit", Integer.toString(limit));
		if (days != null && days < 1)
			throw new UnsupportedParameterValue("days", Integer.toString(days));
		return plansLogHandler.retrievePlanLog(offset, limit, days == null ? -1 : days);
	}

}
