/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.openapi.v2;

import static org.slf4j.LoggerFactory.getLogger;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.latlon.xplanbox.api.manager.config.ManagerApiConfiguration;
import de.latlon.xplanbox.api.manager.openapi.AbstractApiConfig;
import de.latlon.xplanbox.api.manager.v1.DefaultApi;
import de.latlon.xplanbox.api.manager.v2.InfoApi2;
import de.latlon.xplanbox.api.manager.v2.PlanAenderungenApi2;
import de.latlon.xplanbox.api.manager.v2.PlanApi2;
import de.latlon.xplanbox.api.manager.v2.PlanBasisdatenApi2;
import de.latlon.xplanbox.api.manager.v2.PlanDokumentApi2;
import de.latlon.xplanbox.api.manager.v2.PlanRasterbasisApi2;
import de.latlon.xplanbox.api.manager.v2.PlanTextApi2;
import de.latlon.xplanbox.api.manager.v2.PlansApi2;
import de.latlon.xplanbox.api.manager.v2.PlansLogApi2;
import de.latlon.xplanbox.api.manager.v2.ReportApi;
import de.latlon.xplanbox.api.manager.v2.StatusApi;
import de.latlon.xplanbox.security.openapi.BearerSecurityOpenApiWriter;
import io.swagger.v3.jaxrs2.integration.resources.BaseOpenApiResource;
import io.swagger.v3.oas.integration.SwaggerConfiguration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Context;
import org.slf4j.Logger;
import org.springframework.context.annotation.Configuration;

/**
 * Application configuration for XPlanManager REST API. Example mapping for proxy mapping:
 * http://xplanbox.lat-lon.de/api/v2/ -> http://host:8080/xplan-manager-api/api/v2/ Public
 * address: http://xplanbox.lat-lon.de/api/v2 Internal address:
 * http://host:8080/xplan-manager-api/api/v2
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@ApplicationPath("/api/v2")
@Configuration
public class ApiV2Config extends AbstractApiConfig {

	private static final Logger LOG = getLogger(ApiV2Config.class);

	public static final String APP_PATH = "api/v2";

	public ApiV2Config(@Context ServletContext servletContext, @Context ManagerApiConfiguration managerApiConfiguration,
			Optional<BearerSecurityOpenApiWriter> bearerSecurityOpenApiWriter) {
		super(servletContext, managerApiConfiguration, bearerSecurityOpenApiWriter);

		register(InfoApi2.class);
		register(PlanAenderungenApi2.class);
		register(PlanApi2.class);
		register(PlanBasisdatenApi2.class);
		register(PlanDokumentApi2.class);
		register(PlanRasterbasisApi2.class);
		register(PlansApi2.class);
		register(PlanTextApi2.class);
		register(StatusApi.class);
		register(ReportApi.class);
		register(PlansLogApi2.class);

		LOG.info("XPlanManagerAPI v2 successfully initialized");
	}

	@Override
	protected BaseOpenApiResource createDefaultApi(ServletContext servletContext,
			ManagerApiConfiguration managerApiConfiguration,
			Optional<BearerSecurityOpenApiWriter> bearerSecurityOpenApiWriter) {
		DefaultApi openApiResourceV1 = new DefaultApi();
		OpenAPI v1OpenApi = createOpenAPI(servletContext, managerApiConfiguration, APP_PATH,
				bearerSecurityOpenApiWriter);
		SwaggerConfiguration oasConfigV1 = new SwaggerConfiguration().openAPI(v1OpenApi)
			.filterClass(ApiV2Filter.class.getCanonicalName())
			.prettyPrint(true)
			.resourcePackages(Stream.of("de.latlon.xplanbox.api.manager.v2").collect(Collectors.toSet()));
		openApiResourceV1.setOpenApiConfiguration(oasConfigV1);
		return openApiResourceV1;
	}

	protected void addInfo(OpenAPI openApi, ManagerApiConfiguration managerApiConfiguration) {
		openApi.setInfo(new Info().title("XPlanManagerAPI")
			.version("2.1.0")
			.description("XPlanManager REST API v2")
			.termsOfService(getTermsOfService(managerApiConfiguration))
			.license(new License().name("Apache 2.0").url("http://www.apache.org/licenses/LICENSE-2.0.html")));
	}

}
