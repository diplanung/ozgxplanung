/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.manager.openapi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import de.latlon.xplanbox.api.commons.ObjectMapperContextResolver;
import de.latlon.xplanbox.api.commons.converter.StringListConverterProvider;
import de.latlon.xplanbox.api.commons.exception.ConstraintViolationExceptionMapper;
import de.latlon.xplanbox.api.commons.exception.EventExecutionExceptionMapper;
import de.latlon.xplanbox.api.commons.exception.UnsupportedContentTypeExceptionMapper;
import de.latlon.xplanbox.api.commons.exception.ValidatorExceptionMapper;
import de.latlon.xplanbox.api.commons.exception.XPlanApiExceptionMapper;
import de.latlon.xplanbox.api.manager.config.ManagerApiConfiguration;
import de.latlon.xplanbox.api.manager.exception.AmbiguousBereichNummernExceptionMapper;
import de.latlon.xplanbox.api.manager.exception.PlanNotFoundExceptionMapper;
import de.latlon.xplanbox.api.manager.exception.UnsupportedPlanExceptionMapper;
import de.latlon.xplanbox.api.manager.exception.ValidationExceptionMapper;
import de.latlon.xplanbox.security.openapi.BearerSecurityOpenApiWriter;
import io.swagger.v3.jaxrs2.integration.resources.BaseOpenApiResource;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.tags.Tag;
import jakarta.servlet.ServletContext;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

/**
 * Application configuration for XPlanManager REST API. Example mapping for proxy mapping:
 * http://xplanbox.lat-lon.de/xmanager/api/v1/ ->
 * http://host:8080/xplan-manager-api/xmanager/api/v1/ Public address:
 * http://xplanbox.lat-lon.de/xmanager/api/v1 Internal address:
 * http://host:8080/xplan-manager-api/xmanager/api/v1
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public abstract class AbstractApiConfig extends ResourceConfig {

	public AbstractApiConfig(ServletContext servletContext, ManagerApiConfiguration managerApiConfiguration,
			Optional<BearerSecurityOpenApiWriter> bearerSecurityOpenApiWriter) {
		property("jersey.config.server.response.setStatusOverSendError", true);
		property(ServerProperties.WADL_FEATURE_DISABLE, true);

		register(new ObjectMapperContextResolver());

		BaseOpenApiResource openApiResource = createDefaultApi(servletContext, managerApiConfiguration,
				bearerSecurityOpenApiWriter);
		register(openApiResource);

		register(ConstraintViolationExceptionMapper.class);
		register(ValidationExceptionMapper.class);
		register(UnsupportedContentTypeExceptionMapper.class);
		register(ValidatorExceptionMapper.class);
		register(XPlanApiExceptionMapper.class);
		register(EventExecutionExceptionMapper.class);
		register(StringListConverterProvider.class);

		register(AmbiguousBereichNummernExceptionMapper.class);
		register(PlanNotFoundExceptionMapper.class);
		register(UnsupportedPlanExceptionMapper.class);

		// packages("org.glassfish.jersey.examples.multipart");
		property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
		register(MultiPartFeature.class);
	}

	protected abstract BaseOpenApiResource createDefaultApi(ServletContext servletContext,
			ManagerApiConfiguration managerApiConfiguration,
			Optional<BearerSecurityOpenApiWriter> bearerSecurityOpenApiWriter);

	protected abstract void addInfo(OpenAPI openApi, ManagerApiConfiguration managerApiConfiguration);

	protected OpenAPI createOpenAPI(ServletContext servletContext, ManagerApiConfiguration managerApiConfiguration,
			String apiPath, Optional<BearerSecurityOpenApiWriter> bearerSecurityOpenApiWriter) {
		OpenAPI openApi = new OpenAPI();
		addInfo(openApi, managerApiConfiguration);
		addContact(openApi, managerApiConfiguration);
		addServers(openApi, servletContext, managerApiConfiguration, apiPath);
		addTags(openApi, managerApiConfiguration);
		bearerSecurityOpenApiWriter.ifPresent(apiSecurity -> apiSecurity.addSecurity(openApi));
		return openApi;
	}

	private void addTags(OpenAPI openApi, ManagerApiConfiguration managerApiConfiguration) {
		List<Tag> tags = new ArrayList<>();
		Tag manageTag = new Tag().name("manage").description("Manage XPlanGML documents");
		if (managerApiConfiguration != null && managerApiConfiguration.getDocumentationUrl() != null) {
			manageTag.externalDocs(new ExternalDocumentation().description("xPlanBox")
				.url(managerApiConfiguration.getDocumentationUrl()));
		}
		Tag searchTag = new Tag().name("search").description("Search for XPlanGML documents");
		if (managerApiConfiguration != null && managerApiConfiguration.getDocumentationUrl() != null) {
			searchTag.externalDocs(new ExternalDocumentation().description("xPlanBox")
				.url(managerApiConfiguration.getDocumentationUrl()));
		}
		tags.add(manageTag);
		tags.add(searchTag);
		openApi.tags(tags);
	}

	private void addContact(OpenAPI openApi, ManagerApiConfiguration managerApiConfiguration) {
		if (managerApiConfiguration != null && managerApiConfiguration.getContactEMailAddress() != null) {
			String contactEMailAddress = managerApiConfiguration.getContactEMailAddress();
			openApi.getInfo().setContact(new Contact().email(contactEMailAddress));
		}
	}

	private void addServers(OpenAPI openApi, ServletContext servletContext,
			ManagerApiConfiguration managerApiConfiguration, String apiPath) {
		String serverUrl = getServerUrl(servletContext, managerApiConfiguration, apiPath);
		Server server = new Server().url(serverUrl);
		openApi.servers(Collections.singletonList(server));
	}

	private String getServerUrl(ServletContext servletContext, ManagerApiConfiguration managerApiConfiguration,
			String apiPath) {
		StringBuilder serverUrl = new StringBuilder();
		if (managerApiConfiguration != null && managerApiConfiguration.getApiUrl() != null) {
			String apiEndpoint = managerApiConfiguration.getApiUrl().toString();
			serverUrl.append(apiEndpoint);
		}
		else {
			serverUrl.append(servletContext.getContextPath());
		}
		if (!serverUrl.toString().endsWith("/"))
			serverUrl.append("/");
		serverUrl.append(apiPath);
		return serverUrl.toString();
	}

	protected String getTermsOfService(ManagerApiConfiguration managerApiConfiguration) {
		if (managerApiConfiguration != null)
			return managerApiConfiguration.getTermsOfServiceUrl();
		return null;
	}

}
