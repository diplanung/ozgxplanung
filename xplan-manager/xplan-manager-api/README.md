# XPlanManagerAPI

OpenAPI v1: #host#/xplan-manager-api/xmanager/api/v1/
OpenAPI v2: #host#/xplan-manager-api/api/v2/

## Technische Endpoints

- `/xplan-manager-api/actuator/health/liveness`: liveness check
    ```
    $> curl http://localhost:8086/xplan-manager-api/actuator/health/liveness
    {"status":"UP"}
    ```

- `/xplan-manager-api/actuator/info`: build infos
    ```
    $> curl http://localhost:8086/xplan-manager-api/actuator/info
    {"build":{"gitRevision":"08e35dc18b463715bbf0feab0c4c3a2e8c8a0904","key":""}}
    ```
