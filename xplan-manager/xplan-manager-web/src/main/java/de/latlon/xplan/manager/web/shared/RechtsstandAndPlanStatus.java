/*-
 * #%L
 * xplan-manager-web - Webanwendung XPlanManagerWeb
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.web.shared;

import java.io.Serializable;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class RechtsstandAndPlanStatus implements Serializable {

	private Rechtsstand rechtsstand;

	private PlanStatus planStatus;

	public RechtsstandAndPlanStatus() {
	}

	public RechtsstandAndPlanStatus(Rechtsstand rechtsstand, PlanStatus planStatus) {
		this.rechtsstand = rechtsstand;
		this.planStatus = planStatus;
	}

	public Rechtsstand getRechtsstand() {
		return rechtsstand;
	}

	public void setRechtsstand(Rechtsstand rechtsstand) {
		this.rechtsstand = rechtsstand;
	}

	public PlanStatus getPlanStatus() {
		return planStatus;
	}

	public void setPlanStatus(PlanStatus planStatus) {
		this.planStatus = planStatus;
	}

}
