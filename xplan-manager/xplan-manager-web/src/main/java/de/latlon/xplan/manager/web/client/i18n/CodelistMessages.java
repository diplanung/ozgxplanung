/*-
 * #%L
 * xplan-manager-web - Webanwendung XPlanManagerWeb
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.web.client.i18n;

import com.google.gwt.i18n.client.Messages;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version $Revision: $, $Date: $
 */
public interface CodelistMessages extends Messages {

	String ChangeType_CHANGED_BY();

	String ChangeType_CHANGES();

	String ReferenceType_BESCHREIBUNG();

	String ReferenceType_BEGRUENDUNG();

	String ReferenceType_LEGENDE();

	String ReferenceType_RECHTSPLAN();

	String ReferenceType_PLANGRUNDLAGE();

	String ReferenceType_UMWELTBERICHT();

	String ReferenceType_SATZUNG();

	String ReferenceType_VERORDNUNG();

	String ReferenceType_KARTE();

	String ReferenceType_ERLAEUTERUNG();

	String ReferenceType_ZUSAMMENFASSENDEERKLAERUNG();

	String ReferenceType_KOORDINATENLISTE();

	String ReferenceType_GRUNDSTUECKSVERZEICHNIS();

	String ReferenceType_PFLANZLISTE();

	String ReferenceType_GRUENORDNUNGSPLAN();

	String ReferenceType_ERSCHLIESSUNGSVERTRAG();

	String ReferenceType_DURCHFUEHRUNGSVERTRAG();

	String ReferenceType_STAEDTEBAULICHERVERTRAG();

	String ReferenceType_UMWELTBEZOGENESTELLUNGNAHMEN();

	String ReferenceType_BESCHLUSS();

	String ReferenceType_VORHABENUNDERSCHLIESSUNGSPLAN();

	String ReferenceType_METADATENPLAN();

	String ReferenceType_GENEHMIGUNG();

	String ReferenceType_BEKANNTMACHUNG();

	String ReferenceType_SCHUTZGEBIETSVERORDNUNG();

	String ReferenceType_RECHTSVERBINDLICH();

	String ReferenceType_INFORMELL();

	String RasterReferenceType_LEGEND();

	String RasterReferenceType_SCAN();

	String RasterReferenceType_TEXT();

	String TextAbschnitt_BP_Festsetzung();

	String TextAbschnitt_BP_Hinweis();

	String TextAbschnitt_BP_Kennzeichnung();

	String TextAbschnitt_BP_NachrichtlicheUebernahme();

	String TextAbschnitt_BP_Vermerk();

	String TextAbschnitt_BP_Unbekannt();

	String TextAbschnitt_FP_Darstellung();

	String TextAbschnitt_FP_NachrichtlicheUebernahme();

	String TextAbschnitt_FP_Hinweis();

	String TextAbschnitt_FP_Vermerk();

	String TextAbschnitt_FP_Kennzeichnung();

	String TextAbschnitt_FP_Unbekannt();

	String TextAbschnitt_LP_Festsetzung();

	String TextAbschnitt_LP_Geplant();

	String TextAbschnitt_LP_NachrichtlicheUebernahme();

	String TextAbschnitt_LP_DarstellungKennzeichnung();

	String TextAbschnitt_LP_FestsetzungInBPlan();

	String TextAbschnitt_LP_Unbekannt();

	String TextAbschnitt_LP_SonstigerStatus();

	String TextAbschnitt_RP_ZielDerRaumordnung();

	String TextAbschnitt_RP_GrundsatzDerRaumordnung();

	String TextAbschnitt_RP_NachrichtlicheUebernahme();

	String TextAbschnitt_RP_NachrichtlicheUebernahmeZiel();

	String TextAbschnitt_RP_NachrichtlicheUebernahmeGrundsatz();

	String TextAbschnitt_RP_NurInformatinsGehalt();

	String TextAbschnitt_RP_TextlichesZiel();

	String TextAbschnitt_RP_ZielUndGrundsatz();

	String TextAbschnitt_RP_Vorschlag();

	String TextAbschnitt_RP_Unbekannt();

	String TextAbschnitt_SO_FestsetzungBPlan();

	String TextAbschnitt_SO_DarstellungFPlan();

	String TextAbschnitt_SO_InhaltLPlan();

	String TextAbschnitt_SO_NachrichtlicheUebernahme();

	String TextAbschnitt_SO_Hinweis();

	String TextAbschnitt_SO_Vermerk();

	String TextAbschnitt_SO_Kennzeichnung();

	String TextAbschnitt_SO_Unbekannt();

	String TextAbschnitt_SO_Sonstiges();

	String TextAbschnitt_XP_FestsetzungBPlan();

	String TextAbschnitt_XP_NachrichtlicheUebernahme();

	String TextAbschnitt_XP_DarstellungFPlan();

	String TextAbschnitt_XP_ZielDerRaumordnung();

	String TextAbschnitt_XP_GrundsatzDerRaumordnung();

	String TextAbschnitt_XP_NachrichtlicheUebernahmeZiel();

	String TextAbschnitt_XP_NachrichtlicheUebernahmeGrundsatz();

	String TextAbschnitt_XP_NurInformatinsGehalt();

	String TextAbschnitt_XP_TextlichesZielRaumordnung();

	String TextAbschnitt_XP_ZielUndGrundsatzDerRaumordnung();

	String TextAbschnitt_XP_VorschlagRaumordnung();

	String TextAbschnitt_XP_FestsetzungImLP();

	String TextAbschnitt_XP_GeplanteFestsetzungImLP();

	String TextAbschnitt_XP_DarstellungKennzeichnungImLP();

	String TextAbschnitt_XP_LandschaftsplanungsInhaltZurBeruecksichtigung();

	String TextAbschnitt_XP_Hinweis();

	String TextAbschnitt_XP_Kennzeichnung();

	String TextAbschnitt_XP_Vermerk();

	String TextAbschnitt_XP_Unbekannt();

	String TextAbschnitt_XP_Sonstiges();

	String XP_ExterneReferenzArt_Dokument();

	String XP_ExterneReferenzArt_PlanMitGeoreferenz();

	String XPLAN_41_BP_PlanArt_1000_BPlan();

	String XPLAN_41_BP_PlanArt_10000_EinfacherBPlan();

	String XPLAN_41_BP_PlanArt_10001_QualifizierterBPlan();

	String XPLAN_41_BP_PlanArt_3000_VorhabenbezogenerBPlan();

	String XPLAN_41_BP_PlanArt_4000_InnenbereichsSatzung();

	String XPLAN_41_BP_PlanArt_40000_KlarstellungsSatzung();

	String XPLAN_41_BP_PlanArt_40001_EntwicklungsSatzung();

	String XPLAN_41_BP_PlanArt_40002_ErgaenzungsSatzung();

	String XPLAN_41_BP_PlanArt_5000_AussenbereichsSatzung();

	String XPLAN_41_BP_PlanArt_7000_OertlicheBauvorschrift();

	String XPLAN_41_BP_PlanArt_9999_Sonstiges();

	String XPLAN_41_BP_Rechtsstand_1000_Aufstellungsbeschluss();

	String XPLAN_41_BP_Rechtsstand_2000_Entwurf();

	String XPLAN_41_BP_Rechtsstand_2100_FruehzeitigeBehoerdenBeteiligung();

	String XPLAN_41_BP_Rechtsstand_2200_FruehzeitigeOeffentlichkeitsBeteiligung();

	String XPLAN_41_BP_Rechtsstand_2300_BehoerdenBeteiligung();

	String XPLAN_41_BP_Rechtsstand_2400_OeffentlicheAuslegung();

	String XPLAN_41_BP_Rechtsstand_3000_Satzung();

	String XPLAN_41_BP_Rechtsstand_4000_InkraftGetreten();

	String XPLAN_41_BP_Rechtsstand_4500_TeilweiseUntergegangen();

	String XPLAN_41_BP_Rechtsstand_5000_Untergegangen();

	String XPLAN_41_BP_Verfahren_1000_Normal();

	String XPLAN_41_BP_Verfahren_2000_Parag13();

	String XPLAN_41_BP_Verfahren_3000_Parag13a();

	String XPLAN_41_XP_RechtscharakterPlanaenderung_1000_Aenderung();

	String XPLAN_41_XP_RechtscharakterPlanaenderung_1100_Ergaenzung();

	String XPLAN_41_XP_RechtscharakterPlanaenderung_2000_Aufhebung();

	String XPLAN_50_BP_PlanArt_1000_BPlan();

	String XPLAN_50_BP_PlanArt_10000_EinfacherBPlan();

	String XPLAN_50_BP_PlanArt_10001_QualifizierterBPlan();

	String XPLAN_50_BP_PlanArt_3000_VorhabenbezogenerBPlan();

	String XPLAN_50_BP_PlanArt_4000_InnenbereichsSatzung();

	String XPLAN_50_BP_PlanArt_40000_KlarstellungsSatzung();

	String XPLAN_50_BP_PlanArt_40001_EntwicklungsSatzung();

	String XPLAN_50_BP_PlanArt_40002_ErgaenzungsSatzung();

	String XPLAN_50_BP_PlanArt_5000_AussenbereichsSatzung();

	String XPLAN_50_BP_PlanArt_7000_OertlicheBauvorschrift();

	String XPLAN_50_BP_PlanArt_9999_Sonstiges();

	String XPLAN_50_BP_Rechtsstand_1000_Aufstellungsbeschluss();

	String XPLAN_50_BP_Rechtsstand_2000_Entwurf();

	String XPLAN_50_BP_Rechtsstand_2100_FruehzeitigeBehoerdenBeteiligung();

	String XPLAN_50_BP_Rechtsstand_2200_FruehzeitigeOeffentlichkeitsBeteiligung();

	String XPLAN_50_BP_Rechtsstand_2300_BehoerdenBeteiligung();

	String XPLAN_50_BP_Rechtsstand_2400_OeffentlicheAuslegung();

	String XPLAN_50_BP_Rechtsstand_3000_Satzung();

	String XPLAN_50_BP_Rechtsstand_4000_InkraftGetreten();

	String XPLAN_50_BP_Rechtsstand_4500_TeilweiseUntergegangen();

	String XPLAN_50_BP_Rechtsstand_5000_Untergegangen();

	String XPLAN_50_BP_Verfahren_1000_Normal();

	String XPLAN_50_BP_Verfahren_2000_Parag13();

	String XPLAN_50_BP_Verfahren_3000_Parag13a();

	String XPLAN_50_XP_RechtscharakterPlanaenderung_1000_Aenderung();

	String XPLAN_50_XP_RechtscharakterPlanaenderung_1100_Ergaenzung();

	String XPLAN_50_XP_RechtscharakterPlanaenderung_2000_Aufhebung();

	String XPLAN_51_BP_PlanArt_1000_BPlan();

	String XPLAN_51_BP_PlanArt_10000_EinfacherBPlan();

	String XPLAN_51_BP_PlanArt_10001_QualifizierterBPlan();

	String XPLAN_51_BP_PlanArt_3000_VorhabenbezogenerBPlan();

	String XPLAN_51_BP_PlanArt_4000_InnenbereichsSatzung();

	String XPLAN_51_BP_PlanArt_40000_KlarstellungsSatzung();

	String XPLAN_51_BP_PlanArt_40001_EntwicklungsSatzung();

	String XPLAN_51_BP_PlanArt_40002_ErgaenzungsSatzung();

	String XPLAN_51_BP_PlanArt_5000_AussenbereichsSatzung();

	String XPLAN_51_BP_PlanArt_7000_OertlicheBauvorschrift();

	String XPLAN_51_BP_PlanArt_9999_Sonstiges();

	String XPLAN_51_BP_Rechtsstand_1000_Aufstellungsbeschluss();

	String XPLAN_51_BP_Rechtsstand_2000_Entwurf();

	String XPLAN_51_BP_Rechtsstand_2100_FruehzeitigeBehoerdenBeteiligung();

	String XPLAN_51_BP_Rechtsstand_2200_FruehzeitigeOeffentlichkeitsBeteiligung();

	String XPLAN_51_BP_Rechtsstand_2300_BehoerdenBeteiligung();

	String XPLAN_51_BP_Rechtsstand_2400_OeffentlicheAuslegung();

	String XPLAN_51_BP_Rechtsstand_3000_Satzung();

	String XPLAN_51_BP_Rechtsstand_4000_InkraftGetreten();

	String XPLAN_51_BP_Rechtsstand_4500_TeilweiseUntergegangen();

	String XPLAN_51_BP_Rechtsstand_5000_Untergegangen();

	String XPLAN_51_BP_Verfahren_1000_Normal();

	String XPLAN_51_BP_Verfahren_2000_Parag13();

	String XPLAN_51_BP_Verfahren_3000_Parag13a();

	String XPLAN_51_BP_Verfahren_4000_Parag13b();

	String XPLAN_51_XP_RechtscharakterPlanaenderung_1000_Aenderung();

	String XPLAN_51_XP_RechtscharakterPlanaenderung_1100_Ergaenzung();

	String XPLAN_51_XP_RechtscharakterPlanaenderung_2000_Aufhebung();

	String XPLAN_52_BP_PlanArt_1000_BPlan();

	String XPLAN_52_BP_PlanArt_10000_EinfacherBPlan();

	String XPLAN_52_BP_PlanArt_10001_QualifizierterBPlan();

	String XPLAN_52_BP_PlanArt_3000_VorhabenbezogenerBPlan();

	String XPLAN_52_BP_PlanArt_4000_InnenbereichsSatzung();

	String XPLAN_52_BP_PlanArt_40000_KlarstellungsSatzung();

	String XPLAN_52_BP_PlanArt_40001_EntwicklungsSatzung();

	String XPLAN_52_BP_PlanArt_40002_ErgaenzungsSatzung();

	String XPLAN_52_BP_PlanArt_5000_AussenbereichsSatzung();

	String XPLAN_52_BP_PlanArt_7000_OertlicheBauvorschrift();

	String XPLAN_52_BP_PlanArt_9999_Sonstiges();

	String XPLAN_52_BP_Rechtsstand_1000_Aufstellungsbeschluss();

	String XPLAN_52_BP_Rechtsstand_2000_Entwurf();

	String XPLAN_52_BP_Rechtsstand_2100_FruehzeitigeBehoerdenBeteiligung();

	String XPLAN_52_BP_Rechtsstand_2200_FruehzeitigeOeffentlichkeitsBeteiligung();

	String XPLAN_52_BP_Rechtsstand_2300_BehoerdenBeteiligung();

	String XPLAN_52_BP_Rechtsstand_2400_OeffentlicheAuslegung();

	String XPLAN_52_BP_Rechtsstand_3000_Satzung();

	String XPLAN_52_BP_Rechtsstand_4000_InkraftGetreten();

	String XPLAN_52_BP_Rechtsstand_4500_TeilweiseUntergegangen();

	String XPLAN_52_BP_Rechtsstand_5000_Untergegangen();

	String XPLAN_52_BP_Verfahren_1000_Normal();

	String XPLAN_52_BP_Verfahren_2000_Parag13();

	String XPLAN_52_BP_Verfahren_3000_Parag13a();

	String XPLAN_52_BP_Verfahren_4000_Parag13b();

	String XPLAN_52_XP_RechtscharakterPlanaenderung_1000_Aenderung();

	String XPLAN_52_XP_RechtscharakterPlanaenderung_1100_Ergaenzung();

	String XPLAN_52_XP_RechtscharakterPlanaenderung_2000_Aufhebung();

	String XPLAN_53_BP_PlanArt_1000_BPlan();

	String XPLAN_53_BP_PlanArt_10000_EinfacherBPlan();

	String XPLAN_53_BP_PlanArt_10001_QualifizierterBPlan();

	String XPLAN_53_BP_PlanArt_3000_VorhabenbezogenerBPlan();

	String XPLAN_53_BP_PlanArt_3001_VorhabenUndErschliessungsplan();

	String XPLAN_53_BP_PlanArt_4000_InnenbereichsSatzung();

	String XPLAN_53_BP_PlanArt_40000_KlarstellungsSatzung();

	String XPLAN_53_BP_PlanArt_40001_EntwicklungsSatzung();

	String XPLAN_53_BP_PlanArt_40002_ErgaenzungsSatzung();

	String XPLAN_53_BP_PlanArt_5000_AussenbereichsSatzung();

	String XPLAN_53_BP_PlanArt_7000_OertlicheBauvorschrift();

	String XPLAN_53_BP_PlanArt_9999_Sonstiges();

	String XPLAN_53_BP_Rechtsstand_1000_Aufstellungsbeschluss();

	String XPLAN_53_BP_Rechtsstand_2000_Entwurf();

	String XPLAN_53_BP_Rechtsstand_2100_FruehzeitigeBehoerdenBeteiligung();

	String XPLAN_53_BP_Rechtsstand_2200_FruehzeitigeOeffentlichkeitsBeteiligung();

	String XPLAN_53_BP_Rechtsstand_2300_BehoerdenBeteiligung();

	String XPLAN_53_BP_Rechtsstand_2400_OeffentlicheAuslegung();

	String XPLAN_53_BP_Rechtsstand_3000_Satzung();

	String XPLAN_53_BP_Rechtsstand_4000_InkraftGetreten();

	String XPLAN_53_BP_Rechtsstand_4500_TeilweiseUntergegangen();

	String XPLAN_53_BP_Rechtsstand_5000_Untergegangen();

	String XPLAN_53_BP_Rechtsstand_50000_Aufgehoben();

	String XPLAN_53_BP_Rechtsstand_50001_AusserKraft();

	String XPLAN_53_BP_Verfahren_1000_Normal();

	String XPLAN_53_BP_Verfahren_2000_Parag13();

	String XPLAN_53_BP_Verfahren_3000_Parag13a();

	String XPLAN_53_BP_Verfahren_4000_Parag13b();

	String XPLAN_53_XP_RechtscharakterPlanaenderung_1000_Aenderung();

	String XPLAN_53_XP_RechtscharakterPlanaenderung_1100_Ergaenzung();

	String XPLAN_53_XP_RechtscharakterPlanaenderung_2000_Aufhebung();

	String XPLAN_53_XP_RechtscharakterPlanaenderung_20000_Aufhebungsverfahren();

	String XPLAN_53_XP_RechtscharakterPlanaenderung_20001_Ueberplanung();

	String XPLAN_54_BP_PlanArt_1000_BPlan();

	String XPLAN_54_BP_PlanArt_10000_EinfacherBPlan();

	String XPLAN_54_BP_PlanArt_10001_QualifizierterBPlan();

	String XPLAN_54_BP_PlanArt_10002_BebauungsplanZurWohnraumversorgung();

	String XPLAN_54_BP_PlanArt_3000_VorhabenbezogenerBPlan();

	String XPLAN_54_BP_PlanArt_3001_VorhabenUndErschliessungsplan();

	String XPLAN_54_BP_PlanArt_4000_InnenbereichsSatzung();

	String XPLAN_54_BP_PlanArt_40000_KlarstellungsSatzung();

	String XPLAN_54_BP_PlanArt_40001_EntwicklungsSatzung();

	String XPLAN_54_BP_PlanArt_40002_ErgaenzungsSatzung();

	String XPLAN_54_BP_PlanArt_5000_AussenbereichsSatzung();

	String XPLAN_54_BP_PlanArt_7000_OertlicheBauvorschrift();

	String XPLAN_54_BP_PlanArt_9999_Sonstiges();

	String XPLAN_54_BP_Rechtsstand_1000_Aufstellungsbeschluss();

	String XPLAN_54_BP_Rechtsstand_2000_Entwurf();

	String XPLAN_54_BP_Rechtsstand_2100_FruehzeitigeBehoerdenBeteiligung();

	String XPLAN_54_BP_Rechtsstand_2200_FruehzeitigeOeffentlichkeitsBeteiligung();

	String XPLAN_54_BP_Rechtsstand_2300_BehoerdenBeteiligung();

	String XPLAN_54_BP_Rechtsstand_2400_OeffentlicheAuslegung();

	String XPLAN_54_BP_Rechtsstand_3000_Satzung();

	String XPLAN_54_BP_Rechtsstand_4000_InkraftGetreten();

	String XPLAN_54_BP_Rechtsstand_4500_TeilweiseUntergegangen();

	String XPLAN_54_BP_Rechtsstand_5000_Untergegangen();

	String XPLAN_54_BP_Rechtsstand_50000_Aufgehoben();

	String XPLAN_54_BP_Rechtsstand_50001_AusserKraft();

	String XPLAN_54_BP_Verfahren_1000_Normal();

	String XPLAN_54_BP_Verfahren_2000_Parag13();

	String XPLAN_54_BP_Verfahren_3000_Parag13a();

	String XPLAN_54_BP_Verfahren_4000_Parag13b();

	String XPLAN_54_XP_RechtscharakterPlanaenderung_1000_Aenderung();

	String XPLAN_54_XP_RechtscharakterPlanaenderung_1100_Ergaenzung();

	String XPLAN_54_XP_RechtscharakterPlanaenderung_2000_Aufhebung();

	String XPLAN_54_XP_RechtscharakterPlanaenderung_20000_Aufhebungsverfahren();

	String XPLAN_54_XP_RechtscharakterPlanaenderung_20001_Ueberplanung();

	String XPLAN_60_BP_PlanArt_1000_BPlan();

	String XPLAN_60_BP_PlanArt_10000_EinfacherBPlan();

	String XPLAN_60_BP_PlanArt_10001_QualifizierterBPlan();

	String XPLAN_60_BP_PlanArt_10002_BebauungsplanZurWohnraumversorgung();

	String XPLAN_60_BP_PlanArt_3000_VorhabenbezogenerBPlan();

	String XPLAN_60_BP_PlanArt_3001_VorhabenUndErschliessungsplan();

	String XPLAN_60_BP_PlanArt_4000_InnenbereichsSatzung();

	String XPLAN_60_BP_PlanArt_40000_KlarstellungsSatzung();

	String XPLAN_60_BP_PlanArt_40001_EntwicklungsSatzung();

	String XPLAN_60_BP_PlanArt_40002_ErgaenzungsSatzung();

	String XPLAN_60_BP_PlanArt_5000_AussenbereichsSatzung();

	String XPLAN_60_BP_PlanArt_7000_OertlicheBauvorschrift();

	String XPLAN_60_BP_PlanArt_9999_Sonstiges();

	String XPLAN_60_BP_Rechtsstand_1000_Aufstellungsbeschluss();

	String XPLAN_60_BP_Rechtsstand_2000_Entwurf();

	String XPLAN_60_BP_Rechtsstand_2100_FruehzeitigeBehoerdenBeteiligung();

	String XPLAN_60_BP_Rechtsstand_2200_FruehzeitigeOeffentlichkeitsBeteiligung();

	String XPLAN_60_BP_Rechtsstand_2250_Entwurfsbeschluss();

	String XPLAN_60_BP_Rechtsstand_2300_BehoerdenBeteiligung();

	String XPLAN_60_BP_Rechtsstand_2400_OeffentlicheAuslegung();

	String XPLAN_60_BP_Rechtsstand_3000_Satzung();

	String XPLAN_60_BP_Rechtsstand_4000_InkraftGetreten();

	String XPLAN_60_BP_Rechtsstand_4500_TeilweiseUntergegangen();

	String XPLAN_60_BP_Rechtsstand_45000_TeilweiseAufgehoben();

	String XPLAN_60_BP_Rechtsstand_45001_TeilweiseAusserKraft();

	String XPLAN_60_BP_Rechtsstand_5000_Untergegangen();

	String XPLAN_60_BP_Rechtsstand_50000_Aufgehoben();

	String XPLAN_60_BP_Rechtsstand_50001_AusserKraft();

	String XPLAN_60_BP_Verfahren_1000_Normal();

	String XPLAN_60_BP_Verfahren_2000_Parag13();

	String XPLAN_60_BP_Verfahren_3000_Parag13a();

	String XPLAN_60_BP_Verfahren_4000_Parag13b();

	String XPLAN_60_XP_RechtscharakterPlanaenderung_1000_Aenderung();

	String XPLAN_60_XP_RechtscharakterPlanaenderung_10000_Ersetzung();

	String XPLAN_60_XP_RechtscharakterPlanaenderung_10001_Ergaenzung();

	String XPLAN_60_XP_RechtscharakterPlanaenderung_10002_Streichung();

	String XPLAN_60_XP_RechtscharakterPlanaenderung_2000_Aufhebung();

	String XPLAN_60_XP_RechtscharakterPlanaenderung_3000_Ueberplanung();

	String XPLAN_FP_PlanArt_1000_FPlan();

	String XPLAN_FP_PlanArt_2000_GemeinsamerFPlan();

	String XPLAN_FP_PlanArt_3000_RegFPlan();

	String XPLAN_FP_PlanArt_4000_FPlanRegPlan();

	String XPLAN_FP_PlanArt_5000_SachlicherTeilplan();

	String XPLAN_FP_PlanArt_9999_Sonstiges();

	String XPLAN_FP_Verfahren_1000_Normal();

	String XPLAN_FP_Verfahren_2000_Parag13();

	String XPLAN_FP_Rechtsstand_1000_Aufstellungsbeschluss();

	String XPLAN_FP_Rechtsstand_2000_Entwurf();

	String XPLAN_FP_Rechtsstand_2100_FruehzeitigeBehoerdenBeteiligung();

	String XPLAN_FP_Rechtsstand_2200_FruehzeitigeOeffentlichkeitsBeteiligung();

	String XPLAN_FP_Rechtsstand_2300_BehoerdenBeteiligung();

	String XPLAN_FP_Rechtsstand_2400_OeffentlicheAuslegung();

	String XPLAN_FP_Rechtsstand_3000_Plan();

	String XPLAN_FP_Rechtsstand_4000_Wirksamkeit();

	String XPLAN_FP_Rechtsstand_5000_Untergegangen();

	String XPLAN_FP_Rechtsstand_50000_Aufgehoben();

	String XPLAN_FP_Rechtsstand_50001_AusserKraft();

	String XPLAN_60_FP_Rechtsstand_1000_Aufstellungsbeschluss();

	String XPLAN_60_FP_Rechtsstand_2000_ImVerfahren();

	String XPLAN_60_FP_Rechtsstand_2100_FruehzeitigeBehoerdenBeteiligung();

	String XPLAN_60_FP_Rechtsstand_2200_FruehzeitigeOeffentlichkeitsBeteiligung();

	String XPLAN_60_FP_Rechtsstand_2250_Entwurfsbeschluss();

	String XPLAN_60_FP_Rechtsstand_2300_BehoerdenBeteiligung();

	String XPLAN_60_FP_Rechtsstand_2400_OeffentlicheAuslegung();

	String XPLAN_60_FP_Rechtsstand_3000_Plan();

	String XPLAN_60_FP_Rechtsstand_4000_Wirksamkeit();

	String XPLAN_60_FP_Rechtsstand_5000_Untergegangen();

	String XPLAN_60_FP_Rechtsstand_50000_Aufgehoben();

	String XPLAN_60_FP_Rechtsstand_50001_AusserKraft();

	String XPLAN_RP_PlanArt_1000_Regionalplan();

	String XPLAN_RP_PlanArt_2000_SachlicherTeilplanRegionalebene();

	String XPLAN_RP_PlanArt_2001_SachlicherTeilplanLandesebene();

	String XPLAN_RP_PlanArt_3000_Braunkohlenplan();

	String XPLAN_RP_PlanArt_4000_LandesweiterRaumordnungsplan();

	String XPLAN_RP_PlanArt_5000_StandortkonzeptBund();

	String XPLAN_RP_PlanArt_5001_AWZPlan();

	String XPLAN_RP_PlanArt_5000_RaeumlicherTeilplan();

	String XPLAN_RP_PlanArt_9999_Sonstiges();

	String XPLAN_RP_Rechtsstand_1000_Aufstellungsbeschluss();

	String XPLAN_RP_Rechtsstand_2000_Entwurf();

	String XPLAN_RP_Rechtsstand_2001_EntwurfGenehmigt();

	String XPLAN_RP_Rechtsstand_2002_EntwurfGeaendert();

	String XPLAN_RP_Rechtsstand_2003_EntwurfAufgegeben();

	String XPLAN_RP_Rechtsstand_2004_EntwurfRuht();

	String XPLAN_RP_Rechtsstand_3000_Plan();

	String XPLAN_RP_Rechtsstand_4000_Inkraftgetreten();

	String XPLAN_RP_Rechtsstand_5000_AllgemeinePlanungsabsicht();

	String XPLAN_RP_Rechtsstand_5500_TeilweiseAusserKraft();

	String XPLAN_RP_Rechtsstand_6000_AusserKraft();

	String XPLAN_RP_Rechtsstand_7000_PlanUngueltig();

	String XPLAN_RP_Verfahren_1000_Aenderung();

	String XPLAN_RP_Verfahren_2000_Teilfortschreibung();

	String XPLAN_RP_Verfahren_3000_Neuaufstellung();

	String XPLAN_RP_Verfahren_4000_Gesamtfortschreibung();

	String XPLAN_RP_Verfahren_5000_Aktualisierung();

	String XPLAN_RP_Verfahren_6000_Neubekanntmachung();

	String XPLAN_LP_PlanArt_1000_Landschaftsprogramm();

	String XPLAN_LP_PlanArt_2000_Landschaftsrahmenplan();

	String XPLAN_LP_PlanArt_3000_Landschaftsplan();

	String XPLAN_LP_PlanArt_4000_Gruenordnungsplan();

	String XPLAN_LP_PlanArt_9999_Sonstiges();

	String XPLAN_LP_Rechtsstand_1000_Aufstellungsbeschluss();

	String XPLAN_LP_Rechtsstand_2000_Entwurf();

	String XPLAN_LP_Rechtsstand_3000_Plan();

	String XPLAN_LP_Rechtsstand_4000_Wirksamkeit();

	String XPLAN_LP_Rechtsstand_5000_Untergegangen();

	String XPLAN_LP_Rechtsstand_6000_InFortschreibung();

}
