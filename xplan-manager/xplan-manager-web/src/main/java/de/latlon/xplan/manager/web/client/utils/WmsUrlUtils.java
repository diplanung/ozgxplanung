/*-
 * #%L
 * xplan-manager-web - Webanwendung XPlanManagerWeb
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.web.client.utils;

import static de.latlon.xplan.manager.web.shared.PlanStatus.ARCHIVIERT;
import static de.latlon.xplan.manager.web.shared.PlanStatus.IN_AUFSTELLUNG;

import de.latlon.xplan.manager.web.shared.MapPreviewConfiguration;
import de.latlon.xplan.manager.web.shared.PlanStatus;
import org.gwtopenmaps.openlayers.client.Bounds;

/**
 * Contains some useful methods to create WMS urls.
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version $Revision: $, $Date: $
 */
public final class WmsUrlUtils {

	private WmsUrlUtils() {
	}

	/**
	 * Determines the correct XPlanWMS url:
	 * <ul>
	 * <li>/services/wms</li>
	 * <li>/services/wmspre</li>
	 * <li>/services/wmsarchive</li>
	 * </ul>
	 * @param planStatus status of the plan, may be <code>null</code> (means
	 * PlanStatus.FESTGESTELLT)
	 * @param configuration never <code>null</code>
	 * @return the url of the wms (ending with a '?'), never <code>null</code>
	 */
	public static String determineWmsUrl(PlanStatus planStatus, MapPreviewConfiguration configuration) {
		String xplanWmsUrl = retrieveXPlanWmsUrl(configuration);
		String endpointToAdd = detectEndpointToAdd(planStatus);
		return xplanWmsUrl + endpointToAdd + "?";
	}

	/**
	 * Determines the correct XPlanWerkWMS url:
	 * <ul>
	 * <li>/services/planwerkwms/planname/{planName}</li>
	 * <li>/services/planwerkwmspre/planname/{planName}</li>
	 * <li>/services/planwerkwmsarchive/planname/{planName}</li>
	 * </ul>
	 * @param planStatus status of the plan, may be <code>null</code> (means
	 * PlanStatus.FESTGESTELLT)
	 * @param configuration never <code>null</code>
	 * @return the url of the wms (ending with a '?'), never <code>null</code>
	 */
	public static String createPlanwerkWmsUrl(String planname, MapPreviewConfiguration configuration,
			PlanStatus planStatus) {
		String xplanWmsUrl = retrieveXPlanWmsUrl(configuration);
		String planwerkWmsPath = planwerkWmsPath(planStatus);
		return xplanWmsUrl + planwerkWmsPath + "/planname/" + planname
				+ "?REQUEST=GetCapabilities&SERVICE=WMS&VERSION=1.3.0";
	}

	private static String retrieveXPlanWmsUrl(MapPreviewConfiguration configuration) {
		String xplanWmsUrl = configuration.getXPlanWmsUrl();
		if (!xplanWmsUrl.endsWith("/"))
			xplanWmsUrl = xplanWmsUrl + "/";
		return xplanWmsUrl + "services/";
	}

	public static String createUrl(final MapPreviewConfiguration configuration, final String planType,
			final PlanStatus planStatus, final Bounds bounds, String width, String height) {
		StringBuilder urlBuilder = new StringBuilder();
		urlBuilder.append(determineWmsUrl(planStatus, configuration));
		urlBuilder.append("SERVICE=").append("WMS");
		urlBuilder.append("&VERSION=").append("1.1.1");
		urlBuilder.append("&REQUEST=").append("GetMap");
		urlBuilder.append("&LAYERS=").append(createLayerValue(planType));
		urlBuilder.append("&STYLES=").append("");
		urlBuilder.append("&FORMAT=").append("image/png");
		urlBuilder.append("&TRANSPARENT=").append("true");
		urlBuilder.append("&EXCEPTIONS=").append("application/vnd.ogc.se_inimage");
		urlBuilder.append("&SRS=").append(configuration.getMapExtent().getCrs());
		urlBuilder.append("&BBOX=").append(createBboxValue(bounds));
		urlBuilder.append("&WIDTH=").append(width);
		urlBuilder.append("&HEIGHT=").append(height);
		return urlBuilder.toString();
	}

	private static String planwerkWmsPath(PlanStatus planStatus) {
		switch (planStatus) {
			case ARCHIVIERT:
				return "planwerkwmsarchive";
			case IN_AUFSTELLUNG:
				return "planwerkwmspre";
			default:
				return "planwerkwms";
		}
	}

	private static String detectEndpointToAdd(PlanStatus planStatus) {
		if (IN_AUFSTELLUNG.equals(planStatus))
			return "wmspre";
		if (ARCHIVIERT.equals(planStatus))
			return "wmsarchive";
		return "wms";
	}

	private static String createLayerValue(String planType) {
		if ("bp_plan".equals(planType.toLowerCase()))
			return "bp_objekte,bp_raster";
		if ("fp_plan".equals(planType.toLowerCase()))
			return "fp_objekte,fp_raster";
		if ("lp_plan".equals(planType.toLowerCase()))
			return "lp_objekte,lp_raster";
		if ("rp_plan".equals(planType.toLowerCase()))
			return "rp_objekte,rp_raster";
		if ("so_plan".equals(planType.toLowerCase()))
			return "so_objekte,so_raster";
		return null;
	}

	private static String createBboxValue(final Bounds bounds) {
		StringBuilder bboxBuilder = new StringBuilder();
		bboxBuilder.append(bounds.getLowerLeftX());
		bboxBuilder.append(",");
		bboxBuilder.append(bounds.getLowerLeftY());
		bboxBuilder.append(",");
		bboxBuilder.append(bounds.getUpperRightX());
		bboxBuilder.append(",");
		bboxBuilder.append(bounds.getUpperRightY());
		return bboxBuilder.toString();
	}

}
