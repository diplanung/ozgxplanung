/*-
 * #%L
 * xplan-manager-web - Webanwendung XPlanManagerWeb
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.web.client.utils;

import static de.latlon.xplan.manager.web.shared.PlanStatus.ARCHIVIERT;
import static de.latlon.xplan.manager.web.shared.PlanStatus.FESTGESTELLT;
import static de.latlon.xplan.manager.web.shared.PlanStatus.IN_AUFSTELLUNG;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import de.latlon.xplan.manager.web.shared.MapPreviewConfiguration;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version $Revision: $, $Date: $
 */
class WmsUrlUtilsTest {

	@Test
	void testDetermineWmsUrl_PlanstatusPre() {
		String xplanWmsUrl = "http://xplan-services/xplan-wms";
		String wmsUrl = WmsUrlUtils.determineWmsUrl(IN_AUFSTELLUNG, mockConfiguration(xplanWmsUrl));

		assertEquals("http://xplan-services/xplan-wms/services/wmspre?", wmsUrl);
	}

	@Test
	void testDetermineWmsUrlWithTrailingSlash_PlanstatusPre() {
		String xplanWmsUrl = "http://xplan-services/xplan-wms/";
		String wmsUrl = WmsUrlUtils.determineWmsUrl(IN_AUFSTELLUNG, mockConfiguration(xplanWmsUrl));

		assertEquals("http://xplan-services/xplan-wms/services/wmspre?", wmsUrl);
	}

	@Test
	void testDetermineWmsUrl_PlanstatusFestgestellt() {
		String xplanWmsUrl = "http://xplan-services/xplan-wms";
		String wmsUrl = WmsUrlUtils.determineWmsUrl(FESTGESTELLT, mockConfiguration(xplanWmsUrl));

		assertEquals("http://xplan-services/xplan-wms/services/wms?", wmsUrl);
	}

	@Test
	void testDetermineWmsUrlWithTrailingSlash_PlanstatusFestgestellt() {
		String xplanWmsUrl = "http://xplan-services/xplan-wms/";
		String wmsUrl = WmsUrlUtils.determineWmsUrl(FESTGESTELLT, mockConfiguration(xplanWmsUrl));

		assertEquals("http://xplan-services/xplan-wms/services/wms?", wmsUrl);
	}

	@Test
	void testDetermineWmsUrl_PlanstatusArchiviert() {
		String xplanWmsUrl = "http://xplan-services/xplan-wms";
		String wmsUrl = WmsUrlUtils.determineWmsUrl(ARCHIVIERT, mockConfiguration(xplanWmsUrl));

		assertEquals("http://xplan-services/xplan-wms/services/wmsarchive?", wmsUrl);
	}

	@Test
	void testDetermineWmsUrlWithTrailingSlash_PlanstatusArchiviert() {
		String xplanWmsUrl = "http://xplan-services/xplan-wms/";
		String wmsUrl = WmsUrlUtils.determineWmsUrl(ARCHIVIERT, mockConfiguration(xplanWmsUrl));

		assertEquals("http://xplan-services/xplan-wms/services/wmsarchive?", wmsUrl);
	}

	@Test
	void testDetermineWmsUrl_PlanstatusNull() {
		String xplanWmsUrl = "http://xplan-services/xplan-wms";
		String wmsUrl = WmsUrlUtils.determineWmsUrl(null, mockConfiguration(xplanWmsUrl));

		assertEquals("http://xplan-services/xplan-wms/services/wms?", wmsUrl);
	}

	@Test
	void testDetermineWmsUrlWithTrailingSlash_PlanstatusNull() {
		String xplanWmsUrl = "http://xplan-services/xplan-wms/";
		String wmsUrl = WmsUrlUtils.determineWmsUrl(null, mockConfiguration(xplanWmsUrl));

		assertEquals("http://xplan-services/xplan-wms/services/wms?", wmsUrl);
	}

	@Test
	void testCreatePlanwerkWmsUrl() {
		String xplanWmsUrl = "http://xplan-services/xplan-wms";
		String planwerkWmsUrl = WmsUrlUtils.createPlanwerkWmsUrl("PlanName10", mockConfiguration(xplanWmsUrl),
				ARCHIVIERT);

		assertEquals(
				"http://xplan-services/xplan-wms/services/planwerkwmsarchive/planname/PlanName10?REQUEST=GetCapabilities&SERVICE=WMS&VERSION=1.3.0",
				planwerkWmsUrl);
	}

	@Test
	void testCreatePlanwerkWmsUrlReplaceRequired() {
		String xplanWmsUrl = "http://xplan-services/xplan-wms";
		String planwerkWmsUrl = WmsUrlUtils.createPlanwerkWmsUrl("Plan Name 10 mit /", mockConfiguration(xplanWmsUrl),
				FESTGESTELLT);

		assertEquals(
				"http://xplan-services/xplan-wms/services/planwerkwms/planname/Plan Name 10 mit /?REQUEST=GetCapabilities&SERVICE=WMS&VERSION=1.3.0",
				planwerkWmsUrl);
	}

	@Test
	void testCreatePlanwerkWmsUrlReplaceRequiredWithServices() {
		String xplanWmsUrl = "http://xplan-services/xplan-wms";
		String planwerkWmsUrl = WmsUrlUtils.createPlanwerkWmsUrl("Plan Name 10 mit /", mockConfiguration(xplanWmsUrl),
				IN_AUFSTELLUNG);

		assertEquals(
				"http://xplan-services/xplan-wms/services/planwerkwmspre/planname/Plan Name 10 mit /?REQUEST=GetCapabilities&SERVICE=WMS&VERSION=1.3.0",
				planwerkWmsUrl);
	}

	private MapPreviewConfiguration mockConfiguration(String xplanWmsUrl) {
		MapPreviewConfiguration mockedConfiguration = Mockito.mock(MapPreviewConfiguration.class);
		when(mockedConfiguration.getXPlanWmsUrl()).thenReturn(xplanWmsUrl);
		return mockedConfiguration;
	}

}
