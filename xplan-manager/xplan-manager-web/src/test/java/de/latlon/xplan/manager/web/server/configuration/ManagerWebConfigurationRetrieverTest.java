/*-
 * #%L
 * xplan-manager-web - Webanwendung XPlanManagerWeb
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.web.server.configuration;

import static de.latlon.xplan.commons.configuration.SystemPropertyPropertiesLoader.CONFIG_SYSTEM_PROPERTY;
import static de.latlon.xplan.manager.web.server.configuration.ManagerWebConfigurationRetriever.XPLAN_WMS_URL_PUBLIC;
import static org.apache.commons.io.IOUtils.copy;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Properties;

import de.latlon.xplan.manager.web.shared.ManagerWebConfiguration;
import de.latlon.xplan.manager.web.shared.MapPreviewConfiguration;
import de.latlon.xplan.manager.web.shared.RasterLayerConfiguration;
import de.latlon.xplan.manager.web.shared.VectorLayerConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Please note: This test uses static mocking of the <link>java.lang.System</link>-class,
 * so the behaviour of this class changes in all tested implementation code!
 *
 * @author <a href="mailto:erben@lat-lon.de">Alexander Erben</a>
 * @author <a href="mailto:wanhoff@lat-lon.de">Jeronimo Wanhoff</a>
 */
class ManagerWebConfigurationRetrieverTest {

	private static final String PROPERTIES_NAME = "managerWebConfiguration.properties";

	private static String oldProperty;

	private static String oldXplanWmsUrl;

	private static String xplanWmsUrl;

	@BeforeAll
	static void copyPropertiesFileAndSetProxyConfigSystemVaraiable() throws IOException {
		File configDir = copyPropertiesFileToNewConfigDir();
		oldProperty = System.getProperty(CONFIG_SYSTEM_PROPERTY);
		System.setProperty(CONFIG_SYSTEM_PROPERTY, configDir.toString());
		oldXplanWmsUrl = System.getProperty(XPLAN_WMS_URL_PUBLIC);
		xplanWmsUrl = "http://xplan-services/xplan-services-wms";
		System.setProperty(XPLAN_WMS_URL_PUBLIC, xplanWmsUrl);
	}

	@AfterAll
	static void resetProxyConfigSystemProperty() {
		if (oldProperty != null)
			System.setProperty(CONFIG_SYSTEM_PROPERTY, oldProperty);
		if (oldXplanWmsUrl != null)
			System.setProperty(XPLAN_WMS_URL_PUBLIC, oldXplanWmsUrl);
	}

	@Test
	void testSetupManagerWebConfigurationShouldReturnMatchingPropertiesFromSystemEnv() throws Exception {
		ManagerWebConfiguration configuration = new ManagerWebConfigurationRetriever().setupManagerWebConfiguration();

		Properties properties = loadPropertiesFromOriginalFile();
		assertEquals(Boolean.valueOf(properties.getProperty("activateEditor")), configuration.isEditorActivated());
	}

	@Test
	void testSetupMapPreviewConfigurationShouldReturnMatchingPropertiesFromSystemEnv() throws Exception {
		MapPreviewConfiguration configuration = new ManagerWebConfigurationRetriever().setupMapPreviewConfiguration();

		Properties properties = loadPropertiesFromOriginalFile();
		assertEquals(properties.getProperty("basemapUrl"), configuration.getBasemapUrl());
		assertEquals(properties.getProperty("basemapName"), configuration.getBasemapName());
		assertEquals(properties.getProperty("basemapLayer"), configuration.getBasemapLayer());
		assertEquals(xplanWmsUrl, configuration.getXPlanWmsUrl());
	}

	@Test
	void testSetupVectorLayerConfigurationShouldReturnMatchingPropertiesFromSystemEnv() throws Exception {
		VectorLayerConfiguration configuration = new ManagerWebConfigurationRetriever().setupMapPreviewConfiguration()
			.getVectorLayerConfiguration();

		Properties properties = loadPropertiesFromOriginalFile();

		assertEquals(properties.getProperty("vectorWmsName"), configuration.getVectorWmsName());
		assertEquals(properties.getProperty("bpVectorLayer"), configuration.getBpVectorLayer());
		assertEquals(properties.getProperty("fpVectorLayer"), configuration.getFpVectorLayer());
		assertEquals(properties.getProperty("lpVectorLayer"), configuration.getLpVectorLayer());
		assertEquals(properties.getProperty("rpVectorLayer"), configuration.getRpVectorLayer());
	}

	@Test
	void testSetupRasterLayerConfigurationShouldReturnMatchingPropertiesFromSystemEnv() throws Exception {
		RasterLayerConfiguration configuration = new ManagerWebConfigurationRetriever().setupMapPreviewConfiguration()
			.getRasterLayerConfiguration();

		Properties properties = loadPropertiesFromOriginalFile();

		assertEquals(properties.getProperty("rasterWmsName"), configuration.getRasterWmsName());
		assertEquals(properties.getProperty("bpRasterLayer"), configuration.getBpRasterLayer());
		assertEquals(properties.getProperty("fpRasterLayer"), configuration.getFpRasterLayer());
		assertEquals(properties.getProperty("lpRasterLayer"), configuration.getLpRasterLayer());
		assertEquals(properties.getProperty("rpRasterLayer"), configuration.getRpRasterLayer());
	}

	private static File copyPropertiesFileToNewConfigDir() throws IOException {
		File configDirectory = createConfigDirectory();
		File configPropertiesFile = new File(configDirectory, PROPERTIES_NAME);
		FileOutputStream fileOutputStream = new FileOutputStream(configPropertiesFile);

		InputStream testConfigProperties = ManagerWebConfigurationRetrieverTest.class
			.getResourceAsStream(PROPERTIES_NAME);
		copy(testConfigProperties, fileOutputStream);

		testConfigProperties.close();
		fileOutputStream.close();

		return configDirectory;
	}

	private static File createConfigDirectory() throws IOException {
		File configDirectory = Files.createTempDirectory("xplanungisk-config").toFile();
		return configDirectory;
	}

	private Properties loadPropertiesFromOriginalFile() throws IOException {
		InputStream resource = ManagerWebConfigurationRetrieverTest.class.getResourceAsStream(PROPERTIES_NAME);
		Properties properties = new Properties();
		properties.load(resource);
		return properties;
	}

}
