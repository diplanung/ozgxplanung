/*-
 * #%L
 * xplan-manager-web - Webanwendung XPlanManagerWeb
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.web.server.service.security;

import de.latlon.xplan.manager.web.server.service.SecurityServiceImpl;
import de.latlon.xplan.manager.web.shared.AuthorizationInfo;
import de.latlon.xplan.manager.web.shared.ConfigurationException;

import org.junit.jupiter.api.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static de.latlon.xplan.manager.web.spring.security.XPlanAuthorizationRole.ROLE_SUPERUSER;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version $Revision: $, $Date: $
 */
class AuthorizationManagerTest {

	@Test
	void testCreateAuthorizationInfoFromAuthentication_WithEnabledSecurityShouldReturnSuperUser() throws Exception {
		AuthorizationManager managerWithEnabledSecurity = createSecurityManager(true, createAuthoritiesWithSuperUser());
		AuthorizationInfo authorizationInfo = managerWithEnabledSecurity.createAuthorizationInfoFromAuthentication();

		assertTrue(authorizationInfo.isSuperUser());
	}

	@Test
	void testCreateAuthorizationInfoFromAuthentication_WithDisabledSecurityShouldReturnSuperUserPermissions()
			throws Exception {
		AuthorizationManager managerWithEnabledSecurity = createSecurityManager(false);
		AuthorizationInfo authorizationInfo = managerWithEnabledSecurity.createAuthorizationInfoFromAuthentication();

		assertFalse(authorizationInfo.isSuperUser());
	}

	@Test
	void testIsSuperUser_WithEnabledSecurityFromNotSuperUser() throws Exception {
		AuthorizationManager managerWithEnabledSecurity = createSecurityManager(true);
		assertFalse(managerWithEnabledSecurity.isSuperUser());
	}

	@Test
	void testIsSuperUser_WithEnabledSecurityFromSuperUser() throws Exception {
		AuthorizationManager managerWithEnabledSecurity = createSecurityManager(true, createAuthoritiesWithSuperUser());
		assertTrue(managerWithEnabledSecurity.isSuperUser());
	}

	@Test
	void testIsSuperUser_WithDisabledSecurity() throws Exception {
		AuthorizationManager managerWithEnabledSecurity = createSecurityManager(false);
		assertTrue(managerWithEnabledSecurity.isSuperUser());
	}

	@Test
	void testRetrieveAuthorizationInfoWithEnabledSecurityAndWithoutAuthenticationInstanceShouldThrowException()
			throws Exception {
		AuthorizationManager securityManager = new AuthorizationManager(true);
		SecurityServiceImpl controllerWithEnabledSecurity = new SecurityServiceImpl(securityManager);

		assertThrows(ConfigurationException.class, controllerWithEnabledSecurity::retrieveAuthorizationInfo);
	}

	private AuthorizationManager createSecurityManager(boolean isSecurityEnabled) {
		return createSecurityManager(isSecurityEnabled, Collections.emptyList());
	}

	private AuthorizationManager createSecurityManager(boolean isSecurityEnabled,
			List<SimpleGrantedAuthority> authorities) {
		AuthorizationManager securityManager = spy(new AuthorizationManager(isSecurityEnabled));
		Authentication authentication = mock(Authentication.class);
		when(securityManager.retrieveAuthentication()).thenReturn(authentication);
		doReturn(authorities).when(authentication).getAuthorities();
		return securityManager;
	}

	private List<SimpleGrantedAuthority> createAuthoritiesWithSuperUser() {
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		SimpleGrantedAuthority authority = new SimpleGrantedAuthority(ROLE_SUPERUSER.toString());
		authorities.add(authority);
		return authorities;
	}

}
