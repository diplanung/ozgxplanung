/*-
 * #%L
 * xplan-manager-web - Webanwendung XPlanManagerWeb
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.web.client.comparator;

import de.latlon.xplan.manager.web.shared.XPlan;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static de.latlon.xplan.manager.web.client.gui.PlanListColumnType.IMPORTDATE;
import static de.latlon.xplan.manager.web.client.gui.PlanListColumnType.NAME;
import static de.latlon.xplan.manager.web.client.gui.PlanListColumnType.TYPE;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for {@link ColumnComparator}.
 *
 * @author <a href="mailto:stenger@lat-lon.de">Dirk Stenger</a>
 * @version $Revision: $, $Date: $
 */
class ColumnComparatorTest {

	private XPlan plan1;

	private XPlan plan1Equal;

	private XPlan plan1Null;

	private XPlan plan2;

	@BeforeEach
	void setup() {
		plan1 = new XPlan("name1", "1", "type1");
		plan1Equal = new XPlan("name1", "1", "type1");
		plan1Null = new XPlan("name2", "2", null);
		plan2 = new XPlan("name2", "2", "type2");
	}

	@Test
	void testCompareShouldBeSmaller() throws Exception {
		ColumnComparator comparator = new ColumnComparator(NAME);
		int result = comparator.compare(plan1, plan2);

		assertEquals(-1, result);
	}

	@Test
	void testCompareShouldBeGreater() throws Exception {
		ColumnComparator comparator = new ColumnComparator(NAME);
		int result = comparator.compare(plan2, plan1);

		assertEquals(1, result);
	}

	@Test
	void testCompareShouldBeEqual() throws Exception {
		ColumnComparator comparator = new ColumnComparator(NAME);
		int result = comparator.compare(plan1, plan1Equal);

		assertEquals(0, result);
	}

	@Test
	void testCompareWithNullShouldBeSmaller() throws Exception {
		ColumnComparator comparator = new ColumnComparator(TYPE);
		int result = comparator.compare(plan1Null, plan1);

		assertEquals(-1, result);
	}

	@Test
	void testCompareWithNullShouldBeGreater() throws Exception {
		ColumnComparator comparator = new ColumnComparator(TYPE);
		int result = comparator.compare(plan1, plan1Null);

		assertEquals(1, result);
	}

	@Test
	void testCompareWithNullShouldBeEqual() throws Exception {
		ColumnComparator comparator = new ColumnComparator(NAME);
		int result = comparator.compare(plan1Null, plan1Null);

		assertEquals(0, result);
	}

	@Test
	void testCompareDate() throws Exception {
		plan1.setImportDate(new Date(112014));
		plan2.setImportDate(new Date(212014));
		ColumnComparator comparator = new ColumnComparator(IMPORTDATE);
		int result = comparator.compare(plan1, plan2);

		assertEquals(-1, result);
	}

}
