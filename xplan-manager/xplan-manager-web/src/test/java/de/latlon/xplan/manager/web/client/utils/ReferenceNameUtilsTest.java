/*-
 * #%L
 * xplan-manager-web - Webanwendung XPlanManagerWeb
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.web.client.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

class ReferenceNameUtilsTest {

	@Test
	void testThatParseFilenameFromUrlWithQueryParameter() {
		String url = "https://example.com/path/to/file.txt?abc=1&xyz=abc.pdf";
		String filename = ReferenceNameUtils.extractFilenameFromUrl(url);
		assertEquals("file", filename);
	}

	@Test
	void testThatParseFilenameFromUrlWithQueryParameterWithoutFile() {
		String url = "https://example.com/path/to?abc=1&xyz=abc.pdf";
		String filename = ReferenceNameUtils.extractFilenameFromUrl(url);
		assertEquals("abc=1&xyz=abc.pdf", filename);
	}

	@Test
	void testThatParseFilenameFromUrlWithQueryParameterWithoutFile1() {
		String url = "https://example.com/path/to?abc=1&xyz=abc";
		String filename = ReferenceNameUtils.extractFilenameFromUrl(url);
		assertEquals("abc=1&xyz=abc", filename);
	}

	@Test
	void testThatParseFilenameFromUrlWithQueryParameterWithTrailingSlash() {
		String url = "https://example.com/path/to/?abc=1&xyz=abc.pdf";
		String filename = ReferenceNameUtils.extractFilenameFromUrl(url);
		assertEquals("abc=1&xyz=abc.pdf", filename);
	}

	@Test
	void testThatParseFilenameFromUrlWithTrailingQuestionmark() {
		String url = "https://example.com/path/to/file.txt?";
		String filename = ReferenceNameUtils.extractFilenameFromUrl(url);
		assertEquals("file", filename);
	}

	@Test
	void testThatParseFilenameFromUrlWithoutExtensionWithTrailingQuestionmark() {
		String url = "https://example.com/path/to/file?";
		String filename = ReferenceNameUtils.extractFilenameFromUrl(url);
		assertEquals("file", filename);
	}

	@Test
	void testThatParseFilenameFromUrlWithoutExtension() {
		String url = "https://example.com/path/to/file";
		String filename = ReferenceNameUtils.extractFilenameFromUrl(url);
		assertEquals("file", filename);
	}

	@Test
	void testThatParseFilenameFromUrlWithExtension() {
		String url = "https://example.com/path/to/file.txt";
		String filename = ReferenceNameUtils.extractFilenameFromUrl(url);
		assertEquals("file", filename);
	}

	@Test
	void testThatParseFilenameFromUrlWithTrailingSlash() {
		String url = "https://example.com/path/to/file.txt/";
		String filename = ReferenceNameUtils.extractFilenameFromUrl(url);
		assertEquals("file", filename);
	}

	@Test
	void testThatParseFilenameFromUrlWithoutPathAndFile() {
		String url = "https://example.com/";
		String filename = ReferenceNameUtils.extractFilenameFromUrl(url);
		assertEquals("example", filename);

	}

	@Test
	void testThatParseFilenameFromUrlWithCredentials() {
		String url = "https://foo@bar:example.com/path/to/file.txt";
		String filename = ReferenceNameUtils.extractFilenameFromUrl(url);
		assertEquals("file", filename);
	}

	@Test
	void testThatParseFilenameFromUrlWithEmptyString() {
		String url = "";
		String filename = ReferenceNameUtils.extractFilenameFromUrl(url);
		assertNull(filename);
	}

	@Test
	void testThatParseFilenameFromFileWithExtension() {
		String file = "file.txt";
		String filename = ReferenceNameUtils.extractFilenameFromFile(file);
		assertEquals("file", filename);
	}

}
