/*-
 * #%L
 * xplan-validator-executor - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.validator.executor;

import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.VALIDATION_FINISHED;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.VALIDATION_STARTED;
import static de.latlon.xplan.validator.report.geojson.GeoJsonBuilder.createGeoJsonFailures;
import static de.latlon.xplanbox.validator.storage.StatusType.VALIDATION_FAILED;
import static de.latlon.xplanbox.validator.storage.exception.ErrorType.INTERNAL_ERROR;
import static de.latlon.xplanbox.validator.storage.exception.ErrorType.INVALID_REQUEST;
import static de.latlon.xplanbox.validator.storage.exception.ErrorType.fromStatusCode;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.latlon.core.validator.events.EventSender;
import de.latlon.core.validator.events.ValidationRequestedEvent;
import de.latlon.core.validator.events.v1.XPlanPublicV1Event;
import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.validator.ValidatorException;
import de.latlon.xplan.validator.report.ReportGenerationException;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.report.geojson.model.FeatureCollection;
import de.latlon.xplanbox.api.commons.ObjectMapperContextResolver;
import de.latlon.xplanbox.api.commons.ValidationReportBuilder;
import de.latlon.xplanbox.api.commons.exception.InvalidXPlanGmlOrArchive;
import de.latlon.xplanbox.api.commons.exception.XPlanApiException;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReport;
import de.latlon.xplanbox.validator.executor.handler.ValidationHandler;
import de.latlon.xplanbox.validator.storage.StatusType;
import de.latlon.xplanbox.validator.storage.StoredValidationReport;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage.ReportType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
@Component
public class PlanValidator {

	private static final Logger LOG = LoggerFactory.getLogger(PlanValidator.class);

	private final ValidationExecutionStorage validationExecutionStorage;

	private final ValidationHandler validationHandler;

	private final EventSender eventSender;

	public PlanValidator(ValidationExecutionStorage validationExecutionStorage, ValidationHandler validationHandler,
			EventSender eventSender) {
		this.validationExecutionStorage = validationExecutionStorage;
		this.validationHandler = validationHandler;
		this.eventSender = eventSender;
	}

	public ValidationReport validateAndCleanUp(ValidationRequestedEvent event) throws Exception {
		return validate(event, true);
	}

	public ValidationReport validate(ValidationRequestedEvent event) throws Exception {
		return validate(event, false);
	}

	private ValidationReport validate(ValidationRequestedEvent event, boolean cleanUp) throws Exception {
		validationExecutionStorage.changeStatus(event.getUuid(), StatusType.VALIDATION_STARTED);
		eventSender.sendPublicEvent(new XPlanPublicV1Event(VALIDATION_STARTED, event.getUuid()),
				VALIDATION_STARTED.routingKeySuffix());
		return doValidate(event, cleanUp);
	}

	private ValidationReport doValidate(ValidationRequestedEvent event, boolean cleanUp) throws Exception {
		Map<ReportType, Path> reports = new HashMap<>();
		Path tmpPath = null;
		try {
			tmpPath = createTmpPath(event);
			validationExecutionStorage.writePlanToValidate(event.getUuid(), tmpPath);

			XPlanArchive archive = readAsArchive(event, tmpPath);

			LOG.info("Validating {}", tmpPath);
			ValidatorReport validatorReport = validationHandler.validate(event.getUuid(), archive, event.getxFileName(),
					event.getSettings());

			ValidationReport validationReport = createAndAddReports(event, archive, validatorReport, reports);

			StoredValidationReport storedValidationReport = validationExecutionStorage
				.saveValidationResult(event.getUuid(), reports);
			LOG.info("Validation Report: {}", validatorReport);
			validationExecutionStorage.changeStatus(event.getUuid(), StatusType.VALIDATION_FINISHED,
					storedValidationReport);
			if (cleanUp && validatorReport.isValid())
				validationExecutionStorage.cleanupAfterValidation(event.getUuid());
			eventSender.sendPublicEvent(
					new XPlanPublicV1Event(VALIDATION_FINISHED, event.getUuid(), validatorReport.isValid()),
					VALIDATION_FINISHED.routingKeySuffix());
			return validationReport;
		}
		catch (XPlanApiException e) {
			validationExecutionStorage.changeStatus(event.getUuid(), e.getMessage(), fromStatusCode(e.getStatusCode()),
					VALIDATION_FAILED);
			LOG.info("Validation with uuid {} failed: ", event.getUuid(), e.getMessage());
			throw e;
		}
		catch (ValidatorException e) {
			validationExecutionStorage.changeStatus(event.getUuid(), e.getMessage(), INVALID_REQUEST,
					VALIDATION_FAILED);
			LOG.info("Validation with uuid {} failed: ", event.getUuid(), e.getMessage());
			throw e;
		}
		catch (Exception e) {
			validationExecutionStorage.changeStatus(event.getUuid(), e.getMessage(), INTERNAL_ERROR, VALIDATION_FAILED);
			LOG.info("Validation with uuid {} failed: ", event.getUuid(), e.getMessage());
			throw e;
		}
		finally {
			for (Path path : reports.values()) {
				Files.deleteIfExists(path);
			}
			if (tmpPath != null)
				Files.deleteIfExists(tmpPath);
		}
	}

	private ValidationReport createAndAddReports(ValidationRequestedEvent event, XPlanArchive archive,
			ValidatorReport validatorReport, Map<ReportType, Path> reports)
			throws IOException, ReportGenerationException {
		ValidationReport validationReport = createValidationReport(event, archive, validatorReport);

		reports.put(ReportType.JSON, createJsonReportFile(validationReport));
		reports.put(ReportType.GEOJSON, createGeoJsonReportFile(validatorReport));
		reports.put(ReportType.PDF, createPdfReportFile(validatorReport));
		reports.put(ReportType.ZIP, createZipReportFile(validatorReport));
		return validationReport;
	}

	private static Path createTmpPath(ValidationRequestedEvent event) throws IOException {
		Path tmpPath = Files.createTempFile(event.getUuid(), ".rcv");
		if (Files.exists(tmpPath))
			Files.delete(tmpPath);
		return tmpPath;
	}

	private XPlanArchive readAsArchive(ValidationRequestedEvent event, Path tmpPath) throws InvalidXPlanGmlOrArchive {
		final XPlanArchive archive;
		if (event.getOriginFile() == ValidationRequestedEvent.OriginFile.GML) {
			archive = validationHandler.createArchiveFromGml(tmpPath.toFile(), event.getSettings().getValidationName());
		}
		else {
			archive = validationHandler.createArchiveFromZip(tmpPath.toFile(), event.getSettings().getValidationName());
		}
		return archive;
	}

	private ValidationReport createValidationReport(ValidationRequestedEvent event, final XPlanArchive archive,
			ValidatorReport validatorReport) throws ReportGenerationException {
		URI wmsUrl = validationHandler.addToWms(archive);
		return new ValidationReportBuilder().validatorReport(validatorReport) //
			.filename(event.getxFileName()) //
			.wmsUrl(wmsUrl) //
			.build();
	}

	private Path createJsonReportFile(ValidationReport validationReport) throws IOException {
		ObjectMapper mapper = new ObjectMapperContextResolver().getContext(ValidationReport.class);
		Path reportFile = Files.createTempFile("", ".json");
		mapper.writeValue(reportFile.toFile(), validationReport);
		return reportFile;
	}

	private Path createGeoJsonReportFile(ValidatorReport validatorReport)
			throws IOException, ReportGenerationException {
		FeatureCollection geoJsonFailures = createGeoJsonFailures(validatorReport);
		if (geoJsonFailures == null)
			geoJsonFailures = new FeatureCollection();
		ObjectMapper mapper = new ObjectMapperContextResolver().getContext(FeatureCollection.class);
		Path reportFile = Files.createTempFile("", ".geojson");
		mapper.writeValue(reportFile.toFile(), geoJsonFailures);
		return reportFile;
	}

	private Path createPdfReportFile(ValidatorReport validatorReport) throws IOException {
		return validationHandler.writePdfReport(validatorReport);
	}

	private Path createZipReportFile(ValidatorReport validatorReport) throws IOException {
		return validationHandler.zipReports(validatorReport);
	}

}
