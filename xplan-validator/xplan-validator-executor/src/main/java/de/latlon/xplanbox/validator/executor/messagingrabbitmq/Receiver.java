/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.validator.executor.messagingrabbitmq;

import static de.latlon.core.validator.events.ValidationFinishedEvent.ValidationFinishedStatus.FAILED;
import static de.latlon.core.validator.events.ValidationFinishedEvent.ValidationFinishedStatus.SUCCEEDED;
import static de.latlon.core.validator.events.planimport.ImportFinishedEvent.ImportFinishedEventStatus.VALIDATION_FAILED;
import static de.latlon.core.validator.events.planimport.ImportRequestedEvent.EXTERNAL_REFERENCES_RESULT.AVAILABLE_AND_VALID;
import static de.latlon.core.validator.events.planimport.ImportRequestedEvent.EXTERNAL_REFERENCES_RESULT.MISSING_OR_INVALID;
import static de.latlon.core.validator.events.planimport.ImportRequestedEvent.VALIDATION_RESULT.INVALID;
import static de.latlon.core.validator.events.planimport.ImportRequestedEvent.VALIDATION_RESULT.VALID;
import static de.latlon.xplanbox.api.commons.ValidationReportUtils.hasInvalidRasterdata;
import static de.latlon.xplanbox.api.commons.ValidationReportUtils.hasMissingReferences;
import static org.slf4j.LoggerFactory.getLogger;

import de.latlon.core.validator.events.EventSender;
import de.latlon.core.validator.events.ValidationFinishedEvent;
import de.latlon.core.validator.events.ValidationRequestedEvent;
import de.latlon.core.validator.events.planimport.ImportFinishedEvent;
import de.latlon.core.validator.events.planimport.ImportRequestedEvent;
import de.latlon.core.validator.events.planimport.ImportValidationRequestedEvent;
import de.latlon.xplanbox.api.commons.ValidationReportUtils;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReport;
import de.latlon.xplanbox.validator.executor.PlanValidator;
import org.slf4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
@Component
@RabbitListener(queues = "#{validationTaskQueue.name}", concurrency = "1")
public class Receiver {

	private static final Logger LOG = getLogger(Receiver.class);

	@Autowired
	private PlanValidator validator;

	@Autowired
	private EventSender eventSender;

	@RabbitHandler
	public void accept(ValidationRequestedEvent event) {
		LOG.info("Received event: " + event);

		if (event instanceof ImportValidationRequestedEvent) {
			handleImportEvent((ImportValidationRequestedEvent) event);
		}
		else {
			handleValidationEvent(event);
		}
	}

	private void handleValidationEvent(ValidationRequestedEvent event) {
		try {
			validator.validateAndCleanUp(event);

			ValidationFinishedEvent finishedEvent = new ValidationFinishedEvent(event.getUuid(), SUCCEEDED);
			eventSender.sendPrivateEvent(finishedEvent);
		}
		catch (Exception e) {
			LOG.error("Failed to process event. Discarding it", e);

			ValidationFinishedEvent finishedEvent = new ValidationFinishedEvent(event.getUuid(), FAILED);
			eventSender.sendPrivateEvent(finishedEvent);
		}
	}

	private void handleImportEvent(ImportValidationRequestedEvent event) {
		try {
			ValidationReport validationReport = validator.validate(event);

			ValidationFinishedEvent finishedEvent = new ValidationFinishedEvent(event.getUuid(), SUCCEEDED);
			eventSender.sendPrivateEvent(finishedEvent);
			ImportRequestedEvent importRequestedEvent = new ImportRequestedEvent(event.getUuid(),
					event.getSettings().getValidationName(), event.getInternalId(), event.getPlanStatus(),
					event.getOriginFile(), ValidationReportUtils.isValid(validationReport) ? VALID : INVALID,
					rasterEvaluationStatus(validationReport), event.isSupportsGetPlanByIdAsZip());
			eventSender.sendPrivateEvent(importRequestedEvent);
		}
		catch (Exception e) {
			LOG.error("Failed to process event. Discarding it", e);

			ValidationFinishedEvent finishedEvent = new ValidationFinishedEvent(event.getUuid(), FAILED);
			eventSender.sendPrivateEvent(finishedEvent);
			ImportFinishedEvent finishedImportEvent = new ImportFinishedEvent(event.getUuid(), VALIDATION_FAILED);
			eventSender.sendPrivateEvent(finishedImportEvent);
		}
	}

	private static ImportRequestedEvent.EXTERNAL_REFERENCES_RESULT rasterEvaluationStatus(
			ValidationReport validationReport) {
		if (hasMissingReferences(validationReport))
			return MISSING_OR_INVALID;
		boolean hasInvalidRasterReferences = hasInvalidRasterdata(validationReport);
		return hasInvalidRasterReferences ? MISSING_OR_INVALID : AVAILABLE_AND_VALID;
	}

}
