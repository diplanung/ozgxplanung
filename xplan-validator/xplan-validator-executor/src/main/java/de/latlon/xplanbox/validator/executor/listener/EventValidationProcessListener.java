/*-
 * #%L
 * xplan-validator-executor - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.validator.executor.listener;

import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.GEOMETRIC_VALIDATION_FINISHED;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.GEOMETRIC_VALIDATION_STARTED;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.PROFILE_VALIDATION_FINISHED;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.PROFILE_VALIDATION_STARTED;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.REFERENCES_FINISHED;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.REFERENCES_STARTED;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.SEMANTIC_VALIDATION_FINISHED;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.SEMANTIC_VALIDATION_STARTED;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.SYNTACTIC_VALIDATION_FINISHED;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.SYNTACTIC_VALIDATION_STARTED;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.VALIDATION_FAILED;
import static de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType.VALIDATION_STARTED;

import de.latlon.core.validator.events.EventSender;
import de.latlon.core.validator.events.v1.XPlanPublicV1Event;
import de.latlon.xplan.validator.listener.ValidationProcessListener;
import de.latlon.xplan.validator.web.shared.ValidationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
@Component
public class EventValidationProcessListener implements ValidationProcessListener {

	@Autowired
	private EventSender eventSender;

	@Override
	public void validationStarted(String uuid) {
		// handled in PlanValidator.validate
	}

	@Override
	public void validationPartStarted(String uuid, ValidationType validationType) {
		XPlanPublicV1Event.EventType eventType = SYNTACTIC_VALIDATION_STARTED;
		switch (validationType) {
			case REFERENCES -> eventType = REFERENCES_STARTED;
			case GEOMETRIC -> eventType = GEOMETRIC_VALIDATION_STARTED;
			case SEMANTIC -> eventType = SEMANTIC_VALIDATION_STARTED;
		}
		XPlanPublicV1Event event = new XPlanPublicV1Event(eventType, uuid);
		eventSender.sendPublicEvent(event, eventType.routingKeySuffix());
	}

	@Override
	public void validationPartFinished(String uuid, ValidationType validationType, Boolean isValid) {
		XPlanPublicV1Event.EventType eventType = SYNTACTIC_VALIDATION_FINISHED;
		switch (validationType) {
			case REFERENCES -> eventType = REFERENCES_FINISHED;
			case GEOMETRIC -> eventType = GEOMETRIC_VALIDATION_FINISHED;
			case SEMANTIC -> eventType = SEMANTIC_VALIDATION_FINISHED;
		}
		XPlanPublicV1Event event = new XPlanPublicV1Event(eventType, uuid, isValid);
		eventSender.sendPublicEvent(event, eventType.routingKeySuffix());
	}

	@Override
	public void validationProfileStarted(String uuid, String profileId) {
		XPlanPublicV1Event event = new XPlanPublicV1Event(PROFILE_VALIDATION_STARTED, uuid);
		eventSender.sendPublicEvent(event, PROFILE_VALIDATION_STARTED.routingKeySuffix() + profileId);

	}

	@Override
	public void validationProfileFinished(String uuid, String profileId, Boolean isValid) {
		XPlanPublicV1Event event = new XPlanPublicV1Event(PROFILE_VALIDATION_FINISHED, uuid, isValid);
		eventSender.sendPublicEvent(event, PROFILE_VALIDATION_FINISHED.routingKeySuffix() + profileId);
	}

	@Override
	public void validationFinished(String uuid, Boolean isValid) {
		// handled in PlanValidator.validate
	}

	@Override
	public void validationFailed(String uuid, String error) {
		XPlanPublicV1Event event = new XPlanPublicV1Event(VALIDATION_FAILED, uuid, error);
		eventSender.sendPublicEvent(event, VALIDATION_STARTED.routingKeySuffix());

	}

}
