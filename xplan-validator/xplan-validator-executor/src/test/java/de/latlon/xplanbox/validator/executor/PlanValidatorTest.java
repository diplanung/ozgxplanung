/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.validator.executor;

import static de.latlon.xplan.validator.web.shared.ValidationType.GEOMETRIC;
import static de.latlon.xplan.validator.web.shared.ValidationType.SEMANTIC;
import static de.latlon.xplan.validator.web.shared.ValidationType.SYNTACTIC;
import static de.latlon.xplanbox.validator.storage.StatusType.VALIDATION_REQUESTED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.latlon.core.validator.events.EventSender;
import de.latlon.core.validator.events.ValidationRequestedEvent;
import de.latlon.core.validator.events.ValidationRequestedEvent.MediaType;
import de.latlon.core.validator.events.ValidationRequestedEvent.OriginFile;
import de.latlon.core.validator.events.v1.XPlanPublicV1Event;
import de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType;
import de.latlon.xplan.commons.archive.SemanticValidableXPlanArchive;
import de.latlon.xplan.validator.semantic.configuration.metadata.RulesMetadata;
import de.latlon.xplan.validator.semantic.profile.SemanticProfileValidator;
import de.latlon.xplan.validator.semantic.profile.SemanticProfiles;
import de.latlon.xplan.validator.semantic.report.SemanticValidatorResult;
import de.latlon.xplan.validator.web.shared.ValidationSettings;
import de.latlon.xplan.validator.web.shared.ValidationType;
import de.latlon.xplanbox.validator.executor.config.TestS3Context;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage.ReportType;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.bean.override.mockito.MockitoBean;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
@SpringBootTest(classes = { SpringBootApp.class })
@Import(TestS3Context.class)
@ActiveProfiles({ "mock", "test" })
@TestPropertySource("classpath:s3Mock.properties")
class PlanValidatorTest {

	@TestConfiguration
	static class TestConfig {

		@Primary
		@Bean
		public SemanticProfiles fakeSemanticProfiles() {
			RulesMetadata profile1 = new RulesMetadata("id1", "test1", "description1", "0.1", "unbekannt");
			RulesMetadata profile2 = new RulesMetadata("id2", "test2", "description2", "0.2", "lokal");
			return new SemanticProfiles().add(profile1, createValidator(profile1))
				.add(profile2, createValidator(profile2));
		}

		private static SemanticProfileValidator createValidator(RulesMetadata profile) {
			SemanticProfileValidator semanticProfileValidator = mock(SemanticProfileValidator.class);
			when(semanticProfileValidator.getId()).thenReturn(profile.getId());
			SemanticValidatorResult result = mock(SemanticValidatorResult.class);
			when(result.getRulesMetadata()).thenReturn(profile);
			when(semanticProfileValidator.validateSemantic(any(SemanticValidableXPlanArchive.class), anyList()))
				.thenReturn(result);
			return semanticProfileValidator;
		}

	}

	@MockitoBean
	EventSender eventSender;

	@Captor
	ArgumentCaptor<XPlanPublicV1Event> publicEventCaptor;

	@Captor
	ArgumentCaptor<String> stringCaptor;

	@Autowired
	PlanValidator validator;

	@Autowired
	ValidationExecutionStorage validationExecutionStorage;

	@Autowired
	ObjectMapper jsonMapper;

	@Autowired
	private SemanticProfiles semanticProfiles;

	@Test
	// from old
	// ValidateApiTest.verifyThat_Response_ContainsCorrectStatusCodeAndMediaType
	void validateGmlJsonReport() throws Exception {

		Path pathToPlan = Paths.get(getClass().getResource("/xplan.gml").toURI());
		String uuid = validationExecutionStorage.addPlanToValidate(pathToPlan, VALIDATION_REQUESTED);

		List<ValidationType> validationTypes = List.of(SYNTACTIC, SEMANTIC, GEOMETRIC);
		ValidationSettings settings = new ValidationSettings("edfd613e-b85d-4ea6-9b97-bb33712b1ba6", validationTypes,
				null);
		ValidationRequestedEvent event = new ValidationRequestedEvent(uuid, settings, null, MediaType.JSON,
				OriginFile.GML);

		validator.validate(event);

		verifyExpectedReport(uuid, ReportType.JSON, "report1.expected.json");

		XPlanPublicV1Event expectedStartEvent = new XPlanPublicV1Event(EventType.VALIDATION_STARTED, uuid);
		XPlanPublicV1Event expectedSynStartedEvent = new XPlanPublicV1Event(EventType.SYNTACTIC_VALIDATION_STARTED,
				uuid);
		XPlanPublicV1Event expectedSynFinishedEvent = new XPlanPublicV1Event(EventType.SYNTACTIC_VALIDATION_FINISHED,
				uuid, true);
		XPlanPublicV1Event expectedRefStartedEvent = new XPlanPublicV1Event(EventType.REFERENCES_STARTED, uuid);
		XPlanPublicV1Event expectedRefFinishedEvent = new XPlanPublicV1Event(EventType.REFERENCES_FINISHED, uuid, true);
		XPlanPublicV1Event expectedSemStartedEvent = new XPlanPublicV1Event(EventType.SEMANTIC_VALIDATION_STARTED,
				uuid);
		XPlanPublicV1Event expectedSemFinishedEvent = new XPlanPublicV1Event(EventType.SEMANTIC_VALIDATION_FINISHED,
				uuid, true);
		XPlanPublicV1Event expectedGeomStartedEvent = new XPlanPublicV1Event(EventType.GEOMETRIC_VALIDATION_STARTED,
				uuid);
		XPlanPublicV1Event expectedGeomFinishedEvent = new XPlanPublicV1Event(EventType.GEOMETRIC_VALIDATION_FINISHED,
				uuid, false);
		XPlanPublicV1Event expectedFinishedEvent = new XPlanPublicV1Event(EventType.VALIDATION_FINISHED, uuid, false);
		verify(eventSender, times(10)).sendPublicEvent(publicEventCaptor.capture(), stringCaptor.capture());

		assertThat(publicEventCaptor.getAllValues()).containsExactly(expectedStartEvent, expectedSynStartedEvent,
				expectedSynFinishedEvent, expectedRefStartedEvent, expectedRefFinishedEvent, expectedGeomStartedEvent,
				expectedGeomFinishedEvent, expectedSemStartedEvent, expectedSemFinishedEvent, expectedFinishedEvent);
		assertThat(stringCaptor.getAllValues()).containsExactly("validation", "validation.syntaktisch",
				"validation.syntaktisch", "validation.referenzen", "validation.referenzen", "validation.geometrisch",
				"validation.geometrisch", "validation.semantisch", "validation.semantisch", "validation");
	}

	@Test
	// from old
	// ValidateApiTest.verifyThat_validationXZipCompressedWithProfile_Response_ContainsJsonEncoding()
	void valideZipWithOneProfile() throws Exception {
		Path pathToPlan = Paths.get(getClass().getResource("/bplan_valid_41.zip").toURI());
		String uuid = validationExecutionStorage.addPlanToValidate(pathToPlan, VALIDATION_REQUESTED);

		List<ValidationType> validationTypes = List.of(SYNTACTIC, SEMANTIC, GEOMETRIC);
		List<String> profiles = List.of(semanticProfiles.getProfileValidators().get(0).getId());
		ValidationSettings settings = new ValidationSettings("8dc3163c-a361-49b8-9c53-3733f7d61274", validationTypes,
				profiles, null);
		ValidationRequestedEvent event = new ValidationRequestedEvent(uuid, settings, null, MediaType.JSON,
				OriginFile.ZIP);

		validator.validate(event);

		verifyExpectedReport(uuid, ReportType.JSON, "report2.expected.json");
	}

	@Test
	// from old
	// ValidateApiTest.verifyThat_validationXZipCompressedWithProfiles_Response_ContainsJsonEncoding()
	void valideZipWithTwoProfiles() throws Exception {
		Path pathToPlan = Paths.get(getClass().getResource("/bplan_valid_41.zip").toURI());
		String uuid = validationExecutionStorage.addPlanToValidate(pathToPlan, VALIDATION_REQUESTED);

		List<ValidationType> validationTypes = List.of(SYNTACTIC, SEMANTIC, GEOMETRIC);
		List<String> profiles = List.of(semanticProfiles.getProfileValidators().get(0).getId(),
				semanticProfiles.getProfileValidators().get(1).getId());
		ValidationSettings settings = new ValidationSettings("6463c72f-4a7d-40d0-a129-3de95fdd0eb8", validationTypes,
				profiles, null);
		ValidationRequestedEvent event = new ValidationRequestedEvent(uuid, settings, null, MediaType.JSON,
				OriginFile.ZIP);

		validator.validate(event);

		verifyExpectedReport(uuid, ReportType.JSON, "report3.expected.json");
	}

	private void verifyExpectedReport(String uuid, ReportType reportType, String expectedReportFile) throws Exception {
		String jsonReport = new String(validationExecutionStorage.retrieveReport(uuid, reportType),
				StandardCharsets.UTF_8);

		String expectedReport = Files.readString(Paths.get(getClass().getResource(expectedReportFile).toURI()));
		assertThat(sanitizeDates(jsonReport)).isEqualTo(sanitizeDates(expectedReport));
	}

	private String sanitizeDates(String s) throws Exception {
		s = s.replaceAll("\"date\":\"[^\"]+\"", "\"date\":\"-fixed-for-comparison-\"");
		JSONObject json = new JSONObject(s);
		return json.toString(4);
	}

}
