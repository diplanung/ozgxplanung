# xPlanValidatorExecutor

## Development

The application can be started locally with

```
mvn spring-boot:run -Dspring-boot.run.profiles=dev
```

(RabbitMQ required on localhost:5672 with credentials guest/guest)
