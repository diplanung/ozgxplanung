# XPlanValidatorAPI

OpenAPI v1: #host#/xplan-validator-api/xvalidator/api/v1/
OpenAPI v2: #host#/xplan-validator-api/api/v2/

## Development

The application can be started locally with

```
mvn spring-boot:run -Dspring-boot.run.profiles=dev
```

(RabbitMQ required on localhost:5672 with credentials guest/guest)

The application starts on port 8085:

- v1 API: http://localhost:8085/xplan-validator-api/xvalidator/api/v1

- v2 API: http://localhost:8085/xplan-validator-api/api/v2

## Examples with curl 

```
export XPLAN_VALIDATOR_API_HOST=http://localhost:8085
```

### validation

```
curl -k -H "Content-Type: text/xml" \
  -d @../../xplan-tests/xplan-tests-soapui/src/main/resources/xplan-validator-api/plans/BP_5.1.gml \
  $XPLAN_VALIDATOR_API_HOST/xplan-validator-api/xvalidator/api/v1/validate
```

```
curl -k -H "Content-Type: application/zip" \
  --data-binary @../../xplan-tests/xplan-tests-soapui/src/main/resources/xplan-validator-api/plans/BP_6.0.2_valide.zip \
  $XPLAN_VALIDATOR_API_HOST/xplan-validator-api/xvalidator/api/v1/validate
```

## Technische Endpoints

- `/xplan-validator-api/actuator/health/liveness`: liveness check
    ```
    $> curl http://localhost:8085/xplan-validator-api/actuator/health/liveness
    {"status":"UP"}
    ```

- `/xplan-validator-api/actuator/info`: build infos
    ```
    $> curl http://localhost:8085/xplan-validator-api/actuator/info
    {"build":{"gitRevision":"08e35dc18b463715bbf0feab0c4c3a2e8c8a0904","key":""}}
    ```
