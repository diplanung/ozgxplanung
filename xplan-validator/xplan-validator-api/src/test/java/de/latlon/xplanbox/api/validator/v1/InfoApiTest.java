/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.v1;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import de.latlon.xplanbox.api.validator.ValidatorApiJerseyTest;
import de.latlon.xplanbox.api.validator.api.AbstractApiConfig;
import de.latlon.xplanbox.api.validator.api.v1.ApiV1Config;
import de.latlon.xplanbox.api.validator.config.ValidatorApiConfiguration;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.core.Response;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.json.BasicJsonTester;

/**
 * @author <a href="mailto:friebe@lat-lon.de">Torsten Friebe</a>
 */
class InfoApiTest extends ValidatorApiJerseyTest {

	public AbstractApiConfig createConfig(ServletContext mockServletContext,
			ValidatorApiConfiguration validatorConfig) {
		return new ApiV1Config(mockServletContext, validatorConfig, Optional.empty());
	}

	@Test
	void verifyThat_Response_ContainsCorrectStatusCodeAndMediaType() {
		final Response response = target("/info").request(APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertEquals(APPLICATION_JSON, response.getHeaderString(HttpHeaders.CONTENT_TYPE));

		BasicJsonTester json = new BasicJsonTester(getClass());
		assertThat(json.from(response.readEntity(String.class))).extractingJsonPathStringValue("$.rulesMetadata.source")
			.startsWith("https://gitlab.opencode.de/xleitstelle/xplanung/validierungsregeln/standard/-/tree/");

	}

	@Test
	void verifyThat_Response_ContainsSupportedXplanGmlVersionsAndProfiles() {
		final String response = target("/info").request(APPLICATION_JSON).get(String.class);

		assertThat(response).contains("supportedXPlanGmlVersions");
		assertThat(response).contains("profiles");
		assertThat(response).contains("skipRasterEvaluation");
	}

}
