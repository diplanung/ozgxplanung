/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.v1;

import static de.latlon.xplanbox.api.commons.XPlanMediaType.APPLICATION_X_ZIP;
import static de.latlon.xplanbox.api.commons.XPlanMediaType.APPLICATION_X_ZIP_COMPRESSED;
import static de.latlon.xplanbox.api.commons.XPlanMediaType.APPLICATION_ZIP;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM;
import static jakarta.ws.rs.core.MediaType.APPLICATION_XML;
import static jakarta.ws.rs.core.MediaType.TEXT_XML;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import de.latlon.core.validator.events.ValidationRequestedEvent.OriginFile;
import de.latlon.xplan.validator.semantic.profile.SemanticProfiles;
import de.latlon.xplanbox.api.validator.ValidatorApiJerseyTest;
import de.latlon.xplanbox.api.validator.api.AbstractApiConfig;
import de.latlon.xplanbox.api.validator.api.v1.ApiV1Config;
import de.latlon.xplanbox.api.validator.config.FakeAsyncValidationWrapper;
import de.latlon.xplanbox.api.validator.config.ValidatorApiConfiguration;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Response;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author <a href="mailto:friebe@lat-lon.de">Torsten Friebe</a>
 */
public class ValidateApiJerseyTest extends ValidatorApiJerseyTest {

	private SemanticProfiles semanticProfiles;

	private FakeAsyncValidationWrapper fakeAsyncValidationWrapper;

	@Override
	protected Application configure() {
		Application application = super.configure();
		AnnotationConfigApplicationContext context = (AnnotationConfigApplicationContext) application.getProperties()
			.get("contextConfig");
		fakeAsyncValidationWrapper = context.getBean(FakeAsyncValidationWrapper.class);
		semanticProfiles = context.getBean(SemanticProfiles.class);
		return application;
	}

	@Override
	protected AbstractApiConfig createConfig(ServletContext mockServletContext,
			ValidatorApiConfiguration validatorConfig) {
		return new ApiV1Config(mockServletContext, validatorConfig, Optional.empty());
	}

	@Test
	void verifyThat_Response_ContainsCorrectStatusCodeAndMediaType() throws IOException, URISyntaxException {
		final String data = Files.readString(Paths.get(ValidateApiJerseyTest.class.getResource("/xplan.gml").toURI()));

		fakeAsyncValidationWrapper
			.saveReportForNextValidateRequest(Paths.get(getClass().getResource("validation-report1.json").toURI()));

		final Response response = target("/validate").request()
			.accept(APPLICATION_JSON)
			.post(Entity.entity(data, TEXT_XML));

		assertThat(fakeAsyncValidationWrapper.getLastValidatedEvent().getOriginFile()).isEqualTo(OriginFile.GML);
		assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_JSON);
	}

	@Test
	void verifyThat_validationOctetStream_Response_ContainsXmlEncoding() throws URISyntaxException, IOException {
		final byte[] data = Files.readAllBytes(Paths.get(getClass().getResource("/bplan_valid_41.zip").toURI()));

		fakeAsyncValidationWrapper.saveReportForNextValidateRequest(someJsonReport());

		final Response response = target("/validate").request()
			.accept(APPLICATION_XML)
			.post(Entity.entity(data, APPLICATION_OCTET_STREAM));

		assertThat(fakeAsyncValidationWrapper.getLastValidatedEvent().getOriginFile()).isEqualTo(OriginFile.ZIP);

		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_XML);
		assertThat(response.readEntity(String.class)).contains("valid");
	}

	@Test
	void verifyThat_validationZip_Response_ContainsXmlEncoding() throws Exception {
		final byte[] data = Files.readAllBytes(Paths.get(getClass().getResource("/bplan_valid_41.zip").toURI()));

		fakeAsyncValidationWrapper.saveReportForNextValidateRequest(someJsonReport());

		final Response response = target("/validate").request()
			.accept(APPLICATION_XML)
			.post(Entity.entity(data, APPLICATION_ZIP));

		assertThat(fakeAsyncValidationWrapper.getLastValidatedEvent().getOriginFile()).isEqualTo(OriginFile.ZIP);

		assertThat(response.getStatus()).isEqualTo(200);
		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_XML);
		assertThat(response.readEntity(String.class)).contains("valid");
	}

	@Test
	void verifyThat_validationXZip_Response_ContainsXmlEncoding() throws Exception {
		final byte[] data = Files.readAllBytes(Paths.get(getClass().getResource("/bplan_valid_41.zip").toURI()));

		fakeAsyncValidationWrapper.saveReportForNextValidateRequest(someJsonReport());

		final Response response = target("/validate").request()
			.accept(APPLICATION_XML)
			.post(Entity.entity(data, APPLICATION_X_ZIP));

		assertThat(fakeAsyncValidationWrapper.getLastValidatedEvent().getOriginFile()).isEqualTo(OriginFile.ZIP);

		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_XML);
		assertThat(response.readEntity(String.class)).contains("valid");
	}

	@Test
	void verifyThat_validationXZipCompressed_Response_ContainsXmlEncoding() throws Exception {
		final byte[] data = Files.readAllBytes(Paths.get(getClass().getResource("/bplan_valid_41.zip").toURI()));

		fakeAsyncValidationWrapper.saveReportForNextValidateRequest(someJsonReport());

		final Response response = target("/validate").request()
			.accept(APPLICATION_XML)
			.post(Entity.entity(data, APPLICATION_X_ZIP_COMPRESSED));

		assertThat(fakeAsyncValidationWrapper.getLastValidatedEvent().getOriginFile()).isEqualTo(OriginFile.ZIP);

		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_XML);
		assertThat(response.readEntity(String.class)).contains("valid");
	}

	@Test
	void verifyThat_validationXZipCompressedWithProfilesCommaseparated_Response_ContainsJsonEncoding()
			throws Exception {

		fakeAsyncValidationWrapper.saveReportForNextValidateRequest(someJsonReport());

		final byte[] data = Files
			.readAllBytes(Paths.get(ValidateApiJerseyTest.class.getResource("/bplan_valid_41.zip").toURI()));
		String profileId0 = semanticProfiles.getProfileValidators().get(0).getId();
		String profileId1 = semanticProfiles.getProfileValidators().get(1).getId();
		final Response response = target("/validate").queryParam("profiles", profileId0 + "," + profileId1)
			.request()
			.accept(APPLICATION_JSON)
			.post(Entity.entity(data, APPLICATION_X_ZIP_COMPRESSED));

		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_JSON);
		assertThat(fakeAsyncValidationWrapper.getLastValidatedEvent().getSettings().getProfiles())
			.containsExactlyInAnyOrder(profileId0, profileId1);
	}

	@Test
	void verifyThat_validationWithInvalidXFileName_Response_IsStatusCode400() throws URISyntaxException, IOException {
		final byte[] data = Files
			.readAllBytes(Paths.get(ValidateApiJerseyTest.class.getResource("/bplan_valid_41.zip").toURI()));
		final Response response = target("/validate").request()
			.header("X-Filename", "invalid.filename with blanks")
			.accept(APPLICATION_JSON)
			.post(Entity.entity(data, APPLICATION_X_ZIP_COMPRESSED));

		assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
	}

	@Test
	void verifyThat_validationWithInvalidName_Response_IsStatusCode400() throws Exception {
		final byte[] data = Files
			.readAllBytes(Paths.get(ValidateApiJerseyTest.class.getResource("/bplan_valid_41.zip").toURI()));
		final Response response = target("/validate").queryParam("name", "invalid.name with blanks")
			.request()
			.accept(APPLICATION_JSON)
			.post(Entity.entity(data, APPLICATION_X_ZIP_COMPRESSED));

		assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
	}

	Path someJsonReport() throws URISyntaxException {
		return Paths.get(getClass().getResource("validation-report1.json").toURI());
	}

}
