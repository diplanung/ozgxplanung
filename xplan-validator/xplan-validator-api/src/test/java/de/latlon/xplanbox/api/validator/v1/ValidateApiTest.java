/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.v1;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

import de.latlon.core.validator.events.ValidationRequestedEvent;
import de.latlon.xplanbox.api.validator.config.ValidatorApiConfiguration;
import de.latlon.xplanbox.api.validator.handler.AsyncValidationWrapper;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage;
import jakarta.ws.rs.core.Request;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;

/**
 */
class ValidateApiTest {

	@TempDir
	Path tmpDir;

	@Test
	void verifyThatProfilesAreSentToValidator_noProfile() throws Exception {
		List<String> profilesInEvent = callValidateAndExtractProfilesInValidationRequestEvent(null);
		assertThat(profilesInEvent).isEmpty();

		profilesInEvent = callValidateAndExtractProfilesInValidationRequestEvent(Collections.emptyList());
		assertThat(profilesInEvent).isEmpty();

	}

	@Test
	void verifyThatProfilesAreSentToValidator_oneProfile() throws Exception {
		List<String> profilesInEvent = callValidateAndExtractProfilesInValidationRequestEvent(List.of("profile1"));
		assertThat(profilesInEvent).containsExactlyInAnyOrder("profile1");
	}

	@Test
	void verifyThatProfilesAreSentToValidator_twoProfiles() throws Exception {
		List<String> profilesInEvent = callValidateAndExtractProfilesInValidationRequestEvent(
				List.of("profile1", "profile2"));
		assertThat(profilesInEvent).containsExactlyInAnyOrder("profile1", "profile2");
	}

	private List<String> callValidateAndExtractProfilesInValidationRequestEvent(List profiles) throws Exception {
		ValidateApi validateApi = new ValidateApi();
		ValidationExecutionStorage validationExecutionStorage = mock(ValidationExecutionStorage.class,
				Answers.RETURNS_MOCKS);
		when(validationExecutionStorage.retrieveReport(anyString(), eq(ValidationExecutionStorage.ReportType.JSON)))
			.thenReturn(getClass().getResourceAsStream("validation-report1.json").readAllBytes());
		validateApi.validationExecutionStorage = validationExecutionStorage;
		validateApi.validationWrapper = mock(AsyncValidationWrapper.class);
		validateApi.validatorApiConfiguration = mock(ValidatorApiConfiguration.class);

		Path tmpFile = tmpDir.resolve("my-file.xml");
		Files.writeString(tmpFile, "");
		Request request = mock(Request.class);
		validateApi.validate(request, tmpFile.toFile(), null, null, false, false, false, false, false, profiles);

		ArgumentCaptor<ValidationRequestedEvent> argumentCaptor = ArgumentCaptor
			.forClass(ValidationRequestedEvent.class);
		verify(validateApi.validationWrapper).sendEvent(argumentCaptor.capture());

		ValidationRequestedEvent event = argumentCaptor.getValue();
		return event.getSettings().getProfiles();
	}

}
