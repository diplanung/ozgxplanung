/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator;

import de.latlon.xplan.commons.configuration.DefaultPropertiesLoader;
import de.latlon.xplanbox.api.commons.exception.XPlanApiExceptionMapper;
import de.latlon.xplanbox.api.validator.api.AbstractApiConfig;
import de.latlon.xplanbox.api.validator.config.ApplicationContext;
import de.latlon.xplanbox.api.validator.config.TestContext;
import de.latlon.xplanbox.api.validator.config.ValidatorApiConfiguration;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.core.Application;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.mockito.Mockito;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public abstract class ValidatorApiJerseyTest extends JerseyTest {

	@Override
	protected Application configure() {
		enable(TestProperties.LOG_TRAFFIC);

		ResourceConfig resourceConfig = createResourceConfig();
		AnnotationConfigApplicationContext context = createApplicationContext();
		resourceConfig.register(XPlanApiExceptionMapper.class);
		resourceConfig.property("contextConfig", context);
		return resourceConfig;
	}

	private AnnotationConfigApplicationContext createApplicationContext() {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		PropertySourcesPlaceholderConfigurer pph = new PropertySourcesPlaceholderConfigurer();
		pph.setLocations(new ClassPathResource("/application.properties"), new ClassPathResource("/s3Mock.properties"));
		context.getEnvironment().setActiveProfiles("test", "mock");
		context.addBeanFactoryPostProcessor(pph);
		context.register(ApplicationContext.class, TestContext.class);
		context.refresh();
		return context;
	}

	private ResourceConfig createResourceConfig() {
		ServletContext mockServletContext = Mockito.mock(ServletContext.class);
		Mockito.when(mockServletContext.getContextPath()).thenReturn("");

		try {
			ValidatorApiConfiguration validatorConfig = new ValidatorApiConfiguration(
					new DefaultPropertiesLoader(ValidatorApiJerseyTest.class));
			return createConfig(mockServletContext, validatorConfig);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected abstract AbstractApiConfig createConfig(ServletContext mockServletContext,
			ValidatorApiConfiguration validatorConfig);

}
