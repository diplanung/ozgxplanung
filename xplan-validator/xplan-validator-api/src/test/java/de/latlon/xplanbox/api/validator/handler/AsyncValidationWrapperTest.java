/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.handler;

import static de.latlon.core.validator.events.ValidationFinishedEvent.ValidationFinishedStatus.SUCCEEDED;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import de.latlon.core.validator.events.EventSender;
import de.latlon.core.validator.events.ValidationFinishedEvent;
import de.latlon.core.validator.events.ValidationRequestedEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
class AsyncValidationWrapperTest {

	private List<String> logs = Collections.synchronizedList(new ArrayList<>());

	@BeforeEach
	private void cleanup() {
		logs.clear();
	}

	@Test
	void simpleCase() throws Exception {
		EventSender eventSender = Mockito.mock(EventSender.class);
		AsyncValidationWrapper wrapper = new AsyncValidationWrapper(eventSender);

		startAsyncValidation(wrapper, someEventWithUuid("uuid1"), "event1");
		verifyNewLog("event1: before validate");

		// send validation finished event
		wrapper.someValidationFinished(new ValidationFinishedEvent("uuid1", SUCCEEDED));
		verifyNewLog("event1: after validate");
	}

	@Test
	void moreComplicated() throws Exception {
		EventSender eventSender = Mockito.mock(EventSender.class);
		AsyncValidationWrapper wrapper = new AsyncValidationWrapper(eventSender);

		startAsyncValidation(wrapper, someEventWithUuid("uuid1"), "event1");
		verifyNewLog("event1: before validate");

		startAsyncValidation(wrapper, someEventWithUuid("uuid2"), "event2");
		verifyNewLog("event2: before validate");

		startAsyncValidation(wrapper, someEventWithUuid("uuid3"), "event3");
		verifyNewLog("event3: before validate");

		// send validation finished event for something else
		wrapper.someValidationFinished(new ValidationFinishedEvent("uuidOther", SUCCEEDED));

		// send validation finished event for event 2
		wrapper.someValidationFinished(new ValidationFinishedEvent("uuid2", SUCCEEDED));
		verifyNewLog("event2: after validate");

		// send validation finished events for something else
		wrapper.someValidationFinished(new ValidationFinishedEvent("uuidOther2", SUCCEEDED));
		wrapper.someValidationFinished(new ValidationFinishedEvent("uuid2", SUCCEEDED));

		// send validation finished event for event 1
		wrapper.someValidationFinished(new ValidationFinishedEvent("uuid1", SUCCEEDED));
		verifyNewLog("event1: after validate");

		// send validation finished event for event 1
		wrapper.someValidationFinished(new ValidationFinishedEvent("uuid3", SUCCEEDED));
		verifyNewLog("event3: after validate");
	}

	private void startAsyncValidation(AsyncValidationWrapper wrapper, ValidationRequestedEvent event,
			String msgPrefix) {
		new Thread("Thread " + msgPrefix) {
			public void run() {
				try {
					logs.add(msgPrefix + ": before validate");
					wrapper.sendEvent(event);
				}
				catch (Exception e) {
					logs.add(msgPrefix + ": exception " + e.getMessage());
				}
				logs.add(msgPrefix + ": after validate");
			};
		}.start();
	}

	private void verifyNewLog(String expectedNewMessage) throws Exception {
		waitingIfNeeded(() -> assertThat(logs).containsExactly(expectedNewMessage));
		logs.clear();
	}

	private void waitingIfNeeded(Callable<?> callable) throws Exception {
		long maxWaitTime = System.currentTimeMillis() + 5_000;
		while (true) {
			try {
				callable.call();
				return;
			}
			catch (Throwable t) {
				if (System.currentTimeMillis() > maxWaitTime) {
					throw t;
				}
				Thread.sleep(1_000);
			}
		}

	}

	private ValidationRequestedEvent someEventWithUuid(String uuid) {
		return new ValidationRequestedEvent(uuid, null, null, null, null);
	}

}
