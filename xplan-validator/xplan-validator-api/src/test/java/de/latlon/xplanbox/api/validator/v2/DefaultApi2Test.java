/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.v2;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import de.latlon.xplanbox.api.validator.ValidatorApiJerseyTest;
import de.latlon.xplanbox.api.validator.api.AbstractApiConfig;
import de.latlon.xplanbox.api.validator.api.v2.ApiV2Config;
import de.latlon.xplanbox.api.validator.config.ValidatorApiConfiguration;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.core.Response;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.json.BasicJsonTester;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 8.0
 */
class DefaultApi2Test extends ValidatorApiJerseyTest {

	public AbstractApiConfig createConfig(ServletContext mockServletContext,
			ValidatorApiConfiguration validatorConfig) {
		return new ApiV2Config(mockServletContext, validatorConfig, Optional.empty());
	}

	@Test
	void verifyThat_Response_ContainsCorrectStatusCodeAndMediaType() {
		final Response response = target("/").request(APPLICATION_JSON).get();

		assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_JSON);
	}

	@Test
	void verifyThat_Response_ContainsOpenApiDocument() {
		final String response = target("/").request(APPLICATION_JSON).get(String.class);

		BasicJsonTester json = new BasicJsonTester(getClass());
		assertThat(json.from(response)).extractingJsonPathStringValue("$.openapi").isEqualTo("3.0.1");
	}

	@Test
	void verifyThat_Response_ContainsLicence() {
		final String response = target("/").request(APPLICATION_JSON).get(String.class);

		BasicJsonTester json = new BasicJsonTester(getClass());
		assertThat(json.from(response)).extractingJsonPathStringValue("$.info.license.name").isEqualTo("Apache 2.0");
	}

	@Test
	void verifyThat_Response_Paths() {
		final String response = target("/").request(APPLICATION_JSON).get(String.class);

		BasicJsonTester json = new BasicJsonTester(getClass());
		assertThat(json.from(response)).extractingJsonPathMapValue("$.paths")
			.containsOnlyKeys("/", "/info", "/status/{uuid}", "/validate", "/report/{uuid}",
					"/report/{uuid}/geomfindings", "/report/{uuid}/geomfindings.json");
	}

	@Test
	void verifyThat_Response_ContainsTermsOfService() {
		final String response = target("/").request(APPLICATION_JSON).get(String.class);

		BasicJsonTester json = new BasicJsonTester(getClass());
		assertThat(json.from(response)).extractingJsonPathStringValue("$.info.termsOfService")
			.isEqualTo("http://xplanbox/api-validator/terms");
	}

	@Test
	void verifyThat_Response_ContainsServersUrl() {
		final String response = target("/").request(APPLICATION_JSON).get(String.class);

		BasicJsonTester json = new BasicJsonTester(getClass());
		assertThat(json.from(response)).extractingJsonPathStringValue("$.servers[0].url")
			.isEqualTo("http://xplanbox-api-validator/api/v2");
	}

}
