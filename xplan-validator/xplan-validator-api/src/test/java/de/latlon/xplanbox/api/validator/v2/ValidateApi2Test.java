/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.v2;

import static de.latlon.xplanbox.api.commons.XPlanMediaType.APPLICATION_X_ZIP_COMPRESSED;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.MediaType.TEXT_XML;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

import de.latlon.xplanbox.api.validator.ValidatorApiJerseyTest;
import de.latlon.xplanbox.api.validator.api.AbstractApiConfig;
import de.latlon.xplanbox.api.validator.api.v2.ApiV2Config;
import de.latlon.xplanbox.api.validator.config.ValidatorApiConfiguration;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Response;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 8.0
 */
public class ValidateApi2Test extends ValidatorApiJerseyTest {

	public AbstractApiConfig createConfig(ServletContext mockServletContext,
			ValidatorApiConfiguration validatorConfig) {
		return new ApiV2Config(mockServletContext, validatorConfig, Optional.empty());
	}

	@Test
	void verifyThat_Response_ContainsCorrectStatusCodeAndMediaType() throws IOException, URISyntaxException {
		final String data = new String(
				Files.readAllBytes(Paths.get(ValidateApi2Test.class.getResource("/xplan.gml").toURI())));
		final Response response = target("/validate").request()
			.accept(APPLICATION_JSON)
			.post(Entity.entity(data, TEXT_XML));

		assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_JSON);
		assertThat(response.readEntity(String.class)).contains("uuid");
	}

	@Test
	void verifyThat_validationXZipCompressed_Response_ContainsJsonEncoding() throws URISyntaxException, IOException {
		final byte[] data = Files
			.readAllBytes(Paths.get(ValidateApi2Test.class.getResource("/bplan_valid_41.zip").toURI()));
		final Response response = target("/validate").request()
			.accept(APPLICATION_JSON)
			.post(Entity.entity(data, APPLICATION_X_ZIP_COMPRESSED));

		assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE)).isEqualTo(APPLICATION_JSON);
		assertThat(response.readEntity(String.class)).contains("uuid");
	}

	@Test
	void verifyThat_validationWithInvalidName_Response_IsStatusCode400() throws URISyntaxException, IOException {
		final byte[] data = Files.readAllBytes(Paths.get(ValidateApi2Test.class.getResource("/xplan.gml").toURI()));
		final Response response = target("/validate").queryParam("name", "invalid.name with blanks")
			.request()
			.accept(APPLICATION_JSON)
			.post(Entity.entity(data, TEXT_XML));

		assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
	}

	@Test
	void verifyThat_validationXZipCompressedWithInvalidName_Response_IsStatusCode400()
			throws URISyntaxException, IOException {
		final byte[] data = Files
			.readAllBytes(Paths.get(ValidateApi2Test.class.getResource("/bplan_valid_41.zip").toURI()));
		final Response response = target("/validate").queryParam("name", "invalid.name with blanks")
			.request()
			.accept(APPLICATION_JSON)
			.post(Entity.entity(data, APPLICATION_X_ZIP_COMPRESSED));

		assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
	}

}
