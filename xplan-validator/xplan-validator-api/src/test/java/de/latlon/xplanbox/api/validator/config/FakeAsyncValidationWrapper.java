/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.config;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import de.latlon.core.validator.events.ValidationRequestedEvent;
import de.latlon.xplanbox.api.commons.handler.AbstractAsyncValidationWrapper;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage.ReportType;
import org.springframework.beans.factory.annotation.Autowired;

public class FakeAsyncValidationWrapper extends AbstractAsyncValidationWrapper {

	@Autowired
	private ValidationExecutionStorage validationExecutionStorage;

	private Path reportForNextValidateRequest;

	private ValidationRequestedEvent lastValidatedEvent;

	FakeAsyncValidationWrapper() {
		super(null);
	}

	@Override
	public boolean sendEvent(ValidationRequestedEvent event) {
		lastValidatedEvent = (ValidationRequestedEvent) event;

		try {
			Map<ReportType, Path> reports = new HashMap<>();
			reports.put(ReportType.JSON, reportForNextValidateRequest);
			validationExecutionStorage.saveValidationResult(event.getUuid(), reports);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}

		return true;
	}

	public void saveReportForNextValidateRequest(Path validationResult) {
		reportForNextValidateRequest = validationResult;
	}

	public ValidationRequestedEvent getLastValidatedEvent() {
		return lastValidatedEvent;
	}

}
