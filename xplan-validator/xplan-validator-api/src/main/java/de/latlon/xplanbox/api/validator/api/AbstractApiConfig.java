/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.api;

import java.util.Collections;
import java.util.Optional;

import de.latlon.xplanbox.api.commons.ObjectMapperContextResolver;
import de.latlon.xplanbox.api.commons.converter.StringListConverterProvider;
import de.latlon.xplanbox.api.commons.exception.ConstraintViolationExceptionMapper;
import de.latlon.xplanbox.api.commons.exception.EventExecutionExceptionMapper;
import de.latlon.xplanbox.api.commons.exception.UnsupportedContentTypeExceptionMapper;
import de.latlon.xplanbox.api.commons.exception.ValidatorExceptionMapper;
import de.latlon.xplanbox.api.commons.exception.XPlanApiExceptionMapper;
import de.latlon.xplanbox.api.validator.config.ValidatorApiConfiguration;
import de.latlon.xplanbox.security.openapi.BearerSecurityOpenApiWriter;
import io.swagger.v3.jaxrs2.integration.resources.BaseOpenApiResource;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.tags.Tag;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.core.Context;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

/**
 * Application configuration for XPlanValidator REST API. Example mapping for proxy
 * mapping: http://xplanbox.lat-lon.de/xvalidator/api/v1/ ->
 * http://host:8080/xplan-api-validator/xvalidator/api/v1/ Public address:
 * http://xplanbox.lat-lon.de/xvalidator/ Internal address:
 * http://host:8080/xplan-api-validator/xvalidator/
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public abstract class AbstractApiConfig extends ResourceConfig {

	public AbstractApiConfig(@Context ServletContext servletContext,
			ValidatorApiConfiguration validatorApiConfiguration,
			Optional<BearerSecurityOpenApiWriter> bearerSecurityOpenApiWriter) {
		property(ServerProperties.WADL_FEATURE_DISABLE, true);

		register(new ObjectMapperContextResolver());

		BaseOpenApiResource openApiResource = createDefaultApi(servletContext, validatorApiConfiguration,
				bearerSecurityOpenApiWriter);
		register(openApiResource);

		register(ConstraintViolationExceptionMapper.class);
		register(UnsupportedContentTypeExceptionMapper.class);
		register(EventExecutionExceptionMapper.class);
		register(ValidatorExceptionMapper.class);
		register(XPlanApiExceptionMapper.class);
		register(StringListConverterProvider.class);
	}

	protected abstract BaseOpenApiResource createDefaultApi(ServletContext servletContext,
			ValidatorApiConfiguration validatorApiConfiguration,
			Optional<BearerSecurityOpenApiWriter> bearerSecurityOpenApiWriter);

	protected abstract void addInfo(OpenAPI openApi, ValidatorApiConfiguration validatorApiConfiguration);

	protected OpenAPI createOpenAPI(ServletContext servletContext, ValidatorApiConfiguration validatorApiConfiguration,
			Optional<BearerSecurityOpenApiWriter> bearerSecurityOpenApiWriter, String apiPath) {
		OpenAPI openApi = new OpenAPI();
		addInfo(openApi, validatorApiConfiguration);
		addContact(openApi, validatorApiConfiguration);
		addServers(openApi, servletContext, validatorApiConfiguration, apiPath);
		addTag(openApi, validatorApiConfiguration);
		bearerSecurityOpenApiWriter.ifPresent(apiSecurity -> apiSecurity.addSecurity(openApi));
		return openApi;
	}

	private void addTag(OpenAPI openApi, ValidatorApiConfiguration validatorApiConfiguration) {
		Tag tag = new Tag().name("validate").description("Validate XPlanGML documents");
		if (validatorApiConfiguration != null && validatorApiConfiguration.getDocumentationUrl() != null) {
			tag.externalDocs(new ExternalDocumentation().description("xPlanBox")
				.url(validatorApiConfiguration.getDocumentationUrl()));
		}
		openApi.tags(Collections.singletonList(tag));
	}

	private void addContact(OpenAPI openApi, ValidatorApiConfiguration validatorApiConfiguration) {
		if (validatorApiConfiguration != null && validatorApiConfiguration.getContactEMailAddress() != null) {
			String contactEMailAddress = validatorApiConfiguration.getContactEMailAddress();
			openApi.getInfo().setContact(new Contact().email(contactEMailAddress));
		}
	}

	private void addServers(OpenAPI openApi, ServletContext servletContext,
			ValidatorApiConfiguration validatorApiConfiguration, String apiPath) {
		String serverUrl = getServerUrl(servletContext, validatorApiConfiguration, apiPath);
		Server server = new Server().url(serverUrl);
		openApi.servers(Collections.singletonList(server));
	}

	private String getServerUrl(ServletContext servletContext, ValidatorApiConfiguration validatorApiConfiguration,
			String apiPath) {
		StringBuilder serverUrl = new StringBuilder();
		if (validatorApiConfiguration != null && validatorApiConfiguration.getApiUrl() != null) {
			String apiEndpoint = validatorApiConfiguration.getApiUrl().toString();
			serverUrl.append(apiEndpoint);
		}
		else {
			serverUrl.append(servletContext.getContextPath());
		}
		if (!serverUrl.toString().endsWith("/"))
			serverUrl.append("/");
		serverUrl.append(apiPath);
		return serverUrl.toString();
	}

	protected String getTermsOfService(ValidatorApiConfiguration validatorApiConfiguration) {
		if (validatorApiConfiguration != null)
			return validatorApiConfiguration.getTermsOfServiceUrl();
		return null;
	}

}
