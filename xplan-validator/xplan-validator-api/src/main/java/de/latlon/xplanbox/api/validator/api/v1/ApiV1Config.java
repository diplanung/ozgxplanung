/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.api.v1;

import static org.slf4j.LoggerFactory.getLogger;

import java.util.Optional;
import java.util.Set;

import de.latlon.xplanbox.api.commons.openapi.OpenApiFilter;
import de.latlon.xplanbox.api.validator.api.AbstractApiConfig;
import de.latlon.xplanbox.api.validator.config.ValidatorApiConfiguration;
import de.latlon.xplanbox.api.validator.v1.DefaultApi;
import de.latlon.xplanbox.api.validator.v1.InfoApi;
import de.latlon.xplanbox.api.validator.v1.ValidateApi;
import de.latlon.xplanbox.security.openapi.BearerSecurityOpenApiWriter;
import io.swagger.v3.jaxrs2.integration.resources.BaseOpenApiResource;
import io.swagger.v3.oas.integration.SwaggerConfiguration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Context;
import org.slf4j.Logger;

/**
 * Application configuration for XPlanValidator REST API. Example mapping for proxy
 * mapping: http://xplanbox.lat-lon.de/xvalidator/api/v1/ ->
 * http://host:8080/xplan-api-validator/xvalidator/api/v1/ Public address:
 * http://xplanbox.lat-lon.de/xvalidator/ Internal address:
 * http://host:8080/xplan-api-validator/xvalidator/
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
@ApplicationPath("/xvalidator/api/v1")
public class ApiV1Config extends AbstractApiConfig {

	private static final Logger LOG = getLogger(ApiV1Config.class);

	private static final String APP_PATH = "xvalidator/api/v1";

	public ApiV1Config(@Context ServletContext servletContext, ValidatorApiConfiguration validatorApiConfiguration,
			Optional<BearerSecurityOpenApiWriter> bearerSecurityOpenApiWriter) {
		super(servletContext, validatorApiConfiguration, bearerSecurityOpenApiWriter);

		register(InfoApi.class);
		register(ValidateApi.class);

		LOG.info("XPlanApiValidator v1 successfully initialized");
	}

	public BaseOpenApiResource createDefaultApi(ServletContext servletContext,
			ValidatorApiConfiguration validatorApiConfiguration,
			Optional<BearerSecurityOpenApiWriter> bearerSecurityOpenApiWriter) {
		DefaultApi openApiResourceV1 = new DefaultApi();
		OpenAPI v1OpenApi = createOpenAPI(servletContext, validatorApiConfiguration, bearerSecurityOpenApiWriter,
				APP_PATH);
		SwaggerConfiguration oasConfigV1 = new SwaggerConfiguration().openAPI(v1OpenApi)
			.filterClass(OpenApiFilter.class.getCanonicalName())
			.prettyPrint(true)
			.resourcePackages(Set.of("de.latlon.xplanbox.api.validator.v1"));
		openApiResourceV1.setOpenApiConfiguration(oasConfigV1);
		return openApiResourceV1;
	}

	protected void addInfo(OpenAPI openApi, ValidatorApiConfiguration validatorApiConfiguration) {
		openApi.setInfo(new Info().title("XPlanValidatorAPI")
			.version("1.4.0")
			.description("XPlanValidator REST API")
			.termsOfService(getTermsOfService(validatorApiConfiguration))
			.license(new License().name("Apache 2.0").url("http://www.apache.org/licenses/LICENSE-2.0.html")));
	}

}
