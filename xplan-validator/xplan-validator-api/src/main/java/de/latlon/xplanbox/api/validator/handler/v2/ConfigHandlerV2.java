/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.handler.v2;

import static org.slf4j.LoggerFactory.getLogger;

import java.io.IOException;
import java.util.List;

import de.latlon.xplanbox.api.commons.handler.SystemConfigHandler;
import de.latlon.xplanbox.api.commons.v2.model.SystemConfig;
import de.latlon.xplanbox.api.validator.config.ValidatorApiConfiguration;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@Component
@Singleton
public class ConfigHandlerV2 {

	private static final Logger LOG = getLogger(ConfigHandlerV2.class);

	@Autowired
	private List<String> activatedProfiles;

	@Autowired
	private SystemConfigHandler systemConfigHandler;

	@Autowired
	private ValidatorApiConfiguration validatorApiConfiguration;

	public SystemConfig describeSystem() throws IOException {
		LOG.debug("Generating validator config information");
		return new SystemConfig().version(parseVersion())
			.profiles(activatedProfiles)
			.supportedXPlanGmlVersions(systemConfigHandler.allSupportedVersions())
			.skipRasterEvaluation(validatorApiConfiguration.isSkipRasterEvaluation());
	}

	public String parseVersion() {
		Package thisPackage = getClass().getPackage();
		return thisPackage.getImplementationVersion();
	}

}
