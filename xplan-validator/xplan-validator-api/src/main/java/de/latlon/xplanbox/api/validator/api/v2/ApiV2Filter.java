/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.api.v2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.latlon.xplanbox.api.commons.openapi.OpenApiFilter;

/**
 * Filter to keep only v2 Api elements.
 *
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
public class ApiV2Filter extends OpenApiFilter {

	protected String createNewKey(String path) {
		Pattern pattern = Pattern.compile("\\/api\\/v[\\d_\\-\\.]*(\\/|)");
		Matcher matcher = pattern.matcher(path);
		return matcher.replaceFirst("/");
	}

}
