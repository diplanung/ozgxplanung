/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.v2.model;

import de.latlon.xplanbox.api.commons.v2.model.ResponseLink;

/**
 * A receipt for the submission of a validation request.
 *
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
public class ValidationReceipt {

	private final String uuid;

	private final ResponseLink statusLink;

	public ValidationReceipt(String uuid, ResponseLink statusLink) {
		this.uuid = uuid;
		this.statusLink = statusLink;
	}

	public String getUuid() {
		return uuid;
	}

	public ResponseLink getStatusLink() {
		return statusLink;
	}

}
