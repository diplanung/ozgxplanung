/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.api.v2;

import static org.slf4j.LoggerFactory.getLogger;

import java.util.Optional;
import java.util.Set;

import de.latlon.xplanbox.api.validator.api.AbstractApiConfig;
import de.latlon.xplanbox.api.validator.config.ValidatorApiConfiguration;
import de.latlon.xplanbox.api.validator.v2.DefaultApi2;
import de.latlon.xplanbox.api.validator.v2.InfoApi2;
import de.latlon.xplanbox.api.validator.v2.ReportApi;
import de.latlon.xplanbox.api.validator.v2.StatusApi;
import de.latlon.xplanbox.api.validator.v2.ValidateApi2;
import de.latlon.xplanbox.security.openapi.BearerSecurityOpenApiWriter;
import io.swagger.v3.jaxrs2.integration.resources.BaseOpenApiResource;
import io.swagger.v3.oas.integration.SwaggerConfiguration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Context;
import org.slf4j.Logger;

/**
 * Application configuration for XPlanValidator REST API. Example mapping for proxy
 * mapping: http://xplanbox.lat-lon.de/xvalidator/api/v1/ ->
 * http://host:8080/xplan-api-validator/xvalidator/api/v1/ Public address:
 * http://xplanbox.lat-lon.de/xvalidator/ Internal address:
 * http://host:8080/xplan-api-validator/xvalidator/
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
@ApplicationPath("/api/v2")
public class ApiV2Config extends AbstractApiConfig {

	private static final Logger LOG = getLogger(ApiV2Config.class);

	private static final String APP_PATH = "api/v2";

	public ApiV2Config(@Context ServletContext servletContext, ValidatorApiConfiguration validatorApiConfiguration,
			Optional<BearerSecurityOpenApiWriter> bearerSecurityOpenApiWriter) {
		super(servletContext, validatorApiConfiguration, bearerSecurityOpenApiWriter);

		register(InfoApi2.class);
		register(ValidateApi2.class);
		register(StatusApi.class);
		register(ReportApi.class);

		LOG.info("XPlanApiValidator v2 successfully initialized");
	}

	@Override
	protected BaseOpenApiResource createDefaultApi(ServletContext servletContext,
			ValidatorApiConfiguration validatorApiConfiguration,
			Optional<BearerSecurityOpenApiWriter> bearerSecurityOpenApiWriter) {
		DefaultApi2 openApiResourceV2 = new DefaultApi2();
		OpenAPI v2OpenApi = createOpenAPI(servletContext, validatorApiConfiguration, bearerSecurityOpenApiWriter,
				APP_PATH);
		SwaggerConfiguration oasConfigV2 = new SwaggerConfiguration().openAPI(v2OpenApi)
			.filterClass(ApiV2Filter.class.getCanonicalName())
			.prettyPrint(true)
			.resourcePackages(Set.of("de.latlon.xplanbox.api.validator.v2"));
		openApiResourceV2.setOpenApiConfiguration(oasConfigV2);
		return openApiResourceV2;
	}

	protected void addInfo(OpenAPI openApi, ValidatorApiConfiguration validatorApiConfiguration) {
		openApi.setInfo(new Info().title("XPlanValidatorAPI")
			.version("2.0.0")
			.description("XPlanValidator REST API v2")
			.termsOfService(getTermsOfService(validatorApiConfiguration))
			.license(new License().name("Apache 2.0").url("http://www.apache.org/licenses/LICENSE-2.0.html")));
	}

}
