/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.v1;

import static io.swagger.v3.oas.integration.api.OpenApiContext.OPENAPI_CONTEXT_ID_DEFAULT;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

import io.swagger.v3.jaxrs2.integration.resources.BaseOpenApiResource;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.servlet.ServletConfig;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

/**
 * Controller class for handling access to the default resource returning the OpenAPI
 * document.
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 4.0
 * @deprecated since 8.0 use API V2 instead
 */
@Deprecated
@Path("/")
@jakarta.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen",
		date = "2020-08-27T12:32:04.497+02:00[Europe/Berlin]")
public class DefaultApi extends BaseOpenApiResource {

	@Context
	private ServletConfig config;

	@Context
	private Application app;

	@GET
	@Produces({ "application/json" })
	@Operation(summary = "OpenAPI document", description = "API documentation",
			responses = { @ApiResponse(responseCode = "200", description = "successful operation"),
					@ApiResponse(responseCode = "406", description = "Requested format is not available") })
	public Response openApi(@Context HttpHeaders headers, @Context UriInfo uriInfo) throws Exception {
		return super.getOpenApi(headers, this.config, this.app, uriInfo, APPLICATION_JSON);
	}

	@Override
	protected String getContextId(ServletConfig config) {
		String contextId = super.getContextId(config);
		if (OPENAPI_CONTEXT_ID_DEFAULT.equals(contextId))
			return contextId + getClass().getSimpleName();
		return contextId;
	}

}
