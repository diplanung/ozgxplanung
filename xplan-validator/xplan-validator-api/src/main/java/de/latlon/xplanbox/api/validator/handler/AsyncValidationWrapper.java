/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.handler;

import de.latlon.core.validator.events.EventSender;
import de.latlon.core.validator.events.ValidationFinishedEvent;
import de.latlon.xplanbox.api.commons.handler.AbstractAsyncValidationWrapper;
import jakarta.inject.Singleton;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
@Component
@Singleton
public class AsyncValidationWrapper extends AbstractAsyncValidationWrapper {

	protected AsyncValidationWrapper(EventSender validationRequestNotifier) {
		super(validationRequestNotifier);
	}

	@RabbitListener(queues = "#{privateTopicQueue.name}")
	public void someValidationFinished(ValidationFinishedEvent t) {
		someEventFinished(t.getUuid());
	}

}
