/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.v1;

import static de.latlon.xplan.commons.util.ContentTypeChecker.checkContentTypesOfXPlanArchiveOrGml;
import static de.latlon.xplan.commons.util.TextPatternConstants.SIMPLE_NAME_PATTERN;
import static de.latlon.xplanbox.api.commons.ValidatorConverter.createValidationSettings;
import static de.latlon.xplanbox.api.commons.ValidatorConverter.detectOrCreateValidationName;
import static de.latlon.xplanbox.api.commons.XPlanMediaType.APPLICATION_PDF;
import static de.latlon.xplanbox.api.commons.XPlanMediaType.APPLICATION_PDF_TYPE;
import static de.latlon.xplanbox.api.commons.XPlanMediaType.APPLICATION_ZIP;
import static de.latlon.xplanbox.api.commons.XPlanMediaType.APPLICATION_ZIP_TYPE;
import static de.latlon.xplanbox.validator.storage.StatusType.VALIDATION_REQUESTED;
import static io.swagger.v3.oas.annotations.enums.Explode.FALSE;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static jakarta.ws.rs.core.MediaType.APPLICATION_XML;
import static jakarta.ws.rs.core.MediaType.APPLICATION_XML_TYPE;
import static jakarta.ws.rs.core.MediaType.TEXT_XML;
import static jakarta.ws.rs.core.MediaType.TEXT_XML_TYPE;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.latlon.core.validator.events.ValidationRequestedEvent;
import de.latlon.xplan.validator.web.shared.ValidationSettings;
import de.latlon.xplanbox.api.commons.ObjectMapperContextResolver;
import de.latlon.xplanbox.api.commons.ValidationReportConverter;
import de.latlon.xplanbox.api.commons.handler.AbstractAsyncValidationWrapper;
import de.latlon.xplanbox.api.commons.v1.model.ValidationReport;
import de.latlon.xplanbox.api.validator.config.ValidatorApiConfiguration;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Variant;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Controller class for handling access to the validate resource.
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @author <a href="mailto:friebe@lat-lon.de">Torsten Friebe</a>
 * @since 4.0
 * @deprecated since 8.0 use API V2 instead
 */
@Deprecated
@Path("/validate")
public class ValidateApi {

	@Autowired
	ValidationExecutionStorage validationExecutionStorage;

	@Autowired
	AbstractAsyncValidationWrapper validationWrapper;

	@Autowired
	ValidatorApiConfiguration validatorApiConfiguration;

	@POST
	@Consumes({ "text/xml", "application/gml+xml" })
	@Produces({ "application/json", "application/xml", "text/xml", "application/pdf", "application/zip" })
	@Operation(summary = "Validate XPlanGML or XPlanArchive", description = "Validates XPlanGML or XPlanArchive file",
			tags = { "validate" },
			responses = {
					@ApiResponse(responseCode = "200", description = "ValidationReport", content = {
							@Content(mediaType = APPLICATION_JSON,
									schema = @Schema(implementation = ValidationReport.class)),
							@Content(mediaType = APPLICATION_XML,
									schema = @Schema(implementation = ValidationReport.class)),
							@Content(mediaType = TEXT_XML, schema = @Schema(implementation = ValidationReport.class)),
							@Content(mediaType = APPLICATION_PDF, schema = @Schema(type = "string", format = "binary")),
							@Content(mediaType = APPLICATION_ZIP,
									schema = @Schema(type = "string", format = "binary",
											description = "XPlanGML or XPlanArchive (application/zip) file to upload",
											implementation = Object.class)) }),
					@ApiResponse(responseCode = "400", description = "Invalid input"),
					@ApiResponse(responseCode = "406", description = "Requested format is not available"),
					@ApiResponse(responseCode = "415",
							description = "Unsupported media type or content - only xml/gml, zip are accepted; all zip files entries must also match the supported content types for XPlanArchives"),
					@ApiResponse(responseCode = "422",
							description = "Invalid content - the content of the XPlanGML file must conform to the specification of xPlanBox XPlanGML files") },
			requestBody = @RequestBody(content = {
					@Content(mediaType = "application/octet-stream",
							schema = @Schema(type = "string", format = "binary",
									description = "XPlanGML or XPlanArchive (application/zip) file to upload")),
					@Content(mediaType = "application/zip",
							schema = @Schema(type = "string", format = "binary",
									description = "XPlanGML or XPlanArchive (application/zip) file to upload")),
					@Content(mediaType = "application/x-zip",
							schema = @Schema(type = "string", format = "binary",
									description = "XPlanGML or XPlanArchive (application/zip) file to upload")),
					@Content(mediaType = "application/x-zip-compressed",
							schema = @Schema(type = "string", format = "binary",
									description = "XPlanGML or XPlanArchive (application/zip) file to upload")),
					@Content(mediaType = "text/xml",
							schema = @Schema(type = "string", format = "binary", description = "XPlanGML to upload")),
					@Content(mediaType = "application/gml+xml", schema = @Schema(type = "string", format = "binary",
							description = "XPlanGML to upload")) }))
	public Response validate(@Context Request request, @Valid File body,
			@HeaderParam("X-Filename") @Parameter(description = "Name of the file to be uploaded",
					example = "File names such as xplan.gml, xplan.xml, xplan.zip",
					schema = @Schema(pattern = SIMPLE_NAME_PATTERN)) String xFilename,
			@QueryParam("name") @Parameter(description = "Name of the validation",
					schema = @Schema(pattern = SIMPLE_NAME_PATTERN),
					example = "xplan-1Pruefbericht_Torstrasse_10_report-4223") String name,
			@QueryParam("skipSemantisch") @DefaultValue("false") @Parameter(
					description = "skip semantische Validierung") Boolean skipSemantisch,
			@QueryParam("skipGeometrisch") @DefaultValue("false") @Parameter(
					description = "skip geometrische Validierung") Boolean skipGeometrisch,
			@QueryParam("skipFlaechenschluss") @DefaultValue("false") @Parameter(
					description = "skip Flaechenschluss Ueberpruefung") Boolean skipFlaechenschluss,
			@QueryParam("skipGeltungsbereich") @DefaultValue("false") @Parameter(
					description = "skip Geltungsbereich Ueberpruefung") Boolean skipGeltungsbereich,
			@QueryParam("skipLaufrichtung") @DefaultValue("false") @Parameter(
					description = "skip Laufrichtung Ueberpruefung") Boolean skipLaufrichtung,
			@QueryParam("profiles") @Parameter(
					description = "Names of profiles which shall be additionaly used for validation",
					explode = FALSE) List<String> profiles)
			throws Exception {
		checkContentTypesOfXPlanArchiveOrGml(body.toPath());
		String validationName = detectOrCreateValidationName(xFilename, name);

		return doValidate(body, request, xFilename, validationName, skipSemantisch, skipGeometrisch,
				skipFlaechenschluss, skipGeltungsbereich, skipLaufrichtung, profiles,
				ValidationRequestedEvent.OriginFile.GML);
	}

	@POST
	@Consumes({ "application/octet-stream", "application/zip", "application/x-zip", "application/x-zip-compressed" })
	@Produces({ "application/json", "application/xml", "text/xml", "application/pdf", "application/zip" })
	@Hidden
	public Response validateZip(@Context Request request, @Valid File body,
			@HeaderParam("X-Filename") @Parameter(schema = @Schema(pattern = SIMPLE_NAME_PATTERN)) String xFilename,
			@QueryParam("name") @Parameter(schema = @Schema(pattern = SIMPLE_NAME_PATTERN)) String name,
			@QueryParam("skipSemantisch") @DefaultValue("false") Boolean skipSemantisch,
			@QueryParam("skipGeometrisch") @DefaultValue("false") Boolean skipGeometrisch,
			@QueryParam("skipFlaechenschluss") @DefaultValue("false") Boolean skipFlaechenschluss,
			@QueryParam("skipGeltungsbereich") @DefaultValue("false") Boolean skipGeltungsbereich,
			@QueryParam("skipLaufrichtung") @DefaultValue("false") Boolean skipLaufrichtung,
			@QueryParam("profiles") @Parameter(
					description = "Names of profiles which shall be additionaly used for validation",
					explode = FALSE) List<String> profiles)
			throws Exception {

		return doValidate(body, request, xFilename, name, skipSemantisch, skipGeometrisch, skipFlaechenschluss,
				skipGeltungsbereich, skipLaufrichtung, profiles, ValidationRequestedEvent.OriginFile.ZIP);
	}

	private Response doValidate(File body, Request request, String xFilename, String name, Boolean skipSemantisch,
			Boolean skipGeometrisch, Boolean skipFlaechenschluss, Boolean skipGeltungsbereich, Boolean skipLaufrichtung,
			List<String> profiles, ValidationRequestedEvent.OriginFile originFile) throws Exception {
		checkContentTypesOfXPlanArchiveOrGml(body.toPath());

		String uuid = validationExecutionStorage.addPlanToValidate(body.toPath(), VALIDATION_REQUESTED);
		String validationName = detectOrCreateValidationName(xFilename, name);
		MediaType mediaType = detectRequestedMediaType(request);
		final ValidationRequestedEvent.MediaType requestedMediaType = detectValidationEventMediaType(mediaType);

		ValidationSettings settings = createValidationSettings(validationName, skipGeometrisch, skipSemantisch,
				skipFlaechenschluss, skipGeltungsbereich, skipLaufrichtung, profiles,
				validatorApiConfiguration.isSkipRasterEvaluation());

		ValidationRequestedEvent event = new ValidationRequestedEvent(uuid, settings, xFilename, requestedMediaType,
				originFile);

		return validate(event, mediaType);
	}

	private Response validate(ValidationRequestedEvent validationRequestedEvent, MediaType mediaType) throws Exception {

		ValidationSettings settings = validationRequestedEvent.getSettings();
		validationWrapper.sendEvent(validationRequestedEvent);

		ValidationRequestedEvent.MediaType requestedMediaType = validationRequestedEvent.getRequestedMediaType();
		ValidationExecutionStorage.ReportType reportType = ValidationExecutionStorage.ReportType
			.byFileExtension(requestedMediaType.getFileExtension());
		byte[] report = validationExecutionStorage.retrieveReport(validationRequestedEvent.getUuid(), reportType);
		String filename = settings.getValidationName() + requestedMediaType.getFileExtension();
		// xml not directly supported anymore => parse and convert
		if (APPLICATION_JSON_TYPE.equals(mediaType) || APPLICATION_XML_TYPE.equals(mediaType)
				|| TEXT_XML_TYPE.equals(mediaType)) {
			de.latlon.xplanbox.api.commons.v2.model.ValidationReport validationReportV2 = createReportFileFrom(report);
			ValidationReport validationReport = ValidationReportConverter.convertToV1(validationReportV2);
			return Response.ok(validationReport) //
				.type(mediaType) //
				.header("Content-Disposition", "attachment; filename=\"" + filename + "\"") //
				.build();
		}

		return Response.ok(report) //
			.type(mediaType) //
			.header("Content-Disposition", "attachment; filename=\"" + filename + "\"") //
			.build();
	}

	private MediaType detectRequestedMediaType(Request request) {
		Variant.VariantListBuilder acceptedMediaTypes = Variant.mediaTypes(APPLICATION_JSON_TYPE, APPLICATION_XML_TYPE,
				TEXT_XML_TYPE, APPLICATION_PDF_TYPE, APPLICATION_ZIP_TYPE);
		Variant selectVariant = request.selectVariant(acceptedMediaTypes.build());
		if (selectVariant == null)
			return APPLICATION_JSON_TYPE;
		return selectVariant.getMediaType();
	}

	private static ValidationRequestedEvent.MediaType detectValidationEventMediaType(MediaType mediaType) {
		if (APPLICATION_ZIP_TYPE.equals(mediaType))
			return ValidationRequestedEvent.MediaType.ZIP;
		if (APPLICATION_PDF_TYPE.equals(mediaType))
			return ValidationRequestedEvent.MediaType.PDF;
		return ValidationRequestedEvent.MediaType.JSON;
	}

	private de.latlon.xplanbox.api.commons.v2.model.ValidationReport createReportFileFrom(byte[] reportAsJsonInByte)
			throws IOException {
		ObjectMapper mapper = new ObjectMapperContextResolver()
			.getContext(de.latlon.xplanbox.api.commons.v2.model.ValidationReport.class);
		return mapper.readValue(reportAsJsonInByte, de.latlon.xplanbox.api.commons.v2.model.ValidationReport.class);
	}

}
