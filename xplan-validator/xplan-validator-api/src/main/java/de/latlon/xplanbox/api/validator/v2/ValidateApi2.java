/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.validator.v2;

import static de.latlon.xplan.commons.util.ContentTypeChecker.checkContentTypesOfXPlanArchiveOrGml;
import static de.latlon.xplan.commons.util.TextPatternConstants.SIMPLE_NAME_PATTERN;
import static de.latlon.xplanbox.api.commons.ValidatorConverter.createValidationSettings;
import static de.latlon.xplanbox.api.commons.ValidatorConverter.detectOrCreateValidationName;
import static de.latlon.xplanbox.api.commons.v2.model.ResponseLink.RelEnum.STATUS;
import static de.latlon.xplanbox.validator.storage.StatusType.VALIDATION_REQUESTED;
import static io.swagger.v3.oas.annotations.enums.Explode.FALSE;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import de.latlon.core.validator.events.EventSender;
import de.latlon.core.validator.events.ValidationRequestedEvent;
import de.latlon.xplan.commons.s3.StorageException;
import de.latlon.xplan.commons.util.UnsupportedContentTypeException;
import de.latlon.xplan.validator.ValidatorException;
import de.latlon.xplan.validator.web.shared.ValidationSettings;
import de.latlon.xplanbox.api.commons.exception.InvalidXPlanGmlOrArchive;
import de.latlon.xplanbox.api.commons.exception.UnsupportedHeaderValue;
import de.latlon.xplanbox.api.commons.exception.UnsupportedParameterValue;
import de.latlon.xplanbox.api.commons.v2.model.ResponseLink;
import de.latlon.xplanbox.api.validator.config.ValidatorApiConfiguration;
import de.latlon.xplanbox.api.validator.v2.model.ValidationReceipt;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.servlet.ServletContext;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Controller class for handling access to the validate resource.
 *
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
@Path("/validate")
public class ValidateApi2 {

	@Autowired
	private ValidationExecutionStorage validationExecutionStorage;

	@Autowired
	private EventSender validationRequestNotifier;

	@Autowired
	private ValidatorApiConfiguration validatorApiConfiguration;

	@Context
	private ServletContext servletContext;

	@POST
	@Consumes({ "text/xml", "application/gml+xml" })
	@Produces({ "application/json" })
	@Operation(summary = "Validate XPlanGML or XPlanArchive", description = "Validates XPlanGML or XPlanArchive file",
			tags = { "validate" },
			responses = {
					@ApiResponse(responseCode = "200", description = "ValidationReceipt with uuid of the validation",
							content = { @Content(mediaType = APPLICATION_JSON,
									schema = @Schema(implementation = ValidationReceipt.class)) }),
					@ApiResponse(responseCode = "400", description = "Invalid input"),
					@ApiResponse(responseCode = "406", description = "Requested format is not available"),
					@ApiResponse(responseCode = "415",
							description = "Unsupported media type or content - only xml/gml, zip are accepted; all zip files entries must also match the supported content types for XPlanArchives") },
			requestBody = @RequestBody(content = {
					@Content(mediaType = "application/octet-stream",
							schema = @Schema(type = "string", format = "binary",
									description = "XPlanGML or XPlanArchive (application/zip) file to upload")),
					@Content(mediaType = "application/zip",
							schema = @Schema(type = "string", format = "binary",
									description = "XPlanGML or XPlanArchive (application/zip) file to upload")),
					@Content(mediaType = "application/x-zip",
							schema = @Schema(type = "string", format = "binary",
									description = "XPlanGML or XPlanArchive (application/zip) file to upload")),
					@Content(mediaType = "application/x-zip-compressed",
							schema = @Schema(type = "string", format = "binary",
									description = "XPlanGML or XPlanArchive (application/zip) file to upload")),
					@Content(mediaType = "text/xml",
							schema = @Schema(type = "string", format = "binary", description = "XPlanGML to upload")),
					@Content(mediaType = "application/gml+xml", schema = @Schema(type = "string", format = "binary",
							description = "XPlanGML to upload")) }))
	public Response validate(@Valid File body,
			@HeaderParam("X-Filename") @Parameter(description = "Name of the file to be uploaded",
					example = "File names such as xplan.gml, xplan.xml, xplan.zip",
					schema = @Schema(pattern = SIMPLE_NAME_PATTERN)) String xFilename,
			@QueryParam("name") @Parameter(description = "Name of the validation",
					schema = @Schema(pattern = SIMPLE_NAME_PATTERN),
					example = "xplan-1Pruefbericht_Torstrasse_10_report-4223") String name,
			@QueryParam("skipSemantisch") @DefaultValue("false") @Parameter(
					description = "skip semantische Validierung") Boolean skipSemantisch,
			@QueryParam("skipGeometrisch") @DefaultValue("false") @Parameter(
					description = "skip geometrische Validierung") Boolean skipGeometrisch,
			@QueryParam("skipFlaechenschluss") @DefaultValue("false") @Parameter(
					description = "skip Flaechenschluss Ueberpruefung") Boolean skipFlaechenschluss,
			@QueryParam("skipGeltungsbereich") @DefaultValue("false") @Parameter(
					description = "skip Geltungsbereich Ueberpruefung") Boolean skipGeltungsbereich,
			@QueryParam("skipLaufrichtung") @DefaultValue("false") @Parameter(
					description = "skip Laufrichtung Ueberpruefung") Boolean skipLaufrichtung,
			@QueryParam("profiles") @Parameter(
					description = "Names of profiles which shall be additionally used for validation",
					explode = FALSE) List<String> profiles)
			throws IOException, ValidatorException, URISyntaxException, InvalidXPlanGmlOrArchive,
			UnsupportedContentTypeException, UnsupportedParameterValue, UnsupportedHeaderValue, StorageException {
		return doValidate(body, xFilename, name, skipSemantisch, skipGeometrisch, skipFlaechenschluss,
				skipGeltungsbereich, skipLaufrichtung, profiles, ValidationRequestedEvent.OriginFile.GML);
	}

	@POST
	@Consumes({ "application/octet-stream", "application/zip", "application/x-zip", "application/x-zip-compressed" })
	@Produces({ "application/json" })
	@Hidden
	public Response validateZip(@Valid File body,
			@HeaderParam("X-Filename") @Parameter(schema = @Schema(pattern = SIMPLE_NAME_PATTERN)) String xFilename,
			@QueryParam("name") @Parameter(schema = @Schema(pattern = SIMPLE_NAME_PATTERN)) String name,
			@QueryParam("skipSemantisch") @DefaultValue("false") Boolean skipSemantisch,
			@QueryParam("skipGeometrisch") @DefaultValue("false") Boolean skipGeometrisch,
			@QueryParam("skipFlaechenschluss") @DefaultValue("false") Boolean skipFlaechenschluss,
			@QueryParam("skipGeltungsbereich") @DefaultValue("false") Boolean skipGeltungsbereich,
			@QueryParam("skipLaufrichtung") @DefaultValue("false") Boolean skipLaufrichtung,
			@QueryParam("profiles") @Parameter(
					description = "Names of profiles which shall be additionally used for validation",
					explode = FALSE) List<String> profiles)
			throws IOException, UnsupportedContentTypeException, UnsupportedParameterValue, UnsupportedHeaderValue,
			StorageException {
		return doValidate(body, xFilename, name, skipSemantisch, skipGeometrisch, skipFlaechenschluss,
				skipGeltungsbereich, skipLaufrichtung, profiles, ValidationRequestedEvent.OriginFile.ZIP);
	}

	private Response doValidate(File body, String xFilename, String name, Boolean skipSemantisch,
			Boolean skipGeometrisch, Boolean skipFlaechenschluss, Boolean skipGeltungsbereich, Boolean skipLaufrichtung,
			List<String> profiles, ValidationRequestedEvent.OriginFile originFile) throws IOException,
			UnsupportedContentTypeException, StorageException, UnsupportedParameterValue, UnsupportedHeaderValue {
		checkContentTypesOfXPlanArchiveOrGml(body.toPath());

		String uuid = validationExecutionStorage.addPlanToValidate(body.toPath(), VALIDATION_REQUESTED);

		String validationName = detectOrCreateValidationName(xFilename, name);
		ValidationSettings settings = createValidationSettings(validationName, skipGeometrisch, skipSemantisch,
				skipFlaechenschluss, skipGeltungsbereich, skipLaufrichtung, profiles,
				validatorApiConfiguration.isSkipRasterEvaluation());

		validationRequestNotifier.sendPrivateEvent(new ValidationRequestedEvent(uuid, settings, xFilename,
				ValidationRequestedEvent.MediaType.JSON, originFile));

		URI linkToStatus = createLinkToStatus(uuid);
		ResponseLink statusLink = (ResponseLink) new ResponseLink().rel(STATUS)
			.href(linkToStatus)
			.type("application/json");
		ValidationReceipt receipt = new ValidationReceipt(uuid, statusLink);

		return Response.ok(receipt).build();
	}

	private URI createLinkToStatus(String uuid) {
		URI apiUrl = validatorApiConfiguration.getApiUrl();
		if (apiUrl == null)
			return null;
		return apiUrl.resolve(servletContext.getContextPath() + "/api/v2/status/" + uuid);
	}

}
