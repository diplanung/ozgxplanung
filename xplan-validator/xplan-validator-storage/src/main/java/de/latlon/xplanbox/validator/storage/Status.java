/*-
 * #%L
 * xplan-validator-storage - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.validator.storage;

import java.util.Date;
import java.util.List;

import de.latlon.xplanbox.validator.storage.exception.ErrorType;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class Status {

	private StatusType statusType;

	private String errorMsg;

	private ErrorType errorType;

	private Date expirationTime;

	private List<Integer> importedPlanIds;

	public Status() {
	}

	public Status(StatusType statusType) {
		this.statusType = statusType;
	}

	public StatusType getStatusType() {
		return statusType;
	}

	public void setStatusType(StatusType statusType) {
		this.statusType = statusType;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public ErrorType getErrorType() {
		return errorType;
	}

	public void setErrorType(ErrorType errorType) {
		this.errorType = errorType;
	}

	public void setReportLinkExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public List<Integer> getImportedPlanIds() {
		return importedPlanIds;
	}

	public void setImportedPlanIds(List<Integer> importedPlanIds) {
		this.importedPlanIds = importedPlanIds;
	}

}
