/*-
 * #%L
 * xplan-validator-storage - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.validator.storage.s3;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Map.Entry;

import de.latlon.xplan.commons.s3.S3Metadata;
import de.latlon.xplan.commons.s3.S3Object;
import de.latlon.xplan.commons.s3.S3Storage;
import de.latlon.xplan.commons.s3.StorageException;
import de.latlon.xplanbox.validator.storage.StoredValidationReport;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class S3PlanValidationExecutionStorage extends ValidationExecutionStorage {

	private final S3Storage s3Storage;

	public S3PlanValidationExecutionStorage(S3Storage s3Storage) {
		this.s3Storage = s3Storage;
	}

	@Override
	protected byte[] retrieveContent(Key key) throws StorageException {
		S3Object object = s3Storage.getObject(key.toFileName());
		return object.getContent();
	}

	@Override
	protected void addToStore(Key key, String contentType, byte[] content) throws StorageException {
		S3Metadata s3Metadata = new S3Metadata(key.toFileName(), contentType, content.length);
		S3Object s3Object = new S3Object(s3Metadata, content);
		s3Storage.insertObject(s3Object);
	}

	@Override
	protected void addToStore(Key key, Path file) throws StorageException {
		s3Storage.insertObject(key.toFileName(), file);
	}

	@Override
	protected void removeFromStore(Key key) {
		software.amazon.awssdk.services.s3.model.S3Object object = software.amazon.awssdk.services.s3.model.S3Object
			.builder()
			.key(key.toFileName())
			.build();
		s3Storage.deleteObject(object);
	}

	@Override
	public void writePlanToValidate(String uuid, Path toPath) throws StorageException {
		Key key = Key.plan(uuid);
		S3Object object = s3Storage.getObject(key.toFileName());
		try {
			Files.copy(new ByteArrayInputStream(object.getContent()), toPath);
		}
		catch (IOException e) {
			throw new StorageException("Could not write plan to file" + toPath, e);
		}
	}

	@Override
	public StoredValidationReport saveValidationResult(String uuid, Map<ReportType, Path> reports)
			throws StorageException {
		PutObjectResponse lastPutObjectResult = null;
		for (Entry<ReportType, Path> entry : reports.entrySet()) {
			String fileName = Key.report(uuid, entry.getKey()).toFileName();
			lastPutObjectResult = s3Storage.insertObject(fileName, entry.getValue());
		}
		// TODO
		System.out.println(lastPutObjectResult.expiration());
		return new StoredValidationReport(null);
	}

}
