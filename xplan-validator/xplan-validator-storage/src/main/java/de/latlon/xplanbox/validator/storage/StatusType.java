/*-
 * #%L
 * xplan-validator-storage - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.validator.storage;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public enum StatusType {

	// Validation requested
	VALIDATION_REQUESTED,

	// Validation started
	VALIDATION_STARTED,

	// Validation failed
	VALIDATION_FAILED,

	// Validation finished
	VALIDATION_FINISHED,

	// Import requested
	IMPORT_REQUESTED,

	// Import started
	IMPORT_STARTED,

	// Import aborted due to incomplete XPlanArchive or invalid XPlanGML
	IMPORT_ABORTED,

	// Import failed
	IMPORT_FAILED,

	// Import finished
	IMPORT_FINISHED

}
