/*-
 * #%L
 * xplan-validator-storage - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.validator.storage;

import static de.latlon.xplanbox.validator.storage.StatusType.VALIDATION_FAILED;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.commons.s3.StorageException;
import de.latlon.xplanbox.validator.storage.exception.ErrorType;
import de.latlon.xplanbox.validator.storage.exception.EventExecutionException;
import org.apache.tika.Tika;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public abstract class ValidationExecutionStorage {

	public static final String APPLICATION_YAML = "application/yaml";

	public enum FileType {

		PLAN(""), DETAILS("_details.yaml"), STATUS("_status.yaml"), PLANS("_plans.json");

		private final String suffix;

		FileType(String suffix) {
			this.suffix = suffix;
		}

		public String getSuffix() {
			return suffix;
		}

	}

	public enum ReportType {

		JSON(".json"), PDF(".pdf"), GEOJSON(".geojson"), ZIP(".zip");

		private final String fileExtension;

		ReportType(String fileExtension) {
			this.fileExtension = fileExtension;
		}

		public static ReportType byFileExtension(String fileExtension) {
			Optional<ReportType> reportType = Arrays.stream(values())
				.filter(rt -> rt.fileExtension.equals(fileExtension))
				.findFirst();
			return reportType.orElseThrow(
					() -> new IllegalArgumentException("Could not find ReportType with fileExtension" + fileExtension));
		}

		public String getFileExtension() {
			return fileExtension;
		}

	}

	static public class Key {

		private String fileName;

		Key(String uuid, FileType fileType) {
			this.fileName = uuid + fileType.getSuffix();
		}

		public Key(String uuid, ReportType reportType) {
			this.fileName = uuid + "_report" + reportType.getFileExtension();
		}

		public String toFileName() {
			return fileName;
		}

		public static Key details(String uuid) {
			return new Key(uuid, FileType.DETAILS);
		}

		public static Key status(String uuid) {
			return new Key(uuid, FileType.STATUS);
		}

		public static Key plan(String uuid) {
			return new Key(uuid, FileType.PLAN);
		}

		public static Key plans(String uuid) {
			return new Key(uuid, FileType.PLANS);
		}

		public static Key report(String uuid, ReportType reportType) {
			return new Key(uuid, reportType);
		}

	}

	public String addPlanToValidate(Path plan, StatusType statusType) throws IOException, StorageException {
		String uuid = UUID.randomUUID().toString();
		ValidationDetails validationDetails = createValidationDetails(plan);
		byte[] serializedValidationDetails = serialize(validationDetails);
		addToStore(Key.details(uuid), APPLICATION_YAML, serializedValidationDetails);
		addToStore(Key.status(uuid), APPLICATION_YAML, serialize(new Status(statusType)));
		addToStore(Key.plan(uuid), plan);
		return uuid;
	}

	public void cleanupAfterValidation(String uuid) throws StorageException {
		removeFromStore(Key.details(uuid));
		removeFromStore(Key.plan(uuid));
	}

	public Status retrieveStatus(String uuid) throws StorageException {
		byte[] content = retrieveContent(Key.status(uuid));
		return deserialize(content);
	}

	public void changeStatus(String uuid, StatusType statusType) throws StorageException {
		changeStatusType(uuid, statusType);
	}

	public void changeStatus(String uuid, StatusType statusType, String errorMsg) throws StorageException {
		Status status = retrieveStatus(uuid);
		status.setStatusType(statusType);
		status.setErrorMsg(errorMsg);
		addToStore(Key.status(uuid), APPLICATION_YAML, serialize(status));
	}

	public void changeStatus(String uuid, String errorMsg, ErrorType errorType, StatusType statusType)
			throws StorageException {
		Status status = retrieveStatus(uuid);
		status.setStatusType(statusType);
		status.setErrorMsg(errorMsg);
		status.setErrorType(errorType);
		addToStore(Key.status(uuid), APPLICATION_YAML, serialize(status));
	}

	public void changeStatus(String uuid, StatusType statusType, StoredValidationReport storedValidationReport)
			throws StorageException {
		Status status = retrieveStatus(uuid);
		status.setStatusType(statusType);
		if (storedValidationReport != null) {
			status.setReportLinkExpirationTime(storedValidationReport.expirationTime());
		}
		addToStore(Key.status(uuid), APPLICATION_YAML, serialize(status));
	}

	public void changeStatus(String uuid, StatusType statusType, List<Integer> importedPlanIds)
			throws StorageException {
		Status status = retrieveStatus(uuid);
		status.setStatusType(statusType);
		status.setImportedPlanIds(importedPlanIds);
		addToStore(Key.status(uuid), APPLICATION_YAML, serialize(status));
	}

	public void changeStatusType(String uuid, StatusType statusType) throws StorageException {
		Status status = retrieveStatus(uuid);
		status.setStatusType(statusType);
		addToStore(Key.status(uuid), APPLICATION_YAML, serialize(status));
	}

	public byte[] retrieveReport(String uuid, ReportType reportType) throws StorageException, EventExecutionException {
		Status status = retrieveStatus(uuid);
		if (status.getStatusType() == VALIDATION_FAILED)
			throw new EventExecutionException(status.getErrorMsg(), status.getErrorType());
		return retrieveContent(Key.report(uuid, reportType));
	}

	/**
	 * @param key of the file to return, never <code>null</code>
	 * @return the content of the file, never <code>null</code>
	 * @throws StorageException if an error occured retrieving the file or the file does
	 * not exist
	 */
	protected abstract byte[] retrieveContent(Key key) throws StorageException;

	protected abstract void addToStore(Key key, String contentType, byte[] file) throws StorageException;

	protected abstract void addToStore(Key key, Path file) throws IOException, StorageException;

	protected abstract void removeFromStore(Key key) throws StorageException;

	/**
	 * ugly name to improve
	 * @throws IOException
	 */
	public abstract void writePlanToValidate(String uuid, Path toPath) throws IOException, StorageException;

	/**
	 * ugly name to improve
	 * @return
	 * @throws IOException
	 */
	public abstract StoredValidationReport saveValidationResult(String uuid, Map<ReportType, Path> reports)
			throws IOException, StorageException;

	private ValidationDetails createValidationDetails(Path plan) throws IOException {
		ValidationDetails validationDetails = new ValidationDetails();
		if (isZipFile(plan)) {
			try (ZipFile zipFile = new ZipFile(plan.toFile())) {
				Enumeration<? extends ZipEntry> entries = zipFile.entries();
				while (entries.hasMoreElements()) {
					ZipEntry entry = entries.nextElement();
					validationDetails.addAttachment(entry.getName());
				}
			}
		}
		else {
			validationDetails.addAttachment(XPlanArchiveCreator.MAIN_FILE);
		}
		return validationDetails;
	}

	private boolean isZipFile(Path path) throws IOException {
		String contentType = new Tika().detect(path);
		if ("application/zip".equals(contentType))
			return true;
		return false;
	}

	private byte[] serialize(ValidationDetails validationDetails) throws StorageException {
		try {
			ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
			return mapper.writeValueAsBytes(validationDetails);
		}
		catch (JsonProcessingException e) {
			throw new StorageException("Could not serialize ValidationDetails " + validationDetails, e);
		}
	}

	private byte[] serialize(Status statusNotification) throws StorageException {
		try {
			ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
			return mapper.writeValueAsBytes(statusNotification);
		}
		catch (JsonProcessingException e) {
			throw new StorageException("Could not serialize Status " + statusNotification, e);
		}
	}

	private Status deserialize(byte[] content) throws StorageException {
		try {
			ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
			return mapper.readValue(content, Status.class);
		}
		catch (IOException e) {
			throw new StorageException("Could not deserialize Status", e);
		}
	}

}
