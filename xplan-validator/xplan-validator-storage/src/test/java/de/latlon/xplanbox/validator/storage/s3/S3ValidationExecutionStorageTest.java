/*-
 * #%L
 * xplan-validator-storage - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.validator.storage.s3;

import static de.latlon.xplanbox.validator.storage.StatusType.VALIDATION_REQUESTED;
import static de.latlon.xplanbox.validator.storage.ValidationExecutionStorage.FileType.DETAILS;
import static de.latlon.xplanbox.validator.storage.ValidationExecutionStorage.FileType.STATUS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import de.latlon.xplan.commons.s3.S3Storage;
import de.latlon.xplan.commons.s3.StorageException;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage.Key;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage.ReportType;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.HeadBucketRequest;
import software.amazon.awssdk.services.s3.model.HeadBucketResponse;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
class S3ValidationExecutionStorageTest {

	private static final String BUCKET_NAME = "xplanbox";

	@Test
	void testAddPlanToValidate_gml() throws IOException, URISyntaxException, StorageException {
		S3Client client = createS3ClientSpy();
		S3Storage s3Storage = new S3Storage(client, BUCKET_NAME);
		S3PlanValidationExecutionStorage validationExecutionStorage = new S3PlanValidationExecutionStorage(s3Storage);
		URL xplanGml = getClass().getResource("/xplan.gml");
		String key = validationExecutionStorage.addPlanToValidate(Paths.get(xplanGml.toURI()), VALIDATION_REQUESTED);

		ArgumentCaptor<HeadBucketRequest> headBucketRequestCaptor = ArgumentCaptor.forClass(HeadBucketRequest.class);
		ArgumentCaptor<PutObjectRequest> putObjectRequestCaptor = ArgumentCaptor.forClass(PutObjectRequest.class);

		verify(client, times(3)).headBucket(headBucketRequestCaptor.capture());
		verify(client, times(3)).putObject(putObjectRequestCaptor.capture(), any(RequestBody.class));

		assertThat(headBucketRequestCaptor.getAllValues()).map(HeadBucketRequest::bucket)
			.containsExactlyInAnyOrder(BUCKET_NAME, BUCKET_NAME, BUCKET_NAME);
		assertThat(putObjectRequestCaptor.getAllValues()).map(PutObjectRequest::bucket)
			.containsExactlyInAnyOrder(BUCKET_NAME, BUCKET_NAME, BUCKET_NAME);
		assertThat(putObjectRequestCaptor.getAllValues()).map(PutObjectRequest::key)
			.containsExactlyInAnyOrder(key, key + DETAILS.getSuffix(), key + STATUS.getSuffix());
	}

	@Test
	void testAddPlanToValidate_zip() throws IOException, URISyntaxException, StorageException {
		S3Client client = createS3ClientSpy();
		S3Storage s3Storage = new S3Storage(client, BUCKET_NAME);
		S3PlanValidationExecutionStorage validationExecutionStorage = new S3PlanValidationExecutionStorage(s3Storage);
		URL xplanGml = getClass().getResource("/BPlan002_5-3.zip");
		String key = validationExecutionStorage.addPlanToValidate(Paths.get(xplanGml.toURI()), VALIDATION_REQUESTED);

		ArgumentCaptor<HeadBucketRequest> headBucketRequestCaptor = ArgumentCaptor.forClass(HeadBucketRequest.class);
		ArgumentCaptor<PutObjectRequest> putObjectRequestCaptor = ArgumentCaptor.forClass(PutObjectRequest.class);

		verify(client, times(3)).headBucket(headBucketRequestCaptor.capture());
		verify(client, times(3)).putObject(putObjectRequestCaptor.capture(), any(RequestBody.class));

		assertThat(headBucketRequestCaptor.getAllValues()).map(HeadBucketRequest::bucket)
			.containsExactlyInAnyOrder(BUCKET_NAME, BUCKET_NAME, BUCKET_NAME);
		assertThat(putObjectRequestCaptor.getAllValues()).map(PutObjectRequest::bucket)
			.containsExactlyInAnyOrder(BUCKET_NAME, BUCKET_NAME, BUCKET_NAME);
		assertThat(putObjectRequestCaptor.getAllValues()).map(PutObjectRequest::key)
			.containsExactlyInAnyOrder(key, key + DETAILS.getSuffix(), key + STATUS.getSuffix());
	}

	@Test
	void testSaveValidationResult() throws URISyntaxException, StorageException {
		S3Client client = createS3ClientSpy();
		S3Storage s3Storage = new S3Storage(client, BUCKET_NAME);
		S3PlanValidationExecutionStorage validationExecutionStorage = new S3PlanValidationExecutionStorage(s3Storage);
		URL xplanGml = getClass().getResource("/BPlan002_5-3.zip");
		String uuid = UUID.randomUUID().toString();

		Map<ReportType, Path> reports = new HashMap<>();
		reports.put(ReportType.JSON, Paths.get(xplanGml.toURI()));
		reports.put(ReportType.PDF, Paths.get(xplanGml.toURI()));
		validationExecutionStorage.saveValidationResult(uuid, reports);

		ArgumentCaptor<HeadBucketRequest> headBucketRequestCaptor = ArgumentCaptor.forClass(HeadBucketRequest.class);
		ArgumentCaptor<PutObjectRequest> putObjectRequestCaptor = ArgumentCaptor.forClass(PutObjectRequest.class);

		verify(client, times(2)).headBucket(headBucketRequestCaptor.capture());
		verify(client, times(2)).putObject(putObjectRequestCaptor.capture(), any(RequestBody.class));

		assertThat(headBucketRequestCaptor.getAllValues()).map(HeadBucketRequest::bucket)
			.containsExactlyInAnyOrder(BUCKET_NAME, BUCKET_NAME);
		assertThat(putObjectRequestCaptor.getAllValues()).map(PutObjectRequest::bucket)
			.containsExactlyInAnyOrder(BUCKET_NAME, BUCKET_NAME);
		assertThat(putObjectRequestCaptor.getAllValues()).map(PutObjectRequest::key)
			.containsExactlyInAnyOrder(Key.report(uuid, ReportType.PDF).toFileName(),
					Key.report(uuid, ReportType.JSON).toFileName());
	}

	private static S3Client createS3ClientSpy() {
		S3Client client = spy(S3Client.class);
		doReturn(HeadBucketResponse.builder().build()).when(client).headBucket(any(HeadBucketRequest.class));
		doReturn(PutObjectResponse.builder().build()).when(client)
			.putObject(any(PutObjectRequest.class), any(RequestBody.class));
		return client;
	}

}
