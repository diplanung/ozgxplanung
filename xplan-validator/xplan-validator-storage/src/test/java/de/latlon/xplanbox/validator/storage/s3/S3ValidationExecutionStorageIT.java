/*-
 * #%L
 * xplan-validator-storage - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.validator.storage.s3;

import static de.latlon.xplanbox.validator.storage.StatusType.VALIDATION_FAILED;
import static de.latlon.xplanbox.validator.storage.StatusType.VALIDATION_FINISHED;
import static de.latlon.xplanbox.validator.storage.StatusType.VALIDATION_REQUESTED;
import static de.latlon.xplanbox.validator.storage.StatusType.VALIDATION_STARTED;
import static de.latlon.xplanbox.validator.storage.exception.ErrorType.INTERNAL_ERROR;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Date;

import de.latlon.xplan.commons.s3.StorageException;
import de.latlon.xplanbox.validator.storage.Status;
import de.latlon.xplanbox.validator.storage.StoredValidationReport;
import de.latlon.xplanbox.validator.storage.ValidationExecutionStorage;
import de.latlon.xplanbox.validator.storage.config.AmazonS3ValidationContext;
import de.latlon.xplanbox.validator.storage.exception.ErrorType;
import de.latlon.xplanbox.validator.storage.exception.EventExecutionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { AmazonS3ValidationContext.class, TestS3Context.class })
@ActiveProfiles({ "mock", "test" })
@TestPropertySource("classpath:s3Mock.properties")
public class S3ValidationExecutionStorageIT {

	@Autowired
	private S3PlanValidationExecutionStorage validationExecutionStorage;

	@Test
	void testStatus() throws StorageException, URISyntaxException, IOException {
		URL xplanGml = getClass().getResource("/xplan.gml");

		String uuid = validationExecutionStorage.addPlanToValidate(Paths.get(xplanGml.toURI()), VALIDATION_REQUESTED);
		Status statusRequested = validationExecutionStorage.retrieveStatus(uuid);
		assertThat(statusRequested.getStatusType()).isEqualTo(VALIDATION_REQUESTED);

		validationExecutionStorage.changeStatus(uuid, VALIDATION_STARTED);
		Status statusStarted = validationExecutionStorage.retrieveStatus(uuid);
		assertThat(statusStarted.getStatusType()).isEqualTo(VALIDATION_STARTED);

		StoredValidationReport validationReportDetails = new StoredValidationReport(new Date());
		validationExecutionStorage.changeStatus(uuid, VALIDATION_FINISHED, validationReportDetails);
		Status statusFinished = validationExecutionStorage.retrieveStatus(uuid);
		assertThat(statusFinished.getStatusType()).isEqualTo(VALIDATION_FINISHED);
		assertThat(statusFinished.getExpirationTime()).isEqualTo(validationReportDetails.expirationTime());
	}

	@Test
	void testAddAndCleanup() throws StorageException, URISyntaxException, IOException {
		URL xplanGml = getClass().getResource("/xplan.gml");

		String uuid = validationExecutionStorage.addPlanToValidate(Paths.get(xplanGml.toURI()), VALIDATION_REQUESTED);
		validationExecutionStorage.cleanupAfterValidation(uuid);
	}

	@Test
	void testChangeStatus_toError() throws URISyntaxException, StorageException, IOException {
		URL xplanGml = getClass().getResource("/xplan.gml");
		String uuid = validationExecutionStorage.addPlanToValidate(Paths.get(xplanGml.toURI()), VALIDATION_REQUESTED);

		String errorMsg = "Error test";
		validationExecutionStorage.changeStatus(uuid, errorMsg, INTERNAL_ERROR, VALIDATION_FAILED);

		EventExecutionException exception = Assertions.assertThrows(EventExecutionException.class,
				() -> validationExecutionStorage.retrieveReport(uuid, ValidationExecutionStorage.ReportType.JSON));
		assertThat(exception.getErrorType()).isEqualTo(ErrorType.fromStatusCode(INTERNAL_ERROR.getStatusCode()));
		assertThat(exception.getMessage()).isEqualTo(errorMsg);
	}

}
