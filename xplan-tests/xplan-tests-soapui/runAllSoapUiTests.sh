#!/bin/bash
set -e

XPLAN_DIENSTE_BASE_URL=${XPLAN_DIENSTE_BASE_URL:-http://xplan-services}
XPLAN_MANAGER_API_BASE_URL=${XPLAN_MANAGER_API_BASE_URL:-http://xplan-manager-api}
XPLAN_MAPPROXY_BASE_URL=${XPLAN_MAPPROXY_BASE_URL:-http://xplan-mapproxy:8080}
XPLAN_MAPSERVER_BASE_URL=${XPLAN_MAPSERVER_BASE_URL:-http://xplan-mapserver:8080}
XPLAN_VALIDATOR_API_BASE_URL=${XPLAN_VALIDATOR_API_BASE_URL:-http://xplan-validator-api}
MSG_PREFIX=$([ -n "$POD_NAMESPACE" ] && echo "[$POD_NAMESPACE] " || echo "")

############################################
function sendSlackMessage() {
	local message=$MSG_PREFIX$1
	curl --show-error --silent -d "text=$message" -d "channel=$XPLAN_NOTIFY_SLACK_CHANNEL" -H "Authorization: Bearer $XPLAN_NOTIFY_SLACK_TOKEN" -X POST https://slack.com/api/chat.postMessage > /dev/null
}

function waitForRightVersion() {
	local url=$1
	local sleepDuration=10
	local nbRepeat=60
	echo "- checking $url (waiting up to $((nbRepeat * sleepDuration))s)"
	echo -n "   "

	n=1
	while [ $n -le $nbRepeat ]
	do
  		n=$(($n+1))
		local content=$(curl -s $url)
		if [[ "$content" == *"$GIT_REVISION"* ]]; then
  			echo "  [ok]"
  			return 0
  		else
  			echo -n "."
  			sleep $((sleepDuration))s
		fi
	done

	echo "  [failed]"
	return 2
}
############################################

if [ -n "$XPLAN_NOTIFY_SLACK_CHANNEL" ] &&  [ -n "$XPLAN_NOTIFY_SLACK_TOKEN" ]; then
	sendSlackMessage "Starting SoapUI tests (git revision: $GIT_REVISION)..."
fi

if [ -z ${XPLAN_DB_HOSTNAME+x} ]; then
  echo "XPLAN_DB_HOSTNAME is not set. jdbcUrl is not used in xplan-manager-api SoapUI Tests"
else
	echo "XPLAN_DB_HOSTNAME is set, jdbcUrl is created and used for xplan-manager-api SoapUI Tests"
	JDBC_URL="jdbc:postgresql://$XPLAN_DB_HOSTNAME:$XPLAN_DB_PORT/$XPLAN_DB_NAME?user=$XPLAN_DB_USER&password=$XPLAN_DB_PASSWORD"
fi

echo "Waiting for services with git revision $GIT_REVISION:"
urlsToCheck="$XPLAN_VALIDATOR_API_BASE_URL/xplan-validator-api/actuator/info \
$XPLAN_MANAGER_API_BASE_URL/xplan-manager-api/actuator/info \
$XPLAN_DIENSTE_BASE_URL/xplan-wfs/version.txt \
$XPLAN_DIENSTE_BASE_URL/xplan-wms/version.txt"

for url in $urlsToCheck; do
	waitForRightVersion $url
done;

echo "Executing tests..."
mvn test -Psystem-tests -DtestFileName=xplan-manager-api-soapui-project.xml \
    -DbaseUrlManagerApi=$XPLAN_MANAGER_API_BASE_URL \
    -Dusername=$XPLAN_MANAGER_API_USERNAME -Dpassword=$XPLAN_MANAGER_API_PASSWORD \
    -DjwtUrl=$XPLAN_JWT_URL -DjwtClientId=$XPLAN_JWT_CLIENTID \
    -DjdbcUrl=$JDBC_URL

mvn test -Psystem-tests -DtestFileName=xplan-validator-api-soapui-project.xml \
    -DbaseUrlValidatorApi=$XPLAN_VALIDATOR_API_BASE_URL \
    -Dusername=$XPLAN_VALIDATOR_API_USERNAME -Dpassword=$XPLAN_VALIDATOR_API_PASSWORD \
    -DjwtUrl=$XPLAN_JWT_URL -DjwtClientId=$XPLAN_JWT_CLIENTID

if [ -z ${XPLAN_DOKUMENTE_API_BASE_URL+x} ];
then
	echo "XPlanDokumenteAPI Tests are skipped!"
else
	waitForRightVersion $XPLAN_DOKUMENTE_API_BASE_URL/xplan-dokumente-api/actuator/info
	mvn test -Psystem-tests -DtestFileName=xplan-dokumente-api-soapui-project.xml \
	    -DbaseUrlManagerApi=$XPLAN_MANAGER_API_BASE_URL -DusernameManagerApi=$XPLAN_MANAGER_API_USERNAME -DpasswordManagerApi=$XPLAN_MANAGER_API_PASSWORD \
	    -DbaseUrlDokumenteApi=$XPLAN_DOKUMENTE_API_BASE_URL -DusernameDokumenteApi=$XPLAN_DOKUMENTE_API_USERNAME -DpasswordDokumenteApi=$XPLAN_DOKUMENTE_API_PASSWORD \
	    -DjwtUrl=$XPLAN_JWT_URL -DjwtClientId=$XPLAN_JWT_CLIENTID
fi

mvn test -Psystem-tests -DtestFileName=xplan-webservices-soapui-project.xml \
    -DbaseUrlServices=${XPLAN_DIENSTE_BASE_URL} -DusernameServices=$XPLAN_DIENSTE_USERNAME -DpasswordServices=$XPLAN_DIENSTE_PASSWORD \
    -DbaseUrlInspirePlu=${XPLAN_BASE_URL_INSPIRE_PLU} -DusernameInspirePlu=$XPLAN_INSPIRE_PLU_USERNAME -DpasswordInspirePlu=$XPLAN_INSPIRE_PLU_PASSWORD \
    -DbaseUrlManagerApi=${XPLAN_MANAGER_API_BASE_URL} -DusernameManagerApi=$XPLAN_MANAGER_API_USERNAME -DpasswordManagerApi=$XPLAN_MANAGER_API_PASSWORD \
    -DbaseUrlMapServer=${XPLAN_MAPSERVER_BASE_URL} -DusernameMapServer=$XPLAN_MAPSERVER_USERNAME -DpasswordMapServer=$XPLAN_MAPSERVER_PASSWORD \
    -DbaseUrlMapProxy=${XPLAN_MAPPROXY_BASE_URL} -DusernameMapProxy=$XPLAN_MAPPROXY_USERNAME -DpasswordMapProxy=$XPLAN_MAPPROXY_PASSWORD \
    -DapiKey=$XPLAN_SERVICES_API_KEY \
    -DjwtUrl=$XPLAN_JWT_URL -DjwtClientId=$XPLAN_JWT_CLIENTID

echo -e "\n"
echo "Results:"
results=`find target/soapui/ -name 'TEST-*.xml' | xargs cat | grep "<testsuite" | sed 's/>.*/>/' | sed -e 's/[">]//g' -e 's/.*name=//' | sort`
echo -e "$results"
echo -e "\n"


echo "Generating html report..."
mvn surefire-report:report-only -q

REPORT_PATH_PDF=target/test-report.pdf
echo "Transforming to PDF $REPORT_PATH_PDF..."
sed -i 's/display:none;//' target/reports/surefire.html
weasyprint file://$PWD/target/reports/surefire.html $REPORT_PATH_PDF
REPORT_PATH_TAR=target/test-report.tar.gz
tar cfz $REPORT_PATH_TAR -C target reports soapui

if [ "$XPLAN_UPLOAD_TEST_REPORT" = "true" ];
then
	export AWS_ACCESS_KEY_ID="$XPLAN_S3_ACCESS_KEY"
	export AWS_SECRET_ACCESS_KEY="$XPLAN_S3_SECRET_ACCESS_KEY"
	export AWS_DEFAULT_REGION="$XPLAN_S3_REGION"
	XPLAN_S3_REPORT_ID="${XPLAN_S3_REPORT_ID:-`date +%y-%m-%dT%H:%m:%S`}"
	XPLAN_S3_REPORT_PATH="${XPLAN_S3_REPORT_PATH:-test-reports}"

	S3_PATH_TAR="s3://$XPLAN_S3_BUCKET_ATTACHMENTS/$XPLAN_S3_REPORT_PATH/report-soapui-$XPLAN_S3_REPORT_ID.tar.gz"
	S3_PATH_PDF="s3://$XPLAN_S3_BUCKET_ATTACHMENTS/$XPLAN_S3_REPORT_PATH/report-soapui-$XPLAN_S3_REPORT_ID.pdf"
	echo "Uploading $REPORT_PATH_TAR to $S3_PATH_TAR..."
	aws --endpoint-url $XPLAN_S3_ENDPOINT_URL s3 cp $REPORT_PATH_TAR $S3_PATH_TAR
	echo "Uploading report to $XPLAN_S3_REPORT_PATH..."
	aws --endpoint-url $XPLAN_S3_ENDPOINT_URL s3 cp $REPORT_PATH_PDF $S3_PATH_PDF
else
	echo "No upload to S3 configured"
fi


if [ -n "$XPLAN_NOTIFY_SLACK_CHANNEL" ] &&  [ -n "$XPLAN_NOTIFY_SLACK_TOKEN" ]; then
	echo "Sending slack notification to $XPLAN_NOTIFY_SLACK_CHANNEL"
	formattedResults=`echo -e "$results" | sed 's/^/%0A* /'`
	message="Finished SoapUI tests.%0A\
$formattedResults"
	if [ -n "$S3_PATH_PDF" ]; then
		message="$message%0A%0A\
Get test reports from S3 bucket with:%0A\
\`\`\`aws --endpoint-url $XPLAN_S3_ENDPOINT_URL s3 cp $S3_PATH_PDF .%0A\
aws --endpoint-url $XPLAN_S3_ENDPOINT_URL s3 cp $S3_PATH_TAR .\`\`\`"
	fi
	sendSlackMessage "$message"
fi

echo "Ready"
