Hinweis:
========

Die Testdaten im Verzeichnis src/main/resources werden zu Testzwecken verwendet.
Zu diesen Dateien können keine genaueren Angaben zur Quelle und zu etwaigen Nutzungsrechten angegeben werden. Eine Weitergabe an
Dritte ausser zu Testzwecken der Anwendung xPlanBox ist nicht gestattet.

Inhalt des Verzeichnisses src/main/resources:
--------------------------
├── xplan-dokumente-api
│   └── plans
│       └── BPlan004_6-0.zip
├── xplan-loadtests
│   └── plans
│       └── BPlan004_6-0.zip
├── xplan-manager-api
│   ├── attachments
│   │   ├── invalidContentType.odt
│   │   ├── postDokInvalidContent.json
│   │   ├── postDok.json
│   │   ├── postDok.pdf
│   │   ├── postDokRasterInvalidContent.json
│   │   ├── postDokRaster.json
│   │   ├── postDokRefName.json
│   │   ├── postImg.pgw
│   │   ├── postImg.png
│   │   ├── postTextInvalidContent.json
│   │   ├── postText.json
│   │   ├── postText.pdf
│   │   ├── putDokInvalidContent.json
│   │   ├── putDok.json
│   │   ├── putDok.pdf
│   │   ├── putDokRasterInvalidContent.json
│   │   ├── putDokRaster.json
│   │   ├── putDokRasterMitBereich1.json
│   │   ├── putDokRasterNoBereich.json
│   │   ├── putImg.pgw
│   │   ├── putImg.png
│   │   ├── putTextInvalidContent.json
│   │   ├── putText.json
│   │   └── putText.txt
│   └── plans
│       ├── Blankenese29_Test_60_InvalidContent.zip
│       ├── BP_4.0_Laufrichtungsfehler.zip
│       ├── BP_4.0.zip
│       ├── BP_4.1_2.zip
│       ├── BP_4.1.zip
│       ├── BP_5.0_Entity.zip
│       ├── BP_5.0_WithAenderungAndDokument.zip
│       ├── BP_5.0.zip
│       ├── BP_5.1.gml
│       ├── BP_5.2_geometricErrorSelbstueberschneidung.gml
│       ├── BP_5.2_HttpReference.gml
│       ├── BP_5.2_HttpReference.zip
│       ├── BP_5.2_Laufrichtungsfehler.zip
│       ├── BP_5.2_MissingReference.gml
│       ├── BP_5.2_MissingReference.zip
│       ├── BP_5.2_MultipleBereiche.zip
│       ├── BP_5.2_MultiplePlaene.zip
│       ├── BP_5.2_WithoutBereich.zip
│       ├── BP_5.3_MultiplePlansWfsFcAdditionalObjects.gml
│       ├── BP_5.3_WfsFc.zip
│       ├── BP_5.3_WithText.zip
│       ├── BP_5.3.zip
│       ├── BP_5.4_MultiplePlaene_geometricError.gml
│       ├── BP_5.4_MultiplePlaene.gml
│       ├── BP_5.4_MultiplePlaene_semanticError.gml
│       ├── BP_5.4_MultiplePlaene_syntacticError.gml
│       ├── BP_5.4_MultiplePlaene.zip
│       ├── BP_6.0.2_NoBereichButReferenceExists.zip
│       ├── BP_6.0.2.zip
│       ├── BP_6.0_Dortmund.gml
│       ├── BP_6.0.gml
│       ├── BP_6.0_semanticErrorRule5_1_5.gml
│       ├── BP_6.0_syntacticErrors.gml
│       ├── BP_6.0.zip
│       ├── BPlan003_6-0.zip
│       ├── BPlan004_4-1.zip
│       ├── BPlan004_6-0_MissingPNG.zip
│       ├── BPlan004_6-0_NotSupportedFormatJPEG.zip
│       ├── BPlan004_6-0_ValidationError.zip
│       ├── BPlan004_6-0.zip
│       ├── BPlan006_6-0_WrongCRSInTIFF4326.zip
│       ├── BPlan006_6-0.zip
│       ├── BPlan007_6-0.zip
│       ├── FP_5.0.zip
│       ├── FP_6.0.2.zip
│       ├── invalidContentType.odt
│       ├── LP_6.0.zip
│       ├── RP_5.1.zip
│       └── SO_5.3.zip
├── xplan-validator-api
│   └── plans
│       ├── Blankenese29_Test_60_InvalidContent.zip
│       ├── BP_5.0_Entity.zip
│       ├── BP_5.1.gml
│       ├── BP_5.1.zip
│       ├── BP_5.2_geometricErrorSelbstueberschneidung.gml
│       ├── BP_5.2_MissingAndUncheckedReference.zip
│       ├── BP_5.3_MultiplePlansWfsFcAdditionalObjects.gml
│       ├── BP_5.3_MultiplePlansWfsFc.gml
│       ├── BP_5.3_MultiplePlansWfsFc.zip
│       ├── BP_5.3_WfsFc.gml
│       ├── BP_5.3_WfsFc.zip
│       ├── BP_5.3.zip
│       ├── BP_5.4_MultiplePlaene_geometricAndSemanticError.gml
│       ├── BP_5.4_MultiplePlaene_geometricError.gml
│       ├── BP_5.4_MultiplePlaene.gml
│       ├── BP_5.4_MultiplePlaene_semanticError.gml
│       ├── BP_5.4_MultiplePlaene_syntacticError.gml
│       ├── BP_6.0.2_invalide.zip
│       ├── BP_6.0.2_NoBereichButReferenceExists.zip
│       ├── BP_6.0.2_valide.zip
│       ├── BP_6.0.gml
│       ├── BP_6.0_semanticErrorRule5_1_5.gml
│       ├── BP_6.0_syntacticErrors.gml
│       ├── BP_6.0.zip
│       ├── BPlan004_6-0.zip
│       ├── FP_6.0.2_invalide.zip
│       ├── FP_6.0.2_valide.zip
│       └── invalidContentType.odt
├── xplan-webservices
│   └── plans
│       ├── BP_5.1_Textsortierung_1.zip
│       ├── BP_5.1_Textsortierung_3.zip
│       ├── BP_5.1_Textsortierung_4.zip
│       ├── BP_5.2_externalCodelist.zip
│       ├── BP_5.2_Textsortierung_2.zip
│       ├── BPlan002_5-3.zip
│       ├── BPlan004_6-0_CompositeCurve.gml
│       ├── BPlan004_6-0.zip
│       ├── BPlan41_HH_Bergedorf110_1Aend_soapui-webservices.zip
│       ├── Eidelstedt_4_V4-soapui-webservices.zip
│       ├── Eidelstedt4V4-with-nummer-soapui-webservices.zip
│       └── Osdorf48_Test_60.zip