# SoapUI TestSuites

Details zu den einzelnen SoapUI TestSuites sind in der [README](src/main/resources/README.md) zu finden.

## Aufruf der SoapUI Tests mit mvn

### xplan-validator-api-soapui-project

```
mvn clean test -Psystem-tests -DtestFileName=xplan-validator-api-soapui-project.xml \ 
    -DbaseUrlValidatorApi=https://xplanbox.lat-lon.de -Dusername=xplanbox -Dpassword='PWD'
```

Bei Nutzung der Absicherung über ein Bearer Token muss zusätzlich die URL von z.B. Keycloak angegeben werden, um das JSON Web Token anzufragen:
```
-DjwtUrl=https://xplanbox.lat-lon.de/keycloak/realms/xplanbox/protocol/openid-connect/token -DjwtClientId=xplanbox-api
```

### xplan-manager-api-soapui-project

```
mvn clean test -Psystem-tests -DtestFileName=xplan-manager-api-soapui-project.xml \ 
    -DbaseUrlManagerApi=https://xplanbox.lat-lon.de -Dusername=xplanbox -Dpassword='PWD' \
    -DjdbcUrl=jdbc:postgresql://localhost:5433/xplanbox?user=xplanbox&password=xplanbox
```

Bei Nutzung der Absicherung über ein Bearer Token muss zusätzlich die URL von z.B. Keycloak angegeben werden, um das JSON Web Token anzufragen, der mit `-Dusername` und `-Dpassword` (s. oben) angegebenen Nutzer, muss der Rolle XPLANBOX_ADMIN zugeordnet sein:
```
-DjwtUrl=https://xplanbox.lat-lon.de/keycloak/realms/xplanbox/protocol/openid-connect/token -DjwtClientId=xplanbox-api \
-DusernameDortmund=userdortmund -DpasswordDortmund='PWD' -DusernameNotDortmund=usergelsenkirchen -DpasswordNotDortmund='PWD'
```
Der Nutzer `-DusernameDortmund` muss einer Gruppe mit dem AGS 1731391860993 zugeordnet sein, der Nutzer `-DusernameNotDortmund` darf dagegen keiner Gruppe mit diesem AGS zugeordnet sein.

### xplan-dokumente-api-soapui-project

```
mvn clean test -Psystem-tests -DtestFileName=xplan-dokumente-api-soapui-project.xml \
    -DbaseUrlManagerApi=https://xplanbox.lat-lon.de -DbaseUrlDokumenteApi=https://xplanbox.lat-lon.de -Dusername=xplanbox -Dpassword='PWD'
```
Bei verschiedenen Zugangsdaten:
```
mvn clean test -Psystem-tests -DtestFileName=xplan-dokumente-api-soapui-project.xml \
    -DbaseUrlManagerApi=https://xplanbox.lat-lon.de  -DusernameManagerApi=xplanbox -DpasswordManagerApi='PWD' \ 
    -DbaseUrlDokumenteApi=https://xplanbox.lat-lon.de -DusernameDokumenteApi=xplanbox -DpasswordDokumenteApi='PWD'
```
Bei Nutzung der Absicherung über ein Bearer Token muss zusätzlich die URL von z.B. Keycloak angegeben werden, um das JSON Web Token anzufragen:
```
-DjwtUrl=https://xplanbox.lat-lon.de/keycloak/realms/xplanbox/protocol/openid-connect/token -DjwtClientId=xplanbox-api
```

### xplan-webservices-soapui-project

```
mvn clean test -Psystem-tests -DtestFileName=xplan-webservices-soapui-project.xml \
    -DbaseUrlServices=https://xplanbox.lat-lon.de \
    -DbaseUrlInspirePlu=https://xplanbox.lat-lon.de \
    -DbaseUrlManagerApi=https://xplanbox.lat-lon.de \
    -DbaseUrlMapServer=https://xplanbox.lat-lon.de \
    -DbaseUrlMapProxy=https://xplanbox.lat-lon.de \
    -Dusername=xplanbox -Dpassword='PWD' \
    -DapiKey=xplanbox
```
Bei verschiedenen Zugangsdaten:
```
mvn clean test -Psystem-tests -DtestFileName=xplan-webservices-soapui-project.xml \
    -DbaseUrlServices=https://xplanbox.lat-lon.de -DusernameServices=xplanbox -DpasswordServices='PWD' \
    -DbaseUrlInspirePlu=https://xplanbox.lat-lon.de -DusernameInspirePlu=xplanbox -DpasswordInspirePlu='PWD' \
    -DbaseUrlManagerApi=https://xplanbox.lat-lon.de -DusernameManagerApi=xplanbox -DpasswordManagerApi='PWD'\
    -DbaseUrlMapServer=https://xplanbox.lat-lon.de -DusernameMapServer=xplanbox -DpasswordMapServer='PWD'\
    -DbaseUrlMapProxy=https://xplanbox.lat-lon.de -DusernameMapProxy=xplanbox -DpasswordMapProxy='PWD' \
    -DapiKey=xplanbox
```
Bei Nutzung der Absicherung über ein Bearer Token muss zusätzlich die URL von z.B. Keycloak angegeben werden, um das JSON Web Token anzufragen:
```
-DjwtUrl=https://xplanbox.lat-lon.de/keycloak/realms/xplanbox/protocol/openid-connect/token -DjwtClientId=xplanbox-api
```

## Ausführung im Docker container

Die SoapUI Tests können in einem Docker Container ausgeführt werden:

```
docker run --env ... xplanbox/xplan-tests-soapui
```

### Umgebungsvariablen

- `XPLAN_DIENSTE_BASE_URL`
- `XPLAN_DIENSTE_USERNAME`
- `XPLAN_DIENSTE_PASSWORD`
- `XPLAN_DOKUMENTE_API_BASE_URL` - optional: XPlanDokumenteAPI Tests werden übersprungen, wenn die Umgebungsvariable nicht gesetzt ist.
- `XPLAN_DOKUMENTE_API_USERNAME`
- `XPLAN_DOKUMENTE_API_PASSWORD`
- `XPLAN_BASE_URL_INSPIRE_PLU`
- `XPLAN_INSPIRE_PLU_USERNAME`
- `XPLAN_INSPIRE_PLU_PASSWORD`
- `XPLAN_MANAGER_API_BASE_URL`
- `XPLAN_MANAGER_API_USERNAME`
- `XPLAN_MANAGER_API_PASSWORD`
- `XPLAN_MAPPROXY_BASE_URL`
- `XPLAN_MAPPROXY_USERNAME`
- `XPLAN_MAPPROXY_PASSWORD`
- `XPLAN_MAPSERVER_BASE_URL`
- `XPLAN_MAPSERVER_USERNAME`
- `XPLAN_MAPSERVER_PASSWORD`
- `XPLAN_SERVICES_API_KEY`
- `XPLAN_VALIDATOR_API_BASE_URL`
- `XPLAN_JWT_URL`
- `XPLAN_JWT_CLIENTID`

Optional, wenn die Tests der XPlanDB bei Ausführung der XPlanManagerAPI-SopaUI-Tests ausgeführt werden sollen:

- `XPLAN_DB_HOSTNAME`
- `XPLAN_DB_PORT`
- `XPLAN_DB_NAME`
- `XPLAN_DB_USER`
- `XPLAN_DB_PASSWORD`


Der Report im PDF Format kann zu einem S3-Bucket hochgeladen werden, dafür müssen folgende Umgebungsvariablen gesetzt werden:

- `XPLAN_UPLOAD_TEST_REPORT`: muss auf `true` gesetzt werden
- `XPLAN_S3_ENDPOINT_URL`: die S3 Url, z.B. https://the.s3.url
- `XPLAN_S3_REPORT_ID` (optional): ein Id, dass im S3-Objektname verwendet werden soll
- `XPLAN_S3_REPORT_PATH`(optional): der Pfad im S3 Bucket (default: `test-reports`)
- `XPLAN_S3_ACCESS_KEY`: der S3-Zugangschlüssel
- `XPLAN_S3_SECRET_ACCESS_KEY`: der S3-Geheimzugangschlüssel
- `XPLAN_S3_REGION`: die S3 Region
- `XPLAN_S3_BUCKET_ATTACHMENTS`: der Name des Buckets

Der Report kann aus S3 lokal kopiert werden, z.B. mit:

	aws s3 cp s3://my-bucket/test-reports/report-2023-05-26T08:57:15.pdf report.pdf --endpoint-url https://the.s3.url

Eine Notification kann nach der Ausführung der Tests zu einem Slack Chanel geschickt werden. Dafür müssen folgende Umgebungsvariablen gesetzt werden:

- `XPLAN_NOTIFY_SLACK_CHANNEL`: der Slack Kanal
- `XPLAN_NOTIFY_SLACK_TOKEN`: das Slack Authorisierungstoken

## Konventionen für Entwickler

 * Um die von den SoapUI-Tests verwendeten Plänen identifizieren zu können, werden alle Pläne, die importiert werden nach folgendem Schema umbenannt: _\<NAME>\_SoapUI-\<KOMPONENTE>_. Mit folgenden Platzhaltern:
   * _\<NAME>_: Name des Plans
   * _\<KOMPONENTE>_: Name der zu testenden Komponente, z.B.: XPlanManagerAPI, XPlanValidatorAPI, XPlanDokumenteAPI