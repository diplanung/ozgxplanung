<?xml version='1.0' encoding='UTF-8'?>
<XPlanAuszug xmlns="http://www.xplanung.de/xplangml/6/0" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.xplanung.de/xplangml/6/0 https://repository.gdi-de.org/schemas/de.xleitstelle.xplanung/6.0/XPlanung-Operationen.xsd" gml:id="GML_975d212d-1a41-4611-945d-bc625350833b">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
      <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <BP_Plan gml:id="GML_88bfe952-199f-4bba-bea2-c2b441737144">
      <name>BPlan004_6-0_SoapUI-XPlanDienste</name>
      <beschreibung>Der vorhabenbezogene Bebauungsplan BPlan004_6-0
für den Geltungsbereich Knickweg – Barmbeker Straße –
Gertigstraße
(Bezirk Hamburg-Nord, Ortsteil 412) wird festgestellt.</beschreibung>
      <technHerstellDatum>2017-03-20</technHerstellDatum>
      <erstellungsMassstab>1000</erstellungsMassstab>
      <raeumlicherGeltungsbereich>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_94e334d8-1668-4f25-87e4-9829392f9353">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 567446.882 5937692.583 567390.995 5937669.124 567386.293 5937667.145 567389.479 5937654.684 567404.151 5937595.479 567474.996 5937628.152</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </raeumlicherGeltungsbereich>
      <texte xlink:href="#GML_ad1df68a-7a33-4c35-83dd-db236a88f8aa"/>
      <texte xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf"/>
      <texte xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc"/>
      <texte xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628"/>
      <texte xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd"/>
      <texte xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1"/>
      <texte xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603"/>
      <texte xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90"/>
      <texte xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d"/>
      <gemeinde>
        <XP_Gemeinde>
          <ags>02000000</ags>
          <gemeindeName>Freie und Hansestadt Hamburg</gemeindeName>
          <ortsteilName>412</ortsteilName>
        </XP_Gemeinde>
      </gemeinde>
      <planArt>3000</planArt>
      <rechtsstand>3000</rechtsstand>
      <rechtsverordnungsDatum>2016-10-25</rechtsverordnungsDatum>
      <staedtebaulicherVertrag>false</staedtebaulicherVertrag>
      <erschliessungsVertrag>false</erschliessungsVertrag>
      <durchfuehrungsVertrag>true</durchfuehrungsVertrag>
      <gruenordnungsplan>false</gruenordnungsplan>
      <bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
    </BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_ad1df68a-7a33-4c35-83dd-db236a88f8aa">
      <schluessel>§2 Nr.6</schluessel>
      <text>Im Plangebiet ist in den Schlafräumen durch geeignete bauliche
Schallschutzmaßnahmen wie zum Beispiel Doppelfassaden,
verglaste Loggien, Wintergärten, besondere Fensterkonstruktionen
oder in ihrer Wirkung vergleichbare Maßnahmen
sicherzustellen, dass ein Innenraumpegel bei
gekipptem Fenster von 30 dB(A) während der Nachtzeit
nicht überschritten wird. Erfolgt die bauliche Schallschutzmaßnahme
in Form von verglasten Loggien oder Wintergärten
muss dieser Innenraumpegel bei gekippten/teilgeöffneten
Bauteilen erreicht werden. Wohn-/Schlafräume in
Einzimmerwohnungen und Kinderzimmer sind wie Schlafräume
zu beurteilen.</text>
      <rechtscharakter>1000</rechtscharakter>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_0d37aade-f41e-4ec9-b026-79d4709945bf">
      <schluessel>§2 Nr.8</schluessel>
      <text>Im allgemeinen Wohngebiet sind die nicht überbauten Flächen
von Tiefgaragen mit einem mindestens 50 cm starken
durchwurzelbaren Substrataufbau zu versehen und gärtnerisch
anzulegen. Hiervon ausgenommen sind die erforderlichen
Flächen für Terrassen, Wege und Freitreppen, Kinderspielflächen
sowie Bereiche, die der Belichtung, Be- und
Entlüftung oder der Aufnahme von technischen Anlagen
dienen.</text>
      <rechtscharakter>1000</rechtscharakter>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc">
      <schluessel>§2 Nr.3</schluessel>
      <text>Im allgemeinen Wohngebiet sind Stellplätze nur in Tiefgaragen
zulässig. Tiefgaragen sind auch außerhalb der überbaubaren
Grundstücksflächen zulässig. Tiefgaragen dürfen
einschließlich ihrer Überdeckung nicht über die natürliche
Geländeoberfläche herausragen.</text>
      <rechtscharakter>1000</rechtscharakter>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628">
      <schluessel>§2 Nr.9</schluessel>
      <text>Im allgemeinen Wohngebiet sind die Freiflächen zur öffentlichen
Grünanlage mit einer Hecke abzupflanzen. Für die
Hecke sind standortgerechte einheimische Gehölze zu verwenden.</text>
      <rechtscharakter>1000</rechtscharakter>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_2d0243f5-23e4-4f40-8538-d833e9c168dd">
      <schluessel>§2 Nr.7</schluessel>
      <text>Im allgemeinen Wohngebiet sind mindestens 55 vom Hundert
(v. H.) der Dachflächen und auf der Fläche für den
besonderen Nutzungszweck – Kiosk – mindestens 80 v. H.
der Dachflächen mit einem mindestens 10 cm starken
durchwurzelbaren Substrataufbau zu versehen und extensiv
zu begrünen.</text>
      <rechtscharakter>1000</rechtscharakter>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_6bac8f46-a325-4392-b115-07bb0e41e8c1">
      <schluessel>§2 Nr.2</schluessel>
      <text>Im allgemeinen Wohngebiet sind Terrassen im Anschluss
an die Hauptnutzung bis zu einer Tiefe von 4 m auch außerhalb
der überbaubaren Grundstücksfläche zulässig. Untergeordnete
Bauteile wie Vordächer, Balkone und Erker im
Bereich von öffentlichen Grünflächen sind unzulässig.</text>
      <rechtscharakter>1000</rechtscharakter>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603">
      <schluessel>§2 Nr.1</schluessel>
      <text>Im allgemeinen Wohngebiet sind innerhalb der mit „(A)“
bezeichneten Fläche (Vorhabengebiet) im Rahmen der festgesetzten
Nutzungen nur solche Vorhaben zulässig, zu
deren Durchführung sich der Vorhabenträger im Durchführungsvertrag
verpflichtet.</text>
      <rechtscharakter>1000</rechtscharakter>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90">
      <schluessel>§2 Nr.4</schluessel>
      <text>Im allgemeinen Wohngebiet darf die festgesetzte Grundflächenzahl
für Tiefgaragen bis zu einer Grundflächenzahl
von 1,0 überschritten werden.</text>
      <rechtscharakter>1000</rechtscharakter>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_114d1690-3976-42e3-87be-ec97b7ad773d">
      <schluessel>§2 Nr.5</schluessel>
      <text>Im allgemeinen Wohngebiet sind auf den Flurstücken 1477,
1478 und 1480 der Gemarkung Winterhude vor den straßenzugewandten
Fenstern der Wohn- und Schlafräume
lärmgeschützte Außenbereiche durch bauliche Schallschutzmaßnahmen,
wie etwa verglaste Loggien, Wintergärten
oder in ihrer Wirkung vergleichbare Maßnahmen zwingend
vorzusehen. In den lärmgeschützten Außenbereichen
ist bei geöffneten Fenstern/Bauteilen sicherzustellen, dass
ein Tagpegel von weniger als 65 dB(A) erreicht wird.</text>
      <rechtscharakter>1000</rechtscharakter>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <BP_Bereich gml:id="GML_6e871e4c-58e1-4b97-bf2a-a696816b2458">
      <nummer>0</nummer>
      <geltungsbereich>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b35ad3c8-f059-4f9c-af9f-4205794b8f36">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 567446.882 5937692.583 567390.995 5937669.124 567386.293 5937667.145 567389.479 5937654.684 567404.151 5937595.479 567474.996 5937628.152</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </geltungsbereich>
      <planinhalt xlink:href="#GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665"/>
      <planinhalt xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f"/>
      <planinhalt xlink:href="#GML_5276e223-57f7-4596-9c9c-8fb909e11db2"/>
      <planinhalt xlink:href="#GML_d48b9ec4-07b4-4ad8-9664-407ae6cf9c4e"/>
      <planinhalt xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af"/>
      <planinhalt xlink:href="#GML_354dc6b6-d910-4f29-8aeb-66d238013da6"/>
      <planinhalt xlink:href="#GML_b607f50f-8221-4811-81f0-7f6efef25db3"/>
      <planinhalt xlink:href="#GML_20f74701-9a0b-4c38-89b8-ae81db151e28"/>
      <planinhalt xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab"/>
      <planinhalt xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136"/>
      <planinhalt xlink:href="#GML_ea917a61-3dcc-4889-9be0-0d56cbaad94f"/>
      <planinhalt xlink:href="#GML_9875e68a-2b21-40e4-8f66-ecf5c2290936"/>
      <planinhalt xlink:href="#GML_7b11be88-979b-483f-8042-82c51eff70af"/>
      <planinhalt xlink:href="#GML_be2c01dd-4d15-4a6a-85c0-dc9565324486"/>
      <planinhalt xlink:href="#GML_21c90a12-5886-40dd-8a86-463267b6e201"/>
      <planinhalt xlink:href="#GML_cf3318a5-fc25-4f6b-a594-893928f6a3de"/>
      <planinhalt xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872"/>
      <planinhalt xlink:href="#GML_56980ee8-0e1a-42e0-93f5-bd9c5247a299"/>
      <planinhalt xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e"/>
      <planinhalt xlink:href="#GML_8376d624-8ccb-473d-81df-fc95b1ef28b3"/>
      <planinhalt xlink:href="#GML_91e2ff9f-d5ff-4c87-84ae-9a6a02ddeed3"/>
      <planinhalt xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e"/>
      <planinhalt xlink:href="#GML_55177fec-7599-4785-a8e2-806742a9b2d9"/>
      <planinhalt xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292"/>
      <planinhalt xlink:href="#GML_5355860b-25fa-401c-a2ea-bb70654d17ab"/>
      <planinhalt xlink:href="#GML_e1568a2b-1768-4889-aedb-95449c029c25"/>
      <praesentationsobjekt xlink:href="#GML_de628759-edb3-4f31-9d43-7d4f816bfb0c"/>
      <praesentationsobjekt xlink:href="#GML_82075a17-ae02-4228-a4ac-debedfd66c1e"/>
      <praesentationsobjekt xlink:href="#GML_a435c756-f459-41ce-b27c-d5992aa8f87e"/>
      <praesentationsobjekt xlink:href="#GML_7d7588c9-1211-471e-ad15-8dd9f6605701"/>
      <praesentationsobjekt xlink:href="#GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede"/>
      <praesentationsobjekt xlink:href="#GML_190f5132-7a3c-4e3f-900e-0705faba810a"/>
      <praesentationsobjekt xlink:href="#GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a"/>
      <praesentationsobjekt xlink:href="#GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8"/>
      <praesentationsobjekt xlink:href="#GML_af37976e-6da1-4e93-a376-ad285c900b4c"/>
      <praesentationsobjekt xlink:href="#GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1"/>
      <praesentationsobjekt xlink:href="#GML_10c25c52-be4f-422d-8171-469001af2bf4"/>
      <praesentationsobjekt xlink:href="#GML_045ae35f-52f5-48d9-b5a6-3d5f339418be"/>
      <praesentationsobjekt xlink:href="#GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad"/>
      <praesentationsobjekt xlink:href="#GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e"/>
      <praesentationsobjekt xlink:href="#GML_9036c79b-5e09-4030-8c15-e0d9877d084c"/>
      <praesentationsobjekt xlink:href="#GML_fba093ee-e774-4846-b286-e46649619852"/>
      <praesentationsobjekt xlink:href="#GML_c34619af-a786-41f0-a899-8a67df7b6750"/>
      <praesentationsobjekt xlink:href="#GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6"/>
      <praesentationsobjekt xlink:href="#GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d"/>
      <praesentationsobjekt xlink:href="#GML_f44449a4-6628-4c84-9dfe-d82b4dafca06"/>
      <praesentationsobjekt xlink:href="#GML_796aa811-807f-4550-abab-24b9cc8aa359"/>
      <praesentationsobjekt xlink:href="#GML_5890c370-a3bc-412c-b650-13894b87a426"/>
      <praesentationsobjekt xlink:href="#GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28"/>
      <praesentationsobjekt xlink:href="#GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91"/>
      <praesentationsobjekt xlink:href="#GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12"/>
      <praesentationsobjekt xlink:href="#GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea"/>
      <verfahren>1000</verfahren>
      <gehoertZuPlan xlink:href="#GML_88bfe952-199f-4bba-bea2-c2b441737144"/>
    </BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <BP_TextAbschnittFlaeche gml:id="GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665">
      <gliederung2>(A)</gliederung2>
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <wirdDargestelltDurch xlink:href="#GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1"/>
      <rechtscharakter>1000</rechtscharakter>
      <refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603"/>
      <position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_91b0f658-5334-442d-8738-870bb666e953">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567433.287 5937679.305 567416.015 5937672.057 567419.988 5937662.431 567432.605 5937667.621 567439.105 5937652.001 567428.275 5937631.879 567422.612 5937629.344 567428.448 5937616.596 567434.138 5937619.1 567436.484 5937623.475 567442.246 5937634.25 567452.958 5937654.188 567450.566 5937668.248 567438.775 5937666.233 567433.287 5937679.305</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
    </BP_TextAbschnittFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1">
      <art>xplan:gliederung2</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_5ade4326-1120-44fa-9a0a-4f5fd2b568b4">
          <gml:pos>567437.222 5937662.769</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f">
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>6000</bezugspunkt>
          <h uom="m">30.0</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <wirdDargestelltDurch xlink:href="#GML_045ae35f-52f5-48d9-b5a6-3d5f339418be"/>
      <wirdDargestelltDurch xlink:href="#GML_5890c370-a3bc-412c-b650-13894b87a426"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b5dc437e-5dd1-46c5-97a1-8eab330f4829">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567453.455 5937654.108 567452.175 5937661.631 567442.18 5937659.754 567439.277 5937659.277 567439.035 5937655.06 567438.79 5937652.388 567432.026 5937639.916 567442.758 5937634.17 567453.455 5937654.108</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Z>7</Z>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_045ae35f-52f5-48d9-b5a6-3d5f339418be">
      <art>xplan:Z</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_23d6f880-3ea8-4a95-a83d-80c5d2bd4ece">
          <gml:pos>567444.114 5937651.76</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_5890c370-a3bc-412c-b650-13894b87a426">
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</art>
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</art>
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_b2957ec3-b4c3-460b-bae2-e9e22382482d">
          <gml:pos>567439.328 5937644.053</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_NutzungsartenGrenze gml:id="GML_5276e223-57f7-4596-9c9c-8fb909e11db2">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:LineString srsName="EPSG:25832" gml:id="GML_b6d45f53-0d1e-4294-843b-6feaa7b1b347">
          <gml:posList>567416.015 5937672.057 567419.988 5937662.431 567432.605 5937667.621 567439.105 5937652.001 567428.275 5937631.879 567422.612 5937629.344 567428.448 5937616.596 567434.138 5937619.1 567436.484 5937623.475 567442.246 5937634.25 567452.958 5937654.188 567450.566 5937668.248 567438.775 5937666.233 567433.287 5937679.305 567416.015 5937672.057</gml:posList>
        </gml:LineString>
      </position>
      <typ>9999</typ>
    </BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_HoehenMass gml:id="GML_d48b9ec4-07b4-4ad8-9664-407ae6cf9c4e">
      <text>Straßenhöhe bezogen auf NHN</text>
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <h uom="m">7.0</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_3f4dba74-a942-4f8b-a950-7329c7ed451e">
          <gml:pos>567428.23 5937680.271</gml:pos>
        </gml:Point>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <nordwinkel uom="grad">0.0</nordwinkel>
    </BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e21981e2-40a7-4dac-84b0-c88d371030af">
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>6000</bezugspunkt>
          <h uom="m">23.5</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <wirdDargestelltDurch xlink:href="#GML_10c25c52-be4f-422d-8171-469001af2bf4"/>
      <wirdDargestelltDurch xlink:href="#GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e131fa1a-cf04-424c-adca-6787fb097bc6">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567434.477 5937618.73 567442.758 5937634.17 567432.026 5937639.916 567427.969 5937632.352 567421.935 5937629.605 567428.223 5937615.867 567434.477 5937618.73</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Z>5</Z>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_10c25c52-be4f-422d-8171-469001af2bf4">
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</art>
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</art>
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_0ac241c5-ff25-4cd5-85f8-ef2f03291d0c">
          <gml:pos>567426.835 5937628.058</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d">
      <art>xplan:Z</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_0f1038a5-456e-44e4-ac24-b883181204d6">
          <gml:pos>567433.633 5937633.755</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="GML_354dc6b6-d910-4f29-8aeb-66d238013da6">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:LineString srsName="EPSG:25832" gml:id="GML_7e2f420b-f4b1-4dea-9ce0-4f4d4f09bdd8">
          <gml:posList>567415.353 5937672.325 567419.705 5937661.78 567437.993 5937669.342 567437.269 5937671.067 567433.538 5937679.955 567415.353 5937672.325</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="GML_b607f50f-8221-4811-81f0-7f6efef25db3">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:LineString srsName="EPSG:25832" gml:id="GML_89bee8ae-25e7-41f4-91db-91cfeb8f7e62">
          <gml:posList>567428.223 5937615.867 567434.477 5937618.73 567442.758 5937634.17 567432.026 5937639.916 567427.969 5937632.352 567421.935 5937629.605 567428.223 5937615.867</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="GML_20f74701-9a0b-4c38-89b8-ae81db151e28">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:LineString srsName="EPSG:25832" gml:id="GML_8450740b-081b-4dc9-8622-4a67df561b3b">
          <gml:posList>567450.952 5937668.824 567440.924 5937667.111 567442.18 5937659.754 567452.173 5937661.63 567450.952 5937668.824</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BaugebietsTeilFlaeche gml:id="GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <wirdDargestelltDurch xlink:href="#GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8"/>
      <wirdDargestelltDurch xlink:href="#GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e"/>
      <wirdDargestelltDurch xlink:href="#GML_c34619af-a786-41f0-a899-8a67df7b6750"/>
      <rechtscharakter>1000</rechtscharakter>
      <refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603"/>
      <refTextInhalt xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1"/>
      <refTextInhalt xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc"/>
      <refTextInhalt xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90"/>
      <refTextInhalt xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d"/>
      <refTextInhalt xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd"/>
      <refTextInhalt xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf"/>
      <refTextInhalt xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628"/>
      <position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_70be827f-29e1-4948-87d5-656b398943b7">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567450.952 5937668.824 567451.021 5937668.836 567448.434 5937683.72 567445.762 5937685.099 567443.507 5937686.264 567432.779 5937681.762 567433.538 5937679.955 567415.353 5937672.325 567418.938 5937663.64 567419.705 5937661.78 567432.297 5937667.063 567438.475 5937652.392 567438.319 5937651.648 567428.755 5937633.817 567427.969 5937632.352 567424.532 5937630.787 567421.935 5937629.605 567428.223 5937615.867 567434.477 5937618.73 567442.758 5937634.17 567453.455 5937654.108 567452.176 5937661.63 567450.952 5937668.824</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <GRZ>0.9</GRZ>
      <besondereArtDerBaulNutzung>1200</besondereArtDerBaulNutzung>
      <bauweise>2000</bauweise>
    </BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8">
      <art>xplan:GRZ</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_64501099-cc1e-4189-ba7e-cc9eb7fc1341">
          <gml:pos>567425.133 5937653.994</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e">
      <art>xplan:besondereArtDerBaulNutzung</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_77e36710-9e23-4ca3-a466-65c2d8ca92ae">
          <gml:pos>567428.301 5937671.953</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_c34619af-a786-41f0-a899-8a67df7b6750">
      <art>xplan:bauweise</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_24687849-77d7-43a0-b5dd-5fc7fcf0a1f6">
          <gml:pos>567434.689 5937670.707</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136">
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>6000</bezugspunkt>
          <h uom="m">26.5</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <wirdDargestelltDurch xlink:href="#GML_9036c79b-5e09-4030-8c15-e0d9877d084c"/>
      <wirdDargestelltDurch xlink:href="#GML_fba093ee-e774-4846-b286-e46649619852"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_5c9caab4-0bf1-462d-9051-d3ecb4ec1a35">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567452.173 5937661.63 567450.952 5937668.824 567440.924 5937667.111 567442.18 5937659.754 567452.173 5937661.63</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Z>6</Z>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_9036c79b-5e09-4030-8c15-e0d9877d084c">
      <art>xplan:Z</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_dd27df90-6716-4f6e-9366-656f9811404f">
          <gml:pos>567445.543 5937665.568</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_fba093ee-e774-4846-b286-e46649619852">
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</art>
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</art>
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d1a69f6f-e93d-45ad-b204-2bcd8d232d64">
          <gml:pos>567443.466 5937662.039</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ea917a61-3dcc-4889-9be0-0d56cbaad94f">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_647b0391-4eb8-4aa7-9838-73d07dbba7f4">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567416.408 5937613.826 567412.057 5937623.382 567405.121 5937620.224 567409.472 5937610.668 567416.408 5937613.826</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Z>1</Z>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="GML_9875e68a-2b21-40e4-8f66-ecf5c2290936">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:LineString srsName="EPSG:25832" gml:id="GML_19ea0a78-62e1-44d5-82a7-d7d2156e666b">
          <gml:posList>567439.035 5937655.06 567438.79 5937652.388 567432.026 5937639.916 567442.758 5937634.17 567453.455 5937654.108 567452.175 5937661.631 567442.18 5937659.754 567439.277 5937659.277 567439.035 5937655.06</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="GML_7b11be88-979b-483f-8042-82c51eff70af">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:LineString srsName="EPSG:25832" gml:id="GML_b16fc20a-28ac-49c8-9709-3b206faa84b9">
          <gml:posList>567432.779 5937681.762 567433.538 5937679.955 567437.269 5937671.067 567437.993 5937669.342 567439.063 5937666.793 567450.952 5937668.824 567448.434 5937683.72 567443.507 5937686.264 567432.779 5937681.762</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_VerEntsorgung gml:id="GML_be2c01dd-4d15-4a6a-85c0-dc9565324486">
      <rechtsstand>1000</rechtsstand>
      <ebene>-1</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <rechtscharakter>7000</rechtscharakter>
      <position>
        <gml:LineString srsName="EPSG:25832" gml:id="GML_25e11113-d7f2-4cd2-8bad-38199e8e0b3a">
          <gml:posList>567432.929 5937675.181 567431.741 5937679.201</gml:posList>
        </gml:LineString>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <zweckbestimmung>
        <BP_KomplexeZweckbestVerEntsorgung>
          <allgemein>1000</allgemein>
        </BP_KomplexeZweckbestVerEntsorgung>
      </zweckbestimmung>
    </BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="GML_21c90a12-5886-40dd-8a86-463267b6e201">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:LineString srsName="EPSG:25832" gml:id="GML_43308c21-78e2-43bb-aca8-37286c3650f1">
          <gml:posList>567407.329 5937656.657 567419.705 5937661.78 567415.353 5937672.325 567403.0 5937667.139 567407.329 5937656.657</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UnverbindlicheVormerkung gml:id="GML_cf3318a5-fc25-4f6b-a594-893928f6a3de">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <wirdDargestelltDurch xlink:href="#GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede"/>
      <rechtscharakter>7000</rechtscharakter>
      <position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_9fcea416-837e-47e1-877e-ee6ad21163f0">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567468.867 5937663.607 567465.27 5937684.42 567462.07 5937698.959 567458.005 5937697.252 567460.825 5937687.181 567456.625 5937685.781 567452.825 5937636.781 567458.625 5937635.581 567459.825 5937647.581 567467.425 5937647.381 567468.867 5937663.607</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <vormerkung>vorgesehene Bahnanlage</vormerkung>
    </BP_UnverbindlicheVormerkung>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PTO gml:id="GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede">
      <art>xplan:vormerkung</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_cf3318a5-fc25-4f6b-a594-893928f6a3de"/>
      <schriftinhalt>vorgesehene Bahnanlage</schriftinhalt>
      <fontSperrung>0.0</fontSperrung>
      <skalierung>1.0</skalierung>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_a9673c68-633d-44ef-a79d-70497f769258">
          <gml:pos>567457.938 5937683.621</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">282.22</drehwinkel>
    </XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_GruenFlaeche gml:id="GML_9a4506b9-78b2-4dc4-bb55-088a4c627872">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <wirdDargestelltDurch xlink:href="#GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6"/>
      <wirdDargestelltDurch xlink:href="#GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_545074d0-e941-4a34-bc05-c16726a92a4f">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567432.297 5937667.063 567419.705 5937661.78 567405.227 5937655.787 567401.59 5937657.649 567394.606 5937655.401 567403.316 5937621.126 567421.935 5937629.605 567424.532 5937630.787 567427.969 5937632.352 567428.755 5937633.817 567438.319 5937651.648 567438.475 5937652.392 567432.297 5937667.063</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <zweckbestimmung>
        <BP_KomplexeZweckbestGruen>
          <allgemein>1600</allgemein>
        </BP_KomplexeZweckbestGruen>
      </zweckbestimmung>
      <nutzungsform>2000</nutzungsform>
      <zugunstenVon>(FHH)</zugunstenVon>
    </BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6">
      <art>xplan:zugunstenVon</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_b1c121e1-df40-4e35-87bb-61077f64900f">
          <gml:pos>567409.089 5937633.593</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PTO gml:id="GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28">
      <art>xplan:zweckbestimmung[1]/xplan:BP_KomplexeZweckbestGruen/xplan:allgemein</art>
      <art>xplan:nutzungsform</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872"/>
      <schriftinhalt>Spielplatz</schriftinhalt>
      <fontSperrung>0.0</fontSperrung>
      <skalierung>1.0</skalierung>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_5d6e5bc9-a8e9-4d5e-b6e9-bd925ae35166">
          <gml:pos>567406.396 5937643.214</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
    </XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="GML_56980ee8-0e1a-42e0-93f5-bd9c5247a299">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:LineString srsName="EPSG:25832" gml:id="GML_345b4b32-0dcc-4652-bed6-3549d0c1174a">
          <gml:posList>567409.472 5937610.668 567416.408 5937613.826 567412.057 5937623.382 567405.121 5937620.224 567409.472 5937610.668</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BaugebietsTeilFlaeche gml:id="GML_321bdb15-2400-4c44-a553-35f402f8743e">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <wirdDargestelltDurch xlink:href="#GML_af37976e-6da1-4e93-a376-ad285c900b4c"/>
      <wirdDargestelltDurch xlink:href="#GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad"/>
      <wirdDargestelltDurch xlink:href="#GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12"/>
      <rechtscharakter>1000</rechtscharakter>
      <refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603"/>
      <refTextInhalt xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1"/>
      <refTextInhalt xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc"/>
      <refTextInhalt xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90"/>
      <refTextInhalt xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d"/>
      <refTextInhalt xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd"/>
      <refTextInhalt xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf"/>
      <refTextInhalt xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628"/>
      <position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_fadac7dc-1508-4b98-a22f-edd251a50ee8">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567419.705 5937661.78 567418.938 5937663.64 567415.353 5937672.325 567403.0 5937667.139 567395.687 5937664.07 567393.37 5937660.265 567394.606 5937655.401 567401.59 5937657.649 567405.227 5937655.787 567419.705 5937661.78</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <GRZ>0.6</GRZ>
      <besondereArtDerBaulNutzung>1200</besondereArtDerBaulNutzung>
      <bauweise>2000</bauweise>
    </BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_af37976e-6da1-4e93-a376-ad285c900b4c">
      <art>xplan:GRZ</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_776e0c5b-79ea-4242-a0cf-92cfb5f5cfbf">
          <gml:pos>567399.836 5937653.516</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad">
      <art>xplan:bauweise</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_07961eb1-116b-472a-bee1-df1fa0430afd">
          <gml:pos>567416.427 5937662.618</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12">
      <art>xplan:besondereArtDerBaulNutzung</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_2f4f40fe-6a84-46d1-8945-8f2c565cf6b3">
          <gml:pos>567410.188 5937663.997</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <SO_Strassenverkehr gml:id="GML_8376d624-8ccb-473d-81df-fc95b1ef28b3">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567462.07 5937698.959 567446.882 5937692.583 567390.995 5937669.124 567386.293 5937667.145 567389.479 5937654.684 567404.151 5937595.479 567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.0">
                  <gml:segments>
                    <gml:ArcString>
                      <gml:posList>567410.377 5937607.7 567407.834857796 5937607.72206805 567406.223 5937609.688</gml:posList>
                    </gml:ArcString>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.1">
                  <gml:posList>567406.223 5937609.688 567403.316 5937621.126</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.2">
                  <gml:posList>567403.316 5937621.126 567394.606 5937655.401</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.3">
                  <gml:posList>567394.606 5937655.401 567393.37 5937660.265</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.4">
                  <gml:posList>567393.37 5937660.265 567395.687 5937664.07</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.5">
                  <gml:posList>567395.687 5937664.07 567403.0 5937667.139</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.6">
                  <gml:posList>567403.0 5937667.139 567415.353 5937672.325</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.7">
                  <gml:posList>567415.353 5937672.325 567433.538 5937679.955</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.8">
                  <gml:posList>567433.538 5937679.955 567432.779 5937681.762</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.9">
                  <gml:posList>567432.779 5937681.762 567443.507 5937686.264</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.10">
                  <gml:posList>567443.507 5937686.264 567445.762 5937685.099</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.11">
                  <gml:posList>567445.762 5937685.099 567448.434 5937683.72</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.12">
                  <gml:posList>567448.434 5937683.72 567451.021 5937668.836</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.13">
                  <gml:posList>567451.021 5937668.836 567450.952 5937668.824</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.14">
                  <gml:posList>567450.952 5937668.824 567452.176 5937661.63</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.15">
                  <gml:posList>567452.176 5937661.63 567453.455 5937654.108</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.16">
                  <gml:posList>567453.455 5937654.108 567442.758 5937634.17</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.17">
                  <gml:posList>567442.758 5937634.17 567434.477 5937618.73</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.18">
                  <gml:posList>567434.477 5937618.73 567428.223 5937615.867</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_b91eb199-6c4e-462e-beea-040f9c618c83.interior.0.19">
                  <gml:posList>567428.223 5937615.867 567410.377 5937607.7</gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:interior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <nutzungsform>2000</nutzungsform>
      <hatDarstellungMitBesondZweckbest>false</hatDarstellungMitBesondZweckbest>
    </SO_Strassenverkehr>
  </gml:featureMember>
  <gml:featureMember>
    <BP_HoehenMass gml:id="GML_91e2ff9f-d5ff-4c87-84ae-9a6a02ddeed3">
      <text>Straßenhöhe bezogen auf NHN</text>
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <h uom="m">6.7</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_7e2310e1-0c17-4600-b91c-f016e3701bc9">
          <gml:pos>567453.759 5937669.764</gml:pos>
        </gml:Point>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <nordwinkel uom="grad">0.0</nordwinkel>
    </BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BesondererNutzungszweckFlaeche gml:id="GML_9775f239-210a-4d61-ad8f-251f5a31296e">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <wirdDargestelltDurch xlink:href="#GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a"/>
      <wirdDargestelltDurch xlink:href="#GML_f44449a4-6628-4c84-9dfe-d82b4dafca06"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_4417bd21-26bf-48ec-805b-19c7d1bf461b">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="GML_4417bd21-26bf-48ec-805b-19c7d1bf461b.exterior.0">
                  <gml:segments>
                    <gml:ArcString>
                      <gml:posList>567406.223 5937609.688 567407.834857796 5937607.72206805 567410.377 5937607.7</gml:posList>
                    </gml:ArcString>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_4417bd21-26bf-48ec-805b-19c7d1bf461b.exterior.1">
                  <gml:posList>567410.377 5937607.7 567428.223 5937615.867</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_4417bd21-26bf-48ec-805b-19c7d1bf461b.exterior.2">
                  <gml:posList>567428.223 5937615.867 567421.935 5937629.605</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_4417bd21-26bf-48ec-805b-19c7d1bf461b.exterior.3">
                  <gml:posList>567421.935 5937629.605 567403.316 5937621.126</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="GML_4417bd21-26bf-48ec-805b-19c7d1bf461b.exterior.4">
                  <gml:posList>567403.316 5937621.126 567406.223 5937609.688</gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <GRZ>1.0</GRZ>
      <zweckbestimmung>Kiosk</zweckbestimmung>
    </BP_BesondererNutzungszweckFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a">
      <art>xplan:GRZ</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_a5e04450-b5eb-4ca9-9ef1-d8c75f9940f0">
          <gml:pos>567412.297 5937615.306</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_f44449a4-6628-4c84-9dfe-d82b4dafca06">
      <art>xplan:GRZ</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_0c42e4eb-c85d-496d-9b9e-b75135217265">
          <gml:pos>567416.532 5937621.891</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_55177fec-7599-4785-a8e2-806742a9b2d9">
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>6000</bezugspunkt>
          <h uom="m">22.5</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <wirdDargestelltDurch xlink:href="#GML_796aa811-807f-4550-abab-24b9cc8aa359"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_2154e790-03ec-4467-a869-d4ff05c3593e">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567419.705 5937661.78 567415.353 5937672.325 567403.0 5937667.139 567407.329 5937656.657 567419.705 5937661.78</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Z>5</Z>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_796aa811-807f-4550-abab-24b9cc8aa359">
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</art>
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</art>
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_55177fec-7599-4785-a8e2-806742a9b2d9"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_e8cfedbc-5bb4-45d9-a360-b7c41058a6c9">
          <gml:pos>567421.862 5937659.0</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_bff033ce-687f-48fe-b27c-542b545fb292">
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>6000</bezugspunkt>
          <h uom="m">30.0</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <wirdDargestelltDurch xlink:href="#GML_190f5132-7a3c-4e3f-900e-0705faba810a"/>
      <wirdDargestelltDurch xlink:href="#GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_f83eff71-d44c-45fa-bc58-2c11b852418d">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567450.952 5937668.824 567448.434 5937683.72 567443.507 5937686.264 567432.779 5937681.762 567433.538 5937679.955 567437.269 5937671.067 567437.993 5937669.342 567439.063 5937666.793 567450.952 5937668.824</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Z>6</Z>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_190f5132-7a3c-4e3f-900e-0705faba810a">
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</art>
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</art>
      <art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_e6fc38c1-f1ba-4087-afd8-c2fff65964d6">
          <gml:pos>567439.986 5937674.081</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea">
      <art>xplan:Z</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_7aeb9650-3653-486d-b711-ece2c616c35e">
          <gml:pos>567440.194 5937679.946</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_StrassenbegrenzungsLinie gml:id="GML_5355860b-25fa-401c-a2ea-bb70654d17ab">
      <ebene>0</ebene>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:CompositeCurve srsName="EPSG:25832" gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e">
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.0">
              <gml:posList>567406.223 5937609.688 567403.316 5937621.126</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.1">
              <gml:posList>567403.316 5937621.126 567394.606 5937655.401</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.2">
              <gml:posList>567394.606 5937655.401 567393.37 5937660.265</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.3">
              <gml:posList>567393.37 5937660.265 567395.687 5937664.07</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.4">
              <gml:posList>567395.687 5937664.07 567403.0 5937667.139</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.5">
              <gml:posList>567403.0 5937667.139 567415.353 5937672.325</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.6">
              <gml:posList>567415.353 5937672.325 567433.538 5937679.955</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.7">
              <gml:posList>567433.538 5937679.955 567432.779 5937681.762</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.8">
              <gml:posList>567432.779 5937681.762 567443.507 5937686.264</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.9">
              <gml:posList>567443.507 5937686.264 567445.762 5937685.099</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.10">
              <gml:posList>567445.762 5937685.099 567448.434 5937683.72</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.11">
              <gml:posList>567448.434 5937683.72 567451.021 5937668.836</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.12">
              <gml:posList>567451.021 5937668.836 567450.952 5937668.824</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.13">
              <gml:posList>567450.952 5937668.824 567452.176 5937661.63</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.14">
              <gml:posList>567452.176 5937661.63 567453.455 5937654.108</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.15">
              <gml:posList>567453.455 5937654.108 567442.758 5937634.17</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.16">
              <gml:posList>567442.758 5937634.17 567434.477 5937618.73</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.17">
              <gml:posList>567434.477 5937618.73 567428.223 5937615.867</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.18">
              <gml:posList>567428.223 5937615.867 567410.377 5937607.7</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:Curve gml:id="GML_b48e82d2-e506-44ea-a3cf-8be47ef4502e.19">
              <gml:segments>
                <gml:ArcString>
                  <gml:posList>567410.377 5937607.7 567407.834857796 5937607.72206805 567406.223 5937609.688</gml:posList>
                </gml:ArcString>
              </gml:segments>
            </gml:Curve>
          </gml:curveMember>
        </gml:CompositeCurve>
      </position>
    </BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e1568a2b-1768-4889-aedb-95449c029c25">
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>6000</bezugspunkt>
          <h uom="m">22.5</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <wirdDargestelltDurch xlink:href="#GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91"/>
      <rechtscharakter>1000</rechtscharakter>
      <position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_7544d298-86e9-4775-9e01-7b2d2eaf9c59">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567437.993 5937669.342 567437.269 5937671.067 567433.538 5937679.955 567415.353 5937672.325 567419.705 5937661.78 567437.993 5937669.342</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Z>4</Z>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91">
      <art>xplan:Z</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <dientZurDarstellungVon xlink:href="#GML_e1568a2b-1768-4889-aedb-95449c029c25"/>
      <position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_4b1f9eae-a579-47b0-b732-86b2da01353a">
          <gml:pos>567421.339 5937668.137</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0.0</drehwinkel>
      <skalierung>1.0</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_LPO gml:id="GML_de628759-edb3-4f31-9d43-7d4f816bfb0c">
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <position>
        <gml:LineString srsName="EPSG:25832" gml:id="GML_b5fcca77-d75d-4537-993c-53bac4d79a8c">
          <gml:posList>567426.484 5937661.969 567427.781 5937668.164</gml:posList>
        </gml:LineString>
      </position>
    </XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_LPO gml:id="GML_82075a17-ae02-4228-a4ac-debedfd66c1e">
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <position>
        <gml:LineString srsName="EPSG:25832" gml:id="GML_e9d2c138-6861-4fa9-8814-980c59cef55e">
          <gml:posList>567417.901 5937658.752 567411.794 5937660.897</gml:posList>
        </gml:LineString>
      </position>
    </XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_LPO gml:id="GML_a435c756-f459-41ce-b27c-d5992aa8f87e">
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <position>
        <gml:LineString srsName="EPSG:25832" gml:id="GML_172fc8b5-d8e2-469f-af07-92382c8f017e">
          <gml:posList>567434.065 5937655.343 567437.337 5937661.831</gml:posList>
        </gml:LineString>
      </position>
    </XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_LPO gml:id="GML_7d7588c9-1211-471e-ad15-8dd9f6605701">
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458"/>
      <position>
        <gml:LineString srsName="EPSG:25832" gml:id="GML_e632d5f5-d76a-4668-8fd0-bc72b931ce27">
          <gml:posList>567401.041 5937656.059 567400.372 5937660.34</gml:posList>
        </gml:LineString>
      </position>
    </XP_LPO>
  </gml:featureMember>
</XPlanAuszug>
