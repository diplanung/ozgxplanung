<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<xplan:XPlanAuszug xmlns:xplan="http://www.xplanung.de/xplangml/5/2" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wfs="http://www.opengis.net/wfs" gml:id="GML_5a5b6542-5745-47fd-9bac-bf8ca80483c6">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>565466.357 5932633.546</gml:lowerCorner>
      <gml:upperCorner>566039.903 5932821.874</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_a3d4da50-1ef8-4b7e-9982-cebbdd5bc462">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565466.357 5932633.546</gml:lowerCorner>
          <gml:upperCorner>566039.903 5932821.874</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan003_5-2_SoapUI-XPlanManagerAPI</xplan:name>
      <xplan:beschreibung>Der Bebauungsplan BPlan003_5-2 für das Gebiet der Landzunge zwischen Grasbrookhafen und Norderelbe sowie östlich der Landzunge bis zur Chicagostraße (Bezirk Hamburg-Mitte, Ortsteil 103) wird festgestellt. Das Plangebiet wird wie folgt begrenzt: West- und Nordgrenze des Flurstücks 2523 (alt: 2377, 1963 - Strandhöft und Hübenerkai), über das Flurstück 6596 (alt: 2021 - Marco-Polo-Terrassen), Ostgrenzen der Flurstücke 6596 und 2523, Nordgrenze des Flurstücks 2068, Westgrenzen der Flurstücke 2522 (alt: 2371 - Hübenerstraße) und 2370, über die Flurstücke 2370 und 2522, Ostgrenze des Flurstücks 2486 (alt: 2375), über das Flurstück 2522 (Vancouverstraße), Ostgrenzen der Flurstücke 2374 (alt: 1963) und 2508, Südgrenzen der Flurstücke 2508 und 2507, über das Flurstück 2495 (alt: 2376 - Chicagokai), Südgrenzen der Flurstücke 2495 und 2523 der Gemarkung Altstadt-Süd.</xplan:beschreibung>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:MultiSurface srsName="EPSG:25832" gml:id="GML_8b32800f-1ff7-4b9c-ab79-8fc8ae4b4600">
          <gml:surfaceMember>
            <gml:Polygon srsName="EPSG:25832" gml:id="GML_e6000682-a5be-4e2a-9c3c-5923fe69cfe4">
              <gml:exterior>
                <gml:Ring>
                  <gml:curveMember>
                    <gml:Curve srsName="EPSG:25832" gml:id="GML_567e266f-2fe2-42d1-aa27-9eb0bf1f4993">
                      <gml:segments>
                        <gml:LineStringSegment interpolation="linear">
                          <gml:posList srsDimension="2" count="4">566018.685 5932818.208 566018.219 5932821.874 565886.529 5932785.604 565886.286 5932786.487 </gml:posList>
                        </gml:LineStringSegment>
                        <gml:ArcString interpolation="circularArc3Points">
                          <gml:posList srsDimension="2" count="7">565886.286 5932786.487 565884.260 5932786.076 565882.196 5932785.945 565879.536 5932786.709 565877.668 5932788.751 565876.076 5932792.569 565875.090 5932796.586 </gml:posList>
                        </gml:ArcString>
                        <gml:LineStringSegment interpolation="linear">
                          <gml:posList srsDimension="2" count="30">565875.090 5932796.586 565877.167 5932780.277 565848.013 5932771.824 565846.081 5932786.990 565844.345 5932800.629 565838.579 5932800.871 565817.704 5932801.748 565785.618 5932803.097 565785.411 5932798.091 565785.061 5932789.623 565771.225 5932790.194 565750.221 5932791.062 565725.312 5932792.551 565692.462 5932794.402 565667.474 5932795.494 565643.556 5932796.362 565604.863 5932797.583 565561.744 5932799.532 565544.463 5932800.651 565518.984 5932801.683 565490.864 5932802.354 565490.872 5932803.669 565469.410 5932804.104 565469.269 5932803.990 565466.939 5932787.432 565466.357 5932783.298 565467.303 5932781.812 565619.348 5932722.606 565619.364 5932722.462 565824.500 5932642.091 </gml:posList>
                        </gml:LineStringSegment>
                        <gml:ArcString interpolation="circularArc3Points">
                          <gml:posList srsDimension="2" count="7">565824.500 5932642.091 565829.381 5932640.364 565834.348 5932638.903 565838.824 5932637.762 565843.378 5932636.991 565848.420 5932636.431 565853.483 5932636.125 </gml:posList>
                        </gml:ArcString>
                        <gml:LineStringSegment interpolation="linear">
                          <gml:posList srsDimension="2" count="13">565853.483 5932636.125 565895.671 5932634.930 565940.829 5932633.546 565939.582 5932647.538 565939.560 5932649.199 565958.118 5932648.298 566039.903 5932651.651 566032.314 5932711.230 566029.946 5932729.803 566026.381 5932757.798 566020.325 5932805.339 566018.691 5932818.163 566018.685 5932818.208 </gml:posList>
                        </gml:LineStringSegment>
                      </gml:segments>
                    </gml:Curve>
                  </gml:curveMember>
                </gml:Ring>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#GML_2c404c4d-ea5a-4a1e-9a33-44b0216db2ee"/>
      <xplan:texte xlink:href="#GML_2577d7ff-6eba-421d-bb14-e1e07dc28acc"/>
      <xplan:texte xlink:href="#GML_79bd11df-a261-46ed-8059-5c50322eace7"/>
      <xplan:texte xlink:href="#GML_780db5ba-9a30-4f5c-8b8c-dc2140285545"/>
      <xplan:texte xlink:href="#GML_bbd24b02-d2b0-4363-b215-0103b5f536a2"/>
      <xplan:texte xlink:href="#GML_0004eb97-9458-4ec0-8d77-657bf88d18b8"/>
      <xplan:texte xlink:href="#GML_0cb41294-2033-4a5a-a157-f7b249731d5d"/>
      <xplan:texte xlink:href="#GML_a06eb58c-85b0-4c8e-8611-5cadb4bc4f9c"/>
      <xplan:texte xlink:href="#GML_ff170ce3-93ea-4eba-8a3c-2c857a97ca78"/>
      <xplan:texte xlink:href="#GML_4a3632a9-9dfc-42b0-a651-e3f1f6513c54"/>
      <xplan:texte xlink:href="#GML_16d136cb-fe5f-4867-8a22-40850fa95ec7"/>
      <xplan:texte xlink:href="#GML_30ca1209-45fc-4a5e-a5df-c40f9cdd392f"/>
      <xplan:texte xlink:href="#GML_6af2b297-49f7-4b0a-883d-b73fab5571a9"/>
      <xplan:texte xlink:href="#GML_e06f8260-990e-4f04-913b-1f54d14705c4"/>
      <xplan:texte xlink:href="#GML_c9f52069-f701-446d-9208-c83f72dce875"/>
      <xplan:texte xlink:href="#GML_51e6e5f3-b8ca-4a73-8d1e-70c2410388c9"/>
      <xplan:texte xlink:href="#GML_bddf163c-f007-4fe7-8fc7-71d7cb8d11c1"/>
      <xplan:texte xlink:href="#GML_a2100ab4-cb15-434a-958e-c3ff29115600"/>
      <xplan:texte xlink:href="#GML_f0accd68-1dde-4113-8e4d-854790f72749"/>
      <xplan:texte xlink:href="#GML_06fc76d6-d678-440e-a298-76c3bddb53d4"/>
      <xplan:texte xlink:href="#GML_ec0331a4-a665-42e5-a0ef-d1ff1a682525"/>
      <xplan:texte xlink:href="#GML_9c11408b-f22f-4643-b0f5-cc08ffc810b3"/>
      <xplan:texte xlink:href="#GML_6921793d-c644-49dc-a6e3-166992db081f"/>
      <xplan:texte xlink:href="#GML_50360564-32c3-4aec-9737-b61c5161debd"/>
      <xplan:texte xlink:href="#GML_6aa02183-2b23-40a0-88b4-a8dc4b8ac248"/>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>103</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2019-06-18</xplan:rechtsverordnungsDatum>
      <xplan:veraenderungssperre>false</xplan:veraenderungssperre>
      <xplan:staedtebaulicherVertrag>false</xplan:staedtebaulicherVertrag>
      <xplan:erschliessungsVertrag>false</xplan:erschliessungsVertrag>
      <xplan:durchfuehrungsVertrag>false</xplan:durchfuehrungsVertrag>
      <xplan:gruenordnungsplan>false</xplan:gruenordnungsplan>
      <xplan:bereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_d4750868-0f48-4b92-a24c-217a691c2d92">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565785.411 5932798.091</gml:lowerCorner>
          <gml:upperCorner>565753.337 5932791.111</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:rasterBasis xlink:href="#GML_b3ebb4e7-aa05-4cf3-b431-8605d0c517eb"/>
      <xplan:planinhalt xlink:href="#GML_9a9b2325-22a4-4180-b00a-8cd478def796"/>
      <xplan:planinhalt xlink:href="#GML_ca294177-4054-4575-a750-fcb8313d738f"/>
      <xplan:planinhalt xlink:href="#GML_b27cdc10-b8b2-4867-84a7-52781599ae44"/>
      <xplan:planinhalt xlink:href="#GML_dc74f22e-26a1-4805-98b4-daee8fe2d014"/>
      <xplan:planinhalt xlink:href="#GML_092dc747-9a4a-4bf2-8e13-acda57503fde"/>
      <xplan:planinhalt xlink:href="#GML_21f1e4ac-6237-479a-815d-b9d94bf441e0"/>
      <xplan:planinhalt xlink:href="#GML_6e85d151-b81e-470b-8319-67af0391cd7e"/>
      <xplan:planinhalt xlink:href="#GML_cf427698-f8aa-42bf-a20c-4991bf24b981"/>
      <xplan:planinhalt xlink:href="#GML_67789f17-f7f2-4e6d-aac0-5de2b1797227"/>
      <xplan:planinhalt xlink:href="#GML_67c4c97d-b10a-430b-b7c8-baecf8bf4b7b"/>
      <xplan:planinhalt xlink:href="#GML_d5bf168a-6ce9-4960-81f0-af5f4cea6e73"/>
      <xplan:planinhalt xlink:href="#GML_ea5cbdcf-d663-4b3d-a275-fa4d87cb7ac6"/>
      <xplan:planinhalt xlink:href="#GML_15b32247-bdb8-49f3-9e7f-4d4d36fcda09"/>
      <xplan:planinhalt xlink:href="#GML_4f9749fb-b24d-49b5-8618-2e46d14cea74"/>
      <xplan:planinhalt xlink:href="#GML_e22ef689-31a8-48e6-9f04-5c2cac0ddb9a"/>
      <xplan:planinhalt xlink:href="#GML_c5e9d7b7-c8e2-4fea-8add-e4e247eebb00"/>
      <xplan:planinhalt xlink:href="#GML_61f783fe-c183-49c0-a69e-73a566a5709a"/>
      <xplan:planinhalt xlink:href="#GML_5f683752-d838-4f11-98d0-0c82c015814c"/>
      <xplan:planinhalt xlink:href="#GML_581669dd-b9d4-455b-99da-a78277533a4a"/>
      <xplan:planinhalt xlink:href="#GML_c216b9fc-7de3-452d-94e9-3b709e138385"/>
      <xplan:planinhalt xlink:href="#GML_86e5da01-14b0-4bbc-bece-dd2d31a66949"/>
      <xplan:planinhalt xlink:href="#GML_0c68adac-f80c-4065-9073-a3ae6647885a"/>
      <xplan:planinhalt xlink:href="#GML_82078959-8a74-46cb-9290-f3eeba601bfa"/>
      <xplan:planinhalt xlink:href="#GML_87df14da-fab8-4964-b5ed-319303280a69"/>
      <xplan:planinhalt xlink:href="#GML_17009554-c8b4-4b5d-8592-4d15b5aa78b6"/>
      <xplan:planinhalt xlink:href="#GML_9056efbc-a820-4e0a-ab68-f710974bede8"/>
      <xplan:planinhalt xlink:href="#GML_ee89b16f-c94d-4f6b-9ebb-29a5cc18e56c"/>
      <xplan:planinhalt xlink:href="#GML_b2feec06-3c69-408b-9226-4999a917ab0a"/>
      <xplan:planinhalt xlink:href="#GML_e4b1d827-0f7e-4b81-817d-21d982e6b206"/>
      <xplan:planinhalt xlink:href="#GML_34535da5-584f-4a53-b899-9c61a97a2181"/>
      <xplan:planinhalt xlink:href="#GML_7be06cd5-9d19-4c48-a414-1c8a312b2561"/>
      <xplan:planinhalt xlink:href="#GML_5ed3ecf3-fc36-453d-ad0d-47598c5c7f9d"/>
      <xplan:planinhalt xlink:href="#GML_96b54c5a-bd3b-4a36-b719-2b6d986ed052"/>
      <xplan:planinhalt xlink:href="#GML_b01e6178-59e6-4b0c-93ae-448838d0053c"/>
      <xplan:planinhalt xlink:href="#GML_0b0e7af4-8435-4711-843b-9259559be42e"/>
      <xplan:planinhalt xlink:href="#GML_69ee8dfa-8102-4291-9747-196eb9339388"/>
      <xplan:planinhalt xlink:href="#GML_632a2120-60b6-47b1-8107-3e79292cbae7"/>
      <xplan:planinhalt xlink:href="#GML_c1ae22b6-c053-4628-9b18-2726992774d4"/>
      <xplan:planinhalt xlink:href="#GML_b185f595-61cc-484e-9d44-d0174015a250"/>
      <xplan:planinhalt xlink:href="#GML_5f9737ad-11f4-43f9-9c83-1ba4d3e68cff"/>
      <xplan:planinhalt xlink:href="#GML_69208cea-ba5b-4e2a-b0b8-c0b0361330a0"/>
      <xplan:planinhalt xlink:href="#GML_b7402418-df72-4f5f-bb77-e2252de42228"/>
      <xplan:planinhalt xlink:href="#GML_07e76da2-4540-4458-9c21-855b61a7d5de"/>
      <xplan:planinhalt xlink:href="#GML_b2f19715-2b4d-4dca-80bc-391e844d3b26"/>
      <xplan:planinhalt xlink:href="#GML_c80d2161-fdd5-416e-bedd-03ae39305310"/>
      <xplan:planinhalt xlink:href="#GML_06ee1e4c-14a2-4710-97b8-a57f9ce09058"/>
      <xplan:planinhalt xlink:href="#GML_82185115-542d-40cd-b755-c39e54ce7c1a"/>
      <xplan:planinhalt xlink:href="#GML_0855a411-8b3d-40bb-94c8-0f3f135e9c1b"/>
      <xplan:planinhalt xlink:href="#GML_3616e3dd-c0df-416a-97aa-56660f5b223f"/>
      <xplan:planinhalt xlink:href="#GML_ddc42fdd-602e-4227-9f04-921b8f4409bc"/>
      <xplan:planinhalt xlink:href="#GML_ec7c1d59-e49f-49a6-881c-1178845dbcc1"/>
      <xplan:planinhalt xlink:href="#GML_50e5c61a-f9e1-4099-b8cd-0611b6b61695"/>
      <xplan:planinhalt xlink:href="#GML_6a67c2b6-fd48-41e8-a2dd-5f7828a4585b"/>
      <xplan:planinhalt xlink:href="#GML_a7030dc5-ac76-4f12-be82-0b7662618159"/>
      <xplan:planinhalt xlink:href="#GML_96425556-8eff-429f-9db5-ced4ae60516f"/>
      <xplan:planinhalt xlink:href="#GML_d1a0ee37-41b8-4c7c-b2ff-52a53d948e04"/>
      <xplan:planinhalt xlink:href="#GML_fb8ec91b-8bb3-492d-afd1-9d8323e9d38c"/>
      <xplan:planinhalt xlink:href="#GML_8bd2275f-7926-4f49-8a4e-55ae6a0f1037"/>
      <xplan:planinhalt xlink:href="#GML_1971f8c0-ca44-4fbe-8081-2b5d48521d35"/>
      <xplan:planinhalt xlink:href="#GML_d728f5c8-f2e2-48aa-8d95-0e48ce88e6a8"/>
      <xplan:planinhalt xlink:href="#GML_7a3a837b-cfee-4c4c-86b5-ba172fac1794"/>
      <xplan:planinhalt xlink:href="#GML_f958a30d-b144-4701-9567-b57e96729735"/>
      <xplan:planinhalt xlink:href="#GML_b0ea0abf-cc1e-415e-86ca-a5fdf28e75c5"/>
      <xplan:planinhalt xlink:href="#GML_6d5caa25-9659-4499-b41a-d2621f6f0eef"/>
      <xplan:planinhalt xlink:href="#GML_e48d8bf8-b0d1-47a1-9932-db6c432c3f8f"/>
      <xplan:planinhalt xlink:href="#GML_bc39d635-6817-43f6-893c-e12724490016"/>
      <xplan:planinhalt xlink:href="#GML_9b442d6c-7876-438a-a903-e45032bd193a"/>
      <xplan:planinhalt xlink:href="#GML_1df34f6c-7eac-4c7b-a47a-f9d8d028099f"/>
      <xplan:planinhalt xlink:href="#GML_41f6b2c8-5bfb-46b3-8d00-4736a3ecd98c"/>
      <xplan:planinhalt xlink:href="#GML_852e66f0-b988-40b2-bebe-3f9c07e6f7e7"/>
      <xplan:planinhalt xlink:href="#GML_a3ac24b6-73a1-42c3-9cea-6b94fa3744b8"/>
      <xplan:planinhalt xlink:href="#GML_b83adf42-cb7d-4fc9-8fc5-ac15e82ee985"/>
      <xplan:planinhalt xlink:href="#GML_6e38a3b9-e3ca-4c82-a313-40f92bd1e53b"/>
      <xplan:planinhalt xlink:href="#GML_77593218-e457-49b8-b1c8-562dd73e88e0"/>
      <xplan:planinhalt xlink:href="#GML_6d1fe3ea-eb97-4cee-97d2-c96e6cc2d4b3"/>
      <xplan:planinhalt xlink:href="#GML_bb48d8e7-26db-4f84-b32f-e08938904528"/>
      <xplan:planinhalt xlink:href="#GML_b6b1355e-ff64-4a76-bc72-f677717507ae"/>
      <xplan:planinhalt xlink:href="#GML_b98aa7c7-6d0c-414c-b007-a91dd03c5197"/>
      <xplan:planinhalt xlink:href="#GML_911db489-58c9-489a-b23b-a31c561294ae"/>
      <xplan:planinhalt xlink:href="#GML_87862542-24ab-41fd-acba-45600290a502"/>
      <xplan:planinhalt xlink:href="#GML_f5810a83-37a7-450e-8eb9-15e0fa48536b"/>
      <xplan:planinhalt xlink:href="#GML_4c53246f-42ea-4bd4-b1b1-5ba972a52dfb"/>
      <xplan:planinhalt xlink:href="#GML_a1436623-01a9-491f-8cf5-921ee8ebaeac"/>
      <xplan:planinhalt xlink:href="#GML_eca52c4c-9060-4cb7-80d6-c2d3d14e9eac"/>
      <xplan:planinhalt xlink:href="#GML_10c76eed-8d63-44ff-ada7-3a421efd4e78"/>
      <xplan:planinhalt xlink:href="#GML_97eca282-8799-45ca-aea7-8e2642d0cff7"/>
      <xplan:planinhalt xlink:href="#GML_11f4408e-5a52-440d-9d26-c17b044d28a5"/>
      <xplan:planinhalt xlink:href="#GML_143fcf9b-61c3-41ef-b23b-0cff3764dfe5"/>
      <xplan:planinhalt xlink:href="#GML_66c4cb50-e3a4-4a5c-8bca-eb16d64d237a"/>
      <xplan:planinhalt xlink:href="#GML_13fb7d4e-6cb1-4025-b05e-0c505d857211"/>
      <xplan:planinhalt xlink:href="#GML_88098600-5cd2-431b-865c-7c79f14264f8"/>
      <xplan:planinhalt xlink:href="#GML_a4b8c804-abbd-4297-9c1a-d4a9d3464242"/>
      <xplan:planinhalt xlink:href="#GML_3016090c-8b24-498d-9717-cb5c93ea914a"/>
      <xplan:planinhalt xlink:href="#GML_4c2c00ad-a48b-4eda-adae-684835aa844c"/>
      <xplan:planinhalt xlink:href="#GML_61fc3efd-8db9-47a0-a58b-5b4677e15a02"/>
      <xplan:planinhalt xlink:href="#GML_865c1105-4be4-4df2-9e31-461aeb609802"/>
      <xplan:planinhalt xlink:href="#GML_b473520b-9de1-43a6-817e-8927c8377d70"/>
      <xplan:planinhalt xlink:href="#GML_c5c5b870-7402-499e-a07e-84b966ab2380"/>
      <xplan:planinhalt xlink:href="#GML_144e9d11-6d29-4852-ae13-8678467c56fc"/>
      <xplan:planinhalt xlink:href="#GML_fb306e82-538a-4679-bc52-690183ba0a7c"/>
      <xplan:planinhalt xlink:href="#GML_a2e0776a-110b-4782-a11e-d4fe70ca1d5f"/>
      <xplan:planinhalt xlink:href="#GML_a6594fbf-407f-4351-b3ae-5af4f2c87b01"/>
      <xplan:planinhalt xlink:href="#GML_41c4f008-fd82-4bd9-9a0b-4020ff16c7bc"/>
      <xplan:planinhalt xlink:href="#GML_8be64b5f-88ab-4f30-8b0d-a2e4f272ef4c"/>
      <xplan:planinhalt xlink:href="#GML_6f0cc62f-d5a1-4fef-81da-fc3723ff3e11"/>
      <xplan:planinhalt xlink:href="#GML_0cb13251-e2df-463f-bfa2-6fe59731f59f"/>
      <xplan:planinhalt xlink:href="#GML_bb35d750-2755-468c-92f2-6294b8d75f53"/>
      <xplan:planinhalt xlink:href="#GML_70ae082f-d9dc-4308-80a2-822c369d97d8"/>
      <xplan:planinhalt xlink:href="#GML_1048f9c7-e935-4593-bea0-46d9eee4d619"/>
      <xplan:planinhalt xlink:href="#GML_0c3dfa93-9efb-4283-8b1f-6db1d2c5bc8b"/>
      <xplan:planinhalt xlink:href="#GML_ce97a57d-e686-466f-829c-be3cca0c9f90"/>
      <xplan:planinhalt xlink:href="#GML_245f5228-dbfc-445a-a50c-93bd98f0356f"/>
      <xplan:planinhalt xlink:href="#GML_a6f20217-5907-4cc7-bd95-6fc7d41835c4"/>
      <xplan:planinhalt xlink:href="#GML_72fcbdf6-e245-4f05-a185-36a9e00c76e4"/>
      <xplan:planinhalt xlink:href="#GML_0a4866da-e023-4092-94aa-6c0213bec033"/>
      <xplan:planinhalt xlink:href="#GML_2a1e9c3f-efe0-4cb8-9954-aa5969381a04"/>
      <xplan:planinhalt xlink:href="#GML_6451d01a-9722-4da0-96b2-200cff85b0f7"/>
      <xplan:planinhalt xlink:href="#GML_b6aaa0d7-dd24-41d6-bdaa-9ef9cebdd678"/>
      <xplan:planinhalt xlink:href="#GML_a2612630-e9fb-44d4-824a-96e371fb5885"/>
      <xplan:planinhalt xlink:href="#GML_56d95de6-a232-4dd2-b332-30871aeca91d"/>
      <xplan:planinhalt xlink:href="#GML_96abb9d8-6277-4e08-9a41-091df5de3360"/>
      <xplan:planinhalt xlink:href="#GML_f4e30c51-ba59-4730-9604-d97ceda14c17"/>
      <xplan:planinhalt xlink:href="#GML_2211fd90-eac2-4501-bbe8-b446d3c3293e"/>
      <xplan:planinhalt xlink:href="#GML_7a92dc24-ef04-43f0-8dd4-e7a0dc24c4ef"/>
      <xplan:planinhalt xlink:href="#GML_66c1df3e-2b4c-4fb5-9650-371655a5b8f1"/>
      <xplan:planinhalt xlink:href="#GML_53ee118d-8c4c-45b5-a951-4605817e142f"/>
      <xplan:planinhalt xlink:href="#GML_30d3e1fa-b1fa-42fe-994f-d617658cb412"/>
      <xplan:planinhalt xlink:href="#GML_da343c9f-3532-40dc-a39c-27823fa28b84"/>
      <xplan:planinhalt xlink:href="#GML_c477ec48-117a-47c3-ab34-98a809d7f950"/>
      <xplan:planinhalt xlink:href="#GML_e13588fe-85f5-41f0-946f-ca0fd7508555"/>
      <xplan:planinhalt xlink:href="#GML_fa85402a-4b45-436a-a19f-fdd6529ac3d0"/>
      <xplan:planinhalt xlink:href="#GML_0dc09d65-60cd-4506-a28c-cc81750ddfda"/>
      <xplan:planinhalt xlink:href="#GML_0c50d59b-aa4d-418a-ae99-df7bc18ed03e"/>
      <xplan:planinhalt xlink:href="#GML_1b538451-90d0-41b4-8e63-8a30ca9cd087"/>
      <xplan:planinhalt xlink:href="#GML_a08b6875-0376-4e69-98e5-96f685c1a0b8"/>
      <xplan:planinhalt xlink:href="#GML_7ddc701d-5523-45cc-b1c2-57a358627960"/>
      <xplan:planinhalt xlink:href="#GML_fd6c2735-5c90-494e-acc2-e027bedf075d"/>
      <xplan:praesentationsobjekt xlink:href="#GML_057e45f1-bfd0-4d9e-affc-317be4dec835"/>
      <xplan:praesentationsobjekt xlink:href="#GML_0a4b386f-605e-4742-baf3-adb7e931fe82"/>
      <xplan:praesentationsobjekt xlink:href="#GML_ed6831a9-45a2-48a3-81c4-1c40ac213417"/>
      <xplan:praesentationsobjekt xlink:href="#GML_edc7c7fa-3a65-4a60-8687-e8c0294838bf"/>
      <xplan:praesentationsobjekt xlink:href="#GML_230d0675-209b-439d-b2f5-9b7216ab7c9b"/>
      <xplan:praesentationsobjekt xlink:href="#GML_b10ac8ff-a802-4e14-8756-f9dda612989d"/>
      <xplan:praesentationsobjekt xlink:href="#GML_4476acfb-1b28-4331-86c2-35777ed50404"/>
      <xplan:praesentationsobjekt xlink:href="#GML_1c629753-705c-4932-8b2f-d02d970dca5b"/>
      <xplan:praesentationsobjekt xlink:href="#GML_de62f70e-d6e5-4b50-9702-d329b6a93c04"/>
      <xplan:praesentationsobjekt xlink:href="#GML_64b2b4f1-3b4d-4360-898e-f1fab051b1cf"/>
      <xplan:praesentationsobjekt xlink:href="#GML_ab9d2ac8-1f4f-4f57-b54f-24b493c377c2"/>
      <xplan:praesentationsobjekt xlink:href="#GML_48d1383b-f1d4-47c9-a7ae-ead916a60cb8"/>
      <xplan:praesentationsobjekt xlink:href="#GML_92fa22fc-2e8a-4c0c-96d3-6c36bf65273e"/>
      <xplan:praesentationsobjekt xlink:href="#GML_908a9646-520c-456c-b448-91cf6f0ea225"/>
      <xplan:praesentationsobjekt xlink:href="#GML_3cf0aaf7-21ec-457f-9c96-f595380a9453"/>
      <xplan:praesentationsobjekt xlink:href="#GML_f998ec7e-4496-452c-acef-fec212cd39c9"/>
      <xplan:praesentationsobjekt xlink:href="#GML_ede3aab2-e41a-4569-8775-6280fe7115ce"/>
      <xplan:praesentationsobjekt xlink:href="#GML_7e373680-91fb-49a7-882f-71a0838de704"/>
      <xplan:praesentationsobjekt xlink:href="#GML_ba716834-b859-456d-a300-19d5e429ee2a"/>
      <xplan:praesentationsobjekt xlink:href="#GML_42db0425-d7bd-44e4-97ae-c3f70ab80061"/>
      <xplan:praesentationsobjekt xlink:href="#GML_50e2c425-aeed-4652-b5ad-ad25a8522993"/>
      <xplan:praesentationsobjekt xlink:href="#GML_6d1389a1-b1c0-460d-99d3-f792ecc96b5d"/>
      <xplan:praesentationsobjekt xlink:href="#GML_fb4823b8-5104-45b8-a1d7-46ee7c37cbb3"/>
      <xplan:praesentationsobjekt xlink:href="#GML_e23b69d9-b633-4b1b-b5c0-bd27b6b35d36"/>
      <xplan:praesentationsobjekt xlink:href="#GML_551efb2f-83bb-4f95-8b77-1c7a119e0216"/>
      <xplan:praesentationsobjekt xlink:href="#GML_68e516b8-4840-44ef-8781-b632022bb1cf"/>
      <xplan:praesentationsobjekt xlink:href="#GML_65d9180b-f37d-4a8e-acee-cd570aff9612"/>
      <xplan:praesentationsobjekt xlink:href="#GML_01f7b9c6-f77f-4614-a6cb-bb1b25ff3b05"/>
      <xplan:praesentationsobjekt xlink:href="#GML_99928044-35dc-4174-a1aa-5427c205808e"/>
      <xplan:praesentationsobjekt xlink:href="#GML_90ad5034-c656-4527-afa7-c81dff108352"/>
      <xplan:praesentationsobjekt xlink:href="#GML_4a13a610-a0c4-4175-b41d-e459290be6a2"/>
      <xplan:praesentationsobjekt xlink:href="#GML_d363b0ce-c578-4052-9546-96bc9b3f5042"/>
      <xplan:praesentationsobjekt xlink:href="#GML_d8a263dc-f61b-46ca-8369-d683e2bde216"/>
      <xplan:praesentationsobjekt xlink:href="#GML_0b71b288-182b-440f-b9dc-3f104e0b4add"/>
      <xplan:praesentationsobjekt xlink:href="#GML_937f8f81-340a-447d-bf09-5c8360c8b8d8"/>
      <xplan:praesentationsobjekt xlink:href="#GML_830cd12a-b58c-46d0-825e-643f9d4be8af"/>
      <xplan:praesentationsobjekt xlink:href="#GML_ca7ab734-78e3-4b47-839c-cedf3bbc7bcf"/>
      <xplan:praesentationsobjekt xlink:href="#GML_50afe552-4272-4213-85fa-f8f8666a5f7a"/>
      <xplan:praesentationsobjekt xlink:href="#GML_d10aab1f-32af-42c1-9984-c0a8bcda02ad"/>
      <xplan:praesentationsobjekt xlink:href="#GML_b67a57a6-6aec-4590-8ab2-a22e21ee7149"/>
      <xplan:praesentationsobjekt xlink:href="#GML_82c0e20c-f4eb-476e-bec2-04fd6d79bb0d"/>
      <xplan:praesentationsobjekt xlink:href="#GML_abc3741a-7af6-4d6d-b01c-2a73ecd6bd48"/>
      <xplan:praesentationsobjekt xlink:href="#GML_453eb485-75cf-4d56-979a-25c8effab0f5"/>
      <xplan:praesentationsobjekt xlink:href="#GML_777152b5-d0b6-4da5-81bd-fde94f5716a0"/>
      <xplan:praesentationsobjekt xlink:href="#GML_39484140-e5a1-448e-aaba-4e905abfd378"/>
      <xplan:praesentationsobjekt xlink:href="#GML_89c273d6-7f81-4b54-a29d-aa469ed8d6df"/>
      <xplan:praesentationsobjekt xlink:href="#GML_4a0b9fc1-6bf2-4394-a004-c5a756a62df1"/>
      <xplan:praesentationsobjekt xlink:href="#GML_bd1d438f-771d-41b2-8298-9d40bb4e44b2"/>
      <xplan:praesentationsobjekt xlink:href="#GML_fb160f7b-ec24-48db-8e61-90794f6249c4"/>
      <xplan:praesentationsobjekt xlink:href="#GML_2c25e0f3-e0ff-48e5-ad01-3451f7876e91"/>
      <xplan:praesentationsobjekt xlink:href="#GML_8bd91d56-9ff8-4c04-917e-fd683afc0d7b"/>
      <xplan:praesentationsobjekt xlink:href="#GML_2eee5e20-893c-435e-b044-05ebc2b71856"/>
      <xplan:praesentationsobjekt xlink:href="#GML_0ae6b5a2-b9e4-4452-b901-0d170840c34b"/>
      <xplan:praesentationsobjekt xlink:href="#GML_181e563a-bc73-4e7a-b5d4-fc177f151dcb"/>
      <xplan:praesentationsobjekt xlink:href="#GML_b6f96831-804d-46d8-812e-7386edb6b998"/>
      <xplan:praesentationsobjekt xlink:href="#GML_4cdf9d17-eccd-4d8a-aeaf-6ab702f69cd3"/>
      <xplan:praesentationsobjekt xlink:href="#GML_bc65c102-ac8d-4045-926d-9e3c64c7cf7c"/>
      <xplan:praesentationsobjekt xlink:href="#GML_86c3b748-ca37-4c2e-b5c2-a91edf11d069"/>
      <xplan:praesentationsobjekt xlink:href="#GML_d541f15d-f012-40d2-b758-9d7d934d30f1"/>
      <xplan:praesentationsobjekt xlink:href="#GML_e7f6dbbf-73c0-41cb-a6fe-5087f03b261b"/>
      <xplan:praesentationsobjekt xlink:href="#GML_23c28af5-0f5f-40dc-867c-7d5352ac6485"/>
      <xplan:praesentationsobjekt xlink:href="#GML_e0c24079-67a8-4ec3-925a-d14962367962"/>
      <xplan:praesentationsobjekt xlink:href="#GML_4a1f49fc-d304-479d-aa04-5bff6e28ca34"/>
      <xplan:praesentationsobjekt xlink:href="#GML_9979086a-0c74-4d14-9e3c-b3028b821559"/>
      <xplan:praesentationsobjekt xlink:href="#GML_e4100735-5422-4ec9-b598-3577a4179daa"/>
      <xplan:praesentationsobjekt xlink:href="#GML_d3a25e72-ea8b-4056-b939-b3cc1e840c7e"/>
      <xplan:praesentationsobjekt xlink:href="#GML_a135caf0-72ff-4ac7-82cd-abbdc9455c25"/>
      <xplan:praesentationsobjekt xlink:href="#GML_059ebc29-e9a5-4917-acd3-55ba7eef6988"/>
      <xplan:praesentationsobjekt xlink:href="#GML_8676c057-52ed-484e-abdd-7f633473dc3f"/>
      <xplan:versionBauNVOText>Version vom 21.11.2017</xplan:versionBauNVOText>
      <xplan:gehoertZuPlan xlink:href="#GML_a3d4da50-1ef8-4b7e-9982-cebbdd5bc462"/>
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_9a9b2325-22a4-4180-b00a-8cd478def796">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565673.083 5932783.759</gml:lowerCorner>
          <gml:upperCorner>565753.337 5932791.111</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_a5500bb3-1b31-4624-b5c7-4162444cce95">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565673.083 5932791.111 565753.337 5932783.759 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>2000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_ca294177-4054-4575-a750-fcb8313d738f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565980.840 5932649.473</gml:lowerCorner>
          <gml:upperCorner>565986.781 5932696.225</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_e13a8108-adf4-4067-95e7-2fa99f971dd1">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565980.840 5932696.225 565986.781 5932649.473 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_b27cdc10-b8b2-4867-84a7-52781599ae44">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565783.337 5932734.503</gml:lowerCorner>
          <gml:upperCorner>565813.870 5932765.762</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2000</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">4.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_b67a57a6-6aec-4590-8ab2-a22e21ee7149"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ec95cb06-8299-4f9c-b5a5-291f1f958578">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="37">565813.870 5932763.441 565813.853 5932763.828 565813.770 5932764.207 565813.621 5932764.566 565813.413 5932764.893 565813.151 5932765.179 565812.844 5932765.415 565812.500 5932765.595 565812.130 5932765.712 565811.745 5932765.762 565811.391 5932765.750 565801.581 5932765.223 565784.183 5932764.290 565784.053 5932764.177 565783.737 5932763.787 565783.546 5932763.443 565783.412 5932763.054 565783.337 5932762.525 565785.344 5932751.307 565784.217 5932745.516 565783.789 5932743.315 565784.649 5932742.438 565784.370 5932736.386 565796.078 5932734.530 565796.426 5932734.503 565796.773 5932734.537 565797.109 5932734.631 565797.423 5932734.782 565797.706 5932734.985 565797.950 5932735.234 565805.725 5932744.779 565811.528 5932751.904 565811.726 5932752.156 565811.878 5932752.439 565811.978 5932752.743 565812.798 5932757.378 565813.870 5932763.441 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">565809.935 5932760.745 565809.546 5932752.121 565806.785 5932752.245 565802.942 5932745.749 565797.152 5932735.962 565786.795 5932736.545 565786.081 5932748.696 565786.212 5932751.586 565786.672 5932761.795 565809.935 5932760.745 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_dc74f22e-26a1-4805-98b4-daee8fe2d014">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565878.934 5932728.618</gml:lowerCorner>
          <gml:upperCorner>565905.170 5932772.299</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">0.0</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_f253a71a-8af4-4bd6-9055-a833e449d0b3">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">565900.360 5932772.299 565878.934 5932766.397 565883.744 5932728.618 565905.170 5932734.519 565904.337 5932741.175 565902.529 5932755.263 565900.360 5932772.299 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_092dc747-9a4a-4bf2-8e13-acda57503fde">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565887.287 5932649.333</gml:lowerCorner>
          <gml:upperCorner>565948.277 5932717.429</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(I)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_86c3b748-ca37-4c2e-b5c2-a91edf11d069"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_9979086a-0c74-4d14-9e3c-b3028b821559"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e430971a-ede2-4964-9457-1c89822d78d3">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">565944.373 5932717.429 565887.287 5932700.791 565893.645 5932650.856 565935.897 5932649.333 565933.724 5932666.423 565908.685 5932667.325 565905.985 5932688.533 565930.020 5932695.538 565931.757 5932681.887 565948.277 5932686.737 565944.373 5932717.429 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_21f1e4ac-6237-479a-815d-b9d94bf441e0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565541.828 5932649.634</gml:lowerCorner>
          <gml:upperCorner>565873.525 5932792.738</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(B, C, E, F, G, H, I,  K, L)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_908a9646-520c-456c-b448-91cf6f0ea225"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_d3a25e72-ea8b-4056-b939-b3cc1e840c7e"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_6921793d-c644-49dc-a6e3-166992db081f"/>
      <xplan:refTextInhalt xlink:href="#GML_a06eb58c-85b0-4c8e-8611-5cadb4bc4f9c"/>
      <xplan:refTextInhalt xlink:href="#GML_f0accd68-1dde-4113-8e4d-854790f72749"/>
      <xplan:refTextInhalt xlink:href="#GML_ff170ce3-93ea-4eba-8a3c-2c857a97ca78"/>
      <xplan:refTextInhalt xlink:href="#GML_0cb41294-2033-4a5a-a157-f7b249731d5d"/>
      <xplan:refTextInhalt xlink:href="#GML_780db5ba-9a30-4f5c-8b8c-dc2140285545"/>
      <xplan:refTextInhalt xlink:href="#GML_bddf163c-f007-4fe7-8fc7-71d7cb8d11c1"/>
      <xplan:refTextInhalt xlink:href="#GML_bbd24b02-d2b0-4363-b215-0103b5f536a2"/>
      <xplan:refTextInhalt xlink:href="#GML_4a3632a9-9dfc-42b0-a651-e3f1f6513c54"/>
      <xplan:refTextInhalt xlink:href="#GML_c9f52069-f701-446d-9208-c83f72dce875"/>
      <xplan:refTextInhalt xlink:href="#GML_9c11408b-f22f-4643-b0f5-cc08ffc810b3"/>
      <xplan:refTextInhalt xlink:href="#GML_16d136cb-fe5f-4867-8a22-40850fa95ec7"/>
      <xplan:refTextInhalt xlink:href="#GML_79bd11df-a261-46ed-8059-5c50322eace7"/>
      <xplan:refTextInhalt xlink:href="#GML_06fc76d6-d678-440e-a298-76c3bddb53d4"/>
      <xplan:refTextInhalt xlink:href="#GML_30ca1209-45fc-4a5e-a5df-c40f9cdd392f"/>
      <xplan:refTextInhalt xlink:href="#GML_51e6e5f3-b8ca-4a73-8d1e-70c2410388c9"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_93804580-3af1-4acd-b7f1-5394bc5dc214">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_a3ae0dd5-ab63-4d51-982b-146e9782802b">
                  <gml:segments>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">565591.675 5932792.465 565574.171 5932792.737 565556.667 5932792.535 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="4">565556.667 5932792.535 565551.019 5932792.437 565541.828 5932771.726 565580.306 5932754.650 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">565580.306 5932754.650 565595.709 5932747.978 565611.268 5932741.678 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="36">565611.268 5932741.678 565613.110 5932740.957 565626.281 5932735.806 565630.930 5932733.988 565629.719 5932730.882 565683.901 5932709.763 565685.116 5932712.880 565697.832 5932707.896 565740.121 5932691.320 565748.052 5932688.212 565776.875 5932676.914 565785.309 5932673.608 565794.479 5932670.021 565846.606 5932649.634 565873.373 5932650.580 565873.525 5932650.585 565866.147 5932708.540 565859.464 5932761.035 565849.328 5932761.490 565813.807 5932763.085 565804.114 5932763.520 565785.459 5932764.358 565783.066 5932764.466 565777.872 5932764.699 565773.687 5932764.887 565774.511 5932758.412 565760.086 5932758.992 565759.391 5932759.020 565752.746 5932766.197 565752.638 5932766.314 565753.168 5932779.557 565753.337 5932783.759 565673.083 5932791.111 565672.699 5932786.929 565650.945 5932789.863 565650.871 5932788.031 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="5">565650.871 5932788.031 565645.705 5932788.635 565640.534 5932789.193 565616.135 5932791.291 565591.675 5932792.465 </gml:posList>
                    </gml:ArcString>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6e85d151-b81e-470b-8319-67af0391cd7e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565681.892 5932691.320</gml:lowerCorner>
          <gml:upperCorner>565764.954 5932769.047</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_3078a3e5-a009-4eb5-a0a3-5557364b5ad4">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="15">565750.074 5932718.831 565749.389 5932724.218 565745.796 5932752.466 565701.248 5932754.257 565704.856 5932740.415 565705.824 5932740.667 565711.259 5932719.818 565719.064 5932716.759 565741.582 5932707.933 565740.121 5932691.320 565697.832 5932707.896 565681.892 5932769.047 565759.203 5932765.938 565764.954 5932720.724 565750.074 5932718.831 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_cf427698-f8aa-42bf-a20c-4991bf24b981">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565556.643 5932754.650</gml:lowerCorner>
          <gml:upperCorner>565580.306 5932792.535</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_64b2b4f1-3b4d-4360-898e-f1fab051b1cf"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_a3ecf010-c02a-4dc6-8874-424619352e1a">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="5">565556.667 5932792.535 565558.831 5932778.114 565566.021 5932765.427 565572.553 5932759.229 565580.306 5932754.650 </gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_67789f17-f7f2-4e6d-aac0-5de2b1797227">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565883.744 5932717.864</gml:lowerCorner>
          <gml:upperCorner>565942.202 5932782.154</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(I)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_830cd12a-b58c-46d0-825e-643f9d4be8af"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_a2100ab4-cb15-434a-958e-c3ff29115600"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_2b999144-0f33-4787-8607-882c18776b42">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">565942.202 5932734.503 565936.141 5932782.154 565900.360 5932772.299 565902.529 5932755.263 565921.751 5932760.557 565923.506 5932746.761 565904.337 5932741.175 565905.170 5932734.519 565883.744 5932728.618 565885.113 5932717.864 565942.202 5932734.503 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_67c4c97d-b10a-430b-b7c8-baecf8bf4b7b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565916.432 5932647.550</gml:lowerCorner>
          <gml:upperCorner>565916.432 5932647.550</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">5.1</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_fb1eeb52-0f2a-4ff6-bd48-6b67c5a470a4">
          <gml:pos>565916.432 5932647.550</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_d5bf168a-6ce9-4960-81f0-af5f4cea6e73">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565638.380 5932720.017</gml:lowerCorner>
          <gml:upperCorner>565671.581 5932757.682</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_efb892ff-8a6a-4219-b009-e6e5a14b762f">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">565638.380 5932731.075 565648.731 5932757.682 565671.581 5932753.124 565666.805 5932720.017 565638.380 5932731.075 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_ea5cbdcf-d663-4b3d-a275-fa4d87cb7ac6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565752.746 5932758.992</gml:lowerCorner>
          <gml:upperCorner>565760.086 5932766.197</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2000</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">4.3</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_ede3aab2-e41a-4569-8775-6280fe7115ce"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_8e332e8b-9826-458e-a759-feb8581daae4">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">565752.746 5932766.197 565759.391 5932759.020 565760.086 5932758.992 565759.203 5932765.938 565752.746 5932766.197 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_15b32247-bdb8-49f3-9e7f-4d4d36fcda09">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565782.836 5932736.386</gml:lowerCorner>
          <gml:upperCorner>565784.649 5932743.315</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Auskragung</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2000</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">4.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e1531326-7c4a-496a-aaba-455b2730e10a">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">565784.649 5932742.438 565783.789 5932743.315 565782.836 5932738.412 565782.858 5932737.914 565782.965 5932737.430 565783.101 5932737.101 565783.310 5932736.773 565783.545 5932736.517 565784.370 5932736.386 565784.649 5932742.438 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_4f9749fb-b24d-49b5-8618-2e46d14cea74">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565885.113 5932700.791</gml:lowerCorner>
          <gml:upperCorner>565887.287 5932717.864</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_f94b8cc1-adc9-411b-8d90-fc6c95877b7a">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="3">565887.287 5932700.791 565885.978 5932711.065 565885.113 5932717.864 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_e22ef689-31a8-48e6-9f04-5c2cac0ddb9a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565809.788 5932658.580</gml:lowerCorner>
          <gml:upperCorner>565859.366 5932730.058</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(K)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_d8a263dc-f61b-46ca-8369-d683e2bde216"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_ec0331a4-a665-42e5-a0ef-d1ff1a682525"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_27da64b1-afc1-4236-8e41-aa368900cd8f">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">565839.048 5932730.058 565812.416 5932714.429 565809.788 5932689.737 565814.587 5932682.436 565812.434 5932676.931 565859.366 5932658.580 565839.048 5932730.058 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c5e9d7b7-c8e2-4fea-8add-e4e247eebb00">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565905.985 5932666.423</gml:lowerCorner>
          <gml:upperCorner>565933.724 5932695.538</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(H)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_23c28af5-0f5f-40dc-867c-7d5352ac6485"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_9bdc5182-5734-4903-8461-0738d82a3f45">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">565931.757 5932681.887 565930.020 5932695.538 565905.985 5932688.533 565908.685 5932667.325 565933.724 5932666.423 565931.757 5932681.887 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Zzwingend>1</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_61f783fe-c183-49c0-a69e-73a566a5709a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565752.675 5932718.443</gml:lowerCorner>
          <gml:upperCorner>565861.185 5932762.434</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_6fae1045-df0a-4e64-b7c6-ff0e825445b5">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">565858.747 5932718.443 565861.185 5932725.432 565755.113 5932762.434 565752.675 5932755.445 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_5f683752-d838-4f11-98d0-0c82c015814c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565887.287 5932700.791</gml:lowerCorner>
          <gml:upperCorner>565944.373 5932717.429</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_48d1383b-f1d4-47c9-a7ae-ead916a60cb8"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_d1756918-a186-4801-bec9-8928cb00197d">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565944.373 5932717.429 565887.287 5932700.791 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BesondererNutzungszweckFlaeche gml:id="GML_581669dd-b9d4-455b-99da-a78277533a4a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565784.722 5932778.786</gml:lowerCorner>
          <gml:upperCorner>565846.639 5932803.096</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">17</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_ba716834-b859-456d-a300-19d5e429ee2a"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_99928044-35dc-4174-a1aa-5427c205808e"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_50afe552-4272-4213-85fa-f8f8666a5f7a"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_059ebc29-e9a5-4917-acd3-55ba7eef6988"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_2577d7ff-6eba-421d-bb14-e1e07dc28acc"/>
      <xplan:refTextInhalt xlink:href="#GML_9c11408b-f22f-4643-b0f5-cc08ffc810b3"/>
      <xplan:refTextInhalt xlink:href="#GML_16d136cb-fe5f-4867-8a22-40850fa95ec7"/>
      <xplan:refTextInhalt xlink:href="#GML_79bd11df-a261-46ed-8059-5c50322eace7"/>
      <xplan:refTextInhalt xlink:href="#GML_30ca1209-45fc-4a5e-a5df-c40f9cdd392f"/>
      <xplan:refTextInhalt xlink:href="#GML_51e6e5f3-b8ca-4a73-8d1e-70c2410388c9"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_fd3d907a-6ab6-422b-996d-7e4ef0ab0b22">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">565844.345 5932800.629 565838.579 5932800.871 565817.704 5932801.748 565785.618 5932803.096 565785.411 5932798.091 565785.061 5932789.623 565784.722 5932781.409 565787.821 5932781.269 565843.114 5932778.786 565846.639 5932782.612 565844.345 5932800.629 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Z>2</xplan:Z>
      <xplan:zweckbestimmung>SERVICEGEBÄUDE MARINA</xplan:zweckbestimmung>
    </xplan:BP_BesondererNutzungszweckFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Wasserrecht gml:id="GML_c216b9fc-7de3-452d-94e9-3b709e138385">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565466.357 5932633.546</gml:lowerCorner>
          <gml:upperCorner>565940.829 5932804.104</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Hochwassergefährdeter Bereich</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d5330b4a-6658-46d8-b2c6-9863f14ebf76">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_852de300-c801-4846-a80d-73cf7a44b76a">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="24">565817.183 5932799.207 565817.704 5932801.748 565785.618 5932803.097 565785.411 5932798.091 565785.061 5932789.623 565771.225 5932790.194 565750.221 5932791.062 565725.312 5932792.551 565692.462 5932794.402 565667.474 5932795.494 565643.556 5932796.362 565604.863 5932797.583 565561.744 5932799.532 565544.463 5932800.651 565518.984 5932801.683 565490.864 5932802.354 565490.872 5932803.669 565469.410 5932804.104 565469.269 5932803.990 565466.357 5932783.298 565467.303 5932781.812 565619.348 5932722.606 565619.364 5932722.462 565824.500 5932642.091 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">565824.500 5932642.091 565829.381 5932640.364 565834.348 5932638.903 565838.824 5932637.762 565843.378 5932636.991 565848.420 5932636.431 565853.483 5932636.125 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="25">565853.483 5932636.125 565895.671 5932634.930 565940.829 5932633.546 565939.582 5932647.538 565939.560 5932649.201 565935.897 5932649.333 565893.645 5932650.856 565887.287 5932700.791 565885.978 5932711.065 565866.147 5932708.540 565873.525 5932650.585 565846.606 5932649.634 565794.479 5932670.021 565785.309 5932673.608 565776.875 5932676.914 565748.052 5932688.212 565740.121 5932691.320 565697.832 5932707.896 565685.116 5932712.880 565683.901 5932709.763 565629.719 5932730.882 565630.930 5932733.988 565626.281 5932735.806 565613.110 5932740.957 565611.268 5932741.678 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="13">565611.268 5932741.678 565595.709 5932747.978 565580.306 5932754.650 565572.553 5932759.229 565566.021 5932765.427 565558.831 5932778.114 565556.667 5932792.535 565574.171 5932792.737 565591.675 5932792.465 565616.135 5932791.291 565640.534 5932789.193 565645.705 5932788.635 565650.871 5932788.031 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="12">565650.871 5932788.031 565650.945 5932789.863 565672.699 5932786.929 565753.168 5932779.557 565753.337 5932783.759 565784.722 5932781.409 565787.821 5932781.269 565787.877 5932782.528 565788.102 5932787.522 565803.995 5932797.619 565816.740 5932797.046 565817.183 5932799.207 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:artDerFestlegung>20002</xplan:artDerFestlegung>
      <xplan:istNatuerlichesUberschwemmungsgebiet>false</xplan:istNatuerlichesUberschwemmungsgebiet>
    </xplan:SO_Wasserrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_86e5da01-14b0-4bbc-bece-dd2d31a66949">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565859.464 5932650.585</gml:lowerCorner>
          <gml:upperCorner>565873.525 5932761.035</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_a2d02452-6e7f-405f-a6c8-cbad77d91e6f">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="3">565859.464 5932761.035 565866.147 5932708.540 565873.525 5932650.585 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_0c68adac-f80c-4065-9073-a3ae6647885a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565626.281 5932707.896</gml:lowerCorner>
          <gml:upperCorner>565753.337 5932791.111</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(G)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_4a13a610-a0c4-4175-b41d-e459290be6a2"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e06f8260-990e-4f04-913b-1f54d14705c4"/>
      <xplan:refTextInhalt xlink:href="#GML_06fc76d6-d678-440e-a298-76c3bddb53d4"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_fcb4b379-3f5e-4d68-bcf2-eb6c10a1b5fa">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_509af4de-b346-4728-a5e2-8be52822ade6">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="6">565753.168 5932779.557 565753.337 5932783.759 565673.083 5932791.111 565672.699 5932786.929 565650.945 5932789.863 565650.871 5932788.031 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="5">565650.871 5932788.031 565645.705 5932788.635 565640.534 5932789.193 565635.887 5932761.838 565626.281 5932735.806 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="10">565626.281 5932735.806 565630.930 5932733.988 565629.719 5932730.882 565683.901 5932709.763 565685.116 5932712.880 565697.832 5932707.896 565695.334 5932717.476 565681.892 5932769.047 565752.633 5932766.202 565753.168 5932779.557 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">565671.581 5932753.124 565666.805 5932720.017 565638.380 5932731.075 565648.731 5932757.682 565671.581 5932753.124 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_82078959-8a74-46cb-9290-f3eeba601bfa">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565902.529 5932741.171</gml:lowerCorner>
          <gml:upperCorner>565923.506 5932760.557</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_587bb160-79c8-4f40-85d4-aefffe5595b5">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">565904.323 5932741.171 565923.506 5932746.761 565921.751 5932760.557 565902.529 5932755.263 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:bautiefe uom="m">17</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_87df14da-fab8-4964-b5ed-319303280a69">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565883.744 5932717.864</gml:lowerCorner>
          <gml:upperCorner>565942.202 5932782.154</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_e877fd09-dd1b-4aaa-bdfd-04574d855a99">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">565900.360 5932772.299 565936.141 5932782.154 565942.202 5932734.503 565885.113 5932717.864 565883.744 5932728.618 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:bautiefe uom="m">17</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_17009554-c8b4-4b5d-8592-4d15b5aa78b6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565809.788 5932658.580</gml:lowerCorner>
          <gml:upperCorner>565859.366 5932730.058</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_7f2d909b-23ac-42b1-b006-c16c533f6580">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="7">565812.416 5932714.429 565839.048 5932730.058 565859.366 5932658.580 565812.434 5932676.931 565814.587 5932682.436 565809.788 5932689.737 565812.416 5932714.429 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_9056efbc-a820-4e0a-ab68-f710974bede8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565887.287 5932649.333</gml:lowerCorner>
          <gml:upperCorner>565948.277 5932717.429</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(I)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_551efb2f-83bb-4f95-8b77-1c7a119e0216"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_a2100ab4-cb15-434a-958e-c3ff29115600"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_6fff69c9-5ec3-43c2-8fae-0f8452ef8ad5">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">565905.985 5932688.533 565930.020 5932695.538 565931.757 5932681.887 565948.277 5932686.737 565944.373 5932717.429 565887.287 5932700.791 565893.645 5932650.856 565935.897 5932649.333 565933.724 5932666.423 565908.685 5932667.325 565905.985 5932688.533 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ee89b16f-c94d-4f6b-9ebb-29a5cc18e56c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565781.174 5932731.821</gml:lowerCorner>
          <gml:upperCorner>565813.922 5932763.681</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">65.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_143c1528-af32-46bb-82f4-142ba9b7d46c">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="54">565796.981 5932731.821 565797.410 5932731.876 565797.825 5932732.006 565798.211 5932732.205 565810.249 5932741.152 565810.376 5932741.261 565810.481 5932741.391 565810.563 5932741.537 565811.194 5932742.871 565813.310 5932747.348 565813.417 5932747.636 565813.864 5932749.419 565813.919 5932749.722 565813.922 5932750.030 565813.871 5932750.333 565812.618 5932754.119 565810.200 5932761.425 565809.968 5932761.945 565809.648 5932762.417 565809.252 5932762.827 565808.791 5932763.161 565804.968 5932763.329 565800.216 5932763.540 565784.571 5932763.681 565784.467 5932763.630 565784.159 5932763.441 565783.868 5932763.202 565783.505 5932762.855 565783.364 5932762.713 565783.266 5932762.601 565782.974 5932762.156 565782.647 5932761.458 565782.487 5932760.788 565781.461 5932750.180 565781.188 5932747.270 565781.174 5932746.865 565781.205 5932746.596 565781.283 5932746.334 565781.324 5932746.253 565781.731 5932745.488 565781.865 5932745.279 565783.789 5932743.315 565784.649 5932742.438 565784.328 5932735.475 565784.301 5932734.644 565784.319 5932734.282 565784.430 5932733.778 565784.598 5932733.370 565784.751 5932733.130 565785.148 5932732.695 565785.461 5932732.454 565785.695 5932732.332 565787.166 5932732.264 565796.981 5932731.821 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zmin>14</xplan:Zmin>
      <xplan:Zmax>16</xplan:Zmax>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b2feec06-3c69-408b-9226-4999a917ab0a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565591.675 5932735.806</gml:lowerCorner>
          <gml:upperCorner>565640.534 5932792.465</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_0e2675f8-a08f-4f86-9eef-3ff8eb76b63a">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="5">565591.675 5932792.465 565616.135 5932791.291 565640.534 5932789.193 565635.887 5932761.838 565626.281 5932735.806 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565626.281 5932735.806 565613.110 5932740.957 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_e4b1d827-0f7e-4b81-817d-21d982e6b206">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565905.985 5932666.423</gml:lowerCorner>
          <gml:upperCorner>565933.724 5932695.538</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_10f3594b-03b2-4857-a00a-3ebe25853485">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">565931.757 5932681.887 565930.020 5932695.538 565905.985 5932688.533 565908.685 5932667.325 565933.724 5932666.423 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:bautiefe uom="m">17</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_34535da5-584f-4a53-b899-9c61a97a2181">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565752.638 5932758.412</gml:lowerCorner>
          <gml:upperCorner>565878.934 5932783.759</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_22df592a-a444-4997-9d69-9e8edff2dbfb">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="18">565846.639 5932782.612 565843.114 5932778.786 565787.821 5932781.269 565784.722 5932781.409 565753.337 5932783.759 565752.638 5932766.314 565752.746 5932766.197 565759.391 5932759.020 565760.086 5932758.992 565774.511 5932758.412 565773.687 5932764.887 565777.624 5932764.710 565785.459 5932764.358 565804.671 5932763.495 565813.807 5932763.085 565849.328 5932761.490 565859.464 5932761.035 565878.934 5932766.397 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_7be06cd5-9d19-4c48-a414-1c8a312b2561">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565695.334 5932691.320</gml:lowerCorner>
          <gml:upperCorner>565741.582 5932721.512</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(B, D, I, M)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_ed6831a9-45a2-48a3-81c4-1c40ac213417"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_6921793d-c644-49dc-a6e3-166992db081f"/>
      <xplan:refTextInhalt xlink:href="#GML_6aa02183-2b23-40a0-88b4-a8dc4b8ac248"/>
      <xplan:refTextInhalt xlink:href="#GML_2c404c4d-ea5a-4a1e-9a33-44b0216db2ee"/>
      <xplan:refTextInhalt xlink:href="#GML_a2100ab4-cb15-434a-958e-c3ff29115600"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_434ccdcb-9398-4f00-ab97-0467579d5a26">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="8">565741.582 5932707.933 565719.064 5932716.759 565711.259 5932719.818 565710.817 5932721.512 565695.334 5932717.476 565697.832 5932707.896 565740.121 5932691.320 565741.582 5932707.933 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_5ed3ecf3-fc36-453d-ad0d-47598c5c7f9d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565777.597 5932732.699</gml:lowerCorner>
          <gml:upperCorner>565812.983 5932767.581</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_6972a7ff-26bb-4b6d-8be1-f7cc4e5a86c8">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="21">565804.968 5932763.329 565808.791 5932763.161 565810.845 5932763.069 565811.223 5932763.019 565811.597 5932762.901 565811.945 5932762.720 565812.256 5932762.481 565812.521 5932762.191 565812.731 5932761.860 565812.881 5932761.498 565812.966 5932761.115 565812.983 5932760.733 565812.981 5932760.699 565812.798 5932757.378 565812.618 5932754.119 565812.503 5932752.038 565810.449 5932743.609 565810.306 5932743.417 565810.164 5932743.283 565810.009 5932743.150 565798.399 5932733.210 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">565798.399 5932733.210 565797.726 5932732.829 565796.964 5932732.699 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565796.964 5932732.699 565785.421 5932733.218 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">565785.421 5932733.218 565784.600 5932734.214 565784.328 5932735.475 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="8">565784.328 5932735.475 565784.370 5932736.386 565784.748 5932744.595 565784.217 5932745.516 565781.750 5932749.795 565781.461 5932750.180 565778.415 5932754.252 565778.265 5932754.484 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="7">565778.265 5932754.484 565778.213 5932754.607 565778.165 5932754.732 565778.129 5932754.886 565778.098 5932755.041 565778.086 5932755.163 565778.077 5932755.286 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="13">565778.077 5932755.286 565777.625 5932764.686 565777.597 5932765.269 565777.899 5932765.833 565778.278 5932766.316 565778.616 5932766.619 565796.764 5932767.581 565797.082 5932767.550 565797.392 5932767.463 565798.329 5932767.022 565798.588 5932766.898 565801.581 5932765.223 565804.968 5932763.329 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_96b54c5a-bd3b-4a36-b719-2b6d986ed052">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565638.380 5932720.017</gml:lowerCorner>
          <gml:upperCorner>565671.581 5932757.682</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_39484140-e5a1-448e-aaba-4e905abfd378"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_6bcf5293-6353-447e-97e3-47f8250f81da">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">565666.805 5932720.017 565671.581 5932753.124 565648.731 5932757.682 565638.380 5932731.075 565666.805 5932720.017 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_b01e6178-59e6-4b0c-93ae-448838d0053c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566004.281 5932723.570</gml:lowerCorner>
          <gml:upperCorner>566029.946 5932763.378</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(M)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">0.0</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_c1228ef7-dade-4d07-af39-84b869e57781">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">566020.809 5932761.972 566004.281 5932757.192 566006.388 5932740.644 566008.562 5932723.570 566029.946 5932729.803 566025.671 5932763.378 566020.809 5932761.972 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="GML_0b0e7af4-8435-4711-843b-9259559be42e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565466.357 5932633.546</gml:lowerCorner>
          <gml:upperCorner>565940.829 5932804.104</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>FUSSGÄNGER- UND RADFAHRERBEREICH</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_f998ec7e-4496-452c-acef-fec212cd39c9"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_e0c24079-67a8-4ec3-925a-d14962367962"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_30ca1209-45fc-4a5e-a5df-c40f9cdd392f"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ee22e0f7-7a49-45c8-84cd-5e3517a507f2">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_2e84aef8-0d91-4f0c-9504-8ec5ee12c320">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="20">565785.061 5932789.623 565771.225 5932790.194 565750.221 5932791.062 565725.312 5932792.551 565692.462 5932794.402 565667.474 5932795.494 565643.556 5932796.362 565604.863 5932797.583 565561.744 5932799.532 565544.463 5932800.651 565518.984 5932801.683 565490.864 5932802.354 565490.872 5932803.669 565469.410 5932804.104 565469.269 5932803.990 565466.357 5932783.298 565467.303 5932781.812 565619.348 5932722.606 565619.364 5932722.462 565824.500 5932642.091 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">565824.500 5932642.091 565829.381 5932640.364 565834.348 5932638.903 565838.824 5932637.762 565843.378 5932636.991 565848.420 5932636.431 565853.483 5932636.125 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="30">565853.483 5932636.125 565895.671 5932634.930 565940.829 5932633.546 565939.582 5932647.538 565939.560 5932649.201 565935.897 5932649.333 565893.645 5932650.856 565887.287 5932700.791 565885.978 5932711.065 565885.113 5932717.864 565883.744 5932728.618 565878.934 5932766.397 565859.464 5932761.035 565866.147 5932708.540 565873.525 5932650.585 565873.373 5932650.580 565846.606 5932649.634 565794.479 5932670.021 565785.309 5932673.608 565776.875 5932676.914 565748.052 5932688.212 565740.121 5932691.320 565697.832 5932707.896 565685.116 5932712.880 565683.901 5932709.763 565629.719 5932730.882 565630.930 5932733.988 565626.281 5932735.806 565613.110 5932740.957 565611.268 5932741.678 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">565611.268 5932741.678 565595.709 5932747.978 565580.306 5932754.650 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="4">565580.306 5932754.650 565541.828 5932771.726 565551.019 5932792.437 565556.667 5932792.535 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">565556.667 5932792.535 565574.171 5932792.737 565591.675 5932792.465 565616.135 5932791.291 565640.534 5932789.193 565645.705 5932788.635 565650.871 5932788.031 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="7">565650.871 5932788.031 565650.945 5932789.863 565672.699 5932786.929 565673.083 5932791.111 565753.337 5932783.759 565784.722 5932781.409 565785.061 5932789.623 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:zweckbestimmung>1300</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_69ee8dfa-8102-4291-9747-196eb9339388">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565777.070 5932719.816</gml:lowerCorner>
          <gml:upperCorner>565777.070 5932719.816</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">9.2</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_e89f4214-35c1-41dd-96ea-6c478865cc57">
          <gml:pos>565777.070 5932719.816</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_632a2120-60b6-47b1-8107-3e79292cbae7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565615.332 5932785.208</gml:lowerCorner>
          <gml:upperCorner>565640.534 5932791.324</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Arkade</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">3.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_7b7bb31a-3a7f-4478-bb33-2f217fad27e0">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_71fe44d5-d5e1-4979-8f0d-828e254d5607">
                  <gml:segments>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">565615.332 5932787.335 565627.765 5932786.387 565640.179 5932785.208 565640.370 5932787.199 565640.534 5932789.193 565628.091 5932790.379 565615.627 5932791.324 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">565615.627 5932791.324 565615.332 5932787.335 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_c1ae22b6-c053-4628-9b18-2726992774d4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565964.119 5932659.284</gml:lowerCorner>
          <gml:upperCorner>565964.119 5932659.284</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">9.3</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_3805ff47-8a9b-464e-a0a6-f9573a51bcd5">
          <gml:pos>565964.119 5932659.284</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_b185f595-61cc-484e-9d44-d0174015a250">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565626.281 5932735.806</gml:lowerCorner>
          <gml:upperCorner>565640.634 5932789.193</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_a221c559-8d0f-4472-8c3c-89756ca7a2a3">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">565626.281 5932735.806 565635.887 5932761.838 565640.534 5932789.193 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565640.534 5932789.193 565640.634 5932789.182 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_5f9737ad-11f4-43f9-9c83-1ba4d3e68cff">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565885.113 5932700.791</gml:lowerCorner>
          <gml:upperCorner>565944.373 5932734.503</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(G)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_b10ac8ff-a802-4e14-8756-f9dda612989d"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e06f8260-990e-4f04-913b-1f54d14705c4"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_2238f65c-04d0-40b6-9901-17cc5442f3f5">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">565942.202 5932734.503 565885.113 5932717.864 565885.978 5932711.065 565887.287 5932700.791 565944.373 5932717.429 565942.202 5932734.503 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_69208cea-ba5b-4e2a-b0b8-c0b0361330a0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565740.121 5932688.212</gml:lowerCorner>
          <gml:upperCorner>565748.052 5932691.320</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_4d1eaf21-518a-40d5-a351-43d831e5823e">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565748.052 5932688.212 565740.121 5932691.320 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_b7402418-df72-4f5f-bb77-e2252de42228">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565731.912 5932787.761</gml:lowerCorner>
          <gml:upperCorner>565731.912 5932787.761</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.4</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_aeeaca7a-5d2f-4f0a-827d-ba6e5656dd82">
          <gml:pos>565731.912 5932787.761</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_07e76da2-4540-4458-9c21-855b61a7d5de">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565782.836 5932734.503</gml:lowerCorner>
          <gml:upperCorner>565813.870 5932765.762</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_0b6344ea-6142-4b82-977d-8483a9bb1578">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="42">565812.798 5932757.378 565811.978 5932752.743 565811.878 5932752.439 565811.726 5932752.156 565811.528 5932751.904 565805.725 5932744.779 565797.950 5932735.234 565797.706 5932734.985 565797.423 5932734.782 565797.109 5932734.631 565796.773 5932734.537 565796.426 5932734.503 565796.078 5932734.530 565784.370 5932736.386 565783.545 5932736.517 565783.310 5932736.773 565783.101 5932737.101 565782.965 5932737.430 565782.858 5932737.914 565782.836 5932738.412 565783.789 5932743.315 565784.217 5932745.516 565785.344 5932751.307 565783.337 5932762.525 565783.412 5932763.054 565783.546 5932763.443 565783.737 5932763.787 565784.053 5932764.177 565784.183 5932764.290 565801.581 5932765.223 565811.391 5932765.750 565811.745 5932765.762 565812.130 5932765.712 565812.500 5932765.595 565812.844 5932765.415 565813.151 5932765.179 565813.413 5932764.893 565813.621 5932764.566 565813.770 5932764.207 565813.853 5932763.828 565813.870 5932763.441 565812.798 5932757.378 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_b2f19715-2b4d-4dca-80bc-391e844d3b26">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565953.137 5932688.153</gml:lowerCorner>
          <gml:upperCorner>565980.840 5932696.225</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_45c19f00-1d7a-4167-bd7d-e06b146a7c12">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565953.137 5932688.153 565980.840 5932696.225 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c80d2161-fdd5-416e-bedd-03ae39305310">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565787.877 5932781.135</gml:lowerCorner>
          <gml:upperCorner>565819.555 5932797.619</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>Besondere Nutzung</xplan:gliederung1>
      <xplan:gliederung2>Servicegebäude Marina</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">17</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b7cdbff1-56cf-4d18-a5e7-3cfc328b2859">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">565819.555 5932796.919 565816.740 5932797.046 565803.995 5932797.619 565788.102 5932787.522 565787.877 5932782.527 565818.846 5932781.135 565819.555 5932796.919 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Z>2</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_06ee1e4c-14a2-4710-97b8-a57f9ce09058">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565695.334 5932717.476</gml:lowerCorner>
          <gml:upperCorner>565710.817 5932721.512</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_f41b4203-c3ef-4a24-a2e5-8a2bc8149c64">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565710.817 5932721.512 565695.334 5932717.476 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_82185115-542d-40cd-b755-c39e54ce7c1a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565760.086 5932676.914</gml:lowerCorner>
          <gml:upperCorner>565776.875 5932758.992</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_ab9d2ac8-1f4f-4f57-b54f-24b493c377c2"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_de9d1794-3105-41d0-a054-1a8dce1fc077">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">565776.875 5932676.914 565772.580 5932714.635 565765.753 5932714.446 565764.954 5932720.724 565760.086 5932758.992 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_0855a411-8b3d-40bb-94c8-0f3f135e9c1b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566004.281 5932723.570</gml:lowerCorner>
          <gml:upperCorner>566029.946 5932763.378</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(M)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_4476acfb-1b28-4331-86c2-35777ed50404"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_2c404c4d-ea5a-4a1e-9a33-44b0216db2ee"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_55b975bc-96a9-428d-b1f0-d9bcd033e892">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">566025.671 5932763.378 566020.809 5932761.972 566004.281 5932757.192 566006.388 5932740.644 566008.562 5932723.570 566029.946 5932729.803 566025.671 5932763.378 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_3616e3dd-c0df-416a-97aa-56660f5b223f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565794.479 5932649.634</gml:lowerCorner>
          <gml:upperCorner>565873.373 5932760.406</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(L)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_2c25e0f3-e0ff-48e5-ad01-3451f7876e91"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_50360564-32c3-4aec-9737-b61c5161debd"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_782a1728-56e7-41b2-a044-ebdb4dfbb078">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="12">565853.006 5932730.773 565860.740 5932737.162 565859.425 5932760.406 565825.029 5932751.011 565822.829 5932746.813 565813.751 5932729.494 565800.793 5932730.754 565797.868 5932702.583 565794.479 5932670.021 565846.606 5932649.634 565873.373 5932650.580 565853.006 5932730.773 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">565809.788 5932689.737 565812.416 5932714.429 565839.048 5932730.058 565859.366 5932658.580 565812.434 5932676.931 565814.587 5932682.436 565809.788 5932689.737 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_ddc42fdd-602e-4227-9f04-921b8f4409bc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565875.090 5932784.163</gml:lowerCorner>
          <gml:upperCorner>566018.409 5932820.376</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_fc7d82d3-0a9a-4157-8a87-d964e312656e">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">566018.409 5932820.376 565969.692 5932806.959 565886.926 5932784.163 565886.286 5932786.487 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="7">565886.286 5932786.487 565884.256 5932786.075 565882.188 5932785.946 565879.566 5932786.691 565877.704 5932788.681 565876.088 5932792.531 565875.090 5932796.586 </gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ec7c1d59-e49f-49a6-881c-1178845dbcc1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565781.174 5932731.821</gml:lowerCorner>
          <gml:upperCorner>565813.922 5932763.681</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_fa153717-528b-4cab-9873-c5d9d4b3445f">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="54">565787.166 5932732.264 565785.695 5932732.332 565785.461 5932732.454 565785.148 5932732.695 565784.751 5932733.130 565784.598 5932733.370 565784.430 5932733.778 565784.319 5932734.282 565784.301 5932734.644 565784.328 5932735.475 565784.649 5932742.438 565783.789 5932743.315 565781.865 5932745.279 565781.731 5932745.488 565781.324 5932746.253 565781.283 5932746.334 565781.205 5932746.596 565781.174 5932746.865 565781.188 5932747.270 565781.461 5932750.180 565782.487 5932760.788 565782.647 5932761.458 565782.974 5932762.156 565783.266 5932762.601 565783.364 5932762.713 565783.505 5932762.855 565783.868 5932763.202 565784.159 5932763.441 565784.467 5932763.630 565784.571 5932763.681 565800.216 5932763.540 565804.968 5932763.329 565808.791 5932763.161 565809.252 5932762.827 565809.648 5932762.417 565809.968 5932761.945 565810.200 5932761.425 565812.618 5932754.119 565813.871 5932750.333 565813.922 5932750.030 565813.919 5932749.722 565813.864 5932749.419 565813.417 5932747.636 565813.310 5932747.348 565811.194 5932742.871 565810.563 5932741.537 565810.481 5932741.391 565810.376 5932741.261 565810.249 5932741.152 565798.211 5932732.205 565797.825 5932732.006 565797.410 5932731.876 565796.981 5932731.821 565787.166 5932732.264 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_50e5c61a-f9e1-4099-b8cd-0611b6b61695">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565878.934 5932728.618</gml:lowerCorner>
          <gml:upperCorner>565905.170 5932772.299</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_780bdc56-0d32-4c08-83e6-b112350df6e2">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="7">565883.744 5932728.618 565878.934 5932766.397 565900.360 5932772.299 565902.529 5932755.263 565904.323 5932741.171 565905.170 5932734.519 565883.744 5932728.618 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:bautiefe uom="m">22</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_6a67c2b6-fd48-41e8-a2dd-5f7828a4585b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565781.174 5932743.315</gml:lowerCorner>
          <gml:upperCorner>565784.217 5932750.180</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Auskragung</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2000</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">43.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_3511cdbc-71b1-4562-8302-aa0f7429486c">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="12">565781.750 5932749.795 565781.461 5932750.180 565781.188 5932747.270 565781.174 5932746.865 565781.205 5932746.596 565781.283 5932746.334 565781.324 5932746.253 565781.731 5932745.488 565781.865 5932745.279 565783.789 5932743.315 565784.217 5932745.516 565781.750 5932749.795 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a7030dc5-ac76-4f12-be82-0b7662618159">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565997.337 5932665.018</gml:lowerCorner>
          <gml:upperCorner>566022.975 5932691.915</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(A, E, H)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_a135caf0-72ff-4ac7-82cd-abbdc9455c25"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_172a8b61-8775-4dfc-8606-e7fd39417942">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">566019.653 5932691.915 565997.337 5932685.410 565999.930 5932665.018 566022.975 5932665.833 566019.653 5932691.915 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Zzwingend>1</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_96425556-8eff-429f-9db5-ced4ae60516f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565953.137 5932648.298</gml:lowerCorner>
          <gml:upperCorner>565958.118 5932688.153</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_82c0e20c-f4eb-476e-bec2-04fd6d79bb0d"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_4b4bca23-58f1-4d76-b195-a6c71d741b3c">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565958.118 5932648.298 565953.137 5932688.153 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_d1a0ee37-41b8-4c7c-b2ff-52a53d948e04">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565556.643 5932754.650</gml:lowerCorner>
          <gml:upperCorner>565580.306 5932792.535</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_3bcf7e18-b3bc-4472-a42c-4c32d6cfd94d">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="5">565556.667 5932792.535 565558.831 5932778.114 565566.021 5932765.427 565572.553 5932759.229 565580.306 5932754.650 </gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_fb8ec91b-8bb3-492d-afd1-9d8323e9d38c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565886.529 5932784.157</gml:lowerCorner>
          <gml:upperCorner>566018.410 5932821.874</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>PARKANLAGE</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_89c273d6-7f81-4b54-a29d-aa469ed8d6df"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_8676c057-52ed-484e-abdd-7f633473dc3f"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_30ca1209-45fc-4a5e-a5df-c40f9cdd392f"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_402a2633-8272-4ca2-bcbe-619710d56943">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">566018.219 5932821.874 565886.529 5932785.604 565886.928 5932784.157 566018.410 5932820.371 566018.219 5932821.874 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:zugunstenVon>( FREIE UND HANSESTADT HAMBURG )</xplan:zugunstenVon>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_8bd2275f-7926-4f49-8a4e-55ae6a0f1037">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565547.855 5932777.103</gml:lowerCorner>
          <gml:upperCorner>565547.855 5932777.103</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">5.2</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_0b9ec614-be4a-4f1c-bf1f-7fb43629b1d5">
          <gml:pos>565547.855 5932777.103</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_1971f8c0-ca44-4fbe-8081-2b5d48521d35">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566015.457 5932761.972</gml:lowerCorner>
          <gml:upperCorner>566025.671 5932803.999</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_4cdf9d17-eccd-4d8a-aeaf-6ab702f69cd3"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_8a5a7d14-145e-4321-850e-6be472993764">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="3">566025.671 5932763.378 566020.809 5932761.972 566015.457 5932803.999 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d728f5c8-f2e2-48aa-8d95-0e48ce88e6a8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565581.696 5932754.786</gml:lowerCorner>
          <gml:upperCorner>565624.561 5932777.635</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(H, M)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_4a1f49fc-d304-479d-aa04-5bff6e28ca34"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_860bf5ce-0f65-48e4-9532-7613a6650aa7">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_8960990e-68d3-402e-a986-d8af284308b0">
                  <gml:segments>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="11">565624.561 5932775.618 565614.889 5932776.336 565605.207 5932776.906 565594.609 5932777.359 565584.004 5932777.635 565583.271 5932774.229 565581.696 5932771.122 565583.988 5932769.601 565586.433 5932768.342 565601.488 5932761.821 565616.694 5932755.662 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">565616.694 5932755.662 565618.933 5932754.786 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">565618.933 5932754.786 565622.161 5932765.090 565624.561 5932775.618 </gml:posList>
                    </gml:ArcString>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Zzwingend>1</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_7a3a837b-cfee-4c4c-86b5-ba172fac1794">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565905.985 5932666.423</gml:lowerCorner>
          <gml:upperCorner>565933.724 5932695.538</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(H)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_230d0675-209b-439d-b2f5-9b7216ab7c9b"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_a2100ab4-cb15-434a-958e-c3ff29115600"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b5cf2fa4-98e6-4df7-b208-d45cd44e1e24">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">565931.757 5932681.887 565930.020 5932695.538 565905.985 5932688.533 565908.685 5932667.325 565933.724 5932666.423 565931.757 5932681.887 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_f958a30d-b144-4701-9567-b57e96729735">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565556.643 5932735.806</gml:lowerCorner>
          <gml:upperCorner>565640.534 5932792.738</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(D, F, I, M)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_e4100735-5422-4ec9-b598-3577a4179daa"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_780db5ba-9a30-4f5c-8b8c-dc2140285545"/>
      <xplan:refTextInhalt xlink:href="#GML_6aa02183-2b23-40a0-88b4-a8dc4b8ac248"/>
      <xplan:refTextInhalt xlink:href="#GML_2c404c4d-ea5a-4a1e-9a33-44b0216db2ee"/>
      <xplan:refTextInhalt xlink:href="#GML_a2100ab4-cb15-434a-958e-c3ff29115600"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_519c7f71-95cf-4bc2-aa1e-36e0c0635066">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_95fdcfca-0e4e-4c02-93b2-4ff16d8ee7f3">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">565613.110 5932740.957 565626.281 5932735.806 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="11">565626.281 5932735.806 565635.887 5932761.838 565640.534 5932789.193 565616.135 5932791.291 565591.675 5932792.465 565574.171 5932792.737 565556.667 5932792.535 565558.831 5932778.114 565566.021 5932765.427 565572.553 5932759.229 565580.306 5932754.650 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="3">565580.306 5932754.650 565611.268 5932741.678 565613.110 5932740.957 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
          <gml:interior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_90eb9aed-4111-4ad5-9a52-14973345f9a5">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">565618.933 5932754.786 565616.694 5932755.662 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="13">565616.694 5932755.662 565601.488 5932761.821 565586.433 5932768.342 565583.988 5932769.601 565581.696 5932771.122 565583.271 5932774.229 565584.004 5932777.635 565594.609 5932777.359 565605.207 5932776.906 565614.889 5932776.336 565624.561 5932775.618 565622.161 5932765.090 565618.933 5932754.786 </gml:posList>
                    </gml:ArcString>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_b0ea0abf-cc1e-415e-86ca-a5fdf28e75c5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565902.529 5932741.175</gml:lowerCorner>
          <gml:upperCorner>565923.506 5932760.557</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(H)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_ca7ab734-78e3-4b47-839c-cedf3bbc7bcf"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_a2100ab4-cb15-434a-958e-c3ff29115600"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_fa831c15-b6b7-4051-babf-a3d898efe48b">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">565921.751 5932760.557 565902.529 5932755.263 565904.337 5932741.175 565923.506 5932746.761 565921.751 5932760.557 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_6d5caa25-9659-4499-b41a-d2621f6f0eef">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565701.248 5932716.759</gml:lowerCorner>
          <gml:upperCorner>565749.389 5932754.257</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(H, M)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_e23b69d9-b633-4b1b-b5c0-bd27b6b35d36"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_2c404c4d-ea5a-4a1e-9a33-44b0216db2ee"/>
      <xplan:refTextInhalt xlink:href="#GML_a2100ab4-cb15-434a-958e-c3ff29115600"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_0cb0abaf-60ee-4c75-acfa-a792b99cf514">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">565745.796 5932752.466 565701.248 5932754.257 565704.856 5932740.415 565705.824 5932740.667 565710.817 5932721.512 565711.259 5932719.818 565719.064 5932716.759 565720.550 5932720.549 565749.389 5932724.218 565745.796 5932752.466 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_e48d8bf8-b0d1-47a1-9932-db6c432c3f8f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565784.301 5932731.821</gml:lowerCorner>
          <gml:upperCorner>565813.922 5932754.119</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2000</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">43.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_35435261-a3d4-4ccb-bc4d-619749ae9c2d">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="44">565810.249 5932741.152 565810.376 5932741.261 565810.481 5932741.391 565810.563 5932741.537 565811.194 5932742.871 565813.310 5932747.348 565813.417 5932747.636 565813.864 5932749.419 565813.919 5932749.722 565813.922 5932750.030 565813.871 5932750.333 565812.618 5932754.119 565812.503 5932752.038 565810.449 5932743.609 565810.306 5932743.417 565810.164 5932743.283 565810.009 5932743.150 565798.399 5932733.210 565798.083 5932732.992 565797.726 5932732.828 565797.347 5932732.729 565796.964 5932732.699 565796.872 5932732.701 565785.421 5932733.218 565785.148 5932733.428 565784.914 5932733.684 565784.626 5932734.138 565784.469 5932734.491 565784.348 5932735.008 565784.328 5932735.475 565784.301 5932734.644 565784.319 5932734.282 565784.430 5932733.778 565784.598 5932733.370 565784.751 5932733.130 565785.148 5932732.695 565785.461 5932732.454 565785.695 5932732.332 565787.166 5932732.264 565796.981 5932731.821 565797.410 5932731.876 565797.825 5932732.006 565798.211 5932732.205 565810.249 5932741.152 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_bc39d635-6817-43f6-893c-e12724490016">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565752.638 5932686.737</gml:lowerCorner>
          <gml:upperCorner>566032.314 5932820.371</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_30ca1209-45fc-4a5e-a5df-c40f9cdd392f"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b3b56184-32aa-4461-b36b-471d0f7da610">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_ec9bfb02-1de2-417a-89df-4a4d0b78a021">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="4">566018.410 5932820.371 565886.928 5932784.157 565886.529 5932785.604 565886.286 5932786.487 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">565886.286 5932786.487 565884.260 5932786.076 565882.196 5932785.945 565879.536 5932786.709 565877.668 5932788.751 565876.076 5932792.569 565875.090 5932796.586 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="40">565875.090 5932796.586 565877.167 5932780.277 565848.013 5932771.824 565846.639 5932782.612 565843.114 5932778.786 565787.821 5932781.269 565784.722 5932781.409 565753.337 5932783.759 565753.168 5932779.557 565752.638 5932766.314 565752.746 5932766.197 565759.391 5932759.020 565760.086 5932758.992 565774.511 5932758.412 565773.687 5932764.887 565777.872 5932764.699 565783.066 5932764.466 565785.459 5932764.358 565804.114 5932763.520 565813.807 5932763.085 565849.328 5932761.490 565859.464 5932761.035 565878.934 5932766.397 565900.360 5932772.299 565936.141 5932782.154 565942.202 5932734.503 565944.373 5932717.429 565948.277 5932686.737 565953.137 5932688.153 565980.840 5932696.225 566032.314 5932711.230 566029.946 5932729.803 566008.562 5932723.570 565963.890 5932710.550 565954.152 5932787.114 566015.457 5932803.999 566020.325 5932805.339 566018.691 5932818.163 566018.685 5932818.208 566018.410 5932820.371 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_9b442d6c-7876-438a-a903-e45032bd193a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565681.892 5932707.896</gml:lowerCorner>
          <gml:upperCorner>565752.746 5932769.047</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_c823b33f-d895-47a3-abf6-f050d3997d7b">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="3">565752.746 5932766.197 565681.892 5932769.047 565697.832 5932707.896 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_1df34f6c-7eac-4c7b-a47a-f9d8d028099f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565556.643 5932740.957</gml:lowerCorner>
          <gml:upperCorner>565618.933 5932792.738</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(D, F, I, M)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">41</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_0b71b288-182b-440f-b9dc-3f104e0b4add"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_bc65c102-ac8d-4045-926d-9e3c64c7cf7c"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_6b68f83a-d4db-4eae-89d6-494adb394463">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_e11eac68-f1e6-4659-b5e6-881e5022a926">
                  <gml:segments>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">565584.004 5932777.635 565594.609 5932777.359 565605.207 5932776.906 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="4">565605.207 5932776.906 565605.531 5932784.318 565591.456 5932784.934 565591.675 5932792.465 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="9">565591.675 5932792.465 565574.171 5932792.737 565556.667 5932792.535 565558.831 5932778.114 565566.021 5932765.427 565572.553 5932759.229 565580.306 5932754.650 565595.709 5932747.978 565611.268 5932741.678 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">565611.268 5932741.678 565613.110 5932740.957 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">565613.110 5932740.957 565616.213 5932747.791 565618.933 5932754.786 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">565618.933 5932754.786 565616.694 5932755.662 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">565616.694 5932755.662 565601.488 5932761.821 565586.433 5932768.342 565583.988 5932769.601 565581.696 5932771.122 565583.271 5932774.229 565584.004 5932777.635 </gml:posList>
                    </gml:ArcString>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Z>9</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_41f6b2c8-5bfb-46b3-8d00-4736a3ecd98c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565760.086 5932650.580</gml:lowerCorner>
          <gml:upperCorner>565873.525 5932764.887</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(G)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_181e563a-bc73-4e7a-b5d4-fc177f151dcb"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e06f8260-990e-4f04-913b-1f54d14705c4"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ca1d6469-cbe5-46d8-977b-7eedf1c00680">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="74">565800.793 5932730.754 565813.751 5932729.494 565822.829 5932746.813 565825.029 5932751.011 565859.425 5932760.406 565860.740 5932737.162 565853.006 5932730.773 565873.373 5932650.580 565873.525 5932650.585 565866.147 5932708.540 565859.464 5932761.035 565849.328 5932761.490 565813.807 5932763.085 565812.798 5932757.378 565812.682 5932755.286 565812.618 5932754.119 565813.871 5932750.333 565813.922 5932750.030 565813.919 5932749.722 565813.864 5932749.419 565813.417 5932747.636 565813.310 5932747.348 565811.194 5932742.871 565810.511 5932741.427 565810.376 5932741.261 565810.249 5932741.152 565798.211 5932732.205 565797.825 5932732.006 565797.410 5932731.876 565796.981 5932731.821 565787.166 5932732.264 565785.695 5932732.332 565785.461 5932732.454 565785.148 5932732.695 565784.751 5932733.130 565784.598 5932733.370 565784.430 5932733.778 565784.319 5932734.282 565784.301 5932734.644 565784.328 5932735.475 565784.370 5932736.386 565783.545 5932736.517 565783.310 5932736.773 565783.101 5932737.101 565782.965 5932737.430 565782.930 5932737.586 565782.858 5932737.914 565782.836 5932738.412 565783.790 5932743.315 565781.865 5932745.279 565781.731 5932745.488 565781.283 5932746.334 565781.205 5932746.596 565781.174 5932746.865 565781.188 5932747.270 565781.461 5932750.180 565778.415 5932754.252 565778.265 5932754.484 565778.165 5932754.732 565778.098 5932755.041 565778.077 5932755.286 565777.625 5932764.686 565777.624 5932764.710 565773.687 5932764.887 565774.511 5932758.412 565760.086 5932758.992 565764.954 5932720.724 565765.753 5932714.446 565772.580 5932714.635 565776.875 5932676.914 565785.309 5932673.608 565794.479 5932670.021 565797.868 5932702.583 565800.793 5932730.754 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_852e66f0-b988-40b2-bebe-3f9c07e6f7e7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565777.624 5932745.516</gml:lowerCorner>
          <gml:upperCorner>565785.344 5932764.710</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Auskragung</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2000</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">17</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_49643f84-0cc3-40f6-942d-6476da3676e5">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="20">565783.337 5932762.525 565783.412 5932763.054 565783.546 5932763.443 565783.737 5932763.787 565784.053 5932764.177 565784.183 5932764.290 565783.066 5932764.466 565777.872 5932764.699 565777.624 5932764.710 565777.625 5932764.686 565778.077 5932755.286 565778.098 5932755.041 565778.165 5932754.732 565778.265 5932754.484 565778.415 5932754.252 565781.461 5932750.180 565781.750 5932749.795 565784.217 5932745.516 565785.344 5932751.307 565783.337 5932762.525 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a3ac24b6-73a1-42c3-9cea-6b94fa3744b8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565954.152 5932710.550</gml:lowerCorner>
          <gml:upperCorner>566020.809 5932803.999</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(I, M)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_777152b5-d0b6-4da5-81bd-fde94f5716a0"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_d541f15d-f012-40d2-b758-9d7d934d30f1"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_03c70f77-df96-40df-91e1-fb5ad85f3696">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">566001.070 5932782.403 566004.281 5932757.192 566020.809 5932761.972 566015.457 5932803.999 565954.152 5932787.114 565963.890 5932710.550 566008.562 5932723.570 566006.388 5932740.644 565978.243 5932732.441 565972.876 5932774.638 566001.070 5932782.403 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_b83adf42-cb7d-4fc9-8fc5-ac15e82ee985">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565681.892 5932717.476</gml:lowerCorner>
          <gml:upperCorner>565764.954 5932769.047</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(D, I, M)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_e7f6dbbf-73c0-41cb-a6fe-5087f03b261b"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_6aa02183-2b23-40a0-88b4-a8dc4b8ac248"/>
      <xplan:refTextInhalt xlink:href="#GML_2c404c4d-ea5a-4a1e-9a33-44b0216db2ee"/>
      <xplan:refTextInhalt xlink:href="#GML_a2100ab4-cb15-434a-958e-c3ff29115600"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_7aacb4d1-b317-4631-9792-53b458e235d7">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="14">565701.248 5932754.257 565745.796 5932752.466 565749.389 5932724.218 565750.074 5932718.831 565764.954 5932720.724 565760.086 5932758.992 565759.203 5932765.938 565752.746 5932766.197 565681.892 5932769.047 565695.334 5932717.476 565710.817 5932721.512 565705.824 5932740.667 565704.856 5932740.415 565701.248 5932754.257 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="GML_6e38a3b9-e3ca-4c82-a313-40f92bd1e53b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565466.357 5932633.546</gml:lowerCorner>
          <gml:upperCorner>566039.903 5932821.874</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_4cc4df52-8953-4a2b-8d93-87a4d7a4da92">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_4c0deba5-7802-476a-9366-53202ebf007a">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="4">566018.410 5932820.371 566018.219 5932821.874 565886.529 5932785.604 565886.286 5932786.487 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">565886.286 5932786.487 565884.256 5932786.075 565882.188 5932785.946 565879.566 5932786.691 565877.704 5932788.681 565876.088 5932792.531 565875.090 5932796.586 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="28">565875.090 5932796.586 565877.167 5932780.277 565848.013 5932771.824 565846.639 5932782.612 565844.345 5932800.629 565817.704 5932801.748 565785.618 5932803.097 565785.424 5932798.393 565785.061 5932789.623 565771.225 5932790.194 565750.221 5932791.062 565725.312 5932792.551 565692.462 5932794.402 565667.474 5932795.494 565643.556 5932796.362 565604.863 5932797.583 565561.744 5932799.532 565544.463 5932800.651 565518.984 5932801.683 565490.864 5932802.354 565490.872 5932803.669 565469.410 5932804.104 565469.269 5932803.990 565466.357 5932783.298 565467.303 5932781.812 565619.348 5932722.606 565619.364 5932722.462 565824.500 5932642.091 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">565824.500 5932642.091 565829.381 5932640.364 565834.348 5932638.903 565838.824 5932637.762 565843.378 5932636.991 565848.420 5932636.431 565853.483 5932636.125 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="12">565853.483 5932636.125 565895.671 5932634.930 565940.829 5932633.546 565939.582 5932647.538 565939.560 5932649.201 565958.118 5932648.298 566039.903 5932651.651 566032.314 5932711.230 566029.946 5932729.803 566026.381 5932757.798 566020.325 5932805.339 566018.410 5932820.371 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>4000</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_77593218-e457-49b8-b1c8-562dd73e88e0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565784.328 5932732.699</gml:lowerCorner>
          <gml:upperCorner>565812.798 5932757.378</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2000</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">17</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_4ee44944-1d50-44d8-b328-7447f7d5fe0f">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="34">565810.009 5932743.150 565810.164 5932743.283 565810.306 5932743.417 565810.449 5932743.609 565812.503 5932752.038 565812.618 5932754.119 565812.798 5932757.378 565811.978 5932752.743 565811.878 5932752.439 565811.726 5932752.156 565811.528 5932751.904 565805.725 5932744.779 565797.950 5932735.234 565797.706 5932734.985 565797.423 5932734.782 565797.109 5932734.631 565796.773 5932734.537 565796.426 5932734.503 565796.078 5932734.530 565784.370 5932736.386 565784.328 5932735.475 565784.348 5932735.008 565784.469 5932734.491 565784.626 5932734.138 565784.914 5932733.684 565785.148 5932733.428 565785.421 5932733.218 565796.872 5932732.701 565796.964 5932732.699 565797.347 5932732.729 565797.726 5932732.828 565798.083 5932732.992 565798.399 5932733.210 565810.009 5932743.150 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_6d1fe3ea-eb97-4cee-97d2-c96e6cc2d4b3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565748.052 5932676.914</gml:lowerCorner>
          <gml:upperCorner>565776.875 5932714.635</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(D, M)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_a21458af-b76a-4cfb-a483-5ab4f9357748">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">565772.580 5932714.635 565752.159 5932714.072 565748.052 5932688.212 565776.875 5932676.914 565772.580 5932714.635 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_bb48d8e7-26db-4f84-b32f-e08938904528">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565931.757 5932648.530</gml:lowerCorner>
          <gml:upperCorner>565958.177 5932688.153</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">0.0</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_7c9d0610-91eb-4b39-8bff-583e272f596c">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="9">565953.137 5932688.153 565948.277 5932686.737 565931.757 5932681.887 565933.724 5932666.423 565935.897 5932649.333 565939.560 5932649.201 565958.108 5932648.530 565958.177 5932648.533 565953.137 5932688.153 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b6b1355e-ff64-4a76-bc72-f677717507ae">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565794.514 5932702.583</gml:lowerCorner>
          <gml:upperCorner>565822.829 5932748.747</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_80b0245d-093e-41e1-8839-0046fcb45774">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">565797.868 5932702.583 565794.514 5932728.738 565806.805 5932733.208 565821.906 5932748.747 565822.829 5932746.813 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_b98aa7c7-6d0c-414c-b007-a91dd03c5197">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565885.113 5932717.864</gml:lowerCorner>
          <gml:upperCorner>565942.202 5932734.503</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_8ddcc57a-877a-4db1-8b47-45c13f982ae0">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565885.113 5932717.864 565942.202 5932734.503 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_911db489-58c9-489a-b23b-a31c561294ae">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565615.332 5932787.335</gml:lowerCorner>
          <gml:upperCorner>565615.627 5932791.324</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_acfc2c77-5d9a-4436-8e81-fa87da5a65bd">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565615.332 5932787.335 565615.627 5932791.324 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_87862542-24ab-41fd-acba-45600290a502">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565980.840 5932649.473</gml:lowerCorner>
          <gml:upperCorner>566039.903 5932711.230</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_fa116591-4f3f-4154-ad83-e61522b9224a">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">565986.781 5932649.473 565980.840 5932696.225 566032.314 5932711.230 566039.903 5932651.651 565986.781 5932649.473 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:bautiefe uom="m">15</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_f5810a83-37a7-450e-8eb9-15e0fa48536b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565786.081 5932735.962</gml:lowerCorner>
          <gml:upperCorner>565809.935 5932761.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">13</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_71bdf3be-72a4-4625-99cd-57ccee73e94a">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">565786.672 5932761.795 565786.212 5932751.586 565786.081 5932748.696 565786.795 5932736.545 565797.152 5932735.962 565802.942 5932745.749 565806.785 5932752.245 565809.546 5932752.121 565809.935 5932760.745 565786.672 5932761.795 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_4c53246f-42ea-4bd4-b1b1-5ba972a52dfb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565556.643 5932740.957</gml:lowerCorner>
          <gml:upperCorner>565618.933 5932792.738</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_ec380f52-0686-4324-9cc6-4e4e63a9278e">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="9">565611.268 5932741.678 565595.709 5932747.978 565580.306 5932754.650 565572.545 5932759.219 565566.021 5932765.427 565558.831 5932778.114 565556.667 5932792.535 565574.171 5932792.737 565591.675 5932792.465 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">565591.675 5932792.465 565591.456 5932784.934 565605.531 5932784.318 565605.207 5932776.906 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="9">565605.207 5932776.906 565594.609 5932777.359 565584.004 5932777.635 565583.271 5932774.229 565581.696 5932771.122 565583.988 5932769.601 565586.433 5932768.342 565601.488 5932761.821 565616.694 5932755.662 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565616.694 5932755.662 565618.933 5932754.786 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">565618.933 5932754.786 565616.213 5932747.791 565613.110 5932740.957 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565613.110 5932740.957 565611.268 5932741.678 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a1436623-01a9-491f-8cf5-921ee8ebaeac">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565591.456 5932735.806</gml:lowerCorner>
          <gml:upperCorner>565640.534 5932792.465</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(D, F, I, M)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">36</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_42db0425-d7bd-44e4-97ae-c3f70ab80061"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_b6f96831-804d-46d8-812e-7386edb6b998"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_69c65303-1e32-434c-9ee6-e8d556d62146">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_c3fea7d3-6390-4883-91e0-5c92d015005f">
                  <gml:segments>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">565640.534 5932789.193 565616.135 5932791.291 565591.675 5932792.465 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="4">565591.675 5932792.465 565591.456 5932784.934 565605.531 5932784.318 565605.207 5932776.906 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">565605.207 5932776.906 565614.889 5932776.336 565624.561 5932775.618 565622.161 5932765.090 565618.933 5932754.786 565616.213 5932747.791 565613.110 5932740.957 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">565613.110 5932740.957 565626.281 5932735.806 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">565626.281 5932735.806 565635.887 5932761.838 565640.534 5932789.193 </gml:posList>
                    </gml:ArcString>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Z>8</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_eca52c4c-9060-4cb7-80d6-c2d3d14e9eac">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565954.152 5932710.550</gml:lowerCorner>
          <gml:upperCorner>566020.809 5932803.999</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(I, M)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_90ad5034-c656-4527-afa7-c81dff108352"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_2c404c4d-ea5a-4a1e-9a33-44b0216db2ee"/>
      <xplan:refTextInhalt xlink:href="#GML_a2100ab4-cb15-434a-958e-c3ff29115600"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_0ebc2ccc-e897-4f40-a9b9-5491eff0244d">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">566015.457 5932803.999 565954.152 5932787.114 565963.890 5932710.550 566008.562 5932723.570 566006.388 5932740.644 565978.243 5932732.441 565972.876 5932774.638 566001.070 5932782.403 566004.281 5932757.192 566020.809 5932761.972 566015.457 5932803.999 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_10c76eed-8d63-44ff-ada7-3a421efd4e78">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565672.699 5932779.557</gml:lowerCorner>
          <gml:upperCorner>565753.168 5932786.929</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_348213ad-d880-4bf3-8152-a5021f273b7f">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565672.699 5932786.929 565753.168 5932779.557 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_97eca282-8799-45ca-aea7-8e2642d0cff7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565887.287 5932649.333</gml:lowerCorner>
          <gml:upperCorner>565948.277 5932717.429</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_6717d790-7470-47a3-9bd8-bfc029e0d6b2">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">565935.897 5932649.333 565893.645 5932650.856 565887.287 5932700.791 565944.373 5932717.429 565948.277 5932686.737 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:bautiefe uom="m">17</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_11f4408e-5a52-440d-9d26-c17b044d28a5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565972.876 5932732.441</gml:lowerCorner>
          <gml:upperCorner>566006.388 5932782.403</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_dda8d7af-2a5e-469d-b4af-1ac18825078c">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">566004.281 5932757.192 566001.070 5932782.403 565972.876 5932774.638 565978.243 5932732.441 566006.388 5932740.644 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:bautiefe uom="m">17</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_143fcf9b-61c3-41ef-b23b-0cff3764dfe5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565638.380 5932720.017</gml:lowerCorner>
          <gml:upperCorner>565671.581 5932757.682</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(D, M)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_01f7b9c6-f77f-4614-a6cb-bb1b25ff3b05"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_2c404c4d-ea5a-4a1e-9a33-44b0216db2ee"/>
      <xplan:refTextInhalt xlink:href="#GML_6aa02183-2b23-40a0-88b4-a8dc4b8ac248"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_a9177bb0-8f79-4bd5-ba30-9c869bd8ee0e">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">565671.581 5932753.124 565648.731 5932757.682 565638.380 5932731.075 565666.805 5932720.017 565671.581 5932753.124 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_66c4cb50-e3a4-4a5c-8bca-eb16d64d237a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565954.152 5932710.550</gml:lowerCorner>
          <gml:upperCorner>566029.946 5932805.339</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(E, F, G, K)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_1c629753-705c-4932-8b2f-d02d970dca5b"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_6d1389a1-b1c0-460d-99d3-f792ecc96b5d"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_6921793d-c644-49dc-a6e3-166992db081f"/>
      <xplan:refTextInhalt xlink:href="#GML_a06eb58c-85b0-4c8e-8611-5cadb4bc4f9c"/>
      <xplan:refTextInhalt xlink:href="#GML_f0accd68-1dde-4113-8e4d-854790f72749"/>
      <xplan:refTextInhalt xlink:href="#GML_ff170ce3-93ea-4eba-8a3c-2c857a97ca78"/>
      <xplan:refTextInhalt xlink:href="#GML_0cb41294-2033-4a5a-a157-f7b249731d5d"/>
      <xplan:refTextInhalt xlink:href="#GML_780db5ba-9a30-4f5c-8b8c-dc2140285545"/>
      <xplan:refTextInhalt xlink:href="#GML_bddf163c-f007-4fe7-8fc7-71d7cb8d11c1"/>
      <xplan:refTextInhalt xlink:href="#GML_bbd24b02-d2b0-4363-b215-0103b5f536a2"/>
      <xplan:refTextInhalt xlink:href="#GML_4a3632a9-9dfc-42b0-a651-e3f1f6513c54"/>
      <xplan:refTextInhalt xlink:href="#GML_c9f52069-f701-446d-9208-c83f72dce875"/>
      <xplan:refTextInhalt xlink:href="#GML_2c404c4d-ea5a-4a1e-9a33-44b0216db2ee"/>
      <xplan:refTextInhalt xlink:href="#GML_9c11408b-f22f-4643-b0f5-cc08ffc810b3"/>
      <xplan:refTextInhalt xlink:href="#GML_79bd11df-a261-46ed-8059-5c50322eace7"/>
      <xplan:refTextInhalt xlink:href="#GML_06fc76d6-d678-440e-a298-76c3bddb53d4"/>
      <xplan:refTextInhalt xlink:href="#GML_30ca1209-45fc-4a5e-a5df-c40f9cdd392f"/>
      <xplan:refTextInhalt xlink:href="#GML_51e6e5f3-b8ca-4a73-8d1e-70c2410388c9"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_72da7d42-88ce-49da-b663-16e872f2721a">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="8">565954.152 5932787.114 565963.890 5932710.550 566008.562 5932723.570 566029.946 5932729.803 566026.381 5932757.798 566020.325 5932805.339 566015.457 5932803.999 565954.152 5932787.114 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_13fb7d4e-6cb1-4025-b05e-0c505d857211">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565794.479 5932649.634</gml:lowerCorner>
          <gml:upperCorner>565873.373 5932760.406</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(K, L)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">0.0</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_f60830a5-0bf1-4525-bf6b-bae4c0132562">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="12">565853.006 5932730.773 565860.740 5932737.162 565859.425 5932760.406 565825.029 5932751.011 565822.829 5932746.813 565813.751 5932729.494 565800.793 5932730.754 565797.868 5932702.583 565794.479 5932670.021 565846.606 5932649.634 565873.373 5932650.580 565853.006 5932730.773 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_88098600-5cd2-431b-865c-7c79f14264f8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565777.597 5932764.290</gml:lowerCorner>
          <gml:upperCorner>565801.581 5932767.581</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2000</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">17</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_fb160f7b-ec24-48db-8e61-90794f6249c4"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b65925f4-ec93-416f-8460-1b74b851b9de">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="14">565784.183 5932764.290 565801.581 5932765.223 565798.588 5932766.898 565798.329 5932767.022 565797.392 5932767.463 565797.082 5932767.550 565796.764 5932767.581 565778.616 5932766.619 565778.278 5932766.316 565777.899 5932765.833 565777.597 5932765.269 565777.625 5932764.686 565783.066 5932764.466 565784.183 5932764.290 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_a4b8c804-abbd-4297-9c1a-d4a9d3464242">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565466.357 5932633.546</gml:lowerCorner>
          <gml:upperCorner>565940.829 5932804.104</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_6ec97631-54cb-4598-a00a-d0b0590507ab">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="21">565784.722 5932781.409 565785.061 5932789.623 565771.225 5932790.194 565750.221 5932791.062 565725.312 5932792.551 565692.462 5932794.402 565667.474 5932795.494 565643.556 5932796.362 565604.863 5932797.583 565561.744 5932799.532 565544.463 5932800.651 565518.984 5932801.683 565490.864 5932802.354 565490.872 5932803.669 565469.410 5932804.104 565469.269 5932803.990 565466.357 5932783.298 565467.303 5932781.812 565619.348 5932722.606 565619.364 5932722.462 565824.500 5932642.091 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="7">565824.500 5932642.091 565829.381 5932640.364 565834.348 5932638.903 565838.824 5932637.762 565843.378 5932636.991 565848.420 5932636.431 565853.483 5932636.125 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">565853.483 5932636.125 565895.671 5932634.930 565940.829 5932633.546 565939.582 5932647.538 565939.560 5932649.201 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_3016090c-8b24-498d-9717-cb5c93ea914a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565787.877 5932781.135</gml:lowerCorner>
          <gml:upperCorner>565819.555 5932797.619</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_dba5e2ff-61eb-4c7e-a3dd-196ffb7765bd">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="7">565818.846 5932781.135 565787.877 5932782.527 565788.102 5932787.522 565803.995 5932797.619 565816.740 5932797.046 565819.555 5932796.919 565818.846 5932781.135 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_4c2c00ad-a48b-4eda-adae-684835aa844c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565972.876 5932732.441</gml:lowerCorner>
          <gml:upperCorner>566006.388 5932782.403</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(H, M)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_3cf0aaf7-21ec-457f-9c96-f595380a9453"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_085df8df-420e-4027-a62f-9072404ff9c5">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">566004.281 5932757.192 566001.070 5932782.403 565972.876 5932774.638 565978.243 5932732.441 566006.388 5932740.644 566004.281 5932757.192 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Zzwingend>1</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_61fc3efd-8db9-47a0-a58b-5b4677e15a02">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565865.344 5932769.084</gml:lowerCorner>
          <gml:upperCorner>565865.344 5932769.084</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">7.9</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_385bc8c3-3b10-4019-bfda-c4ef87d57ad9">
          <gml:pos>565865.344 5932769.084</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_865c1105-4be4-4df2-9e31-461aeb609802">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565902.529 5932741.171</gml:lowerCorner>
          <gml:upperCorner>565923.506 5932760.557</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(H)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_0a4b386f-605e-4742-baf3-adb7e931fe82"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_97733e4b-9c30-40a0-b54b-26deb6ca5ba7">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">565921.751 5932760.557 565902.529 5932755.263 565904.323 5932741.171 565923.506 5932746.761 565921.751 5932760.557 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Zzwingend>1</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_b473520b-9de1-43a6-817e-8927c8377d70">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565777.597 5932732.699</gml:lowerCorner>
          <gml:upperCorner>565812.983 5932767.581</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">53</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_f3267cc5-2072-44f9-9fba-5bef2ecae2d3">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_3c3eb2af-da31-4d97-ba64-f2dab870758e">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="12">565801.581 5932765.223 565798.588 5932766.898 565798.329 5932767.022 565797.392 5932767.463 565797.082 5932767.550 565796.764 5932767.581 565778.616 5932766.619 565778.278 5932766.316 565777.899 5932765.833 565777.597 5932765.269 565777.625 5932764.686 565778.077 5932755.286 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">565778.077 5932755.286 565778.086 5932755.163 565778.098 5932755.041 565778.129 5932754.886 565778.165 5932754.732 565778.213 5932754.607 565778.265 5932754.484 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="8">565778.265 5932754.484 565778.415 5932754.252 565781.461 5932750.180 565781.750 5932749.795 565784.217 5932745.516 565784.748 5932744.595 565784.370 5932736.386 565784.328 5932735.475 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">565784.328 5932735.475 565784.600 5932734.214 565785.421 5932733.218 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">565785.421 5932733.218 565796.964 5932732.699 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">565796.964 5932732.699 565797.726 5932732.829 565798.399 5932733.210 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="22">565798.399 5932733.210 565810.009 5932743.150 565810.164 5932743.283 565810.306 5932743.417 565810.449 5932743.609 565812.503 5932752.038 565812.618 5932754.119 565812.798 5932757.378 565812.981 5932760.699 565812.983 5932760.733 565812.966 5932761.115 565812.881 5932761.498 565812.731 5932761.860 565812.521 5932762.191 565812.256 5932762.481 565811.945 5932762.720 565811.597 5932762.901 565811.223 5932763.019 565810.845 5932763.069 565808.791 5932763.161 565804.968 5932763.329 565801.581 5932765.223 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zmin>6</xplan:Zmin>
      <xplan:Zmax>13</xplan:Zmax>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_c5c5b870-7402-499e-a07e-84b966ab2380">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565541.828 5932754.650</gml:lowerCorner>
          <gml:upperCorner>565580.306 5932792.535</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_4b8c5f90-cc76-47a6-8235-4dc04d1392bc">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">565580.306 5932754.650 565541.828 5932771.726 565551.019 5932792.437 565556.667 5932792.535 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_144e9d11-6d29-4852-ae13-8678467c56fc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565794.514 5932702.583</gml:lowerCorner>
          <gml:upperCorner>565822.829 5932748.747</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">16.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_d363b0ce-c578-4052-9546-96bc9b3f5042"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_1792000d-ecba-450d-ab25-a1b4fa2e6a2e">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="8">565822.829 5932746.813 565821.906 5932748.747 565806.805 5932733.208 565794.514 5932728.738 565797.868 5932702.583 565800.793 5932730.754 565813.751 5932729.494 565822.829 5932746.813 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_fb306e82-538a-4679-bc52-690183ba0a7c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565638.380 5932720.017</gml:lowerCorner>
          <gml:upperCorner>565671.581 5932757.682</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(D, M)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_0969bd46-c5d3-4e1a-8268-893e81ebe10e">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">565671.581 5932753.124 565648.731 5932757.682 565638.380 5932731.075 565666.805 5932720.017 565671.581 5932753.124 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_a2e0776a-110b-4782-a11e-d4fe70ca1d5f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565626.281 5932707.896</gml:lowerCorner>
          <gml:upperCorner>565697.832 5932735.806</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_32f96eb5-7e86-4aac-9e7f-f15acfc349b1">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="6">565697.832 5932707.896 565685.116 5932712.880 565683.901 5932709.763 565629.719 5932730.882 565630.930 5932733.988 565626.281 5932735.806 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a6594fbf-407f-4351-b3ae-5af4f2c87b01">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565701.248 5932716.759</gml:lowerCorner>
          <gml:upperCorner>565749.389 5932754.257</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(H, M)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_937f8f81-340a-447d-bf09-5c8360c8b8d8"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_be9e0cef-dc44-4f0f-acf3-97dff9a44938">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="9">565745.796 5932752.466 565701.248 5932754.257 565704.856 5932740.415 565705.824 5932740.667 565711.259 5932719.818 565719.064 5932716.759 565720.550 5932720.549 565749.389 5932724.218 565745.796 5932752.466 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Zzwingend>1</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_41c4f008-fd82-4bd9-9a0b-4020ff16c7bc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565748.052 5932676.914</gml:lowerCorner>
          <gml:upperCorner>565776.875 5932714.635</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(D, M)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_4a0b9fc1-6bf2-4394-a004-c5a756a62df1"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_2c404c4d-ea5a-4a1e-9a33-44b0216db2ee"/>
      <xplan:refTextInhalt xlink:href="#GML_6aa02183-2b23-40a0-88b4-a8dc4b8ac248"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_95fb8dbe-d0a9-4d8e-bdc0-f2b0ffff40dc">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">565772.580 5932714.635 565765.753 5932714.446 565752.159 5932714.072 565748.052 5932688.212 565776.875 5932676.914 565772.580 5932714.635 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_8be64b5f-88ab-4f30-8b0d-a2e4f272ef4c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565581.696 5932754.786</gml:lowerCorner>
          <gml:upperCorner>565624.561 5932777.635</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(H, M)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_7e373680-91fb-49a7-882f-71a0838de704"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_a2100ab4-cb15-434a-958e-c3ff29115600"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_2d8b929f-f6f6-4352-8387-fb57f7b1d34d">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_1c163907-9300-42ea-905d-aef5b061729d">
                  <gml:segments>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="11">565624.561 5932775.618 565614.889 5932776.336 565605.207 5932776.906 565594.609 5932777.359 565584.004 5932777.635 565583.271 5932774.229 565581.696 5932771.122 565583.988 5932769.601 565586.433 5932768.342 565601.488 5932761.821 565616.694 5932755.662 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">565616.694 5932755.662 565618.933 5932754.786 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">565618.933 5932754.786 565622.161 5932765.090 565624.561 5932775.618 </gml:posList>
                    </gml:ArcString>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_6f0cc62f-d5a1-4fef-81da-fc3723ff3e11">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565787.166 5932672.664</gml:lowerCorner>
          <gml:upperCorner>565794.754 5932732.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_3cc4d961-0c9a-4316-be53-539b6ba28c66">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565787.166 5932732.264 565794.754 5932672.664 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_0cb13251-e2df-463f-bfa2-6fe59731f59f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565764.954 5932714.446</gml:lowerCorner>
          <gml:upperCorner>565765.753 5932720.724</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_653e32d6-7a8d-431e-927e-6deb8a99a986">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565764.954 5932720.724 565765.753 5932714.446 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_bb35d750-2755-468c-92f2-6294b8d75f53">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565980.840 5932649.544</gml:lowerCorner>
          <gml:upperCorner>566039.903 5932711.230</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(A, E, I)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_edc7c7fa-3a65-4a60-8687-e8c0294838bf"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_68e516b8-4840-44ef-8781-b632022bb1cf"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e89a641b-b46b-4787-b9cf-1ec5516feeca">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">566032.314 5932711.230 565980.840 5932696.225 565986.778 5932649.544 566039.903 5932651.651 566032.314 5932711.230 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">565997.337 5932685.410 566019.653 5932691.915 566022.975 5932665.833 565999.930 5932665.018 565997.337 5932685.410 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_70ae082f-d9dc-4308-80a2-822c369d97d8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565997.337 5932665.018</gml:lowerCorner>
          <gml:upperCorner>566022.975 5932691.915</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A, E, H)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_453eb485-75cf-4d56-979a-25c8effab0f5"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_6af2b297-49f7-4b0a-883d-b73fab5571a9"/>
      <xplan:refTextInhalt xlink:href="#GML_0004eb97-9458-4ec0-8d77-657bf88d18b8"/>
      <xplan:refTextInhalt xlink:href="#GML_a2100ab4-cb15-434a-958e-c3ff29115600"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_2e21f147-a080-4df0-85b1-844b17a0f81f">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">566019.653 5932691.915 565997.337 5932685.410 565999.930 5932665.018 566022.975 5932665.833 566019.653 5932691.915 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_1048f9c7-e935-4593-bea0-46d9eee4d619">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565672.699 5932779.557</gml:lowerCorner>
          <gml:upperCorner>565753.168 5932786.929</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_de62f70e-d6e5-4b50-9702-d329b6a93c04"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_94ce43f2-9ccf-4246-848a-396d1f245117">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565753.168 5932779.557 565672.699 5932786.929 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>3000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_0c3dfa93-9efb-4283-8b1f-6db1d2c5bc8b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565750.467 5932775.382</gml:lowerCorner>
          <gml:upperCorner>565751.467 5932776.382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.3</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_8d570225-40b5-42c8-ab46-2d5d075c5bcb">
          <gml:pos>565750.467 5932775.382</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ce97a57d-e686-466f-829c-be3cca0c9f90">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565605.207 5932754.786</gml:lowerCorner>
          <gml:upperCorner>565624.561 5932776.906</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_4d4dca59-0e99-4996-bdf0-39a14592af7e">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="5">565618.933 5932754.786 565622.161 5932765.090 565624.561 5932775.618 565614.889 5932776.336 565605.207 5932776.906 </gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_245f5228-dbfc-445a-a50c-93bd98f0356f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565786.081 5932735.962</gml:lowerCorner>
          <gml:upperCorner>565809.935 5932761.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_38e79e4d-0e81-4c49-a77b-0db61866da6d">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="10">565809.935 5932760.745 565809.546 5932752.121 565806.785 5932752.245 565802.942 5932745.749 565797.152 5932735.962 565786.795 5932736.545 565786.081 5932748.696 565786.212 5932751.586 565786.672 5932761.795 565809.935 5932760.745 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_a6f20217-5907-4cc7-bd95-6fc7d41835c4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565943.811 5932751.298</gml:lowerCorner>
          <gml:upperCorner>565943.811 5932751.298</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.3</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_ec650fe2-993f-4a59-8c73-574161b22cd2">
          <gml:pos>565943.811 5932751.298</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_72fcbdf6-e245-4f05-a185-36a9e00c76e4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566015.457 5932803.999</gml:lowerCorner>
          <gml:upperCorner>566020.325 5932805.339</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_dbf0d832-310a-419b-8734-c14efcd61d82">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">566015.457 5932803.999 566020.325 5932805.339 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_0a4866da-e023-4092-94aa-6c0213bec033">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565980.840 5932649.473</gml:lowerCorner>
          <gml:upperCorner>566039.903 5932711.230</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A, E, I)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_92fa22fc-2e8a-4c0c-96d3-6c36bf65273e"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_6af2b297-49f7-4b0a-883d-b73fab5571a9"/>
      <xplan:refTextInhalt xlink:href="#GML_0004eb97-9458-4ec0-8d77-657bf88d18b8"/>
      <xplan:refTextInhalt xlink:href="#GML_a2100ab4-cb15-434a-958e-c3ff29115600"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_f9b2b166-514b-407d-bab7-6bfeb0e0cd10">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">566032.314 5932711.230 565980.840 5932696.225 565986.781 5932649.473 566039.903 5932651.651 566032.314 5932711.230 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">565997.337 5932685.410 566019.653 5932691.915 566022.975 5932665.833 565999.930 5932665.018 565997.337 5932685.410 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_2a1e9c3f-efe0-4cb8-9954-aa5969381a04">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565681.892 5932691.320</gml:lowerCorner>
          <gml:upperCorner>565764.954 5932769.047</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(B, D, I, M)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">35</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_abc3741a-7af6-4d6d-b01c-2a73ecd6bd48"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_2eee5e20-893c-435e-b044-05ebc2b71856"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_eb07a46c-d786-4422-943f-52170d33b402">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="17">565759.203 5932765.938 565752.746 5932766.197 565681.892 5932769.047 565697.832 5932707.896 565740.121 5932691.320 565741.582 5932707.933 565719.064 5932716.759 565711.259 5932719.818 565705.824 5932740.667 565704.856 5932740.415 565701.248 5932754.257 565745.796 5932752.466 565749.389 5932724.218 565750.074 5932718.831 565764.954 5932720.724 565760.086 5932758.992 565759.203 5932765.938 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6451d01a-9722-4da0-96b2-200cff85b0f7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566004.281 5932723.570</gml:lowerCorner>
          <gml:upperCorner>566029.946 5932763.378</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_85ed279b-0686-4574-98be-f32a13aee631">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="7">566029.946 5932729.803 566008.562 5932723.570 566006.388 5932740.644 566004.281 5932757.192 566020.809 5932761.972 566025.671 5932763.378 566029.946 5932729.803 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:bautiefe uom="m">22</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_b6aaa0d7-dd24-41d6-bdaa-9ef9cebdd678">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565953.137 5932648.298</gml:lowerCorner>
          <gml:upperCorner>565986.781 5932696.225</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(G)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_bd1d438f-771d-41b2-8298-9d40bb4e44b2"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e06f8260-990e-4f04-913b-1f54d14705c4"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_685890bd-d350-4a77-9190-639345d14e43">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">565980.840 5932696.225 565953.137 5932688.153 565958.118 5932648.298 565986.781 5932649.473 565980.840 5932696.225 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_a2612630-e9fb-44d4-824a-96e371fb5885">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565556.643 5932787.335</gml:lowerCorner>
          <gml:upperCorner>565615.627 5932792.738</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Arkade</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">3.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_fb4823b8-5104-45b8-a1d7-46ee7c37cbb3"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_aa677bd3-4a3c-45f5-b5fa-650241232668">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_24eee4f4-f352-49cc-a253-ef5b32ed361d">
                  <gml:segments>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">565556.729 5932788.535 565586.044 5932788.597 565615.332 5932787.335 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">565615.332 5932787.335 565615.627 5932791.324 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="5">565615.627 5932791.324 565586.161 5932792.601 565556.667 5932792.535 565556.648 5932790.534 565556.729 5932788.535 </gml:posList>
                    </gml:ArcString>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_56d95de6-a232-4dd2-b332-30871aeca91d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565719.064 5932716.759</gml:lowerCorner>
          <gml:upperCorner>565749.389 5932724.218</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_55d1ac83-40b2-4873-8722-62bf33e5f297">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="3">565749.389 5932724.218 565720.550 5932720.549 565719.064 5932716.759 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_96abb9d8-6277-4e08-9a41-091df5de3360">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566015.457 5932761.972</gml:lowerCorner>
          <gml:upperCorner>566025.670 5932805.339</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(G)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_d10aab1f-32af-42c1-9984-c0a8bcda02ad"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e06f8260-990e-4f04-913b-1f54d14705c4"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ff0d8d13-c7f7-4bfb-bcd4-fea089cc2f40">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">566020.325 5932805.339 566015.457 5932803.999 566020.809 5932761.972 566025.670 5932763.377 566020.325 5932805.339 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f4e30c51-ba59-4730-9604-d97ceda14c17">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565748.052 5932676.914</gml:lowerCorner>
          <gml:upperCorner>565776.875 5932714.635</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_bf31d58b-a4cc-4db5-b944-32f01d897bf4">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">565752.159 5932714.072 565772.580 5932714.635 565776.875 5932676.914 565748.052 5932688.212 565752.159 5932714.072 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_2211fd90-eac2-4501-bbe8-b446d3c3293e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565776.875 5932670.021</gml:lowerCorner>
          <gml:upperCorner>565794.479 5932676.914</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_842dfc4d-992b-4894-a088-e8aabff2d0da">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="3">565794.479 5932670.021 565785.309 5932673.608 565776.875 5932676.914 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_7a92dc24-ef04-43f0-8dd4-e7a0dc24c4ef">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565497.986 5932791.745</gml:lowerCorner>
          <gml:upperCorner>565497.986 5932791.745</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">5.2</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d7c0f134-2893-48a9-9e83-5cf2a0cbdc69">
          <gml:pos>565497.986 5932791.745</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_66c1df3e-2b4c-4fb5-9650-371655a5b8f1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565960.313 5932738.671</gml:lowerCorner>
          <gml:upperCorner>565976.842 5932743.452</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_295bbad3-1a0e-46a1-8848-c673987eb37c">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565960.313 5932738.671 565976.842 5932743.452 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_53ee118d-8c4c-45b5-a951-4605817e142f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565640.247 5932761.796</gml:lowerCorner>
          <gml:upperCorner>565640.247 5932761.796</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">9.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_b2aa6082-911e-4a4c-96b3-d30b8acafcd5">
          <gml:pos>565640.247 5932761.796</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_30d3e1fa-b1fa-42fe-994f-d617658cb412">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565972.876 5932732.441</gml:lowerCorner>
          <gml:upperCorner>566006.388 5932782.403</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(H, M)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_0ae6b5a2-b9e4-4452-b901-0d170840c34b"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_2c404c4d-ea5a-4a1e-9a33-44b0216db2ee"/>
      <xplan:refTextInhalt xlink:href="#GML_a2100ab4-cb15-434a-958e-c3ff29115600"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_fe815e6d-21a7-4b60-add9-24bbf08f22a7">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">566004.281 5932757.192 566001.070 5932782.403 565972.876 5932774.638 565978.243 5932732.441 566006.388 5932740.644 566004.281 5932757.192 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_da343c9f-3532-40dc-a39c-27823fa28b84">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565954.152 5932710.550</gml:lowerCorner>
          <gml:upperCorner>566020.806 5932803.999</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_3892d86d-895d-4ead-8b68-14c1c698baf1">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">566008.562 5932723.570 565963.890 5932710.550 565954.152 5932787.114 566015.457 5932803.999 566020.806 5932761.971 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:bautiefe uom="m">17</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c477ec48-117a-47c3-ab34-98a809d7f950">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565883.744 5932717.864</gml:lowerCorner>
          <gml:upperCorner>565942.202 5932782.154</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(I)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_50e2c425-aeed-4652-b5ad-ad25a8522993"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_8bd91d56-9ff8-4c04-917e-fd683afc0d7b"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_f14cd29b-25df-4b3f-8ed9-f213ad4ddf5d">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">565942.202 5932734.503 565936.141 5932782.154 565900.360 5932772.299 565902.529 5932755.263 565921.751 5932760.557 565923.506 5932746.761 565904.323 5932741.171 565905.170 5932734.519 565883.744 5932728.618 565885.113 5932717.864 565942.202 5932734.503 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_e13588fe-85f5-41f0-946f-ca0fd7508555">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565878.934 5932648.298</gml:lowerCorner>
          <gml:upperCorner>566039.903 5932782.154</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A, D, E, F, G, K)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_057e45f1-bfd0-4d9e-affc-317be4dec835"/>
      <xplan:wirdDargestelltDurch xlink:href="#GML_65d9180b-f37d-4a8e-acee-cd570aff9612"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_6921793d-c644-49dc-a6e3-166992db081f"/>
      <xplan:refTextInhalt xlink:href="#GML_a06eb58c-85b0-4c8e-8611-5cadb4bc4f9c"/>
      <xplan:refTextInhalt xlink:href="#GML_f0accd68-1dde-4113-8e4d-854790f72749"/>
      <xplan:refTextInhalt xlink:href="#GML_ff170ce3-93ea-4eba-8a3c-2c857a97ca78"/>
      <xplan:refTextInhalt xlink:href="#GML_0cb41294-2033-4a5a-a157-f7b249731d5d"/>
      <xplan:refTextInhalt xlink:href="#GML_780db5ba-9a30-4f5c-8b8c-dc2140285545"/>
      <xplan:refTextInhalt xlink:href="#GML_bddf163c-f007-4fe7-8fc7-71d7cb8d11c1"/>
      <xplan:refTextInhalt xlink:href="#GML_bbd24b02-d2b0-4363-b215-0103b5f536a2"/>
      <xplan:refTextInhalt xlink:href="#GML_4a3632a9-9dfc-42b0-a651-e3f1f6513c54"/>
      <xplan:refTextInhalt xlink:href="#GML_c9f52069-f701-446d-9208-c83f72dce875"/>
      <xplan:refTextInhalt xlink:href="#GML_9c11408b-f22f-4643-b0f5-cc08ffc810b3"/>
      <xplan:refTextInhalt xlink:href="#GML_2c404c4d-ea5a-4a1e-9a33-44b0216db2ee"/>
      <xplan:refTextInhalt xlink:href="#GML_16d136cb-fe5f-4867-8a22-40850fa95ec7"/>
      <xplan:refTextInhalt xlink:href="#GML_79bd11df-a261-46ed-8059-5c50322eace7"/>
      <xplan:refTextInhalt xlink:href="#GML_06fc76d6-d678-440e-a298-76c3bddb53d4"/>
      <xplan:refTextInhalt xlink:href="#GML_30ca1209-45fc-4a5e-a5df-c40f9cdd392f"/>
      <xplan:refTextInhalt xlink:href="#GML_51e6e5f3-b8ca-4a73-8d1e-70c2410388c9"/>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_04a3dc5e-5fcc-4894-9943-7f091c738f03">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="19">565900.360 5932772.299 565878.934 5932766.397 565883.744 5932728.618 565885.113 5932717.864 565885.978 5932711.065 565887.287 5932700.791 565893.645 5932650.856 565935.897 5932649.333 565939.560 5932649.201 565958.118 5932648.298 566039.903 5932651.651 566032.314 5932711.230 565980.840 5932696.225 565953.137 5932688.153 565948.277 5932686.737 565944.373 5932717.429 565942.202 5932734.503 565936.141 5932782.154 565900.360 5932772.299 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_fa85402a-4b45-436a-a19f-fdd6529ac3d0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565942.202 5932717.429</gml:lowerCorner>
          <gml:upperCorner>565944.373 5932734.503</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_d761f7ea-f24b-42a2-b840-ac36d5828dc1">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="3">565942.202 5932734.503 565942.727 5932730.371 565944.373 5932717.429 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_0dc09d65-60cd-4506-a28c-cc81750ddfda">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565997.337 5932665.018</gml:lowerCorner>
          <gml:upperCorner>566022.975 5932691.915</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_201cee8d-8c10-474f-8688-b7b385e0c9cc">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">565997.337 5932685.410 565999.930 5932665.018 566022.975 5932665.833 566019.653 5932691.915 565997.337 5932685.410 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:bautiefe uom="m">15</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_0c50d59b-aa4d-418a-ae99-df7bc18ed03e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565640.527 5932783.759</gml:lowerCorner>
          <gml:upperCorner>565753.337 5932791.111</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_6f1de82f-de03-4059-86eb-97b33e97396e">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">565640.527 5932789.194 565645.702 5932788.635 565650.871 5932788.031 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">565650.871 5932788.031 565650.945 5932789.863 565672.699 5932786.929 565673.083 5932791.111 565753.337 5932783.759 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_1b538451-90d0-41b4-8e63-8a30ca9cd087">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565931.757 5932648.298</gml:lowerCorner>
          <gml:upperCorner>565958.118 5932688.153</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_bbc5b00c-d67c-495f-8416-1952d9533a1f">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="8">565958.118 5932648.298 565939.560 5932649.201 565935.897 5932649.333 565933.724 5932666.423 565931.757 5932681.887 565948.277 5932686.737 565953.137 5932688.153 565958.118 5932648.298 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:bautiefe uom="m">22</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_a08b6875-0376-4e69-98e5-96f685c1a0b8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565794.479 5932649.634</gml:lowerCorner>
          <gml:upperCorner>565873.373 5932760.210</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_41eae937-2303-4ebc-b6bf-f539cd7b4405">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="12">565873.373 5932650.580 565846.606 5932649.634 565794.479 5932670.021 565797.868 5932702.583 565800.793 5932730.754 565813.751 5932729.494 565822.829 5932746.813 565825.029 5932751.011 565859.255 5932760.210 565860.740 5932737.162 565853.006 5932730.773 565873.373 5932650.580 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7ddc701d-5523-45cc-b1c2-57a358627960">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565782.836 5932734.503</gml:lowerCorner>
          <gml:upperCorner>565813.870 5932765.762</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">26</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_04f9276a-3bcb-47a7-b52a-aa5600aff67e">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="42">565813.870 5932763.441 565813.853 5932763.828 565813.770 5932764.207 565813.621 5932764.566 565813.413 5932764.893 565813.151 5932765.179 565812.844 5932765.415 565812.500 5932765.595 565812.130 5932765.712 565811.745 5932765.762 565811.391 5932765.750 565801.581 5932765.223 565784.183 5932764.290 565784.053 5932764.177 565783.737 5932763.787 565783.546 5932763.443 565783.412 5932763.054 565783.337 5932762.525 565785.344 5932751.307 565784.217 5932745.516 565783.789 5932743.315 565782.836 5932738.412 565782.858 5932737.914 565782.965 5932737.430 565783.101 5932737.101 565783.310 5932736.773 565783.545 5932736.517 565784.370 5932736.386 565796.078 5932734.530 565796.426 5932734.503 565796.773 5932734.537 565797.109 5932734.631 565797.423 5932734.782 565797.706 5932734.985 565797.950 5932735.234 565805.725 5932744.779 565811.528 5932751.904 565811.726 5932752.156 565811.878 5932752.439 565811.978 5932752.743 565812.798 5932757.378 565813.870 5932763.441 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zmin>2</xplan:Zmin>
      <xplan:Zmax>5</xplan:Zmax>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_fd6c2735-5c90-494e-acc2-e027bedf075d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565785.411 5932798.091</gml:lowerCorner>
          <gml:upperCorner>565838.579 5932800.871</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_4cbfb5ac-d1ee-48af-ac34-1e0a645ea286">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="5">565838.579 5932800.871 565827.884 5932799.919 565817.174 5932799.164 565801.300 5932798.410 565785.411 5932798.091 </gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_2c404c4d-ea5a-4a1e-9a33-44b0216db2ee">
      <xplan:schluessel>§2Nr.25</xplan:schluessel>
      <xplan:text>Auf den mit „(M)“ bezeichneten Flächen des Mischgebiets ist der Erschütterungsschutz der Gebäude durch bauliche oder technische Maßnahmen (zum Beispiel an Wänden, Decken und Fundamenten) so sicherzustellen, dass die Anhaltswerte der DIN 4150 (Erschütterungen im Bauwesen), Teil 2 (Einwirkungen auf Menschen in Gebäuden), Tabelle 1, Zeile 3 (Mischgebiete nach Baunutzungsverordnung in der Fassung vom 21. November 2017 (BGBl. I S. 3787)) eingehalten werden. Zusätzlich ist durch die baulichen und technischen Maßnahmen zu gewährleisten, dass der sekundäre Luftschall die Immissionsrichtwerte der Technischen Anleitung zum Schutz gegen Lärm vom 26. August 1998 (Gemeinsames Ministerialblatt S. 503), geändert am 1. Juni 2017 (BAnz. AT 08.06.2017 B 5), Nummer 6.2, nicht überschreitet. Einsichtnahmestelle der DIN 4150: Freie und Hansestadt Hamburg, Behörde für Umwelt und Energie, Amt für Immissionsschutz und Abfallwirtschaft, Bezugsquelle der DIN 4150: Beuth Verlag GmbH, Berlin.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_2577d7ff-6eba-421d-bb14-e1e07dc28acc">
      <xplan:schluessel>§2Nr.4</xplan:schluessel>
      <xplan:text>Auf der Fläche für den besonderen Nutzungszweck „Servicegebäude Marina“ sind innerhalb der überbaubaren Fläche nur eine Schank- und Speisewirtschaft sowie Serviceeinrichtungen für die Marina zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_057e45f1-bfd0-4d9e-affc-317be4dec835">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565969.111 5932672.321</gml:lowerCorner>
          <gml:upperCorner>565969.111 5932672.321</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_e13588fe-85f5-41f0-946f-ca0fd7508555"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_98ba2b86-7782-4a76-9926-e70f15ab45ab">
          <gml:pos>565969.111 5932672.321</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0a4b386f-605e-4742-baf3-adb7e931fe82">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565907.860 5932750.530</gml:lowerCorner>
          <gml:upperCorner>565907.860 5932750.530</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_865c1105-4be4-4df2-9e31-461aeb609802"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_2c1e3a08-c483-45f2-96c4-dd5a184ba9de">
          <gml:pos>565907.860 5932750.530</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ed6831a9-45a2-48a3-81c4-1c40ac213417">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565711.245 5932711.515</gml:lowerCorner>
          <gml:upperCorner>565712.245 5932712.515</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_7be06cd5-9d19-4c48-a414-1c8a312b2561"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_0b2fa47f-3186-4d46-a027-c08f1ea51b0b">
          <gml:pos>565711.245 5932711.515</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_79bd11df-a261-46ed-8059-5c50322eace7">
      <xplan:schluessel>§2Nr.18</xplan:schluessel>
      <xplan:text>Werbeanlagen größer als 2 m² und Werbeanlagen oberhalb der Gebäudetraufen sind unzulässig. Die Gestaltung der Gesamtbaukörper und der privaten Freiflächen darf nicht durch Werbeanlagen beeinträchtigt werden. Werbeanlagen sind nur an der Stätte der Leistung zulässig. Oberhalb der Brüstung des zweiten Vollgeschosses sind Werbeanlagen nur ausnahmsweise zulässig, wenn zudem das Ortsbild nicht beeinträchtigt wird. </xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_edc7c7fa-3a65-4a60-8687-e8c0294838bf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566020.526 5932702.491</gml:lowerCorner>
          <gml:upperCorner>566020.526 5932702.491</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_bb35d750-2755-468c-92f2-6294b8d75f53"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_38c79d6e-95c2-4545-b10f-d88cf6ce23d5">
          <gml:pos>566020.526 5932702.491</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_230d0675-209b-439d-b2f5-9b7216ab7c9b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565923.815 5932676.675</gml:lowerCorner>
          <gml:upperCorner>565924.815 5932677.675</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_7a3a837b-cfee-4c4c-86b5-ba172fac1794"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_525e94d1-50c0-4010-905d-6dcc918a1e7e">
          <gml:pos>565923.815 5932676.675</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b10ac8ff-a802-4e14-8756-f9dda612989d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565895.482 5932709.913</gml:lowerCorner>
          <gml:upperCorner>565895.482 5932709.913</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_5f9737ad-11f4-43f9-9c83-1ba4d3e68cff"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_63fe3947-f940-4fe5-8542-f51302a3159a">
          <gml:pos>565895.482 5932709.913</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_780db5ba-9a30-4f5c-8b8c-dc2140285545">
      <xplan:schluessel>§2Nr.11</xplan:schluessel>
      <xplan:text>Eine Überschreitung der Baugrenzen durch untergeordnete Bauteile, zum Beispiel durch Balkone, Erker, Loggien und Sichtschutzwände, kann bis zu einer Tiefe von 1,8 m zugelassen werden. Überschreitungen der Baugrenzen dürfen keine wesentliche Verschattung der benachbarten Nutzungen und der Umgebung bewirken. Auf der mit „(C)“ bezeichneten Fläche sind Überschreitungen der Baugrenze durch untergeordnete Bauteile nur ausnahmsweise zulässig. An den zur Norderelbe orientierten Fassaden ist eine Überschreitung der Baugrenzen durch untergeordnete Bauteile unzulässig. Auf der mit „(F)“ bezeichneten Fläche sind Überschreitungen der Baugrenzen an der Nord- und Ostfassade ab dem zweiten Obergeschoss bis zu einer Tiefe von 1 m allgemein zulässig. Eine Überbauung der Straßenverkehrsfläche ist oberhalb einer lichten Höhe von 4,3 m zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_4476acfb-1b28-4331-86c2-35777ed50404">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566013.591 5932735.450</gml:lowerCorner>
          <gml:upperCorner>566013.591 5932735.450</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_0855a411-8b3d-40bb-94c8-0f3f135e9c1b"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_ccf876ec-76d6-4927-959e-d1a9610a22e0">
          <gml:pos>566013.591 5932735.450</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1c629753-705c-4932-8b2f-d02d970dca5b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565991.363 5932754.255</gml:lowerCorner>
          <gml:upperCorner>565991.363 5932754.255</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_66c4cb50-e3a4-4a5c-8bca-eb16d64d237a"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_a0e158f1-bfc3-4f91-a68e-1c19c662f350">
          <gml:pos>565991.363 5932754.255</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_de62f70e-d6e5-4b50-9702-d329b6a93c04">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565705.170 5932779.206</gml:lowerCorner>
          <gml:upperCorner>565705.170 5932779.206</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>typ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_1048f9c7-e935-4593-bea0-46d9eee4d619"/>
      <xplan:schriftinhalt>Geh- und Fahrrecht</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_dae89360-38e5-46db-9673-6d02777375ab">
          <gml:pos>565705.170 5932779.206</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">350.00</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_bbd24b02-d2b0-4363-b215-0103b5f536a2">
      <xplan:schluessel>§2Nr.14</xplan:schluessel>
      <xplan:text>Auf den nicht überbauten Grundstücksflächen sind Nebenanlagen nur ausnahmsweise zulässig, wenn die Gestaltung der Freiflächen nicht beeinträchtigt ist.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_64b2b4f1-3b4d-4360-898e-f1fab051b1cf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565544.491 5932771.956</gml:lowerCorner>
          <gml:upperCorner>565544.491 5932771.956</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>typ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_cf427698-f8aa-42bf-a20c-4991bf24b981"/>
      <xplan:schriftinhalt>Gehrecht</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_87f5d92c-c987-4e53-a1d7-a438f68f5591">
          <gml:pos>565544.491 5932771.956</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">340.00</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_ab9d2ac8-1f4f-4f57-b54f-24b493c377c2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565766.875 5932730.653</gml:lowerCorner>
          <gml:upperCorner>565766.875 5932730.653</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>typ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_82185115-542d-40cd-b755-c39e54ce7c1a"/>
      <xplan:schriftinhalt>Gehrecht</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_08e3e2f1-e5ab-489b-a8cb-a55be8a6ec3a">
          <gml:pos>565766.875 5932730.653</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_48d1383b-f1d4-47c9-a7ae-ead916a60cb8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565919.942 5932718.490</gml:lowerCorner>
          <gml:upperCorner>565919.942 5932718.490</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>typ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_5f683752-d838-4f11-98d0-0c82c015814c"/>
      <xplan:schriftinhalt>Gehrecht</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_93714e1e-b941-436a-8330-a9572e727f0c">
          <gml:pos>565919.942 5932718.490</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_92fa22fc-2e8a-4c0c-96d3-6c36bf65273e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565998.434 5932694.046</gml:lowerCorner>
          <gml:upperCorner>565999.434 5932695.046</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_0a4866da-e023-4092-94aa-6c0213bec033"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_65135eca-61c0-48bd-9fff-864b08319bbf">
          <gml:pos>565998.434 5932694.046</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_908a9646-520c-456c-b448-91cf6f0ea225">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565653.871 5932766.211</gml:lowerCorner>
          <gml:upperCorner>565653.871 5932766.211</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_21f1e4ac-6237-479a-815d-b9d94bf441e0"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d0b7117b-a060-4e55-921e-408dc155170e">
          <gml:pos>565653.871 5932766.211</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_0004eb97-9458-4ec0-8d77-657bf88d18b8">
      <xplan:schluessel>§2Nr.7</xplan:schluessel>
      <xplan:text>Auf der mit „(E)“ bezeichneten Fläche sind bauliche oder technische Vorkehrungen zur passiven Belüftung an den Gebäuden erforderlich, um gesunde Arbeitsverhältnisse aufgrund der während der Liegezeit von Kreuzfahrtschiffen entstehenden Luftverunreinigungen zu gewährleisten.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3cf0aaf7-21ec-457f-9c96-f595380a9453">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565983.042 5932771.585</gml:lowerCorner>
          <gml:upperCorner>565983.042 5932771.585</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_4c2c00ad-a48b-4eda-adae-684835aa844c"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_570641d2-3bdb-4ea4-8b47-86000bb54af2">
          <gml:pos>565983.042 5932771.585</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f998ec7e-4496-452c-acef-fec212cd39c9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565585.800 5932743.948</gml:lowerCorner>
          <gml:upperCorner>565586.800 5932744.948</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_0b0e7af4-8435-4711-843b-9259559be42e"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_c8de7eb4-bf5e-43fb-9007-5040346efd0a">
          <gml:pos>565585.800 5932743.948</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">338.74</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ede3aab2-e41a-4569-8775-6280fe7115ce">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565728.237 5932772.471</gml:lowerCorner>
          <gml:upperCorner>565728.237 5932772.471</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_ea5cbdcf-d663-4b3d-a275-fa4d87cb7ac6"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_2ba6534e-7dcd-4d45-9f21-7c508d04413e">
          <gml:pos>565728.237 5932772.471</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7e373680-91fb-49a7-882f-71a0838de704">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565606.047 5932767.998</gml:lowerCorner>
          <gml:upperCorner>565606.047 5932767.998</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_8be64b5f-88ab-4f30-8b0d-a2e4f272ef4c"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_dc4760f7-3a71-4bff-b5d3-19f62ee5d887">
          <gml:pos>565606.047 5932767.998</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ba716834-b859-456d-a300-19d5e429ee2a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565803.496 5932792.000</gml:lowerCorner>
          <gml:upperCorner>565803.496 5932792.000</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_581669dd-b9d4-455b-99da-a78277533a4a"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_a59cfa0d-48c9-4df0-b45b-6716a2951895">
          <gml:pos>565803.496 5932792.000</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_0cb41294-2033-4a5a-a157-f7b249731d5d">
      <xplan:schluessel>§2Nr.10</xplan:schluessel>
      <xplan:text>Oberhalb der festgesetzten Vollgeschosse und der Gebäudehöhen (einschließlich einem möglichen Galeriegeschoss im Erdgeschoss) sind weitere Geschosse unzulässig. Technikgeschosse und technische oder erforderliche Aufbauten, wie Treppenräume, sind ausnahmsweise auch oberhalb der festgesetzten Vollgeschosse und Gebäudehöhen zulässig, wenn die Gestaltung des Gesamtbaukörpers und das Ortsbild nicht beeinträchtigt werden und diese keine wesentliche Verschattung der Nachbargebäude und der Umgebung bewirken. Aufbauten, deren Einhausung und Technikgeschosse sind mindestens 2,5 m von der Außenfassade zurückzusetzen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_a06eb58c-85b0-4c8e-8611-5cadb4bc4f9c">
      <xplan:schluessel>§2Nr.6</xplan:schluessel>
      <xplan:text>Durch geeignete bauliche Schallschutzmaßnahmen wie zum Beispiel Doppelfassaden, verglaste Vorbauten (zum Beispiel verglaste Loggien, Wintergärten), besondere Fensterkonstruktionen oder in ihrer Wirkung vergleichbare Maßnahmen ist sicherzustellen, dass durch diese baulichen Maßnahmen insgesamt eine Schallpegeldifferenz erreicht wird, die es ermöglicht, dass in Schlafräumen ein Innenraumpegel bei teilgeöffneten Fenstern von 30 dB(A) während der Nachtzeit (22.00 Uhr bis 6.00 Uhr) nicht überschritten wird. Erfolgt die bauliche Schallschutzmaßnahme in Form von verglasten Vorbauten, muss dieser Innenraumpegel bei teilgeöffneten Bauteilen erreicht werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ff170ce3-93ea-4eba-8a3c-2c857a97ca78">
      <xplan:schluessel>§2Nr.9</xplan:schluessel>
      <xplan:text>Die Oberkante des Fußbodens des ersten Obergeschosses muss auf mindestens 5 m und höchsten 6 m über der angrenzenden Geländeoberfläche liegen. Ausnahmsweise kann im Erdgeschoss eine Galerie eingebaut werden, wenn das Galeriegeschoss eine Grundfläche kleiner 50 vom Hundert (v. H.) der Grundfläche des Erdgeschosses einnimmt. Die Galerieebene muss einen Abstand von mindestens 4,5 m von der Innenseite der zu den öffentlichen Straßenverkehrsflächen und mit Gehrechten belegten Flächen gerichteten Außenfassade einhalten. Das Erdgeschoss einschließlich einem eventuell eingezogenen Galeriegeschoss wird als ein Vollgeschoss gewertet.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_42db0425-d7bd-44e4-97ae-c3f70ab80061">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565622.923 5932750.485</gml:lowerCorner>
          <gml:upperCorner>565622.923 5932750.485</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_a1436623-01a9-491f-8cf5-921ee8ebaeac"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_2e306db5-9ea4-422b-899b-6fc3ac4eabf1">
          <gml:pos>565622.923 5932750.485</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_50e2c425-aeed-4652-b5ad-ad25a8522993">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565929.133 5932750.254</gml:lowerCorner>
          <gml:upperCorner>565929.133 5932750.254</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_c477ec48-117a-47c3-ab34-98a809d7f950"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_1b9a635e-3887-4fa2-a011-c9e083bd28c7">
          <gml:pos>565929.133 5932750.254</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_6d1389a1-b1c0-460d-99d3-f792ecc96b5d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565988.404 5932747.291</gml:lowerCorner>
          <gml:upperCorner>565988.404 5932747.291</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_66c4cb50-e3a4-4a5c-8bca-eb16d64d237a"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_bc9e265f-9969-4f57-9710-0b4ced89664d">
          <gml:pos>565988.404 5932747.291</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_fb4823b8-5104-45b8-a1d7-46ee7c37cbb3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565577.059 5932790.614</gml:lowerCorner>
          <gml:upperCorner>565578.059 5932791.614</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_a2612630-e9fb-44d4-824a-96e371fb5885"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_6437969b-e0d3-47ed-a592-c75f8740eaa0">
          <gml:pos>565577.059 5932790.614</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_4a3632a9-9dfc-42b0-a651-e3f1f6513c54">
      <xplan:schluessel>§2Nr.13</xplan:schluessel>
      <xplan:text>Die festgesetzten Gehrechte umfassen die Befugnis der Freien und Hansestadt Hamburg, allgemein zugängige Gehwege anzulegen und zu unterhalten. Das festgesetzte Geh- und Fahrrecht umfasst die Befugnis der Freien und Hansestadt Hamburg, einen allgemein zugängigen Gehweg anzulegen und zu unterhalten, sowie die Befugnis der für die Unterhaltung der Kaianlagen sowie der Fußgänger- und Radfahrerbereiche zuständigen Stellen und die Befugnis der Nutzer der Marina im Grasbrookhafen, diese Flächen zu befahren. Geringfügige Abweichungen von den festgesetzten Gehrechten und dem festgesetzten Geh- und Fahrrecht sind zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_16d136cb-fe5f-4867-8a22-40850fa95ec7">
      <xplan:schluessel>§2Nr.17</xplan:schluessel>
      <xplan:text>An den Rändern der hochwassergefährdeten Bereiche sind zum Zwecke des Hochwasserschutzes soweit erforderlich zusätzliche besondere bauliche Maßnahmen vorzusehen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e23b69d9-b633-4b1b-b5c0-bd27b6b35d36">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565728.064 5932733.427</gml:lowerCorner>
          <gml:upperCorner>565729.064 5932734.427</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_6d5caa25-9659-4499-b41a-d2621f6f0eef"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_1f2f2960-16de-4b9b-8058-392b6b03f4e6">
          <gml:pos>565728.064 5932733.427</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_551efb2f-83bb-4f95-8b77-1c7a119e0216">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565892.132 5932683.837</gml:lowerCorner>
          <gml:upperCorner>565892.132 5932683.837</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_9056efbc-a820-4e0a-ab68-f710974bede8"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_15fd6de5-8740-4ae6-9b82-da34c42ff748">
          <gml:pos>565892.132 5932683.837</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_68e516b8-4840-44ef-8781-b632022bb1cf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566021.049 5932696.381</gml:lowerCorner>
          <gml:upperCorner>566021.049 5932696.381</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_bb35d750-2755-468c-92f2-6294b8d75f53"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_65df1d2a-e172-4cad-9568-1a2bd5b92d24">
          <gml:pos>566021.049 5932696.381</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_65d9180b-f37d-4a8e-acee-cd570aff9612">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565966.181 5932665.443</gml:lowerCorner>
          <gml:upperCorner>565966.181 5932665.443</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_e13588fe-85f5-41f0-946f-ca0fd7508555"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_f1a34339-c4ff-461f-b0cb-d84a33d1a00c">
          <gml:pos>565966.181 5932665.443</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_30ca1209-45fc-4a5e-a5df-c40f9cdd392f">
      <xplan:schluessel>§2Nr.24</xplan:schluessel>
      <xplan:text>Im Plangebiet sind bauliche Gassicherungsmaßnahmen vorzusehen, die sowohl Gasansammlungen unter den baulichen Anlagen und den befestigten Flächen als auch Gaseintritte in die baulichen Anlagen verhindern.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_01f7b9c6-f77f-4614-a6cb-bb1b25ff3b05">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565650.713 5932735.907</gml:lowerCorner>
          <gml:upperCorner>565650.713 5932735.907</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_143fcf9b-61c3-41ef-b23b-0cff3764dfe5"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_bffc2df7-db4c-4be2-880f-07cd5ece1af0">
          <gml:pos>565650.713 5932735.907</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_99928044-35dc-4174-a1aa-5427c205808e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565832.244 5932784.264</gml:lowerCorner>
          <gml:upperCorner>565832.244 5932784.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_581669dd-b9d4-455b-99da-a78277533a4a"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_7122e8ba-5702-46bb-a42c-aa4e6abe83bf">
          <gml:pos>565832.244 5932784.264</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_90ad5034-c656-4527-afa7-c81dff108352">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565973.494 5932721.423</gml:lowerCorner>
          <gml:upperCorner>565974.494 5932722.423</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_eca52c4c-9060-4cb7-80d6-c2d3d14e9eac"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_ef734434-0fd4-4b3a-9e37-45ff7206a8ac">
          <gml:pos>565973.494 5932721.423</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_4a13a610-a0c4-4175-b41d-e459290be6a2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565695.880 5932774.600</gml:lowerCorner>
          <gml:upperCorner>565695.880 5932774.600</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_0c68adac-f80c-4065-9073-a3ae6647885a"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_ef3c29e2-b6ed-4e98-a344-eca792845779">
          <gml:pos>565695.880 5932774.600</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d363b0ce-c578-4052-9546-96bc9b3f5042">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565831.803 5932745.484</gml:lowerCorner>
          <gml:upperCorner>565831.803 5932745.484</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_144e9d11-6d29-4852-ae13-8678467c56fc"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_b335de1d-c98a-4605-bd46-0c1652f55223">
          <gml:pos>565831.803 5932745.484</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d8a263dc-f61b-46ca-8369-d683e2bde216">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565821.890 5932694.752</gml:lowerCorner>
          <gml:upperCorner>565821.890 5932694.752</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_e22ef689-31a8-48e6-9f04-5c2cac0ddb9a"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_2e3cb70f-1b35-40ab-8876-3bb9029d052e">
          <gml:pos>565821.890 5932694.752</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0b71b288-182b-440f-b9dc-3f104e0b4add">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565570.360 5932769.065</gml:lowerCorner>
          <gml:upperCorner>565570.360 5932769.065</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_1df34f6c-7eac-4c7b-a47a-f9d8d028099f"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_915a5a3b-9bca-43df-98db-2a5a8fc526fc">
          <gml:pos>565570.360 5932769.065</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_937f8f81-340a-447d-bf09-5c8360c8b8d8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565714.656 5932737.764</gml:lowerCorner>
          <gml:upperCorner>565714.656 5932737.764</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_a6594fbf-407f-4351-b3ae-5af4f2c87b01"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_8eeb4a1b-03a2-4066-b24f-5d7a1b3a4b46">
          <gml:pos>565714.656 5932737.764</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_830cd12a-b58c-46d0-825e-643f9d4be8af">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565926.574 5932736.334</gml:lowerCorner>
          <gml:upperCorner>565926.574 5932736.334</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_67789f17-f7f2-4e6d-aac0-5de2b1797227"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_0512b873-3631-4164-86ca-c6ed374df5f8">
          <gml:pos>565926.574 5932736.334</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ca7ab734-78e3-4b47-839c-cedf3bbc7bcf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565916.287 5932748.760</gml:lowerCorner>
          <gml:upperCorner>565916.287 5932748.760</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_b0ea0abf-cc1e-415e-86ca-a5fdf28e75c5"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_6e0ec0a4-f9c3-4071-9203-f0dfa86f3525">
          <gml:pos>565916.287 5932748.760</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_50afe552-4272-4213-85fa-f8f8666a5f7a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565782.664 5932774.634</gml:lowerCorner>
          <gml:upperCorner>565783.664 5932775.634</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_581669dd-b9d4-455b-99da-a78277533a4a"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_406a5c46-4ffc-43bd-96b0-937342a61cf5">
          <gml:pos>565782.664 5932774.634</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d10aab1f-32af-42c1-9984-c0a8bcda02ad">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566007.408 5932810.826</gml:lowerCorner>
          <gml:upperCorner>566007.408 5932810.826</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_96abb9d8-6277-4e08-9a41-091df5de3360"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_7e299848-d45e-4059-ab9e-5f663094958d">
          <gml:pos>566007.408 5932810.826</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b67a57a6-6aec-4590-8ab2-a22e21ee7149">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565796.153 5932759.091</gml:lowerCorner>
          <gml:upperCorner>565796.153 5932759.091</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_b27cdc10-b8b2-4867-84a7-52781599ae44"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_91bf7ad2-9c7a-4df2-9ba5-e99ebf09ccfa">
          <gml:pos>565796.153 5932759.091</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>0.25</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_82c0e20c-f4eb-476e-bec2-04fd6d79bb0d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565960.709 5932684.052</gml:lowerCorner>
          <gml:upperCorner>565960.709 5932684.052</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>typ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_96425556-8eff-429f-9db5-ced4ae60516f"/>
      <xplan:schriftinhalt>Gehrecht</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_310ce7fc-57bc-4d67-914c-e2a31179c2b6">
          <gml:pos>565960.709 5932684.052</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_abc3741a-7af6-4d6d-b01c-2a73ecd6bd48">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565728.009 5932707.413</gml:lowerCorner>
          <gml:upperCorner>565728.009 5932707.413</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_2a1e9c3f-efe0-4cb8-9954-aa5969381a04"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_3d2c624c-ef7e-4007-ad53-f1bdbcf0bbf5">
          <gml:pos>565728.009 5932707.413</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_6af2b297-49f7-4b0a-883d-b73fab5571a9">
      <xplan:schluessel>§2Nr.1</xplan:schluessel>
      <xplan:text>Auf der mit „(A)“ bezeichneten Fläche sind Wohngebäude unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_453eb485-75cf-4d56-979a-25c8effab0f5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566011.038 5932670.948</gml:lowerCorner>
          <gml:upperCorner>566012.038 5932671.948</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_70ae082f-d9dc-4308-80a2-822c369d97d8"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_70134ebd-42b8-4741-ad4d-e615d848c37e">
          <gml:pos>566011.038 5932670.948</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_777152b5-d0b6-4da5-81bd-fde94f5716a0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565968.783 5932732.468</gml:lowerCorner>
          <gml:upperCorner>565968.783 5932732.468</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_a3ac24b6-73a1-42c3-9cea-6b94fa3744b8"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_77190f41-c8c4-49ad-b908-6f7153cc2582">
          <gml:pos>565968.783 5932732.468</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_e06f8260-990e-4f04-913b-1f54d14705c4">
      <xplan:schluessel>§2Nr.19</xplan:schluessel>
      <xplan:text>Auf den mit „(G)“ bezeichneten Flächen sind für je 150 m² dieser Flächen ein kleinkroniger Baum oder für je 300 m² dieser Flächen ein großkroniger Baum zu pflanzen. Sofern Bäume auf Tiefgaragen angepflanzt werden, muss auf einer Fläche von mindestens 16 m² je Baum die Stärke des durchwurzelbaren Substrataufbaus mindestens 80 cm betragen. Für festgesetzte Anpflanzungen sind standortgerechte Laubbäume zu verwenden. Großkronige Bäume müssen einen Stammumfang von mindestens 18 cm, kleinkronige Bäume einen Stammumfang von mindestens 14 cm, in 1 m Höhe über dem Erdboden gemessen, aufweisen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_39484140-e5a1-448e-aaba-4e905abfd378">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565665.960 5932773.036</gml:lowerCorner>
          <gml:upperCorner>565665.960 5932773.036</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>typ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_96b54c5a-bd3b-4a36-b719-2b6d986ed052"/>
      <xplan:schriftinhalt>Gehrecht</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_c8f67921-3c76-438b-9e2d-3b9fad5fc768">
          <gml:pos>565665.960 5932773.036</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_89c273d6-7f81-4b54-a29d-aa469ed8d6df">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565909.441 5932779.808</gml:lowerCorner>
          <gml:upperCorner>565909.441 5932779.808</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>text</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_fb8ec91b-8bb3-492d-afd1-9d8323e9d38c"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_66efc73c-f505-4fb9-ba87-e1e91447b273">
          <gml:pos>565909.441 5932779.808</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">15.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c9f52069-f701-446d-9208-c83f72dce875">
      <xplan:schluessel>§2Nr.15</xplan:schluessel>
      <xplan:text>Im Mischgebiet sind für Einfriedigungen nur Hecken oder durchbrochene Zäune in Verbindung mit Hecken bis zu einer Höhe von 1,2 m zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_4a0b9fc1-6bf2-4394-a004-c5a756a62df1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565757.270 5932690.729</gml:lowerCorner>
          <gml:upperCorner>565757.270 5932690.729</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_41c4f008-fd82-4bd9-9a0b-4020ff16c7bc"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d38fbb5d-8e79-4171-980e-7444388adc38">
          <gml:pos>565757.270 5932690.729</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_51e6e5f3-b8ca-4a73-8d1e-70c2410388c9">
      <xplan:schluessel>§2Nr.16</xplan:schluessel>
      <xplan:text>Gebäude mit zentraler Warmwasserversorgung sind durch Anlagen erneuerbarer Energien zu versorgen, die 30 v. H. oder höhere Anteile des zu erwartenden Jahreswarmwasserbedarfs decken. Im begründeten Einzelfall können geringe Abweichungen aus gestalterischen, funktionalen oder technischen Gründen zugelassen werden. Elektrische Wärmepumpen sind nur zulässig, wenn sie mit Strom aus erneuerbaren Energien betrieben werden. Dezentrale Warmwasseranlagen sind nur dort zulässig, wo der tägliche Warmwasserbedarf bei 60 Grad Celsius weniger als 1 Liter je m² Nutzfläche beträgt. Diese Anforderung nach den Sätzen 1 und 2 kann ausnahmsweise auch durch den Abschluss eines langjährigen Vertrages über die Lieferung von Brauchwarmwasser mit dem von der Freien und Hansestadt Hamburg ausgewählten Wärmelieferanten erfüllt werden; für die Vertragsdauer gelten die Anforderungen der Sätze 1 bis 3 dann als erfüllt. Für die Beheizung und die Bereitstellung des übrigen Warmwasserbedarfs ist die Neubebauung an ein Wärmenetz in Kraft-Wärme-Kopplung anzuschließen, sofern nicht Brennstoffzellen zur ausschließlichen Wärme- und Warmwasserversorgung eingesetzt werden. Vom Anschluss- und Benutzungsgebot nach den Sätzen 1 bis 6 kann auf Antrag befreit werden, wenn die Erfüllung der Anforderungen im Einzelfall wegen besonderer Umstände zu einer unbilligen Härte führen würde. Die Befreiung soll zeitlich befristet werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_bddf163c-f007-4fe7-8fc7-71d7cb8d11c1">
      <xplan:schluessel>§2Nr.12</xplan:schluessel>
      <xplan:text>Im Mischgebiet sind notwendige Stellplätze nur in Tiefgaragen oder Garagengeschossen unterhalb der Höhe von 8 m über Normalhöhennull (NHN) zulässig. Geringfügige Abweichungen sind zulässig, wenn sie durch abweichende Straßenanschlusshöhen von über 8 m über NHN begründet sind.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_bd1d438f-771d-41b2-8298-9d40bb4e44b2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565975.675 5932679.148</gml:lowerCorner>
          <gml:upperCorner>565976.675 5932680.148</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_b6aaa0d7-dd24-41d6-bdaa-9ef9cebdd678"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_de1a306d-51fd-4d63-9651-ed9507153f3f">
          <gml:pos>565975.675 5932679.148</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_fb160f7b-ec24-48db-8e61-90794f6249c4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565795.430 5932746.696</gml:lowerCorner>
          <gml:upperCorner>565795.430 5932746.696</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_88098600-5cd2-431b-865c-7c79f14264f8"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_1320a7c1-57f0-43a3-ae73-71737dc22d72">
          <gml:pos>565795.430 5932746.696</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>0.25</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2c25e0f3-e0ff-48e5-ad01-3451f7876e91">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565802.589 5932717.483</gml:lowerCorner>
          <gml:upperCorner>565802.589 5932717.483</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_3616e3dd-c0df-416a-97aa-56660f5b223f"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_dddb6a4c-8886-4342-bbae-19014c81a700">
          <gml:pos>565802.589 5932717.483</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_a2100ab4-cb15-434a-958e-c3ff29115600">
      <xplan:schluessel>§2Nr.20</xplan:schluessel>
      <xplan:text>Die mit „(H)“ bezeichneten Dachflächen sind mit einem Anteil von mindestens 40 v. H. mit einem mindestens 50 cm starken durchwurzelbaren Substrataufbau zu begrünen. Die mit „(I)“ bezeichneten Dachflächen sind mit Ausnahme der gemäß Nummer 10 zulässigen Anlagen und technischen Aufbauten zu mindestens 30 v. H. mit einem mindestens 15 cm starken durchwurzelbaren Substrataufbau extensiv mit standortangepassten Stauden und Gräsern zu begrünen. Darüber hinaus müssen mindestens 20 v. H. mit einem mindestens 25 cm starken Substrataufbau intensiv mit Stauden und Sträuchern begrünt werden. Alle Dachbegrünungen sind dauerhaft zu erhalten.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_f0accd68-1dde-4113-8e4d-854790f72749">
      <xplan:schluessel>§2Nr.5</xplan:schluessel>
      <xplan:text>Im Mischgebiet sind Gartenbaubetriebe, Tankstellen und Vergnügungsstätten unzulässig. Ausnahmen für Vergnügungsstätten werden ausgeschlossen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_06fc76d6-d678-440e-a298-76c3bddb53d4">
      <xplan:schluessel>§2Nr.21</xplan:schluessel>
      <xplan:text>Die zu den Straßenverkehrsflächen, den Gehrechten und den mit „(G)“ bezeichneten Flächen gerichteten Fassaden sind in hellen Materialien oder Glas auszuführen. Für Fassaden der Gebäude mit einer Gebäudehöhe oder Höhe baulicher Anlagen von 68 m über NHN als Höchstmaß sowie von 60 bis 63 m über NHN als Mindest- und Höchstmaß gilt das auf allen Seiten. Untergeordnete Fassadenanteile können in anderen Materialien ausgeführt werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_8bd91d56-9ff8-4c04-917e-fd683afc0d7b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565929.585 5932744.169</gml:lowerCorner>
          <gml:upperCorner>565929.585 5932744.169</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_c477ec48-117a-47c3-ab34-98a809d7f950"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_999ff079-ce4b-4d9c-a4ea-b430876b1a1b">
          <gml:pos>565929.585 5932744.169</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2eee5e20-893c-435e-b044-05ebc2b71856">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565728.438 5932701.337</gml:lowerCorner>
          <gml:upperCorner>565728.438 5932701.337</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_2a1e9c3f-efe0-4cb8-9954-aa5969381a04"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_5a87d682-de8d-4fcc-a541-620552c3f56a">
          <gml:pos>565728.438 5932701.337</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0ae6b5a2-b9e4-4452-b901-0d170840c34b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565987.940 5932769.844</gml:lowerCorner>
          <gml:upperCorner>565987.940 5932769.844</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_30d3e1fa-b1fa-42fe-994f-d617658cb412"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_c9348968-c228-43bb-8a77-dec5be13d999">
          <gml:pos>565987.940 5932769.844</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_181e563a-bc73-4e7a-b5d4-fc177f151dcb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565768.192 5932741.683</gml:lowerCorner>
          <gml:upperCorner>565768.192 5932741.683</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_41f6b2c8-5bfb-46b3-8d00-4736a3ecd98c"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_c352c64f-f1ff-4c2d-a8d6-8ca31a171dd0">
          <gml:pos>565768.192 5932741.683</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b6f96831-804d-46d8-812e-7386edb6b998">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565618.861 5932744.376</gml:lowerCorner>
          <gml:upperCorner>565618.861 5932744.376</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_a1436623-01a9-491f-8cf5-921ee8ebaeac"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_2e6dd142-9ad3-4cf4-8664-810ae52dfa64">
          <gml:pos>565618.861 5932744.376</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_4cdf9d17-eccd-4d8a-aeaf-6ab702f69cd3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565991.533 5932788.995</gml:lowerCorner>
          <gml:upperCorner>565991.533 5932788.995</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>typ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_1971f8c0-ca44-4fbe-8081-2b5d48521d35"/>
      <xplan:schriftinhalt>Gehrecht</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_efb0bf88-b6fa-4ee0-b915-a13280982e04">
          <gml:pos>565991.533 5932788.995</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_bc65c102-ac8d-4045-926d-9e3c64c7cf7c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565569.109 5932775.133</gml:lowerCorner>
          <gml:upperCorner>565569.109 5932775.133</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_1df34f6c-7eac-4c7b-a47a-f9d8d028099f"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_78a58d33-02db-434b-b193-7cab05aa74f4">
          <gml:pos>565569.109 5932775.133</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_86c3b748-ca37-4c2e-b5c2-a91edf11d069">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565895.197 5932691.677</gml:lowerCorner>
          <gml:upperCorner>565895.197 5932691.677</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_092dc747-9a4a-4bf2-8e13-acda57503fde"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_155ffeb6-03c0-445b-8fe5-ec2384f9b5f4">
          <gml:pos>565895.197 5932691.677</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d541f15d-f012-40d2-b758-9d7d934d30f1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565969.139 5932726.382</gml:lowerCorner>
          <gml:upperCorner>565969.139 5932726.382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_a3ac24b6-73a1-42c3-9cea-6b94fa3744b8"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d829476d-9232-4770-a944-98af6562b655">
          <gml:pos>565969.139 5932726.382</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e7f6dbbf-73c0-41cb-a6fe-5087f03b261b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565702.224 5932757.416</gml:lowerCorner>
          <gml:upperCorner>565702.224 5932757.416</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_b83adf42-cb7d-4fc9-8fc5-ac15e82ee985"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_0ac61faf-7c09-4c6d-ab0c-a580c29c57eb">
          <gml:pos>565702.224 5932757.416</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ec0331a4-a665-42e5-a0ef-d1ff1a682525">
      <xplan:schluessel>§2Nr.22</xplan:schluessel>
      <xplan:text>Auf der mit „(K)“ bezeichneten Fläche ist oberhalb von 34,5 m über NHN nur eine Dachkonstruktion bis zur Höhe von 41 m über NHN zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_23c28af5-0f5f-40dc-867c-7d5352ac6485">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565915.606 5932682.587</gml:lowerCorner>
          <gml:upperCorner>565915.606 5932682.587</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_c5e9d7b7-c8e2-4fea-8add-e4e247eebb00"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_c27c7e73-b56f-44da-84ac-00509dc95b7b">
          <gml:pos>565915.606 5932682.587</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e0c24079-67a8-4ec3-925a-d14962367962">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565865.966 5932729.450</gml:lowerCorner>
          <gml:upperCorner>565866.966 5932730.450</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>text</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_0b0e7af4-8435-4711-843b-9259559be42e"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_dd5e8d7b-29c3-4fb1-a301-ede9f81c4a9d">
          <gml:pos>565865.966 5932729.450</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">280.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_4a1f49fc-d304-479d-aa04-5bff6e28ca34">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565601.098 5932769.700</gml:lowerCorner>
          <gml:upperCorner>565601.098 5932769.700</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_d728f5c8-f2e2-48aa-8d95-0e48ce88e6a8"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_05b8e9bd-f8c2-43dc-ab55-2baf82ef75f0">
          <gml:pos>565601.098 5932769.700</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9979086a-0c74-4d14-9e3c-b3028b821559">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565894.787 5932697.756</gml:lowerCorner>
          <gml:upperCorner>565894.787 5932697.756</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_092dc747-9a4a-4bf2-8e13-acda57503fde"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_21add895-051f-4b78-9c70-2fcf142b9bc7">
          <gml:pos>565894.787 5932697.756</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_9c11408b-f22f-4643-b0f5-cc08ffc810b3">
      <xplan:schluessel>§2Nr.8</xplan:schluessel>
      <xplan:text>Tiefgaragen sind außerhalb der überbaubaren Grundstücksflächen zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e4100735-5422-4ec9-b598-3577a4179daa">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565617.627 5932780.041</gml:lowerCorner>
          <gml:upperCorner>565617.627 5932780.041</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_f958a30d-b144-4701-9567-b57e96729735"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_56fe8a30-4348-4f48-a55f-a1528e6b3366">
          <gml:pos>565617.627 5932780.041</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_6921793d-c644-49dc-a6e3-166992db081f">
      <xplan:schluessel>§2Nr.2</xplan:schluessel>
      <xplan:text>In den Erdgeschossen sind Wohnungen unzulässig. Auf der mit „(B)“ bezeichneten Fläche sind Wohnungen in den Erdgeschossen ausnahmsweise zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_50360564-32c3-4aec-9737-b61c5161debd">
      <xplan:schluessel>§2Nr.23</xplan:schluessel>
      <xplan:text>Auf der mit „(L)“ bezeichneten Fläche ist die Überschreitung der festgesetzten Gebäudehöhe von 34,5 m über NHN durch die Fassade bis zu 1,2 m zulässig, sofern die Überschreitung in transparentem Material (Folie / Glas) ausgeführt wird. Die Materialvorgabe bezieht sich nicht auf die notwendige Tragkonstruktion.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d3a25e72-ea8b-4056-b939-b3cc1e840c7e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565656.833 5932773.083</gml:lowerCorner>
          <gml:upperCorner>565656.833 5932773.083</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_21f1e4ac-6237-479a-815d-b9d94bf441e0"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_0f98fdb8-1a8e-4092-acf2-f72ebdb85bea">
          <gml:pos>565656.833 5932773.083</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a135caf0-72ff-4ac7-82cd-abbdc9455c25">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566010.438 5932678.415</gml:lowerCorner>
          <gml:upperCorner>566010.438 5932678.415</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_a7030dc5-ac76-4f12-be82-0b7662618159"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_e1d8c33c-0c4d-4205-bdad-739a27a8cbb1">
          <gml:pos>566010.438 5932678.415</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_Rasterdarstellung gml:id="GML_b3ebb4e7-aa05-4cf3-b431-8605d0c517eb">
      <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>http://example.org/BPlan003_5-2.pgw</xplan:georefURL>
          <xplan:referenzURL>http://example.org/BPlan003_5-2.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
    </xplan:XP_Rasterdarstellung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_059ebc29-e9a5-4917-acd3-55ba7eef6988">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565805.553 5932785.956</gml:lowerCorner>
          <gml:upperCorner>565805.553 5932785.956</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_581669dd-b9d4-455b-99da-a78277533a4a"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_e57f3b73-5e5b-4306-a5af-08800db73f1b">
          <gml:pos>565805.553 5932785.956</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_6aa02183-2b23-40a0-88b4-a8dc4b8ac248">
      <xplan:schluessel>§2Nr.3</xplan:schluessel>
      <xplan:text>Auf der mit „(C)“ bezeichneten Fläche sind ab dem zweiten Obergeschoss nur Wohnungen zulässig. Auf der mit „(C)“ bezeichneten Fläche dürfen maximal 11.500 m2 Geschossfläche errichtet werden. Auf den mit „(D)“ bezeichneten Flächen sind ab dem ersten Obergeschoss nur Wohnungen zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_8676c057-52ed-484e-abdd-7f633473dc3f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565909.441 5932779.808</gml:lowerCorner>
          <gml:upperCorner>565909.441 5932779.808</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zugunstenVon</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_d4750868-0f48-4b92-a24c-217a691c2d92"/>
      <xplan:dientZurDarstellungVon xlink:href="#GML_fb8ec91b-8bb3-492d-afd1-9d8323e9d38c"/>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_6b63cf2e-302b-429f-8186-d819a10fbd5c">
          <gml:pos>565909.441 5932779.808</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">15.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
</xplan:XPlanAuszug>
