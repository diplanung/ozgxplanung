﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_62BBF1FB-FE3B-4527-B9E9-5E8DE27A4B16" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/2 http://www.xplanungwiki.de/upload/XPlanGML/5.2/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/5/2">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>568723.267 5937461.39</gml:lowerCorner>
      <gml:upperCorner>568902.866 5937658.494</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="Gml_AE0CA885-7C38-47E2-AEE5-6708776B3ECA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>568723.267 5937461.39</gml:lowerCorner>
          <gml:upperCorner>568902.866 5937658.494</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BP_5.2-geometricErrorSelbstueberschneidung_SoapUI-XPlanValidatorAPI</xplan:name>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_7A6594F5-85E3-47BA-B509-57DDFF4E0BDF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B7279196-12E3-4F90-8B1F-2C4CD81A79BA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568892.152 5937464.247 568900.68 5937492.909 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_01A47FC0-1AA8-4E37-9469-BD9984934C8D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568900.68 5937492.909 568902.866 5937500.258 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_7C0140C7-D9A7-44DA-BF25-CCAE182A3698" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">568902.866 5937500.258 568899.450163407 5937510.03930246 568898.166 5937520.32 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1932AB86-5EE4-4816-B92E-EA9AE4D189A4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568898.166 5937520.32 568896.97 5937586.037 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_77F03A6E-A50A-4C6B-99F6-7C69C28928C4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568896.97 5937586.037 568893.002 5937589.132 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_24D0029A-E02E-41A0-8444-3684F63F68DA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568893.002 5937589.132 568875.428 5937602.837 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6B427F34-5972-4E8A-AE68-9FAD3B731C02" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568875.428 5937602.837 568882.531 5937636.883 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8B37E715-2AEB-4162-B419-A8BD9A4BF9FD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568882.531 5937636.883 568879.637 5937640.958 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EFA363BD-1F9A-41DE-9205-BD4811E11A30" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568879.637 5937640.958 568745.595 5937658.494 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1C993446-641B-430A-B185-D188A751280E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568745.595 5937658.494 568741.667 5937655.403 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F6CBBF09-7B52-4205-8C95-AEBEF28C69E4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568741.667 5937655.403 568732.412 5937569.454 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_98814370-383A-4CA9-B987-4EA5910870F4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568732.412 5937569.454 568732.332 5937568.722 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_30610A1E-7910-45F6-B4C8-9DC15342CD87" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568732.332 5937568.722 568723.267 5937512.77 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9EEFD20F-9195-4192-8812-915998CAFEC1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568723.267 5937512.77 568725.393 5937509.383 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_081FD86D-12BA-4A57-AE53-F1BEA5BE8C8E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568725.393 5937509.383 568756.173 5937500.237 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_93B8E1B9-94FB-40A4-9FD7-AE22D7548D7B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568756.173 5937500.237 568812.636 5937483.455 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_57CEE049-AC71-45D6-87E7-A806BD56A664" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568812.636 5937483.455 568826.563 5937479.316 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_983ED081-646D-4E3D-84AC-1EE4346EEF64" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568826.563 5937479.316 568842.205 5937474.668 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_29E246C7-4D60-47CB-A6F8-4C991C58783C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568842.205 5937474.668 568858 5937469.974 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6B11CD77-7F76-42A8-8961-0A29859725DA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568858 5937469.974 568886.878 5937461.39 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9F3980A8-3D43-4188-AEA6-A188D526F465" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568886.878 5937461.39 568892.152 5937464.247 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>012</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:bereich xlink:href="#Gml_DECAC275-0BCF-4160-B654-86833CEA9C65" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="Gml_DECAC275-0BCF-4160-B654-86833CEA9C65">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>568723.267 5937461.39</gml:lowerCorner>
          <gml:upperCorner>568902.866 5937658.494</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_096EDACA-364E-46A0-BB59-DB9B6F156C7C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_94B51FAD-F1BB-4535-BC3D-64CF1A3C1F23" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568892.152 5937464.247 568900.68 5937492.909 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ED191CCC-7633-4235-9F50-E862C144541D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568900.68 5937492.909 568902.866 5937500.258 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_87ABB293-666F-419B-803F-0DBADD8501FB" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">568902.866 5937500.258 568899.450163407 5937510.03930246 568898.166 5937520.32 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B132337A-8E1D-403F-8FF9-50F07A5B14AA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568898.166 5937520.32 568896.97 5937586.037 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_02F2E5E6-AFEE-4C15-A32C-516CD8FFFFD5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568896.97 5937586.037 568893.002 5937589.132 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C51D2C3B-DA94-482F-8AE7-9E24767346F2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568893.002 5937589.132 568875.428 5937602.837 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7BD28359-DE3A-4162-992B-8F208BEFDF83" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568875.428 5937602.837 568882.531 5937636.883 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_43E2B7FB-0F81-4E98-AADE-D6695C82A0A8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568882.531 5937636.883 568879.637 5937640.958 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3C428B17-3DBF-4AF4-8484-9FF179EB7588" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568879.637 5937640.958 568745.595 5937658.494 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A3F222DF-B187-47D5-8DB0-70EEC145BD9F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568745.595 5937658.494 568741.667 5937655.403 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E01DFE78-6A86-4FD2-8CA5-34EADAC16730" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568741.667 5937655.403 568732.412 5937569.454 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3FA905F1-FE9C-48F2-960A-7CAD5D4C5A8D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568732.412 5937569.454 568732.332 5937568.722 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F39E8B99-4F80-4946-A4DA-E10FBD9A3D77" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568732.332 5937568.722 568723.267 5937512.77 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_014030E3-E9FC-4491-895E-396B0C8CFBC5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568723.267 5937512.77 568725.393 5937509.383 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_57524EE7-64A7-4FD4-929B-F1EC641F4867" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568725.393 5937509.383 568756.173 5937500.237 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_905C5641-58FE-4CBB-8106-D851E5922621" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568756.173 5937500.237 568812.636 5937483.455 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_14E2FB0D-8095-481D-88F0-E2BE234CE956" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568812.636 5937483.455 568826.563 5937479.316 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FE6737B2-031F-47DC-977B-3ADC23608A99" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568826.563 5937479.316 568842.205 5937474.668 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7A588BEA-8A96-4F91-8DEF-2AB1FAC7373B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568842.205 5937474.668 568858 5937469.974 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_529B7BF2-4936-4DF3-B99E-B09FE29E9F68" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568858 5937469.974 568886.878 5937461.39 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_95AC46DF-A33A-4D60-ABDF-BED9B1348063" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568886.878 5937461.39 568892.152 5937464.247 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#Gml_3659A188-A511-4199-B2CF-AA70DE548A4F" />
      <xplan:planinhalt xlink:href="#Gml_3A4E3526-DB32-4115-AFFD-45236FB2D060" />
      <xplan:planinhalt xlink:href="#Gml_6E962628-467C-4481-BA88-D5012ED0213C" />
      <xplan:planinhalt xlink:href="#Gml_C7986975-B702-46BF-9669-E8A2794BCF23" />
      <xplan:gehoertZuPlan xlink:href="#Gml_AE0CA885-7C38-47E2-AEE5-6708776B3ECA" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_3659A188-A511-4199-B2CF-AA70DE548A4F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>568732.412 5937520.32</gml:lowerCorner>
          <gml:upperCorner>568898.166 5937658.494</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_DECAC275-0BCF-4160-B654-86833CEA9C65" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_30493664-4A23-4C89-A601-EBFDACF7F99B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">568896.97 5937586.037 568893.002 5937589.132 568875.428 5937602.837 
568882.531 5937636.883 568879.637 5937640.958 568745.595 5937658.494 
568741.667 5937655.403 568732.412 5937569.454 568750.426 5937566.457 
568759.391 5937567.691 568760.753 5937557.794 568761.462 5937552.642 
568764.718 5937528.972 568824.763 5937525.792 568829.073 5937526.384 
568868.352 5937526.183 568898.166 5937520.32 568896.97 5937586.037 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_3A4E3526-DB32-4115-AFFD-45236FB2D060">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>568867.7537 5937492.909</gml:lowerCorner>
          <gml:upperCorner>568902.866 5937526.183</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_DECAC275-0BCF-4160-B654-86833CEA9C65" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_689E4859-7C7F-4903-A104-E7A9D897818F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_7B04A1C8-6AA1-4B8D-9EFF-CA5B6DCB2695" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">568902.866 5937500.258 568899.450163406 5937510.03930246 568898.166 5937520.32 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A8E978A2-C71B-41F1-AF68-4AD331F85CAA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568898.166 5937520.32 568868.352 5937526.183 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_59144AE9-E441-450C-BACC-243BF8FF6A6B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568868.352 5937526.183 568867.7537 5937501.7688 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_594EF67B-CB8A-493C-9356-EB2ED232AF16" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568867.7537 5937501.7688 568875.954 5937499.733 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EB15A1C7-38BC-46AC-BE41-D8BB31F03E9E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568875.954 5937499.733 568887.156 5937496.743 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_39325939-E52B-4969-BEE9-06FF656216B1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568887.156 5937496.743 568900.68 5937492.909 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0199FEF1-9691-4430-8125-E2B6FEFBEFBD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">568900.68 5937492.909 568902.866 5937500.258 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_6E962628-467C-4481-BA88-D5012ED0213C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>568859.5134 5937486.708</gml:lowerCorner>
          <gml:upperCorner>568867.7537 5937503.5039</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_DECAC275-0BCF-4160-B654-86833CEA9C65" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_825FB781-8953-41D7-9164-46141A3DE49F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">568867.7537 5937501.7688 568859.5134 5937503.5039 568862.978 5937486.708 
568867.7537 5937501.7688 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_C7986975-B702-46BF-9669-E8A2794BCF23">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>568723.267 5937461.39</gml:lowerCorner>
          <gml:upperCorner>568900.68 5937569.454</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_DECAC275-0BCF-4160-B654-86833CEA9C65" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_06C75E89-F30C-4D92-A7A6-31274E4A607C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">568867.7537 5937501.7688 568868.352 5937526.183 568829.073 5937526.384 
568824.763 5937525.792 568764.718 5937528.972 568761.462 5937552.642 
568760.753 5937557.794 568759.391 5937567.691 568750.426 5937566.457 
568732.412 5937569.454 568732.332 5937568.722 568723.267 5937512.77 
568725.393 5937509.383 568756.173 5937500.237 568812.636 5937483.455 
568826.563 5937479.316 568842.205 5937474.668 568858 5937469.974 
568886.878 5937461.39 568892.152 5937464.247 568900.68 5937492.909 
568887.156 5937496.743 568875.954 5937499.733 568867.7537 5937501.7688 
568862.978 5937486.708 568859.5134 5937503.5039 568867.7537 5937501.7688 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
</xplan:XPlanAuszug>