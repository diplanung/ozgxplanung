﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_1DC1447B-EF7E-4738-B595-81F54C118BDE" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/4 http://www.xplanungwiki.de/upload/XPlanGML/5.4/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/5/4">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>566778.099 5932374.896</gml:lowerCorner>
      <gml:upperCorner>567509.287 5932905.509</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_7e324fed-7859-448d-a7b6-7ba341b6b0f9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566839.305 5932624.6</gml:lowerCorner>
          <gml:upperCorner>567509.287 5932905.509</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>HafenCity11_SoapUI-XPlanManagerAPI</xplan:name>
      <xplan:beschreibung>Der Bebauungsplan HafenCity 11 für das Gebiet am
Nordufer des Baakenhafens (Bezirk Hamburg-Mitte, Ortsteil
104) wird festgestellt.
Das Gebiet wird wie folgt begrenzt:
Über die Flurstücke 2348 (alt: 2236 – Pfeilerbahn), 1635,
1637 (Versmannstraße) und 2367 (alt: 1639) der Gemarkung
Altstadt Süd – Baakenhafen – über das Flurstück 2367,
Westgrenze des Flurstücks 2361, über das Flurstück 2358
(alt: 1021 – Versmannstraße), Nordgrenzen der Flurstücke
2358 und 1636 (Versmannstraße), über das Flurstück 1634
(Versmannstraße) der Gemarkung Altstadt Süd.</xplan:beschreibung>
      <xplan:technHerstellDatum>2017-03-29</xplan:technHerstellDatum>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_47AB942A-41F9-45A9-BE54-7F44E6187BC9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567071.225 5932875.978 566952.478 5932905.509 566941.899 5932887.112 
566926.109 5932859.655 566925.011 5932857.747 566850.623 5932868.84 
566849.428 5932859.158 566848.719 5932853.417 566848.591 5932852.383 
566842.603 5932805.784 566841.516 5932797.098 566839.305 5932779.633 
566885.545 5932770.145 566933.255 5932760.356 567084.333 5932725.96 
567189.373 5932700.56 567374.73 5932651.63 567470.445 5932624.6 
567476.043 5932642.107 567477.061 5932645.289 567475.978 5932645.649 
567489.76 5932696.417 567494.443 5932713.666 567502.35 5932743.169 
567509.287 5932769.105 567416.044 5932790.244 567354.506 5932803.777 
567322.897 5932810.727 567243.981 5932831.427 567071.225 5932875.978 567071.225 5932875.978 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#GML_915778b2-65ac-472f-8c81-7fd17d57d1c1" />
      <xplan:texte xlink:href="#GML_1f136ab2-8263-4d60-ba4f-b7b272e78b13" />
      <xplan:texte xlink:href="#GML_ca5005b0-4c55-4a51-9793-8306a404f448" />
      <xplan:texte xlink:href="#GML_dc728bae-a4dc-4bc3-a602-f2b4837e1263" />
      <xplan:texte xlink:href="#GML_d99d7f46-18c7-4b7c-87b4-0660884f7022" />
      <xplan:texte xlink:href="#GML_34198f6f-76f9-40b3-9018-c111ad67d822" />
      <xplan:texte xlink:href="#GML_c3000662-4308-41e3-852c-d74e90c60818" />
      <xplan:texte xlink:href="#GML_1b203c2f-d6ba-43d3-b73e-3cf67a00bb26" />
      <xplan:texte xlink:href="#GML_3c2d1544-a26d-4788-a7ce-736aff669377" />
      <xplan:texte xlink:href="#GML_198c3872-987b-4a3a-b836-96b84cbea024" />
      <xplan:texte xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:texte xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:texte xlink:href="#GML_d2b1d8bb-169a-4ed0-b6dc-e310692d3257" />
      <xplan:texte xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:texte xlink:href="#GML_24813ba0-c9b0-4655-9ccd-42baad49dcbf" />
      <xplan:texte xlink:href="#GML_967d2f7e-c198-4539-96a9-85ed0a81d4a2" />
      <xplan:texte xlink:href="#GML_7f160780-9db6-4be1-8ed3-11df2d255806" />
      <xplan:texte xlink:href="#GML_10a2fbc6-3805-4691-b013-61905c99ede3" />
      <xplan:texte xlink:href="#GML_27e1d5c5-2e37-42be-b0be-bf85a23883d5" />
      <xplan:texte xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:texte xlink:href="#GML_b0540621-d713-4e0d-8f09-9f70e1771e40" />
      <xplan:texte xlink:href="#GML_60885b8d-ed98-44c0-859b-e937a34d45ab" />
      <xplan:texte xlink:href="#GML_c3cb239b-32bc-4361-9e35-d4869768a4a6" />
      <xplan:texte xlink:href="#GML_de927319-5336-47ea-9a54-270bedb07ca7" />
      <xplan:texte xlink:href="#GML_4360595b-e82a-4207-84b1-713aaefab5da" />
      <xplan:texte xlink:href="#GML_1f6cf107-2977-4732-9401-3a469bfe9bca" />
      <xplan:texte xlink:href="#GML_b2a6f7ae-3d3c-462b-a0a7-3a3dc7d6bffd" />
      <xplan:texte xlink:href="#GML_bd4953ce-ce43-47f3-90c8-f8b4cec45f43" />
      <xplan:texte xlink:href="#GML_10f35421-3c0d-4867-9624-93d58f4a85be" />
      <xplan:texte xlink:href="#GML_a9f0e386-df8b-448a-aba5-b99a9e17fb19" />
      <xplan:texte xlink:href="#GML_25a71fae-9279-4085-a124-4d507aa5e6ab" />
      <xplan:texte xlink:href="#GML_ecdadf5a-49dd-4414-8002-6294054f6e98" />
      <xplan:texte xlink:href="#GML_3b43fd0e-dc9d-4c85-b8ed-d6a0086631fd" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>104</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2014-12-23</xplan:rechtsverordnungsDatum>
      <xplan:versionBauNVOText>Version vom 11.06.2013</xplan:versionBauNVOText>
      <xplan:bereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_915778b2-65ac-472f-8c81-7fd17d57d1c1">
      <xplan:schluessel>§2 Nr.27</xplan:schluessel>
      <xplan:text>In den Mischgebieten ist der Erschütterungsschutz der
Gebäude durch bauliche oder technische Maßnahmen
(zum Beispiel an Wänden, Decken und Fundamenten)
so sicherzustellen, dass die Anhaltswerte der DIN 4150
(Erschütterungen im Bauwesen), Teil 2 (Einwirkungen auf
Menschen in Gebäuden), Tabelle 1, Zeile 3 (Mischgebiete
nach BauNVO) eingehalten werden. Zusätzlich ist durch
die baulichen und technischen Maßnahmen zu gewährleisten,
dass der sekundäre Luftschall die Immissionsrichtwerte
der Technischen Anleitung zum Schutz gegen Lärm
vom 26. August 1998 (Gemeinsames Ministerialblatt
S. 503), Nummer 6.2, nicht überschreitet. Einsichtnahmestelle
der DIN 4150: Freie und Hansestadt Hamburg,
Behörde für Stadtentwicklung und Umwelt, Amt für
Immissionsschutz und Betriebe, Bezugsquelle der DIN
4150: Beuth Verlag GmbH, Berlin.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_1f136ab2-8263-4d60-ba4f-b7b272e78b13">
      <xplan:schluessel>§2 Nr.25</xplan:schluessel>
      <xplan:text>In den Mischgebieten sind Dächer als Flachdächer oder
flachgeneigte Dächer mit einer Neigung bis zu 10 Grad
auszuführen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ca5005b0-4c55-4a51-9793-8306a404f448">
      <xplan:schluessel>§2 Nr.13.3</xplan:schluessel>
      <xplan:text>Vom Anschluss- und Benutzungsgebot nach Nummer 13.1
kann auf Antrag befreit werden, soweit die Erfüllung der
Anforderungen im Einzelfall wegen besonderer Umstände
zu einer unbilligen Härte führen würde. Die Befreiung soll
zeitlich befristet werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_dc728bae-a4dc-4bc3-a602-f2b4837e1263">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Die Aufenthaltsräume für gewerbliche Nutzungen – hier insbesondere
die Pausen- und Ruheräume – sind durch geeignete
Grundrissgestaltung den Verkehrslärm abgewandten
Gebäudeseiten zuzuordnen. Soweit die Anordnung an den
vom Verkehrslärm abgewandten Gebäudeseiten nicht möglich
ist, muss für diese Räume ein ausreichender Schallschutz
an Außentüren, Fenstern, Außenwänden und Dächern der
Gebäude durch bauliche Maßnahmen geschaffen werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_d99d7f46-18c7-4b7c-87b4-0660884f7022">
      <xplan:schluessel>§2 Nr.14</xplan:schluessel>
      <xplan:text>Das auf den Mischgebietsflächen und den Straßenverkehrsflächen
südlich der Versmannstraße anfallende
Nieder schlagswasser ist direkt in das nächst liegende
Gewässer (Baakenhafen) einzuleiten.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_34198f6f-76f9-40b3-9018-c111ad67d822">
      <xplan:schluessel>§2 Nr.13.2</xplan:schluessel>
      <xplan:text>Vom Anschluss- und Benutzungsgebot nach Nummer 13.1
kann ausnahmsweise abgesehen werden, wenn der berechnete
Heizwärmebedarf der Gebäude nach der Energieeinsparverordnung
vom 24. Juli 2007 (BGBl. I S. 1519),
zuletzt geändert am 18. November 2013 (BGBl. I S. 3951),
den Wert von 15 kWh/m² Nutzfläche nicht übersteigt.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c3000662-4308-41e3-852c-d74e90c60818">
      <xplan:schluessel>§2 Nr.13.1</xplan:schluessel>
      <xplan:text>Neu zu errichtende Gebäude sind an ein Wärmenetz anzuschließen,
das überwiegend mit erneuerbaren Energien
versorgt wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_1b203c2f-d6ba-43d3-b73e-3cf67a00bb26">
      <xplan:schluessel>§2 Nr.18</xplan:schluessel>
      <xplan:text>Auf den gekennzeichneten Flächen nördlich des Baakenhafens
(im Bereich Versmannkai und Versmannstraße),
deren Böden erheblich mit umweltgefährdenden Stoffen
belastet sind, sind bauliche Gassicherungsmaßnahmen
vorzusehen, die sowohl Gasansammlungen unter den
baulichen Anlagen und den befestigten Flächen als auch
Gaseintritte in die baulichen Anlagen verhindern.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_3c2d1544-a26d-4788-a7ce-736aff669377">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>Tiefgaragen sind außerhalb der überbaubaren Grundstücksflächen
zulässig. Stellplätze sind nur in Tiefgaragen
oder Garagengeschossen unterhalb der Höhe von 8,7 m
über Normalnull (NN) zulässig. Geringfügige Abweichungen
sind zulässig, wenn sie durch abweichende Straßenanschlusshöhen
von über 8,7 m über NN begründet
sind.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_198c3872-987b-4a3a-b836-96b84cbea024">
      <xplan:schluessel>§2 Nr.26</xplan:schluessel>
      <xplan:text>Für festgesetzte Anpflanzungen sind standortgerechte
Laubbäume oder belaubte Heckenpflanzen zu verwenden.
Großkronige Bäume müssen einen Stammumfang von
mindestens 18 cm, kleinkronige Bäume von mindestens
14 cm, in 1 m Höhe über dem Erdboden gemessen, aufweisen;
Heckenpflanzen eine Mindesthöhe von 80 cm.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_fe81b5af-a098-4f52-b354-3129f5e127b9">
      <xplan:schluessel>§2 Nr.12</xplan:schluessel>
      <xplan:text>Werbeanlagen größer als 2 m² und Werbeanlagen oberhalb
der Gebäudetraufen sind unzulässig. Die Gestaltung der
Gesamtbaukörper und der privaten Freiflächen darf nicht
durch Werbeanlagen beeinträchtigt werden. Werbeanlagen
sind nur an der Stätte der Leistung zulässig. Oberhalb der
Brüstung des zweiten Vollgeschosses sind Werbeanlagen
nur ausnahmsweise zulässig, wenn zudem das Ortsbild
nicht beeinträchtigt wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_90fc528d-6819-4a33-91a2-9267924316c2">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Auf den mit „(A)“ bezeichneten Flächen der Mischgebiete
sind Wohnungen in den Erdgeschossen unzulässig. Auf den
mit „(F)“ bezeichneten Flächen der Mischgebiete sind
Wohnungen in den Erdgeschossen ausnahmsweise zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_d2b1d8bb-169a-4ed0-b6dc-e310692d3257">
      <xplan:schluessel>§2 Nr.10</xplan:schluessel>
      <xplan:text>Die Gebäudefassaden können in unterschiedlichen Materialien
ausschließlich in den Farben Weiß, Beige, Gelb und
Blaubunt ausgeführt werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_71c1033e-3f2b-4986-b697-64dc19b0ab13">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Außer auf den mit „(C)“ bezeichneten Flächen muss die
Oberkante des Fußbodens des ersten Obergeschosses mindestens
5,5 m und höchstens 6,5 m über der angrenzenden
Geländeoberfläche liegen. Ausnahmsweise kann im Erdgeschoss
eine Galerie eingebaut werden, wenn das Galeriegeschoss
eine Grundfläche kleiner 50 vom Hundert (v.H.)
der Grundfläche des Erdgeschosses einnimmt. Die Galerieebene
muss einen Abstand von mindestens 4,5 m von der
Innenseite der zu den öffentlichen Straßenverkehrsflächen
und mit Gehrechten belegten Flächen gerichteten Außenfassade
einhalten. Das Erdgeschoss samt einem eventuell
eingezogenen Galeriegeschoss wird als ein Vollgeschoss
gewertet.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_24813ba0-c9b0-4655-9ccd-42baad49dcbf">
      <xplan:schluessel>§2 Nr.20</xplan:schluessel>
      <xplan:text>In den Baugebieten sind für Einfriedigungen nur Hecken
oder durchbrochene Zäune in Verbindung mit Hecken bis
zu einer Höhe von 1,2 m zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_967d2f7e-c198-4539-96a9-85ed0a81d4a2">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Oberhalb der festgesetzten Vollgeschosse (einschließlich
einem möglichen Galeriegeschoss im Erdgeschoss) sind
weitere Geschosse unzulässig. Technikgeschosse und technische
oder erforderliche Aufbauten, wie Treppenräume, sind ausnahmsweise, auch über der festgesetzten Gebäudehöhe,
zulässig, wenn die Gestaltung des Gesamtbaukörpers
und das Ortsbild nicht beeinträchtigt werden und
diese keine wesentliche Verschattung der Nachbargebäude
und der Umgebung bewirken. Aufbauten, deren Einhausung
und Technikgeschosse sind mindestens 2,5 m von
der Außenfassade zurückzusetzen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_7f160780-9db6-4be1-8ed3-11df2d255806">
      <xplan:schluessel>§2 Nr.2.2</xplan:schluessel>
      <xplan:text>Vergnügungsstätten in den Teilen des Mischgebiets, die
überwiegend durch gewerbliche Nutzungen geprägt sind
sowie Tankstellen sind unzulässig. Ausnahmen für Vergnügungsstätten
in den übrigen Teilen des Mischgebiets werden
ausgeschlossen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_10a2fbc6-3805-4691-b013-61905c99ede3">
      <xplan:schluessel>§2 Nr.23</xplan:schluessel>
      <xplan:text>Tiefgaragen und die Dachflächen der festgesetzten eingeschossigen
Gebäude auf den mit „(E)“ bezeichneten
Flächen sind in den zu begrünenden Bereichen mit einem
mindestens 50 cm starken durchwurzelbaren Substrataufbau
zu versehen. Für Baumpflanzungen muss auf einer
Fläche von 16 m² je Baum die Stärke des durchwurzelbaren
Substrataufbaus mindestens 80 cm betragen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_27e1d5c5-2e37-42be-b0be-bf85a23883d5">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>In den Mischgebieten an der Versmannstraße sind die
Schlafräume zur lärmabgewandten Gebäudeseite zu orientieren.
Wohn- / Schlafräume in Einzimmerwohnungen und
Kinderzimmer sind wie Schlafräume zu beurteilen. Wird an
den Gebäudeseiten ein Pegel von 70 dB(A) am Tag (6.00 Uhr
bis 22.00 Uhr) überschritten, sind vor den Fenstern der
zu dieser Gebäudeseite orientierten Wohnräume bauliche
Schallschutzmaßnahmen in Form von verglasten Vorbauten
(zum Beispiel verglaste Loggien, Wintergärten) oder vergleichbare
Maßnahmen vorzusehen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_5179f8b9-b2ee-4b16-bf73-805140661591">
      <xplan:schluessel>§2 Nr.11</xplan:schluessel>
      <xplan:text>Eine Überschreitung der Baugrenzen durch Balkone,
Erker, Loggien und Sichtschutzwände kann zu den öffentlichen
Straßenräumen oder den mit Gehrechten belasteten
Flächen ausnahmsweise bis zu einer Tiefe von 1,5 m zugelassen
werden, wenn die Gestaltung des Gesamtbaukörpers
nicht beeinträchtigt wird und diese keine wesentliche Verschattung
der benachbarten Nutzungen und der Umgebung
bewirken. Dabei ist eine Überbauung der Straßenverkehrsfläche
nur oberhalb einer lichten Höhe von 4,5 m
zulässig</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_b0540621-d713-4e0d-8f09-9f70e1771e40">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Für die Mischgebiete gilt:</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_60885b8d-ed98-44c0-859b-e937a34d45ab">
      <xplan:schluessel>§2 Nr.21</xplan:schluessel>
      <xplan:text>Die nicht überbauten Grundstücksflächen der Mischgebiete,
mit Ausnahme der Flächen mit festgesetzten
Gehrechten, sowie die Dachflächen der festgesetzten eingeschossigen
Gebäude auf den mit „(E)“ bezeichneten
Flächen sind mit einem Anteil von mindestens 50 v. H.
zu begrünen. Je 300 m² ist mindestens ein großkroniger
Baum oder je 150 m² ein kleinkroniger Baum zu pflanzen
und dauerhaft zu erhalten. Bei Abgang ist eine gleichwertige
Ersatzpflanzung vorzunehmen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c3cb239b-32bc-4361-9e35-d4869768a4a6">
      <xplan:schluessel>§2 Nr.24</xplan:schluessel>
      <xplan:text>Die übrigen Dachflächen in den Mischgebieten sind mit
Ausnahme der gemäß Nummer 9 zulässigen Anlagen und
technischen Aufbauten zu mindestens 30 v. H. mit einem
mindestens 15 cm starken durchwurzelbaren Substrataufbau
extensiv mit standortangepassten Stauden und Gräsern
zu begrünen. Darüber hinaus müssen mindestens
20 v. H. mit einem mindestens 50 cm starken Substrataufbau
intensiv mit Stauden und Sträuchern begrünt werden.
Die Dachbegrünung ist dauerhaft zu erhalten.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_de927319-5336-47ea-9a54-270bedb07ca7">
      <xplan:schluessel>§2 Nr.19</xplan:schluessel>
      <xplan:text>Auf den nicht überbauten Grundstücksflächen sind
Nebenanlagen nur ausnahmsweise zulässig, wenn die
Gestaltung der Freiflächen nicht beeinträchtigt ist.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_4360595b-e82a-4207-84b1-713aaefab5da">
      <xplan:schluessel>§2 Nr.2.3</xplan:schluessel>
      <xplan:text>Die festgesetzten Grundflächenzahlen können für Nutzungen
nach § 19 Absatz 4 Satz 1 der Baunutzungsverordnung
(BauNVO) in der Fassung vom 23. Januar 1990
(BGBl. I S. 133), zuletzt geändert am 11. Juni 2013 (BGBl. I
S. 1548, 1551), bis 1,0 überschritten werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_1f6cf107-2977-4732-9401-3a469bfe9bca">
      <xplan:schluessel>§2 Nr.16</xplan:schluessel>
      <xplan:text>Die festgesetzten Gehrechte umfassen die Befugnis der
Freien und Hansestadt Hamburg, allgemein zugängige
Gehwege anzulegen und zu unterhalten. Geringfügige
Abweichungen von den festgesetzten Gehrechten sind
zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_b2a6f7ae-3d3c-462b-a0a7-3a3dc7d6bffd">
      <xplan:schluessel>§2 Nr.13</xplan:schluessel>
      <xplan:text>Für die Beheizung und Bereitstellung des Warmwassers
gilt:</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_bd4953ce-ce43-47f3-90c8-f8b4cec45f43">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>Durch geeignete bauliche Schallschutzmaßnahmen wie zum
Beispiel Doppelfassaden, verglaste Vorbauten (zum Beispiel
verglaste Loggien, Wintergärten), besondere Fensterkonstruktionen
oder in ihrer Wirkung vergleichbare Maßnahmen
ist sicherzustellen, dass durch diese baulichen Maßnahmen
insgesamt eine Schallpegeldifferenz erreicht wird,
die es ermöglicht, dass in Schlafräumen ein Innenraumpegel
bei teilgeöffneten Fenstern von 30 dB(A) während der
Nachtzeit (22.00 Uhr bis 6.00 Uhr) nicht überschritten
wird. Erfolgt die bauliche Schallschutzmaßnahme in Form
von verglasten Vorbauten, muss dieser Innenraumpegel bei
teilgeöffneten Bauteilen erreicht werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_10f35421-3c0d-4867-9624-93d58f4a85be">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Für einen Außenbereich einer Wohnung ist entweder durch
Orientierung an lärmabgewandten Gebäudeseiten oder
durch bauliche Schallschutzmaßnahmen wie zum Beispiel
verglaste Vorbauten (zum Beispiel verglaste Loggien, Wintergärten)
mit teilgeöffneten Bauteilen sicherzustellen, dass
durch diese baulichen Maßnahmen insgesamt eine Schallpegelminderung
erreicht wird, die es ermöglicht, dass in
dem der Wohnung zugehörigen Außenbereich ein Tagpegel
(6.00 Uhr bis 22.00 Uhr) von kleiner 65 dB(A) erreicht
wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_a9f0e386-df8b-448a-aba5-b99a9e17fb19">
      <xplan:schluessel>§2 Nr.17</xplan:schluessel>
      <xplan:text>Das festgesetzte Geh- und Fahrrecht umfasst die Befugnis
der für die Unterhaltung der Kaianlagen zuständigen
Stelle, diese Flächen zu begehen und zu befahren.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_25a71fae-9279-4085-a124-4d507aa5e6ab">
      <xplan:schluessel>§2 Nr.22</xplan:schluessel>
      <xplan:text>Die mit festgesetzten Gehrechten belegten Flächen der
Mischgebiete sind mit einem Anteil von mindestens 20 v. H.
zu begrünen. Je 500 m² ist mindestens ein großkroniger
Baum oder je 250 m² ein kleinkroniger Baum zu pflanzen
und dauerhaft zu erhalten. Bei Abgang ist eine gleichwertige
Ersatzpflanzung vorzunehmen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_TextAbschnitt gml:id="GML_ecdadf5a-49dd-4414-8002-6294054f6e98">
      <xplan:schluessel>§2 Nr.15</xplan:schluessel>
      <xplan:text>An den Rändern der hochwassergefährdeten Bereiche sind
zum Zwecke des Hochwasserschutzes, soweit erforderlich,
zusätzliche besondere bauliche Maßnahmen vorzusehen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:SO_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_3b43fd0e-dc9d-4c85-b8ed-d6a0086631fd">
      <xplan:schluessel>§2 Nr.2.1</xplan:schluessel>
      <xplan:text>Großflächiger Einzelhandel kann ausnahmsweise auf den
mit „(B)“ bezeichneten Flächen zugelassen werden, wenn
er der Nahversorgung der angrenzenden Quartiere dient.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_477f762f-a694-4610-af8f-973ff3d2f2ce">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566839.305 5932624.6</gml:lowerCorner>
          <gml:upperCorner>567509.287 5932905.509</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_D993F2EE-E0B6-4D5C-8439-1DFADD23DF51" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567071.225 5932875.978 566952.478 5932905.509 566941.899 5932887.112 
566926.109 5932859.655 566925.011 5932857.747 566850.623 5932868.84 
566849.428 5932859.158 566848.719 5932853.417 566848.591 5932852.383 
566842.603 5932805.784 566841.516 5932797.098 566839.305 5932779.633 
566885.545 5932770.145 566933.255 5932760.356 567084.333 5932725.96 
567189.373 5932700.56 567374.73 5932651.63 567470.445 5932624.6 
567476.043 5932642.107 567477.061 5932645.289 567475.978 5932645.649 
567489.76 5932696.417 567494.443 5932713.666 567502.35 5932743.169 
567509.287 5932769.105 567416.044 5932790.244 567354.506 5932803.777 
567322.897 5932810.727 567243.981 5932831.427 567071.225 5932875.978 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#GML_02dd68c2-496d-4777-acf7-954835b58a67" />
      <xplan:planinhalt xlink:href="#GML_e8daa017-b8d6-4c15-a622-8f6b823cd660" />
      <xplan:planinhalt xlink:href="#GML_4f8a5796-8e25-456a-b60d-c8a81fe68c86" />
      <xplan:planinhalt xlink:href="#GML_fbc2da63-5f20-4a3f-93c7-eb64280946ae" />
      <xplan:planinhalt xlink:href="#GML_3b3add07-9f13-469c-8cf6-8b188bdd1846" />
      <xplan:planinhalt xlink:href="#GML_f5411b61-a297-48cc-bcd6-02acb174660d" />
      <xplan:planinhalt xlink:href="#GML_3c272272-a948-4daf-bc50-0bf6dcdd7558" />
      <xplan:planinhalt xlink:href="#GML_ff635c03-2029-44fe-a4c9-799ec98d6017" />
      <xplan:planinhalt xlink:href="#GML_3167c3ee-6594-4ec5-aa70-c6c5743ec594" />
      <xplan:planinhalt xlink:href="#GML_5ce2746c-d2f2-4cc7-a1cb-d61cff0edf82" />
      <xplan:planinhalt xlink:href="#GML_1634d328-4ca1-43d8-8e39-539ed19ab7ba" />
      <xplan:planinhalt xlink:href="#GML_f5657c68-9c44-43cf-a5e9-b13d01c91b28" />
      <xplan:planinhalt xlink:href="#GML_c2779008-e039-4a6a-bebb-6dcc0b02a980" />
      <xplan:planinhalt xlink:href="#GML_7a54d3f4-fc77-44e2-bfa6-d59646047694" />
      <xplan:planinhalt xlink:href="#GML_07954792-1f29-409f-8532-60a5944403c4" />
      <xplan:planinhalt xlink:href="#GML_556d6162-eb2e-41fd-bbea-e7af0b91cccf" />
      <xplan:planinhalt xlink:href="#GML_cd539988-9180-4496-8356-08634f1d76c1" />
      <xplan:planinhalt xlink:href="#GML_33e716e4-2c4c-4472-83eb-6392fa56e2bd" />
      <xplan:planinhalt xlink:href="#GML_020eb107-85d7-45aa-9fdf-ef12e84144d0" />
      <xplan:planinhalt xlink:href="#GML_24333227-aa84-42c4-9831-ea28ded220a8" />
      <xplan:planinhalt xlink:href="#GML_860cf820-17fc-41a2-b52d-88831d48efdc" />
      <xplan:planinhalt xlink:href="#GML_d105805d-5b94-4c38-851e-f57fa40143db" />
      <xplan:planinhalt xlink:href="#GML_262977ef-78a1-4a14-b31d-c298a7a731d5" />
      <xplan:planinhalt xlink:href="#GML_b8519f16-2806-4743-a671-086a66f97829" />
      <xplan:planinhalt xlink:href="#GML_40456788-05f8-4ef7-9cb9-e17a75f7f7d2" />
      <xplan:planinhalt xlink:href="#GML_519c6b6e-0cf7-4f70-b533-0c440c6ded88" />
      <xplan:planinhalt xlink:href="#GML_96f1b42d-a792-44c7-a21c-3f53beae3629" />
      <xplan:planinhalt xlink:href="#GML_00037b3a-065e-4ee5-831a-96abdc3b2a56" />
      <xplan:planinhalt xlink:href="#GML_47533d0a-8f43-40ad-90dc-12c6205b8e2a" />
      <xplan:planinhalt xlink:href="#GML_bcd2f6f9-6734-4bb8-b212-6f4e5f4177c1" />
      <xplan:planinhalt xlink:href="#GML_0e4752a7-9b89-4245-af86-bf44bf361d51" />
      <xplan:planinhalt xlink:href="#GML_cc2492ff-ad96-426a-b9eb-f915fa7d1266" />
      <xplan:planinhalt xlink:href="#GML_36001f8d-47e0-417d-9594-7c69e61ca0d7" />
      <xplan:planinhalt xlink:href="#GML_0219fdc1-bc9c-406a-9542-f78d0dae9bdd" />
      <xplan:planinhalt xlink:href="#GML_130ebfa5-1353-4d35-b32f-c680267a5105" />
      <xplan:planinhalt xlink:href="#GML_cb50a87e-6bcb-434b-926d-bb0f116a3e35" />
      <xplan:planinhalt xlink:href="#GML_6c1d6a77-860b-4766-a29b-6e8cebe8e5a8" />
      <xplan:planinhalt xlink:href="#GML_b4620683-ac15-4ed9-a32f-375d18e18624" />
      <xplan:planinhalt xlink:href="#GML_2a1a8469-8532-434a-b3d9-6db9061ffa85" />
      <xplan:planinhalt xlink:href="#GML_fd9e5e86-7496-456e-b965-db4c24d190ff" />
      <xplan:planinhalt xlink:href="#GML_61c8d395-4999-4d8d-9a8c-588a515cdcd5" />
      <xplan:planinhalt xlink:href="#GML_4b599ce4-9ceb-4e5f-bcc2-7e9bf43b2de1" />
      <xplan:planinhalt xlink:href="#GML_7c289604-22fb-4b59-956d-1c7820c71f91" />
      <xplan:planinhalt xlink:href="#GML_f3a9d9ec-ed46-438b-8787-0767c1d85678" />
      <xplan:planinhalt xlink:href="#GML_d6c03cdf-59ec-438b-b449-7053b93f1495" />
      <xplan:planinhalt xlink:href="#GML_2d2284da-1953-417f-8777-ad3be6806192" />
      <xplan:planinhalt xlink:href="#GML_d076487c-6dfa-49db-84d1-c2e342ccbd3e" />
      <xplan:planinhalt xlink:href="#GML_ca29b001-c537-4294-afe8-21aad4509df5" />
      <xplan:planinhalt xlink:href="#GML_66de4c58-8475-4f9a-b839-f9f354afe301" />
      <xplan:planinhalt xlink:href="#GML_396a4b8e-9b41-4655-a364-270b1adde6e4" />
      <xplan:planinhalt xlink:href="#GML_650fa2db-25da-4d3b-beec-23ecd390ddd7" />
      <xplan:planinhalt xlink:href="#GML_d0a6a6e6-1ed9-4e21-8148-2ee1a8b6d50b" />
      <xplan:planinhalt xlink:href="#GML_7a92e329-04b9-4a0d-bec0-34420e31cd4a" />
      <xplan:planinhalt xlink:href="#GML_448d3424-a399-4060-a407-b77dcc907bcc" />
      <xplan:planinhalt xlink:href="#GML_7783e4b1-3338-4928-9dc4-8f62f4daa301" />
      <xplan:planinhalt xlink:href="#GML_2eba39b8-a88d-462d-b5dd-6d2e974e420c" />
      <xplan:planinhalt xlink:href="#GML_c2410e2e-515c-4957-bc9a-d2d17ab1c20d" />
      <xplan:planinhalt xlink:href="#GML_e0d97f17-3d29-4a3b-a358-acb94ddfbb0e" />
      <xplan:planinhalt xlink:href="#GML_3095c3b2-bf9a-4caa-b770-923851df098f" />
      <xplan:planinhalt xlink:href="#GML_237802ac-d7ee-44fc-b9f6-786766f89ae6" />
      <xplan:planinhalt xlink:href="#GML_7e9f592e-57fd-4b74-9606-9b669c15ffe3" />
      <xplan:planinhalt xlink:href="#GML_dc3c8dd9-0b31-4034-a89f-a54b23e92efc" />
      <xplan:planinhalt xlink:href="#GML_c175151e-ccba-4b9c-b4a6-baa2511b1b03" />
      <xplan:planinhalt xlink:href="#GML_fa39ceab-4d83-45a4-b6ec-faffeddcee69" />
      <xplan:planinhalt xlink:href="#GML_da819480-9342-4af1-8f68-b1ea4c5eeae5" />
      <xplan:planinhalt xlink:href="#GML_4ff8a367-a539-4c50-9988-236d90236798" />
      <xplan:planinhalt xlink:href="#GML_2a85e360-97a9-4bc4-80ec-b0afd8096ecd" />
      <xplan:planinhalt xlink:href="#GML_0dd72b33-c610-471c-84ef-b4c3f7420cd9" />
      <xplan:planinhalt xlink:href="#GML_6d511353-1dd7-4ce2-a68d-104fc2e1ddb3" />
      <xplan:planinhalt xlink:href="#GML_c1a63310-c2fd-4744-aa2a-ad679ead56e0" />
      <xplan:planinhalt xlink:href="#GML_4deccb28-239e-4bf5-9192-de849c58ed27" />
      <xplan:planinhalt xlink:href="#GML_a9bf7239-bec5-44c9-be44-c3f3a702a3ce" />
      <xplan:planinhalt xlink:href="#GML_cf435a9f-da5f-4bfa-86b6-8e7227d9a307" />
      <xplan:planinhalt xlink:href="#GML_c7f09eff-61af-4ce5-92d4-768a4ffcba51" />
      <xplan:planinhalt xlink:href="#GML_43b9d9d0-c9b7-4bab-8d4a-83ff6816ac9a" />
      <xplan:planinhalt xlink:href="#GML_31cd27db-f486-48dc-95da-b11febdb0c3b" />
      <xplan:planinhalt xlink:href="#GML_5cccc7b9-584a-4411-a2cf-2666e263e1a5" />
      <xplan:planinhalt xlink:href="#GML_65a932e6-2b7e-49ca-aca3-abb276f6214d" />
      <xplan:planinhalt xlink:href="#GML_882980c4-52c5-4bef-bc65-b89f01190809" />
      <xplan:planinhalt xlink:href="#GML_7f2cbfaa-c9d2-435d-b1fc-2044bc03e30e" />
      <xplan:planinhalt xlink:href="#GML_af9af125-5727-4cc2-bf81-707298b0e96d" />
      <xplan:planinhalt xlink:href="#GML_c12222f8-99f2-40d0-a00c-f8ceae794f2a" />
      <xplan:planinhalt xlink:href="#GML_1358c7ad-9cc6-4765-8cdd-0743e4afdeff" />
      <xplan:planinhalt xlink:href="#GML_c9f973a9-e612-48db-84c2-27f2249ba674" />
      <xplan:planinhalt xlink:href="#GML_c7dcbb99-916c-4351-9ded-bb7cb343c5b5" />
      <xplan:planinhalt xlink:href="#GML_28258ff8-f5aa-4855-bde5-66baedd98f9c" />
      <xplan:planinhalt xlink:href="#GML_302e44ae-b842-4db6-bf1e-dcb482d9b2b8" />
      <xplan:planinhalt xlink:href="#GML_6ccdea5e-2740-4a8b-86ec-eef6b0d443de" />
      <xplan:planinhalt xlink:href="#GML_9d47fb51-694e-408f-9da8-d26c9c249c47" />
      <xplan:planinhalt xlink:href="#GML_7b2f09c1-4492-401a-a40d-2e2a8c81db31" />
      <xplan:planinhalt xlink:href="#GML_74c03a65-d58e-40f8-9d4b-32dd03930c41" />
      <xplan:planinhalt xlink:href="#GML_7ec703d7-c3bd-45d3-bda6-d9e59461b1dc" />
      <xplan:planinhalt xlink:href="#GML_df1512e4-15b5-463a-8b81-01815191ae01" />
      <xplan:planinhalt xlink:href="#GML_343e9ae0-e730-463d-be6c-6571d48c28d2" />
      <xplan:planinhalt xlink:href="#GML_ad3cbd9a-db08-40fd-88c7-48376c8cb21c" />
      <xplan:planinhalt xlink:href="#GML_98eed3a9-f7e5-4275-a5cd-9064f7769814" />
      <xplan:planinhalt xlink:href="#GML_cb7f8790-400f-4d67-a775-dd6bed006e33" />
      <xplan:planinhalt xlink:href="#GML_8a4c319e-9aac-4005-8bd9-6ed4ac473239" />
      <xplan:planinhalt xlink:href="#GML_fbf5e8fb-ca3f-4bd7-85ca-08ad3ba879b6" />
      <xplan:planinhalt xlink:href="#GML_84402729-ce76-47a2-b8a1-c59ec3a42b13" />
      <xplan:planinhalt xlink:href="#GML_31f84f9a-16ad-49b6-b6d8-68713690886f" />
      <xplan:praesentationsobjekt xlink:href="#GML_9e252d7d-380f-49f0-8718-c93163053c0b" />
      <xplan:praesentationsobjekt xlink:href="#GML_f1707941-0a6d-4480-a044-ef5306e8d416" />
      <xplan:praesentationsobjekt xlink:href="#GML_78fd6ab0-d51a-4424-8d6c-e65ca3db859c" />
      <xplan:praesentationsobjekt xlink:href="#GML_50060811-ddf4-40d4-b490-42fc4b709fb7" />
      <xplan:praesentationsobjekt xlink:href="#GML_47e8f0cf-4bee-4d5d-b13b-228154ffec57" />
      <xplan:praesentationsobjekt xlink:href="#GML_dc934fb5-7110-4036-a2d7-8f85df59c512" />
      <xplan:praesentationsobjekt xlink:href="#GML_16bc1466-4f4f-4483-8cd1-c85f2489997a" />
      <xplan:praesentationsobjekt xlink:href="#GML_0822b608-7f36-4045-ba9e-f940a63d233a" />
      <xplan:praesentationsobjekt xlink:href="#GML_74366e54-6699-4695-968e-3788be24bac1" />
      <xplan:praesentationsobjekt xlink:href="#GML_5b958bf4-f969-44c1-8af2-94f5004fc446" />
      <xplan:praesentationsobjekt xlink:href="#GML_605ae52a-fe59-4132-b238-4d94f704e0a0" />
      <xplan:praesentationsobjekt xlink:href="#GML_702512ea-8cfd-4ebe-931f-1da28fb187ca" />
      <xplan:praesentationsobjekt xlink:href="#GML_e9c977e3-3ec7-4d3a-949b-e1f4f5b039cf" />
      <xplan:praesentationsobjekt xlink:href="#GML_b3a250e6-75c0-430a-b2c6-6eb23c6847f4" />
      <xplan:praesentationsobjekt xlink:href="#GML_2c74db1d-8339-499d-b144-983eca85ac3a" />
      <xplan:praesentationsobjekt xlink:href="#GML_e4cb37bd-d6cc-42b7-952a-7d62e48ff6b4" />
      <xplan:praesentationsobjekt xlink:href="#GML_adce9ef0-df8c-423d-915a-467553a8c706" />
      <xplan:praesentationsobjekt xlink:href="#GML_e4028887-074d-4418-b503-63b85bed9a3b" />
      <xplan:praesentationsobjekt xlink:href="#GML_9ff978bc-695f-4c7a-8f18-5ad5bd63f498" />
      <xplan:praesentationsobjekt xlink:href="#GML_4bb328be-269b-4047-b041-75c3945fb0e7" />
      <xplan:praesentationsobjekt xlink:href="#GML_04ecbb85-2406-4654-b074-5843bb62c901" />
      <xplan:praesentationsobjekt xlink:href="#GML_b6087a6b-8ef1-41c8-972c-8a2f57b5b57b" />
      <xplan:praesentationsobjekt xlink:href="#GML_f48e9304-0f4e-4424-b063-6cce0bb5cd61" />
      <xplan:praesentationsobjekt xlink:href="#GML_5c832b32-2ee9-4161-a134-5ba17f738509" />
      <xplan:praesentationsobjekt xlink:href="#GML_96bd2a1e-0f12-4269-b10e-837fc9c1af15" />
      <xplan:praesentationsobjekt xlink:href="#GML_58c8e09a-302e-4e3a-8964-91fe6635df83" />
      <xplan:praesentationsobjekt xlink:href="#GML_6c06086c-178f-4c1f-889d-f5de82936598" />
      <xplan:praesentationsobjekt xlink:href="#GML_ca01c4e0-b15c-4847-b564-5a95d5b0d748" />
      <xplan:praesentationsobjekt xlink:href="#GML_c92b8f46-1e04-4d25-84c0-16b25b34edf2" />
      <xplan:praesentationsobjekt xlink:href="#GML_5ee3b068-e31a-4127-a41a-f4dfa6f56fbf" />
      <xplan:praesentationsobjekt xlink:href="#GML_fd317d40-5a09-455d-881e-fc0ce7f23fc1" />
      <xplan:praesentationsobjekt xlink:href="#GML_01b1a9f6-1cda-41c8-83a1-38da7bfe9942" />
      <xplan:praesentationsobjekt xlink:href="#GML_95069cea-fde7-4685-a9a7-5134604c24f0" />
      <xplan:praesentationsobjekt xlink:href="#GML_5f8ef1e1-d442-490a-a334-defa5d6e9e3c" />
      <xplan:praesentationsobjekt xlink:href="#GML_42bbdc04-3f6c-4e9f-9cff-7d1a083bd839" />
      <xplan:praesentationsobjekt xlink:href="#GML_f15dfde7-bb6a-49d3-9cb7-3097e47ce71b" />
      <xplan:praesentationsobjekt xlink:href="#GML_4c91d606-2c98-4045-9aa4-eafc3e05bac3" />
      <xplan:praesentationsobjekt xlink:href="#GML_62616a14-e2bf-4213-b186-d9abb111a5cb" />
      <xplan:praesentationsobjekt xlink:href="#GML_83b268c1-f0d9-4b73-8ce5-d37f49bd43fb" />
      <xplan:praesentationsobjekt xlink:href="#GML_0f3ee005-1a5b-4f97-86e6-3abe44fb35d5" />
      <xplan:praesentationsobjekt xlink:href="#GML_05b142a0-7b98-449f-950e-20ff08345eaf" />
      <xplan:praesentationsobjekt xlink:href="#GML_805db65c-eeb8-46f0-aa9c-70afc20d1e9b" />
      <xplan:praesentationsobjekt xlink:href="#GML_644dd5cc-db19-453b-9c87-c73257f9248b" />
      <xplan:praesentationsobjekt xlink:href="#GML_6005068f-2d54-4e55-955b-9565e9adba07" />
      <xplan:praesentationsobjekt xlink:href="#GML_0d3a599d-cf38-4a5d-8928-03618d9e4e1c" />
      <xplan:praesentationsobjekt xlink:href="#GML_b4a961d7-e672-452b-bae7-63749b7569e3" />
      <xplan:praesentationsobjekt xlink:href="#GML_332d1724-0907-402d-aa2c-0273a3931e7d" />
      <xplan:praesentationsobjekt xlink:href="#GML_1dcc2044-69bd-4b01-ac7c-d0ad489ba723" />
      <xplan:praesentationsobjekt xlink:href="#GML_b57fb67a-2217-4aa6-906b-40717787c8e9" />
      <xplan:praesentationsobjekt xlink:href="#GML_30ca3046-bdcd-4a5f-9ebf-50740c1934fc" />
      <xplan:praesentationsobjekt xlink:href="#GML_de2fa5a2-2f0e-4404-8485-980e7d68b2af" />
      <xplan:praesentationsobjekt xlink:href="#GML_9edf8894-bc32-484b-8f9a-6df0efa926a0" />
      <xplan:praesentationsobjekt xlink:href="#GML_1796cf02-2c08-45cb-9d75-cc9a52c8acce" />
      <xplan:praesentationsobjekt xlink:href="#GML_bd98df33-1c88-400c-acbc-2056c84b5b54" />
      <xplan:praesentationsobjekt xlink:href="#GML_3bea4192-8397-4d1f-9d4b-50128b15abc6" />
      <xplan:praesentationsobjekt xlink:href="#GML_34b9302e-3694-44f3-bb68-1479e1beeaa4" />
      <xplan:praesentationsobjekt xlink:href="#GML_88e4567e-b82f-49a2-8339-15dd4dbabf3b" />
      <xplan:praesentationsobjekt xlink:href="#GML_43cf3357-f468-48b4-95ac-9879abf761b8" />
      <xplan:praesentationsobjekt xlink:href="#GML_3085cdba-43bb-4dcb-abd6-ed87a0ee9dfb" />
      <xplan:praesentationsobjekt xlink:href="#GML_c1a8e5fd-693c-48fc-99f1-877ca595d0c1" />
      <xplan:praesentationsobjekt xlink:href="#GML_7d0d67af-5c61-49b2-8649-390dc3ef87a1" />
      <xplan:praesentationsobjekt xlink:href="#GML_ccc616ec-5875-40b7-9e86-9a48a5928c8e" />
      <xplan:praesentationsobjekt xlink:href="#GML_f46e10ce-6410-4874-adae-6b93d5c1ee68" />
      <xplan:praesentationsobjekt xlink:href="#GML_394bfe1f-35ac-4287-8b60-12e036f01cb6" />
      <xplan:praesentationsobjekt xlink:href="#GML_d51ed01f-67ac-4c2c-872e-685a4e3c8a2e" />
      <xplan:praesentationsobjekt xlink:href="#GML_3eb33633-a610-4de3-a9a1-a3ebdc8b8dd4" />
      <xplan:praesentationsobjekt xlink:href="#GML_c91124bb-24fd-4bc3-962c-975a7771c561" />
      <xplan:praesentationsobjekt xlink:href="#GML_fff6fa08-f6bd-4a2b-a6b2-f9fee2f33004" />
      <xplan:praesentationsobjekt xlink:href="#GML_2a89d1fb-4d1e-4186-b89d-16ece028a375" />
      <xplan:versionBauNVOText>Version vom 11.06.2013</xplan:versionBauNVOText>
      <xplan:gehoertZuPlan xlink:href="#GML_7e324fed-7859-448d-a7b6-7ba341b6b0f9" />
    </xplan:BP_Bereich>
  </gml:featureMember> 
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_02dd68c2-496d-4777-acf7-954835b58a67">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567181.156 5932731.601</gml:lowerCorner>
          <gml:upperCorner>567245.957 5932781.173</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_28EE057D-F779-4C02-B421-55C859547E2F" srsName="EPSG:25832">
          <gml:posList>567202.283 5932764.51 567229.869 5932757.348 567224.591 5932737.022 
567237.174 5932733.755 567245.957 5932767.583 567193.176 5932781.173 
567181.156 5932734.868 567193.739 5932731.601 567202.283 5932764.51 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e8daa017-b8d6-4c15-a622-8f6b823cd660">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567108.409 5932752.067</gml:lowerCorner>
          <gml:upperCorner>567140.779 5932786.174</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(B)</xplan:gliederung1>
      <xplan:gliederung2>(E)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f48e9304-0f4e-4424-b063-6cce0bb5cd61" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5c832b32-2ee9-4161-a134-5ba17f738509" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_fd317d40-5a09-455d-881e-fc0ce7f23fc1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_10a2fbc6-3805-4691-b013-61905c99ede3" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:refTextInhalt xlink:href="#GML_3b43fd0e-dc9d-4c85-b8ed-d6a0086631fd" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B841F9B7-D3A7-41E9-B1CE-EDE0340C3407" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567137.502 5932780.326 567112.927 5932786.174 567108.409 5932767.187 
567120.277 5932764.106 567118.644 5932757.814 567140.779 5932752.067 
567137.502 5932780.326 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_f48e9304-0f4e-4424-b063-6cce0bb5cd61">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567124.376 5932775.503</gml:lowerCorner>
          <gml:upperCorner>567124.376 5932775.503</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e8daa017-b8d6-4c15-a622-8f6b823cd660" />
      <xplan:schriftinhalt>(B)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_A04B8EDF-24DD-4E08-AC99-433F6A8D212E" srsName="EPSG:25832">
          <gml:pos>567124.376 5932775.503</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5c832b32-2ee9-4161-a134-5ba17f738509">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567124.376 5932769.996</gml:lowerCorner>
          <gml:upperCorner>567124.376 5932769.996</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e8daa017-b8d6-4c15-a622-8f6b823cd660" />
      <xplan:position>
        <gml:Point gml:id="Gml_1B2108FA-0D27-4819-BBED-F45F55CFF8CD" srsName="EPSG:25832">
          <gml:pos>567124.376 5932769.996</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_fd317d40-5a09-455d-881e-fc0ce7f23fc1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567124.376 5932764.202</gml:lowerCorner>
          <gml:upperCorner>567124.376 5932764.202</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e8daa017-b8d6-4c15-a622-8f6b823cd660" />
      <xplan:position>
        <gml:Point gml:id="Gml_E4FB37D4-2A24-45CB-B391-308ED40695F0" srsName="EPSG:25832">
          <gml:pos>567124.376 5932764.202</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_4f8a5796-8e25-456a-b60d-c8a81fe68c86">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566904.86 5932822.968</gml:lowerCorner>
          <gml:upperCorner>566939.864 5932843.606</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_1f6cf107-2977-4732-9401-3a469bfe9bca" />
      <xplan:refTextInhalt xlink:href="#GML_25a71fae-9279-4085-a124-4d507aa5e6ab" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E9AC46FF-884D-463A-9B91-87AF0D50C362" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566939.864 5932838.193 566919.2 5932841.614 566906.14 5932843.606 
566904.86 5932835.912 566918.024 5932833.904 566916.859 5932826.266 
566936.789 5932822.968 566939.864 5932838.193 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_fbc2da63-5f20-4a3f-93c7-eb64280946ae">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567347.101 5932723.312</gml:lowerCorner>
          <gml:upperCorner>567359.658 5932726.674</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_3F1E1418-77D8-408E-B314-6BD94BE14727" srsName="EPSG:25832">
          <gml:posList>567347.101 5932726.674 567359.658 5932723.312 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_3b3add07-9f13-469c-8cf6-8b188bdd1846">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566978.985 5932794.066</gml:lowerCorner>
          <gml:upperCorner>566995.451 5932815.855</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(F)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_01b1a9f6-1cda-41c8-83a1-38da7bfe9942" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_22CBD10B-0FCE-4D71-919C-BB5B30C7E2B3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566995.451 5932813.416 566982.682 5932815.855 566978.985 5932796.505 
566991.754 5932794.066 566995.451 5932813.416 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_01b1a9f6-1cda-41c8-83a1-38da7bfe9942">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566987.218 5932804.96</gml:lowerCorner>
          <gml:upperCorner>566987.218 5932804.96</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3b3add07-9f13-469c-8cf6-8b188bdd1846" />
      <xplan:position>
        <gml:Point gml:id="Gml_879D22B9-AB56-4709-BF49-04E8490CFDC3" srsName="EPSG:25832">
          <gml:pos>566987.218 5932804.96</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_f5411b61-a297-48cc-bcd6-02acb174660d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567381.24 5932690.361</gml:lowerCorner>
          <gml:upperCorner>567399.746 5932715.941</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>3000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0822b608-7f36-4045-ba9e-f940a63d233a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5f8ef1e1-d442-490a-a334-defa5d6e9e3c" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A7B005BE-0CAE-492D-A955-AD72A80B1AE9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567399.746 5932712.579 567387.189 5932715.941 567381.24 5932693.724 
567393.798 5932690.361 567399.746 5932712.579 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0822b608-7f36-4045-ba9e-f940a63d233a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567390.493 5932699.938</gml:lowerCorner>
          <gml:upperCorner>567390.493 5932699.938</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f5411b61-a297-48cc-bcd6-02acb174660d" />
      <xplan:position>
        <gml:Point gml:id="Gml_A941982E-4EDE-4677-BBD2-4F4479ED77FA" srsName="EPSG:25832">
          <gml:pos>567390.493 5932699.938</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5f8ef1e1-d442-490a-a334-defa5d6e9e3c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567390.493 5932705.278</gml:lowerCorner>
          <gml:upperCorner>567390.493 5932705.278</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f5411b61-a297-48cc-bcd6-02acb174660d" />
      <xplan:position>
        <gml:Point gml:id="Gml_2CBC3F05-44C1-4B10-BFE1-2B293E44E355" srsName="EPSG:25832">
          <gml:pos>567390.493 5932705.278</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_3c272272-a948-4daf-bc50-0bf6dcdd7558">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567261.31 5932723.846</gml:lowerCorner>
          <gml:upperCorner>567279.2 5932747.803</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1100</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_50060811-ddf4-40d4-b490-42fc4b709fb7" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e4028887-074d-4418-b503-63b85bed9a3b" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B19EEA45-E6BA-4B9D-BED1-2EB1AA57735F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567279.2 5932744.514 567266.628 5932747.803 567261.31 5932727.488 
567273.793 5932723.846 567279.2 5932744.514 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_50060811-ddf4-40d4-b490-42fc4b709fb7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567269.576 5932735.532</gml:lowerCorner>
          <gml:upperCorner>567269.576 5932735.532</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3c272272-a948-4daf-bc50-0bf6dcdd7558" />
      <xplan:position>
        <gml:Point gml:id="Gml_B56394A8-CADA-4931-8BB0-BDBA9BD3C0E2" srsName="EPSG:25832">
          <gml:pos>567269.576 5932735.532</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e4028887-074d-4418-b503-63b85bed9a3b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567269.576 5932730.191</gml:lowerCorner>
          <gml:upperCorner>567269.576 5932730.191</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3c272272-a948-4daf-bc50-0bf6dcdd7558" />
      <xplan:position>
        <gml:Point gml:id="Gml_EA13B291-DE0F-4263-ADAF-0CF5942CDF45" srsName="EPSG:25832">
          <gml:pos>567269.576 5932730.191</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ff635c03-2029-44fe-a4c9-799ec98d6017">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567100.317 5932741.64</gml:lowerCorner>
          <gml:upperCorner>567155.075 5932802.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_8350CF36-C63E-448B-8C3D-1466C88FD5FF" srsName="EPSG:25832">
          <gml:posList>567149.128 5932791.971 567103.525 5932802.817 567100.317 5932789.338 
567113.013 5932786.154 567137.502 5932780.326 567141.581 5932745.143 
567155.075 5932741.64 567154.527 5932745.398 567149.128 5932791.971 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_3167c3ee-6594-4ec5-aa70-c6c5743ec594">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566876.538 5932813.399</gml:lowerCorner>
          <gml:upperCorner>566904.004 5932832.072</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2a89d1fb-4d1e-4186-b89d-16ece028a375" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_77E643C8-E850-42CA-8193-0C2C371DA01B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566904.004 5932828.228 566878.799 5932832.072 566876.538 5932817.244 
566901.745 5932813.399 566904.004 5932828.228 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>2</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2a89d1fb-4d1e-4186-b89d-16ece028a375">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566890.271 5932822.736</gml:lowerCorner>
          <gml:upperCorner>566890.271 5932822.736</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3167c3ee-6594-4ec5-aa70-c6c5743ec594" />
      <xplan:position>
        <gml:Point gml:id="Gml_2828E45E-879D-4BBC-8021-C09F206D10B9" srsName="EPSG:25832">
          <gml:pos>566890.271 5932822.736</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_5ce2746c-d2f2-4cc7-a1cb-d61cff0edf82">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567088.12 5932789.185</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932805.699</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">8</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_13221845-B43D-4E60-9F0C-045094056F5F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567103.525 5932802.817 567091.361 5932805.699 567088.12 5932792.079 
567100.281 5932789.185 567103.525 5932802.817 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_1634d328-4ca1-43d8-8e39-539ed19ab7ba">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567108.409 5932752.067</gml:lowerCorner>
          <gml:upperCorner>567140.779 5932786.174</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_679B7E2B-2F68-4437-95B3-CEABB73DF606" srsName="EPSG:25832">
          <gml:posList>567137.502 5932780.326 567112.927 5932786.174 567108.409 5932767.187 
567120.277 5932764.106 567118.644 5932757.814 567140.779 5932752.067 
567137.502 5932780.326 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f5657c68-9c44-43cf-a5e9-b13d01c91b28">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566938.222 5932794.066</gml:lowerCorner>
          <gml:upperCorner>566998.078 5932837.394</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_C6461EF5-0433-4F54-9788-4C24490CC963" srsName="EPSG:25832">
          <gml:posList>566998.078 5932827.167 566944.546 5932837.394 566938.222 5932804.293 
566950.991 5932801.853 566954.688 5932821.203 566982.682 5932815.855 
566978.985 5932796.505 566991.754 5932794.066 566995.451 5932813.416 
566998.078 5932827.167 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_c2779008-e039-4a6a-bebb-6dcc0b02a980">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567092.822 5932745.398</gml:lowerCorner>
          <gml:upperCorner>567154.527 5932802.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_42bbdc04-3f6c-4e9f-9cff-7d1a083bd839" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_332d1724-0907-402d-aa2c-0273a3931e7d" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_915778b2-65ac-472f-8c81-7fd17d57d1c1" />
      <xplan:refTextInhalt xlink:href="#GML_1f136ab2-8263-4d60-ba4f-b7b272e78b13" />
      <xplan:refTextInhalt xlink:href="#GML_dc728bae-a4dc-4bc3-a602-f2b4837e1263" />
      <xplan:refTextInhalt xlink:href="#GML_d99d7f46-18c7-4b7c-87b4-0660884f7022" />
      <xplan:refTextInhalt xlink:href="#GML_3c2d1544-a26d-4788-a7ce-736aff669377" />
      <xplan:refTextInhalt xlink:href="#GML_d2b1d8bb-169a-4ed0-b6dc-e310692d3257" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_24813ba0-c9b0-4655-9ccd-42baad49dcbf" />
      <xplan:refTextInhalt xlink:href="#GML_967d2f7e-c198-4539-96a9-85ed0a81d4a2" />
      <xplan:refTextInhalt xlink:href="#GML_7f160780-9db6-4be1-8ed3-11df2d255806" />
      <xplan:refTextInhalt xlink:href="#GML_27e1d5c5-2e37-42be-b0be-bf85a23883d5" />
      <xplan:refTextInhalt xlink:href="#GML_b0540621-d713-4e0d-8f09-9f70e1771e40" />
      <xplan:refTextInhalt xlink:href="#GML_60885b8d-ed98-44c0-859b-e937a34d45ab" />
      <xplan:refTextInhalt xlink:href="#GML_c3cb239b-32bc-4361-9e35-d4869768a4a6" />
      <xplan:refTextInhalt xlink:href="#GML_de927319-5336-47ea-9a54-270bedb07ca7" />
      <xplan:refTextInhalt xlink:href="#GML_4360595b-e82a-4207-84b1-713aaefab5da" />
      <xplan:refTextInhalt xlink:href="#GML_bd4953ce-ce43-47f3-90c8-f8b4cec45f43" />
      <xplan:refTextInhalt xlink:href="#GML_10f35421-3c0d-4867-9624-93d58f4a85be" />
      <xplan:refTextInhalt xlink:href="#GML_25a71fae-9279-4085-a124-4d507aa5e6ab" />
      <xplan:refTextInhalt xlink:href="#GML_3b43fd0e-dc9d-4c85-b8ed-d6a0086631fd" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_8090102F-924E-4E03-BFA4-94343124E59B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567149.128 5932791.971 567103.525 5932802.817 567092.822 5932757.841 
567117.019 5932751.558 567117.89 5932754.911 567154.527 5932745.398 
567149.128 5932791.971 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_42bbdc04-3f6c-4e9f-9cff-7d1a083bd839">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567133.905 5932761.277</gml:lowerCorner>
          <gml:upperCorner>567133.905 5932761.277</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c2779008-e039-4a6a-bebb-6dcc0b02a980" />
      <xplan:position>
        <gml:Point gml:id="Gml_103074C5-BA7C-4264-A81D-8012E0E12EBA" srsName="EPSG:25832">
          <gml:pos>567133.905 5932761.277</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_332d1724-0907-402d-aa2c-0273a3931e7d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567133.905 5932767.233</gml:lowerCorner>
          <gml:upperCorner>567133.905 5932767.233</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c2779008-e039-4a6a-bebb-6dcc0b02a980" />
      <xplan:position>
        <gml:Point gml:id="Gml_15761E6C-F27A-4BBA-8F52-0E021D8244D0" srsName="EPSG:25832">
          <gml:pos>567133.905 5932767.233</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_7a54d3f4-fc77-44e2-bfa6-d59646047694">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566849.469 5932728.094</gml:lowerCorner>
          <gml:upperCorner>567498.31 5932859.222</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B7604CC9-B3D0-479C-9775-7E765E61D018" srsName="EPSG:25832">
          <gml:posList>566849.469 5932859.222 566896.825 5932852.378 566894.962 5932839.469 
566913.019 5932836.466 566918.283 5932835.601 566931.081 5932833.497 
566932.117 5932839.756 566918.207 5932842.058 566904.297 5932844.361 
566905.266 5932851.083 566916.768 5932849.21 566919.425 5932849.281 
566968.153 5932840.52 566980.426 5932838.241 566987.76 5932836.783 
567057.602 5932824.015 567127.727 5932812.902 567173.834 5932806.143 
567206.34 5932801.096 567210.643 5932800.367 567232.728 5932796.1 
567254.637 5932791.004 567270.059 5932786.989 567293.738 5932780.36 
567328.582 5932770.627 567361.266 5932761.734 567386.531 5932755.262 
567391.412 5932754.053 567403.33 5932751.144 567403.33 5932751.144 
567437.692 5932742.804 567473.816 5932734.037 567498.31 5932728.094 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_07954792-1f29-409f-8532-60a5944403c4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567306.772 5932734.136</gml:lowerCorner>
          <gml:upperCorner>567318.865 5932737.3</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_974E36B4-8573-4F13-ACE0-FE3F2F81695B" srsName="EPSG:25832">
          <gml:posList>567306.772 5932737.3 567318.865 5932734.136 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_556d6162-eb2e-41fd-bbea-e7af0b91cccf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566982.682 5932813.416</gml:lowerCorner>
          <gml:upperCorner>566995.451 5932815.855</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_AE49A8F5-997F-40C6-8760-6818B7F0793B" srsName="EPSG:25832">
          <gml:posList>566982.682 5932815.855 566995.451 5932813.416 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_cd539988-9180-4496-8356-08634f1d76c1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567470.593 5932666.819</gml:lowerCorner>
          <gml:upperCorner>567491.419 5932705.541</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_1f6cf107-2977-4732-9401-3a469bfe9bca" />
      <xplan:refTextInhalt xlink:href="#GML_25a71fae-9279-4085-a124-4d507aa5e6ab" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_06326C67-492D-4538-A5D2-E00A101AEF47" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567491.419 5932702.527 567480.162 5932705.541 567470.593 5932669.8 
567481.725 5932666.819 567491.419 5932702.527 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_33e716e4-2c4c-4472-83eb-6392fa56e2bd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566848.591 5932702.527</gml:lowerCorner>
          <gml:upperCorner>567502.35 5932887.077</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7B21F865-3B26-4CB9-A8D6-03736A625FB3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567047.196 5932861.151 566942.034 5932887.077 566925.011 5932857.747 
566850.623 5932868.84 566848.591 5932852.383 566919.171 5932841.619 
566944.512 5932837.424 567013.667 5932824.189 567149.128 5932791.971 
567193.176 5932781.173 567245.957 5932767.583 567270.174 5932761.347 
567338.647 5932743.431 567427.516 5932719.637 567491.419 5932702.527 
567502.35 5932743.169 567485.287 5932746.96 567390.254 5932768.247 
567363.553 5932775.056 567163.038 5932830.198 567122.404 5932841.663 
567047.196 5932861.151 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_020eb107-85d7-45aa-9fdf-ef12e84144d0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566942.034 5932743.169</gml:lowerCorner>
          <gml:upperCorner>567509.287 5932905.509</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_FEB78EA1-50B7-4945-9C50-230733CAFE67" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567509.287 5932769.105 567416.044 5932790.244 567354.506 5932803.777 
567322.897 5932810.727 567243.981 5932831.427 567071.225 5932875.978 
566952.478 5932905.509 566942.034 5932887.077 567047.196 5932861.151 
567122.404 5932841.663 567163.038 5932830.198 567363.553 5932775.056 
567390.254 5932768.247 567485.287 5932746.96 567502.35 5932743.169 
567509.287 5932769.105 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_24333227-aa84-42c4-9831-ea28ded220a8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567022.941 5932794.967</gml:lowerCorner>
          <gml:upperCorner>567079.203 5932821.212</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_47e8f0cf-4bee-4d5d-b13b-228154ffec57" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_6c06086c-178f-4c1f-889d-f5de82936598" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D1C76A6F-95FA-4790-9167-7FA6CBC2A5EE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567079.203 5932808.602 567026.182 5932821.212 567022.941 5932807.591 
567035.587 5932804.581 567063.313 5932797.983 567075.983 5932794.967 
567079.203 5932808.602 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_47e8f0cf-4bee-4d5d-b13b-228154ffec57">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567051.077 5932810.386</gml:lowerCorner>
          <gml:upperCorner>567051.077 5932810.386</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_24333227-aa84-42c4-9831-ea28ded220a8" />
      <xplan:position>
        <gml:Point gml:id="Gml_FDF6EAA9-6B8D-43D6-9595-D84E3F62D3DC" srsName="EPSG:25832">
          <gml:pos>567051.077 5932810.386</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_6c06086c-178f-4c1f-889d-f5de82936598">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567051.077 5932804.592</gml:lowerCorner>
          <gml:upperCorner>567051.077 5932804.592</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_24333227-aa84-42c4-9831-ea28ded220a8" />
      <xplan:position>
        <gml:Point gml:id="Gml_934CAF12-6E82-4CD8-A124-E5D702721A8B" srsName="EPSG:25832">
          <gml:pos>567051.077 5932804.592</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_860cf820-17fc-41a2-b52d-88831d48efdc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566842.603 5932757.841</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932852.383</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e4cb37bd-d6cc-42b7-952a-7d62e48ff6b4" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_bd98df33-1c88-400c-acbc-2056c84b5b54" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_915778b2-65ac-472f-8c81-7fd17d57d1c1" />
      <xplan:refTextInhalt xlink:href="#GML_1f136ab2-8263-4d60-ba4f-b7b272e78b13" />
      <xplan:refTextInhalt xlink:href="#GML_dc728bae-a4dc-4bc3-a602-f2b4837e1263" />
      <xplan:refTextInhalt xlink:href="#GML_d99d7f46-18c7-4b7c-87b4-0660884f7022" />
      <xplan:refTextInhalt xlink:href="#GML_3c2d1544-a26d-4788-a7ce-736aff669377" />
      <xplan:refTextInhalt xlink:href="#GML_d2b1d8bb-169a-4ed0-b6dc-e310692d3257" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_24813ba0-c9b0-4655-9ccd-42baad49dcbf" />
      <xplan:refTextInhalt xlink:href="#GML_967d2f7e-c198-4539-96a9-85ed0a81d4a2" />
      <xplan:refTextInhalt xlink:href="#GML_7f160780-9db6-4be1-8ed3-11df2d255806" />
      <xplan:refTextInhalt xlink:href="#GML_27e1d5c5-2e37-42be-b0be-bf85a23883d5" />
      <xplan:refTextInhalt xlink:href="#GML_b0540621-d713-4e0d-8f09-9f70e1771e40" />
      <xplan:refTextInhalt xlink:href="#GML_60885b8d-ed98-44c0-859b-e937a34d45ab" />
      <xplan:refTextInhalt xlink:href="#GML_c3cb239b-32bc-4361-9e35-d4869768a4a6" />
      <xplan:refTextInhalt xlink:href="#GML_de927319-5336-47ea-9a54-270bedb07ca7" />
      <xplan:refTextInhalt xlink:href="#GML_4360595b-e82a-4207-84b1-713aaefab5da" />
      <xplan:refTextInhalt xlink:href="#GML_bd4953ce-ce43-47f3-90c8-f8b4cec45f43" />
      <xplan:refTextInhalt xlink:href="#GML_10f35421-3c0d-4867-9624-93d58f4a85be" />
      <xplan:refTextInhalt xlink:href="#GML_25a71fae-9279-4085-a124-4d507aa5e6ab" />
      <xplan:refTextInhalt xlink:href="#GML_3b43fd0e-dc9d-4c85-b8ed-d6a0086631fd" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C683F45C-632E-4ED2-A216-6FDD613506D6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567103.525 5932802.817 567013.667 5932824.189 566944.512 5932837.424 
566919.171 5932841.619 566848.591 5932852.383 566842.603 5932805.784 
566858.262 5932803.586 566899.326 5932797.323 566923.976 5932793.564 
566925.489 5932793.308 566935.752 5932791.578 567004.845 5932778.378 
567038.535 5932771.936 567092.822 5932757.841 567103.525 5932802.817 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.7</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e4cb37bd-d6cc-42b7-952a-7d62e48ff6b4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566930 5932818.487</gml:lowerCorner>
          <gml:upperCorner>566930 5932818.487</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_860cf820-17fc-41a2-b52d-88831d48efdc" />
      <xplan:position>
        <gml:Point gml:id="Gml_DB1C8417-DA01-4FA0-8A86-F80797E82DD9" srsName="EPSG:25832">
          <gml:pos>566930 5932818.487</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_bd98df33-1c88-400c-acbc-2056c84b5b54">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566928.39 5932812.523</gml:lowerCorner>
          <gml:upperCorner>566928.39 5932812.523</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_860cf820-17fc-41a2-b52d-88831d48efdc" />
      <xplan:position>
        <gml:Point gml:id="Gml_7311CBA2-F6A7-43FC-A81E-61A6F24551DC" srsName="EPSG:25832">
          <gml:pos>566928.39 5932812.523</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_d105805d-5b94-4c38-851e-f57fa40143db">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567377.878 5932674.7</gml:lowerCorner>
          <gml:upperCorner>567405.389 5932693.724</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_961CA6D6-EA7B-4D7E-B102-761CE0D3C286" srsName="EPSG:25832">
          <gml:posList>567405.389 5932687.258 567393.798 5932690.361 567381.24 5932693.724 
567377.878 5932681.166 567402.027 5932674.7 567405.389 5932687.258 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_262977ef-78a1-4a14-b31d-c298a7a731d5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566842.603 5932803.585</gml:lowerCorner>
          <gml:upperCorner>566865.323 5932852.383</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_a9f0e386-df8b-448a-aba5-b99a9e17fb19" />
      <xplan:refTextInhalt xlink:href="#GML_25a71fae-9279-4085-a124-4d507aa5e6ab" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4AEBD31E-B102-47F0-A92F-56B1D55DE460" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566865.319 5932849.807 566865.323 5932849.831 566848.591 5932852.383 
566842.603 5932805.784 566858.269 5932803.585 566865.319 5932849.807 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>3000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b8519f16-2806-4743-a671-086a66f97829">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566916.859 5932822.166</gml:lowerCorner>
          <gml:upperCorner>566944.546 5932841.614</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_65ABFB7C-D2B4-4083-90FC-BF9F04F218C8" srsName="EPSG:25832">
          <gml:posList>566944.546 5932837.394 566939.864 5932838.193 566919.2 5932841.614 
566916.859 5932826.266 566941.637 5932822.166 566944.546 5932837.394 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_40456788-05f8-4ef7-9cb9-e17a75f7f7d2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566904.86 5932833.904</gml:lowerCorner>
          <gml:upperCorner>566919.2 5932843.606</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_90932205-ED54-48D2-9456-65C5FD0FF7D4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566919.2 5932841.614 566906.14 5932843.606 566904.86 5932835.912 
566918.024 5932833.904 566919.2 5932841.614 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1400</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_519c6b6e-0cf7-4f70-b533-0c440c6ded88">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566976.546 5932778.857</gml:lowerCorner>
          <gml:upperCorner>567004.523 5932796.505</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_46D9AAD8-4994-4A45-96ED-3D936EC7DE37" srsName="EPSG:25832">
          <gml:posList>567004.523 5932791.626 566991.754 5932794.066 566978.985 5932796.505 
566976.546 5932783.737 567002.084 5932778.857 567004.523 5932791.626 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_96f1b42d-a792-44c7-a21c-3f53beae3629">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567195.374 5932730.73</gml:lowerCorner>
          <gml:upperCorner>567229.869 5932764.51</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(B)</xplan:gliederung1>
      <xplan:gliederung2>(E)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_adce9ef0-df8c-423d-915a-467553a8c706" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_de2fa5a2-2f0e-4404-8485-980e7d68b2af" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_43cf3357-f468-48b4-95ac-9879abf761b8" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_10a2fbc6-3805-4691-b013-61905c99ede3" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:refTextInhalt xlink:href="#GML_3b43fd0e-dc9d-4c85-b8ed-d6a0086631fd" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_15418DC4-D5BF-4BF4-BC1F-8289B4DFE13E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567224.591 5932737.022 567229.869 5932757.348 567202.283 5932764.51 
567195.374 5932737.898 567222.958 5932730.73 567224.591 5932737.022 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_adce9ef0-df8c-423d-915a-467553a8c706">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567212.621 5932749.916</gml:lowerCorner>
          <gml:upperCorner>567212.621 5932749.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_96f1b42d-a792-44c7-a21c-3f53beae3629" />
      <xplan:schriftinhalt>(B)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_131A5839-DA4A-4563-B96B-0DFBC505F9E6" srsName="EPSG:25832">
          <gml:pos>567212.621 5932749.916</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_de2fa5a2-2f0e-4404-8485-980e7d68b2af">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567212.621 5932744.409</gml:lowerCorner>
          <gml:upperCorner>567212.621 5932744.409</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_96f1b42d-a792-44c7-a21c-3f53beae3629" />
      <xplan:position>
        <gml:Point gml:id="Gml_65601955-5043-49FB-8320-10D655B6F580" srsName="EPSG:25832">
          <gml:pos>567212.621 5932744.409</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_43cf3357-f468-48b4-95ac-9879abf761b8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567212.621 5932738.615</gml:lowerCorner>
          <gml:upperCorner>567212.621 5932738.615</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_96f1b42d-a792-44c7-a21c-3f53beae3629" />
      <xplan:position>
        <gml:Point gml:id="Gml_AF08F118-035E-4C0C-B6CA-581BEA2DC786" srsName="EPSG:25832">
          <gml:pos>567212.621 5932738.615</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_00037b3a-065e-4ee5-831a-96abdc3b2a56">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567095.823 5932767.187</gml:lowerCorner>
          <gml:upperCorner>567112.927 5932789.338</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1100</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_30ca3046-bdcd-4a5f-9ebf-50740c1934fc" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_88e4567e-b82f-49a2-8339-15dd4dbabf3b" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D640D12F-36C6-4689-9977-8893A3DE051E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567112.927 5932786.175 567100.317 5932789.338 567100.281 5932789.185 
567095.823 5932770.455 567108.409 5932767.187 567112.927 5932786.175 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_30ca3046-bdcd-4a5f-9ebf-50740c1934fc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567104.369 5932780.416</gml:lowerCorner>
          <gml:upperCorner>567104.369 5932780.416</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00037b3a-065e-4ee5-831a-96abdc3b2a56" />
      <xplan:position>
        <gml:Point gml:id="Gml_0B94091B-2F83-4132-9096-80157C2A4D04" srsName="EPSG:25832">
          <gml:pos>567104.369 5932780.416</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_88e4567e-b82f-49a2-8339-15dd4dbabf3b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567104.369 5932775.075</gml:lowerCorner>
          <gml:upperCorner>567104.369 5932775.075</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00037b3a-065e-4ee5-831a-96abdc3b2a56" />
      <xplan:position>
        <gml:Point gml:id="Gml_8FC73C34-DA33-46E7-A32D-90C0EA1973A5" srsName="EPSG:25832">
          <gml:pos>567104.369 5932775.075</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_47533d0a-8f43-40ad-90dc-12c6205b8e2a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567254.372 5932747.803</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932764.526</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">8</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_EEAFE0AC-C5FD-424D-98DC-1BBCCE66979B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567270.174 5932761.347 567257.829 5932764.526 567254.372 5932750.986 
567266.628 5932747.803 567270.174 5932761.347 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_bcd2f6f9-6734-4bb8-b212-6f4e5f4177c1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567143.706 5932787.913</gml:lowerCorner>
          <gml:upperCorner>567143.706 5932787.913</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.75</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_4FFB4BE7-3F06-49C4-8292-20C1A53B2067" srsName="EPSG:25832">
          <gml:pos>567143.706 5932787.913</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_0e4752a7-9b89-4245-af86-bf44bf361d51">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567266.628 5932696.257</gml:lowerCorner>
          <gml:upperCorner>567325.501 5932761.347</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_51C89278-11AF-4E25-B00D-A1C4B603AC79" srsName="EPSG:25832">
          <gml:posList>567322.893 5932747.553 567270.174 5932761.347 567266.628 5932747.803 
567279.2 5932744.514 567306.772 5932737.3 567297.868 5932703.269 
567321.864 5932696.257 567325.501 5932708.738 567313.643 5932712.203 
567319.349 5932734.009 567322.893 5932747.553 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_cc2492ff-ad96-426a-b9eb-f915fa7d1266">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566847.954 5932803.585</gml:lowerCorner>
          <gml:upperCorner>566862.976 5932836.346</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_1f6cf107-2977-4732-9401-3a469bfe9bca" />
      <xplan:refTextInhalt xlink:href="#GML_25a71fae-9279-4085-a124-4d507aa5e6ab" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_50095F9F-E53A-4764-9E66-FEC16984C2FD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566862.976 5932834.447 566851.949 5932836.346 566847.954 5932805.033 
566858.269 5932803.585 566862.976 5932834.447 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_36001f8d-47e0-417d-9594-7c69e61ca0d7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566839.305 5932624.6</gml:lowerCorner>
          <gml:upperCorner>567474.886 5932795.563</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_31A81E28-262D-468D-93FF-67EEDD9970A8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567474.886 5932638.488 567462.605 5932641.979 567379.905 5932665.733 
567324.249 5932681.582 567309.188 5932685.742 567309.648 5932687.426 
567251.407 5932703.902 567207.598 5932715.718 567165.898 5932726.231 
567099.41 5932743.34 567034.553 5932760.037 567024.668 5932762.288 
567001.253 5932766.989 566957.155 5932775.947 566925.622 5932781.675 
566925.816 5932782.812 566893.555 5932788.149 566876.457 5932790.784 
566856.166 5932793.67 566841.322 5932795.563 566839.305 5932779.633 
566885.545 5932770.145 566933.255 5932760.356 567084.333 5932725.96 
567189.373 5932700.56 567374.73 5932651.63 567470.445 5932624.6 
567474.886 5932638.488 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_0219fdc1-bc9c-406a-9542-f78d0dae9bdd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567092.822 5932757.841</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932802.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_DE9162D2-BC0C-4E1A-B1F4-AFCB84AF687D" srsName="EPSG:25832">
          <gml:posList>567103.525 5932802.817 567100.317 5932789.337 567100.281 5932789.185 
567095.824 5932770.455 567092.822 5932757.841 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_130ebfa5-1353-4d35-b32f-c680267a5105">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567242.452 5932747.803</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932767.583</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_2239D9A1-5889-4C6A-ADD9-7F3945FB83B6" srsName="EPSG:25832">
          <gml:posList>567270.174 5932761.347 567245.957 5932767.583 567242.452 5932754.081 
567266.628 5932747.803 567270.174 5932761.347 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_cb50a87e-6bcb-434b-926d-bb0f116a3e35">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567056.034 5932761.11</gml:lowerCorner>
          <gml:upperCorner>567083.489 5932780.007</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1100</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_05b142a0-7b98-449f-950e-20ff08345eaf" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b4a961d7-e672-452b-bae7-63749b7569e3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_469F2129-4879-4F2C-99C3-113D2DF0C361" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567083.489 5932773.657 567070.652 5932776.99 567059.035 5932780.007 
567056.034 5932767.393 567080.231 5932761.11 567083.489 5932773.657 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_05b142a0-7b98-449f-950e-20ff08345eaf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567069.698 5932772.669</gml:lowerCorner>
          <gml:upperCorner>567069.698 5932772.669</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_cb50a87e-6bcb-434b-926d-bb0f116a3e35" />
      <xplan:position>
        <gml:Point gml:id="Gml_CFAAA7AE-C5A4-4876-8C5F-E87CED1E62A9" srsName="EPSG:25832">
          <gml:pos>567069.698 5932772.669</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b4a961d7-e672-452b-bae7-63749b7569e3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567069.698 5932767.328</gml:lowerCorner>
          <gml:upperCorner>567069.698 5932767.328</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_cb50a87e-6bcb-434b-926d-bb0f116a3e35" />
      <xplan:position>
        <gml:Point gml:id="Gml_DC3A9238-1BA9-48C5-8AC8-85A86832E640" srsName="EPSG:25832">
          <gml:pos>567069.698 5932767.328</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6c1d6a77-860b-4766-a29b-6e8cebe8e5a8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567417.947 5932680.533</gml:lowerCorner>
          <gml:upperCorner>567436.453 5932706.113</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4E5A1456-45E1-48ED-B8E2-5CDE50F1B61F" srsName="EPSG:25832">
          <gml:posList>567436.453 5932702.751 567423.896 5932706.113 567417.947 5932683.896 
567430.505 5932680.533 567436.453 5932702.751 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_b4620683-ac15-4ed9-a32f-375d18e18624">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567242.452 5932747.803</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932767.583</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1dcc2044-69bd-4b01-ac7c-d0ad489ba723" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_d51ed01f-67ac-4c2c-872e-685a4e3c8a2e" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C6E1161E-E820-4BD9-9DE1-0C47D46B374A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567270.174 5932761.347 567245.957 5932767.583 567242.452 5932754.081 
567266.628 5932747.803 567270.174 5932761.347 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1dcc2044-69bd-4b01-ac7c-d0ad489ba723">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567256.304 5932754.198</gml:lowerCorner>
          <gml:upperCorner>567256.304 5932754.198</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_b4620683-ac15-4ed9-a32f-375d18e18624" />
      <xplan:position>
        <gml:Point gml:id="Gml_60B66911-4480-433D-BE7B-9304C0A6E8AD" srsName="EPSG:25832">
          <gml:pos>567256.304 5932754.198</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d51ed01f-67ac-4c2c-872e-685a4e3c8a2e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567256.304 5932759.992</gml:lowerCorner>
          <gml:upperCorner>567256.304 5932759.992</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_b4620683-ac15-4ed9-a32f-375d18e18624" />
      <xplan:position>
        <gml:Point gml:id="Gml_C633C833-817B-4558-B7E2-9E7001A22860" srsName="EPSG:25832">
          <gml:pos>567256.304 5932759.992</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_2a1a8469-8532-434a-b3d9-6db9061ffa85">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567261.31 5932723.846</gml:lowerCorner>
          <gml:upperCorner>567279.2 5932747.803</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_F20A4D5D-EAA3-4933-BA05-A8B3BC0E8F3A" srsName="EPSG:25832">
          <gml:posList>567279.2 5932744.514 567266.628 5932747.803 567261.31 5932727.488 
567273.793 5932723.846 567279.2 5932744.514 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_fd9e5e86-7496-456e-b965-db4c24d190ff">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567168.773 5932775.928</gml:lowerCorner>
          <gml:upperCorner>567168.773 5932775.928</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_4B7810DA-37FA-43B3-8A10-ACAB636F1B78" srsName="EPSG:25832">
          <gml:pos>567168.773 5932775.928</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_61c8d395-4999-4d8d-9a8c-588a515cdcd5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567258.017 5932707.901</gml:lowerCorner>
          <gml:upperCorner>567285.65 5932727.488</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1100</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_04ecbb85-2406-4654-b074-5843bb62c901" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_644dd5cc-db19-453b-9c87-c73257f9248b" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_98A78531-3DC6-47DE-80B7-C47F42783C1E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567285.65 5932720.382 567273.793 5932723.846 567261.31 5932727.488 
567258.017 5932714.912 567282.014 5932707.901 567285.65 5932720.382 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_04ecbb85-2406-4654-b074-5843bb62c901">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567273.005 5932714.09</gml:lowerCorner>
          <gml:upperCorner>567273.005 5932714.09</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_61c8d395-4999-4d8d-9a8c-588a515cdcd5" />
      <xplan:position>
        <gml:Point gml:id="Gml_1395AE40-6FBD-4C9C-B8CF-60077B4EA050" srsName="EPSG:25832">
          <gml:pos>567273.005 5932714.09</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_644dd5cc-db19-453b-9c87-c73257f9248b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567273.005 5932719.431</gml:lowerCorner>
          <gml:upperCorner>567273.005 5932719.431</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_61c8d395-4999-4d8d-9a8c-588a515cdcd5" />
      <xplan:position>
        <gml:Point gml:id="Gml_1D723717-D041-4940-9466-153175D2F595" srsName="EPSG:25832">
          <gml:pos>567273.005 5932719.431</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_4b599ce4-9ceb-4e5f-bcc2-7e9bf43b2de1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567022.941 5932804.581</gml:lowerCorner>
          <gml:upperCorner>567035.587 5932807.591</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_9BB485E2-3637-4191-88B0-35E73E9E5514" srsName="EPSG:25832">
          <gml:posList>567022.941 5932807.591 567035.587 5932804.581 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7c289604-22fb-4b59-956d-1c7820c71f91">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566899.289 5932793.365</gml:lowerCorner>
          <gml:upperCorner>566926.952 5932810.137</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1100</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_74366e54-6699-4695-968e-3788be24bac1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_1029C7AF-3399-4D85-93C8-27F2F2783B09" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566926.952 5932806.22 566914.1 5932808.177 566901.249 5932810.137 
566900.314 5932804.008 566899.289 5932797.285 566924.003 5932793.516 
566924.991 5932793.365 566926.952 5932806.22 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_74366e54-6699-4695-968e-3788be24bac1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566918.914 5932800.868</gml:lowerCorner>
          <gml:upperCorner>566918.914 5932800.868</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7c289604-22fb-4b59-956d-1c7820c71f91" />
      <xplan:position>
        <gml:Point gml:id="Gml_9B4FF55D-CE9E-4515-A687-810DDC3F1862" srsName="EPSG:25832">
          <gml:pos>566918.914 5932800.868</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_f3a9d9ec-ed46-438b-8787-0767c1d85678">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566916.859 5932822.166</gml:lowerCorner>
          <gml:upperCorner>566944.546 5932841.614</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_58c8e09a-302e-4e3a-8964-91fe6635df83" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6031C419-B534-4BB8-A850-AF3C71A8C1ED" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566944.546 5932837.394 566939.864 5932838.193 566919.2 5932841.614 
566916.859 5932826.266 566941.637 5932822.166 566944.546 5932837.394 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_58c8e09a-302e-4e3a-8964-91fe6635df83">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566929.173 5932832.096</gml:lowerCorner>
          <gml:upperCorner>566929.173 5932832.096</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f3a9d9ec-ed46-438b-8787-0767c1d85678" />
      <xplan:position>
        <gml:Point gml:id="Gml_1B2920A6-32E4-484A-90C5-D8CAF4CF9C87" srsName="EPSG:25832">
          <gml:pos>566929.173 5932832.096</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_d6c03cdf-59ec-438b-b449-7053b93f1495">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566991.754 5932776.34</gml:lowerCorner>
          <gml:upperCorner>567026.182 5932827.172</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_1f6cf107-2977-4732-9401-3a469bfe9bca" />
      <xplan:refTextInhalt xlink:href="#GML_25a71fae-9279-4085-a124-4d507aa5e6ab" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_AE730898-0421-406B-890F-57EABEBFDA44" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567026.182 5932821.212 567013.667 5932824.189 566998.079 5932827.172 
566995.451 5932813.416 566991.754 5932794.066 567004.523 5932791.626 
567002.093 5932778.904 567004.845 5932778.378 567015.503 5932776.34 
567026.182 5932821.212 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_2d2284da-1953-417f-8777-ad3be6806192">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566935.783 5932786.645</gml:lowerCorner>
          <gml:upperCorner>566963.761 5932804.293</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_F20EF58F-9B0C-4DA3-9D16-F01DCC1809CC" srsName="EPSG:25832">
          <gml:posList>566963.761 5932799.414 566950.991 5932801.853 566938.222 5932804.293 
566935.783 5932791.524 566961.321 5932786.645 566963.761 5932799.414 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d076487c-6dfa-49db-84d1-c2e342ccbd3e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567414.585 5932664.872</gml:lowerCorner>
          <gml:upperCorner>567442.096 5932683.896</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1100</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_96bd2a1e-0f12-4269-b10e-837fc9c1af15" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_fff6fa08-f6bd-4a2b-a6b2-f9fee2f33004" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BAEFBCA4-DD93-4413-AC96-32E9C0BF307D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567442.096 5932677.43 567430.505 5932680.533 567417.947 5932683.896 
567414.585 5932671.338 567438.734 5932664.872 567442.096 5932677.43 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_96bd2a1e-0f12-4269-b10e-837fc9c1af15">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.341 5932671.17</gml:lowerCorner>
          <gml:upperCorner>567428.341 5932671.17</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d076487c-6dfa-49db-84d1-c2e342ccbd3e" />
      <xplan:position>
        <gml:Point gml:id="Gml_F55AB04A-6169-407D-8926-6056F2AFD11E" srsName="EPSG:25832">
          <gml:pos>567428.341 5932671.17</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_fff6fa08-f6bd-4a2b-a6b2-f9fee2f33004">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.341 5932676.511</gml:lowerCorner>
          <gml:upperCorner>567428.341 5932676.511</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d076487c-6dfa-49db-84d1-c2e342ccbd3e" />
      <xplan:position>
        <gml:Point gml:id="Gml_934B7B65-1514-455E-96A5-8706C3523AE1" srsName="EPSG:25832">
          <gml:pos>567428.341 5932676.511</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_ca29b001-c537-4294-afe8-21aad4509df5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567181.156 5932714.912</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932781.173</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_ca01c4e0-b15c-4847-b564-5a95d5b0d748" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3eb33633-a610-4de3-a9a1-a3ebdc8b8dd4" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_915778b2-65ac-472f-8c81-7fd17d57d1c1" />
      <xplan:refTextInhalt xlink:href="#GML_1f136ab2-8263-4d60-ba4f-b7b272e78b13" />
      <xplan:refTextInhalt xlink:href="#GML_d99d7f46-18c7-4b7c-87b4-0660884f7022" />
      <xplan:refTextInhalt xlink:href="#GML_3c2d1544-a26d-4788-a7ce-736aff669377" />
      <xplan:refTextInhalt xlink:href="#GML_d2b1d8bb-169a-4ed0-b6dc-e310692d3257" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_24813ba0-c9b0-4655-9ccd-42baad49dcbf" />
      <xplan:refTextInhalt xlink:href="#GML_967d2f7e-c198-4539-96a9-85ed0a81d4a2" />
      <xplan:refTextInhalt xlink:href="#GML_7f160780-9db6-4be1-8ed3-11df2d255806" />
      <xplan:refTextInhalt xlink:href="#GML_27e1d5c5-2e37-42be-b0be-bf85a23883d5" />
      <xplan:refTextInhalt xlink:href="#GML_b0540621-d713-4e0d-8f09-9f70e1771e40" />
      <xplan:refTextInhalt xlink:href="#GML_60885b8d-ed98-44c0-859b-e937a34d45ab" />
      <xplan:refTextInhalt xlink:href="#GML_c3cb239b-32bc-4361-9e35-d4869768a4a6" />
      <xplan:refTextInhalt xlink:href="#GML_de927319-5336-47ea-9a54-270bedb07ca7" />
      <xplan:refTextInhalt xlink:href="#GML_4360595b-e82a-4207-84b1-713aaefab5da" />
      <xplan:refTextInhalt xlink:href="#GML_bd4953ce-ce43-47f3-90c8-f8b4cec45f43" />
      <xplan:refTextInhalt xlink:href="#GML_10f35421-3c0d-4867-9624-93d58f4a85be" />
      <xplan:refTextInhalt xlink:href="#GML_25a71fae-9279-4085-a124-4d507aa5e6ab" />
      <xplan:refTextInhalt xlink:href="#GML_3b43fd0e-dc9d-4c85-b8ed-d6a0086631fd" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B37619FB-3E62-49CE-97DC-FFD8C0882A13" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567270.174 5932761.347 567245.957 5932767.583 567193.176 5932781.173 
567181.156 5932734.868 567258.017 5932714.912 567270.174 5932761.347 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.8</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ca01c4e0-b15c-4847-b564-5a95d5b0d748">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567246.427 5932738.9</gml:lowerCorner>
          <gml:upperCorner>567246.427 5932738.9</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ca29b001-c537-4294-afe8-21aad4509df5" />
      <xplan:position>
        <gml:Point gml:id="Gml_4B7BB822-690E-4202-90AB-5FAE7800DA8D" srsName="EPSG:25832">
          <gml:pos>567246.427 5932738.9</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3eb33633-a610-4de3-a9a1-a3ebdc8b8dd4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567247.247 5932745.454</gml:lowerCorner>
          <gml:upperCorner>567247.247 5932745.454</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ca29b001-c537-4294-afe8-21aad4509df5" />
      <xplan:position>
        <gml:Point gml:id="Gml_1D8954F0-EE65-45BF-9804-0DD47D7C44B0" srsName="EPSG:25832">
          <gml:pos>567247.247 5932745.454</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_66de4c58-8475-4f9a-b839-f9f354afe301">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567423.896 5932654.275</gml:lowerCorner>
          <gml:upperCorner>567481.725 5932719.637</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5b958bf4-f969-44c1-8af2-94f5004fc446" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3085cdba-43bb-4dcb-abd6-ed87a0ee9dfb" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_412036B7-F279-4CB6-BA06-0AD2E0319845" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567481.725 5932666.819 567470.593 5932669.8 567480.162 5932705.541 
567427.517 5932719.637 567423.896 5932706.113 567436.453 5932702.751 
567463.984 5932695.38 567454.673 5932660.605 567478.32 5932654.275 
567481.725 5932666.819 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5b958bf4-f969-44c1-8af2-94f5004fc446">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567452.029 5932702.327</gml:lowerCorner>
          <gml:upperCorner>567452.029 5932702.327</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_66de4c58-8475-4f9a-b839-f9f354afe301" />
      <xplan:position>
        <gml:Point gml:id="Gml_883AB836-2C39-4047-8134-C31BFE3E2E19" srsName="EPSG:25832">
          <gml:pos>567452.029 5932702.327</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3085cdba-43bb-4dcb-abd6-ed87a0ee9dfb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567452.029 5932708.121</gml:lowerCorner>
          <gml:upperCorner>567452.029 5932708.121</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_66de4c58-8475-4f9a-b839-f9f354afe301" />
      <xplan:position>
        <gml:Point gml:id="Gml_950E79EF-2EDF-43FB-A93D-0ED9C4F9D99F" srsName="EPSG:25832">
          <gml:pos>567452.029 5932708.121</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_396a4b8e-9b41-4655-a364-270b1adde6e4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567100.317 5932741.64</gml:lowerCorner>
          <gml:upperCorner>567155.075 5932802.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0d3a599d-cf38-4a5d-8928-03618d9e4e1c" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_394bfe1f-35ac-4287-8b60-12e036f01cb6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2EFD3AF2-4CE0-42FD-8F21-F1B789E22FA9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567154.527 5932745.398 567149.128 5932791.971 567103.525 5932802.817 
567100.317 5932789.338 567113.013 5932786.154 567137.502 5932780.326 
567141.581 5932745.143 567155.075 5932741.64 567154.527 5932745.398 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0d3a599d-cf38-4a5d-8928-03618d9e4e1c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567128.362 5932786.204</gml:lowerCorner>
          <gml:upperCorner>567128.362 5932786.204</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_396a4b8e-9b41-4655-a364-270b1adde6e4" />
      <xplan:position>
        <gml:Point gml:id="Gml_13F9417B-27E9-45EE-9FE4-36038723E82B" srsName="EPSG:25832">
          <gml:pos>567128.362 5932786.204</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_394bfe1f-35ac-4287-8b60-12e036f01cb6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567128.362 5932791.999</gml:lowerCorner>
          <gml:upperCorner>567128.362 5932791.999</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_396a4b8e-9b41-4655-a364-270b1adde6e4" />
      <xplan:position>
        <gml:Point gml:id="Gml_99DC91E8-A6C9-44E9-B9EF-3FAA79AF6207" srsName="EPSG:25832">
          <gml:pos>567128.362 5932791.999</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Wasserrecht gml:id="GML_650fa2db-25da-4d3b-beec-23ecd390ddd7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566839.305 5932624.6</gml:lowerCorner>
          <gml:upperCorner>567478.319 5932805.784</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Hochwassergefährdeter Bereich</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_ecdadf5a-49dd-4414-8002-6294054f6e98" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BB5177C9-42C0-4C2C-8CF2-A34557B23B8B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567038.607 5932771.88 567004.903 5932778.319 566935.784 5932791.524 
566924.003 5932793.516 566858.263 5932803.542 566842.603 5932805.784 
566841.516 5932797.098 566841.353 5932795.811 566841.322 5932795.563 
566839.305 5932779.633 566885.545 5932770.145 566933.255 5932760.356 
567084.333 5932725.96 567189.373 5932700.56 567374.73 5932651.63 
567470.445 5932624.6 567474.886 5932638.488 567474.962 5932638.726 
567477.061 5932645.289 567475.978 5932645.649 567478.319 5932654.273 
567325.715 5932695.132 567291.866 5932705.022 567258.017 5932714.912 
567181.156 5932734.868 567182.755 5932741.028 567172.637 5932743.881 
567171.049 5932756.863 567159.379 5932755.521 567161.242 5932742.524 
567155.075 5932741.64 567154.642 5932745.368 567117.89 5932754.911 
567117.01 5932751.523 567038.607 5932771.88 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:artDerFestlegung>20002</xplan:artDerFestlegung>
    </xplan:SO_Wasserrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_d0a6a6e6-1ed9-4e21-8148-2ee1a8b6d50b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567423.896 5932654.275</gml:lowerCorner>
          <gml:upperCorner>567481.725 5932719.637</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_546EC928-76F2-4FFC-88D4-32AA17EC86C7" srsName="EPSG:25832">
          <gml:posList>567427.517 5932719.637 567423.896 5932706.113 567436.453 5932702.751 
567463.984 5932695.38 567454.673 5932660.605 567478.32 5932654.275 
567481.725 5932666.819 567470.593 5932669.8 567480.162 5932705.541 
567427.517 5932719.637 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7a92e329-04b9-4a0d-bec0-34420e31cd4a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566875.105 5932804.008</gml:lowerCorner>
          <gml:upperCorner>566901.745 5932817.244</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9edf8894-bc32-484b-8f9a-6df0efa926a0" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6FB8D03A-9F69-47C3-9D30-16D3DF4E6D89" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566901.249 5932810.137 566901.745 5932813.399 566876.538 5932817.244 
566875.105 5932807.852 566900.314 5932804.008 566901.249 5932810.137 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9edf8894-bc32-484b-8f9a-6df0efa926a0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566888.426 5932810.626</gml:lowerCorner>
          <gml:upperCorner>566888.426 5932810.626</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7a92e329-04b9-4a0d-bec0-34420e31cd4a" />
      <xplan:position>
        <gml:Point gml:id="Gml_8DB7D77A-0F4D-457C-9C24-7159E143FE23" srsName="EPSG:25832">
          <gml:pos>566888.426 5932810.626</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="GML_448d3424-a399-4060-a407-b77dcc907bcc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566841.322 5932638.488</gml:lowerCorner>
          <gml:upperCorner>567509.287 5932905.509</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_1b203c2f-d6ba-43d3-b73e-3cf67a00bb26" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_EF61C94D-5DCC-46F7-B4E5-972A14B78FCB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567071.225 5932875.978 566952.478 5932905.509 566925.077 5932857.666 
566850.623 5932868.84 566848.591 5932852.383 566842.603 5932805.784 
566841.516 5932797.098 566841.322 5932795.563 566856.166 5932793.67 
566876.457 5932790.784 566893.555 5932788.149 566925.816 5932782.812 
566925.622 5932781.675 566957.155 5932775.947 567001.253 5932766.989 
567024.668 5932762.288 567034.553 5932760.037 567099.41 5932743.34 
567165.898 5932726.231 567207.598 5932715.718 567251.407 5932703.902 
567309.648 5932687.426 567309.188 5932685.742 567324.249 5932681.582 
567379.905 5932665.733 567462.605 5932641.979 567474.886 5932638.488 
567477.061 5932645.289 567475.978 5932645.649 567478.319 5932654.273 
567481.725 5932666.819 567491.419 5932702.527 567500.798 5932737.377 
567502.35 5932743.169 567509.287 5932769.105 567416.044 5932790.244 
567354.506 5932803.777 567322.897 5932810.727 567243.981 5932831.427 
567071.225 5932875.978 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>4000</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_7783e4b1-3338-4928-9dc4-8f62f4daa301">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566914.1 5932791.571</gml:lowerCorner>
          <gml:upperCorner>566941.637 5932826.266</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_1f6cf107-2977-4732-9401-3a469bfe9bca" />
      <xplan:refTextInhalt xlink:href="#GML_25a71fae-9279-4085-a124-4d507aa5e6ab" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_58BCEE7D-D6E7-4B26-A76B-1C093A03D324" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566938.222 5932804.293 566941.637 5932822.166 566916.859 5932826.266 
566914.1 5932808.177 566926.952 5932806.22 566924.995 5932793.392 
566925.489 5932793.308 566935.792 5932791.571 566938.222 5932804.293 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_2eba39b8-a88d-462d-b5dd-6d2e974e420c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567347.101 5932712.579</gml:lowerCorner>
          <gml:upperCorner>567403.367 5932740.198</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5ee3b068-e31a-4127-a41a-f4dfa6f56fbf" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b57fb67a-2217-4aa6-906b-40717787c8e9" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C5C03F63-D509-4CBE-8261-416EB90EF74D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567403.367 5932726.103 567350.721 5932740.198 567347.101 5932726.674 
567359.658 5932723.312 567387.189 5932715.941 567399.746 5932712.579 
567403.367 5932726.103 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5ee3b068-e31a-4127-a41a-f4dfa6f56fbf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567375.234 5932728.683</gml:lowerCorner>
          <gml:upperCorner>567375.234 5932728.683</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_2eba39b8-a88d-462d-b5dd-6d2e974e420c" />
      <xplan:position>
        <gml:Point gml:id="Gml_09F9DB8F-2A62-463C-A482-3D404B53DBEB" srsName="EPSG:25832">
          <gml:pos>567375.234 5932728.683</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b57fb67a-2217-4aa6-906b-40717787c8e9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567375.234 5932722.888</gml:lowerCorner>
          <gml:upperCorner>567375.234 5932722.888</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_2eba39b8-a88d-462d-b5dd-6d2e974e420c" />
      <xplan:position>
        <gml:Point gml:id="Gml_853509EE-C877-42A2-8D92-A5F4AA49C3E5" srsName="EPSG:25832">
          <gml:pos>567375.234 5932722.888</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_c2410e2e-515c-4957-bc9a-d2d17ab1c20d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567092.822 5932751.558</gml:lowerCorner>
          <gml:upperCorner>567120.277 5932770.455</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_9C1C17A0-2686-49D2-8F83-D9AC020C0E32" srsName="EPSG:25832">
          <gml:posList>567120.277 5932764.106 567108.409 5932767.187 567095.823 5932770.455 
567092.822 5932757.841 567117.019 5932751.558 567120.277 5932764.106 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_e0d97f17-3d29-4a3b-a358-acb94ddfbb0e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567461.102 5932715.143</gml:lowerCorner>
          <gml:upperCorner>567461.102 5932715.143</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.75</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_9C5423F9-A0A3-4055-9AD2-8D268F6B2697" srsName="EPSG:25832">
          <gml:pos>567461.102 5932715.143</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_3095c3b2-bf9a-4caa-b770-923851df098f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566876.538 5932813.399</gml:lowerCorner>
          <gml:upperCorner>566904.004 5932832.072</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_CEDC30FB-B541-4EF4-973F-22F60C78E55A" srsName="EPSG:25832">
          <gml:posList>566904.004 5932828.228 566878.799 5932832.072 566876.538 5932817.244 
566901.745 5932813.399 566904.004 5932828.228 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_237802ac-d7ee-44fc-b9f6-786766f89ae6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566869.36 5932853.767</gml:lowerCorner>
          <gml:upperCorner>566869.36 5932853.767</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.6</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_08AD4CFB-4ECC-4E3E-A62E-7346DE31AD77" srsName="EPSG:25832">
          <gml:pos>566869.36 5932853.767</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_7e9f592e-57fd-4b74-9606-9b669c15ffe3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566858.112 5932800.141</gml:lowerCorner>
          <gml:upperCorner>566874.087 5932803.585</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">8.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_9E38FBBB-D1C4-43FC-8BC2-5F8E87FD70E5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566874.087 5932801.172 566858.269 5932803.585 566858.112 5932802.554 
566873.929 5932800.141 566874.087 5932801.172 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_dc3c8dd9-0b31-4034-a89f-a54b23e92efc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566930.194 5932737.377</gml:lowerCorner>
          <gml:upperCorner>567500.798 5932866.541</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4F1EF65D-040F-49B2-A40E-30A5D0323CEB" srsName="EPSG:25832">
          <gml:posList>567500.798 5932737.377 567446.666 5932750.517 567434.247 5932753.531 
567393.238 5932763.716 567365.696 5932770.815 567297.461 5932789.308 
567270.403 5932796.898 567224.067 5932809.859 567224.066 5932809.858 
567177.219 5932822.041 567141.654 5932830.401 567119.92 5932835.137 
567056.168 5932847.424 567014.704 5932854.187 566973.112 5932860.188 
566930.194 5932866.541 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c175151e-ccba-4b9c-b4a6-baa2511b1b03">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566938.222 5932801.853</gml:lowerCorner>
          <gml:upperCorner>566998.078 5932837.394</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_83b268c1-f0d9-4b73-8ce5-d37f49bd43fb" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c1a8e5fd-693c-48fc-99f1-877ca595d0c1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_348D88B0-07AC-4675-85F4-140B6AF48CA7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566998.078 5932827.167 566944.546 5932837.394 566938.222 5932804.293 
566950.991 5932801.853 566954.688 5932821.203 566982.682 5932815.855 
566995.451 5932813.416 566998.078 5932827.167 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_83b268c1-f0d9-4b73-8ce5-d37f49bd43fb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566969.999 5932827.699</gml:lowerCorner>
          <gml:upperCorner>566969.999 5932827.699</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c175151e-ccba-4b9c-b4a6-baa2511b1b03" />
      <xplan:position>
        <gml:Point gml:id="Gml_3E5EEA85-4EC8-4FF1-8A97-B0A21F70C15C" srsName="EPSG:25832">
          <gml:pos>566969.999 5932827.699</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c1a8e5fd-693c-48fc-99f1-877ca595d0c1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566969.999 5932821.905</gml:lowerCorner>
          <gml:upperCorner>566969.999 5932821.905</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c175151e-ccba-4b9c-b4a6-baa2511b1b03" />
      <xplan:position>
        <gml:Point gml:id="Gml_277FBBBC-BA28-43B6-8E73-A85A5B60B698" srsName="EPSG:25832">
          <gml:pos>566969.999 5932821.905</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_fa39ceab-4d83-45a4-b6ec-faffeddcee69">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567258.017 5932707.901</gml:lowerCorner>
          <gml:upperCorner>567285.65 5932727.488</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_2B3E89DC-54FD-4B18-BA53-AF03B2AA9854" srsName="EPSG:25832">
          <gml:posList>567273.793 5932723.846 567261.31 5932727.488 567258.017 5932714.912 
567282.014 5932707.901 567285.65 5932720.382 567273.793 5932723.846 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_da819480-9342-4af1-8f68-b1ea4c5eeae5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567162.168 5932733.046</gml:lowerCorner>
          <gml:upperCorner>567162.168 5932733.046</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">5.4</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_41F6158E-A896-452D-9AF5-CA202F8EEE34" srsName="EPSG:25832">
          <gml:pos>567162.168 5932733.046</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_4ff8a367-a539-4c50-9988-236d90236798">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566916.859 5932822.968</gml:lowerCorner>
          <gml:upperCorner>566939.864 5932841.614</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_2C5FD064-148D-4057-8649-D68BF21D664F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566939.864 5932838.193 566919.2 5932841.614 566916.859 5932826.266 
566936.789 5932822.968 566939.864 5932838.193 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_2a85e360-97a9-4bc4-80ec-b0afd8096ecd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566875.105 5932804.008</gml:lowerCorner>
          <gml:upperCorner>566901.745 5932817.244</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_6DFCB8AE-ECCE-4A69-A665-B12F6D5748EF" srsName="EPSG:25832">
          <gml:posList>566901.745 5932813.399 566876.538 5932817.244 566875.105 5932807.852 
566900.314 5932804.008 566901.249 5932810.137 566901.745 5932813.399 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_0dd72b33-c610-471c-84ef-b4c3f7420cd9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567075.983 5932789.185</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932808.602</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_C44125F8-E461-4E35-895A-4BB2A5ED0B47" srsName="EPSG:25832">
          <gml:posList>567103.525 5932802.817 567079.203 5932808.602 567075.983 5932794.967 
567100.281 5932789.185 567103.525 5932802.817 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_6d511353-1dd7-4ce2-a68d-104fc2e1ddb3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566976.546 5932778.857</gml:lowerCorner>
          <gml:upperCorner>567004.523 5932796.505</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1100</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_78fd6ab0-d51a-4424-8d6c-e65ca3db859c" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_74E78442-84B0-46D1-8873-F753ED3558E5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567004.523 5932791.626 566991.754 5932794.066 566978.985 5932796.505 
566976.546 5932783.737 567002.084 5932778.857 567004.523 5932791.626 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_78fd6ab0-d51a-4424-8d6c-e65ca3db859c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566990.534 5932787.681</gml:lowerCorner>
          <gml:upperCorner>566990.534 5932787.681</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6d511353-1dd7-4ce2-a68d-104fc2e1ddb3" />
      <xplan:position>
        <gml:Point gml:id="Gml_6B68D6EC-7D4E-4A01-BF89-D3B284A3CED0" srsName="EPSG:25832">
          <gml:pos>566990.534 5932787.681</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_c1a63310-c2fd-4744-aa2a-ad679ead56e0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567056.034 5932761.11</gml:lowerCorner>
          <gml:upperCorner>567083.489 5932780.007</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_354C774B-CD31-49FC-B0BB-ABA076D3D21E" srsName="EPSG:25832">
          <gml:posList>567083.489 5932773.657 567070.652 5932776.99 567059.035 5932780.007 
567056.034 5932767.393 567080.231 5932761.11 567083.489 5932773.657 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_4deccb28-239e-4bf5-9192-de849c58ed27">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567258.017 5932714.912</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932761.347</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_9DADA0E9-CB01-4706-9B50-A2B2F7C34F38" srsName="EPSG:25832">
          <gml:posList>567270.174 5932761.347 567258.017 5932714.912 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_a9bf7239-bec5-44c9-be44-c3f3a702a3ce">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567414.585 5932664.872</gml:lowerCorner>
          <gml:upperCorner>567442.096 5932683.896</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4A008EA5-B2E1-42CA-88ED-86167543B388" srsName="EPSG:25832">
          <gml:posList>567430.505 5932680.533 567417.947 5932683.896 567414.585 5932671.338 
567438.734 5932664.872 567442.096 5932677.43 567430.505 5932680.533 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_cf435a9f-da5f-4bfa-86b6-8e7227d9a307">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567266.628 5932734.009</gml:lowerCorner>
          <gml:upperCorner>567322.893 5932761.347</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f15dfde7-bb6a-49d3-9cb7-3097e47ce71b" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7d0d67af-5c61-49b2-8649-390dc3ef87a1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F2E40E88-018B-4245-A795-856E656C4FDC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567322.893 5932747.553 567270.174 5932761.347 567266.628 5932747.803 
567279.2 5932744.514 567306.772 5932737.3 567319.349 5932734.009 
567322.893 5932747.553 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f15dfde7-bb6a-49d3-9cb7-3097e47ce71b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567294.761 5932744.178</gml:lowerCorner>
          <gml:upperCorner>567294.761 5932744.178</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_cf435a9f-da5f-4bfa-86b6-8e7227d9a307" />
      <xplan:position>
        <gml:Point gml:id="Gml_9ECFA23B-70A8-4C56-B3CD-D6D1B2E718CB" srsName="EPSG:25832">
          <gml:pos>567294.761 5932744.178</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7d0d67af-5c61-49b2-8649-390dc3ef87a1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567294.761 5932749.973</gml:lowerCorner>
          <gml:upperCorner>567294.761 5932749.973</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_cf435a9f-da5f-4bfa-86b6-8e7227d9a307" />
      <xplan:position>
        <gml:Point gml:id="Gml_6B69B4C3-1481-464E-AE8B-77A9C9A112C6" srsName="EPSG:25832">
          <gml:pos>567294.761 5932749.973</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c7f09eff-61af-4ce5-92d4-768a4ffcba51">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567059.035 5932776.739</gml:lowerCorner>
          <gml:upperCorner>567075.96 5932797.983</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1100</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_dc934fb5-7110-4036-a2d7-8f85df59c512" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0f3ee005-1a5b-4f97-86e6-3abe44fb35d5" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F22CD9C6-E05A-4831-A22F-DF77C65BEBD5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567075.96 5932794.973 567063.313 5932797.983 567059.035 5932780.007 
567070.652 5932776.99 567071.62 5932776.739 567075.96 5932794.973 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_dc934fb5-7110-4036-a2d7-8f85df59c512">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567067.482 5932789.553</gml:lowerCorner>
          <gml:upperCorner>567067.482 5932789.553</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c7f09eff-61af-4ce5-92d4-768a4ffcba51" />
      <xplan:position>
        <gml:Point gml:id="Gml_916B68DD-E88E-4DEF-A058-1124F3582D0D" srsName="EPSG:25832">
          <gml:pos>567067.482 5932789.553</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0f3ee005-1a5b-4f97-86e6-3abe44fb35d5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567067.482 5932784.212</gml:lowerCorner>
          <gml:upperCorner>567067.482 5932784.212</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c7f09eff-61af-4ce5-92d4-768a4ffcba51" />
      <xplan:position>
        <gml:Point gml:id="Gml_0861A8E6-16C5-4370-88E9-C164662A1E61" srsName="EPSG:25832">
          <gml:pos>567067.482 5932784.212</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_43b9d9d0-c9b7-4bab-8d4a-83ff6816ac9a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567381.24 5932690.361</gml:lowerCorner>
          <gml:upperCorner>567399.746 5932715.941</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_5BBB8793-EFF0-4391-B83E-1851A4865BE1" srsName="EPSG:25832">
          <gml:posList>567399.746 5932712.579 567387.189 5932715.941 567381.24 5932693.724 
567393.798 5932690.361 567399.746 5932712.579 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_31cd27db-f486-48dc-95da-b11febdb0c3b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567075.983 5932789.185</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932808.602</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_605ae52a-fe59-4132-b238-4d94f704e0a0" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_95069cea-fde7-4685-a9a7-5134604c24f0" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_44AF8D50-7265-4036-A2A5-19D143E66D10" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567103.525 5932802.817 567079.203 5932808.602 567075.983 5932794.967 
567100.281 5932789.185 567103.525 5932802.817 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_605ae52a-fe59-4132-b238-4d94f704e0a0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567086.764 5932797.583</gml:lowerCorner>
          <gml:upperCorner>567086.764 5932797.583</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_31cd27db-f486-48dc-95da-b11febdb0c3b" />
      <xplan:position>
        <gml:Point gml:id="Gml_8C8B9065-9EC3-48B4-B102-6D6295E13249" srsName="EPSG:25832">
          <gml:pos>567086.764 5932797.583</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_95069cea-fde7-4685-a9a7-5134604c24f0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567083.063 5932803.424</gml:lowerCorner>
          <gml:upperCorner>567083.063 5932803.424</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_31cd27db-f486-48dc-95da-b11febdb0c3b" />
      <xplan:position>
        <gml:Point gml:id="Gml_B83B0FAB-1E36-448F-B07A-D6C665C6FEED" srsName="EPSG:25832">
          <gml:pos>567083.063 5932803.424</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_5cccc7b9-584a-4411-a2cf-2666e263e1a5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567417.947 5932680.533</gml:lowerCorner>
          <gml:upperCorner>567436.453 5932706.113</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1100</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b6087a6b-8ef1-41c8-972c-8a2f57b5b57b" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f46e10ce-6410-4874-adae-6b93d5c1ee68" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_DAE5DF61-4D3A-4293-B237-109B8084586D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567436.453 5932702.751 567423.896 5932706.113 567417.947 5932683.896 
567430.505 5932680.533 567436.453 5932702.751 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b6087a6b-8ef1-41c8-972c-8a2f57b5b57b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567427.2 5932690.109</gml:lowerCorner>
          <gml:upperCorner>567427.2 5932690.109</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_5cccc7b9-584a-4411-a2cf-2666e263e1a5" />
      <xplan:position>
        <gml:Point gml:id="Gml_8F7ADF9F-91A3-4364-8B04-AA96CF98469A" srsName="EPSG:25832">
          <gml:pos>567427.2 5932690.109</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f46e10ce-6410-4874-adae-6b93d5c1ee68">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567427.2 5932695.45</gml:lowerCorner>
          <gml:upperCorner>567427.2 5932695.45</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_5cccc7b9-584a-4411-a2cf-2666e263e1a5" />
      <xplan:position>
        <gml:Point gml:id="Gml_4BAE7A08-7FBF-4B6B-A696-3562A472DEF5" srsName="EPSG:25832">
          <gml:pos>567427.2 5932695.45</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_65a932e6-2b7e-49ca-aca3-abb276f6214d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567411.526 5932706.113</gml:lowerCorner>
          <gml:upperCorner>567427.516 5932722.929</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">8</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_A376954E-46E6-4E79-8D06-1CA607AE1D40" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567427.516 5932719.637 567415.221 5932722.929 567411.526 5932709.425 
567423.896 5932706.113 567427.516 5932719.637 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_882980c4-52c5-4bef-bc65-b89f01190809">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567337.79 5932688.537</gml:lowerCorner>
          <gml:upperCorner>567359.658 5932726.674</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(F)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_4bb328be-269b-4047-b041-75c3945fb0e7" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_8ACDA771-750E-4BE9-A61F-EC6F650265C7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567359.658 5932723.312 567347.101 5932726.674 567337.79 5932691.899 
567350.348 5932688.537 567359.658 5932723.312 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_4bb328be-269b-4047-b041-75c3945fb0e7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567348.724 5932707.606</gml:lowerCorner>
          <gml:upperCorner>567348.724 5932707.606</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_882980c4-52c5-4bef-bc65-b89f01190809" />
      <xplan:position>
        <gml:Point gml:id="Gml_ACB1EDD6-699E-4618-831F-92F10FFB1DEE" srsName="EPSG:25832">
          <gml:pos>567348.724 5932707.606</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7f2cbfaa-c9d2-435d-b1fc-2044bc03e30e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567181.156 5932731.601</gml:lowerCorner>
          <gml:upperCorner>567245.957 5932781.173</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_16bc1466-4f4f-4483-8cd1-c85f2489997a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c92b8f46-1e04-4d25-84c0-16b25b34edf2" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0504879E-2159-46B7-8FC9-988111098F86" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567245.957 5932767.583 567193.176 5932781.173 567181.156 5932734.868 
567193.739 5932731.601 567202.283 5932764.51 567229.869 5932757.348 
567224.591 5932737.022 567237.174 5932733.755 567245.957 5932767.583 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_16bc1466-4f4f-4483-8cd1-c85f2489997a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567217.826 5932764.134</gml:lowerCorner>
          <gml:upperCorner>567217.826 5932764.134</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7f2cbfaa-c9d2-435d-b1fc-2044bc03e30e" />
      <xplan:position>
        <gml:Point gml:id="Gml_29667180-736B-4E4E-AD0D-74E1CBD014E7" srsName="EPSG:25832">
          <gml:pos>567217.826 5932764.134</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c92b8f46-1e04-4d25-84c0-16b25b34edf2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567217.826 5932769.928</gml:lowerCorner>
          <gml:upperCorner>567217.826 5932769.928</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7f2cbfaa-c9d2-435d-b1fc-2044bc03e30e" />
      <xplan:position>
        <gml:Point gml:id="Gml_FC677752-CD39-4AA1-A88C-8D489DA54E71" srsName="EPSG:25832">
          <gml:pos>567217.826 5932769.928</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_af9af125-5727-4cc2-bf81-707298b0e96d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566858.112 5932800.141</gml:lowerCorner>
          <gml:upperCorner>566919.2 5932849.831</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9e252d7d-380f-49f0-8718-c93163053c0b" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_702512ea-8cfd-4ebe-931f-1da28fb187ca" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_8D5AE65C-69F4-4EFC-A59A-60EF03B23DCB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566919.2 5932841.614 566865.323 5932849.831 566858.112 5932802.554 
566873.929 5932800.141 566878.799 5932832.072 566904.004 5932828.228 
566901.249 5932810.137 566914.1 5932808.177 566919.2 5932841.614 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9e252d7d-380f-49f0-8718-c93163053c0b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566869.626 5932821.486</gml:lowerCorner>
          <gml:upperCorner>566869.626 5932821.486</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_af9af125-5727-4cc2-bf81-707298b0e96d" />
      <xplan:position>
        <gml:Point gml:id="Gml_6DB58551-B06A-46E4-9C37-837C05029A64" srsName="EPSG:25832">
          <gml:pos>566869.626 5932821.486</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_702512ea-8cfd-4ebe-931f-1da28fb187ca">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566869.626 5932827.281</gml:lowerCorner>
          <gml:upperCorner>566869.626 5932827.281</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_af9af125-5727-4cc2-bf81-707298b0e96d" />
      <xplan:position>
        <gml:Point gml:id="Gml_C2782881-5A5A-49F9-A173-FBB7EDB7027A" srsName="EPSG:25832">
          <gml:pos>566869.626 5932827.281</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_c12222f8-99f2-40d0-a00c-f8ceae794f2a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567399.746 5932706.113</gml:lowerCorner>
          <gml:upperCorner>567427.517 5932726.103</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_EDE9B6C7-80F8-4BAD-921E-822941B60D82" srsName="EPSG:25832">
          <gml:posList>567427.517 5932719.637 567403.367 5932726.103 567399.746 5932712.579 
567423.896 5932706.113 567427.517 5932719.637 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_1358c7ad-9cc6-4765-8cdd-0743e4afdeff">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567377.878 5932674.7</gml:lowerCorner>
          <gml:upperCorner>567405.389 5932693.724</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1100</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_805db65c-eeb8-46f0-aa9c-70afc20d1e9b" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3bea4192-8397-4d1f-9d4b-50128b15abc6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2C750FEC-B1F0-4A46-94AC-D2AC22906720" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567405.389 5932687.258 567393.798 5932690.361 567381.24 5932693.724 
567377.878 5932681.166 567402.027 5932674.7 567405.389 5932687.258 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_805db65c-eeb8-46f0-aa9c-70afc20d1e9b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567391.634 5932680.998</gml:lowerCorner>
          <gml:upperCorner>567391.634 5932680.998</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1358c7ad-9cc6-4765-8cdd-0743e4afdeff" />
      <xplan:position>
        <gml:Point gml:id="Gml_5C088E0D-CAF7-4B77-844B-BD2E2AE0B09E" srsName="EPSG:25832">
          <gml:pos>567391.634 5932680.998</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3bea4192-8397-4d1f-9d4b-50128b15abc6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567391.634 5932686.339</gml:lowerCorner>
          <gml:upperCorner>567391.634 5932686.339</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1358c7ad-9cc6-4765-8cdd-0743e4afdeff" />
      <xplan:position>
        <gml:Point gml:id="Gml_304EBB16-9AAE-4848-825E-B03838AF0AB1" srsName="EPSG:25832">
          <gml:pos>567391.634 5932686.339</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_c9f973a9-e612-48db-84c2-27f2249ba674">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567059.035 5932776.739</gml:lowerCorner>
          <gml:upperCorner>567075.96 5932797.983</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B59AE904-F9E1-4372-BD1E-2CB34D8BD9EC" srsName="EPSG:25832">
          <gml:posList>567075.96 5932794.973 567063.313 5932797.983 567059.035 5932780.007 
567070.652 5932776.99 567071.62 5932776.739 567075.96 5932794.973 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c7dcbb99-916c-4351-9ded-bb7cb343c5b5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566935.783 5932786.645</gml:lowerCorner>
          <gml:upperCorner>566963.761 5932804.293</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1100</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_6005068f-2d54-4e55-955b-9565e9adba07" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BD692C15-CC9B-400C-BF0F-E2AA0FC95A94" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566963.761 5932799.414 566950.991 5932801.853 566938.222 5932804.293 
566935.783 5932791.524 566961.321 5932786.645 566963.761 5932799.414 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_6005068f-2d54-4e55-955b-9565e9adba07">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566949.772 5932795.469</gml:lowerCorner>
          <gml:upperCorner>566949.772 5932795.469</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c7dcbb99-916c-4351-9ded-bb7cb343c5b5" />
      <xplan:position>
        <gml:Point gml:id="Gml_C98FEF5B-B278-4599-9D27-5CC25F7186CF" srsName="EPSG:25832">
          <gml:pos>566949.772 5932795.469</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_28258ff8-f5aa-4855-bde5-66baedd98f9c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567297.868 5932696.257</gml:lowerCorner>
          <gml:upperCorner>567325.501 5932737.3</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(F)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2c74db1d-8339-499d-b144-983eca85ac3a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_00F75C26-7015-4C6E-8747-066C31C4D50A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567325.501 5932708.738 567313.643 5932712.203 567319.349 5932734.009 
567306.772 5932737.3 567297.868 5932703.269 567321.864 5932696.257 
567325.501 5932708.738 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2c74db1d-8339-499d-b144-983eca85ac3a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567311.6 5932716.843</gml:lowerCorner>
          <gml:upperCorner>567311.6 5932716.843</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_28258ff8-f5aa-4855-bde5-66baedd98f9c" />
      <xplan:position>
        <gml:Point gml:id="Gml_A6C5B310-A003-4FBC-A5E8-348829CBE6AE" srsName="EPSG:25832">
          <gml:pos>567311.6 5932716.843</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_302e44ae-b842-4db6-bf1e-dcb482d9b2b8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567141.149 5932741.64</gml:lowerCorner>
          <gml:upperCorner>567155.075 5932748.872</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">4.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_C5919861-711F-4EFD-9376-E0EF12D01DE8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567154.527 5932745.398 567141.149 5932748.872 567141.581 5932745.143 
567155.075 5932741.64 567154.527 5932745.398 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6ccdea5e-2740-4a8b-86ec-eef6b0d443de">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.503 5932773.896</gml:lowerCorner>
          <gml:upperCorner>567079.203 5932821.212</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_A0474085-C33C-4DD3-B8F5-3C84BD5A4A0A" srsName="EPSG:25832">
          <gml:posList>567079.203 5932808.602 567026.182 5932821.212 567022.941 5932807.591 
567015.503 5932776.34 567028.284 5932773.896 567035.587 5932804.581 
567063.313 5932797.983 567075.983 5932794.967 567079.203 5932808.602 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_9d47fb51-694e-408f-9da8-d26c9c249c47">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567092.822 5932751.558</gml:lowerCorner>
          <gml:upperCorner>567120.277 5932770.455</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1100</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f1707941-0a6d-4480-a044-ef5306e8d416" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e9c977e3-3ec7-4d3a-949b-e1f4f5b039cf" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2F102EB0-AFD0-47CA-B423-0FD019114D20" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567120.277 5932764.106 567108.409 5932767.187 567095.823 5932770.455 
567092.822 5932757.841 567117.019 5932751.558 567120.277 5932764.106 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f1707941-0a6d-4480-a044-ef5306e8d416">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567106.486 5932763.117</gml:lowerCorner>
          <gml:upperCorner>567106.486 5932763.117</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9d47fb51-694e-408f-9da8-d26c9c249c47" />
      <xplan:position>
        <gml:Point gml:id="Gml_FD803C33-ED4D-46A7-91A0-22A568373F7C" srsName="EPSG:25832">
          <gml:pos>567106.486 5932763.117</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e9c977e3-3ec7-4d3a-949b-e1f4f5b039cf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567106.486 5932757.776</gml:lowerCorner>
          <gml:upperCorner>567106.486 5932757.776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9d47fb51-694e-408f-9da8-d26c9c249c47" />
      <xplan:position>
        <gml:Point gml:id="Gml_79D6664E-481A-431A-9CBA-462ABBAA8C9E" srsName="EPSG:25832">
          <gml:pos>567106.486 5932757.776</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_7b2f09c1-4492-401a-a40d-2e2a8c81db31">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567337.79 5932688.537</gml:lowerCorner>
          <gml:upperCorner>567403.367 5932740.198</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_451AE749-C001-4562-BFFA-83D2A25DCD04" srsName="EPSG:25832">
          <gml:posList>567350.721 5932740.198 567347.101 5932726.674 567337.79 5932691.899 
567350.348 5932688.537 567359.658 5932723.312 567387.189 5932715.941 
567399.746 5932712.579 567403.367 5932726.103 567350.721 5932740.198 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_74c03a65-d58e-40f8-9d4b-32dd03930c41">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567195.374 5932730.73</gml:lowerCorner>
          <gml:upperCorner>567229.869 5932764.51</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_21D4834A-6421-42CD-949C-680B5775B8D1" srsName="EPSG:25832">
          <gml:posList>567229.869 5932757.348 567202.283 5932764.51 567195.374 5932737.898 
567222.958 5932730.73 567224.591 5932737.022 567229.869 5932757.348 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_7ec703d7-c3bd-45d3-bda6-d9e59461b1dc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567221.324 5932718.156</gml:lowerCorner>
          <gml:upperCorner>567248.789 5932737.022</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_7AEC7B2F-47C6-41EB-8232-41978B8ECF55" srsName="EPSG:25832">
          <gml:posList>567237.174 5932733.755 567224.591 5932737.022 567222.958 5932730.73 
567221.324 5932724.439 567245.522 5932718.156 567248.789 5932730.739 
567237.174 5932733.755 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="GML_df1512e4-15b5-463a-8b81-01815191ae01">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566841.322 5932638.488</gml:lowerCorner>
          <gml:upperCorner>567478.319 5932805.784</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>FUSSGÄNGER- UND RADFAHRERBEREICH</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_62616a14-e2bf-4213-b186-d9abb111a5cb" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_90FA9E59-20EF-4EC8-A1E4-FE9F1F1BE8CE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567038.535 5932771.936 567004.845 5932778.378 566935.752 5932791.578 
566925.489 5932793.308 566923.976 5932793.564 566899.326 5932797.323 
566858.262 5932803.586 566842.603 5932805.784 566841.516 5932797.098 
566841.322 5932795.563 566856.166 5932793.67 566876.457 5932790.784 
566893.555 5932788.149 566925.816 5932782.812 566925.622 5932781.675 
566957.155 5932775.947 567001.253 5932766.989 567024.668 5932762.288 
567034.553 5932760.037 567099.41 5932743.34 567165.898 5932726.231 
567207.598 5932715.718 567251.407 5932703.902 567309.648 5932687.426 
567309.188 5932685.742 567324.249 5932681.582 567379.905 5932665.733 
567462.605 5932641.979 567474.886 5932638.488 567476.043 5932642.107 
567477.061 5932645.289 567475.978 5932645.649 567478.319 5932654.273 
567414.585 5932671.338 567325.715 5932695.132 567258.017 5932714.912 
567181.156 5932734.868 567193.176 5932781.173 567149.128 5932791.971 
567154.527 5932745.398 567117.89 5932754.911 567117.019 5932751.558 
567038.535 5932771.936 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_62616a14-e2bf-4213-b186-d9abb111a5cb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567192.366 5932725.471</gml:lowerCorner>
          <gml:upperCorner>567192.366 5932725.471</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>text</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_df1512e4-15b5-463a-8b81-01815191ae01" />
      <xplan:position>
        <gml:Point gml:id="Gml_15DEA263-56D9-4CD9-83F2-07EDA74344C4" srsName="EPSG:25832">
          <gml:pos>567192.366 5932725.471</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">344.51</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_343e9ae0-e730-463d-be6c-6571d48c28d2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567258.017 5932654.273</gml:lowerCorner>
          <gml:upperCorner>567491.419 5932761.347</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9ff978bc-695f-4c7a-8f18-5ad5bd63f498" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c91124bb-24fd-4bc3-962c-975a7771c561" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_915778b2-65ac-472f-8c81-7fd17d57d1c1" />
      <xplan:refTextInhalt xlink:href="#GML_1f136ab2-8263-4d60-ba4f-b7b272e78b13" />
      <xplan:refTextInhalt xlink:href="#GML_dc728bae-a4dc-4bc3-a602-f2b4837e1263" />
      <xplan:refTextInhalt xlink:href="#GML_d99d7f46-18c7-4b7c-87b4-0660884f7022" />
      <xplan:refTextInhalt xlink:href="#GML_3c2d1544-a26d-4788-a7ce-736aff669377" />
      <xplan:refTextInhalt xlink:href="#GML_d2b1d8bb-169a-4ed0-b6dc-e310692d3257" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_24813ba0-c9b0-4655-9ccd-42baad49dcbf" />
      <xplan:refTextInhalt xlink:href="#GML_967d2f7e-c198-4539-96a9-85ed0a81d4a2" />
      <xplan:refTextInhalt xlink:href="#GML_7f160780-9db6-4be1-8ed3-11df2d255806" />
      <xplan:refTextInhalt xlink:href="#GML_27e1d5c5-2e37-42be-b0be-bf85a23883d5" />
      <xplan:refTextInhalt xlink:href="#GML_b0540621-d713-4e0d-8f09-9f70e1771e40" />
      <xplan:refTextInhalt xlink:href="#GML_60885b8d-ed98-44c0-859b-e937a34d45ab" />
      <xplan:refTextInhalt xlink:href="#GML_c3cb239b-32bc-4361-9e35-d4869768a4a6" />
      <xplan:refTextInhalt xlink:href="#GML_de927319-5336-47ea-9a54-270bedb07ca7" />
      <xplan:refTextInhalt xlink:href="#GML_4360595b-e82a-4207-84b1-713aaefab5da" />
      <xplan:refTextInhalt xlink:href="#GML_bd4953ce-ce43-47f3-90c8-f8b4cec45f43" />
      <xplan:refTextInhalt xlink:href="#GML_10f35421-3c0d-4867-9624-93d58f4a85be" />
      <xplan:refTextInhalt xlink:href="#GML_25a71fae-9279-4085-a124-4d507aa5e6ab" />
      <xplan:refTextInhalt xlink:href="#GML_3b43fd0e-dc9d-4c85-b8ed-d6a0086631fd" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_67562038-C9C6-481F-BE08-DC1A393D0D33" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567491.419 5932702.527 567427.516 5932719.637 567338.647 5932743.431 
567270.174 5932761.347 567258.017 5932714.912 567325.715 5932695.132 
567414.585 5932671.338 567478.319 5932654.273 567491.419 5932702.527 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.7</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9ff978bc-695f-4c7a-8f18-5ad5bd63f498">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567369.97 5932706.832</gml:lowerCorner>
          <gml:upperCorner>567369.97 5932706.832</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_343e9ae0-e730-463d-be6c-6571d48c28d2" />
      <xplan:position>
        <gml:Point gml:id="Gml_F268D05F-0A4C-4BFA-A483-8077476A8DF2" srsName="EPSG:25832">
          <gml:pos>567369.97 5932706.832</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c91124bb-24fd-4bc3-962c-975a7771c561">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567367.806 5932700.104</gml:lowerCorner>
          <gml:upperCorner>567367.806 5932700.104</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_343e9ae0-e730-463d-be6c-6571d48c28d2" />
      <xplan:position>
        <gml:Point gml:id="Gml_1D2E407F-2B8B-41C8-BEE4-CE80B829A340" srsName="EPSG:25832">
          <gml:pos>567367.806 5932700.104</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_ad3cbd9a-db08-40fd-88c7-48376c8cb21c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567313.643 5932691.899</gml:lowerCorner>
          <gml:upperCorner>567350.721 5932747.553</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_1f6cf107-2977-4732-9401-3a469bfe9bca" />
      <xplan:refTextInhalt xlink:href="#GML_25a71fae-9279-4085-a124-4d507aa5e6ab" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D88819DA-FFDD-4059-AD65-F100494C0276" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567350.721 5932740.198 567338.647 5932743.431 567322.893 5932747.553 
567319.349 5932734.009 567313.643 5932712.203 567325.501 5932708.738 
567321.864 5932696.257 567325.715 5932695.132 567337.79 5932691.899 
567350.721 5932740.198 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_98eed3a9-f7e5-4275-a5cd-9064f7769814">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.503 5932773.896</gml:lowerCorner>
          <gml:upperCorner>567035.587 5932807.591</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(F)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b3a250e6-75c0-430a-b2c6-6eb23c6847f4" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_99AA7ED7-4BEA-420B-BE8C-D06BEAA0A5E2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567035.587 5932804.581 567022.941 5932807.591 567015.503 5932776.34 
567028.284 5932773.896 567035.587 5932804.581 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b3a250e6-75c0-430a-b2c6-6eb23c6847f4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567025.58 5932790.605</gml:lowerCorner>
          <gml:upperCorner>567025.58 5932790.605</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_98eed3a9-f7e5-4275-a5cd-9064f7769814" />
      <xplan:position>
        <gml:Point gml:id="Gml_DB3B3335-2EBE-4656-82AB-7E73DD9D14D8" srsName="EPSG:25832">
          <gml:pos>567025.58 5932790.605</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_cb7f8790-400f-4d67-a775-dd6bed006e33">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566899.289 5932793.365</gml:lowerCorner>
          <gml:upperCorner>566926.952 5932810.137</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_844EF8CC-D401-4935-8528-ABD4AAF0AB5B" srsName="EPSG:25832">
          <gml:posList>566926.952 5932806.22 566914.1 5932808.177 566901.249 5932810.137 
566900.314 5932804.008 566899.289 5932797.285 566924.003 5932793.516 
566924.991 5932793.365 566926.952 5932806.22 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_8a4c319e-9aac-4005-8bd9-6ed4ac473239">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566858.112 5932800.141</gml:lowerCorner>
          <gml:upperCorner>566919.2 5932849.831</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_A06818A6-B2DD-427E-B184-98B97C75FDB1" srsName="EPSG:25832">
          <gml:posList>566914.1 5932808.177 566919.2 5932841.614 566865.323 5932849.831 
566858.112 5932802.554 566873.929 5932800.141 566878.799 5932832.072 
566904.004 5932828.228 566901.249 5932810.137 566914.1 5932808.177 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_fbf5e8fb-ca3f-4bd7-85ca-08ad3ba879b6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567221.324 5932718.156</gml:lowerCorner>
          <gml:upperCorner>567248.789 5932737.022</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1100</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_34b9302e-3694-44f3-bb68-1479e1beeaa4" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_ccc616ec-5875-40b7-9e86-9a48a5928c8e" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_71c1033e-3f2b-4986-b697-64dc19b0ab13" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9791D7CB-8943-4991-AB53-29C35D2F34FF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567248.789 5932730.739 567237.174 5932733.755 567224.591 5932737.022 
567222.958 5932730.73 567221.324 5932724.439 567245.522 5932718.156 
567248.789 5932730.739 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_34b9302e-3694-44f3-bb68-1479e1beeaa4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567235.057 5932724.375</gml:lowerCorner>
          <gml:upperCorner>567235.057 5932724.375</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_fbf5e8fb-ca3f-4bd7-85ca-08ad3ba879b6" />
      <xplan:position>
        <gml:Point gml:id="Gml_4EEE6444-250D-44C0-80C5-84FF4F325778" srsName="EPSG:25832">
          <gml:pos>567235.057 5932724.375</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ccc616ec-5875-40b7-9e86-9a48a5928c8e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567235.057 5932729.716</gml:lowerCorner>
          <gml:upperCorner>567235.057 5932729.716</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_fbf5e8fb-ca3f-4bd7-85ca-08ad3ba879b6" />
      <xplan:position>
        <gml:Point gml:id="Gml_44986D88-C1C5-4003-A4BF-4C20A2383EC5" srsName="EPSG:25832">
          <gml:pos>567235.057 5932729.716</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_84402729-ce76-47a2-b8a1-c59ec3a42b13">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567095.823 5932767.187</gml:lowerCorner>
          <gml:upperCorner>567112.927 5932789.338</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_CA408523-2712-435A-9676-8B23BAB41493" srsName="EPSG:25832">
          <gml:posList>567112.927 5932786.175 567100.317 5932789.338 567100.281 5932789.185 
567095.823 5932770.455 567108.409 5932767.187 567112.927 5932786.175 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_31f84f9a-16ad-49b6-b6d8-68713690886f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567399.746 5932706.113</gml:lowerCorner>
          <gml:upperCorner>567427.517 5932726.103</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_4c91d606-2c98-4045-9aa4-eafc3e05bac3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1796cf02-2c08-45cb-9d75-cc9a52c8acce" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fe81b5af-a098-4f52-b354-3129f5e127b9" />
      <xplan:refTextInhalt xlink:href="#GML_90fc528d-6819-4a33-91a2-9267924316c2" />
      <xplan:refTextInhalt xlink:href="#GML_5179f8b9-b2ee-4b16-bf73-805140661591" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4A52366E-D97B-4F10-B075-765BC6C1E230" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567427.517 5932719.637 567403.367 5932726.103 567399.746 5932712.579 
567423.896 5932706.113 567427.517 5932719.637 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_4c91d606-2c98-4045-9aa4-eafc3e05bac3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567413.631 5932718.402</gml:lowerCorner>
          <gml:upperCorner>567413.631 5932718.402</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_31f84f9a-16ad-49b6-b6d8-68713690886f" />
      <xplan:position>
        <gml:Point gml:id="Gml_52C32C4E-34AF-4328-95CF-C3353F823420" srsName="EPSG:25832">
          <gml:pos>567413.631 5932718.402</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1796cf02-2c08-45cb-9d75-cc9a52c8acce">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567413.631 5932712.608</gml:lowerCorner>
          <gml:upperCorner>567413.631 5932712.608</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_477f762f-a694-4610-af8f-973ff3d2f2ce" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_31f84f9a-16ad-49b6-b6d8-68713690886f" />
      <xplan:position>
        <gml:Point gml:id="Gml_5E4D13A6-DBD7-4097-A96F-BBF2E15BA60B" srsName="EPSG:25832">
          <gml:pos>567413.631 5932712.608</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_4ba83953-00d3-4209-b080-a356ad509869">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566778.099 5932374.896</gml:lowerCorner>
          <gml:upperCorner>567470.445 5932779.633</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>HafenCity14_SoapUI-XPlanManagerAPI</xplan:name>
      <xplan:beschreibung>Der Bebauungsplan HafenCity 14 für das Gebiet zwischen
Baakenhafen und Norderelbe (Bezirk Hamburg-Mitte,
Ortsteil 104) wird festgestellt.
Das Gebiet wird wie folgt begrenzt:
Baakenhafen – über die Flurstücke 1624 und 1352, Südgrenzen
der Flurstücke 1624 und 1625, über das Flurstück 1625 der
Gemarkung Altstadt Süd.</xplan:beschreibung>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_AF6462CD-D943-443A-AC88-6ED5F55C9BAD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D1F7F5E8-886E-4DC8-B8C2-2745B1E4D754" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566933.255 5932760.356 566885.545 5932770.145 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_83F47CDA-DCF3-4F5D-A975-DC2B3A6D3F68" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566885.545 5932770.145 566839.305 5932779.633 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_14C0BC76-0870-4773-9572-29D0AD423324" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566839.305 5932779.633 566833.794 5932737.623 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6E56B797-8959-4744-A33C-6543F4A49626" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566833.794 5932737.623 566778.099 5932746.09 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2ECCFD36-ECF1-41B0-BE9C-36755B2ACE39" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566778.099 5932746.09 566806.421 5932681.101 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_33615684-69B4-4007-88E1-9075596C1915" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566806.421 5932681.101 566811.751 5932668.747 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_40FC94F5-EAA1-4735-9FFC-D522B087F518" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566811.751 5932668.747 566867.114 5932541.836 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_76466A9C-D656-4824-8E2E-118CB4AB52FF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566867.114 5932541.836 566881.191 5932509.534 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_662C2C98-D749-43BD-AF5F-34300456AD4D" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566881.191 5932509.534 566904.618968746 5932505.069006 566928.036 5932500.547 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_46B42A05-3935-4718-9F6E-3000412F6157" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566928.036 5932500.547 566930.476 5932500.073 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E2ABCC62-87BB-4439-8569-76F883109945" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566930.476 5932500.073 566964.214 5932493.074 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_5DD11926-ADD5-44E2-B8B5-A58B9EBAD4F8" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566964.214 5932493.074 567036.828398072 5932476.62190217 567108.797 5932457.542 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0293199D-2898-4B42-88A3-4D6529575A4B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567108.797 5932457.542 567118.766 5932454.705 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_553777E5-F691-46B0-A888-25EA7E78C17E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567118.766 5932454.705 567163.242 5932442.044 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_75147516-D781-421A-81A5-B419C1DCD25A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567163.242 5932442.044 567249.81 5932417.15 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_60213755-AC87-4EB3-A2E3-10ADE71F39C4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567249.81 5932417.15 567332.731 5932392.835 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_33F6EC50-70B2-4617-A7A9-F3B532F88E76" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567332.731 5932392.835 567360.457157475 5932384.26594882 567387.923 5932374.896 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0DD4C511-56D0-4651-9C56-FB1677D336B2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567387.923 5932374.896 567396.661 5932403.241 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0355D74B-327B-47CC-B500-6F4C7A522BF4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567396.661 5932403.241 567410.998 5932444.716 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7DE8C4C0-0E0B-417A-97FA-471DF2035693" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567410.998 5932444.716 567436.534 5932521.988 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E67AB856-CAB3-4008-95DE-1F1CA5F9FDC9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567436.534 5932521.988 567470.445 5932624.6 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_04DD946C-F185-4DFE-AEF4-665E43F04028" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567470.445 5932624.6 567374.73 5932651.63 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_70B7497C-0935-4D11-9B41-74F331B4BE57" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567374.73 5932651.63 567189.373 5932700.56 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EF3D637D-7987-4688-BEB7-CC3E82E3B5DF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567189.373 5932700.56 567084.333 5932725.96 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_06A02AD3-AC56-48FE-B684-258FF13A1986" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567084.333 5932725.96 566933.255 5932760.356 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#GML_3e7ae2f8-4b5d-4a31-aced-656804df7c3b" />
      <xplan:texte xlink:href="#GML_f4e240c9-c8be-4a5a-a128-9b2c9b241df8" />
      <xplan:texte xlink:href="#GML_e66fdfa2-d75b-42cf-8816-f5f40e91d5ee" />
      <xplan:texte xlink:href="#GML_2266bfaa-fec8-4bc2-95b3-eb1a31293c4f" />
      <xplan:texte xlink:href="#GML_177a6d7b-fd34-4539-a553-13fca56cd965" />
      <xplan:texte xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:texte xlink:href="#GML_d7b659f8-f713-4d27-bb48-29a979349931" />
      <xplan:texte xlink:href="#GML_14aeafe7-eb22-41da-9bdc-ec63b28ff7af" />
      <xplan:texte xlink:href="#GML_af148c04-d4bc-4965-b920-b9e02ec6b0ca" />
      <xplan:texte xlink:href="#GML_84329dc1-833d-4d58-96a7-a19a8aa4ebe5" />
      <xplan:texte xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:texte xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:texte xlink:href="#GML_d7f26fd1-256a-4453-8384-65e163dc7874" />
      <xplan:texte xlink:href="#GML_457fffb9-dcce-4d2f-bd8d-1e043dfae1b0" />
      <xplan:texte xlink:href="#GML_1c0ece7b-0f1e-4303-8d5b-867ada375758" />
      <xplan:texte xlink:href="#GML_28da36c8-5466-4300-a823-2c9bf9a86484" />
      <xplan:texte xlink:href="#GML_f6f7e3d3-1de7-45f8-8263-691a9ea5d611" />
      <xplan:texte xlink:href="#GML_fb5a3001-7cc9-4dc6-a02d-83ab886bafd0" />
      <xplan:texte xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:texte xlink:href="#GML_ad52165d-f9b8-428b-9ab2-d312f49e819b" />
      <xplan:texte xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:texte xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:texte xlink:href="#GML_0b57dbfd-230d-4b27-b6ec-be8cab882f2b" />
      <xplan:texte xlink:href="#GML_8d068ddb-d876-42a4-9414-cdb642b6695f" />
      <xplan:texte xlink:href="#GML_332fdf10-4e62-4d64-9657-8e9fae6cc9c4" />
      <xplan:texte xlink:href="#GML_fa848c8c-32b7-4173-adc0-9cfccebcaccc" />
      <xplan:texte xlink:href="#GML_afad1a34-347b-4780-a7b4-6a314fea8328" />
      <xplan:texte xlink:href="#GML_4a229a55-87d9-41e7-a476-f3122e8477d9" />
      <xplan:texte xlink:href="#GML_2bf4b007-7246-4c60-8ed6-86f18843d9ec" />
      <xplan:texte xlink:href="#GML_8e760f3f-dfcf-4dca-abc7-1c8eeb773689" />
      <xplan:texte xlink:href="#GML_d22754b3-3c44-4f4c-b043-98e6748ac240" />
      <xplan:texte xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:texte xlink:href="#GML_e0ac5d8e-1150-46ca-925d-a76a5d887d7a" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>104</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:plangeber>
        <xplan:XP_Plangeber>
          <xplan:name>104</xplan:name>
        </xplan:XP_Plangeber>
      </xplan:plangeber>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2016-01-05</xplan:rechtsverordnungsDatum>
      <xplan:versionBauNVODatum>1990-01-01</xplan:versionBauNVODatum>
      <xplan:versionBauNVOText>Version vom 11.06.2013</xplan:versionBauNVOText>
      <xplan:bereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_3e7ae2f8-4b5d-4a31-aced-656804df7c3b">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Auf der Fläche für den Gemeinbedarf sind lärmempfindliche
Räume (zum Beispiel Unterrichtsräume, Pausenräume,
Bibliotheksräume) durch geeignete Anordnung
der Baukörper oder durch geeignete Grundrissgestaltung
den vom Verkehrslärm abgewandten Gebäudeseiten
zuzuordnen. Soweit die Anordnung der in Satz 1 genannten
Räume ausnahmsweise nicht an den lärmabgewandten
Seiten
erfolgen kann, ist in diesen Räumen ein Innenraumpegel
von kleiner 35 dB(A) am Tag (6.00 Uhr bis
22.00 Uhr) durch baulichen Schallschutz sicherzustellen.
Für den Schulhof ist zu gewährleisten, dass durch geeignete
Anordnung der Baukörper, Schallschutzwände oder
vergleichbare Maßnahmen ein Pegel von 60 dB(A) am
Tag nicht überschritten wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_f4e240c9-c8be-4a5a-a128-9b2c9b241df8">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>In der Spiel- und Freizeitanlage sind innerhalb der überbaubaren
Fläche nur ein Spielhaus und ein Kiosk zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_e66fdfa2-d75b-42cf-8816-f5f40e91d5ee">
      <xplan:schluessel>§2 Nr.15.1</xplan:schluessel>
      <xplan:text>Neu zu errichtende Gebäude sind an ein Wärmenetz
anzuschließen, das überwiegend mit erneuerbaren Energien
versorgt wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_2266bfaa-fec8-4bc2-95b3-eb1a31293c4f">
      <xplan:schluessel>§2 Nr.10</xplan:schluessel>
      <xplan:text>Oberhalb der festgesetzten Vollgeschosse (einschließlich
einem möglichen Galeriegeschoss im Erdgeschoss) sind
weitere Geschosse unzulässig. Technikgeschosse und
technische oder erforderliche Aufbauten, wie Treppenräume,
sind ausnahmsweise, auch über der festgesetzten
Gebäudehöhe, zulässig, wenn die Gestaltung des Gesamtbaukörpers
und das Ortsbild nicht beeinträchtigt werden
und diese keine wesentliche Verschattung der Nachbargebäude
und der Umgebung bewirken. Aufbauten, deren Einhausung und Technikgeschosse sind mindestens
2,5 m von der Außenfassade zurückzusetzen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_177a6d7b-fd34-4539-a553-13fca56cd965">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Für die Mischgebiete gilt:</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a">
      <xplan:schluessel>§2 Nr.25</xplan:schluessel>
      <xplan:text>Die übrigen, neben den in Nummer 24 genannten, Dachflächen
in den allgemeinen Wohngebieten und den
Mischgebieten sind mit Ausnahme der gemäß Nummer
10 zulässigen Anlagen und technischen Aufbauten zu
mindestens 30 v. H. mit einem mindestens 15 cm starken
durchwurzelbaren Substrataufbau extensiv mit standortangepassten
Stauden und Gräsern zu begrünen. Darüber
hinaus müssen mindestens 20 v. H. mit einem mindestens
50 cm starken Substrataufbau intensiv mit Stauden
und Sträuchern begrünt werden. Die Dachbegrünung
ist dauerhaft zu erhalten.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_d7b659f8-f713-4d27-bb48-29a979349931">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>Durch geeignete bauliche Schallschutzmaßnahmen wie
zum Beispiel Doppelfassaden, verglaste Vorbauten (zum Beispiel verglaste Loggien, Wintergärten), besondere
Fensterkonstruktionen oder in ihrer Wirkung vergleichbare
Maßnahmen ist sicherzustellen, dass durch diese
baulichen Maßnahmen insgesamt eine Schallpegeldifferenz
erreicht wird, die es ermöglicht, dass in Schlafräumen
ein Innenraumpegel bei teilgeöffneten Fenstern von
30 dB(A) während der Nachtzeit (22.00 Uhr bis 6.00 Uhr)
nicht überschritten wird. Erfolgt die bauliche Schallschutzmaßnahme
in Form von verglasten Vorbauten,
muss dieser Innenraumpegel bei teilgeöffneten Bauteilen
erreicht werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_14aeafe7-eb22-41da-9bdc-ec63b28ff7af">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Die Aufenthaltsräume für gewerbliche Nutzungen – hier
insbesondere die Pausen- und Ruheräume – sind durch
geeignete Grundrissgestaltung den Verkehrslärm abgewandten
Gebäudeseiten zuzuordnen. Soweit die Anordnung
an den vom Verkehrslärm abgewandten Gebäudeseiten
nicht möglich ist, muss für diese Räume ein ausreichender
Schallschutz an Außentüren, Fenstern,
Außenwänden und Dächern der Gebäude durch bauliche
Maßnahmen geschaffen werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_TextAbschnitt gml:id="GML_af148c04-d4bc-4965-b920-b9e02ec6b0ca">
      <xplan:schluessel>§2 Nr.17</xplan:schluessel>
      <xplan:text>An den Rändern der hochwassergefährdeten Bereiche
sind zum Zwecke des Hochwasserschutzes, soweit
erforderlich,
zusätzliche besondere bauliche Maßnahmen
vorzusehen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:SO_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_84329dc1-833d-4d58-96a7-a19a8aa4ebe5">
      <xplan:schluessel>§2 Nr.18</xplan:schluessel>
      <xplan:text>Die festgesetzten Gehrechte umfassen die Befugnis der
Freien und Hansestadt Hamburg, allgemein zugängige
Gehwege anzulegen und zu unterhalten. Geringfügige
Abweichungen von den festgesetzten Gehrechten sind
zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_548c7447-2601-4b47-99b5-092171bd6dc4">
      <xplan:schluessel>§2 Nr.22</xplan:schluessel>
      <xplan:text>Die nicht überbauten Grundstücksflächen der allgemeinen
Wohngebiete und der Mischgebiete, mit Ausnahme
der Flächen mit festgesetzten Gehrechten, sowie die
Dachflächen der festgesetzten eingeschossigen Gebäude
auf den mit „(E)“ bezeichneten Flächen sind mit einem
Anteil von mindestens 50 v. H. zu begrünen. Je 300 m² ist
mindestens ein großkroniger Baum oder je 150 m² ein
kleinkroniger Baum zu pflanzen und dauerhaft zu erhalten.
Bei Abgang ist eine gleichwertige Ersatzpflanzung
vorzunehmen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4">
      <xplan:schluessel>§2 Nr.11</xplan:schluessel>
      <xplan:text>Die Gebäudefassaden können in unterschiedlichen
Materialien ausschließlich in den Farben Weiß, Beige,
Gelb und Blaubunt ausgeführt werden. Die Gebäudefassaden
auf den mit „(D)“ bezeichneten Flächen sind in
hellen Materialien auszuführen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_d7f26fd1-256a-4453-8384-65e163dc7874">
      <xplan:schluessel>§2 Nr.15.3</xplan:schluessel>
      <xplan:text>Vom Anschluss- und Benutzungsgebot nach Nummer
15.1 kann auf Antrag befreit werden, soweit die Erfüllung
der Anforderungen im Einzelfall wegen besonderer
Umstände
zu einer unbilligen Härte führen würde. Die
Befreiung soll zeitlich befristet werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_457fffb9-dcce-4d2f-bd8d-1e043dfae1b0">
      <xplan:schluessel>§2 Nr.2.3</xplan:schluessel>
      <xplan:text>Die festgesetzten Grundflächenzahlen können für Nutzungen
nach § 19 Absatz 4 Satz 1 der Baunutzungsverordnung
in der Fassung vom 23. Januar 1990 (BGBl. I
S. 133), zuletzt geändert am 11. Juni 2013 (BGBl. I S.
1548, 1551), bis 1,0 überschritten werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_1c0ece7b-0f1e-4303-8d5b-867ada375758">
      <xplan:schluessel>§2 Nr.2.2</xplan:schluessel>
      <xplan:text>Vergnügungsstätten in den Teilen des Mischgebiets, die
überwiegend durch gewerbliche Nutzungen geprägt sind,
sowie Tankstellen sind unzulässig. Ausnahmen für Vergnügungsstätten
in den übrigen Teilen des Mischgebiets
werden ausgeschlossen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_28da36c8-5466-4300-a823-2c9bf9a86484">
      <xplan:schluessel>§2 Nr.15</xplan:schluessel>
      <xplan:text>Für die Beheizung und Bereitstellung des Warmwassers
gilt:</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_f6f7e3d3-1de7-45f8-8263-691a9ea5d611">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Tiefgaragen sind außerhalb der überbaubaren Grundstücksflächen
zulässig. Stellplätze sind nur in Tiefgaragen
oder Garagengeschossen unterhalb der Höhe von
8,7 m über Normalhöhennull (NHN) zulässig. Geringfügige
Abweichungen sind zulässig, wenn sie durch abweichende
Straßenanschlusshöhen von über 8,7 m über
NHN begründet sind.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_fb5a3001-7cc9-4dc6-a02d-83ab886bafd0">
      <xplan:schluessel>§2 Nr.20</xplan:schluessel>
      <xplan:text>Auf den nicht überbauten Grundstücksflächen sind
Nebenanlagen nur ausnahmsweise zulässig, wenn die
Gestaltung der Freiflächen nicht beeinträchtigt ist.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_be6475a4-b0a8-4f18-afd0-6974662bbd88">
      <xplan:schluessel>§2 Nr.26</xplan:schluessel>
      <xplan:text>In den allgemeinen Wohngebieten und den Mischgebieten
sind Dächer als Flachdächer oder flachgeneigte
Dächer mit einer Neigung bis zu 10 Grad auszuführen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ad52165d-f9b8-428b-9ab2-d312f49e819b">
      <xplan:schluessel>§2 Nr.16</xplan:schluessel>
      <xplan:text>Das auf den Straßenverkehrsflächen besonderer Zweckbestimmung,
den Grünflächen und den Wasserhäusern
anfallende Niederschlagswasser ist direkt in das nächst
liegende
Gewässer (Baakenhafen oder Norderelbe) einzuleiten.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_1e889626-8dad-47ea-875f-abad85e839d5">
      <xplan:schluessel>§2 Nr.21</xplan:schluessel>
      <xplan:text>In den Baugebieten sind für Einfriedigungen nur Hecken
oder durchbrochene Zäune in Verbindung mit Hecken
bis zu einer Höhe von 1,2 m zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Auf den mit „(A)“ bezeichneten Flächen der allgemeinen
Wohngebiete und der Mischgebiete sind Wohnungen in
den Erdgeschossen unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_0b57dbfd-230d-4b27-b6ec-be8cab882f2b">
      <xplan:schluessel>§2 Nr.13</xplan:schluessel>
      <xplan:text>Werbeanlagen größer als 2 m² und Werbeanlagen oberhalb
der Gebäudetraufen sind unzulässig. Die Gestaltung
der Gesamtbaukörper und der privaten Freiflächen darf
nicht durch Werbeanlagen beeinträchtigt werden. Werbeanlagen
sind nur an der Stätte der Leistung zulässig.
Oberhalb der Brüstung des zweiten Vollgeschosses sind
Werbeanlagen nur ausnahmsweise zulässig, wenn zudem
das Ortsbild nicht beeinträchtigt wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_8d068ddb-d876-42a4-9414-cdb642b6695f">
      <xplan:schluessel>§2 Nr.19</xplan:schluessel>
      <xplan:text>Die festgesetzten Geh- und Fahrrechte umfassen die
Befugnis der Freien und Hansestadt Hamburg, allgemein zugängige Gehwege anzulegen und zu unterhalten, sowie
die Befugnis der für die Unterhaltung der Kaianlagen
sowie Fußgänger- und Radfahrerbereiche zuständigen
Stellen, diese Flächen zu befahren. Geringfügige Abweichungen
von den festgesetzten Geh- und Fahrrechten
sind zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_332fdf10-4e62-4d64-9657-8e9fae6cc9c4">
      <xplan:schluessel>§2 Nr.27</xplan:schluessel>
      <xplan:text>Für festgesetzte Anpflanzungen sind standortgerechte
Laubbäume oder belaubte Heckenpflanzen zu verwenden.
Großkronige Bäume müssen einen Stammumfang
von mindestens 18 cm, kleinkronige Bäume von mindestens
14 cm, in 1 m Höhe über dem Erdboden gemessen,
aufweisen; Heckenpflanzen eine Mindesthöhe von 80 cm.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_fa848c8c-32b7-4173-adc0-9cfccebcaccc">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>In den allgemeinen Wohngebieten können die festgesetzten
Grundflächenzahlen für Nutzungen nach § 19 Absatz
4 Satz 1 der Baunutzungsverordnung bis 1,0 überschritten
werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_afad1a34-347b-4780-a7b4-6a314fea8328">
      <xplan:schluessel>§2 Nr.15.2</xplan:schluessel>
      <xplan:text>Vom Anschluss- und Benutzungsgebot nach Nummer
15.1 kann ausnahmsweise abgesehen werden, wenn der
berechnete Heizwärmebedarf der Gebäude nach der
Energieeinsparverordnung vom 24. Juli 2007 (BGBl. I
S. 1519), zuletzt geändert am 24. Oktober 2015 (BGBl. I
S. 1789, 1790), den Wert von 15 kWh/ m² Nutzfläche
nicht übersteigt.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_4a229a55-87d9-41e7-a476-f3122e8477d9">
      <xplan:schluessel>§2 Nr.23</xplan:schluessel>
      <xplan:text>Außer auf den mit „(F)“ bezeichneten Flächen sind die
mit festgesetzten Gehrechten belegten Flächen der allgemeinen
Wohngebiete und der Mischgebiete mit einem
Anteil von mindestens 20 v. H. zu begrünen. Je 500 m² der
mit festgesetzten Gehrechten belegten Flächen der allgemeinen
Wohngebiete und der Mischgebiete ist mindestens
ein großkroniger Baum oder je 250 m² ein kleinkroniger
Baum zu pflanzen und dauerhaft zu erhalten.
Bei Abgang ist eine gleichwertige Ersatzpflanzung vorzunehmen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_2bf4b007-7246-4c60-8ed6-86f18843d9ec">
      <xplan:schluessel>§2 Nr.12</xplan:schluessel>
      <xplan:text>Eine Überschreitung der Baugrenzen durch Balkone,
Erker, Loggien und Sichtschutzwände kann zu den
öffentlichen Straßenräumen, den Wasserflächen oder den
mit Gehrechten belasteten Flächen ausnahmsweise bis
zu einer Tiefe von 1,5 m zugelassen werden, wenn die
Gestaltung des Gesamtbaukörpers nicht beeinträchtigt
wird und diese keine wesentliche Verschattung der
benachbarten Nutzungen und der Umgebung bewirken.
Dabei ist eine Überbauung der Straßenverkehrsfläche
nur oberhalb einer lichten Höhe von 4,5 m zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_8e760f3f-dfcf-4dca-abc7-1c8eeb773689">
      <xplan:schluessel>§2 Nr.24</xplan:schluessel>
      <xplan:text>Tiefgaragen und die Dachflächen der festgesetzten eingeschossigen
Gebäude auf den mit „(E)“ bezeichneten
Flächen sind in den zu begrünenden Bereichen mit
einem mindestens 50 cm starken durchwurzelbaren Substrataufbau
zu versehen. Für Baumpflanzungen muss auf
einer Fläche von 16 m² je Baum die Stärke des durchwurzelbaren
Substrataufbaus mindestens 80 cm betragen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_d22754b3-3c44-4f4c-b043-98e6748ac240">
      <xplan:schluessel>§2 Nr.14</xplan:schluessel>
      <xplan:text>An den zur Norderelbe gerichteten Fassaden sind Werbeanlagen
oberhalb der Brüstung des ersten Obergeschosses
der Fassaden unzulässig; Schriftzeichen müssen in
Einzelbuchstaben ausgeführt werden und zur Beleuchtung
der Buchstaben darf nur warmweißes Licht verwendet
werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_e45b25c0-9560-4380-bd62-be06baabaa48">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Außer auf den mit „(C)“ bezeichneten Flächen muss die
Oberkante des Fußbodens des ersten Obergeschosses
mindestens 5,5 m und höchstens 6,5 m über der angrenzenden
Geländeoberfläche liegen. Ausnahmsweise kann
im Erdgeschoss
eine Galerie eingebaut werden, wenn das
Galeriegeschoss eine Grundfläche kleiner 50 vom Hundert
(v. H.) der Grundfläche des Erdgeschosses einnimmt.
Die Galerieebene muss einen Abstand von mindestens
4,5 m von der Innenseite der zu den öffentlichen
Straßenverkehrsflächen
und mit Gehrechten belegten
Flächen gerichteten Außenfassade einhalten. Das Erdgeschoss
samt einem eventuell eingezogenen Galeriegeschoss
wird als ein Vollgeschoss gewertet.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_e0ac5d8e-1150-46ca-925d-a76a5d887d7a">
      <xplan:schluessel>§2 Nr.2.1</xplan:schluessel>
      <xplan:text>Großflächiger Einzelhandel kann ausnahmsweise auf
den mit „(B)“ bezeichneten Flächen zugelassen werden,
wenn er der Nahversorgung der angrenzenden Quartiere
dient.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_44403280-26ad-4e62-b59b-21a719a4219f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566778.099 5932374.896</gml:lowerCorner>
          <gml:upperCorner>567470.445 5932779.633</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_DCCDECEE-8CEC-4FE5-BC10-FB18C31E0AFD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D98EC8DD-2C31-4A34-9AC5-2EF527BB3321" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566933.255 5932760.356 566885.545 5932770.145 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_89510A87-DE7B-4DDA-8F55-05498111D4D0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566885.545 5932770.145 566839.305 5932779.633 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3AC2F7AC-CD68-4A2A-8301-232F473EA7FA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566839.305 5932779.633 566833.794 5932737.623 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E0092A9F-7E2D-4065-927A-B4A00F94628A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566833.794 5932737.623 566778.099 5932746.09 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E4D5A253-CFF6-42EA-96FE-A376A6FA184D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566778.099 5932746.09 566806.421 5932681.101 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0E18256F-5357-4C8B-82D0-F814FE09C0D2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566806.421 5932681.101 566811.81 5932668.737 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9A141339-B8DE-4517-91C7-8A23DD0C8C90" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566811.81 5932668.737 566867.114 5932541.836 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_78C88738-580A-45CE-A38A-2F8663C35300" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566867.114 5932541.836 566881.191 5932509.534 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_A0F11D47-60BA-4CA2-818E-0B0E0F7DDC26" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566881.191 5932509.534 566904.618968746 5932505.069006 566928.036 5932500.547 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_719BD845-CC4B-4D1A-943F-61DCC1200210" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566928.036 5932500.547 566930.476 5932500.073 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_93AAFBF7-1BF9-4C0E-B191-8391E1E27EF0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566930.476 5932500.073 566964.214 5932493.074 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_07FC9DDF-A33D-41FB-86AD-8138E95091E7" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566964.214 5932493.074 567036.828398072 5932476.62190217 567108.797 5932457.542 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2CC972B1-D941-4775-8A2A-F5D9521248E1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567108.797 5932457.542 567118.766 5932454.705 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E372D5B3-EF2A-4C4A-9908-1525BF213955" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567118.766 5932454.705 567163.242 5932442.044 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C624EE34-AD2A-4CB3-AC70-57B5B92C1D34" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567163.242 5932442.044 567249.81 5932417.15 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B52EE2A9-4A26-4CDA-B2D9-8CB2D8FFD0B0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567249.81 5932417.15 567332.731 5932392.835 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_5E42847D-69AB-444C-8DD8-10B7A700505F" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567332.731 5932392.835 567360.457157475 5932384.26594882 567387.923 5932374.896 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DE3CA6E5-4B29-4D56-8BD7-29F98F2D6879" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567387.923 5932374.896 567396.661 5932403.241 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F63B700F-2C9E-49F6-95B4-B9C6397C7784" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567396.661 5932403.241 567410.998 5932444.716 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_27325CEC-6457-4ACB-AFB7-9CE42638EA11" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567410.998 5932444.716 567436.534 5932521.988 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CD5132CB-6DBF-4A8D-BDD0-EAB8916CC24B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567436.534 5932521.988 567470.445 5932624.6 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8D9C302D-C9C7-406C-821D-CCC9589E4D63" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567470.445 5932624.6 567374.73 5932651.63 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_30D01E1B-A87D-482D-8CDF-FADE0B2B901D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567374.73 5932651.63 567189.373 5932700.56 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_33449908-A179-4587-9736-BAA34FB3AC3E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567189.373 5932700.56 567084.333 5932725.96 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7E6E9B49-E705-404A-AB0F-B17C3D761AFA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567084.333 5932725.96 566933.255 5932760.356 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#GML_36192594-a872-4644-813d-c4bf0058dd0b" />
      <xplan:planinhalt xlink:href="#GML_21bf5d31-e811-465d-b22d-ccc20c13b0e9" />
      <xplan:planinhalt xlink:href="#GML_d34c8ce0-8509-45a5-8dd1-ae56b430372d" />
      <xplan:planinhalt xlink:href="#GML_b1955143-ae0d-4b61-91b9-7560bb978540" />
      <xplan:planinhalt xlink:href="#GML_f5140669-1dea-4f5d-878e-c00e88b23928" />
      <xplan:planinhalt xlink:href="#GML_ad414f6c-9d69-436e-8f0f-5a71a202b632" />
      <xplan:planinhalt xlink:href="#GML_340cca07-7af7-4f8a-86e1-a26cffd2daf1" />
      <xplan:planinhalt xlink:href="#GML_322ffe3c-3d69-437b-8bb0-a9bf9eef5e42" />
      <xplan:planinhalt xlink:href="#GML_cb5d6f11-acb5-4fc8-99c7-994110b75438" />
      <xplan:planinhalt xlink:href="#GML_dfeaa284-4868-44f7-974f-97299d76ddee" />
      <xplan:planinhalt xlink:href="#GML_556d7c40-ac69-4bae-b34f-6ab23be12a2b" />
      <xplan:planinhalt xlink:href="#GML_11f87cab-00d2-44fd-a395-fd85697f0a5c" />
      <xplan:planinhalt xlink:href="#GML_052c069c-32a5-4947-8507-1f7e5f40a2c1" />
      <xplan:planinhalt xlink:href="#GML_522f81a6-9a91-4704-a4dc-5f05c0a930b1" />
      <xplan:planinhalt xlink:href="#GML_38dc31c5-78d7-42e0-b325-fdedbfc5ed14" />
      <xplan:planinhalt xlink:href="#GML_0a533c49-f4e8-4f1a-b8b0-cc954d8bde37" />
      <xplan:planinhalt xlink:href="#GML_1278d94f-1194-4230-a3dd-5315055202ab" />
      <xplan:planinhalt xlink:href="#GML_51f8d1ed-542d-41ed-a4b0-88434b66da57" />
      <xplan:planinhalt xlink:href="#GML_8caf70af-ac6b-4558-a754-536b1dd66585" />
      <xplan:planinhalt xlink:href="#GML_6f04f5a4-dbc1-4152-8185-67076a911937" />
      <xplan:planinhalt xlink:href="#GML_03bccf70-637e-44c5-9da1-4e3c8b83c935" />
      <xplan:planinhalt xlink:href="#GML_bd1095eb-12ac-4a26-a009-4f6b0753e65a" />
      <xplan:planinhalt xlink:href="#GML_704500d4-3b21-4afd-8c14-818105bf8100" />
      <xplan:planinhalt xlink:href="#GML_48f5db87-4ca0-4a53-a2ec-c5d823f03524" />
      <xplan:planinhalt xlink:href="#GML_306bdad4-1abf-448b-b567-51490cdc6693" />
      <xplan:planinhalt xlink:href="#GML_f24da74a-29a0-4481-b8a2-6553140ee5d6" />
      <xplan:planinhalt xlink:href="#GML_33c4d13d-8576-4c2e-b0c8-c89d4eae622f" />
      <xplan:planinhalt xlink:href="#GML_886e99ed-a72b-4538-bbbc-4c44886d15d8" />
      <xplan:planinhalt xlink:href="#GML_8150acbc-7b2f-4e36-a9f4-215a2c150717" />
      <xplan:planinhalt xlink:href="#GML_ec0258cd-9e32-4387-a212-c90af044273e" />
      <xplan:planinhalt xlink:href="#GML_5e0eacc3-a69f-4d4f-819b-ccbcf300d2ea" />
      <xplan:planinhalt xlink:href="#GML_502e2af3-8181-4c32-85c7-57f935467898" />
      <xplan:planinhalt xlink:href="#GML_bd564468-9997-4a9e-ba50-0fff9e54a681" />
      <xplan:planinhalt xlink:href="#GML_7571dac1-40fe-48ad-9209-ab766d49ddcf" />
      <xplan:planinhalt xlink:href="#GML_9de634ff-26a0-4084-9be7-0449f6363b12" />
      <xplan:planinhalt xlink:href="#GML_e937af61-4451-494a-abe3-7b7ead18ef7b" />
      <xplan:planinhalt xlink:href="#GML_d747be85-d5bd-4917-ab4d-dc9962b7d166" />
      <xplan:planinhalt xlink:href="#GML_ba4d251c-cdea-4335-96fa-fcae168c11bd" />
      <xplan:planinhalt xlink:href="#GML_c02f90ac-b876-4a51-a8bf-a700b663b384" />
      <xplan:planinhalt xlink:href="#GML_82527acb-ece6-4125-928e-653005bf4aeb" />
      <xplan:planinhalt xlink:href="#GML_e69087d6-f3f5-4230-aee6-cc5c0c3020fb" />
      <xplan:planinhalt xlink:href="#GML_cda7b3a7-baa3-4e38-8a21-3076e52574c5" />
      <xplan:planinhalt xlink:href="#GML_d3e7ad65-e26a-4945-87fe-afcf1a71932d" />
      <xplan:planinhalt xlink:href="#GML_47617446-63fc-44dd-918b-57464b545e6b" />
      <xplan:planinhalt xlink:href="#GML_3d818c65-b0b2-4263-b716-6f6841b6dd5d" />
      <xplan:planinhalt xlink:href="#GML_f972e2e0-8fad-4aa7-96f6-59533220c80f" />
      <xplan:planinhalt xlink:href="#GML_a96b7e7e-633b-43e6-8a3b-f46a8ec08afb" />
      <xplan:planinhalt xlink:href="#GML_0eca3fe9-27ca-4aff-aaf1-8f94fb83c981" />
      <xplan:planinhalt xlink:href="#GML_9a34bae3-0bb3-4ef8-9a3a-1cbaf4d3d120" />
      <xplan:planinhalt xlink:href="#GML_3af1409c-0164-4674-90e5-31313da6ac6d" />
      <xplan:planinhalt xlink:href="#GML_f7dc7da7-d3c4-4694-a611-5301ba65c890" />
      <xplan:planinhalt xlink:href="#GML_3bee6318-813e-48ec-9cc4-478ddd5ebbbb" />
      <xplan:planinhalt xlink:href="#GML_3ebad625-4912-46eb-bfc1-b41a0cdfe8de" />
      <xplan:planinhalt xlink:href="#GML_7cce43dd-c66b-4034-846f-49bdbe0b4c48" />
      <xplan:planinhalt xlink:href="#GML_52d17b0e-ca95-4234-a548-44ee74a542bc" />
      <xplan:planinhalt xlink:href="#GML_9a4873b8-c3c2-4a2c-ad51-9b5efd286c8b" />
      <xplan:planinhalt xlink:href="#GML_b258f060-321f-4733-bc32-6089b00ca2d9" />
      <xplan:planinhalt xlink:href="#GML_cc6f9fd6-b3b5-4aa1-9ba4-28d96a043fc2" />
      <xplan:planinhalt xlink:href="#GML_f563d570-a475-4455-8a82-edfc277880f8" />
      <xplan:planinhalt xlink:href="#GML_3c708863-041f-49a0-a48a-9d11e8b01407" />
      <xplan:planinhalt xlink:href="#GML_9099cb95-cbfb-4b9d-964a-04a597a05155" />
      <xplan:planinhalt xlink:href="#GML_05ddcd24-31e8-4ac4-b2e8-a9d241e0f68c" />
      <xplan:planinhalt xlink:href="#GML_16f20bf9-77ba-45ab-bc8c-e8a2a45e5a25" />
      <xplan:planinhalt xlink:href="#GML_061be2ce-4f34-4381-8cd9-1172360a9c94" />
      <xplan:planinhalt xlink:href="#GML_3dbb7e2c-0958-456f-9eaa-9276af5962fe" />
      <xplan:planinhalt xlink:href="#GML_d67210c8-bfa4-469e-91f1-ec1623e9df4b" />
      <xplan:planinhalt xlink:href="#GML_75e5c764-13f6-4c64-a5bc-7f8490335488" />
      <xplan:planinhalt xlink:href="#GML_0e0c5781-d873-49fc-bc62-954652ef8b6f" />
      <xplan:planinhalt xlink:href="#GML_ffcb1923-653f-4363-8c00-21c804ae0618" />
      <xplan:planinhalt xlink:href="#GML_654479c2-0386-4f57-b8a4-7958a3e0ba99" />
      <xplan:planinhalt xlink:href="#GML_3634a213-3a14-4e9e-acca-399a652e59e8" />
      <xplan:planinhalt xlink:href="#GML_ff9f2c78-f492-4ce2-b2c6-6b65309a212b" />
      <xplan:planinhalt xlink:href="#GML_3951ee4b-cd21-4031-af9c-0c6d2980857c" />
      <xplan:planinhalt xlink:href="#GML_6aae2f4b-add8-44b0-9d14-1afd8dbc37ca" />
      <xplan:planinhalt xlink:href="#GML_fed9d064-95da-46e1-88d2-ece285db3954" />
      <xplan:planinhalt xlink:href="#GML_10fbc2bb-c1b4-4462-baf0-001d7c12a5e5" />
      <xplan:planinhalt xlink:href="#GML_d00e8001-0ba9-48d1-9bd6-db5b33de5aad" />
      <xplan:planinhalt xlink:href="#GML_9562ca70-343d-482c-861b-03477e9b1b1b" />
      <xplan:planinhalt xlink:href="#GML_bc1f68e2-8fa2-437f-b773-dd73ea36b568" />
      <xplan:planinhalt xlink:href="#GML_a3c6c5b8-6ef8-469e-bf82-1477dd4460a6" />
      <xplan:planinhalt xlink:href="#GML_af2bf57b-a7cf-4fe5-a7c3-e6d60dd17e42" />
      <xplan:planinhalt xlink:href="#GML_256aae4d-0181-4d4e-b08e-5f53f12607e2" />
      <xplan:planinhalt xlink:href="#GML_cc59be5e-e899-47c3-a03d-9b11e90f1eaf" />
      <xplan:planinhalt xlink:href="#GML_3f3c4844-e2c6-41af-917a-ed7b1691c2ba" />
      <xplan:planinhalt xlink:href="#GML_dc732d53-1d3f-4904-bc76-4b72e45dc32f" />
      <xplan:planinhalt xlink:href="#GML_92cddd00-0dd1-41fa-91f2-187f32f25a34" />
      <xplan:planinhalt xlink:href="#GML_35418166-77a9-4c31-a4de-a09e9c734be5" />
      <xplan:planinhalt xlink:href="#GML_e0f8f64a-eadb-4339-a3e7-01b46f8237f9" />
      <xplan:planinhalt xlink:href="#GML_17953607-0953-4f66-bd48-6a53ce01e292" />
      <xplan:planinhalt xlink:href="#GML_76b0d072-3b69-44cd-a786-d6b5b41eafb7" />
      <xplan:planinhalt xlink:href="#GML_de32ebfe-bfba-4528-bbdf-9f7aea8fbeb9" />
      <xplan:planinhalt xlink:href="#GML_0687e417-1b79-4c14-943f-459624c03587" />
      <xplan:planinhalt xlink:href="#GML_4c4fdf72-1206-4c34-b7c0-cf97907f253c" />
      <xplan:planinhalt xlink:href="#GML_4df4f7e4-04dd-4580-b8f1-cab0f1858b3d" />
      <xplan:planinhalt xlink:href="#GML_6d6a209d-c823-4b23-b12e-c691a5bb5c87" />
      <xplan:planinhalt xlink:href="#GML_22d965e6-a303-4e7c-99e5-145f18df1655" />
      <xplan:planinhalt xlink:href="#GML_512fe6ac-e468-4869-9bf4-48fd7155458a" />
      <xplan:planinhalt xlink:href="#GML_b9ec29d9-624d-49ed-9fe9-e6bf45228de9" />
      <xplan:planinhalt xlink:href="#GML_941d9c5c-a90d-4404-8d49-07364d3428e5" />
      <xplan:planinhalt xlink:href="#GML_a5556eba-cc28-4ad2-9908-edab89687e51" />
      <xplan:planinhalt xlink:href="#GML_0b8b172c-afe2-43b6-bc68-5243d1848c6e" />
      <xplan:planinhalt xlink:href="#GML_aa2795da-1ed0-4dec-bf8f-ccfb65e357a7" />
      <xplan:planinhalt xlink:href="#GML_d466ba49-6dbe-42cc-b4c0-b8568a868d06" />
      <xplan:planinhalt xlink:href="#GML_0dd73c4d-7915-46a0-a0d6-02422e122d6e" />
      <xplan:planinhalt xlink:href="#GML_d34293ec-cdbc-4e29-b0fd-14208a6fa8c5" />
      <xplan:planinhalt xlink:href="#GML_48fc4210-0482-4805-9984-f37574f2b898" />
      <xplan:planinhalt xlink:href="#GML_b8a7b538-8618-44c0-bf6d-7a756742e99d" />
      <xplan:planinhalt xlink:href="#GML_f3b549e8-b5a4-442b-aee3-c8084614199d" />
      <xplan:planinhalt xlink:href="#GML_c7e1d227-4566-4be9-8c7b-0ccdbf250ca2" />
      <xplan:planinhalt xlink:href="#GML_d18ea6a3-740f-40b4-9a1f-1af3f613af71" />
      <xplan:planinhalt xlink:href="#GML_c9a274dc-7cf0-4d6c-a721-b2befc8f12d9" />
      <xplan:planinhalt xlink:href="#GML_ae86a301-80ec-435b-86d0-6ea5a0cae013" />
      <xplan:planinhalt xlink:href="#GML_647abc65-d8a9-4cf4-b659-c25ab9da9124" />
      <xplan:planinhalt xlink:href="#GML_39f07895-2ace-4854-9fd3-bc543823c8ef" />
      <xplan:planinhalt xlink:href="#GML_f62a886d-9d38-4be8-9441-51a490efb7c2" />
      <xplan:planinhalt xlink:href="#GML_f272741a-a3f4-4f15-8953-3e9c0015320b" />
      <xplan:planinhalt xlink:href="#GML_688bfdaf-c222-4faa-8154-ce8432616ac3" />
      <xplan:planinhalt xlink:href="#GML_6c68c524-3ebe-4e04-b77a-cd92c4ce514d" />
      <xplan:planinhalt xlink:href="#GML_c8b02f74-b7cf-4917-baff-52fdb3b38b94" />
      <xplan:planinhalt xlink:href="#GML_de03f542-3875-45ea-80a2-1b10a9c1b373" />
      <xplan:planinhalt xlink:href="#GML_19ae3847-02f9-4459-b58b-4131298290e8" />
      <xplan:planinhalt xlink:href="#GML_1d5b5d0a-2d41-40d9-9cde-6826ed0ca100" />
      <xplan:planinhalt xlink:href="#GML_67369fc7-d714-4bf7-b9df-65ad2579f46a" />
      <xplan:planinhalt xlink:href="#GML_455dcd6a-e725-45a4-8284-59f8cbc74875" />
      <xplan:planinhalt xlink:href="#GML_cb81c23d-32ef-4d96-9697-a38d1f3ccd14" />
      <xplan:planinhalt xlink:href="#GML_83c9160c-48dd-4c74-9928-62dbc3c41d9c" />
      <xplan:planinhalt xlink:href="#GML_6eb8a849-8f4c-429e-ba74-61cbddf8d5cc" />
      <xplan:planinhalt xlink:href="#GML_0072c790-0132-43c5-9d61-c7fc67792969" />
      <xplan:planinhalt xlink:href="#GML_0ae17b01-dfe6-4efc-b166-ff9f2a512fd3" />
      <xplan:planinhalt xlink:href="#GML_fe669d60-3a70-4cce-830a-a06515c9caa8" />
      <xplan:planinhalt xlink:href="#GML_260bcdc5-a5d0-4a5f-aa6c-76a1165f860a" />
      <xplan:planinhalt xlink:href="#GML_d1aead2b-00c0-4cbd-bdc5-16417f9b8847" />
      <xplan:planinhalt xlink:href="#GML_27586df2-0bca-4554-9289-2968d521fef9" />
      <xplan:planinhalt xlink:href="#GML_1b5ec360-bc95-4972-b51c-ab6265d635fd" />
      <xplan:planinhalt xlink:href="#GML_1d5bcaf3-35bd-4301-b05c-42a472e25700" />
      <xplan:planinhalt xlink:href="#GML_9dfcc4fc-111d-4c72-a6a8-85d3dc431505" />
      <xplan:planinhalt xlink:href="#GML_8bf2aed9-1e5a-45c3-a2e3-7d6d8eb80466" />
      <xplan:planinhalt xlink:href="#GML_f5d3e793-af9f-4768-bdde-d1a8b99ed490" />
      <xplan:planinhalt xlink:href="#GML_de453833-d822-471a-88f8-b6803bfb6eed" />
      <xplan:planinhalt xlink:href="#GML_be28ff05-2595-448e-bcb4-0302d0bd4b2a" />
      <xplan:planinhalt xlink:href="#GML_b1340782-fbfb-4fed-bfb3-f8b556eba0da" />
      <xplan:planinhalt xlink:href="#GML_01063915-d781-4171-9a47-f1181a0a95ba" />
      <xplan:planinhalt xlink:href="#GML_3261c6e0-f590-4376-9e31-7519f4941009" />
      <xplan:planinhalt xlink:href="#GML_e356cd30-3c8a-48d8-8b67-2bcbdf689a10" />
      <xplan:planinhalt xlink:href="#GML_52094ae1-c883-4ec2-9993-09c1d4878af0" />
      <xplan:planinhalt xlink:href="#GML_8a3bb7d0-a795-4702-96ad-590ecf76dda6" />
      <xplan:planinhalt xlink:href="#GML_01fa555e-90d4-42a0-88bc-f42d913273b1" />
      <xplan:planinhalt xlink:href="#GML_fbb6a595-befb-47c4-b114-c89c09c2985b" />
      <xplan:planinhalt xlink:href="#GML_ddf01997-bbf6-42da-81e6-f87ea884f470" />
      <xplan:planinhalt xlink:href="#GML_9643427d-8ce9-4078-a2ba-2ceddd84616a" />
      <xplan:planinhalt xlink:href="#GML_a0579862-62aa-4c2e-8178-aa46e54c3a6f" />
      <xplan:planinhalt xlink:href="#GML_6311f207-555c-4ebf-ad4f-3a93b10b84d5" />
      <xplan:planinhalt xlink:href="#GML_917ed001-5f60-4fd6-9ada-6f82e6bc8139" />
      <xplan:planinhalt xlink:href="#GML_6092cc1f-98fb-44a3-b388-42c684bcf36d" />
      <xplan:planinhalt xlink:href="#GML_1b0823c1-7c0d-405a-8ee4-1bcb5fe76e24" />
      <xplan:planinhalt xlink:href="#GML_b55e2425-6b11-482b-abd4-6170ffbe6625" />
      <xplan:planinhalt xlink:href="#GML_55d1c764-ac8e-4ebd-88e2-b6586115f4c5" />
      <xplan:planinhalt xlink:href="#GML_2015bec0-6b08-4f2e-bf2d-3db16497baab" />
      <xplan:planinhalt xlink:href="#GML_a129458c-748f-44d9-bd66-171737027e3c" />
      <xplan:planinhalt xlink:href="#GML_9108ee83-2efb-499e-b526-8c60f20f0835" />
      <xplan:planinhalt xlink:href="#GML_82885d40-e137-471e-83e0-b252925369cb" />
      <xplan:planinhalt xlink:href="#GML_722a9724-bcc1-4d0b-8bd5-53523832afb0" />
      <xplan:planinhalt xlink:href="#GML_975cab29-9993-470a-8c5d-d419e1fef571" />
      <xplan:planinhalt xlink:href="#GML_56432479-b8d8-49a9-8da2-28505eec7075" />
      <xplan:planinhalt xlink:href="#GML_b868966b-7f46-4e1f-a4a2-b68d25547d31" />
      <xplan:planinhalt xlink:href="#GML_e0a7cedd-d8c8-4885-897d-cd238e8d04db" />
      <xplan:planinhalt xlink:href="#GML_67b85773-6f21-4611-8012-bd1b32d2e2c1" />
      <xplan:planinhalt xlink:href="#GML_61882aca-5ec0-48e6-9fdc-2f7fd0cb40ef" />
      <xplan:planinhalt xlink:href="#GML_8accc93d-669f-4b1a-8728-d5a075b4c479" />
      <xplan:planinhalt xlink:href="#GML_da61ad55-4853-4292-9ff5-243cf3153ad3" />
      <xplan:planinhalt xlink:href="#GML_6a0d5315-0339-42a3-98f1-047b6ac3db00" />
      <xplan:planinhalt xlink:href="#GML_1f50f386-aea5-4b7c-890c-60bd27bc539e" />
      <xplan:planinhalt xlink:href="#GML_23c367a7-5d9f-4e6d-ba88-584b8cb82ebc" />
      <xplan:planinhalt xlink:href="#GML_5b773436-fc36-44f9-ada1-a45f28478333" />
      <xplan:planinhalt xlink:href="#GML_72b6588a-aab2-434e-bec5-ad5e33faace6" />
      <xplan:planinhalt xlink:href="#GML_bc6acd24-71c2-4121-8763-72c862f3914e" />
      <xplan:planinhalt xlink:href="#GML_7048fffd-1bdb-42e8-a6ee-d07b5907fdca" />
      <xplan:planinhalt xlink:href="#GML_0904f805-e3aa-46e8-aa0a-203dd664a292" />
      <xplan:planinhalt xlink:href="#GML_d1267281-0849-43ef-92b7-7d2048a7b272" />
      <xplan:planinhalt xlink:href="#GML_a6b9fa92-7fd5-4840-bc50-80c6d7da8dc1" />
      <xplan:planinhalt xlink:href="#GML_c07b06ba-314b-4229-b154-4d1ec37bba2f" />
      <xplan:planinhalt xlink:href="#GML_1bab3a54-f3ae-42fa-942f-c8cd774036c2" />
      <xplan:planinhalt xlink:href="#GML_c85011d4-394b-4b52-bae1-bf7cc8a798af" />
      <xplan:planinhalt xlink:href="#GML_ac2d27bf-e328-4302-b98b-343127e9b6f6" />
      <xplan:planinhalt xlink:href="#GML_c5e60282-572a-4481-9d9f-1a3f9253da31" />
      <xplan:planinhalt xlink:href="#GML_0cabdc46-fa47-4894-8520-b91f42beafdc" />
      <xplan:planinhalt xlink:href="#GML_3ce0a5df-ecb1-4368-b86d-3e38f0400656" />
      <xplan:planinhalt xlink:href="#GML_f8c76475-e101-46a4-bd1a-027860c6b5de" />
      <xplan:planinhalt xlink:href="#GML_3383bed7-613a-481c-b697-be51752fcceb" />
      <xplan:planinhalt xlink:href="#GML_6227ee87-ec9a-4601-b068-16cf51c46ec2" />
      <xplan:planinhalt xlink:href="#GML_9daf8ab2-a0d2-4026-898e-257ac8eb9faf" />
      <xplan:planinhalt xlink:href="#GML_6be57063-39f8-4b4d-89c1-3f45ce1c66fc" />
      <xplan:planinhalt xlink:href="#GML_0cbb5e9d-3d21-44d4-84f7-6a995476f627" />
      <xplan:planinhalt xlink:href="#GML_d1b5dd40-5401-4778-ade7-ad8f64602d86" />
      <xplan:planinhalt xlink:href="#GML_671df8ee-b64e-4b70-9dc9-3bd0b070c72a" />
      <xplan:planinhalt xlink:href="#GML_c0998984-2b93-4408-8b8d-46e2b534c3b4" />
      <xplan:planinhalt xlink:href="#GML_7655911c-722f-4905-870f-623fb7714c52" />
      <xplan:planinhalt xlink:href="#GML_045eba20-890a-497d-9a0a-69132d901c68" />
      <xplan:planinhalt xlink:href="#GML_9afb0fc3-dd77-41b1-b5f8-1afd24238689" />
      <xplan:planinhalt xlink:href="#GML_5754c10b-2762-4317-9be0-9e6599391792" />
      <xplan:planinhalt xlink:href="#GML_e08d085b-6b7e-4743-86b3-a8638ad35c73" />
      <xplan:planinhalt xlink:href="#GML_4ce195e1-0c5e-421b-a6e2-53a3efaccbe4" />
      <xplan:planinhalt xlink:href="#GML_862ae865-2d97-47bb-97c6-08171757657b" />
      <xplan:planinhalt xlink:href="#GML_52ea432b-2027-4255-a410-6ed9d888911f" />
      <xplan:planinhalt xlink:href="#GML_2f028f76-2db3-442c-ab15-c3e04d7ba925" />
      <xplan:planinhalt xlink:href="#GML_656fea01-d7ac-4d77-9d10-4f5e770fc779" />
      <xplan:planinhalt xlink:href="#GML_cbbebd11-86e5-4d45-addc-0841f1e92a39" />
      <xplan:planinhalt xlink:href="#GML_6fa314b1-c97c-475d-a324-f3bb0536629a" />
      <xplan:planinhalt xlink:href="#GML_157049a7-2bfa-49c4-bbd7-16fed4f65a73" />
      <xplan:planinhalt xlink:href="#GML_c2b9d550-2444-4799-b789-0f0fe3b1d7cb" />
      <xplan:planinhalt xlink:href="#GML_9e7c78e6-69f6-4b31-89de-4748bcb645fe" />
      <xplan:planinhalt xlink:href="#GML_6dcf5c9e-b618-4122-b1e1-6de3a206ad8f" />
      <xplan:planinhalt xlink:href="#GML_560d5848-3aa2-482e-b2a7-9cad75788ecc" />
      <xplan:planinhalt xlink:href="#GML_2d98de27-e595-430b-b7f1-f09ce3493343" />
      <xplan:planinhalt xlink:href="#GML_9f9aaf8d-a8d5-49e9-9214-6f2e2b2f2948" />
      <xplan:planinhalt xlink:href="#GML_3e44a8a3-2e27-4cca-af34-d46e8561dd12" />
      <xplan:planinhalt xlink:href="#GML_bc672f2f-b760-4538-be97-fd44840abb56" />
      <xplan:planinhalt xlink:href="#GML_9f73fd87-3c94-459b-98db-e7ee438b3f33" />
      <xplan:planinhalt xlink:href="#GML_a9cd8d73-e8ac-469b-87a9-3beb2c925f68" />
      <xplan:planinhalt xlink:href="#GML_42340637-90e6-459e-8ade-f5c577d42407" />
      <xplan:planinhalt xlink:href="#GML_78660969-b98c-486f-80e6-0e1866312b19" />
      <xplan:planinhalt xlink:href="#GML_73250940-e373-48bf-86ff-2f44846a4bbc" />
      <xplan:planinhalt xlink:href="#GML_92fdc00f-053e-43cd-b4a5-8296167b6975" />
      <xplan:planinhalt xlink:href="#GML_f972361c-1dd3-410d-a9b8-23859ab6f9da" />
      <xplan:planinhalt xlink:href="#GML_59ddf1de-5576-4fdb-9121-1f0eda08b3f9" />
      <xplan:planinhalt xlink:href="#GML_4226b49b-8d87-4683-b5ed-3b7ca47fca86" />
      <xplan:planinhalt xlink:href="#GML_27485c38-dad5-4b2a-9ef7-9a3fd75c089e" />
      <xplan:planinhalt xlink:href="#GML_bec94b65-0f9a-4ffb-a616-46f6f2893fe5" />
      <xplan:planinhalt xlink:href="#GML_e0d3029c-3d59-4b56-8fc3-b61f50da3f42" />
      <xplan:planinhalt xlink:href="#Gml_284B179C-3467-44F6-9B04-C2014D529B66" />
      <xplan:planinhalt xlink:href="#Gml_9D324BB6-40A7-46AA-B8AA-4340B40B8173" />
      <xplan:planinhalt xlink:href="#Gml_223B043C-1A7A-4AE2-BA39-577D6655F371" />
      <xplan:planinhalt xlink:href="#Gml_6157700A-BB36-459E-BCDC-06517E81E6E3" />
      <xplan:planinhalt xlink:href="#Gml_9C7CBD5B-194F-4F60-8DA4-974188283E0A" />
      <xplan:planinhalt xlink:href="#Gml_F5814B54-E41D-4193-B85C-59D7D129876D" />
      <xplan:planinhalt xlink:href="#Gml_C06DFF33-FE68-4435-8BDF-AC62C1C93810" />
      <xplan:planinhalt xlink:href="#GML_0238e7f9-3ca7-4798-aeea-f49bba91128b" />
      <xplan:planinhalt xlink:href="#GML_4d98ae00-0a48-4e96-9e52-5799884a21d1" />
      <xplan:planinhalt xlink:href="#GML_be90417b-fc6c-4f10-8cae-37dd3e57effe" />
      <xplan:praesentationsobjekt xlink:href="#GML_7fcbdd4f-b89d-4201-b7dd-21fb6aca7680" />
      <xplan:praesentationsobjekt xlink:href="#GML_921e7bd3-21e4-4ee1-9cb4-71c77b13a037" />
      <xplan:praesentationsobjekt xlink:href="#GML_be6d6a08-a892-425f-8c06-095641545e1e" />
      <xplan:praesentationsobjekt xlink:href="#GML_2f4da185-59ea-4615-96f3-7ead1214baf8" />
      <xplan:praesentationsobjekt xlink:href="#GML_f3f6e29f-8615-45af-811a-0ec5af006e54" />
      <xplan:praesentationsobjekt xlink:href="#GML_107c5d99-db8e-411e-93d7-e554b1c9040d" />
      <xplan:praesentationsobjekt xlink:href="#GML_3f319409-48c8-4375-b7de-fc41f83f9a2e" />
      <xplan:praesentationsobjekt xlink:href="#GML_a1b0fc92-aa10-477e-a9ae-0ee6eb0d8ade" />
      <xplan:praesentationsobjekt xlink:href="#GML_c8465bd9-6303-4487-b384-05db29667696" />
      <xplan:praesentationsobjekt xlink:href="#GML_19adf723-36ed-450b-b941-884a9ff7c295" />
      <xplan:praesentationsobjekt xlink:href="#GML_5c7b2ca0-ef48-49ed-8e00-937f731529a1" />
      <xplan:praesentationsobjekt xlink:href="#GML_ad05c3b2-e661-4399-91a4-270b968056e5" />
      <xplan:praesentationsobjekt xlink:href="#GML_af0b767c-a6f7-4b47-8ae5-02d7bc47d8ca" />
      <xplan:praesentationsobjekt xlink:href="#GML_2d033d54-2280-4672-afa8-a5cdf8913760" />
      <xplan:praesentationsobjekt xlink:href="#GML_986a57d6-df0e-4455-aea4-faf64c4e0514" />
      <xplan:praesentationsobjekt xlink:href="#GML_29d670c8-b105-44d8-b7e1-c876875ef6ee" />
      <xplan:praesentationsobjekt xlink:href="#GML_767dc8ec-28f4-4d57-af1d-6b3315f04cfa" />
      <xplan:praesentationsobjekt xlink:href="#GML_d662ff6d-a0da-452f-af2b-525735527e98" />
      <xplan:praesentationsobjekt xlink:href="#GML_f2852870-c3e6-45f2-b0a8-665b88b9fb01" />
      <xplan:praesentationsobjekt xlink:href="#GML_25421154-ef11-45cf-aca4-5c92e2a84cab" />
      <xplan:praesentationsobjekt xlink:href="#GML_1f5bf61b-d4a1-4902-932a-d905ba332096" />
      <xplan:praesentationsobjekt xlink:href="#GML_f4857370-0598-43ea-9e1a-0afa5f537872" />
      <xplan:praesentationsobjekt xlink:href="#GML_6f522580-1c03-4894-88f7-ef42993d840b" />
      <xplan:praesentationsobjekt xlink:href="#GML_425e24d9-a273-42ef-ad68-7d3d2dae2a1c" />
      <xplan:praesentationsobjekt xlink:href="#GML_fd7fa05c-488e-4740-9638-71b5ce4fb5d0" />
      <xplan:praesentationsobjekt xlink:href="#GML_160705bc-f465-4c54-821c-18dcca3c6ece" />
      <xplan:praesentationsobjekt xlink:href="#GML_a3b95b37-45b0-4631-897f-6294dacb9d75" />
      <xplan:praesentationsobjekt xlink:href="#GML_06237df3-80ed-4071-9a6f-e73fd91115ba" />
      <xplan:praesentationsobjekt xlink:href="#GML_e3da762a-0c16-4e0e-be61-0644b743e2ff" />
      <xplan:praesentationsobjekt xlink:href="#GML_762cf87b-beaa-4055-b88f-ed7cf784a1b4" />
      <xplan:praesentationsobjekt xlink:href="#GML_b2f0e7a1-047e-436a-8a3f-04e94c050fba" />
      <xplan:praesentationsobjekt xlink:href="#GML_88c1e7d6-27f5-4135-9cc9-1067f6ff035d" />
      <xplan:praesentationsobjekt xlink:href="#GML_b30adbd6-d570-4bde-a049-2a5ed93a4a2a" />
      <xplan:praesentationsobjekt xlink:href="#GML_04859a48-ae10-4338-ae75-b96d25a1e2e8" />
      <xplan:praesentationsobjekt xlink:href="#GML_c4ce8c39-c148-4cd2-a900-7c301aa21a77" />
      <xplan:praesentationsobjekt xlink:href="#GML_d85ded2e-455c-49bf-965f-df9db566f230" />
      <xplan:praesentationsobjekt xlink:href="#GML_b1f51a46-920d-431c-8259-b744430fd8e4" />
      <xplan:praesentationsobjekt xlink:href="#GML_a7e35054-373c-4d0d-996d-f47a0eb7021f" />
      <xplan:praesentationsobjekt xlink:href="#GML_7ae5a119-fcf3-40c3-a792-11f11a22b23d" />
      <xplan:praesentationsobjekt xlink:href="#GML_40386b79-18af-4f9e-b06d-59aec6e5285f" />
      <xplan:praesentationsobjekt xlink:href="#GML_f9acd0e1-08c3-4ef8-b4cf-0a6d9802ea20" />
      <xplan:praesentationsobjekt xlink:href="#GML_f68c3ec9-9e6e-4e91-ab85-904719cd5c1a" />
      <xplan:praesentationsobjekt xlink:href="#GML_393e377b-d231-4590-ae50-e1a01a1b9b2d" />
      <xplan:praesentationsobjekt xlink:href="#GML_754ec6d4-2bcc-46e3-a967-c3f69f9c0513" />
      <xplan:praesentationsobjekt xlink:href="#GML_50973c78-7db3-4bd0-8214-435b7f42455b" />
      <xplan:praesentationsobjekt xlink:href="#GML_67ee41de-6cab-4a6b-9353-b48392691a24" />
      <xplan:praesentationsobjekt xlink:href="#GML_ce0398e9-44b8-4e23-8b03-034a7cc8fe96" />
      <xplan:praesentationsobjekt xlink:href="#GML_40108ddd-a238-489f-9c0e-b9d9a53ec338" />
      <xplan:praesentationsobjekt xlink:href="#GML_5615e2e3-d58e-4a20-96df-8610e70a44ce" />
      <xplan:praesentationsobjekt xlink:href="#GML_fcd08368-5940-432c-989e-1f81afe63ff7" />
      <xplan:praesentationsobjekt xlink:href="#GML_a5da6db5-e492-4456-888e-06f5dd899507" />
      <xplan:praesentationsobjekt xlink:href="#GML_a72903a0-15f6-4030-942b-19dc3a143f64" />
      <xplan:praesentationsobjekt xlink:href="#GML_d94706b1-832a-4940-a5dc-f2cc91471e9b" />
      <xplan:praesentationsobjekt xlink:href="#GML_61b126da-95ce-4cf0-bb45-5616e479ccc8" />
      <xplan:praesentationsobjekt xlink:href="#GML_a588b80d-4031-4be4-aef6-acac3c43cf65" />
      <xplan:praesentationsobjekt xlink:href="#GML_59a6f60b-ad27-45c1-80ab-9362c3fdd4e0" />
      <xplan:praesentationsobjekt xlink:href="#GML_1cac905f-f8c9-4115-9dbd-e1c5bd0c1dbd" />
      <xplan:praesentationsobjekt xlink:href="#GML_b08c9ee6-0ecf-4dfe-b86d-0a1fe0159faf" />
      <xplan:praesentationsobjekt xlink:href="#GML_a922b98b-a3da-44e7-bf2b-b6a033b1e361" />
      <xplan:praesentationsobjekt xlink:href="#GML_6a184ea3-b80c-421d-af52-a7ce3b9e4c55" />
      <xplan:praesentationsobjekt xlink:href="#GML_10de8bdc-8d40-4111-aee9-c1460fd1d912" />
      <xplan:praesentationsobjekt xlink:href="#GML_84e43113-9f5b-43f5-bc22-48b6cf5e8c08" />
      <xplan:praesentationsobjekt xlink:href="#GML_4fcd0d62-647d-440c-9b4f-d6483da922ca" />
      <xplan:praesentationsobjekt xlink:href="#GML_571389f3-dcd1-4512-aa86-f03228711cae" />
      <xplan:praesentationsobjekt xlink:href="#GML_9ead2ab7-55ef-457c-9d21-07d267fe7780" />
      <xplan:praesentationsobjekt xlink:href="#GML_9028834a-eff2-470a-8a46-1a6335c2f17c" />
      <xplan:praesentationsobjekt xlink:href="#GML_2e133a2d-d355-47c8-b0a8-dc884245b3dd" />
      <xplan:praesentationsobjekt xlink:href="#GML_a03df1e3-0787-4f6a-bc90-05063271b33d" />
      <xplan:praesentationsobjekt xlink:href="#GML_1ec79299-f886-472b-981d-74f6de2caf51" />
      <xplan:versionBauNVODatum>1990-01-01</xplan:versionBauNVODatum>
      <xplan:versionBauNVOText>Version vom 11.06.2013</xplan:versionBauNVOText>
      <xplan:gehoertZuPlan xlink:href="#GML_4ba83953-00d3-4209-b080-a356ad509869" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_36192594-a872-4644-813d-c4bf0058dd0b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566984.022 5932512.784</gml:lowerCorner>
          <gml:upperCorner>567006.571 5932529.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B1DE6E4C-C6D7-42E3-9E5D-FCD0D9BFDF05" srsName="EPSG:25832">
          <gml:posList>566984.022 5932529.955 566984.047 5932517.15 567003.884 5932512.784 
567006.571 5932524.992 566997.526 5932526.982 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_21bf5d31-e811-465d-b22d-ccc20c13b0e9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566863.92 5932584.79</gml:lowerCorner>
          <gml:upperCorner>566879.91 5932610.866</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_84329dc1-833d-4d58-96a7-a19a8aa4ebe5" />
      <xplan:refTextInhalt xlink:href="#GML_4a229a55-87d9-41e7-a476-f3122e8477d9" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9F6D3392-56CA-4B5A-AD5F-DE591556CAA7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566879.91 5932607.619 566863.92 5932610.866 566875.31 5932584.79 
566879.91 5932607.619 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="GML_d34c8ce0-8509-45a5-8dd1-ae56b430372d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566806.3997 5932521.988</gml:lowerCorner>
          <gml:upperCorner>567470.445 5932779.633</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Baakenhafen</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>3000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_DCB9272A-8121-4393-AB70-D91874B1BAAB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D8704A0D-05BA-4445-A019-1A4A1D097B12" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566933.255 5932760.356 566885.545 5932770.145 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B506D992-F0BD-4457-9317-FC0FF1EC239D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566885.545 5932770.145 566839.305 5932779.633 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_92747BD3-86D6-4B89-B16B-7FBACCF78A48" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566839.305 5932779.633 566833.794 5932737.623 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F6E14E21-6DCE-40AF-AF12-B6C0FD4F8BCA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566833.794 5932737.623 566806.3997 5932741.7876 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1731C4A8-DB09-4D15-AF21-0FFC438B1DB6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566806.3997 5932741.7876 566807.857 5932738.66 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_5A82F835-CF86-497E-BBEC-E13208B6D752" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566807.857 5932738.66 566812.472959597 5932729.93307302 566817.414 5932721.386 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_7750EDAD-F686-4B8A-917B-AB38AD1CFE10" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566817.414 5932721.386 566819.364005272 5932718.03299084 566821.281 5932714.661 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_D1E661F6-C407-4865-87F4-8581253663B3" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566821.281 5932714.661 566823.111316406 5932711.30040007 566824.851 5932707.892 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_69FD8F92-BA57-47B2-A794-F76308B9C781" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566824.851 5932707.892 566827.957952836 5932700.81212196 566830.423 5932693.484 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6485CF17-EEA2-46F7-B11D-1FBFF5B8111A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566830.423 5932693.484 566835.3753 5932676.0536 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A533267D-7E0C-416C-BD61-87BB107F320E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566835.3753 5932676.0536 566909.885 5932663.065 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7C7E225C-50DA-409B-A29B-AD68F4FCCA2E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566909.885 5932663.065 566915.858 5932661.871 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_013654C7-EF45-4FCA-B16C-BBA821B0E566" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566915.858 5932661.871 566986.402 5932647.564 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C99DAAD7-8E35-43D0-87F6-379000407D80" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566986.402 5932647.564 566992.298 5932646.3 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BD9A6A80-3001-48B2-8170-1500416E3240" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566992.298 5932646.3 567049.947 5932633.375 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3F2B74C3-5278-4D59-9374-CDD9CCC42D0F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567049.947 5932633.375 567060.314 5932630.959 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F90D9D5C-0E8F-4E52-868D-9D957E968DA1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567060.314 5932630.959 567104.243 5932620.722 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BA57CE3C-405E-412C-87E3-3FF55DF39203" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567104.243 5932620.722 567105.836 5932620.31 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3FF71E09-210B-4CC2-ADEF-2766D0A0DBEF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567105.836 5932620.31 567111.875 5932618.745 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EC227978-0579-4AD7-8F21-7FCC94597CB4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567111.875 5932618.745 567123.474 5932627.696 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E9AC53A6-1733-43E3-B700-B4A094B5958F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567123.474 5932627.696 567121.123 5932630.567 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_F26135FC-1DAB-4924-B790-D9B74B31E1E0" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567121.123 5932630.567 567120.26402466 5932633.22945013 567121.409 5932635.782 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FD32EA0F-D231-4ECC-97F4-7912FB0F942C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567121.409 5932635.782 567157.554 5932671.713 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_7ACF2D41-AA33-4FD6-B5F8-A28B870B9514" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567157.554 5932671.713 567159.940300474 5932673.1620966 567162.724 5932673.375 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7937C148-3844-4B44-BCD4-64A4129AFA65" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567162.724 5932673.375 567170.264 5932672.115 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_16F20AA4-0607-484D-B105-CAA668B5C194" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567170.264 5932672.115 567179.292 5932670.725 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C6A457A2-12AB-46C9-BCD9-BA98C5EEADDA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567179.292 5932670.725 567224.757 5932663.401 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_1B72DCF9-7E88-4F7A-9EBD-17A8AA492438" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567224.757 5932663.401 567227.642169331 5932662.20687756 567229.68 5932659.841 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BAC4B5E8-20A8-4382-8637-6CD56F3878BA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567229.68 5932659.841 567240.942 5932638.06 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1E983520-A290-4438-8790-28D88106B21D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567240.942 5932638.06 567246.611 5932643.323 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_1C047A5B-9F8A-44F7-90A9-46FD866F52F4" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567246.611 5932643.323 567249.306595184 5932644.60808494 567252.254 5932644.128 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3A275111-9489-466C-8E42-7C852072D7FA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567252.254 5932644.128 567263.23 5932638.624 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_58B3CF60-7554-4C3C-8048-14833A9D2A0C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567263.23 5932638.624 567267.409 5932646.089 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_41E1CCA4-CA3E-4710-8C41-536484072341" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567267.409 5932646.089 567268.631652061 5932647.10088381 567270.217 5932647.027 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_44735325-1CD6-49C2-9124-5DAF6A258DD3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567270.217 5932647.027 567289.481 5932638.529 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3003D5C8-82CE-4E57-80D6-E5530DE05400" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567289.481 5932638.529 567315.262 5932627.157 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_B7B0C893-AE35-41C2-B74C-07029A37DD31" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567315.262 5932627.157 567316.38591533 5932626.03519128 567316.461 5932624.449 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9AB88915-E281-4EED-A038-5D8A0799754A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567316.461 5932624.449 567312.888 5932613.722 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_419B18C2-4CA8-44CB-B1E2-333171ED1121" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567312.888 5932613.722 567332.567 5932603.854 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7E53B5A4-1994-4335-BF5D-8DEE68B7581B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567332.567 5932603.854 567332.608 5932606.292 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_F2EF52DD-4C2D-4818-8530-D61B9C1985ED" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567332.608 5932606.292 567332.94704748 5932607.217058 567333.787 5932607.732 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7DC7700B-9BB5-4BC8-B1B9-5FFFE7FB4EF0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567333.787 5932607.732 567347.217 5932610.684 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_781FE8C7-B9D2-4202-83E8-D1A4F3E6B031" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567347.217 5932610.684 567348.190716628 5932610.57013671 567348.886 5932609.879 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8C077F1E-15C1-4B21-9D49-394E8D661AA0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567348.886 5932609.879 567350.262 5932607.073 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8DA88A4E-623F-4E67-92A1-46BE2401B3C4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567350.262 5932607.073 567354.756 5932597.904 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6012A510-BDD8-4F1E-833D-2E1E0EFC2BEF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567354.756 5932597.904 567372.068 5932598.244 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_8B28218E-38E5-43BB-BA91-D58CDBB2579E" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567372.068 5932598.244 567377.133845179 5932596.69210919 567380.296 5932592.441 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D865B386-6918-4200-9326-961ED23A2D48" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567380.296 5932592.441 567392.599 5932555.691 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_516FB7B3-7086-4579-87EE-545FAA646E16" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567392.599 5932555.691 567391.949168066 5932551.43723327 567388.12 5932549.474 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_76F1A9EB-0288-4ECB-A25B-F14570D92A52" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567388.12 5932549.474 567380.373 5932549.468 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B713454A-39AF-4815-8533-0EF1D4308795" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567380.373 5932549.468 567381.274 5932541.041 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5C15AEAA-DA3C-4EBF-82EC-B7EF3E3A2CB8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567381.274 5932541.041 567386.424 5932539.397 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_8F910FF1-5284-4AD3-A0DB-E96DE2714A20" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567386.424 5932539.397 567403.359597057 5932533.78913725 567420.197 5932527.893 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_A61E6FC0-E52E-4B54-AD3E-3E8EA1E78546" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567420.197 5932527.893 567428.380122673 5932524.98095565 567436.534 5932521.988 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_56FC570C-DE34-45AB-883A-B6B397FC48CC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567436.534 5932521.988 567470.445 5932624.6 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9BF282A3-F691-4488-96FD-3A2ACC4B57D4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567470.445 5932624.6 567374.73 5932651.63 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_334F989A-0CB3-4CAC-801B-C11B55CD6C02" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567374.73 5932651.63 567189.373 5932700.56 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D38675D3-3C22-40F8-8AA4-CFE6681DBEA9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567189.373 5932700.56 567084.333 5932725.96 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4302CD9D-7CFD-4286-AAA9-6CF3E89AFDEF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567084.333 5932725.96 566933.255 5932760.356 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_b1955143-ae0d-4b61-91b9-7560bb978540">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566933.72 5932609.592</gml:lowerCorner>
          <gml:upperCorner>566961.842 5932627.685</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_107c5d99-db8e-411e-93d7-e554b1c9040d" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_FEF872F0-40E1-4767-8079-658193871B7A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566961.842 5932622.292 566949.134 5932625.001 566936.463 5932627.685 
566933.72 5932614.97 566958.735 5932609.686 566959.158 5932609.592 
566961.842 5932622.292 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_107c5d99-db8e-411e-93d7-e554b1c9040d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566945.043 5932619.223</gml:lowerCorner>
          <gml:upperCorner>566945.043 5932619.223</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_b1955143-ae0d-4b61-91b9-7560bb978540" />
      <xplan:position>
        <gml:Point gml:id="Gml_B8E2E19B-BFDA-4C18-9353-1A611D4E0111" srsName="EPSG:25832">
          <gml:pos>566945.043 5932619.223</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_f5140669-1dea-4f5d-878e-c00e88b23928">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567063.184 5932488.352</gml:lowerCorner>
          <gml:upperCorner>567119.194 5932563.724</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gliederung1>(F)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_921e7bd3-21e4-4ee1-9cb4-71c77b13a037" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_8d068ddb-d876-42a4-9414-cdb642b6695f" />
      <xplan:refTextInhalt xlink:href="#GML_4a229a55-87d9-41e7-a476-f3122e8477d9" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_19738754-33AD-4451-A184-DD80A3314504" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567105.392 5932500.439 567119.194 5932553.393 567078.123 5932563.724 
567077.39 5932507.79 567077.223 5932495.039 567063.768 5932498.267 
567063.184 5932495.848 567084.347 5932490.73 567086.106 5932490.73 
567086.102 5932492.649 567102.204 5932488.352 567105.392 5932500.439 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
      <xplan:typ>2000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_921e7bd3-21e4-4ee1-9cb4-71c77b13a037">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567096.436 5932539.911</gml:lowerCorner>
          <gml:upperCorner>567096.436 5932539.911</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f5140669-1dea-4f5d-878e-c00e88b23928" />
      <xplan:schriftinhalt>(F)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_5A35FCB8-974A-441A-A2E7-17E8403BCFDF" srsName="EPSG:25832">
          <gml:pos>567096.436 5932539.911</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ad414f6c-9d69-436e-8f0f-5a71a202b632">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566875.309 5932532.088</gml:lowerCorner>
          <gml:upperCorner>566928.911 5932607.63</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(A)</xplan:gliederung1>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_25421154-ef11-45cf-aca4-5c92e2a84cab" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_6f522580-1c03-4894-88f7-ef42993d840b" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BB2690DB-0DCE-42DD-AE7C-9EE939C51B7F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566928.911 5932597.672 566879.912 5932607.63 566875.309 5932584.788 
566896.66 5932535.909 566915.755 5932532.088 566892.027 5932586.672 
566892.841 5932590.731 566926.177 5932584.045 566928.911 5932597.672 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_25421154-ef11-45cf-aca4-5c92e2a84cab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566884.862 5932579.787</gml:lowerCorner>
          <gml:upperCorner>566884.862 5932579.787</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ad414f6c-9d69-436e-8f0f-5a71a202b632" />
      <xplan:position>
        <gml:Point gml:id="Gml_11B4D715-2DE0-463F-BEC6-EA0CE2D3187B" srsName="EPSG:25832">
          <gml:pos>566884.862 5932579.787</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_6f522580-1c03-4894-88f7-ef42993d840b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566884.862 5932585.293</gml:lowerCorner>
          <gml:upperCorner>566884.862 5932585.293</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ad414f6c-9d69-436e-8f0f-5a71a202b632" />
      <xplan:schriftinhalt>(A)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_479C8503-FE06-4579-BE03-02A9FCBC3C14" srsName="EPSG:25832">
          <gml:pos>566884.862 5932585.293</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_340cca07-7af7-4f8a-86e1-a26cffd2daf1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567225.33 5932429.553</gml:lowerCorner>
          <gml:upperCorner>567225.33 5932429.553</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">5.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_B1F41A84-BEB8-4190-84A1-2917FE37AF55" srsName="EPSG:25832">
          <gml:pos>567225.33 5932429.553</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_322ffe3c-3d69-437b-8bb0-a9bf9eef5e42">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566918.873 5932668.685</gml:lowerCorner>
          <gml:upperCorner>566940.875 5932695.911</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_D71F5B24-2E47-4477-B88E-4E27D60E65DB" srsName="EPSG:25832">
          <gml:posList>566937.055 5932668.685 566939.119 5932679.68 566939.828 5932685.147 
566940.875 5932693.428 566927.647 5932695.911 566922.931 5932688.214 
566922.294 5932686.911 566921.668 5932685.595 566921.297 5932684.396 
566918.873 5932672.098 566937.055 5932668.685 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_cb5d6f11-acb5-4fc8-99c7-994110b75438">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567228.633 5932531.093</gml:lowerCorner>
          <gml:upperCorner>567266.646 5932542.123</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B09B6246-CA57-4F09-BD4E-B8BBA50AB5DF" srsName="EPSG:25832">
          <gml:posList>567228.633 5932542.123 567234.402 5932540.482 567246.058 5932537.166 
567266.646 5932531.093 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_dfeaa284-4868-44f7-974f-97299d76ddee">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567022.311 5932504.418</gml:lowerCorner>
          <gml:upperCorner>567064.59 5932562.788</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(B)</xplan:gliederung1>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_767dc8ec-28f4-4d57-af1d-6b3315f04cfa" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a72903a0-15f6-4030-942b-19dc3a143f64" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:refTextInhalt xlink:href="#GML_e0ac5d8e-1150-46ca-925d-a76a5d887d7a" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_5DE19E92-461A-4CF6-8488-6500FC517AF2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567063.962 5932511.063 567064.59 5932553.064 567024.554 5932562.788 
567022.311 5932553.53 567035.432 5932550.351 567028.03 5932519.796 
567044.066 5932515.912 567042.536 5932509.594 567063.862 5932504.418 
567063.962 5932511.063 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_767dc8ec-28f4-4d57-af1d-6b3315f04cfa">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567047.852 5932534.187</gml:lowerCorner>
          <gml:upperCorner>567047.852 5932534.187</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_dfeaa284-4868-44f7-974f-97299d76ddee" />
      <xplan:schriftinhalt>(B)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_8746D527-21E5-4F80-8E90-5CCC10C3E5A5" srsName="EPSG:25832">
          <gml:pos>567047.852 5932534.187</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a72903a0-15f6-4030-942b-19dc3a143f64">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567047.852 5932528.681</gml:lowerCorner>
          <gml:upperCorner>567047.852 5932528.681</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_dfeaa284-4868-44f7-974f-97299d76ddee" />
      <xplan:position>
        <gml:Point gml:id="Gml_059EF0A2-5E27-4EEE-81BC-D26DA40BF578" srsName="EPSG:25832">
          <gml:pos>567047.852 5932528.681</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_556d7c40-ac69-4bae-b34f-6ab23be12a2b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566890.642 5932614.97</gml:lowerCorner>
          <gml:upperCorner>566933.72 5932623.629</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_61219A3C-CBE3-4056-855D-E2ADD3C2D8C9" srsName="EPSG:25832">
          <gml:posList>566890.642 5932623.629 566896.078 5932622.538 566933.294 5932615.059 
566933.72 5932614.97 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_11f87cab-00d2-44fd-a395-fd85697f0a5c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567154.466 5932483.944</gml:lowerCorner>
          <gml:upperCorner>567168.882 5932519.99</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2d033d54-2280-4672-afa8-a5cdf8913760" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F22232CB-5B9B-4229-B510-1CFF0FC325CA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567168.882 5932516.457 567155.485 5932519.99 567154.466 5932487.498 
567167.871 5932483.944 567168.882 5932516.457 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2d033d54-2280-4672-afa8-a5cdf8913760">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567161.412 5932493.531</gml:lowerCorner>
          <gml:upperCorner>567161.412 5932493.531</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_11f87cab-00d2-44fd-a395-fd85697f0a5c" />
      <xplan:position>
        <gml:Point gml:id="Gml_EA3CEF82-5F15-400D-B543-2381EE51C2DF" srsName="EPSG:25832">
          <gml:pos>567161.412 5932493.531</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_052c069c-32a5-4947-8507-1f7e5f40a2c1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567290.18 5932512.741</gml:lowerCorner>
          <gml:upperCorner>567327.348 5932524.153</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_180068E3-F066-4676-934A-4C6ADCE4F6BE" srsName="EPSG:25832">
          <gml:posList>567290.18 5932524.153 567307.026 5932519.135 567317.091 5932515.97 
567327.348 5932512.741 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_522f81a6-9a91-4704-a4dc-5f05c0a930b1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567057.194 5932581.109</gml:lowerCorner>
          <gml:upperCorner>567098.432 5932614.725</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a3b95b37-45b0-4631-897f-6294dacb9d75" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BB409969-4695-43EF-9627-20DD0C614992" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567098.432 5932609.719 567077.009 5932614.725 567072.687 5932596.839 
567060.181 5932599.84 567057.194 5932587.175 567069.847 5932584.143 
567082.082 5932581.215 567082.499 5932581.109 567086.711 5932599.108 
567095.476 5932597.059 567098.432 5932609.719 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a3b95b37-45b0-4631-897f-6294dacb9d75">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567077.816 5932597.94</gml:lowerCorner>
          <gml:upperCorner>567077.816 5932597.94</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_522f81a6-9a91-4704-a4dc-5f05c0a930b1" />
      <xplan:position>
        <gml:Point gml:id="Gml_F4A1CF45-21A1-4D51-B105-D1E3C5AE1BD5" srsName="EPSG:25832">
          <gml:pos>567077.816 5932597.94</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_38dc31c5-78d7-42e0-b325-fdedbfc5ed14">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567347.609 5932402.805</gml:lowerCorner>
          <gml:upperCorner>567405.845 5932486.223</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_E08BD9B9-A790-4E1B-8F1B-FFAE5D8AAF8B" srsName="EPSG:25832">
          <gml:posList>567372.369 5932409.173 567391.862 5932402.805 567395.742 5932414.68 
567389.085 5932416.857 567395.164 5932435.902 567405.845 5932468.606 
567351.805 5932486.223 567347.609 5932473.392 567389.003 5932460.021 
567372.369 5932409.173 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_0a533c49-f4e8-4f1a-b8b0-cc954d8bde37">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567388.892 5932519.427</gml:lowerCorner>
          <gml:upperCorner>567408.599 5932526.154</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_C47B1C41-8DD5-40CA-9A26-3DA561FC4834" srsName="EPSG:25832">
          <gml:posList>567408.599 5932519.427 567388.892 5932526.154 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_1278d94f-1194-4230-a3dd-5315055202ab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567256.489 5932454.695</gml:lowerCorner>
          <gml:upperCorner>567281.469 5932497.912</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f2852870-c3e6-45f2-b0a8-665b88b9fb01" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2FCE0E11-D523-47DD-BA4E-85449773C4CE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567281.469 5932493.942 567268.567 5932497.912 567256.489 5932458.666 
567269.392 5932454.695 567281.469 5932493.942 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f2852870-c3e6-45f2-b0a8-665b88b9fb01">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567268.979 5932476.304</gml:lowerCorner>
          <gml:upperCorner>567268.979 5932476.304</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1278d94f-1194-4230-a3dd-5315055202ab" />
      <xplan:position>
        <gml:Point gml:id="Gml_209B08A9-4379-4AB0-8801-8F10001DB274" srsName="EPSG:25832">
          <gml:pos>567268.979 5932476.304</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_51f8d1ed-542d-41ed-a4b0-88434b66da57">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567256.489 5932454.695</gml:lowerCorner>
          <gml:upperCorner>567322.235 5932510.66</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_DD9992B6-3BFF-4272-815F-7677B135B606" srsName="EPSG:25832">
          <gml:posList>567321.222 5932481.709 567322.235 5932495.485 567272.489 5932510.66 
567256.489 5932458.666 567269.392 5932454.695 567281.47 5932493.943 
567321.222 5932481.709 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_8caf70af-ac6b-4558-a754-536b1dd66585">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567331.025 5932415.537</gml:lowerCorner>
          <gml:upperCorner>567356.773 5932434.575</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(C)</xplan:gliederung1>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">28.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a7e35054-373c-4d0d-996d-f47a0eb7021f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_10de8bdc-8d40-4111-aee9-c1460fd1d912" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0D8301C9-1715-45A1-B4CF-BB5276A40BC6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567356.773 5932427.425 567347.744 5932430.378 567334.913 5932434.575 
567331.025 5932422.687 567352.885 5932415.537 567356.773 5932427.425 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a7e35054-373c-4d0d-996d-f47a0eb7021f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567343.899 5932421.844</gml:lowerCorner>
          <gml:upperCorner>567343.899 5932421.844</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_8caf70af-ac6b-4558-a754-536b1dd66585" />
      <xplan:position>
        <gml:Point gml:id="Gml_A3E2E136-9DC6-4DBA-BD58-A2D83E7BCFE2" srsName="EPSG:25832">
          <gml:pos>567343.899 5932421.844</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_10de8bdc-8d40-4111-aee9-c1460fd1d912">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567343.899 5932427.352</gml:lowerCorner>
          <gml:upperCorner>567343.899 5932427.352</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_8caf70af-ac6b-4558-a754-536b1dd66585" />
      <xplan:schriftinhalt>(C)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_0E4EB062-E0C6-485E-A268-C81570110398" srsName="EPSG:25832">
          <gml:pos>567343.899 5932427.352</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6f04f5a4-dbc1-4152-8185-67076a911937">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566911.678 5932632.285</gml:lowerCorner>
          <gml:upperCorner>566932.436 5932650.751</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_85BD9969-2BE7-4181-8D4E-153AD643A85F" srsName="EPSG:25832">
          <gml:posList>566914.841 5932650.751 566911.678 5932636.088 566929.272 5932632.285 
566932.436 5932646.955 566914.841 5932650.751 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_03bccf70-637e-44c5-9da1-4e3c8b83c935">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567043.498 5932617.918</gml:lowerCorner>
          <gml:upperCorner>567092.229 5932685.775</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:refTextInhalt xlink:href="#GML_fa848c8c-32b7-4173-adc0-9cfccebcaccc" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E20019B7-D32F-43B9-87E5-B4847DD08F15" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567073.95 5932665.728 567064.206 5932667.843 567067.203 5932681.504 
567047.666 5932685.775 567043.498 5932666.214 567051.206 5932664.471 
567048.635 5932652.403 567064.056 5932649.028 567057.489 5932619.287 
567063.346 5932617.918 567068.417 5932640.301 567087.955 5932636.027 
567092.229 5932655.565 567072.669 5932659.843 567073.95 5932665.728 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567059.837 5932620.008 567066.517 5932650.463 567069.211 5932662.649 
567065.328 5932663.497 567065.743 5932665.457 567069.638 5932664.601 
567071.569 5932664.201 567070.272 5932658.352 567061.78 5932619.581 
567059.837 5932620.008 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenFlaeche gml:id="GML_bd1095eb-12ac-4a26-a009-4f6b0753e65a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566896.117 5932670.417</gml:lowerCorner>
          <gml:upperCorner>566914.297 5932691.355</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7fcbdd4f-b89d-4201-b7dd-21fb6aca7680" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_F9B484DB-5738-4F8E-92F2-B5ED779F4D24" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566914.297 5932688.317 566913.795 5932688.409 566911.847 5932688.765 
566897.605 5932691.355 566896.117 5932683.966 566911.169 5932670.417 
566914.297 5932688.317 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>3700</xplan:zweckbestimmung>
      <xplan:zweckbestimmung>3000</xplan:zweckbestimmung>
    </xplan:BP_GemeinschaftsanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7fcbdd4f-b89d-4201-b7dd-21fb6aca7680">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566906.787 5932683.086</gml:lowerCorner>
          <gml:upperCorner>566906.787 5932683.086</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_22.02.01</xplan:stylesheetId>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bd1095eb-12ac-4a26-a009-4f6b0753e65a" />
      <xplan:position>
        <gml:Point gml:id="Gml_BB040DE2-2AA6-4FAF-958C-812F289A20BD" srsName="EPSG:25832">
          <gml:pos>566906.787 5932683.086</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_704500d4-3b21-4afd-8c14-818105bf8100">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567197.054 5932562.989</gml:lowerCorner>
          <gml:upperCorner>567214.587 5932582.736</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_75B88213-D154-44E0-9F02-FEB5B452D639" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567214.587 5932579.1 567201.35 5932582.736 567197.054 5932566.651 
567210.048 5932562.989 567214.587 5932579.1 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_48f5db87-4ca0-4a53-a2ec-c5d823f03524">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566811.8098 5932535.912</gml:lowerCorner>
          <gml:upperCorner>566896.646 5932668.7374</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_CDEB30B6-F13B-46F7-B15F-96AFE3FA0FF0" srsName="EPSG:25832">
          <gml:posList>566896.646 5932535.912 566867.114 5932541.836 566811.8098 5932668.7374 
566840.982 5932663.962 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_306bdad4-1abf-448b-b567-51490cdc6693">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567063.768 5932495.039</gml:lowerCorner>
          <gml:upperCorner>567077.38 5932511.063</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_393e377b-d231-4590-ae50-e1a01a1b9b2d" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_70E491E3-EBE6-44D4-A23D-AE91A7BD4137" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567077.38 5932507.792 567063.962 5932511.063 567063.768 5932498.267 
567077.223 5932495.039 567077.38 5932507.792 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_393e377b-d231-4590-ae50-e1a01a1b9b2d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567070.581 5932503.04</gml:lowerCorner>
          <gml:upperCorner>567070.581 5932503.04</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_306bdad4-1abf-448b-b567-51490cdc6693" />
      <xplan:position>
        <gml:Point gml:id="Gml_8B4E0824-2CA1-4E39-9047-F755776A8606" srsName="EPSG:25832">
          <gml:pos>567070.581 5932503.04</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_f24da74a-29a0-4481-b8a2-6553140ee5d6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566935.009 5932537.873</gml:lowerCorner>
          <gml:upperCorner>566957.002 5932580.804</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_762cf87b-beaa-4055-b88f-ed7cf784a1b4" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4B945297-3BF6-43A9-B0A1-590A632D6D8A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566957.002 5932577.915 566943.701 5932580.804 566935.009 5932540.774 
566948.187 5932537.873 566957.002 5932577.915 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_762cf87b-beaa-4055-b88f-ed7cf784a1b4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566948.21 5932569.542</gml:lowerCorner>
          <gml:upperCorner>566948.21 5932569.542</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f24da74a-29a0-4481-b8a2-6553140ee5d6" />
      <xplan:position>
        <gml:Point gml:id="Gml_8191AB20-F182-48CF-A6E7-D6799A4E1FDF" srsName="EPSG:25832">
          <gml:pos>566948.21 5932569.542</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_33c4d13d-8576-4c2e-b0c8-c89d4eae622f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567281.388 5932536.623</gml:lowerCorner>
          <gml:upperCorner>567293.853 5932540.299</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_8C1CAD1C-3F1B-45D2-B0C0-EEB1AB5129B6" srsName="EPSG:25832">
          <gml:posList>567281.388 5932540.299 567293.853 5932536.623 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_886e99ed-a72b-4538-bbbc-4c44886d15d8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567169.615 5932531.929</gml:lowerCorner>
          <gml:upperCorner>567199.367 5932540.094</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4BE4BC80-88CE-48B8-A432-0FE3D88AC943" srsName="EPSG:25832">
          <gml:posList>567199.367 5932531.929 567188.552 5932534.897 567169.615 5932540.094 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_8150acbc-7b2f-4e36-a9f4-215a2c150717">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566892.027 5932532.083</gml:lowerCorner>
          <gml:upperCorner>566926.177 5932590.731</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_AA436CB2-9777-4BDE-8B70-C3064025F570" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566926.177 5932584.044 566892.841 5932590.731 566892.027 5932586.672 
566915.778 5932532.083 566926.177 5932584.044 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_ec0258cd-9e32-4387-a212-c90af044273e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567368.034 5932402.799</gml:lowerCorner>
          <gml:upperCorner>567405.845 5932474.131</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_177a6d7b-fd34-4539-a553-13fca56cd965" />
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_457fffb9-dcce-4d2f-bd8d-1e043dfae1b0" />
      <xplan:refTextInhalt xlink:href="#GML_1c0ece7b-0f1e-4303-8d5b-867ada375758" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_e0ac5d8e-1150-46ca-925d-a76a5d887d7a" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2177487C-4D81-41BD-8383-0DC01C3FFDC5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567395.742 5932414.68 567389.085 5932416.857 567395.164 5932435.902 
567405.845 5932468.606 567388.898 5932474.131 567368.034 5932410.583 
567391.856 5932402.799 567395.742 5932414.68 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.9</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_5e0eacc3-a69f-4d4f-819b-ccbcf300d2ea">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567068.417 5932636.027</gml:lowerCorner>
          <gml:upperCorner>567092.229 5932659.843</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_5702E395-450A-48A5-B9F1-80C2CA1CD881" srsName="EPSG:25832">
          <gml:posList>567087.955 5932636.027 567092.229 5932655.565 567072.669 5932659.843 
567068.417 5932640.301 567087.955 5932636.027 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_502e2af3-8181-4c32-85c7-57f935467898">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567379.74 5932534.313</gml:lowerCorner>
          <gml:upperCorner>567379.74 5932534.313</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.65</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_FBB6010B-DDC8-4DB4-A607-E80CBD610E8A" srsName="EPSG:25832">
          <gml:pos>567379.74 5932534.313</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_bd564468-9997-4a9e-ba50-0fff9e54a681">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566840.984 5932622.495</gml:lowerCorner>
          <gml:upperCorner>566902.608 5932663.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_177a6d7b-fd34-4539-a553-13fca56cd965" />
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_457fffb9-dcce-4d2f-bd8d-1e043dfae1b0" />
      <xplan:refTextInhalt xlink:href="#GML_1c0ece7b-0f1e-4303-8d5b-867ada375758" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_e0ac5d8e-1150-46ca-925d-a76a5d887d7a" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4CA820BB-2664-48A9-A620-F823B47FD55C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566902.608 5932652.887 566896.697 5932653.919 566840.984 5932663.959 
566855.11 5932630.759 566896.291 5932622.495 566902.608 5932652.887 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.8</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_7571dac1-40fe-48ad-9209-ab766d49ddcf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566978.267 5932633.303</gml:lowerCorner>
          <gml:upperCorner>566995.428 5932637.067</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_1C28AE5D-2A5F-4DAF-BF11-F0D7781D7AA6" srsName="EPSG:25832">
          <gml:posList>566995.428 5932633.303 566983.267 5932635.989 566978.267 5932637.067 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_9de634ff-26a0-4084-9be7-0449f6363b12">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567322.235 5932486.223</gml:lowerCorner>
          <gml:upperCorner>567351.805 5932495.485</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_D5215459-6595-4E44-A3DE-F4FD093B6EA9" srsName="EPSG:25832">
          <gml:posList>567351.805 5932486.223 567340.244 5932489.992 567322.235 5932495.485 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_e937af61-4451-494a-abe3-7b7ead18ef7b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567261.942 5932522.565</gml:lowerCorner>
          <gml:upperCorner>567261.942 5932522.565</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_8111C6D0-722A-42C0-A5E5-B4FA3C1A91FC" srsName="EPSG:25832">
          <gml:pos>567261.942 5932522.565</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d747be85-d5bd-4917-ab4d-dc9962b7d166">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567268.567 5932481.709</gml:lowerCorner>
          <gml:upperCorner>567322.235 5932510.66</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_59a6f60b-ad27-45c1-80ab-9362c3fdd4e0" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A3ADED76-3168-4587-8150-64BADFF4484D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567322.235 5932495.485 567272.489 5932510.66 567268.567 5932497.912 
567281.469 5932493.942 567321.222 5932481.709 567322.235 5932495.485 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_59a6f60b-ad27-45c1-80ab-9362c3fdd4e0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567314.916 5932490.686</gml:lowerCorner>
          <gml:upperCorner>567314.916 5932490.686</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d747be85-d5bd-4917-ab4d-dc9962b7d166" />
      <xplan:position>
        <gml:Point gml:id="Gml_7D2F85A5-828C-42FB-9A8D-3546C76ADAAA" srsName="EPSG:25832">
          <gml:pos>567314.916 5932490.686</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_ba4d251c-cdea-4335-96fa-fcae168c11bd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567063.768 5932495.039</gml:lowerCorner>
          <gml:upperCorner>567077.223 5932498.267</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_8B8CF286-C5CE-43D2-B899-C5245CC8CE3C" srsName="EPSG:25832">
          <gml:posList>567077.223 5932495.039 567063.768 5932498.267 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
      <xplan:typ>2000</xplan:typ>
      <xplan:breite uom="m">2.5</xplan:breite>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c02f90ac-b876-4a51-a8bf-a700b663b384">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566984.022 5932512.784</gml:lowerCorner>
          <gml:upperCorner>567006.571 5932529.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(C)</xplan:gliederung1>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">28.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a1b0fc92-aa10-477e-a9ae-0ee6eb0d8ade" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_ce0398e9-44b8-4e23-8b03-034a7cc8fe96" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_EF240582-A225-4766-BD65-D0136BCA7D7B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567006.571 5932524.992 566997.526 5932526.982 566984.022 5932529.955 
566984.047 5932517.15 567003.884 5932512.784 567006.571 5932524.992 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_a1b0fc92-aa10-477e-a9ae-0ee6eb0d8ade">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566994.705 5932523.499</gml:lowerCorner>
          <gml:upperCorner>566994.705 5932523.499</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c02f90ac-b876-4a51-a8bf-a700b663b384" />
      <xplan:schriftinhalt>(C)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_EE9DD041-F2B2-4EA8-B05D-81BAA914D8F4" srsName="EPSG:25832">
          <gml:pos>566994.705 5932523.499</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ce0398e9-44b8-4e23-8b03-034a7cc8fe96">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566994.705 5932517.991</gml:lowerCorner>
          <gml:upperCorner>566994.705 5932517.991</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c02f90ac-b876-4a51-a8bf-a700b663b384" />
      <xplan:position>
        <gml:Point gml:id="Gml_905EF337-9C1D-47FB-B3E0-290D2A945C48" srsName="EPSG:25832">
          <gml:pos>566994.705 5932517.991</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinbedarfsFlaeche gml:id="GML_82527acb-ece6-4125-928e-653005bf4aeb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567123.821 5932540.482</gml:lowerCorner>
          <gml:upperCorner>567242.691 5932598.151</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>KITA</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_3e7ae2f8-4b5d-4a31-aced-656804df7c3b" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_AFEF9696-B832-4FB3-B08A-BF71209C15F2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567150.386 5932592.949 567131.074 5932598.151 567123.821 5932570.617 
567153.295 5932563.252 567182.586 5932555.002 567193.299 5932552.125 
567211.911 5932546.88 567234.402 5932540.482 567242.691 5932571.366 
567177.348 5932589.33 567176.472 5932586.1 567150.386 5932592.949 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GF uom="m2">9000</xplan:GF>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:zweckbestimmung>1200</xplan:zweckbestimmung>
      <xplan:zugunstenVon>(FHH)</xplan:zugunstenVon>
    </xplan:BP_GemeinbedarfsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e69087d6-f3f5-4230-aee6-cc5c0c3020fb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567043.491 5932661.987</gml:lowerCorner>
          <gml:upperCorner>567067.203 5932685.775</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7ae5a119-fcf3-40c3-a792-11f11a22b23d" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B37611B2-2988-4E97-B2CC-5837E49A82BB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567067.203 5932681.504 567047.666 5932685.775 567043.491 5932666.216 
567062.928 5932661.987 567067.203 5932681.504 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:Z>12</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7ae5a119-fcf3-40c3-a792-11f11a22b23d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567055.325 5932673.872</gml:lowerCorner>
          <gml:upperCorner>567055.325 5932673.872</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e69087d6-f3f5-4230-aee6-cc5c0c3020fb" />
      <xplan:position>
        <gml:Point gml:id="Gml_E2EA6E98-0789-410E-A943-6558290B3424" srsName="EPSG:25832">
          <gml:pos>567055.325 5932673.872</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_cda7b3a7-baa3-4e38-8a21-3076e52574c5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567022.311 5932553.53</gml:lowerCorner>
          <gml:upperCorner>567024.554 5932562.788</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_7BFC9906-D6E2-4B62-8490-F735FD3DBA5E" srsName="EPSG:25832">
          <gml:posList>567024.554 5932562.788 567022.311 5932553.53 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_d3e7ad65-e26a-4945-87fe-afcf1a71932d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566811.81 5932431.736</gml:lowerCorner>
          <gml:upperCorner>567423.122 5932668.737</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_631847A0-6FD0-455F-8FEC-6F4E1344086E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566933.72 5932614.97 566896.291 5932622.495 566855.11 5932630.759 
566840.984 5932663.959 566839.738 5932664.165 566837.401 5932664.548 
566815.846 5932668.067 566813.49 5932668.458 566811.81 5932668.737 
566867.114 5932541.836 566896.659 5932535.91 566863.92 5932610.866 
566934.976 5932596.435 566997.267 5932582.876 567016.031 5932578.566 
567027.572 5932575.915 567078.123 5932563.724 567119.194 5932553.393 
567169.475 5932540.133 567187.874 5932535.083 567199.367 5932531.929 
567254.356 5932516.198 567272.257 5932510.731 567340.244 5932489.992 
567388.898 5932474.131 567405.845 5932468.606 567395.164 5932435.902 
567406.511 5932431.736 567410.998 5932444.716 567423.122 5932481.404 
567411.583 5932485.536 567382.662 5932494.965 567356.713 5932503.467 
567327.155 5932512.806 567307.026 5932519.135 567289.775 5932524.274 
567246.058 5932537.166 567234.402 5932540.482 567211.911 5932546.88 
567193.299 5932552.125 567182.586 5932555.002 567153.295 5932563.252 
567123.821 5932570.617 567082.499 5932581.109 567057.194 5932587.175 
567026.383 5932594.554 567000.817 5932600.354 566976.793 5932605.682 
566958.735 5932609.686 566933.72 5932614.97 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_47617446-63fc-44dd-918b-57464b545e6b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567154.466 5932483.944</gml:lowerCorner>
          <gml:upperCorner>567168.882 5932519.99</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_ADF914C9-03E9-46EF-915A-AF2CF325E8B5" srsName="EPSG:25832">
          <gml:posList>567154.466 5932487.498 567167.871 5932483.944 567168.882 5932516.457 
567155.485 5932519.99 567154.466 5932487.498 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_3d818c65-b0b2-4263-b716-6f6841b6dd5d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567014.909 5932519.796</gml:lowerCorner>
          <gml:upperCorner>567035.432 5932553.53</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_2CEABD2D-DD8D-473E-9602-C81545266861" srsName="EPSG:25832">
          <gml:posList>567022.311 5932553.53 567014.909 5932522.975 567028.03 5932519.796 
567035.432 5932550.351 567022.311 5932553.53 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_f972e2e0-8fad-4aa7-96f6-59533220c80f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567123.821 5932542.123</gml:lowerCorner>
          <gml:upperCorner>567236.767 5932598.151</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_4AABC6BD-E108-4C74-8145-5837973333E1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567197.054 5932566.651 567186.553 5932569.468 567172.007 5932573.565 
567157.176 5932577.743 567146.984 5932580.29 567150.376 5932592.952 
567131.074 5932598.151 567123.821 5932570.617 567143.194 5932565.776 
567153.295 5932563.252 567167.94 5932559.127 567182.586 5932555.002 
567193.299 5932552.125 567211.911 5932546.88 567228.633 5932542.123 
567236.767 5932573.006 567214.588 5932579.1 567210.049 5932562.989 
567197.054 5932566.651 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>6</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_a96b7e7e-633b-43e6-8a3b-f46a8ec08afb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567131.074 5932582.736</gml:lowerCorner>
          <gml:upperCorner>567201.35 5932598.151</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_849E53EE-A028-46ED-8CDB-F46DB6DD23DB" srsName="EPSG:25832">
          <gml:posList>567201.35 5932582.736 567177.348 5932589.33 567176.472 5932586.1 
567150.386 5932592.949 567131.074 5932598.151 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_0eca3fe9-27ca-4aff-aaf1-8f94fb83c981">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566933.72 5932609.592</gml:lowerCorner>
          <gml:upperCorner>566978.267 5932642.551</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_176AF956-99E6-4137-96AD-8AF7034DE5DF" srsName="EPSG:25832">
          <gml:posList>566978.267 5932637.067 566952.851 5932642.551 566949.134 5932625.001 
566936.416 5932627.695 566933.72 5932614.97 566946.439 5932612.281 
566959.158 5932609.592 566962.888 5932627.198 566975.605 5932624.504 
566978.267 5932637.067 566978.267 5932637.067 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_9a34bae3-0bb3-4ef8-9a3a-1cbaf4d3d120">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566997.267 5932510.826</gml:lowerCorner>
          <gml:upperCorner>567027.572 5932582.876</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_84329dc1-833d-4d58-96a7-a19a8aa4ebe5" />
      <xplan:refTextInhalt xlink:href="#GML_4a229a55-87d9-41e7-a476-f3122e8477d9" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_22399F38-6859-4615-B227-A146B541EE0F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567014.909 5932522.975 567022.311 5932553.53 567024.554 5932562.788 
567027.572 5932575.915 567016.031 5932578.566 566997.267 5932582.876 
566997.44 5932569.013 566997.461 5932559.253 566997.526 5932526.982 
567006.571 5932524.992 567003.883 5932512.784 567011.967 5932510.826 
567014.909 5932522.975 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_3af1409c-0164-4674-90e5-31313da6ac6d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567382.662 5932485.536</gml:lowerCorner>
          <gml:upperCorner>567429.82 5932519.427</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_7267F444-8CEB-41FB-9EEF-448E420A8E33" srsName="EPSG:25832">
          <gml:posList>567408.599 5932519.427 567402.973 5932502.211 567386.807 5932507.495 
567382.662 5932494.965 567397.122 5932490.251 567411.583 5932485.536 
567417.094 5932502.757 567425.609 5932499.842 567429.82 5932512.141 
567408.599 5932519.427 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f7dc7da7-d3c4-4694-a611-5301ba65c890">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567304.082 5932425.176</gml:lowerCorner>
          <gml:upperCorner>567327.076 5932443.889</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_926F9FC3-B86C-42E2-A00B-D2459B347AF8" srsName="EPSG:25832">
          <gml:posList>567305.006 5932443.889 567304.082 5932431.093 567313.75 5932428.141 
567323.416 5932425.176 567327.076 5932437.136 567318.249 5932439.837 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_EinfahrtsbereichLinie gml:id="GML_3bee6318-813e-48ec-9cc4-478ddd5ebbbb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567120.482 5932539.915</gml:lowerCorner>
          <gml:upperCorner>567189.162 5932558.224</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Anschluss der Grundstücke</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B59E6AFE-38E6-4812-B65F-65F82585EF11" srsName="EPSG:25832">
          <gml:posList>567120.482 5932558.224 567189.162 5932539.915 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_EinfahrtsbereichLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_3ebad625-4912-46eb-bfc1-b41a0cdfe8de">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567102.204 5932393.229</gml:lowerCorner>
          <gml:upperCorner>567391.856 5932488.352</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E5AC209D-37C2-450D-8BB0-3CFDAC5933E2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567391.856 5932402.799 567368.034 5932410.583 567323.344 5932425.199 
567244.778 5932449.239 567173.092 5932469.658 567102.204 5932488.352 
567108.812 5932469.393 567166.561 5932452.951 567253.207 5932428.035 
567336.183 5932403.704 567369.223 5932393.229 567391.856 5932402.799 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:zugunstenVon>(FHH)</xplan:zugunstenVon>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7cce43dd-c66b-4034-846f-49bdbe0b4c48">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567105.392 5932496.996</gml:lowerCorner>
          <gml:upperCorner>567169.615 5932553.393</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_571389f3-dcd1-4512-aa86-f03228711cae" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_FED2053F-7A48-4CA4-B651-8584D52F383C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567169.615 5932540.095 567119.194 5932553.393 567105.392 5932500.439 
567118.446 5932496.996 567128.946 5932536.814 567169.185 5932526.2 
567169.615 5932540.095 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_571389f3-dcd1-4512-aa86-f03228711cae">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567124.059 5932545.024</gml:lowerCorner>
          <gml:upperCorner>567124.059 5932545.024</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7cce43dd-c66b-4034-846f-49bdbe0b4c48" />
      <xplan:position>
        <gml:Point gml:id="Gml_03BED95A-0EF8-43A6-998F-DB00068FBBF8" srsName="EPSG:25832">
          <gml:pos>567124.059 5932545.024</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_52d17b0e-ca95-4234-a548-44ee74a542bc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566994.424 5932652.215</gml:lowerCorner>
          <gml:upperCorner>567018.21 5932676.027</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b30adbd6-d570-4bde-a049-2a5ed93a4a2a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9EB19AC8-E16B-415E-99D5-B59F5CFCC525" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567018.21 5932671.753 566998.672 5932676.027 566994.424 5932656.506 
567013.937 5932652.215 567018.21 5932671.753 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:Z>8</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b30adbd6-d570-4bde-a049-2a5ed93a4a2a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567006.312 5932664.125</gml:lowerCorner>
          <gml:upperCorner>567006.312 5932664.125</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_52d17b0e-ca95-4234-a548-44ee74a542bc" />
      <xplan:position>
        <gml:Point gml:id="Gml_89A41FCE-5CC1-40CB-B1FA-4C4185AB6F56" srsName="EPSG:25832">
          <gml:pos>567006.312 5932664.125</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_9a4873b8-c3c2-4a2c-ad51-9b5efd286c8b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567057.194 5932581.109</gml:lowerCorner>
          <gml:upperCorner>567098.432 5932614.725</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B6157380-78B3-48C4-B192-7532D33CD92F" srsName="EPSG:25832">
          <gml:posList>567077.009 5932614.725 567072.687 5932596.839 567060.181 5932599.84 
567057.194 5932587.175 567069.847 5932584.143 567082.082 5932581.215 
567082.499 5932581.109 567086.711 5932599.108 567095.476 5932597.059 
567098.432 5932609.719 567077.009 5932614.725 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_b258f060-321f-4733-bc32-6089b00ca2d9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566860 5932651.347</gml:lowerCorner>
          <gml:upperCorner>566883.881 5932660.532</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_8d068ddb-d876-42a4-9414-cdb642b6695f" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F091B621-CC9A-4E2B-86D9-A26CEF537A0A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566883.881 5932656.228 566860 5932660.532 566862.364 5932655.039 
566882.908 5932651.347 566883.881 5932656.228 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
      <xplan:typ>2000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_cc6f9fd6-b3b5-4aa1-9ba4-28d96a043fc2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567057.194 5932581.109</gml:lowerCorner>
          <gml:upperCorner>567098.432 5932617.69</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_177a6d7b-fd34-4539-a553-13fca56cd965" />
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_457fffb9-dcce-4d2f-bd8d-1e043dfae1b0" />
      <xplan:refTextInhalt xlink:href="#GML_1c0ece7b-0f1e-4303-8d5b-867ada375758" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_e0ac5d8e-1150-46ca-925d-a76a5d887d7a" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_518FE950-4442-4ED9-A382-A97383865969" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567098.432 5932609.719 567064.323 5932617.69 567057.194 5932587.175 
567082.499 5932581.109 567086.71 5932599.108 567095.474 5932597.06 
567098.432 5932609.719 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.8</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f563d570-a475-4455-8a82-edfc277880f8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567168.882 5932516.457</gml:lowerCorner>
          <gml:upperCorner>567169.185 5932526.2</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_556D874C-ACC6-4F0B-90FF-27E439424BEC" srsName="EPSG:25832">
          <gml:posList>567168.882 5932516.457 567169.185 5932526.2 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="GML_3c708863-041f-49a0-a48a-9d11e8b01407">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566969.529 5932678.102</gml:lowerCorner>
          <gml:upperCorner>566993.233 5932701.953</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Umgrenzung der Grundstücke, für die eine GKF bestimmt ist</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_42D5EF5C-2B39-4090-8F3B-58FBB27C053D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566990.246 5932684.02 566993.233 5932697.68 566973.696 5932701.953 
566969.529 5932682.392 566977.178 5932680.668 566988.947 5932678.102 
566990.246 5932684.02 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_9099cb95-cbfb-4b9d-964a-04a597a05155">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567411.582 5932481.418</gml:lowerCorner>
          <gml:upperCorner>567423.1269 5932485.517</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_1C6F8569-C171-40DD-8637-22CCF0BE7143" srsName="EPSG:25832">
          <gml:posList>567411.582 5932485.517 567423.1269 5932481.418 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_05ddcd24-31e8-4ac4-b2e8-a9d241e0f68c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567002.74 5932625.558</gml:lowerCorner>
          <gml:upperCorner>567002.74 5932625.558</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_3CE4CE11-E30B-441F-ABE1-39A26EBEBDE7" srsName="EPSG:25832">
          <gml:pos>567002.74 5932625.558</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_16f20bf9-77ba-45ab-bc8c-e8a2a45e5a25">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567318.249 5932422.687</gml:lowerCorner>
          <gml:upperCorner>567351.805 5932495.485</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_6E6EE3EF-F402-40D6-BD5E-34849952E110" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567334.913 5932434.575 567344.705 5932464.514 567347.609 5932473.392 
567351.805 5932486.223 567340.244 5932489.992 567322.235 5932495.485 
567321.222 5932481.709 567320.581 5932472.132 567318.249 5932439.837 
567327.076 5932437.136 567323.416 5932425.176 567331.025 5932422.687 
567334.913 5932434.575 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_061be2ce-4f34-4381-8cd9-1172360a9c94">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567266.646 5932524.154</gml:lowerCorner>
          <gml:upperCorner>567311.757 5932558.719</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_85F2A5B9-5CFD-44A1-A886-BFBE4993A63B" srsName="EPSG:25832">
          <gml:posList>567286.823 5932558.719 567281.388 5932540.299 567270.323 5932543.562 
567266.646 5932531.093 567290.18 5932524.154 567295.554 5932542.376 
567308.023 5932538.699 567311.757 5932551.368 567286.823 5932558.719 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_3dbb7e2c-0958-456f-9eaa-9276af5962fe">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567024.554 5932495.019</gml:lowerCorner>
          <gml:upperCorner>567078.117 5932575.915</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_A80BDF44-9B21-45F3-908D-043DD313332A" srsName="EPSG:25832">
          <gml:posList>567024.554 5932562.788 567064.59 5932553.064 567063.771 5932498.277 
567077.223 5932495.019 567078.117 5932563.726 567027.572 5932575.915 
567024.554 5932562.788 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d67210c8-bfa4-469e-91f1-ec1623e9df4b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567366.058 5932511.963</gml:lowerCorner>
          <gml:upperCorner>567388.892 5932532.308</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_4E1DE5AB-E231-4527-969F-6770C53CE7AC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567388.892 5932526.154 567370.916 5932532.308 567366.058 5932518.117 
567384.033 5932511.963 567388.892 5932526.154 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_75e5c764-13f6-4c64-a5bc-7f8490335488">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567382.662 5932485.536</gml:lowerCorner>
          <gml:upperCorner>567429.82 5932519.427</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_fd7fa05c-488e-4740-9638-71b5ce4fb5d0" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_421CA27B-05A8-4148-9C0B-BC209BD61963" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567429.82 5932512.141 567408.599 5932519.427 567402.973 5932502.211 
567386.807 5932507.495 567382.662 5932494.965 567397.122 5932490.251 
567411.583 5932485.536 567417.094 5932502.757 567425.609 5932499.842 
567429.82 5932512.141 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_fd7fa05c-488e-4740-9638-71b5ce4fb5d0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567406.226 5932502.443</gml:lowerCorner>
          <gml:upperCorner>567406.226 5932502.443</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_75e5c764-13f6-4c64-a5bc-7f8490335488" />
      <xplan:position>
        <gml:Point gml:id="Gml_BCDC813B-011B-46F9-AB52-4BBA51BA091B" srsName="EPSG:25832">
          <gml:pos>567406.226 5932502.443</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="GML_0e0c5781-d873-49fc-bc62-954652ef8b6f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566918.857 5932668.685</gml:lowerCorner>
          <gml:upperCorner>566940.875 5932695.911</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Umgrenzung der Grundstücke, für die eine GKF bestimmt ist</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E42F1704-6D7D-4710-ACDD-C4C9A89A06C7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566939.119 5932679.68 566940.875 5932693.428 566927.647 5932695.911 
566922.897 5932688.255 566921.646 5932685.623 566921.132 5932683.962 
566920.64 5932681.663 566918.857 5932672.165 566937.055 5932668.685 
566939.119 5932679.68 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ffcb1923-653f-4363-8c00-21c804ae0618">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567146.756 5932579.321</gml:lowerCorner>
          <gml:upperCorner>567150.376 5932592.952</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_F42AEF10-554B-4246-B494-D299759ED993" srsName="EPSG:25832">
          <gml:posList>567146.756 5932579.321 567150.376 5932592.952 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_654479c2-0386-4f57-b8a4-7958a3e0ba99">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567181.142 5932459.143</gml:lowerCorner>
          <gml:upperCorner>567213.423 5932479.389</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_27AC79D5-B24D-444A-ABF9-A31998A67E07" srsName="EPSG:25832">
          <gml:posList>567184.571 5932479.389 567181.142 5932467.365 567209.998 5932459.143 
567213.423 5932471.17 567197.523 5932475.704 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenFlaeche gml:id="GML_3634a213-3a14-4e9e-acca-399a652e59e8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567048.529 5932648.931</gml:lowerCorner>
          <gml:upperCorner>567066.758 5932664.471</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b1f51a46-920d-431c-8259-b744430fd8e4" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_BBF0316B-307A-4514-90BB-1F3971643DCD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567066.758 5932661.079 567062.928 5932661.987 567051.206 5932664.471 
567048.529 5932652.35 567064.165 5932648.931 567066.758 5932661.079 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>3700</xplan:zweckbestimmung>
      <xplan:zweckbestimmung>3000</xplan:zweckbestimmung>
    </xplan:BP_GemeinschaftsanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b1f51a46-920d-431c-8259-b744430fd8e4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567057.67 5932656.725</gml:lowerCorner>
          <gml:upperCorner>567057.67 5932656.725</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_22.02.01</xplan:stylesheetId>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3634a213-3a14-4e9e-acca-399a652e59e8" />
      <xplan:position>
        <gml:Point gml:id="Gml_78065FED-D59D-4F42-9844-014C232F92F2" srsName="EPSG:25832">
          <gml:pos>567057.67 5932656.725</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauLinie gml:id="GML_ff9f2c78-f492-4ce2-b2c6-6b65309a212b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567131.074 5932592.944</gml:lowerCorner>
          <gml:upperCorner>567151.282 5932601.442</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_34F04AB7-01C1-4C80-AF5F-1F95D4BDD8B1" srsName="EPSG:25832">
          <gml:posList>567150.384 5932592.944 567151.282 5932596.362 567131.936 5932601.442 
567131.074 5932598.151 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_3951ee4b-cd21-4031-af9c-0c6d2980857c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566896.291 5932614.97</gml:lowerCorner>
          <gml:upperCorner>566940.257 5932652.887</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:refTextInhalt xlink:href="#GML_fa848c8c-32b7-4173-adc0-9cfccebcaccc" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_32D0F064-EBD8-4EC4-995A-3DA2C1E01797" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566940.257 5932645.268 566914.841 5932650.751 566913.856 5932650.923 
566907.946 5932651.955 566902.608 5932652.887 566896.291 5932622.495 
566933.72 5932614.97 566940.257 5932645.268 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.5</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_6aae2f4b-add8-44b0-9d14-1afd8dbc37ca">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567260.353 5932541.041</gml:lowerCorner>
          <gml:upperCorner>567392.8406 5932638.529</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_fcd08368-5940-432c-989e-1f81afe63ff7" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_d94706b1-832a-4940-a5dc-f2cc91471e9b" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_6226FD68-DD5B-40FA-BBCF-C938956EBBA1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5C9143B6-498F-48B5-939A-E8F7CE1DFDAC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567312.888 5932613.722 567316.461 5932624.449 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_6610B4C2-A2DD-4BDC-B6AF-EE543A859991" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567316.461 5932624.449 567316.385915319 5932626.03519127 567315.262 5932627.157 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4571A909-8AF0-4C05-A5F7-F9BA4F8D97A0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567315.262 5932627.157 567289.481 5932638.529 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2811BBBA-0744-47E6-BDC7-72525BEE4E3C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567289.481 5932638.529 567260.353 5932579.038 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0CB70CC7-54B1-4EE2-BB67-C83FBE583E1C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567260.353 5932579.038 567265.123 5932577.757 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_001B44A7-6EC4-45F0-A341-D50656C8F9B8" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567265.123 5932577.757 567323.35754651 5932559.90214335 567381.274 5932541.041 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D0C3CB64-0927-4BFE-A9A4-CA172B7B2A22" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567381.274 5932541.041 567380.373 5932549.468 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DBA02B5D-6C3E-45AB-B2B8-4F2770A6B16A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567380.373 5932549.468 567388.12 5932549.474 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_7F55F41B-3DA4-4DFF-9F77-CDFCF86AF7D0" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567388.12 5932549.474 567391.949168068 5932551.43723327 567392.599 5932555.691 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C5A93A34-0CD4-4D9B-BBB7-262537A6AD9B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567392.599 5932555.691 567380.296 5932592.441 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_617D16EF-FF4D-4D63-8C2B-8DCD851FEAB1" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567380.296 5932592.441 567377.133845179 5932596.69210919 567372.068 5932598.244 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_95BA2201-2698-482B-93A8-7A7DE114761D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567372.068 5932598.244 567354.756 5932597.904 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5E5C4605-CBA2-4037-B33D-C0B8244B0B54" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567354.756 5932597.904 567350.262 5932607.073 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5CECBCF3-6E2E-414E-9F4A-53B0778E724A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567350.262 5932607.073 567348.886 5932609.879 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_880EF681-605C-4082-B843-31864626C2CC" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567348.886 5932609.879 567348.190716628 5932610.57013671 567347.217 5932610.684 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_53E3E675-BCEC-49DF-8247-7B0EA88E352B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567347.217 5932610.684 567333.787 5932607.732 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_985113D0-3721-4DE0-81BB-FB69A6FA92DE" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567333.787 5932607.732 567332.94704748 5932607.217058 567332.608 5932606.292 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_57CB0820-EAFA-49FA-A92F-B49E806AB61D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567332.608 5932606.292 567332.567 5932603.854 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CB8F13A7-CB88-4229-9C01-D1ECC5A4FD9E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567332.567 5932603.854 567312.888 5932613.722 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:zugunstenVon>(FHH)</xplan:zugunstenVon>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_fcd08368-5940-432c-989e-1f81afe63ff7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567313.899 5932580.045</gml:lowerCorner>
          <gml:upperCorner>567313.899 5932580.045</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_18.01</xplan:stylesheetId>
      <xplan:art>zugunstenVon</xplan:art>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6aae2f4b-add8-44b0-9d14-1afd8dbc37ca" />
      <xplan:position>
        <gml:Point gml:id="Gml_797E15AD-327F-4792-88C0-082D41E13E51" srsName="EPSG:25832">
          <gml:pos>567313.899 5932580.045</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_d94706b1-832a-4940-a5dc-f2cc91471e9b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567303.395 5932583.826</gml:lowerCorner>
          <gml:upperCorner>567303.395 5932583.826</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6aae2f4b-add8-44b0-9d14-1afd8dbc37ca" />
      <xplan:schriftinhalt>Parkanlage</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_48E728F1-1159-4F2E-BD46-F0DEC0E8394C" srsName="EPSG:25832">
          <gml:pos>567303.395 5932583.826</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_fed9d064-95da-46e1-88d2-ece285db3954">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566969.527 5932678.103</gml:lowerCorner>
          <gml:upperCorner>566993.233 5932701.953</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>WA</xplan:gliederung1>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f3f6e29f-8615-45af-811a-0ec5af006e54" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_6a184ea3-b80c-421d-af52-a7ce3b9e4c55" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_AA467B02-BD51-4D20-86D3-E5C1C23297F8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566990.246 5932684.02 566993.233 5932697.68 566973.696 5932701.953 
566969.527 5932682.393 566977.178 5932680.668 566988.946 5932678.103 
566990.246 5932684.02 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:Z>12</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_f3f6e29f-8615-45af-811a-0ec5af006e54">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566981.352 5932691.769</gml:lowerCorner>
          <gml:upperCorner>566981.352 5932691.769</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_fed9d064-95da-46e1-88d2-ece285db3954" />
      <xplan:schriftinhalt>WA</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_70211AF4-B9A0-44DB-A459-272504EFE050" srsName="EPSG:25832">
          <gml:pos>566981.352 5932691.769</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_6a184ea3-b80c-421d-af52-a7ce3b9e4c55">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566981.352 5932686.805</gml:lowerCorner>
          <gml:upperCorner>566981.352 5932686.805</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_fed9d064-95da-46e1-88d2-ece285db3954" />
      <xplan:position>
        <gml:Point gml:id="Gml_784D6936-0AF7-4D9A-B5EF-6EA45F32E939" srsName="EPSG:25832">
          <gml:pos>566981.352 5932686.805</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_10fbc2bb-c1b4-4462-baf0-001d7c12a5e5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567266.647 5932524.154</gml:lowerCorner>
          <gml:upperCorner>567293.857 5932543.562</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a922b98b-a3da-44e7-bf2b-b6a033b1e361" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F1E06470-31DF-4F40-AB2D-DE8F60985946" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567293.857 5932536.622 567281.388 5932540.299 567270.323 5932543.562 
567266.647 5932531.095 567289.775 5932524.274 567290.18 5932524.154 
567293.857 5932536.622 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a922b98b-a3da-44e7-bf2b-b6a033b1e361">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567280.252 5932533.858</gml:lowerCorner>
          <gml:upperCorner>567280.252 5932533.858</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_10fbc2bb-c1b4-4462-baf0-001d7c12a5e5" />
      <xplan:position>
        <gml:Point gml:id="Gml_C5BFDE0B-2A73-40BC-9E30-CBE046948D56" srsName="EPSG:25832">
          <gml:pos>567280.252 5932533.858</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_d00e8001-0ba9-48d1-9bd6-db5b33de5aad">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567123.821 5932542.123</gml:lowerCorner>
          <gml:upperCorner>567236.767 5932598.151</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_1AC29A66-4E9B-4B79-A20F-0048BAAE19A6" srsName="EPSG:25832">
          <gml:posList>567131.074 5932598.151 567123.821 5932570.617 567143.194 5932565.776 
567153.295 5932563.252 567167.94 5932559.127 567182.586 5932555.002 
567193.299 5932552.125 567211.911 5932546.88 567228.633 5932542.123 
567236.767 5932573.006 567214.587 5932579.1 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_9562ca70-343d-482c-861b-03477e9b1b1b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566840.984 5932655.039</gml:lowerCorner>
          <gml:upperCorner>566862.364 5932663.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">4.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_9D4A37F2-CB9B-4645-B9B5-5DF78CB28636" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566859.999 5932660.533 566840.984 5932663.959 566843.323 5932658.461 
566862.364 5932655.039 566859.999 5932660.533 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="GML_bc1f68e2-8fa2-437f-b773-dd73ea36b568">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566867.114 5932374.896</gml:lowerCorner>
          <gml:upperCorner>567406.511 5932541.836</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_ad52165d-f9b8-428b-9ab2-d312f49e819b" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3D1BDA28-C8B3-4D6A-9847-B0D078009E83" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_12AD76E4-EEDC-47B3-A81E-E2D7DF8A64D5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567021.644 5932492.095 566930.353 5932512.193 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F0B216FA-5C6E-4073-8F96-8069E0D2C5EA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566930.353 5932512.193 566905.056 5932516.581 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CB8C5FAE-099B-448C-810C-3BBF21D4C804" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566905.056 5932516.581 566896.659 5932535.91 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F522BB24-BEE3-43A4-BACE-922C7EC9B728" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566896.659 5932535.91 566867.114 5932541.836 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_69C572B1-D32B-410F-944A-0A8B98B544EA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566867.114 5932541.836 566881.191 5932509.534 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_3A98E06F-410D-44C4-A17D-0C2EFA14BEC5" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566881.191 5932509.534 566904.618968746 5932505.069006 566928.036 5932500.547 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E6C9BA99-A4F8-41E0-A3AA-BDC6393E2B43" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566928.036 5932500.547 566930.476 5932500.073 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B5F25222-DA19-4BCC-AE8E-52BDC3578A3B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566930.476 5932500.073 566964.214 5932493.074 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_4B473A3B-49C4-421C-9D7C-A052ABED2AD6" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566964.214 5932493.074 567036.828398072 5932476.62190217 567108.797 5932457.542 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B03BBAB0-6D5B-46A0-8FA9-DF518A18202E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567108.797 5932457.542 567118.766 5932454.705 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9541C33C-3C38-461B-B890-9E9D0AC12D79" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567118.766 5932454.705 567163.242 5932442.044 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1D07608D-D75E-409C-9125-26DB0CE5CE50" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567163.242 5932442.044 567249.81 5932417.15 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7B184E99-10F2-48B1-B8AA-2B6A2474A5CB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567249.81 5932417.15 567332.731 5932392.835 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_CD33EACE-FE38-4765-A10D-0371F817DE72" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567332.731 5932392.835 567360.457157475 5932384.26594882 567387.923 5932374.896 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3212A657-4DD0-4647-A941-E22136C3B0F7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567387.923 5932374.896 567396.661 5932403.241 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3E76B309-27E1-4D6F-8A38-E22985C36F07" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567396.661 5932403.241 567406.511 5932431.736 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E44702BA-7424-47F4-8784-40E831870378" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567406.511 5932431.736 567395.164 5932435.902 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_156254B8-A0D7-497D-8243-F54D952EAD8C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567395.164 5932435.902 567389.085 5932416.857 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_83E241EE-A6E1-4A97-8BF7-EE8313DCA228" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567389.085 5932416.857 567395.742 5932414.68 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0F88F47B-CDFC-4B94-9235-5A78EF357B76" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567395.742 5932414.68 567391.856 5932402.799 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3D1BAF5E-D4C8-4CE2-8685-637F3F1C83C7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567391.856 5932402.799 567369.223 5932393.229 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C58156D2-650E-48DA-B688-DF345353C625" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567369.223 5932393.229 567336.183 5932403.704 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_55C81A5A-230E-43BE-9AB0-717FF5F0AC4F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567336.183 5932403.704 567253.207 5932428.035 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5C1DEA2C-892D-43AA-883B-7287441656A2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567253.207 5932428.035 567166.561 5932452.951 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_96F8B4CA-E3CF-4490-82AA-AB00FA3D1772" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567166.561 5932452.951 567108.812 5932469.393 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4F6C3B3C-1B26-49B6-8E90-B4B5A355FFBD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567108.812 5932469.393 567102.204 5932488.352 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D9C54100-A766-4713-B631-AAEDDC75B487" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567102.204 5932488.352 567086.102 5932492.649 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_913B20E4-4D99-4EC4-B579-0C8C37DF9A1D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567086.102 5932492.649 567086.106 5932490.73 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_63397F62-84A0-498B-9AF7-B89B52085027" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567086.106 5932490.73 567084.347 5932490.73 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_73EEC547-6B37-4B4B-AE55-D51C7E71201E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567084.347 5932490.73 567063.184 5932495.848 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_32C9DEF7-4D58-4AF2-9E25-AEDD9DCF6EBC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567063.184 5932495.848 567053.76 5932484.125 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4EA58DDC-B21D-4AC3-8CA2-2EDFEB8DFE06" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567053.76 5932484.125 567021.644 5932492.095 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1300</xplan:zweckbestimmung>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_a3c6c5b8-6ef8-469e-bf82-1477dd4460a6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566882.908 5932649.044</gml:lowerCorner>
          <gml:upperCorner>566896.697 5932656.228</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">4.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_FE8CDC43-050E-4394-9EAE-5AD0CCA5F0CA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566896.697 5932653.919 566883.881 5932656.228 566882.908 5932651.348 
566895.719 5932649.044 566896.697 5932653.919 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_af2bf57b-a7cf-4fe5-a7c3-e6d60dd17e42">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567228.633 5932531.093</gml:lowerCorner>
          <gml:upperCorner>567266.646 5932542.123</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_670AA8B2-28A0-4FD8-9A45-3EC5751EA73B" srsName="EPSG:25832">
          <gml:posList>567228.633 5932542.123 567234.402 5932540.482 567246.058 5932537.166 
567266.646 5932531.093 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_256aae4d-0181-4d4e-b08e-5f53f12607e2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567305.006 5932439.837</gml:lowerCorner>
          <gml:upperCorner>567320.581 5932476.184</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_19adf723-36ed-450b-b941-884a9ff7c295" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4DFC9DEE-35EC-4521-8945-BE441D11D278" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567320.581 5932472.132 567307.339 5932476.184 567305.006 5932443.889 
567318.249 5932439.837 567320.581 5932472.132 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_19adf723-36ed-450b-b941-884a9ff7c295">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567312.794 5932458.01</gml:lowerCorner>
          <gml:upperCorner>567312.794 5932458.01</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_256aae4d-0181-4d4e-b08e-5f53f12607e2" />
      <xplan:position>
        <gml:Point gml:id="Gml_F5446A73-182A-4A42-9C7C-5184E34AEB07" srsName="EPSG:25832">
          <gml:pos>567312.794 5932458.01</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="GML_cc59be5e-e899-47c3-a03d-9b11e90f1eaf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566893.63 5932688.763</gml:lowerCorner>
          <gml:upperCorner>566914.1151 5932716.539</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Umgrenzung der Grundstücke, für die eine GKF bestimmt ist</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_0A2C505A-88DE-4F0E-8589-A6473CF54A0E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_D857EF58-FC0D-4FC5-8586-ACE526EA3BC2" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566913.644 5932698.444 566914.08697411 5932701.36156212 566913.991 5932704.311 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5637AE22-B8AC-4502-AD15-1C2895982755" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566913.991 5932704.311 566913.512 5932706.751 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A4C4E0EA-DD45-4BD5-868A-B121F7152E7C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566913.512 5932706.751 566912.072 5932714.074 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5548DF91-CF90-4C3B-B76D-CEE03F4E3E23" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566912.072 5932714.074 566898.942 5932716.539 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E404A313-EF8D-4BE7-BBF1-DCC9D086AA19" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566898.942 5932716.539 566895.79 5932703.165 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9EDA66B6-A555-470D-8C09-44AC6BFAE9F6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566895.79 5932703.165 566893.63 5932692.099 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2C7B7E32-6D1C-4AC0-897D-04B2296923ED" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566893.63 5932692.099 566897.605 5932691.355 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CE9FE613-3CDD-4968-83D0-8B25C2CB9848" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566897.605 5932691.355 566911.846 5932688.763 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B6B05160-C7F8-4CAE-A401-FC7C697EAC70" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566911.846 5932688.763 566913.644 5932698.444 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_3f3c4844-e2c6-41af-917a-ed7b1691c2ba">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567026.37 5932594.574</gml:lowerCorner>
          <gml:upperCorner>567033.314 5932624.936</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_19ED2E82-4D20-45DE-94C6-9D672DBD12BA" srsName="EPSG:25832">
          <gml:posList>567026.37 5932594.574 567033.314 5932624.936 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_dc732d53-1d3f-4904-bc76-4b72e45dc32f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567320.88 5932503.403</gml:lowerCorner>
          <gml:upperCorner>567360.906 5932547.274</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_011EC0A3-ED38-4A87-95BD-79F29706D869" srsName="EPSG:25832">
          <gml:posList>567324.846 5932547.274 567320.88 5932535.003 567333.25 5932531.005 
567327.348 5932512.741 567356.908 5932503.403 567360.906 5932515.773 
567343.778 5932521.309 567349.566 5932539.285 567324.846 5932547.274 
567324.846 5932547.274 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_92cddd00-0dd1-41fa-91f2-187f32f25a34">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566920.596 5932512.784</gml:lowerCorner>
          <gml:upperCorner>567016.031 5932596.435</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:refTextInhalt xlink:href="#GML_fa848c8c-32b7-4173-adc0-9cfccebcaccc" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_10278956-BD88-40BC-B99B-8AE67B96656D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567006.571 5932524.992 567003.229 5932525.727 567016.031 5932578.566 
566997.267 5932582.876 566934.976 5932596.435 566920.596 5932531.119 
567003.883 5932512.784 567006.571 5932524.992 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_35418166-77a9-4c31-a4de-a09e9c734be5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566863.92 5932584.788</gml:lowerCorner>
          <gml:upperCorner>566879.912 5932610.867</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_8198A2B5-C5C8-4224-92A8-DE8537A6B79D" srsName="EPSG:25832">
          <gml:posList>566879.912 5932607.63 566863.92 5932610.867 566875.309 5932584.788 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e0f8f64a-eadb-4339-a3e7-01b46f8237f9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567372.37 5932402.805</gml:lowerCorner>
          <gml:upperCorner>567395.742 5932421.054</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>MI</xplan:gliederung1>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_160705bc-f465-4c54-821c-18dcca3c6ece" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_06237df3-80ed-4071-9a6f-e73fd91115ba" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C999376F-EB87-410C-AF94-8AAE15325702" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567395.742 5932414.68 567376.256 5932421.054 567372.37 5932409.174 
567391.862 5932402.805 567395.742 5932414.68 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>0.9</xplan:GRZ>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_160705bc-f465-4c54-821c-18dcca3c6ece">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567384.057 5932413.679</gml:lowerCorner>
          <gml:upperCorner>567384.057 5932413.679</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e0f8f64a-eadb-4339-a3e7-01b46f8237f9" />
      <xplan:schriftinhalt>MI</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_C9BD245D-A0B2-4526-A31C-95F4C254A41E" srsName="EPSG:25832">
          <gml:pos>567384.057 5932413.679</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_06237df3-80ed-4071-9a6f-e73fd91115ba">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567384.057 5932408.717</gml:lowerCorner>
          <gml:upperCorner>567384.057 5932408.717</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e0f8f64a-eadb-4339-a3e7-01b46f8237f9" />
      <xplan:position>
        <gml:Point gml:id="Gml_2B6FB964-D456-4920-91A1-3D756D7915D6" srsName="EPSG:25832">
          <gml:pos>567384.057 5932408.717</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_17953607-0953-4f66-bd48-6a53ce01e292">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567154.076 5932469.653</gml:lowerCorner>
          <gml:upperCorner>567176.287 5932487.498</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(C)</xplan:gliederung1>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">28.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_754ec6d4-2bcc-46e3-a967-c3f69f9c0513" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9028834a-eff2-470a-8a46-1a6335c2f17c" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BE821155-B306-4AC1-BE41-4DE23BDA8F1B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567176.287 5932481.712 567167.871 5932483.944 567154.466 5932487.498 
567154.263 5932480.828 567154.076 5932474.673 567173.091 5932469.653 
567176.287 5932481.712 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_754ec6d4-2bcc-46e3-a967-c3f69f9c0513">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567164.56 5932480.66</gml:lowerCorner>
          <gml:upperCorner>567164.56 5932480.66</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_17953607-0953-4f66-bd48-6a53ce01e292" />
      <xplan:schriftinhalt>(C)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_2F8F1023-D8F5-47BF-814A-C9593F638231" srsName="EPSG:25832">
          <gml:pos>567164.56 5932480.66</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9028834a-eff2-470a-8a46-1a6335c2f17c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567164.56 5932475.153</gml:lowerCorner>
          <gml:upperCorner>567164.56 5932475.153</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_17953607-0953-4f66-bd48-6a53ce01e292" />
      <xplan:position>
        <gml:Point gml:id="Gml_1FB137C1-8062-49E2-984D-0C4A55D723B3" srsName="EPSG:25832">
          <gml:pos>567164.56 5932475.153</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_76b0d072-3b69-44cd-a786-d6b5b41eafb7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566932.315 5932522.089</gml:lowerCorner>
          <gml:upperCorner>566964.301 5932540.774</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(C)</xplan:gliederung1>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">28.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_04859a48-ae10-4338-ae75-b96d25a1e2e8" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_61b126da-95ce-4cf0-bb45-5616e479ccc8" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7CD99FE8-CBC7-4875-BF3F-1D93AEA507CB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566964.301 5932534.297 566948.187 5932537.873 566935.009 5932540.774 
566932.315 5932528.539 566961.614 5932522.089 566964.301 5932534.297 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_04859a48-ae10-4338-ae75-b96d25a1e2e8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566945.644 5932528.807</gml:lowerCorner>
          <gml:upperCorner>566945.644 5932528.807</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_76b0d072-3b69-44cd-a786-d6b5b41eafb7" />
      <xplan:position>
        <gml:Point gml:id="Gml_6EBC870F-C231-484F-900C-EAFB3D20B802" srsName="EPSG:25832">
          <gml:pos>566945.644 5932528.807</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_61b126da-95ce-4cf0-bb45-5616e479ccc8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566945.644 5932534.314</gml:lowerCorner>
          <gml:upperCorner>566945.644 5932534.314</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_76b0d072-3b69-44cd-a786-d6b5b41eafb7" />
      <xplan:schriftinhalt>(C)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_4B11BBD4-A877-4B46-BA3B-6DA7D7D232F8" srsName="EPSG:25832">
          <gml:pos>566945.644 5932534.314</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_de32ebfe-bfba-4528-bbdf-9f7aea8fbeb9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566859.999 5932639.11</gml:lowerCorner>
          <gml:upperCorner>566883.881 5932660.533</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_87760C71-9AE8-43CA-B4CE-079D7C7E3FB8" srsName="EPSG:25832">
          <gml:posList>566883.881 5932656.228 566880.468 5932639.11 566868.159 5932641.579 
566859.999 5932660.533 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_0687e417-1b79-4c14-943f-459624c03587">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566918.873 5932668.685</gml:lowerCorner>
          <gml:upperCorner>566940.875 5932695.911</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>WA</xplan:gliederung1>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f4857370-0598-43ea-9e1a-0afa5f537872" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_88c1e7d6-27f5-4135-9cc9-1067f6ff035d" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_EE4BD52B-D6C4-460E-B596-E5BE91CEAA6A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566939.119 5932679.68 566939.828 5932685.147 566940.875 5932693.428 
566927.647 5932695.911 566925.294 5932692.055 566922.919 5932688.228 
566922.294 5932686.911 566921.668 5932685.595 566921.297 5932684.396 
566918.873 5932672.098 566937.055 5932668.685 566939.119 5932679.68 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:Z>8</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f4857370-0598-43ea-9e1a-0afa5f537872">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566929.951 5932677.75</gml:lowerCorner>
          <gml:upperCorner>566929.951 5932677.75</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_0687e417-1b79-4c14-943f-459624c03587" />
      <xplan:position>
        <gml:Point gml:id="Gml_13568D01-0DF9-458C-A785-0E827E351F04" srsName="EPSG:25832">
          <gml:pos>566929.951 5932677.75</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_88c1e7d6-27f5-4135-9cc9-1067f6ff035d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566929.951 5932682.714</gml:lowerCorner>
          <gml:upperCorner>566929.951 5932682.714</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_0687e417-1b79-4c14-943f-459624c03587" />
      <xplan:schriftinhalt>WA</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_E273DAC6-6EA9-4A5F-B76D-41E702DE79D2" srsName="EPSG:25832">
          <gml:pos>566929.951 5932682.714</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_4c4fdf72-1206-4c34-b7c0-cf97907f253c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567195.705 5932515.233</gml:lowerCorner>
          <gml:upperCorner>567208.814 5932518.924</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_71EC8CDF-870B-4FC1-BA1B-E7232B5E2CA1" srsName="EPSG:25832">
          <gml:posList>567195.705 5932518.924 567208.814 5932515.233 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_4df4f7e4-04dd-4580-b8f1-cab0f1858b3d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567063.962 5932507.79</gml:lowerCorner>
          <gml:upperCorner>567077.39 5932511.063</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_FAEC6CE1-F2AF-4CD1-8B53-A5106C02BB6F" srsName="EPSG:25832">
          <gml:posList>567063.962 5932511.063 567077.39 5932507.79 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_6d6a209d-c823-4b23-b12e-c691a5bb5c87">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567311.757 5932547.274</gml:lowerCorner>
          <gml:upperCorner>567324.846 5932551.368</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_9E8C9315-6F02-45B9-8646-7CCCBCA5642E" srsName="EPSG:25832">
          <gml:posList>567324.846 5932547.274 567316.551 5932549.955 567311.757 5932551.368 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_22d965e6-a303-4e7c-99e5-145f18df1655">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567260.353 5932579.038</gml:lowerCorner>
          <gml:upperCorner>567289.4 5932638.596</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_1B96DBE2-9C54-4F61-A234-6CB7F15C8241" srsName="EPSG:25832">
          <gml:posList>567260.353 5932579.038 567289.4 5932638.596 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_512fe6ac-e468-4869-9bf4-48fd7155458a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567172.93 5932410.583</gml:lowerCorner>
          <gml:upperCorner>567388.898 5932535.083</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:refTextInhalt xlink:href="#GML_fa848c8c-32b7-4173-adc0-9cfccebcaccc" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_92532FA9-3242-4751-941E-CCE1F1EFEB69" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567388.898 5932474.131 567340.244 5932489.992 567272.257 5932510.731 
567254.356 5932516.198 567199.367 5932531.929 567187.874 5932535.083 
567172.93 5932482.603 567176.287 5932481.712 567173.092 5932469.658 
567244.778 5932449.239 567323.344 5932425.199 567368.034 5932410.583 
567388.898 5932474.131 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_b9ec29d9-624d-49ed-9fe9-e6bf45228de9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567195.66 5932503.289</gml:lowerCorner>
          <gml:upperCorner>567254.356 5932531.929</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f68c3ec9-9e6e-4e91-ab85-904719cd5c1a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_552E0F85-C47C-4162-9957-D1AE2DB3CE70" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567254.356 5932516.198 567199.367 5932531.929 567195.66 5932518.766 
567250.615 5932503.289 567254.356 5932516.198 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f68c3ec9-9e6e-4e91-ab85-904719cd5c1a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567224.989 5932517.591</gml:lowerCorner>
          <gml:upperCorner>567224.989 5932517.591</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_b9ec29d9-624d-49ed-9fe9-e6bf45228de9" />
      <xplan:position>
        <gml:Point gml:id="Gml_E6081EFD-BE0A-48A7-861C-F586816C9B0A" srsName="EPSG:25832">
          <gml:pos>567224.989 5932517.591</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_941d9c5c-a90d-4404-8d49-07364d3428e5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566875.309 5932532.088</gml:lowerCorner>
          <gml:upperCorner>566928.911 5932607.63</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4D6A7C4C-1C68-4526-B818-4162F36878FB" srsName="EPSG:25832">
          <gml:posList>566879.912 5932607.63 566875.309 5932584.788 566896.66 5932535.909 
566915.755 5932532.088 566892.027 5932586.672 566892.841 5932590.731 
566926.177 5932584.045 566928.911 5932597.672 566879.912 5932607.63 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_a5556eba-cc28-4ad2-9908-edab89687e51">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567184.571 5932475.704</gml:lowerCorner>
          <gml:upperCorner>567254.356 5932531.929</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4ED2B7C6-093A-4B2C-8B52-1F8F91F38A16" srsName="EPSG:25832">
          <gml:posList>567250.615 5932503.289 567254.356 5932516.198 567199.367 5932531.929 
567184.571 5932479.389 567197.523 5932475.704 567208.769 5932515.074 
567250.615 5932503.289 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_0b8b172c-afe2-43b6-bc68-5243d1848c6e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567090.442 5932578.046</gml:lowerCorner>
          <gml:upperCorner>567098.264 5932590.203</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_ECD3B551-ACB4-43E8-B841-501F2C139DB8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567098.264 5932588.781 567092.948 5932590.203 567090.442 5932579.476 
567095.756 5932578.046 567098.264 5932588.781 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>18002</xplan:zweckbestimmung>
      <xplan:zugunstenVon>(HSE)</xplan:zugunstenVon>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_aa2795da-1ed0-4dec-bf8f-ccfb65e357a7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566840.984 5932623.628</gml:lowerCorner>
          <gml:upperCorner>566896.697 5932663.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_8755DD0F-607D-4601-AF98-434DDB2F0A62" srsName="EPSG:25832">
          <gml:posList>566840.984 5932663.959 566855.11 5932630.759 566890.641 5932623.628 
566896.697 5932653.919 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_d466ba49-6dbe-42cc-b4c0-b8568a868d06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567111.875 5932618.194</gml:lowerCorner>
          <gml:upperCorner>567171.022 5932673.4501</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7EB16AD9-197C-4560-853B-3A965B1E5416" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A2687148-5A3E-4DFA-88DE-5347952882A1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567163.753 5932656.802 567163.7 5932656.869 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_AC0B8433-0658-4AF9-9E3B-38C6A18108FA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567163.7 5932656.869 567169.863 5932661.795 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FDCC83E2-EDEC-4FD1-BCD1-26189291B2DE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567169.863 5932661.795 567171.022 5932665.159 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_172FD3CA-FDEE-4B2E-9A42-B0AC7C597668" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567171.022 5932665.159 567170.264 5932672.115 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BC2F5266-8008-4B5B-987A-A786D09B9654" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567170.264 5932672.115 567162.724 5932673.375 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_A880D006-AD19-488D-B19E-534DDD3D9577" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567162.724 5932673.375 567159.940300474 5932673.1620966 567157.554 5932671.713 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2572FE3B-FB42-4877-8327-514214E722C8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567157.554 5932671.713 567121.409 5932635.782 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_0780491E-FC15-4BF8-90A7-7CD0BC4BD741" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567121.409 5932635.782 567120.26402466 5932633.22945013 567121.123 5932630.567 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_066BD188-8C13-4B6A-B42F-782387783D25" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567121.123 5932630.567 567123.474 5932627.696 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2F2E18FC-BC22-4BF3-ACC4-97F7E8EFDF86" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567123.474 5932627.696 567111.875 5932618.745 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D84F7B34-E16C-457D-9446-7258F454453C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567111.875 5932618.745 567114.182 5932618.194 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5FD68D66-AD32-4ABF-A488-C28F1A89E853" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567114.182 5932618.194 567114.558 5932619.643 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EA431AC0-88E2-4DEE-AD7C-5CE1AEE654BC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567114.558 5932619.643 567146.973 5932645.318 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EB1811F0-232A-4359-BE5A-810C2AF34D0B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567146.973 5932645.318 567148.044 5932643.966 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F31D2B8C-2CA6-4C26-B5E9-D4BFB18371F1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567148.044 5932643.966 567163.753 5932656.802 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:zugunstenVon>(FHH)</xplan:zugunstenVon>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_0dd73c4d-7915-46a0-a0d6-02422e122d6e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567281.388 5932536.622</gml:lowerCorner>
          <gml:upperCorner>567311.757 5932558.719</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_C663D7AB-5963-493F-B39D-E47AD39304BA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567311.757 5932551.368 567286.823 5932558.719 567281.388 5932540.299 
567293.857 5932536.622 567295.554 5932542.376 567308.023 5932538.699 
567311.757 5932551.368 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_d34293ec-cdbc-4e29-b0fd-14208a6fa8c5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567366.058 5932511.963</gml:lowerCorner>
          <gml:upperCorner>567388.892 5932532.308</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_F3699785-C6F0-4D8F-915D-80F89C663E13" srsName="EPSG:25832">
          <gml:posList>567370.916 5932532.308 567366.058 5932518.117 567384.033 5932511.963 
567388.892 5932526.154 567370.916 5932532.308 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_48fc4210-0482-4805-9984-f37574f2b898">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566969.529 5932634.607</gml:lowerCorner>
          <gml:upperCorner>567018.21 5932701.953</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:refTextInhalt xlink:href="#GML_fa848c8c-32b7-4173-adc0-9cfccebcaccc" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_8EDFF9C0-B9D8-4A30-9A55-F327B5C9E9E5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567000.003 5932681.878 566990.246 5932684.02 566993.233 5932697.68 
566973.696 5932701.953 566969.529 5932682.392 566977.178 5932680.668 
566974.446 5932668.562 566990.147 5932665.128 566983.786 5932635.874 
566985.68 5932635.42 566989.525 5932634.607 566994.424 5932656.506 
567013.937 5932652.215 567018.21 5932671.753 566998.672 5932676.027 
567000.003 5932681.878 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_08EB097E-9DFA-4C3E-AD39-0A786FA5BF1B" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566985.686 5932635.448 566986.96891604 5932641.59061408 566988.354 5932647.711 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_498D28FB-FC52-4458-BE09-7D947A6A9247" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566988.354 5932647.711 566992.499 5932666.662 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3CA0BD14-FD8B-479A-8094-3E1EADBD86F8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566992.499 5932666.662 566995.188 5932678.839 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3DAAD59A-7053-4FF7-B65E-BDE35DD32C48" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566995.188 5932678.839 566991.345 5932679.679 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EA96662F-0AB9-4D6E-9C81-910DBF385B73" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566991.345 5932679.679 566991.772 5932681.636 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_71E344B3-72CA-4C91-8D56-3BB3471751F3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566991.772 5932681.636 566995.618 5932680.791 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EE9033B6-460E-4B08-899B-2460B8F602C6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566995.618 5932680.791 566997.572 5932680.363 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_94638FC8-48E5-45F0-AC5B-F5E97637D411" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566997.572 5932680.363 566990.336 5932647.284 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C04A29BF-5AF7-44FE-B78C-93F1421C5AF8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566990.336 5932647.284 566987.562 5932635.04 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EC8E99BD-E490-4CEC-B782-0BD93DE9BBAA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566987.562 5932635.04 566985.686 5932635.448 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b8a7b538-8618-44c0-bf6d-7a756742e99d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566932.315 5932522.089</gml:lowerCorner>
          <gml:upperCorner>566964.301 5932540.774</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_CC32055B-05F1-4A70-A554-E704B9C285A3" srsName="EPSG:25832">
          <gml:posList>566935.009 5932540.774 566932.315 5932528.539 566961.614 5932522.089 
566964.301 5932534.297 566948.187 5932537.873 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f3b549e8-b5a4-442b-aee3-c8084614199d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567351.805 5932402.805</gml:lowerCorner>
          <gml:upperCorner>567405.845 5932486.223</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4E3ECFBC-03C0-4D9A-8128-2518895EA6BA" srsName="EPSG:25832">
          <gml:posList>567391.862 5932402.805 567395.742 5932414.68 567389.085 5932416.857 
567395.164 5932435.902 567405.845 5932468.606 567351.805 5932486.223 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_c7e1d227-4566-4be9-8c7b-0ccdbf250ca2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567102.208 5932480.702</gml:lowerCorner>
          <gml:upperCorner>567134.393 5932500.439</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4C1E7736-A45A-4E77-93B9-4A8CE09ACAE7" srsName="EPSG:25832">
          <gml:posList>567105.392 5932500.439 567102.208 5932488.351 567131.213 5932480.702 
567134.393 5932492.799 567118.446 5932496.996 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_d18ea6a3-740f-40b4-9a1f-1af3f613af71">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567131.074 5932586.103</gml:lowerCorner>
          <gml:upperCorner>567177.347 5932601.441</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">4.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_6B413912-DC6F-41AD-97FB-71E3E8F447A8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567177.347 5932589.328 567151.282 5932596.362 567131.939 5932601.441 
567131.074 5932598.154 567150.375 5932592.947 567176.473 5932586.103 
567177.347 5932589.328 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="GML_c9a274dc-7cf0-4d6c-a721-b2befc8f12d9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567016.084 5932435.4861</gml:lowerCorner>
          <gml:upperCorner>567429.5663 5932620.339</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_9C2CCEFB-AC18-47BF-8684-D42D0ED394A7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567410.998 5932444.716 567412.1864 5932448.312 567419.9705 5932471.8671 
567423.1221 5932481.404 567423.1269 5932481.418 567429.5663 5932500.9039 
567308.703 5932543.646 567311.034 5932551.617 567160.542 5932590.176 
567143.612 5932590.705 567033.544 5932620.339 567016.084 5932556.978 
567407.8074 5932435.4861 567410.998 5932444.716 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>4000</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_ae86a301-80ec-435b-86d0-6ea5a0cae013">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566949.134 5932622.292</gml:lowerCorner>
          <gml:upperCorner>566961.842 5932625.001</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_69A64929-6D98-4A20-910C-F5BE7F7D4834" srsName="EPSG:25832">
          <gml:posList>566949.134 5932625.001 566961.842 5932622.292 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_647abc65-d8a9-4cf4-b659-c25ab9da9124">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567334.913 5932430.378</gml:lowerCorner>
          <gml:upperCorner>567357.536 5932464.514</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_BF288182-DDD1-4127-AD2D-45CF1858735B" srsName="EPSG:25832">
          <gml:posList>567334.913 5932434.575 567347.744 5932430.378 567357.536 5932460.318 
567344.705 5932464.514 567334.913 5932434.575 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_39f07895-2ace-4854-9fd3-bc543823c8ef">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567024.554 5932507.792</gml:lowerCorner>
          <gml:upperCorner>567078.117 5932575.915</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_af0b767c-a6f7-4b47-8ae5-02d7bc47d8ca" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_66704EB2-292D-471F-B451-368A677CE596" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567078.117 5932563.726 567027.572 5932575.915 567024.554 5932562.788 
567064.59 5932553.064 567063.962 5932511.063 567077.38 5932507.792 
567078.117 5932563.726 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_af0b767c-a6f7-4b47-8ae5-02d7bc47d8ca">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567070.704 5932558.547</gml:lowerCorner>
          <gml:upperCorner>567070.704 5932558.547</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_39f07895-2ace-4854-9fd3-bc543823c8ef" />
      <xplan:position>
        <gml:Point gml:id="Gml_A2ADDED4-86C8-4D98-9219-EB2E4D686DC4" srsName="EPSG:25832">
          <gml:pos>567070.704 5932558.547</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_f62a886d-9d38-4be8-9441-51a490efb7c2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567228.633 5932531.093</gml:lowerCorner>
          <gml:upperCorner>567266.646 5932542.123</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_051F2384-8BFA-44C9-8A45-392A0A3EC32F" srsName="EPSG:25832">
          <gml:posList>567228.633 5932542.123 567234.402 5932540.482 567246.058 5932537.166 
567266.646 5932531.093 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_f272741a-a3f4-4f15-8953-3e9c0015320b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567222.982 5932449.219</gml:lowerCorner>
          <gml:upperCorner>567248.276 5932467.473</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>WA</xplan:gliederung1>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1cac905f-f8c9-4115-9dbd-e1c5bd0c1dbd" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BF90868C-FE8E-4676-8B30-078F9163119C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567248.276 5932461.245 567239.39 5932463.775 567226.407 5932467.473 
567222.982 5932455.446 567244.851 5932449.219 567248.276 5932461.245 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1cac905f-f8c9-4115-9dbd-e1c5bd0c1dbd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567235.629 5932455.133</gml:lowerCorner>
          <gml:upperCorner>567235.629 5932455.133</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f272741a-a3f4-4f15-8953-3e9c0015320b" />
      <xplan:position>
        <gml:Point gml:id="Gml_81C677EE-F36C-48D5-B4F7-84F92B67F6DB" srsName="EPSG:25832">
          <gml:pos>567235.629 5932455.133</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_688bfdaf-c222-4faa-8154-ce8432616ac3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567252.933 5932437.967</gml:lowerCorner>
          <gml:upperCorner>567285.279 5932458.666</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_6BAE99CD-528F-4A4D-B4BF-5D592B2383D0" srsName="EPSG:25832">
          <gml:posList>567256.489 5932458.666 567252.933 5932446.744 567281.619 5932437.967 
567285.279 5932449.925 567269.392 5932454.695 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6c68c524-3ebe-4e04-b77a-cd92c4ce514d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567154.076 5932469.653</gml:lowerCorner>
          <gml:upperCorner>567176.287 5932487.498</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_7CF23C37-BB07-458B-B461-BAA844465AAD" srsName="EPSG:25832">
          <gml:posList>567154.466 5932487.498 567154.076 5932474.673 567173.091 5932469.653 
567176.287 5932481.712 567167.871 5932483.944 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_c8b02f74-b7cf-4917-baff-52fdb3b38b94">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566840.984 5932655.039</gml:lowerCorner>
          <gml:upperCorner>566862.364 5932663.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_93BFA5E9-AFFA-47BD-B7F0-77F61137CEF8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566860 5932660.532 566840.984 5932663.959 566843.323 5932658.461 
566862.364 5932655.039 566860 5932660.532 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
      <xplan:typ>2000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_de03f542-3875-45ea-80a2-1b10a9c1b373">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566959.158 5932600.259</gml:lowerCorner>
          <gml:upperCorner>567005.237 5932627.198</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_84329dc1-833d-4d58-96a7-a19a8aa4ebe5" />
      <xplan:refTextInhalt xlink:href="#GML_4a229a55-87d9-41e7-a476-f3122e8477d9" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0CFA565A-5B0D-4331-A006-23DBA7E422D7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567005.237 5932617.823 566992.561 5932620.709 566975.605 5932624.504 
566962.888 5932627.198 566959.158 5932609.592 566976.793 5932605.682 
567001.238 5932600.259 567005.237 5932617.823 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_19ae3847-02f9-4459-b58b-4131298290e8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566893.63 5932688.694</gml:lowerCorner>
          <gml:upperCorner>566914.1151 5932716.539</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="Gml_24EBF1C4-3679-41D6-8467-05094C1A772E" srsName="EPSG:25832">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">566898.942 5932716.539 566895.79 5932703.165 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">566895.79 5932703.165 566893.63 5932692.099 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">566893.63 5932692.099 566897.605 5932691.355 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">566897.605 5932691.355 566911.833 5932688.694 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">566911.833 5932688.694 566913.644 5932698.444 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">566913.644 5932698.444 566914.08697411 5932701.36156212 566913.991 5932704.311 
</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">566913.991 5932704.311 566913.512 5932706.751 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">566913.512 5932706.751 566912.072 5932714.074 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">566912.072 5932714.074 566898.942 5932716.539 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_1d5b5d0a-2d41-40d9-9cde-6826ed0ca100">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566983.955 5932526.982</gml:lowerCorner>
          <gml:upperCorner>566997.526 5932562.226</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_40108ddd-a238-489f-9c0e-b9d9a53ec338" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7399439E-B7E7-4363-B326-B4BC16A67939" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566997.461 5932559.253 566983.955 5932562.226 566984.022 5932529.955 
566997.526 5932526.982 566997.461 5932559.253 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_40108ddd-a238-489f-9c0e-b9d9a53ec338">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566990.741 5932544.604</gml:lowerCorner>
          <gml:upperCorner>566990.741 5932544.604</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1d5b5d0a-2d41-40d9-9cde-6826ed0ca100" />
      <xplan:position>
        <gml:Point gml:id="Gml_8E5FC1C2-9470-4F93-A2D7-73E638405761" srsName="EPSG:25832">
          <gml:pos>566990.741 5932544.604</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_67369fc7-d714-4bf7-b9df-65ad2579f46a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567026.383 5932594.554</gml:lowerCorner>
          <gml:upperCorner>567033.805 5932624.821</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_78D4DF39-50DE-428A-A504-166C4ADBA70F" srsName="EPSG:25832">
          <gml:posList>567026.383 5932594.554 567033.805 5932624.821 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_455dcd6a-e725-45a4-8284-59f8cbc74875">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567042.536 5932504.418</gml:lowerCorner>
          <gml:upperCorner>567063.862 5932509.594</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_430FFD72-CA77-4F4A-822F-2FB0A08A959D" srsName="EPSG:25832">
          <gml:posList>567042.536 5932509.594 567063.862 5932504.418 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_cb81c23d-32ef-4d96-9697-a38d1f3ccd14">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567026.383 5932587.175</gml:lowerCorner>
          <gml:upperCorner>567057.194 5932594.556</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_14D7C007-1372-48CC-A045-F0425A236449" srsName="EPSG:25832">
          <gml:posList>567026.383 5932594.556 567057.194 5932587.175 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_83c9160c-48dd-4c74-9928-62dbc3c41d9c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567026.383 5932587.175</gml:lowerCorner>
          <gml:upperCorner>567064.323 5932624.821</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:refTextInhalt xlink:href="#GML_fa848c8c-32b7-4173-adc0-9cfccebcaccc" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_38325406-59FA-4D20-8BE8-80D10F4AFD91" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567064.323 5932617.69 567057.489 5932619.287 567033.805 5932624.821 
567026.383 5932594.554 567057.194 5932587.175 567064.323 5932617.69 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.5</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_6eb8a849-8f4c-429e-ba74-61cbddf8d5cc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567349.566 5932532.308</gml:lowerCorner>
          <gml:upperCorner>567370.916 5932539.285</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_1C6861EE-2C97-48AD-931B-9A355FF358CD" srsName="EPSG:25832">
          <gml:posList>567370.916 5932532.308 567366.914 5932533.678 567349.566 5932539.285 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_0072c790-0132-43c5-9d61-c7fc67792969">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567001.238 5932594.556</gml:lowerCorner>
          <gml:upperCorner>567029.478 5932612.948</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_d662ff6d-a0da-452f-af2b-525735527e98" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_59DD24A7-7848-4F13-A95D-04FBAD8421DE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567029.478 5932607.177 567016.803 5932610.062 567004.127 5932612.948 
567001.238 5932600.259 567026.383 5932594.556 567029.478 5932607.177 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d662ff6d-a0da-452f-af2b-525735527e98">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567011.457 5932604.611</gml:lowerCorner>
          <gml:upperCorner>567011.457 5932604.611</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_0072c790-0132-43c5-9d61-c7fc67792969" />
      <xplan:position>
        <gml:Point gml:id="Gml_3921E35C-93B9-4822-9071-68987027812D" srsName="EPSG:25832">
          <gml:pos>567011.457 5932604.611</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauLinie gml:id="GML_0ae17b01-dfe6-4efc-b166-ff9f2a512fd3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566840.982 5932660.533</gml:lowerCorner>
          <gml:upperCorner>566859.999 5932663.961</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_78342353-32A3-4451-B39D-AE929424F99F" srsName="EPSG:25832">
          <gml:posList>566859.999 5932660.533 566858.281 5932660.843 566853.359 5932661.73 
566848.44 5932662.617 566840.982 5932663.961 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_fe669d60-3a70-4cce-830a-a06515c9caa8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567004.127 5932610.062</gml:lowerCorner>
          <gml:upperCorner>567016.803 5932612.948</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_42EF758A-972B-44BD-9DDF-1EAFCE07386C" srsName="EPSG:25832">
          <gml:posList>567016.803 5932610.062 567004.127 5932612.948 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_260bcdc5-a5d0-4a5f-aa6c-76a1165f860a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566933.72 5932594.554</gml:lowerCorner>
          <gml:upperCorner>567033.805 5932645.268</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2f4da185-59ea-4615-96f3-7ead1214baf8" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:refTextInhalt xlink:href="#GML_fa848c8c-32b7-4173-adc0-9cfccebcaccc" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_8972CE4E-F7AE-4FC6-AE4E-D41569C77218" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567033.805 5932624.821 567033.314 5932624.936 566989.525 5932634.607 
566985.68 5932635.42 566983.786 5932635.874 566983.267 5932635.989 
566940.257 5932645.268 566933.72 5932614.97 566958.735 5932609.686 
566976.793 5932605.682 567000.817 5932600.354 567026.383 5932594.554 
567033.805 5932624.821 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.7</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2f4da185-59ea-4615-96f3-7ead1214baf8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566986.748 5932615.143</gml:lowerCorner>
          <gml:upperCorner>566986.748 5932615.143</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_04.01.03</xplan:stylesheetId>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>GRZ</xplan:art>
      <xplan:art>rechtscharakter</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_260bcdc5-a5d0-4a5f-aa6c-76a1165f860a" />
      <xplan:position>
        <gml:Point gml:id="Gml_53EB043A-B626-453B-98B8-43058EBF673F" srsName="EPSG:25832">
          <gml:pos>566986.748 5932615.143</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d1aead2b-00c0-4cbd-bdc5-16417f9b8847">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567347.609 5932461.405</gml:lowerCorner>
          <gml:upperCorner>567388.898 5932486.223</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f9acd0e1-08c3-4ef8-b4cf-0a6d9802ea20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_EDBA002F-0114-4689-A7A0-FDF0FCBF9B07" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567388.898 5932474.131 567351.805 5932486.223 567347.609 5932473.392 
567384.72 5932461.405 567388.898 5932474.131 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f9acd0e1-08c3-4ef8-b4cf-0a6d9802ea20">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567357.219 5932477.371</gml:lowerCorner>
          <gml:upperCorner>567357.219 5932477.371</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d1aead2b-00c0-4cbd-bdc5-16417f9b8847" />
      <xplan:position>
        <gml:Point gml:id="Gml_F7D9A7D0-598F-46D4-BE6D-AB38E647D797" srsName="EPSG:25832">
          <gml:pos>567357.219 5932477.371</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_27586df2-0bca-4554-9289-2968d521fef9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567082.502 5932570.613</gml:lowerCorner>
          <gml:upperCorner>567123.833 5932581.108</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_265D5FB2-6F1B-4AE9-B772-37D1D9FD1CD1" srsName="EPSG:25832">
          <gml:posList>567082.502 5932581.108 567123.833 5932570.613 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_1b5ec360-bc95-4972-b51c-ab6265d635fd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567382.662 5932485.536</gml:lowerCorner>
          <gml:upperCorner>567429.82 5932524.897</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_177a6d7b-fd34-4539-a553-13fca56cd965" />
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_457fffb9-dcce-4d2f-bd8d-1e043dfae1b0" />
      <xplan:refTextInhalt xlink:href="#GML_1c0ece7b-0f1e-4303-8d5b-867ada375758" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_e0ac5d8e-1150-46ca-925d-a76a5d887d7a" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A333F40E-7A36-4FEE-9D00-D641B9031065" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567429.82 5932512.141 567392.564 5932524.897 567382.662 5932494.965 
567411.583 5932485.536 567417.094 5932502.757 567425.609 5932499.842 
567429.82 5932512.141 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.8</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_1d5bcaf3-35bd-4301-b05c-42a472e25700">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567293.195 5932437.849</gml:lowerCorner>
          <gml:upperCorner>567293.195 5932437.849</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">9.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_2912557F-8DD1-4472-8230-F2B8D4645C32" srsName="EPSG:25832">
          <gml:pos>567293.195 5932437.849</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_9dfcc4fc-111d-4c72-a6a8-85d3dc431505">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567068.417 5932636.027</gml:lowerCorner>
          <gml:upperCorner>567092.229 5932659.843</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5c7b2ca0-ef48-49ed-8e00-937f731529a1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_79AC4C91-BA94-45CB-9D17-6BE7875C62D3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567092.229 5932655.565 567072.669 5932659.843 567068.417 5932640.301 
567087.955 5932636.027 567092.229 5932655.565 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:Z>8</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5c7b2ca0-ef48-49ed-8e00-937f731529a1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567080.318 5932647.934</gml:lowerCorner>
          <gml:upperCorner>567080.318 5932647.934</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9dfcc4fc-111d-4c72-a6a8-85d3dc431505" />
      <xplan:position>
        <gml:Point gml:id="Gml_CD80EBE6-FBAE-4C72-BCE8-B416ECE740EE" srsName="EPSG:25832">
          <gml:pos>567080.318 5932647.934</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_8bf2aed9-1e5a-45c3-a2e3-7d6d8eb80466">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567197.054 5932566.651</gml:lowerCorner>
          <gml:upperCorner>567214.588 5932582.736</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_15EFB484-F0E9-444C-A745-64CADFB27B8B" srsName="EPSG:25832">
          <gml:posList>567214.588 5932579.1 567201.35 5932582.736 567197.054 5932566.651 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f5d3e793-af9f-4768-bdde-d1a8b99ed490">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567226.407 5932463.775</gml:lowerCorner>
          <gml:upperCorner>567248.051 5932497.884</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_74A487D3-7B3B-45C7-B153-7CAA9C1069C9" srsName="EPSG:25832">
          <gml:posList>567235.067 5932497.884 567226.407 5932467.473 567239.39 5932463.775 
567248.051 5932494.187 567235.067 5932497.884 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_de453833-d822-471a-88f8-b6803bfb6eed">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567134.114 5932611.231</gml:lowerCorner>
          <gml:upperCorner>567158.185 5932635.196</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_69941EB1-37EE-4C34-B7B8-6502B7DADAEA" srsName="EPSG:25832">
          <gml:posList>567147.458 5932635.196 567134.114 5932623.874 567144.841 5932611.231 
567158.185 5932622.553 567147.458 5932635.196 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_be28ff05-2595-448e-bcb4-0302d0bd4b2a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566893.63 5932650.923</gml:lowerCorner>
          <gml:upperCorner>566940.875 5932716.539</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:refTextInhalt xlink:href="#GML_fa848c8c-32b7-4173-adc0-9cfccebcaccc" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_882A3DEC-325C-4651-A1E9-4F54060F2C01" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2F8AF63F-9397-4AF5-9BD4-7E20BE2884EC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566917.566 5932697.804 566913.644 5932698.444 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_030035F6-C5CE-452D-8193-23B5CAEC2DBD" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566913.644 5932698.444 566914.08697411 5932701.36156212 566913.991 5932704.311 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_59F50E90-637C-4C93-9A7F-03D3D0EA697C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566913.991 5932704.311 566913.512 5932706.751 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5A0C8747-F6DF-475F-961A-A7A5E837484D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566913.512 5932706.751 566912.072 5932714.074 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_23711F21-C321-4011-B3E0-AF96D5509D18" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566912.072 5932714.074 566898.942 5932716.539 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F03215B9-373C-477D-B233-78837604E4EA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566898.942 5932716.539 566895.79 5932703.165 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_138BDE41-996F-43DE-BC0D-9655ADCD0114" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566895.79 5932703.165 566893.63 5932692.099 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4EED8403-7C9C-4D1F-9E0C-C649E74E5B7C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566893.63 5932692.099 566897.607 5932691.367 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CDA8D923-7781-413B-B35C-02B9D31E8A4B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566897.607 5932691.367 566896.117 5932683.966 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_79E27496-80B4-4563-9F4C-221CE479B0D4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566896.117 5932683.966 566911.169 5932670.417 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_32D34D33-C22A-4BD7-A6D9-98C205DBBC39" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566911.169 5932670.417 566907.946 5932651.955 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A565A9C3-DADE-404F-B9EF-17CA90170706" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566907.946 5932651.955 566913.856 5932650.923 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_08F04D2B-7EF9-4EB4-8E53-6C466373A4F5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566913.856 5932650.923 566917.595 5932672.338 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3CF469BB-27CC-42DE-A7DD-529FC23B0F55" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566917.595 5932672.338 566918.873 5932672.098 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0363B48B-2301-47A6-92F1-8A3096F4B2E9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566918.873 5932672.098 566937.055 5932668.685 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_AE457058-F2CF-4B96-8EA8-2B0DFD354C36" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566937.055 5932668.685 566939.119 5932679.68 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_865D9C44-79F5-489B-BE1F-0FACBF0763DF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566939.119 5932679.68 566940.875 5932693.428 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8BD4C3AF-3682-4823-B474-56C10A4109C0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566940.875 5932693.428 566927.647 5932695.911 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EFD5ACBF-657C-4060-B25B-E7BCF0754103" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566927.647 5932695.911 566917.566 5932697.804 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566909.893 5932651.638 566913.303 5932671.19 566911.577 5932672.753 
566914.297 5932688.317 566913.795 5932688.409 566915.258 5932696.202 
566924.416 5932694.459 566921.304 5932689.496 566920.681 5932688.383 
566919.89 5932686.596 566919.461 5932685.366 566918.979 5932683.506 
566917.276 5932674.433 566915.998 5932674.673 566911.889 5932651.29 
566909.893 5932651.638 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:Z>8</xplan:Z>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_b1340782-fbfb-4fed-bfb3-f8b556eba0da">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566949.134 5932622.292</gml:lowerCorner>
          <gml:upperCorner>566978.267 5932642.551</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_6F72837A-CC04-4ED2-A87C-A9552AD5639C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566978.267 5932637.067 566952.851 5932642.551 566949.134 5932625.001 
566961.842 5932622.292 566962.888 5932627.198 566975.605 5932624.504 
566978.267 5932637.067 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_01063915-d781-4171-9a47-f1181a0a95ba">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567032.469 5932495.637</gml:lowerCorner>
          <gml:upperCorner>567032.469 5932495.637</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">6</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_3547AACE-42BC-458C-8430-3AFFC834E6F5" srsName="EPSG:25832">
          <gml:pos>567032.469 5932495.637</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_3261c6e0-f590-4376-9e31-7519f4941009">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567356.908 5932494.965</gml:lowerCorner>
          <gml:upperCorner>567382.662 5932503.403</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_755F01CB-43F0-48C9-BC4B-39F08154468F" srsName="EPSG:25832">
          <gml:posList>567356.908 5932503.403 567382.662 5932494.965 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e356cd30-3c8a-48d8-8b67-2bcbdf689a10">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567134.114 5932611.231</gml:lowerCorner>
          <gml:upperCorner>567158.185 5932635.196</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_6E89BDD8-4CDE-4FAD-A78C-DF3F988C3DDC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567158.185 5932622.553 567147.458 5932635.196 567134.114 5932623.874 
567144.841 5932611.231 567158.185 5932622.553 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GR uom="m2">290</xplan:GR>
      <xplan:Z>2</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_52094ae1-c883-4ec2-9993-09c1d4878af0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566959.158 5932600.259</gml:lowerCorner>
          <gml:upperCorner>567001.238 5932609.592</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_038FCBA5-5046-4271-84AD-335BA7336825" srsName="EPSG:25832">
          <gml:posList>566959.158 5932609.592 566976.793 5932605.682 567000.817 5932600.354 
567001.238 5932600.259 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_8a3bb7d0-a795-4702-96ad-590ecf76dda6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567105.392 5932496.996</gml:lowerCorner>
          <gml:upperCorner>567169.615 5932553.393</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_A46B6F9E-5DE7-41FC-A9CD-1C8241427680" srsName="EPSG:25832">
          <gml:posList>567169.185 5932526.2 567169.615 5932540.095 567119.194 5932553.393 
567105.392 5932500.439 567118.446 5932496.996 567128.946 5932536.814 
567169.185 5932526.2 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_01fa555e-90d4-42a0-88bc-f42d913273b1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567254.355 5932510.66</gml:lowerCorner>
          <gml:upperCorner>567272.489 5932516.197</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_3E64EA5F-58A0-4666-A9DF-6923A8F674BC" srsName="EPSG:25832">
          <gml:posList>567272.489 5932510.66 567263.213 5932513.492 567254.355 5932516.197 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_fbb6a595-befb-47c4-b114-c89c09c2985b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567226.407 5932463.775</gml:lowerCorner>
          <gml:upperCorner>567248.051 5932497.884</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>WA</xplan:gliederung1>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_d85ded2e-455c-49bf-965f-df9db566f230" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F61816A9-8843-4FFC-BBA1-4CDD28F21A98" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567248.051 5932494.187 567235.067 5932497.884 567226.407 5932467.473 
567239.39 5932463.775 567248.051 5932494.187 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>0.6</xplan:GRZ>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d85ded2e-455c-49bf-965f-df9db566f230">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567237.229 5932477.616</gml:lowerCorner>
          <gml:upperCorner>567237.229 5932477.616</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_fbb6a595-befb-47c4-b114-c89c09c2985b" />
      <xplan:position>
        <gml:Point gml:id="Gml_CF307672-0880-4E94-8042-A62CEFB49EF7" srsName="EPSG:25832">
          <gml:pos>567237.229 5932477.616</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ddf01997-bbf6-42da-81e6-f87ea884f470">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567376.256 5932416.857</gml:lowerCorner>
          <gml:upperCorner>567405.845 5932474.125</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_29d670c8-b105-44d8-b7e1-c876875ef6ee" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_96888C0D-666F-4992-BC4C-D99E18834B2A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567395.117 5932435.982 567405.845 5932468.606 567388.896 5932474.125 
567384.734 5932461.4 567389.003 5932460.021 567376.256 5932421.054 
567389.085 5932416.857 567395.117 5932435.982 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_29d670c8-b105-44d8-b7e1-c876875ef6ee">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567391.045 5932445.505</gml:lowerCorner>
          <gml:upperCorner>567391.045 5932445.505</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ddf01997-bbf6-42da-81e6-f87ea884f470" />
      <xplan:position>
        <gml:Point gml:id="Gml_AFBD230A-0116-4493-B312-F122E14AD487" srsName="EPSG:25832">
          <gml:pos>567391.045 5932445.505</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_9643427d-8ce9-4078-a2ba-2ceddd84616a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566890.641 5932614.97</gml:lowerCorner>
          <gml:upperCorner>566937.091 5932653.919</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_84329dc1-833d-4d58-96a7-a19a8aa4ebe5" />
      <xplan:refTextInhalt xlink:href="#GML_4a229a55-87d9-41e7-a476-f3122e8477d9" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_94694291-A376-46FA-AF57-C94A05F22838" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566937.091 5932630.594 566899.656 5932638.687 566902.611 5932652.886 
566896.697 5932653.919 566890.641 5932623.628 566896.291 5932622.495 
566933.294 5932615.059 566933.72 5932614.97 566937.091 5932630.594 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_a0579862-62aa-4c2e-8178-aa46e54c3a6f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567356.713 5932494.965</gml:lowerCorner>
          <gml:upperCorner>567387.855 5932519.475</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_59CAD0D9-A2B6-4673-9E98-1885E0BB13DC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567386.807 5932507.495 567387.855 5932510.661 567384.033 5932511.963 
567366.058 5932518.117 567362.103 5932519.475 567360.906 5932515.773 
567356.713 5932503.467 567382.662 5932494.965 567386.807 5932507.495 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenFlaeche gml:id="GML_6311f207-555c-4ebf-ad4f-3a93b10b84d5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566974.445 5932665.129</gml:lowerCorner>
          <gml:upperCorner>566992.73 5932680.669</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_986a57d6-df0e-4455-aea4-faf64c4e0514" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E86F3C0B-6E31-465A-BF87-DD5938E70911" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566992.73 5932677.328 566990.909 5932677.701 566988.946 5932678.103 
566977.177 5932680.669 566974.445 5932668.563 566990.145 5932665.129 
566992.73 5932677.328 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>3700</xplan:zweckbestimmung>
      <xplan:zweckbestimmung>3000</xplan:zweckbestimmung>
    </xplan:BP_GemeinschaftsanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_986a57d6-df0e-4455-aea4-faf64c4e0514">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566983.621 5932672.91</gml:lowerCorner>
          <gml:upperCorner>566983.621 5932672.91</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_22.02.01</xplan:stylesheetId>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6311f207-555c-4ebf-ad4f-3a93b10b84d5" />
      <xplan:position>
        <gml:Point gml:id="Gml_43E78591-E8BF-4ED7-B3E1-58DAC51CCAA9" srsName="EPSG:25832">
          <gml:pos>566983.621 5932672.91</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="GML_917ed001-5f60-4fd6-9ada-6f82e6bc8139">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566896.29 5932587.175</gml:lowerCorner>
          <gml:upperCorner>567064.323 5932652.886</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_237CF504-7868-4E5E-8475-5BD1EF4894A7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567064.323 5932617.69 567033.314 5932624.936 566983.267 5932635.989 
566914.841 5932650.751 566902.609 5932652.886 566896.29 5932622.491 
566933.719 5932614.965 566958.735 5932609.686 566976.793 5932605.682 
567000.817 5932600.354 567026.173 5932594.604 567057.194 5932587.175 
567064.323 5932617.69 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_6092cc1f-98fb-44a3-b388-42c684bcf36d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566863.92 5932531.119</gml:lowerCorner>
          <gml:upperCorner>566934.976 5932610.866</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_177a6d7b-fd34-4539-a553-13fca56cd965" />
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_457fffb9-dcce-4d2f-bd8d-1e043dfae1b0" />
      <xplan:refTextInhalt xlink:href="#GML_1c0ece7b-0f1e-4303-8d5b-867ada375758" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_e0ac5d8e-1150-46ca-925d-a76a5d887d7a" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_813D3EE9-40C3-4E86-8353-9F01B6B8AF7C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566934.976 5932596.435 566863.92 5932610.866 566896.659 5932535.91 
566920.596 5932531.119 566934.976 5932596.435 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.9</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_1b0823c1-7c0d-405a-8ee4-1bcb5fe76e24">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567035.744 5932604.868</gml:lowerCorner>
          <gml:upperCorner>567056.685 5932623.57</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_E106F60A-DAB3-4EDD-871B-E5D4B4950804" srsName="EPSG:25832">
          <gml:posList>567039.157 5932623.57 567035.744 5932608.964 567053.271 5932604.868 
567056.685 5932619.474 567039.157 5932623.57 567039.157 5932623.57 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauLinie gml:id="GML_b55e2425-6b11-482b-abd4-6170ffbe6625">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566883.881 5932653.908</gml:lowerCorner>
          <gml:upperCorner>566896.688 5932656.229</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_D4946439-B8E5-4567-9D26-1CC29F0CDE21" srsName="EPSG:25832">
          <gml:posList>566896.688 5932653.908 566883.881 5932656.229 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_55d1c764-ac8e-4ebd-88e2-b6586115f4c5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566943.701 5932577.915</gml:lowerCorner>
          <gml:upperCorner>566957.002 5932580.804</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_07917004-24AC-4458-92E3-2B80D97CCDBA" srsName="EPSG:25832">
          <gml:posList>566943.701 5932580.804 566957.002 5932577.915 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_2015bec0-6b08-4f2e-bf2d-3db16497baab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566853.293 5932616.66</gml:lowerCorner>
          <gml:upperCorner>566853.293 5932616.66</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">9</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_28D237BF-D9E2-4E2A-91AE-795DFBB52B8E" srsName="EPSG:25832">
          <gml:pos>566853.293 5932616.66</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_a129458c-748f-44d9-bd66-171737027e3c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567234.402 5932494.965</gml:lowerCorner>
          <gml:upperCorner>567392.564 5932571.366</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:refTextInhalt xlink:href="#GML_fa848c8c-32b7-4173-adc0-9cfccebcaccc" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6F139CF5-A588-43FE-8E29-51CE91A856C7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567392.564 5932524.897 567366.914 5932533.678 567316.551 5932549.955 
567260.753 5932566.4 567242.691 5932571.366 567234.402 5932540.482 
567246.058 5932537.166 567289.775 5932524.274 567307.026 5932519.135 
567327.155 5932512.806 567356.713 5932503.467 567382.662 5932494.965 
567392.564 5932524.897 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_9108ee83-2efb-499e-b526-8c60f20f0835">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566983.955 5932526.982</gml:lowerCorner>
          <gml:upperCorner>566997.526 5932562.226</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_C4DA58E7-7C1F-497E-9B31-CFF3AB9F22C1" srsName="EPSG:25832">
          <gml:posList>566983.955 5932562.226 566984.022 5932529.955 566997.526 5932526.982 
566997.461 5932559.253 566983.955 5932562.226 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_82885d40-e137-471e-83e0-b252925369cb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567234.402 5932531.095</gml:lowerCorner>
          <gml:upperCorner>567271.528 5932571.366</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_84329dc1-833d-4d58-96a7-a19a8aa4ebe5" />
      <xplan:refTextInhalt xlink:href="#GML_4a229a55-87d9-41e7-a476-f3122e8477d9" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C56B12CA-5806-4D84-8EBB-85A387EF8779" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567270.323 5932543.562 567271.528 5932547.659 567264.1 5932549.777 
567246.732 5932554.705 567250.54 5932569.208 567242.691 5932571.366 
567234.402 5932540.482 567266.647 5932531.095 567270.323 5932543.562 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_722a9724-bcc1-4d0b-8bd5-53523832afb0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567184.571 5932475.704</gml:lowerCorner>
          <gml:upperCorner>567208.769 5932518.766</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_40386b79-18af-4f9e-b06d-59aec6e5285f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_46ED038F-D9BC-440D-AFCE-CEBBD5DF5E04" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567208.769 5932515.074 567195.66 5932518.766 567184.571 5932479.389 
567197.523 5932475.704 567208.769 5932515.074 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_40386b79-18af-4f9e-b06d-59aec6e5285f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567199.101 5932505.914</gml:lowerCorner>
          <gml:upperCorner>567199.101 5932505.914</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_722a9724-bcc1-4d0b-8bd5-53523832afb0" />
      <xplan:position>
        <gml:Point gml:id="Gml_E22CB960-3EC0-49AF-AEEF-C3C6F4593D0E" srsName="EPSG:25832">
          <gml:pos>567199.101 5932505.914</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_975cab29-9993-470a-8c5d-d419e1fef571">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567305.006 5932439.837</gml:lowerCorner>
          <gml:upperCorner>567320.581 5932476.184</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_6DE4FCD1-2BFD-4C86-84E6-5BE7EFFF972D" srsName="EPSG:25832">
          <gml:posList>567307.339 5932476.184 567305.006 5932443.889 567318.249 5932439.837 
567320.581 5932472.132 567307.339 5932476.184 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_EinfahrtsbereichLinie gml:id="GML_56432479-b8d8-49a9-8da2-28505eec7075">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567017.193 5932568.589</gml:lowerCorner>
          <gml:upperCorner>567079.28 5932583.429</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Anschluss der Grundstücke</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_CCE702F6-6D10-44BA-ABE5-82E4D1CB6F02" srsName="EPSG:25832">
          <gml:posList>567017.193 5932583.429 567079.28 5932568.589 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_EinfahrtsbereichLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_b868966b-7f46-4e1f-a4a2-b68d25547d31">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567026.383 5932587.175</gml:lowerCorner>
          <gml:upperCorner>567060.154 5932607.177</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_2E0DA2C1-793C-4E62-A6BB-BBD187D2C190" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567060.154 5932599.846 567029.478 5932607.177 567026.383 5932594.554 
567057.194 5932587.175 567060.154 5932599.846 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_e0a7cedd-d8c8-4885-897d-cd238e8d04db">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567268.567 5932493.943</gml:lowerCorner>
          <gml:upperCorner>567281.47 5932497.914</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_44E2E2C2-2A35-4B0C-86CB-473D2148CBEA" srsName="EPSG:25832">
          <gml:posList>567281.47 5932493.943 567268.567 5932497.914 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_67b85773-6f21-4611-8012-bd1b32d2e2c1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566911.678 5932632.285</gml:lowerCorner>
          <gml:upperCorner>566932.436 5932650.751</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_A1CF7F1D-286D-4607-82AD-ABFC689EBFA9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566932.436 5932646.955 566914.841 5932650.751 566911.678 5932636.088 
566929.272 5932632.285 566932.436 5932646.955 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_61882aca-5ec0-48e6-9fdc-2f7fd0cb40ef">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567035.744 5932604.868</gml:lowerCorner>
          <gml:upperCorner>567056.685 5932623.57</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_451B0668-4FF3-4710-970F-ED84E0C94BE5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567056.685 5932619.474 567039.157 5932623.57 567035.744 5932608.964 
567053.271 5932604.868 567056.685 5932619.474 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenFlaeche gml:id="GML_8accc93d-669f-4b1a-8728-d5a075b4c479">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566896.291 5932587.176</gml:lowerCorner>
          <gml:upperCorner>567064.323 5932652.889</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_B38FE64D-0D18-4A21-8C6F-DD57E7FDF732" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567064.323 5932617.688 567033.314 5932624.936 566983.267 5932635.989 
566914.857 5932650.747 566902.609 5932652.889 566896.291 5932622.498 
566933.723 5932614.964 566958.735 5932609.686 566976.723 5932605.687 
567001.238 5932600.259 567026.383 5932594.554 567057.191 5932587.176 
567064.323 5932617.688 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>3200</xplan:zweckbestimmung>
    </xplan:BP_GemeinschaftsanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_da61ad55-4853-4292-9ff5-243cf3153ad3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566986.486 5932594.817</gml:lowerCorner>
          <gml:upperCorner>566986.486 5932594.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_8252D52C-D901-4B41-B8D9-7E21F8F22F6B" srsName="EPSG:25832">
          <gml:pos>566986.486 5932594.817</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_6a0d5315-0339-42a3-98f1-047b6ac3db00">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566882.908 5932623.628</gml:lowerCorner>
          <gml:upperCorner>566896.697 5932656.228</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_35231F41-6E91-4B21-AA9A-7D9DCFA7C0C9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566896.697 5932653.919 566883.881 5932656.228 566882.908 5932651.347 
566895.719 5932649.044 566890.641 5932623.628 566896.697 5932653.919 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
      <xplan:typ>2000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_1f50f386-aea5-4b7c-890c-60bd27bc539e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567282.222 5932422.511</gml:lowerCorner>
          <gml:upperCorner>567282.222 5932422.511</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">6</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_8EA290B6-8619-4C3C-99B1-AA2F1DAA021D" srsName="EPSG:25832">
          <gml:pos>567282.222 5932422.511</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_23c367a7-5d9f-4e6d-ba88-584b8cb82ebc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566968.203 5932522.857</gml:lowerCorner>
          <gml:upperCorner>566968.203 5932522.857</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">9.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_23E0CE90-F8FC-447A-B436-2B48D4A6F15F" srsName="EPSG:25832">
          <gml:pos>566968.203 5932522.857</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="GML_5b773436-fc36-44f9-ada1-a45f28478333">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567043.498 5932661.987</gml:lowerCorner>
          <gml:upperCorner>567067.203 5932685.775</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Umgrenzung der Grundstücke, für die eine GKF bestimmt ist</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_35186AC2-B70A-4DFB-8CAD-FC1C4E8296EA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567064.206 5932667.8 567067.203 5932681.504 567047.666 5932685.775 
567043.498 5932666.214 567051.206 5932664.471 567062.928 5932661.987 
567064.206 5932667.8 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_72b6588a-aab2-434e-bec5-ad5e33faace6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567334.913 5932430.378</gml:lowerCorner>
          <gml:upperCorner>567357.536 5932464.514</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_84e43113-9f5b-43f5-bc22-48b6cf5e8c08" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_37E05682-8D1B-4DA1-B76D-3A7C7F6B81F5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567357.536 5932460.318 567344.705 5932464.514 567334.913 5932434.575 
567347.744 5932430.378 567357.536 5932460.318 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_84e43113-9f5b-43f5-bc22-48b6cf5e8c08">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567346.225 5932447.446</gml:lowerCorner>
          <gml:upperCorner>567346.225 5932447.446</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_72b6588a-aab2-434e-bec5-ad5e33faace6" />
      <xplan:position>
        <gml:Point gml:id="Gml_BDB6897C-1CC5-4B87-8F11-6131BBD88BE6" srsName="EPSG:25832">
          <gml:pos>567346.225 5932447.446</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_bc6acd24-71c2-4121-8763-72c862f3914e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567011.967 5932503.763</gml:lowerCorner>
          <gml:upperCorner>567044.066 5932522.975</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_6A76457D-BEA5-47FC-A98D-676BEE93B957" srsName="EPSG:25832">
          <gml:posList>567014.909 5932522.975 567011.967 5932510.826 567041.123 5932503.763 
567044.066 5932515.912 567028.03 5932519.796 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="GML_7048fffd-1bdb-42e8-a6ee-d07b5907fdca">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566835.3753 5932481.404</gml:lowerCorner>
          <gml:upperCorner>567436.534 5932676.0536</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_ad52165d-f9b8-428b-9ab2-d312f49e819b" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0D6D1F16-5B88-48B1-AC20-AB20C2F7CD62" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6C1C5082-BE4D-441E-AB93-23C3A51C0339" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567111.875 5932618.745 567105.836 5932620.31 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7AF082BD-BEDF-451E-8CCA-121163FFFCF6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567105.836 5932620.31 567104.243 5932620.722 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_34396920-1624-4C91-9E2A-CF621404CCE7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567104.243 5932620.722 567060.314 5932630.959 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A3B91151-8D4C-4E8D-8471-40FCB4574B4C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567060.314 5932630.959 567049.947 5932633.375 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6988002D-FB71-4690-887E-6D71378A8836" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567049.947 5932633.375 566992.298 5932646.3 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_89A975A6-2798-4445-91B8-CE33F7AF48AF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566992.298 5932646.3 566986.402 5932647.564 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_796172EC-2FB3-4172-A564-05C34842E424" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566986.402 5932647.564 566915.858 5932661.871 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0CFA43E6-0908-40AC-B437-0A2DD688264A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566915.858 5932661.871 566909.885 5932663.065 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_978F6ECD-A2DB-4C4A-9CC4-044EECAA64A5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566909.885 5932663.065 566835.3753 5932676.0536 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_8B6AD091-C05C-4B60-B53F-30495AD52180" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566835.3753 5932676.0536 566837.404038162 5932670.0532968 566839.738 5932664.165 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3638045C-8C0F-401F-8A38-87258BC54A1A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566839.738 5932664.165 566840.984 5932663.959 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4CEFEE31-E14E-44C8-9A4C-49191A432E34" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566840.984 5932663.959 566896.697 5932653.919 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FF088790-5FD6-426E-9CFE-8060DECF0371" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566896.697 5932653.919 566902.608 5932652.887 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EC831909-1923-4855-A67E-F35DDB2BF9EF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566902.608 5932652.887 566907.946 5932651.955 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EFB29552-8F60-45C2-82D6-C8BE069D8980" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566907.946 5932651.955 566913.856 5932650.923 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0EB8FE37-BF7B-47A7-89EB-C5194B5C0E78" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566913.856 5932650.923 566914.841 5932650.751 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ACA78754-684A-43C8-8B64-395F418514E6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566914.841 5932650.751 566940.257 5932645.268 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_22075DDD-EBA0-4D2D-B1DA-A952F5D54351" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566940.257 5932645.268 566983.267 5932635.989 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_411C1CC3-A05A-4F0B-9348-84B1E50B85F0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566983.267 5932635.989 566983.786 5932635.874 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_912DA56C-541A-4342-BE58-07A25FCDAA0D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566983.786 5932635.874 566985.68 5932635.42 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_217089CC-BF17-40A5-8127-E4709CC494BD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566985.68 5932635.42 566989.525 5932634.607 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_59F99A0A-84CF-476D-BDE2-B7305A0802AC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566989.525 5932634.607 567033.314 5932624.936 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_891AAA56-31E0-4A4C-B673-D33EFBE77EFE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567033.314 5932624.936 567033.805 5932624.821 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_74A8E004-B043-48EF-9E95-486C3D982A5A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567033.805 5932624.821 567057.489 5932619.287 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_388BCE55-C541-4E48-BA95-FD463A3E1996" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567057.489 5932619.287 567064.323 5932617.69 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3BD2A0AF-AC5E-403D-BA29-F1C28C121799" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567064.323 5932617.69 567098.432 5932609.719 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F6E3FEE3-DBDC-4ABA-B6E1-0DD1237B6970" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567098.432 5932609.719 567095.474 5932597.06 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_84D8355A-B5BF-43CE-B203-3B5C3904E130" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567095.474 5932597.06 567086.71 5932599.108 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9417EFD7-5417-488B-8011-119EE2C719B0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567086.71 5932599.108 567082.499 5932581.109 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EA581E25-A2FE-489D-B776-90966A358A28" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567082.499 5932581.109 567123.821 5932570.617 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2944AD72-3F0B-43BF-B525-C18A35C2CC25" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567123.821 5932570.617 567131.074 5932598.151 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C6F821AB-EB1C-4162-9A6D-72B8EB07CCDD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567131.074 5932598.151 567150.386 5932592.949 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_34D7C11F-00DA-4B2D-BF0E-27464207FDFE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567150.386 5932592.949 567176.472 5932586.1 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_857880A2-5761-4BBF-B28E-BFE5E8315F5B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567176.472 5932586.1 567177.348 5932589.33 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_740F3FB1-89D7-4515-BE60-4A5912043336" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567177.348 5932589.33 567242.691 5932571.366 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C56A701A-325C-4DCC-9A85-AE0F978D8221" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567242.691 5932571.366 567260.753 5932566.4 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9F1211D5-2E99-4B91-9B9E-7C7A17109ADE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567260.753 5932566.4 567316.551 5932549.955 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E78980EB-C997-4E93-9DE8-CD9A353BAE8A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567316.551 5932549.955 567366.914 5932533.678 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A2FA01EA-0BDF-4E17-BB2D-1FD1633B2442" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567366.914 5932533.678 567392.564 5932524.897 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CA4C0028-FDE7-4D15-8333-5C1E7BD099A0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567392.564 5932524.897 567429.82 5932512.141 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2EE49F23-1E61-402D-AC79-077A7F647045" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567429.82 5932512.141 567425.609 5932499.842 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A2BD3AA9-7693-4ABB-B680-E8381A22830B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567425.609 5932499.842 567417.094 5932502.757 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FD7CBA19-E095-4A55-A59E-CBA6189409F3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567417.094 5932502.757 567411.583 5932485.536 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DBDE66E2-B84D-412D-B7E7-87EE82AC3163" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567411.583 5932485.536 567423.122 5932481.404 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7EEEFCE0-D2B3-45FA-9BBE-C1C96B105A50" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567423.122 5932481.404 567436.534 5932521.988 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_38657197-39D1-4D55-9822-60917B4FE643" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567436.534 5932521.988 567428.380122673 5932524.98095565 567420.197 5932527.893 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_812D6DE8-51AD-4389-9C13-9C88C157077E" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567420.197 5932527.893 567403.359597057 5932533.78913725 567386.424 5932539.397 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0DD08F41-9890-4DFA-876A-05F1FBBC0C97" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567386.424 5932539.397 567381.274 5932541.041 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_6F1452CE-2456-4BA9-8929-B88920C953F1" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567381.274 5932541.041 567323.357546507 5932559.90214334 567265.123 5932577.757 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F94AF89D-2684-492F-85C7-F88CA09E5060" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567265.123 5932577.757 567260.353 5932579.038 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E787ECE9-B9B7-4440-A03F-18A478489DB5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567260.353 5932579.038 567197.012 5932596.054 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7AE29EF7-6D74-4B5F-B699-244F47097BB8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567197.012 5932596.054 567163.622 5932605.044 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6A15E108-2E6E-47E1-9C72-DE826868EF3A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567163.622 5932605.044 567144.917 5932610.161 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E3A51CDC-5461-4E13-8423-97C0E1106AF1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567144.917 5932610.161 567131.146 5932613.757 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7B96E10B-5344-4F21-9166-2FAB673B6144" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567131.146 5932613.757 567132.995 5932620.186 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C38EC1DE-2E55-4A4F-A553-D936D7C97573" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567132.995 5932620.186 567135.572 5932622.155 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BF18E8C6-49C1-4195-8467-B37D867416B3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567135.572 5932622.155 567134.114 5932623.874 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0D928A3D-539D-429C-81EA-55A41A80081B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567134.114 5932623.874 567147.458 5932635.196 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BB19614D-CA68-4A3D-868F-A8C98BB3E029" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567147.458 5932635.196 567147.817 5932634.773 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_93DE04D7-B468-401F-8945-677C4FCB1290" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567147.817 5932634.773 567166.803 5932650.117 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8FBB66AF-8A35-4413-8C8B-E23501F6D2A9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567166.803 5932650.117 567169.234 5932648.783 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_5F3FCEE2-836A-4836-BD2F-526751670D1A" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567169.234 5932648.783 567172.079373424 5932648.96593218 567174.898 5932649.396 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_7B358AEC-E1EA-401C-A455-E7ED4FA4716B" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567174.898 5932649.396 567177.668797698 5932650.06894136 567180.37 5932650.982 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_094E4F11-002A-44B0-BC1C-BF873D9F7547" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567180.37 5932650.982 567181.433 5932651.694 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0EDD71E5-62B2-4C37-B3B6-4A648F12D7DC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567181.433 5932651.694 567183.357 5932651.903 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EFE56155-090E-4F74-A1B0-4A59E9A0F4D7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567183.357 5932651.903 567189.394 5932657.026 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5F62ED4E-5D00-45E4-A9FC-29023B1DB94F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567189.394 5932657.026 567190.105 5932661.47 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_453A45BD-87AE-4A4E-8A8B-8669A71F33E5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567190.105 5932661.47 567180.676 5932663.104 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F29505EF-3303-43DC-958E-CA0FA85CB3AE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567180.676 5932663.104 567180.463 5932663.213 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_27A4027E-9FAF-4190-B4F2-D24E24232593" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567180.463 5932663.213 567180.19 5932663.776 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ED3A095E-C3A9-448D-BBF8-47DDFBD24EDD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567180.19 5932663.776 567179.292 5932670.725 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3C48F330-56A1-46E1-A3CB-62BF020710E5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567179.292 5932670.725 567170.264 5932672.115 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7A35F146-28CC-4295-AE89-35FEB9C4C4B5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567170.264 5932672.115 567171.022 5932665.159 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_56C95999-091E-4C74-A6ED-B51A6F6A6EB0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567171.022 5932665.159 567169.863 5932661.795 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E708B3B9-1338-44AA-B1D4-B81A523FD516" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567169.863 5932661.795 567163.7 5932656.869 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_80711AE0-9DD7-4590-A4AF-846C8578A93A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567163.7 5932656.869 567163.753 5932656.802 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_76728286-05C1-44AB-99A2-B4B7D99C922F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567163.753 5932656.802 567148.044 5932643.966 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BFAB0321-E018-4C52-9092-B90401C34F8D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567148.044 5932643.966 567146.973 5932645.318 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_55702A14-E0E1-4E79-B622-6FD475A497A1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567146.973 5932645.318 567114.558 5932619.643 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1C196676-0174-4DBC-A6F8-9C9C56995A9F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567114.558 5932619.643 567114.182 5932618.194 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2942739C-DCB0-4652-864A-49065F2300E7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567114.182 5932618.194 567111.875 5932618.745 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1300</xplan:zweckbestimmung>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_0904f805-e3aa-46e8-aa0a-203dd664a292">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567181.142 5932459.143</gml:lowerCorner>
          <gml:upperCorner>567213.423 5932479.389</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(C)</xplan:gliederung1>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">28.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c4ce8c39-c148-4cd2-a900-7c301aa21a77" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1ec79299-f886-472b-981d-74f6de2caf51" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B486FC92-2F8F-4002-B0A7-DDDE31A6F192" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567213.423 5932471.17 567197.523 5932475.704 567184.571 5932479.389 
567181.142 5932467.365 567209.998 5932459.143 567213.423 5932471.17 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c4ce8c39-c148-4cd2-a900-7c301aa21a77">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567197.832 5932465.9</gml:lowerCorner>
          <gml:upperCorner>567197.832 5932465.9</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_0904f805-e3aa-46e8-aa0a-203dd664a292" />
      <xplan:position>
        <gml:Point gml:id="Gml_2F3BD1C4-DC01-477B-94E8-C18316963007" srsName="EPSG:25832">
          <gml:pos>567197.832 5932465.9</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_1ec79299-f886-472b-981d-74f6de2caf51">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567197.832 5932471.407</gml:lowerCorner>
          <gml:upperCorner>567197.832 5932471.407</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_0904f805-e3aa-46e8-aa0a-203dd664a292" />
      <xplan:schriftinhalt>(C)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_344EF8B3-A8D8-426D-B22C-2F595AF48ECC" srsName="EPSG:25832">
          <gml:pos>567197.832 5932471.407</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_d1267281-0849-43ef-92b7-7d2048a7b272">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567290.207 5932512.741</gml:lowerCorner>
          <gml:upperCorner>567333.25 5932542.376</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_84329dc1-833d-4d58-96a7-a19a8aa4ebe5" />
      <xplan:refTextInhalt xlink:href="#GML_4a229a55-87d9-41e7-a476-f3122e8477d9" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9DE2525F-13A9-42B5-A0DE-352EB661E3EC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567333.25 5932531.005 567320.88 5932535.003 567308.023 5932538.699 
567295.554 5932542.376 567293.857 5932536.622 567290.207 5932524.24 
567307.026 5932519.135 567327.348 5932512.741 567333.25 5932531.005 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_a6b9fa92-7fd5-4840-bc50-80c6d7da8dc1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567078.123 5932553.393</gml:lowerCorner>
          <gml:upperCorner>567119.194 5932563.724</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_034DE177-3FE8-4BFE-8C6F-2B000D83021F" srsName="EPSG:25832">
          <gml:posList>567119.194 5932553.393 567078.123 5932563.724 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_c07b06ba-314b-4229-b154-4d1ec37bba2f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566935.009 5932537.873</gml:lowerCorner>
          <gml:upperCorner>566997.44 5932593.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_6B65EAA4-C417-4848-AC6A-75AE86CF7A86" srsName="EPSG:25832">
          <gml:posList>566946.549 5932593.916 566935.009 5932540.774 566948.187 5932537.873 
566957.002 5932577.915 566997.44 5932569.013 566997.267 5932582.876 
566946.549 5932593.916 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_1bab3a54-f3ae-42fa-942f-c8cd774036c2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567304.082 5932425.176</gml:lowerCorner>
          <gml:upperCorner>567327.076 5932443.889</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(C)</xplan:gliederung1>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">28.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1f5bf61b-d4a1-4902-932a-d905ba332096" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e3da762a-0c16-4e0e-be61-0644b743e2ff" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D5E285F1-28F2-4A32-B1C4-36D41609300B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567327.076 5932437.136 567318.249 5932439.837 567305.006 5932443.889 
567304.082 5932431.093 567313.75 5932428.141 567323.416 5932425.176 
567327.076 5932437.136 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1f5bf61b-d4a1-4902-932a-d905ba332096">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567311.247 5932432.23</gml:lowerCorner>
          <gml:upperCorner>567311.247 5932432.23</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1bab3a54-f3ae-42fa-942f-c8cd774036c2" />
      <xplan:position>
        <gml:Point gml:id="Gml_E6BDF3B5-BD78-450A-A39D-E4B00C9323D5" srsName="EPSG:25832">
          <gml:pos>567311.247 5932432.23</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_e3da762a-0c16-4e0e-be61-0644b743e2ff">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567311.247 5932437.737</gml:lowerCorner>
          <gml:upperCorner>567311.247 5932437.737</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1bab3a54-f3ae-42fa-942f-c8cd774036c2" />
      <xplan:schriftinhalt>(C)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_A4C4E64B-5436-4D27-AE5F-F0FBF7FF5DBE" srsName="EPSG:25832">
          <gml:pos>567311.247 5932437.737</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c85011d4-394b-4b52-bae1-bf7cc8a798af">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567102.208 5932480.702</gml:lowerCorner>
          <gml:upperCorner>567134.393 5932500.439</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_425e24d9-a273-42ef-ad68-7d3d2dae2a1c" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F259BEA6-C03B-4B6F-BD77-BBA107DF0FBC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567134.393 5932492.799 567118.446 5932496.996 567105.392 5932500.439 
567102.208 5932488.351 567131.213 5932480.702 567134.393 5932492.799 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_425e24d9-a273-42ef-ad68-7d3d2dae2a1c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567126.602 5932488.384</gml:lowerCorner>
          <gml:upperCorner>567126.602 5932488.384</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c85011d4-394b-4b52-bae1-bf7cc8a798af" />
      <xplan:position>
        <gml:Point gml:id="Gml_43108554-B288-4876-B14A-F12764163853" srsName="EPSG:25832">
          <gml:pos>567126.602 5932488.384</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ac2d27bf-e328-4302-b98b-343127e9b6f6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566969.527 5932678.103</gml:lowerCorner>
          <gml:upperCorner>566993.233 5932701.953</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_8EFC8890-3621-40E9-AD18-4AD48C4EE224" srsName="EPSG:25832">
          <gml:posList>566973.696 5932701.953 566969.527 5932682.393 566977.178 5932680.668 
566988.946 5932678.103 566990.246 5932684.02 566993.233 5932697.68 
566973.696 5932701.953 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c5e60282-572a-4481-9d9f-1a3f9253da31">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567014.909 5932519.796</gml:lowerCorner>
          <gml:upperCorner>567035.432 5932553.53</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c8465bd9-6303-4487-b384-05db29667696" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7D67BAA2-692B-45C2-9B47-DF7334BDFF69" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567035.432 5932550.351 567022.311 5932553.53 567014.909 5932522.975 
567028.03 5932519.796 567035.432 5932550.351 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c8465bd9-6303-4487-b384-05db29667696">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567025.17 5932536.663</gml:lowerCorner>
          <gml:upperCorner>567025.17 5932536.663</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c5e60282-572a-4481-9d9f-1a3f9253da31" />
      <xplan:position>
        <gml:Point gml:id="Gml_A703AE3F-D34B-4082-8A73-B780D4B2A740" srsName="EPSG:25832">
          <gml:pos>567025.17 5932536.663</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_0cabdc46-fa47-4894-8520-b91f42beafdc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566915.756 5932532.088</gml:lowerCorner>
          <gml:upperCorner>566926.177 5932584.045</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_C2E77198-B5FB-425A-A61D-F8426BFB3E4B" srsName="EPSG:25832">
          <gml:posList>566915.756 5932532.088 566926.177 5932584.045 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_3ce0a5df-ecb1-4368-b86d-3e38f0400656">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566928.911 5932593.916</gml:lowerCorner>
          <gml:upperCorner>566946.549 5932597.67</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_7DA2BAB6-6599-4F02-BC65-EBEA8ED14603" srsName="EPSG:25832">
          <gml:posList>566946.549 5932593.916 566934.992 5932596.431 566928.911 5932597.67 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_f8c76475-e101-46a4-bd1a-027860c6b5de">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566991.063 5932493.339</gml:lowerCorner>
          <gml:upperCorner>566991.063 5932493.339</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">5.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_87270E46-F889-4FB3-96BC-2430EB0FEC30" srsName="EPSG:25832">
          <gml:pos>566991.063 5932493.339</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_3383bed7-613a-481c-b697-be51752fcceb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567003.229 5932469.658</gml:lowerCorner>
          <gml:upperCorner>567187.874 5932578.566</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_177a6d7b-fd34-4539-a553-13fca56cd965" />
      <xplan:refTextInhalt xlink:href="#GML_9e579ad3-db88-4dc3-8b64-9631d3e9ef3a" />
      <xplan:refTextInhalt xlink:href="#GML_548c7447-2601-4b47-99b5-092171bd6dc4" />
      <xplan:refTextInhalt xlink:href="#GML_457fffb9-dcce-4d2f-bd8d-1e043dfae1b0" />
      <xplan:refTextInhalt xlink:href="#GML_1c0ece7b-0f1e-4303-8d5b-867ada375758" />
      <xplan:refTextInhalt xlink:href="#GML_be6475a4-b0a8-4f18-afd0-6974662bbd88" />
      <xplan:refTextInhalt xlink:href="#GML_1e889626-8dad-47ea-875f-abad85e839d5" />
      <xplan:refTextInhalt xlink:href="#GML_e0ac5d8e-1150-46ca-925d-a76a5d887d7a" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4E03DC44-0B10-4776-9974-EF92C2A55132" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567187.874 5932535.083 567169.475 5932540.133 567119.194 5932553.393 
567078.123 5932563.724 567027.572 5932575.915 567016.031 5932578.566 
567003.229 5932525.727 567006.571 5932524.992 567003.883 5932512.784 
567063.768 5932498.267 567063.184 5932495.848 567084.347 5932490.73 
567086.106 5932490.73 567086.102 5932492.649 567102.204 5932488.352 
567173.092 5932469.658 567176.287 5932481.712 567172.93 5932482.603 
567187.874 5932535.083 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.9</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_6227ee87-ec9a-4601-b068-16cf51c46ec2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566896.659 5932484.125</gml:lowerCorner>
          <gml:upperCorner>567063.768 5932535.91</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_C1E41C9F-E5EC-4629-9D78-B59F03102C2C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567063.184 5932495.848 567063.768 5932498.267 567003.883 5932512.784 
566920.596 5932531.119 566896.659 5932535.91 566905.056 5932516.581 
566930.353 5932512.193 567021.644 5932492.095 567053.76 5932484.125 
567063.184 5932495.848 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:zugunstenVon>(FHH)</xplan:zugunstenVon>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="GML_9daf8ab2-a0d2-4026-898e-257ac8eb9faf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566994.386 5932652.215</gml:lowerCorner>
          <gml:upperCorner>567018.21 5932676.027</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Umgrenzung der Grundstücke, für die eine GKF bestimmt ist</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_BF184311-0D1B-4AD1-A04D-4F7C713E9F47" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567018.21 5932671.753 566998.672 5932676.027 566994.386 5932656.514 
567013.937 5932652.215 567018.21 5932671.753 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6be57063-39f8-4b4d-89c1-3f45ce1c66fc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567331.025 5932415.537</gml:lowerCorner>
          <gml:upperCorner>567356.773 5932434.575</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_1F12E023-DD67-41A4-98A7-2F06C41845BC" srsName="EPSG:25832">
          <gml:posList>567334.913 5932434.575 567331.025 5932422.687 567352.885 5932415.537 
567356.773 5932427.425 567347.744 5932430.378 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_0cbb5e9d-3d21-44d4-84f7-6a995476f627">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567006.427 5932636.482</gml:lowerCorner>
          <gml:upperCorner>567006.427 5932636.482</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.65</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_03E502F7-CEF9-4749-9A56-5FD739EB4C1C" srsName="EPSG:25832">
          <gml:pos>567006.427 5932636.482</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_d1b5dd40-5401-4778-ade7-ad8f64602d86">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566933.72 5932614.97</gml:lowerCorner>
          <gml:upperCorner>566940.257 5932645.268</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_2EE565DB-EA51-4E35-9FF5-DB38B792D31B" srsName="EPSG:25832">
          <gml:posList>566933.72 5932614.97 566940.257 5932645.268 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_671df8ee-b64e-4b70-9dc9-3bd0b070c72a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567146.984 5932562.989</gml:lowerCorner>
          <gml:upperCorner>567214.587 5932580.29</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_5FAF9387-EDA9-4709-9126-B0ABC93A33B1" srsName="EPSG:25832">
          <gml:posList>567214.587 5932579.1 567210.048 5932562.989 567197.054 5932566.651 
567186.553 5932569.468 567172.007 5932573.565 567157.176 5932577.743 
567146.984 5932580.29 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_c0998984-2b93-4408-8b8d-46e2b534c3b4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566806.421 5932618.153</gml:lowerCorner>
          <gml:upperCorner>567190.105 5932681.101</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="Gml_C97F7666-35C9-492F-9672-D3E2A2BC982B" srsName="EPSG:25832">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">566806.421 5932681.101 566928.621147107 5932659.39466638 567049.975 5932633.37 
</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567049.975 5932633.37 567104.292 5932620.712 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567104.292 5932620.712 567105.798 5932620.322 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567105.798 5932620.322 567111.751 5932618.78 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567111.751 5932618.78 567114.172 5932618.153 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567114.172 5932618.153 567114.558 5932619.643 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567114.558 5932619.643 567146.973 5932645.318 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567146.973 5932645.318 567148.044 5932643.966 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567148.044 5932643.966 567163.753 5932656.802 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567163.753 5932656.802 567163.7 5932656.869 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567163.7 5932656.869 567169.863 5932661.795 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567169.863 5932661.795 567171.022 5932665.159 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567171.022 5932665.159 567170.264 5932672.115 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567170.264 5932672.115 567179.299 5932670.67 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567179.299 5932670.67 567180.19 5932663.776 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567180.19 5932663.776 567180.463 5932663.213 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567180.463 5932663.213 567180.676 5932663.104 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567180.676 5932663.104 567190.105 5932661.47 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567190.105 5932661.47 567189.394 5932657.026 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567189.394 5932657.026 567183.357 5932651.903 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567183.357 5932651.903 567181.433 5932651.694 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567181.433 5932651.694 567180.37 5932650.982 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">567180.37 5932650.982 567177.668797695 5932650.06894137 567174.898 5932649.396 
</gml:posList>
            </gml:ArcString>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">567174.898 5932649.396 567172.079373424 5932648.96593218 567169.234 5932648.783 
</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567169.234 5932648.783 567166.803 5932650.117 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567166.803 5932650.117 567147.817 5932634.773 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_7655911c-722f-4905-870f-623fb7714c52">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567331.408 5932521.309</gml:lowerCorner>
          <gml:upperCorner>567343.778 5932525.306</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_5B4A6015-B342-41D5-ACA8-435899F56A78" srsName="EPSG:25832">
          <gml:posList>567331.408 5932525.306 567343.778 5932521.309 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_045eba20-890a-497d-9a0a-69132d901c68">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567011.967 5932503.763</gml:lowerCorner>
          <gml:upperCorner>567044.066 5932522.975</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(C)</xplan:gliederung1>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">28.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_67ee41de-6cab-4a6b-9353-b48392691a24" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a5da6db5-e492-4456-888e-06f5dd899507" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3BDCD153-18C9-400D-8991-D0D3E0D419A7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567044.066 5932515.912 567028.03 5932519.796 567014.909 5932522.975 
567011.967 5932510.826 567041.123 5932503.763 567044.066 5932515.912 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_67ee41de-6cab-4a6b-9353-b48392691a24">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567028.016 5932515.664</gml:lowerCorner>
          <gml:upperCorner>567028.016 5932515.664</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_045eba20-890a-497d-9a0a-69132d901c68" />
      <xplan:schriftinhalt>(C)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_43ADDE0A-F253-49FC-B681-2419CA931590" srsName="EPSG:25832">
          <gml:pos>567028.016 5932515.664</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a5da6db5-e492-4456-888e-06f5dd899507">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567028.016 5932510.157</gml:lowerCorner>
          <gml:upperCorner>567028.016 5932510.157</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_045eba20-890a-497d-9a0a-69132d901c68" />
      <xplan:position>
        <gml:Point gml:id="Gml_C90F42EE-8994-4F45-B18D-ADE40BD0E14B" srsName="EPSG:25832">
          <gml:pos>567028.016 5932510.157</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_9afb0fc3-dd77-41b1-b5f8-1afd24238689">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566943.701 5932569.013</gml:lowerCorner>
          <gml:upperCorner>566997.44 5932593.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_ad05c3b2-e661-4399-91a4-270b968056e5" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_35EE5C8C-3DC8-4547-BC3A-BAF3E2DFB4F1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566997.267 5932582.876 566946.549 5932593.916 566943.701 5932580.804 
566957.002 5932577.915 566997.44 5932569.013 566997.267 5932582.876 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ad05c3b2-e661-4399-91a4-270b968056e5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566989.742 5932577.612</gml:lowerCorner>
          <gml:upperCorner>566989.742 5932577.612</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9afb0fc3-dd77-41b1-b5f8-1afd24238689" />
      <xplan:position>
        <gml:Point gml:id="Gml_EBD24F30-06FA-41D1-B281-FD821C3D27B4" srsName="EPSG:25832">
          <gml:pos>566989.742 5932577.612</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_5754c10b-2762-4317-9be0-9e6599391792">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567167.871 5932467.365</gml:lowerCorner>
          <gml:upperCorner>567199.367 5932540.133</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_84329dc1-833d-4d58-96a7-a19a8aa4ebe5" />
      <xplan:refTextInhalt xlink:href="#GML_4a229a55-87d9-41e7-a476-f3122e8477d9" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_8E5DC607-41FF-4DD0-AE28-08773CAA4F32" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567184.571 5932479.389 567195.66 5932518.766 567199.367 5932531.929 
567187.874 5932535.083 567169.475 5932540.133 567169.185 5932526.2 
567168.882 5932516.457 567167.871 5932483.944 567172.93 5932482.603 
567176.287 5932481.712 567173.092 5932469.658 567181.142 5932467.365 
567184.571 5932479.389 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_e08d085b-6b7e-4743-86b3-a8638ad35c73">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566896.66 5932393.229</gml:lowerCorner>
          <gml:upperCorner>567391.856 5932535.909</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_30E5FCF7-7DC7-47A2-8593-9BA00808DFD8" srsName="EPSG:25832">
          <gml:posList>566896.66 5932535.909 566905.056 5932516.581 566930.353 5932512.193 
566976.01 5932502.142 567021.644 5932492.095 567053.76 5932484.125 
567063.182 5932495.848 567084.307 5932490.73 567086.117 5932490.73 
567086.102 5932492.649 567102.204 5932488.352 567102.207 5932488.345 
567108.812 5932469.393 567209.884 5932440.493 567253.207 5932428.035 
567336.183 5932403.704 567369.223 5932393.229 567391.856 5932402.799 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_4ce195e1-0c5e-421b-a6e2-53a3efaccbe4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567026.383 5932587.175</gml:lowerCorner>
          <gml:upperCorner>567057.194 5932594.554</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_8E3C8C2C-32F2-4AB3-A0E0-A1469AAA0F90" srsName="EPSG:25832">
          <gml:posList>567026.383 5932594.554 567057.194 5932587.175 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_862ae865-2d97-47bb-97c6-08171757657b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567132.743 5932480.828</gml:lowerCorner>
          <gml:upperCorner>567154.263 5932486.504</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_780BA5F8-368E-4995-9DE5-55A9A9BF564D" srsName="EPSG:25832">
          <gml:posList>567132.743 5932486.504 567154.263 5932480.828 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_52ea432b-2027-4255-a410-6ed9d888911f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567290.501 5932595.494</gml:lowerCorner>
          <gml:upperCorner>567290.501 5932595.494</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_9CAD5EFA-CB15-4E4C-A5F4-03BCB66C85C0" srsName="EPSG:25832">
          <gml:pos>567290.501 5932595.494</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_2f028f76-2db3-442c-ab15-c3e04d7ba925">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567376.256 5932416.857</gml:lowerCorner>
          <gml:upperCorner>567389.085 5932421.054</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_796DB169-5AA3-4348-A980-B587A56F4B22" srsName="EPSG:25832">
          <gml:posList>567376.256 5932421.054 567389.085 5932416.857 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_656fea01-d7ac-4d77-9d10-4f5e770fc779">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567320.88 5932521.309</gml:lowerCorner>
          <gml:upperCorner>567349.566 5932547.274</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_3AA427AA-D619-4876-9782-9EC6813B0755" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567349.566 5932539.285 567324.846 5932547.274 567320.88 5932535.003 
567333.25 5932531.005 567331.408 5932525.306 567343.778 5932521.309 
567349.566 5932539.285 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_cbbebd11-86e5-4d45-addc-0841f1e92a39">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566994.424 5932652.215</gml:lowerCorner>
          <gml:upperCorner>567018.21 5932676.027</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_C50C0EAA-C25B-4F5E-991D-DE7C5BCE3395" srsName="EPSG:25832">
          <gml:posList>566998.672 5932676.027 566994.424 5932656.506 567013.937 5932652.215 
567018.21 5932671.753 566998.672 5932676.027 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_6fa314b1-c97c-475d-a324-f3bb0536629a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567356.908 5932494.965</gml:lowerCorner>
          <gml:upperCorner>567382.662 5932503.403</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_486AD35F-72B8-475E-98B5-BC3BFDFA540F" srsName="EPSG:25832">
          <gml:posList>567356.908 5932503.403 567382.662 5932494.965 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_157049a7-2bfa-49c4-bbd7-16fed4f65a73">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567118.446 5932480.828</gml:lowerCorner>
          <gml:upperCorner>567169.185 5932536.814</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(B)</xplan:gliederung1>
      <xplan:gliederung2>(E)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_be6d6a08-a892-425f-8c06-095641545e1e" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b08c9ee6-0ecf-4dfe-b86d-0a1fe0159faf" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_8e760f3f-dfcf-4dca-abc7-1c8eeb773689" />
      <xplan:refTextInhalt xlink:href="#GML_e0ac5d8e-1150-46ca-925d-a76a5d887d7a" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_023B952B-BFF0-4303-A93B-6AAB02816A16" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567169.185 5932526.2 567128.946 5932536.814 567118.446 5932496.996 
567134.393 5932492.799 567132.738 5932486.505 567154.263 5932480.828 
567154.466 5932487.498 567155.485 5932519.99 567168.882 5932516.457 
567169.185 5932526.2 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_be6d6a08-a892-425f-8c06-095641545e1e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567138.618 5932505.546</gml:lowerCorner>
          <gml:upperCorner>567138.618 5932505.546</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_157049a7-2bfa-49c4-bbd7-16fed4f65a73" />
      <xplan:position>
        <gml:Point gml:id="Gml_0C799463-3117-47B2-A9F9-BB64312CE068" srsName="EPSG:25832">
          <gml:pos>567138.618 5932505.546</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_b08c9ee6-0ecf-4dfe-b86d-0a1fe0159faf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567138.618 5932510.508</gml:lowerCorner>
          <gml:upperCorner>567138.618 5932510.508</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_157049a7-2bfa-49c4-bbd7-16fed4f65a73" />
      <xplan:schriftinhalt>(B)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_9A368C2D-107A-41F1-9529-2370FE6E2DE9" srsName="EPSG:25832">
          <gml:pos>567138.618 5932510.508</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_c2b9d550-2444-4799-b789-0f0fe3b1d7cb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567222.982 5932449.219</gml:lowerCorner>
          <gml:upperCorner>567248.276 5932467.473</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_854A4D86-D3C9-4239-B67E-7188F4D25E8F" srsName="EPSG:25832">
          <gml:posList>567226.407 5932467.473 567222.982 5932455.446 567244.778 5932449.239 
567244.851 5932449.219 567248.276 5932461.245 567239.39 5932463.775 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_9e7c78e6-69f6-4b31-89de-4748bcb645fe">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567246.732 5932549.777</gml:lowerCorner>
          <gml:upperCorner>567267.911 5932569.208</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_557A79F5-9423-481D-9BF3-33F9FDCE8159" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567267.911 5932564.291 567260.753 5932566.4 567250.54 5932569.208 
567246.732 5932554.705 567264.1 5932549.777 567267.911 5932564.291 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_6dcf5c9e-b618-4122-b1e1-6de3a206ad8f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567082.499 5932570.614</gml:lowerCorner>
          <gml:upperCorner>567123.82 5932581.109</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_9D680420-49DF-48E3-9D91-91B4E4F115F2" srsName="EPSG:25832">
          <gml:posList>567082.499 5932581.109 567123.82 5932570.614 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_560d5848-3aa2-482e-b2a7-9cad75788ecc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567131.146 5932579.038</gml:lowerCorner>
          <gml:upperCorner>567289.481 5932670.725</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>SPIEL-UND FREIZEITANLAGE</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_4fcd0d62-647d-440c-9b4f-d6483da922ca" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_f4e240c9-c8be-4a5a-a128-9b2c9b241df8" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C73A2272-B006-4E2C-9340-76420F1DBB0A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_73C2D5C6-F0AE-4787-BC8C-27B596CB087C" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567229.68 5932659.841 567227.642169331 5932662.20687756 567224.757 5932663.401 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B9E9BA3D-FE8A-4081-9F0E-E208AE1150E7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567224.757 5932663.401 567179.292 5932670.725 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D143DF56-E557-4D4D-8CFE-09D5255968C9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567179.292 5932670.725 567180.19 5932663.776 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FA11B2D9-F649-4D49-9851-6E933415B3BF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567180.19 5932663.776 567180.463 5932663.213 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3ACB9950-9F15-4E01-BDF5-6D7E54B8ADA9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567180.463 5932663.213 567180.676 5932663.104 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B34ABB2F-4B3E-438A-A7EE-7CEEDE2DCF7F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567180.676 5932663.104 567190.105 5932661.47 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ADB6A918-9B1D-4B83-87B2-7B5D016FB619" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567190.105 5932661.47 567189.394 5932657.026 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_15F5169F-07B4-4693-86F3-4C78EFB59DD1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567189.394 5932657.026 567183.357 5932651.903 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_81E48D8B-8E13-4E1D-ACDF-9FF888FF7AD4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567183.357 5932651.903 567181.433 5932651.694 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E4F6886D-BBF7-47CA-8943-997E809F0AE0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567181.433 5932651.694 567180.37 5932650.982 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_736E6C9B-B0D6-401A-A0BF-9D673813AEB3" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567180.37 5932650.982 567177.668797695 5932650.06894137 567174.898 5932649.396 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_FE624392-DC28-4E46-8A8F-295E6EA6D5E8" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567174.898 5932649.396 567172.079373424 5932648.96593218 567169.234 5932648.783 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3B51ABF7-99A0-414C-9FE8-67E44699D99F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567169.234 5932648.783 567166.803 5932650.117 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_99B54F30-DE21-4B6A-A622-463147738B7C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567166.803 5932650.117 567147.817 5932634.773 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CF2243FE-ABA0-4FC8-B986-CBF23D732C02" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567147.817 5932634.773 567147.458 5932635.196 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0AC18A58-5BA8-48CE-A87C-2A2F578FB9B5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567147.458 5932635.196 567134.114 5932623.874 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FC7B6A48-431F-4BC2-AE4E-E1442C5329E6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567134.114 5932623.874 567135.572 5932622.155 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_56E493B1-5B61-4E62-B19E-6199687E44E3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567135.572 5932622.155 567132.995 5932620.186 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2972A42D-98BC-4049-AB68-99EADA1FB865" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567132.995 5932620.186 567131.146 5932613.757 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_77F1CDF2-2120-42C1-9BA6-071191166E16" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567131.146 5932613.757 567144.917 5932610.161 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C46FBC52-8A46-42FA-A77C-40CE60451C24" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567144.917 5932610.161 567163.622 5932605.044 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_AADF58D4-DFF2-4820-BB09-E421B367E86D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567163.622 5932605.044 567197.012 5932596.054 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_966C9F41-138F-47F8-92E1-50C4337748A4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567197.012 5932596.054 567260.353 5932579.038 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_29E7313A-D5B0-4308-AD5C-88B774CE5C53" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567260.353 5932579.038 567289.481 5932638.529 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_71D33360-A32F-479E-A5B4-32E24800BF9C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567289.481 5932638.529 567270.217 5932647.027 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_CF48A0E5-2720-467C-BBC8-1046F4D370C2" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567270.217 5932647.027 567268.631652061 5932647.10088381 567267.409 5932646.089 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4D1CAF13-8C21-4A25-9E2E-C9ADF832ADB2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567267.409 5932646.089 567263.23 5932638.624 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D7494788-386A-4FF9-BB09-5A4858D393EE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567263.23 5932638.624 567252.254 5932644.128 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_9FC4E48A-6801-4E61-BA6B-CAE048A9E1F3" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567252.254 5932644.128 567249.306595184 5932644.60808494 567246.611 5932643.323 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_577B39B5-272F-49C0-87C5-5813DDE776CF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567246.611 5932643.323 567240.942 5932638.06 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_19B9CB31-DC38-427F-9889-6D12F3071404" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567240.942 5932638.06 567229.68 5932659.841 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>2200</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:zugunstenVon>(FHH)</xplan:zugunstenVon>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_4fcd0d62-647d-440c-9b4f-d6483da922ca">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567207.831 5932628.232</gml:lowerCorner>
          <gml:upperCorner>567207.831 5932628.232</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_18.01</xplan:stylesheetId>
      <xplan:art>zugunstenVon</xplan:art>
      <xplan:art>text</xplan:art>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_560d5848-3aa2-482e-b2a7-9cad75788ecc" />
      <xplan:position>
        <gml:Point gml:id="Gml_E3FD5FE4-8476-4214-A3CB-F3ED954A7438" srsName="EPSG:25832">
          <gml:pos>567207.831 5932628.232</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_2d98de27-e595-430b-b7f1-f09ce3493343">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566997.267 5932575.915</gml:lowerCorner>
          <gml:upperCorner>567027.572 5932582.876</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_D6C04363-A7F9-4FAB-83BD-61399D22C17F" srsName="EPSG:25832">
          <gml:posList>567027.572 5932575.915 567027.572 5932575.915 567016.031 5932578.566 
566997.267 5932582.876 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_9f9aaf8d-a8d5-49e9-9214-6f2e2b2f2948">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566992.561 5932594.556</gml:lowerCorner>
          <gml:upperCorner>567029.478 5932633.303</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_3EDF90E6-DC13-454A-AAE4-6B0D7877C19F" srsName="EPSG:25832">
          <gml:posList>566995.428 5932633.303 566992.561 5932620.709 567005.237 5932617.823 
567001.238 5932600.259 567026.383 5932594.556 567029.478 5932607.177 
567016.803 5932610.062 567020.817 5932627.696 566995.428 5932633.303 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_3e44a8a3-2e27-4cca-af34-d46e8561dd12">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566992.561 5932610.062</gml:lowerCorner>
          <gml:upperCorner>567020.817 5932633.303</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_CDC62B61-9C1E-46F8-8AD9-1D1D50798836" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567020.817 5932627.696 566995.428 5932633.303 566992.561 5932620.709 
567005.237 5932617.823 567004.127 5932612.948 567016.803 5932610.062 
567020.817 5932627.696 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_bc672f2f-b760-4538-be97-fd44840abb56">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567131.146 5932521.988</gml:lowerCorner>
          <gml:upperCorner>567436.534 5932622.155</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="Gml_27127076-9323-45F8-BA86-F93306590EA8" srsName="EPSG:25832">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567135.572 5932622.155 567132.995 5932620.186 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567132.995 5932620.186 567131.146 5932613.757 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567131.146 5932613.757 567144.917 5932610.161 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567144.917 5932610.161 567163.622 5932605.044 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567163.622 5932605.044 567265.319 5932577.662 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567265.319 5932577.662 567316.153 5932562.408 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567316.153 5932562.408 567346.892 5932552.563 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567346.892 5932552.563 567379.175 5932541.711 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567379.175 5932541.711 567381.274 5932541.041 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567381.274 5932541.041 567386.424 5932539.397 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">567386.424 5932539.397 567403.312947425 5932533.80501785 567420.116 5932527.96 
</gml:posList>
            </gml:ArcString>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">567420.116 5932527.96 567428.334882766 5932525.00116933 567436.534 5932521.988 
</gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_9f73fd87-3c94-459b-98db-e7ee438b3f33">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566850.707 5932657.217</gml:lowerCorner>
          <gml:upperCorner>566850.707 5932657.217</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_7CF1F164-C6C4-4B63-B02A-B9F0CF33DFB4" srsName="EPSG:25832">
          <gml:pos>566850.707 5932657.217</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a9cd8d73-e8ac-469b-87a9-3beb2c925f68">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566893.63 5932688.694</gml:lowerCorner>
          <gml:upperCorner>566914.1151 5932716.539</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b2f0e7a1-047e-436a-8a3f-04e94c050fba" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_5D8527A8-99CE-4B91-B0D8-917299759525" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_8713A246-9561-4DFC-8280-159D79DB150F" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566913.644 5932698.444 566914.08697411 5932701.36156212 566913.991 5932704.311 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1CE49E12-1742-441C-9935-956010871AE2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566913.991 5932704.311 566913.512 5932706.751 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D8D18FAB-E005-48E8-9D22-8618005734BC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566913.512 5932706.751 566912.072 5932714.074 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_732E2783-0BB9-40D6-9FA7-78E746ADF554" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566912.072 5932714.074 566898.942 5932716.539 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_65ED06DC-A272-420B-9D6C-387A684B929A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566898.942 5932716.539 566895.79 5932703.165 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3B217E47-68D5-4D13-874A-D84B4D86240C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566895.79 5932703.165 566893.63 5932692.099 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9EE96DFA-23A4-41B8-A2B0-D42704C33BA7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566893.63 5932692.099 566897.607 5932691.367 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0AA5C164-AF67-46FB-BD9B-94A2D9FC5400" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566897.607 5932691.367 566911.833 5932688.694 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_263FE4E2-8E25-48D0-9A71-134BF9CAC2F5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566911.833 5932688.694 566913.644 5932698.444 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:Z>12</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b2f0e7a1-047e-436a-8a3f-04e94c050fba">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566904.666 5932701.01</gml:lowerCorner>
          <gml:upperCorner>566904.666 5932701.01</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_a9cd8d73-e8ac-469b-87a9-3beb2c925f68" />
      <xplan:position>
        <gml:Point gml:id="Gml_FD2441D2-C1BF-42AC-9A02-9C209DAD92A4" srsName="EPSG:25832">
          <gml:pos>566904.666 5932701.01</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_42340637-90e6-459e-8ade-f5c577d42407">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567327.348 5932503.403</gml:lowerCorner>
          <gml:upperCorner>567360.906 5932525.306</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a588b80d-4031-4be4-aef6-acac3c43cf65" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B836A35A-69FF-46B5-8D27-2D3AE3530ECD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567360.906 5932515.773 567343.778 5932521.309 567331.408 5932525.306 
567327.348 5932512.741 567356.908 5932503.403 567360.906 5932515.773 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a588b80d-4031-4be4-aef6-acac3c43cf65">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567337.013 5932516.588</gml:lowerCorner>
          <gml:upperCorner>567337.013 5932516.588</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_42340637-90e6-459e-8ade-f5c577d42407" />
      <xplan:position>
        <gml:Point gml:id="Gml_45EF00C6-08D5-4317-BD6C-80F2B6E6FF15" srsName="EPSG:25832">
          <gml:pos>567337.013 5932516.588</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_78660969-b98c-486f-80e6-0e1866312b19">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567043.491 5932661.987</gml:lowerCorner>
          <gml:upperCorner>567067.203 5932685.775</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_E0F8113E-BF37-41C3-8756-39421EC8C68B" srsName="EPSG:25832">
          <gml:posList>567047.666 5932685.775 567043.491 5932666.216 567062.928 5932661.987 
567067.203 5932681.504 567047.666 5932685.775 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_73250940-e373-48bf-86ff-2f44846a4bbc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567236.764 5932569.208</gml:lowerCorner>
          <gml:upperCorner>567250.54 5932572.996</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_D7371532-314B-46DC-8473-B3531E95583B" srsName="EPSG:25832">
          <gml:posList>567250.54 5932569.208 567236.764 5932572.996 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_92fdc00f-053e-43cd-b4a5-8296167b6975">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567246.732 5932549.777</gml:lowerCorner>
          <gml:upperCorner>567267.911 5932569.208</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_EDD73036-D94A-4A53-89F0-1D5C2D17BB16" srsName="EPSG:25832">
          <gml:posList>567250.54 5932569.208 567246.732 5932554.705 567264.1 5932549.777 
567267.911 5932564.291 567260.753 5932566.4 567250.54 5932569.208 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_f972361c-1dd3-410d-a9b8-23859ab6f9da">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567252.933 5932437.967</gml:lowerCorner>
          <gml:upperCorner>567285.279 5932458.666</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(C)</xplan:gliederung1>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">28.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_50973c78-7db3-4bd0-8214-435b7f42455b" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a03df1e3-0787-4f6a-bc90-05063271b33d" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7f9eac41-cac0-4a1b-a61d-3d743fbdcec4" />
      <xplan:refTextInhalt xlink:href="#GML_e45b25c0-9560-4380-bd62-be06baabaa48" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BBC6F98F-7C18-4136-957F-B96D049A71E1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567285.279 5932449.925 567269.392 5932454.695 567256.489 5932458.666 
567252.933 5932446.744 567281.619 5932437.967 567285.279 5932449.925 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_50973c78-7db3-4bd0-8214-435b7f42455b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567277.09 5932442.66</gml:lowerCorner>
          <gml:upperCorner>567277.09 5932442.66</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f972361c-1dd3-410d-a9b8-23859ab6f9da" />
      <xplan:position>
        <gml:Point gml:id="Gml_1405A1C8-791B-48B2-AA60-391E9BA84012" srsName="EPSG:25832">
          <gml:pos>567277.09 5932442.66</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_a03df1e3-0787-4f6a-bc90-05063271b33d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567277.09 5932448.168</gml:lowerCorner>
          <gml:upperCorner>567277.09 5932448.168</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>PZV_20.20_Text</xplan:stylesheetId>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f972361c-1dd3-410d-a9b8-23859ab6f9da" />
      <xplan:schriftinhalt>(C)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_D9E0D762-6964-40E6-8880-2F7E2C2F6706" srsName="EPSG:25832">
          <gml:pos>567277.09 5932448.168</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="GML_59ddf1de-5576-4fdb-9121-1f0eda08b3f9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567068.417 5932636.027</gml:lowerCorner>
          <gml:upperCorner>567092.229 5932659.843</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Umgrenzung der Grundstücke, für die eine GKF bestimmt ist</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_97A4804B-DEBA-434C-A764-C68EB4C04FDF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567092.229 5932655.565 567072.669 5932659.843 567068.417 5932640.301 
567087.955 5932636.027 567092.229 5932655.565 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_4226b49b-8d87-4683-b5ed-3b7ca47fca86">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567395.164 5932431.736</gml:lowerCorner>
          <gml:upperCorner>567406.511 5932435.902</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_80358424-4C4D-42AC-9FCB-7D2BC5FA7DCE" srsName="EPSG:25832">
          <gml:posList>567406.511 5932431.736 567395.164 5932435.902 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_27485c38-dad5-4b2a-9ef7-9a3fd75c089e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566840.984 5932623.628</gml:lowerCorner>
          <gml:upperCorner>566896.697 5932663.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2e133a2d-d355-47c8-b0a8-dc884245b3dd" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c4a58f61-9bff-43f6-9acc-6ffc458fa0d6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_47258EA4-F024-4ED0-B075-88A91763F30E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566868.159 5932641.579 566859.999 5932660.533 566840.984 5932663.959 
566855.11 5932630.759 566890.641 5932623.628 566896.697 5932653.919 
566883.881 5932656.228 566880.468 5932639.11 566868.159 5932641.579 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2e133a2d-d355-47c8-b0a8-dc884245b3dd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566857.639 5932645.57</gml:lowerCorner>
          <gml:upperCorner>566857.639 5932645.57</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_27485c38-dad5-4b2a-9ef7-9a3fd75c089e" />
      <xplan:position>
        <gml:Point gml:id="Gml_6A53AE33-7C16-4AC9-AA50-E2C6AB8E92C8" srsName="EPSG:25832">
          <gml:pos>566857.639 5932645.57</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_bec94b65-0f9a-4ffb-a616-46f6f2893fe5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567078.123 5932553.393</gml:lowerCorner>
          <gml:upperCorner>567119.194 5932563.724</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_DA161C99-89B8-4259-B5C0-9CF7477A8DD2" srsName="EPSG:25832">
          <gml:posList>567119.194 5932553.393 567078.123 5932563.724 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_e0d3029c-3d59-4b56-8fc3-b61f50da3f42">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567267.911 5932558.717</gml:lowerCorner>
          <gml:upperCorner>567286.823 5932564.291</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_9C3EDD04-9F88-4ABB-B6EC-4DC3BA937253" srsName="EPSG:25832">
          <gml:posList>567286.823 5932558.717 567267.911 5932564.291 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_284B179C-3467-44F6-9B04-C2014D529B66">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566779.127 5932676.0536</gml:lowerCorner>
          <gml:upperCorner>566835.3753 5932745.9337</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Baakenhafen</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>3000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_1A1A8F4F-0649-423C-9E25-3933C6C083A6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_6FA83B51-35A5-4C0E-A4F6-41D5EA445944" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566830.423 5932693.484 566827.957952836 5932700.81212196 566824.851 5932707.892 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_D01E97DD-D89F-4EED-B023-F3C3822A2E21" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566824.851 5932707.892 566823.111316407 5932711.30040007 566821.281 5932714.661 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_DC6CEB92-EAA1-4B56-A43A-8312E8F40B8F" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566821.281 5932714.661 566819.364005272 5932718.03299084 566817.414 5932721.386 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_79F295E6-9DCC-48ED-8D3C-781BDABD95F9" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566817.414 5932721.386 566812.472959596 5932729.93307302 566807.857 5932738.66 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9E6F0BDF-F6F0-44DC-B593-08166EE781F7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566807.857 5932738.66 566806.3997 5932741.7876 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E61D37A1-68A9-4170-9909-406A86D1E36E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566806.3997 5932741.7876 566779.127 5932745.9337 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_98FD5835-E287-40C8-AB52-352FC93761A7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566779.127 5932745.9337 566780.746 5932742.65 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_620AFC1E-7EC5-4CA3-B307-34B70D3B7DC1" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566780.746 5932742.65 566784.924953126 5932733.70710426 566788.841 5932724.646 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_D8EE917A-B11D-4902-A4CF-9093F5C8340B" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566788.841 5932724.646 566794.923009707 5932710.13297725 566801.188 5932695.698 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_736CCE83-5C3F-4DFE-A84A-9D675721A753" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566801.188 5932695.698 566804.528194416 5932688.28262212 566807.8745 5932680.87 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1401E59D-9C56-4E0A-86F4-46D1C906EC68" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566807.8745 5932680.87 566807.8854 5932680.8457 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F218F2F0-9AC8-46EF-BB44-371048BDC1A6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566807.8854 5932680.8457 566835.3753 5932676.0536 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_88D02D75-F096-4501-9FC6-ED04B97EF47A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566835.3753 5932676.0536 566830.423 5932693.484 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_9D324BB6-40A7-46AA-B8AA-4340B40B8173">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566778.099 5932680.8457</gml:lowerCorner>
          <gml:upperCorner>566807.8854 5932746.09</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Baakenhafen</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>3000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7700CF1C-506C-4A82-AE8F-49EB1EB1CFB8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_4C8B1C52-E2F8-4D41-99A8-225AFF674296" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566807.8745 5932680.87 566804.528194416 5932688.28262212 566801.188 5932695.698 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_A56270B9-B562-4ACA-8B1C-30646C0DD5DF" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566801.188 5932695.698 566794.923009709 5932710.13297725 566788.841 5932724.646 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_40ECA7D3-BA5A-4824-B756-4717E0D6EB10" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566788.841 5932724.646 566784.924953126 5932733.70710426 566780.746 5932742.65 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_90814D91-0851-4A48-92AE-8754D2F89A73" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566780.746 5932742.65 566779.127 5932745.9337 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E3DE3823-5EEE-4ED0-A771-B5B1C1DCAC5E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566779.127 5932745.9337 566778.099 5932746.09 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CE9983EC-9524-4B16-8F14-023C3861579A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566778.099 5932746.09 566806.421 5932681.101 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A392493E-FB81-4B83-AE70-39F3A345EB67" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566806.421 5932681.101 566807.8854 5932680.8457 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9A0C0BAC-F581-4710-AFCF-7852D6D3164D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566807.8854 5932680.8457 566807.8745 5932680.87 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="Gml_223B043C-1A7A-4AE2-BA39-577D6655F371">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566807.8854 5932664.165</gml:lowerCorner>
          <gml:upperCorner>566839.738 5932680.8457</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_01460DF5-FA6E-4770-B906-43E1E1704F95" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_386ECA97-BC53-4D2C-AC88-5469E5F10EBA" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566839.738 5932664.165 566837.404038162 5932670.0532968 566835.3753 5932676.0536 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F79863DB-CEF3-41B0-AB15-1FCD776A90E8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566835.3753 5932676.0536 566807.8854 5932680.8457 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E2EAE796-63F4-4782-957E-4E68ADDA1D6C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566807.8854 5932680.8457 566813.49 5932668.458 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_29577242-4E1F-4F6C-9B0D-D35AD38D6FBB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566813.49 5932668.458 566815.846 5932668.067 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_99C1F177-8310-4040-883A-FA41693B8CF8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566815.846 5932668.067 566837.401 5932664.548 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2860BB19-1B2B-436B-B5A1-A011AEDAD5C1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566837.401 5932664.548 566839.738 5932664.165 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1300</xplan:zweckbestimmung>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="Gml_6157700A-BB36-459E-BCDC-06517E81E6E3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566806.421 5932668.458</gml:lowerCorner>
          <gml:upperCorner>566813.49 5932681.101</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_2B202DE0-DAEE-4ED7-A90F-CB6C24A9E077" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566807.8854 5932680.8457 566806.421 5932681.101 566811.8098 5932668.7374 
566813.49 5932668.458 566807.8854 5932680.8457 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1300</xplan:zweckbestimmung>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="Gml_9C7CBD5B-194F-4F60-8DA4-974188283E0A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566779.127 5932676.0536</gml:lowerCorner>
          <gml:upperCorner>566835.3753 5932745.9337</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_69AC348D-2F3A-41FA-8E17-137BC15AD1E4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_FB0BE583-4B0E-4BB5-96A7-BD6CA5D39AF4" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566830.423 5932693.484 566827.957952836 5932700.81212196 566824.851 5932707.892 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_46CF3044-F310-4937-8804-F0547F4F3CE0" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566824.851 5932707.892 566823.111316407 5932711.30040007 566821.281 5932714.661 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_E427B2DD-7DA5-4530-8CE2-44740F84D7AC" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566821.281 5932714.661 566819.364005272 5932718.03299084 566817.414 5932721.386 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_A9565995-85C1-4FB3-B67E-69FC9756D019" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566817.414 5932721.386 566812.472959596 5932729.93307302 566807.857 5932738.66 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F8F7863A-F723-4102-872F-447088A0F46A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566807.857 5932738.66 566806.3997 5932741.7876 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_AA06440A-138C-49FE-8461-568D2CE0B6AF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566806.3997 5932741.7876 566779.127 5932745.9337 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_739CEFE6-6E37-42EC-95BA-10A05B49B3DF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566779.127 5932745.9337 566780.746 5932742.65 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_D65DD3CB-831E-4478-A1F6-B6F553AD8C4B" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566780.746 5932742.65 566784.924953126 5932733.70710426 566788.841 5932724.646 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_3DD4C574-3A0A-46F4-AEE2-A2B9068BD18A" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566788.841 5932724.646 566794.923009707 5932710.13297725 566801.188 5932695.698 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_E7929745-74B5-4422-BA7F-5FC12EE24D3F" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566801.188 5932695.698 566804.528194416 5932688.28262212 566807.8745 5932680.87 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_72FA4905-49FD-4741-8DED-09E419C2F13C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566807.8745 5932680.87 566807.8854 5932680.8457 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_66F1E2DC-93D4-46A0-B53E-42546BCED3CC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566807.8854 5932680.8457 566835.3753 5932676.0536 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_772BACEC-E62C-4A56-94AD-DE7A47169887" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566835.3753 5932676.0536 566830.423 5932693.484 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="Gml_F5814B54-E41D-4193-B85C-59D7D129876D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566807.8854 5932664.165</gml:lowerCorner>
          <gml:upperCorner>566839.738 5932680.8457</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_5E0E967A-6A80-4649-8F5B-1507486C4ABB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_28D4CB85-3FBA-43ED-A943-5E8B8593CD3B" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566839.738 5932664.165 566837.404038162 5932670.0532968 566835.3753 5932676.0536 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DF3E06D4-D01D-4696-BAFF-A6A82329D4B4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566835.3753 5932676.0536 566807.8854 5932680.8457 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2AC8ED50-9B29-4CA6-AC05-4C64B5C0DFEC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566807.8854 5932680.8457 566813.49 5932668.458 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5C8DCEDD-D32B-47FE-A7E1-F9F0053F8D7B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566813.49 5932668.458 566815.846 5932668.067 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_27C823FD-E427-4567-9F7B-CC9DA9C81711" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566815.846 5932668.067 566837.401 5932664.548 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2A929EC9-BC3B-4E10-B4AB-606EAA248DE6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566837.401 5932664.548 566839.738 5932664.165 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="Gml_C06DFF33-FE68-4435-8BDF-AC62C1C93810">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566881.191 5932374.896</gml:lowerCorner>
          <gml:upperCorner>567387.923 5932509.534</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="Gml_D1D28E96-DEF3-4838-AE6A-CC1C4D7A16BC" srsName="EPSG:25832">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">567387.923 5932374.896 567360.457157475 5932384.26594882 567332.731 5932392.835 
</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567332.731 5932392.835 567315.9435 5932397.7576 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567315.9435 5932397.7576 567249.81 5932417.15 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567249.81 5932417.15 567201.5084 5932431.0399 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567201.5084 5932431.0399 567163.242 5932442.044 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567163.242 5932442.044 567118.766 5932454.705 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567118.766 5932454.705 567108.797 5932457.542 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">567108.797 5932457.542 567036.828398072 5932476.62190217 566964.214 5932493.074 
</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">566964.214 5932493.074 566930.476 5932500.073 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">566930.476 5932500.073 566928.036 5932500.547 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">566928.036 5932500.547 566904.618968746 5932505.069006 566881.191 5932509.534 
</gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Wasserrecht gml:id="GML_0238e7f9-3ca7-4798-aeea-f49bba91128b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566867.114 5932374.896</gml:lowerCorner>
          <gml:upperCorner>567396.069 5932541.836</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Hochwassergefährdeter Bereich</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_af148c04-d4bc-4965-b920-b9e02ec6b0ca" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_86876801-AD48-4094-BC64-12A7D364EBC5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FEA997FD-D721-46D6-8F4F-2919F5760EA8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567077.181 5932492.456 567077.222 5932495.019 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_70C64DF6-6844-4857-98F0-27090FF16A4F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567077.222 5932495.019 567003.883 5932512.784 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8FC9BF4A-C23E-455D-824E-CB5CEEFB3744" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567003.883 5932512.784 566920.596 5932531.119 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9456FDF2-4000-45F8-AD8C-09EC6763C6D5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566920.596 5932531.119 566896.646 5932535.912 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5E56EA3E-C6C4-4991-A551-8492B45BB363" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566896.646 5932535.912 566867.114 5932541.836 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B11DEE77-468A-49FC-B28F-379F82032B0A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566867.114 5932541.836 566881.191 5932509.534 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_A407267E-91D7-4141-A0F2-9704C4BFC827" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566881.191 5932509.534 566904.618968746 5932505.069006 566928.036 5932500.547 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_628371F9-9147-4B2B-B2B8-EFB6F895DB73" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566928.036 5932500.547 566930.476 5932500.073 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C27A354B-CF2B-4DCA-AB10-D28019013945" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566930.476 5932500.073 566964.214 5932493.074 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_5DD22B47-754A-4185-9C6F-123014E3CA2E" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566964.214 5932493.074 567036.828398072 5932476.62190217 567108.797 5932457.542 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0807FAFD-3A81-4758-BEE4-020286994915" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567108.797 5932457.542 567118.766 5932454.705 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_008C80F7-BB04-4FA8-930E-0AD75E00EFEC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567118.766 5932454.705 567163.242 5932442.044 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DBAF0D28-4C81-42A6-9834-DFA87BE77A33" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567163.242 5932442.044 567249.81 5932417.15 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_87A00526-0B8E-4774-8AF8-578AA32361A6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567249.81 5932417.15 567332.731 5932392.835 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_B04DB0C6-280E-4AD5-92F7-8404394D467E" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567332.731 5932392.835 567360.457157475 5932384.26594882 567387.923 5932374.896 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3692C2F8-6F13-40D9-BB2F-41D44113E52F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567387.923 5932374.896 567396.069 5932401.322 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_29FCDB4A-6481-48B2-A275-87DF2B89CE3A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567396.069 5932401.322 567391.856 5932402.799 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7EE5CD74-21C1-4A51-8572-2B61D4B19FC7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567391.856 5932402.799 567323.418 5932425.183 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0DAE8B7C-8E0C-49B1-B894-4060DFBEFF44" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567323.418 5932425.183 567244.852 5932449.223 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0C1EE394-70C0-4FE8-849A-94DACAC381E1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567244.852 5932449.223 567173.092 5932469.658 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FD5DBBBF-8811-4FF8-85ED-CBAB02522C5E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567173.092 5932469.658 567102.204 5932488.352 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F1AD89CC-231D-4A35-9886-650C0798B54F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567102.204 5932488.352 567096.717 5932489.816 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4F6733A1-A80D-49F1-A17F-D1F676A300E0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567096.717 5932489.816 567096.706 5932501.992 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DAE717E6-FBF5-4502-9977-A113FE5D7649" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567096.706 5932501.992 567086.085 5932501.992 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9A7D6C29-4568-472B-9C31-31714D5B45AC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567086.085 5932501.992 567086.102 5932492.649 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5E0594E8-9665-4337-A084-CFCA717E283E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567086.102 5932492.649 567086.117 5932490.73 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E6434F5D-9984-4331-8458-FAE22D564979" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567086.117 5932490.73 567084.307 5932490.73 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_88AA2C39-10B2-4155-8A4C-AD22A884FA88" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567084.307 5932490.73 567077.181 5932492.456 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>20002</xplan:artDerFestlegung>
    </xplan:SO_Wasserrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AusgleichsFlaeche gml:id="GML_4d98ae00-0a48-4e96-9e52-5799884a21d1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567099.6529 5932527.893</gml:lowerCorner>
          <gml:upperCorner>567422.0157 5932692.2008</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_4A49A83C-D67B-4CEE-9181-51710984E562" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_99BDBDA1-F72E-4CB1-B48B-BCA103CA0762" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567244.066 5932671.991 567238.983936496 5932677.89204593 567231.788 5932680.87 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1615138B-3E71-4E74-87A5-ADCF52E7DADF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567231.788 5932680.87 567162.313 5932691.98 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_65D0F411-F26C-4111-9DDF-F22C2379CD6E" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567162.313 5932691.98 567154.129432725 5932691.35113954 567147.12 5932687.081 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B0F91911-A320-4B64-94DB-AA9A1AA80C3A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567147.12 5932687.081 567104.226 5932644.441 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_803F47B9-1CB7-42F8-A74A-94F80786E5C0" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567104.226 5932644.441 567099.675992494 5932634.29286275 567103.092 5932623.709 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CA02312C-09ED-4319-BDF1-14979E452166" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567103.092 5932623.709 567105.836 5932620.31 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6070527C-67DB-4C89-AFD1-982702BDE634" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567105.836 5932620.31 567111.875 5932618.745 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F9DB727B-727D-4870-A07F-D9CBA4D1AA04" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567111.875 5932618.745 567123.474 5932627.696 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0EAE2A45-D4F1-4AC2-8C3A-88C37D57EF69" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567123.474 5932627.696 567121.123 5932630.567 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_32821BA2-FE89-478E-B754-6E4408250B55" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567121.123 5932630.567 567120.26402466 5932633.22945013 567121.409 5932635.782 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B68E8585-84AA-4915-90E6-DB1D844D3909" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567121.409 5932635.782 567157.554 5932671.713 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_F786A31E-29E2-4FD5-AE47-CAD4ABADB436" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567157.554 5932671.713 567159.940300474 5932673.1620966 567162.724 5932673.375 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F6CE639A-9166-487B-A8D7-CBE1D4E7F3C8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567162.724 5932673.375 567170.264 5932672.115 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BD6DDADB-D904-4709-A47A-68A11DD9C6B0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567170.264 5932672.115 567179.292 5932670.725 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D8A2B8E8-3852-43E6-B3A0-11E2B32A0940" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567179.292 5932670.725 567224.757 5932663.401 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_5E1903D8-35EF-4A28-8713-7853EDB9D111" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567224.757 5932663.401 567227.642169331 5932662.20687756 567229.68 5932659.841 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_60CDDED6-B337-4DE3-B2BD-EDE4732F3135" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567229.68 5932659.841 567240.942 5932638.06 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A54C02EF-1829-474D-82D2-EA72A2A691A6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567240.942 5932638.06 567246.611 5932643.323 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_46B212EA-A001-4870-8740-46A3EEA7157A" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567246.611 5932643.323 567249.306595184 5932644.60808494 567252.254 5932644.128 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FA5E81E7-2654-477F-A927-C5A9071C6667" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567252.254 5932644.128 567263.23 5932638.624 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5C365827-6BF4-4A3F-A9B4-8D3FCCDCBB94" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567263.23 5932638.624 567267.409 5932646.089 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_F1F18F1E-4CF4-479A-A058-8BF9451CF12C" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567267.409 5932646.089 567268.631652061 5932647.10088381 567270.217 5932647.027 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CE46242F-FDFC-45DA-B975-DF6D4494875B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567270.217 5932647.027 567289.481 5932638.529 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_04E34ED6-2341-4611-88FD-F6F7855FCE72" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567289.481 5932638.529 567315.262 5932627.157 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_924DAAA3-3565-4CED-A2CB-CED8ACA58325" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567315.262 5932627.157 567316.385915319 5932626.03519127 567316.461 5932624.449 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D553724E-B07C-4C26-96B6-DB74ED461580" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567316.461 5932624.449 567312.888 5932613.722 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_005DE293-0AB0-4130-B677-522A0DF983E5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567312.888 5932613.722 567332.567 5932603.854 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E735F0C3-04AE-4FC2-ADD9-673426F44DD3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567332.567 5932603.854 567332.608 5932606.292 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_157FCFB4-3254-4407-B36A-7184C7C0CE52" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567332.608 5932606.292 567332.94704748 5932607.217058 567333.787 5932607.732 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8FCD126D-D30C-4B53-A009-0CED8F78F250" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567333.787 5932607.732 567347.217 5932610.684 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_F6F0E256-54BD-4169-A208-5D29CB64AC94" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567347.217 5932610.684 567348.190716628 5932610.57013671 567348.886 5932609.879 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A2274313-E4AA-4A35-A672-A531342C4D6C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567348.886 5932609.879 567350.262 5932607.073 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9C4A6A3B-6757-4538-8CE4-DB3D4D73C9BF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567350.262 5932607.073 567354.756 5932597.904 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6DADC00F-BC41-4218-8044-D8D628D31938" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567354.756 5932597.904 567372.068 5932598.244 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_76B1EBFE-2DEF-4077-95D7-44BE4AF0641B" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567372.068 5932598.244 567377.133845179 5932596.69210919 567380.296 5932592.441 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5B7A58D3-6C0F-4F66-8281-EFA9377E3951" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567380.296 5932592.441 567392.599 5932555.691 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_636EF284-1108-47A6-8422-946A26639A88" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567392.599 5932555.691 567391.9590711 5932551.43009869 567388.12 5932549.474 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D6F4BC19-903B-48DC-8A0C-904715FF4120" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567388.12 5932549.474 567380.373 5932549.468 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C6C7291A-1B90-4FE2-99A3-3E729AC28AC9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567380.373 5932549.468 567381.274 5932541.041 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_479609D6-2C0E-459D-8039-3D926BAC224E" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567381.274 5932541.041 567400.79971374 5932534.6570967 567420.197 5932527.893 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_98E5E426-1980-4646-A991-4FDB8CA891B4" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567420.197 5932527.893 567422.009996543 5932537.23282808 567420.574 5932546.638 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FF63874F-B07F-44B6-B052-803C4A32697B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567420.574 5932546.638 567400.821 5932605.659 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_B7CDC673-BF7F-4D42-B3C5-0B0EFB9F1747" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567400.821 5932605.659 567391.913905592 5932617.62906659 567377.649 5932622.003 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EE91530B-442E-469B-AE58-D772E19F0A05" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567377.649 5932622.003 567363.522 5932621.731 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2BAA98B6-A534-4E69-96ED-B7878D2183A4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567363.522 5932621.731 567362.186 5932624.461 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_6F4E3776-7E15-4B11-BAD4-1D72D05FE46C" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567362.186 5932624.461 567357.284310283 5932629.36584947 567350.398 5932630.18 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EE05365E-37AD-4E74-BAEB-024E0B8E9212" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567350.398 5932630.18 567336.713 5932627.174 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_61C12652-C435-4A7E-8424-668426326E12" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567336.713 5932627.174 567336.127172531 5932634.9996287 567330.524 5932640.494 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A84E562B-3549-4F3A-AA04-9BDF20A15429" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567330.524 5932640.494 567269.78 5932667.286 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_31CEE1F5-542B-40AD-B443-8E8B9EA6C5C5" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567269.78 5932667.286 567261.957672114 5932667.72189714 567255.786 5932662.896 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E127A547-4753-499E-A2C7-10B42ED16F78" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567255.786 5932662.896 567254.463 5932663.559 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_5AF89751-75E9-4E56-9D88-5E29E092AA00" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567254.463 5932663.559 567251.166806781 5932664.71204086 567247.686 5932664.992 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3B593025-8F8C-404B-95CA-B89849429397" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567247.686 5932664.992 567244.066 5932671.991 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1500</xplan:klassifizMassnahme>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_AusgleichsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Wasserrecht gml:id="GML_be90417b-fc6c-4f10-8cae-37dd3e57effe">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566778.099 5932511.171</gml:lowerCorner>
          <gml:upperCorner>567470.445 5932779.633</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Hochwassergefährdeter Bereich</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_af148c04-d4bc-4965-b920-b9e02ec6b0ca" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_925497A7-DE6C-4EC9-A6CE-DC74D5A5A9FB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566933.255 5932760.356 566885.545 5932770.145 566839.305 5932779.633 
566833.794 5932737.623 566778.099 5932746.09 566806.421 5932681.101 
566811.81 5932668.737 566840.983 5932663.961 566843.323 5932658.461 
566895.719 5932649.044 566896.697 5932653.919 566902.609 5932652.886 
566907.946 5932651.955 566913.856 5932650.923 566914.841 5932650.751 
566940.257 5932645.268 566983.267 5932635.989 567033.314 5932624.936 
567033.805 5932624.821 567064.323 5932617.69 567098.432 5932609.719 
567108.539 5932607.222 567105.418 5932594.628 567118.038 5932591.501 
567121.133 5932604.111 567131.935 5932601.442 567131.074 5932598.151 
567150.386 5932592.949 567176.469 5932586.088 567177.347 5932589.328 
567260.753 5932566.4 567316.551 5932549.955 567366.914 5932533.678 
567429.82 5932512.141 567432.959 5932511.171 567436.534 5932521.988 
567470.445 5932624.6 567374.73 5932651.63 567189.373 5932700.56 
567084.333 5932725.96 566933.255 5932760.356 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>20002</xplan:artDerFestlegung>
    </xplan:SO_Wasserrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3f319409-48c8-4375-b7de-fc41f83f9a2e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566950.032 5932567.636</gml:lowerCorner>
          <gml:upperCorner>566950.032 5932567.636</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_26.03.07</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:position>
        <gml:Point gml:id="Gml_4661F43B-7260-44D8-ABEF-D279E4EB85BD" srsName="EPSG:25832">
          <gml:pos>566950.032 5932567.636</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5615e2e3-d58e-4a20-96df-8610e70a44ce">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566876.697 5932661.614</gml:lowerCorner>
          <gml:upperCorner>566876.697 5932661.614</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_13.09.01</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:position>
        <gml:Point gml:id="Gml_C58085CA-988D-4288-A271-C211C2AFAF9F" srsName="EPSG:25832">
          <gml:pos>566876.697 5932661.614</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">350</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9ead2ab7-55ef-457c-9d21-07d267fe7780">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567226.513 5932560.197</gml:lowerCorner>
          <gml:upperCorner>567226.513 5932560.197</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>HHBplan_05.06.01</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_44403280-26ad-4e62-b59b-21a719a4219f" />
      <xplan:position>
        <gml:Point gml:id="Gml_C1441EFC-7082-480D-94A5-851CBF792C7E" srsName="EPSG:25832">
          <gml:pos>567226.513 5932560.197</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
</xplan:XPlanAuszug>
