# Selenium Tests

Die Tests in diesem Projekt sind nicht gedacht, um direkt als Teil vom Build ausgeführt zu werden sondern eher zu einem späteren Zeitpunkt, wenn die Anwendungen schon deployt worden sind.

Bei einer lokale Ausführung der Tests muss der Webbrowser "Google Chrome" installiert sein.

## Ausführung mit Maven

```
mvn clean integration-test -Psystem-tests \
  -DXPLAN_API_USERNAME=xplanbox -DXPLAN_API_PASSWORD='pwd' \
  -DXPLAN_DOKUMENTEAPI_URL_PUBLIC=https://xplanbox.lat-lon.de/xplan-dokumente-api \
  -DXPLAN_MANAGERAPI_URL_PUBLIC=https://xplanbox.lat-lon.de/xplan-manager-api \ 
  -DXPLAN_VALIDATORAPI_URL_PUBLIC=https://xplanbox.lat-lon.de/xplan-validator-api \ 
  -DXPLAN_VALIDATOR_WEB_BASE_URL=https://xplanbox.lat-lon.de/xplan-validator-web \
  -DXPLAN_VALIDATOR_WEB_USERNAME=xplanbox -DXPLAN_VALIDATOR_WEB_PASSWORD='PWD'
```

## Ausführung im Docker Container

Die Tests können in einem Docker Container ausgeführt werden

```
docker run --env XPLAN_VALIDATOR_WEB_BASE_URL=... xplanbox/xplan-tests-integration
```

### Umgebungsvariablen

#### APIs
- `XPLAN_VALIDATORAPI_URL_PUBLIC`: die Url der XPlanValidatorAPI Anwendung (Default: `http://localhost:8085/xplan-validator-api`)
- `XPLAN_MANAGERAPI_URL_PUBLIC`: die Url von der XPlanManagerAPI Anwendung (Default: `http://localhost:8086/xplan-manager-api`)
- `XPLAN_DOKUMENTEAPI_URL_PUBLIC`: die Url der XPlanDokumenteAPI Anwendung (Default: `http://localhost:8092/xplan-dokumente-api`)
- `XPLAN_API_USERNAME`: Nutzername für den Zugriff auf die XPlanValidatorAPI, XPlanManagerAPI und XPlanDokumenteAPI (Default: keine Absicherung)
- `XPLAN_API_PASSWORD`: Passwort für den Zugriff auf die XPlanValidatorAPI, XPlanManagerAPI und XPlanDokumenteAPI (Default: keine Absicherung)

#### XPlanValidatorWeb
- `XPLAN_VALIDATOR_WEB_BASE_URL`:  die Url des XPlanValidatorWeb (Default: `http://localhost:8081/xplan-validator-web`)
- `XPLAN_VALIDATOR_WEB_USERNAME`: Nutzername für den Zugriff auf den XPlanValidatorWeb (Default: keine Absicherung)
- `XPLAN_VALIDATOR_WEB_PASSWORD`: Password für den Zugriff auf den XPlanValidatorWeb (Default: keine Absicherung)

#### Rabbit
- `SKIP_RABBIT_TESTS`: wenn `true` oder `1` werden die Tests ignoriert, für die eine Verbindung zum RabbitMQ Server benötigt wird.
- `XPLAN_RABBIT_HOST`: der Host vom RabbitMQ Server (Default: `localhost`)
- `XPLAN_RABBIT_PORT`: der Port vom RabbitMQ Server (Default: `5672`)
- `XPLAN_RABBIT_VIRTUALHOST`: der Virtual Host auf dem RabbitMQ Server (Default: ``)
- `XPLAN_RABBIT_USER`: der Benutzername für die Verbindung zum  RabbitMQ Server (Default: `guest`)
- `XPLAN_RABBIT_PASSWORD`: das Passwort für die Verbindung zum  RabbitMQ Server (Default: `guest`)
- `XPLAN_RABBIT_PUBLIC_TOPIC`: der Name vom Topic Exchange (Default: `xplanbox-public`)
- `XPLAN_RABBIT_PUBLIC_TOPIC_ROUTING_PREFIX`: der Präfix für die verwendeten Routing Keys (Default: ``)

Der Report im PDF Format kann zu einem S3 Bucket hochgeladen werden, dafür müssen folgende Umgebungsvariablen gesetzt werden:

- `XPLAN_UPLOAD_TEST_REPORT`: muss auf `true` gesetzt werden
- `XPLAN_S3_ENDPOINT`: die S3 Url
- `XPLAN_S3_REPORT_ID` (optional): ein Id, dass im S3-Objektname verwendet werden soll
- `XPLAN_S3_REPORT_PATH`(optional): der Pfad im S3 Bucket (default: `test-reports`)
- `XPLAN_S3_ACCESS_KEY`: der S3-Zugangschlüssel
- `XPLAN_S3_SECRET_ACCESS_KEY`: der S3-Geheimzugangschlüssel
- `XPLAN_S3_REGION`: die S3 Region
- `XPLAN_S3_BUCKET_ATTACHMENTS`: der Name des Buckets

Der Report kann aus S3 lokal kopiert werden, z,B. mit:

	aws s3 cp s3://my-bucket/test-reports/report-2023-05-26T08:57:15.pdf report.pdf --endpoint-url https://the.s3.url

Eine Notification kann nach der Ausführung der Tests zu einem Slack Chanel geschickt werden. Dafür müssen folgende Umgebungsvariablen gesetzt werden:

- `XPLAN_NOTIFY_SLACK_CHANNEL`: der Slack Kanal
- `XPLAN_NOTIFY_SLACK_TOKEN`: das Slack Authorisierungstoken
