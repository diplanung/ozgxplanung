/*-
 * #%L
 * xplan-tests-integration - Modul zur Gruppierung aller Module zum testen
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.tests.rabbit.manager;

import java.nio.file.Path;
import java.util.List;

import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 8.0
 */
public interface ManagerApiV2 {

	record StatusResponse(String status, List<ManagerApiV2.Link> links) {
	}

	record Link(String expirationDate, String href, String type, String hreflang, String title, String length,
			String rel) {
	}

	record ValidateResponse(String uuid, ManagerApiV2.Link statusLink) {
	}

	@RequestLine("GET /api/v2/status/{uuid}")
	ManagerApiV2.StatusResponse status(@Param("uuid") String uuid);

	@RequestLine("POST /api/v2/plan")
	@Headers("Content-Type: application/zip")
	ManagerApiV2.ValidateResponse importPlan(@Param("bodyFile") Path f);

}
