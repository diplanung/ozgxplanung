/*-
 * #%L
 * xplan-tests-integration - Modul zur Gruppierung aller Module zum testen
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.tests.validatorapi;

import java.nio.file.Path;
import java.util.List;

import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * Simple interface for the API methods currently tested. All paths relative to
 * "/xplan-validator-api"
 *
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
public interface ValidatorApi {

	public record StatusResponse(String status, List<Link> links) {
	}

	public record Link(String expirationDate, String href, String type, String hreflang, String title, String length,
			String rel) {
	}

	public record ValidateResponse(String uuid, Link statusLink) {
	}

	@RequestLine("GET /api/v2/status/{uuid}")
	StatusResponse v2Status(@Param("uuid") String uuid);

	@RequestLine("POST /api/v2/validate")
	@Headers("Content-Type: application/zip")
	ValidateResponse v2Validate(@Param("bodyFile") Path f);

	static record TraceInfo(String traceId, String spanId) {
	};

	@RequestLine("GET /internal/api/traceInfo")
	@Headers("traceparent: {traceparent}")
	TraceInfo traceinfo(@Param("traceparent") String traceparent);

}
