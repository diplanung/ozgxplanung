/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.tests.managerapi;

import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * Simple interface for the API methods currently tested.
 *
 * @since 8.0
 */
public interface ManagerApi {

	static record TraceInfo(String traceId, String spanId) {
	};

	@RequestLine("GET /internal/api/traceInfo")
	@Headers("traceparent: {traceparent}")
	TraceInfo traceinfo(@Param("traceparent") String traceparent);

}
