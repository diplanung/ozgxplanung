/*-
 * #%L
 * xplan-tests-integration - Modul zur Gruppierung aller Module zum testen
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.tests.rabbit;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import de.latlon.xplanbox.tests.TestConfig.RabbitConfig;
import feign.RequestTemplate;
import feign.codec.EncodeException;
import feign.codec.Encoder;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public abstract class AbstractRabbitITExecution {

	public record ReceivedPublicEvent(String routingKey, String message) {
	}

	protected List<ReceivedPublicEvent> receivedPublicEvents = Collections.synchronizedList(new ArrayList<>());

	protected List<ReceivedPublicEvent> receivedEventsFor(String uuid) {
		return receivedPublicEvents.stream() //
			.filter((it) -> it.message.contains(uuid)) //
			.collect(Collectors.toList());
	}

	protected void startRabbitPublicEventsReception(RabbitConfig rabbitConfig) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(rabbitConfig.rabbitHost());
		factory.setPort(rabbitConfig.rabbitPort());
		factory.setVirtualHost(rabbitConfig.rabbitVirtualHost());
		factory.setUsername(rabbitConfig.rabbitUsername());
		factory.setPassword(rabbitConfig.rabbitPassword());
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		String queueName = channel.queueDeclare("TODO-xplan-test", false, true, true, Collections.emptyMap())
			.getQueue();
		channel.queueBind(queueName, rabbitConfig.publicTopicName(), "#");

		DeliverCallback deliverCallback = (consumerTag, delivery) -> {
			String message = new String(delivery.getBody(), "UTF-8");
			String routingKey = delivery.getEnvelope().getRoutingKey();
			receivedPublicEvents.add(new ReceivedPublicEvent(routingKey, message));
		};
		channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
		});
	}

	// encode @Param("bodyFile") to request body bytes
	public static class SimpleBodyFileEncoder implements Encoder {

		public void encode(Object object, Type type, RequestTemplate template) throws EncodeException {
			@SuppressWarnings("unchecked")
			Map<String, ?> params = (Map<String, ?>) object;
			Path file = (Path) params.get("bodyFile");
			byte[] bytes;
			try {
				bytes = Files.readAllBytes(file);
			}
			catch (IOException e) {
				throw new RuntimeException(e);
			}
			template.body(bytes, StandardCharsets.UTF_8);
		}

	}

	protected void waitingIfNeeded(int maxWaitInSeconds, Callable<?> callable) throws Exception {
		long maxWaitTime = System.currentTimeMillis() + 1_000 * maxWaitInSeconds;
		while (true) {
			try {
				callable.call();
				return;
			}
			catch (Throwable t) {
				if (System.currentTimeMillis() > maxWaitTime) {
					throw t;
				}
				Thread.sleep(1_000);
			}
		}

	}

}
