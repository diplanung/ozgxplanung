/*-
 * #%L
 * xplan-tests-integration - Modul zur Gruppierung aller Module zum testen
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.tests.rabbit.validator;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Path;
import java.nio.file.Paths;

import de.latlon.xplanbox.tests.FeignUtils;
import de.latlon.xplanbox.tests.TestConfig;
import de.latlon.xplanbox.tests.rabbit.AbstractRabbitITExecution;
import de.latlon.xplanbox.tests.validatorapi.ValidatorApi;
import de.latlon.xplanbox.tests.validatorapi.ValidatorApi.StatusResponse;
import de.latlon.xplanbox.tests.validatorapi.ValidatorApi.ValidateResponse;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
@DisabledIfEnvironmentVariable(named = "SKIP_RABBIT_TESTS", matches = "(1|true)")
class ValidationPublicEventsIT extends AbstractRabbitITExecution {

	private final TestConfig testConfig = TestConfig.getTestConfig();

	@Test
	void validateAndVerifyPublicEvents() throws Exception {
		ValidatorApi validatorApi = Feign.builder()
			.client(FeignUtils.unsecuredClient())
			.encoder(new SimpleBodyFileEncoder())
			.decoder(new JacksonDecoder())
			.target(ValidatorApi.class, testConfig.validatorApiBaseUrl());

		startRabbitPublicEventsReception(testConfig.rabbitConfig());

		Path f = Paths.get(getClass().getResource("/BPlan001_5-4.zip").toURI());
		ValidateResponse r = validatorApi.v2Validate(f);

		assertThat(r.uuid()).isNotBlank();
		assertThat(r.statusLink()).isNotNull();

		// verify status (probably not yet finished but who knows...)
		StatusResponse statusResponse = validatorApi.v2Status(r.uuid());
		assertThat(statusResponse.status()).isIn("VALIDATION_REQUESTED", "VALIDATION_FINISHED");

		// awaiting 8 events for this uuid
		ReceivedPublicEvent expectedEventValidationStarted = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"VALIDATION_STARTED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":null,\"error\":null}");

		ReceivedPublicEvent expectedEventSynValidationStarted = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.syntaktisch",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"SYNTACTIC_VALIDATION_STARTED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":null,\"error\":null}");
		ReceivedPublicEvent expectedEventSynValidationFinished = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.syntaktisch",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"SYNTACTIC_VALIDATION_FINISHED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":true,\"error\":null}");

		ReceivedPublicEvent expectedEventRefValidationStarted = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.referenzen",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"REFERENCES_STARTED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":null,\"error\":null}");
		ReceivedPublicEvent expectedEventRefValidationFinished = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.referenzen",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"REFERENCES_FINISHED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":true,\"error\":null}");

		ReceivedPublicEvent expectedEventGeomValidationStarted = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.geometrisch",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"GEOMETRIC_VALIDATION_STARTED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":null,\"error\":null}");
		ReceivedPublicEvent expectedEventGeomValidationFinished = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.geometrisch",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"GEOMETRIC_VALIDATION_FINISHED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":true,\"error\":null}");

		ReceivedPublicEvent expectedEventSemValidationStarted = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.semantisch",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"SEMANTIC_VALIDATION_STARTED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":null,\"error\":null}");
		ReceivedPublicEvent expectedEventSemValidationFinished = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.semantisch",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"SEMANTIC_VALIDATION_FINISHED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":true,\"error\":null}");

		ReceivedPublicEvent expectedEventValidationFinished = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"VALIDATION_FINISHED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":true,\"error\":null}");
		waitingIfNeeded(20,
				() -> assertThat(receivedEventsFor(r.uuid())).containsExactly(expectedEventValidationStarted,
						expectedEventSynValidationStarted, expectedEventSynValidationFinished,
						expectedEventRefValidationStarted, expectedEventRefValidationFinished,
						expectedEventGeomValidationStarted, expectedEventGeomValidationFinished,
						expectedEventSemValidationStarted, expectedEventSemValidationFinished,
						expectedEventValidationFinished));

		// verify status
		statusResponse = validatorApi.v2Status(r.uuid());
		// now surely finished
		assertThat(statusResponse.status()).isEqualTo("VALIDATION_FINISHED");
	}

}
