/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.tests;

/**
 * Configuration for integration tests.
 *
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
public record TestConfig(String dokumenteApiBaseUrl, String managerApiBaseUrl, String validatorApiBaseUrl,
		String apiUsername, String apiPassword, String validatorWebBaseUrl, String validatorWebUsername,
		String validatorWebPassword, RabbitConfig rabbitConfig) {

	public record RabbitConfig(String rabbitHost, int rabbitPort, String rabbitVirtualHost, String rabbitUsername,
			String rabbitPassword, String publicTopicName, String publicTopicRoutingPrefix) {
	};

	public static TestConfig getTestConfig() {
		String dokumenteApiBaseUrl = getEnv("XPLAN_DOKUMENTEAPI_URL_PUBLIC",
				"http://localhost:8092/xplan-dokumente-api");
		String managerApiBaseUrl = getEnv("XPLAN_MANAGERAPI_URL_PUBLIC", "http://localhost:8086/xplan-manager-api");
		String validatorApiBaseUrl = getEnv("XPLAN_VALIDATORAPI_URL_PUBLIC",
				"http://localhost:8085/xplan-validator-api");
		String validatorWebBaseUrl = getEnv("XPLAN_VALIDATOR_WEB_BASE_URL",
				"http://localhost:8081/xplan-validator-web");

		String apiUsername = getEnv("XPLAN_API_USERNAME", null);
		String apiPassword = getEnv("XPLAN_API_PASSWORD", null);

		String validatorWebUsername = getEnv("XPLAN_VALIDATOR_WEB_USERNAME", null);
		String validatorWebPassword = getEnv("XPLAN_VALIDATOR_WEB_PASSWORD", null);

		String rabbitHost = getEnv("XPLAN_RABBIT_HOST", "localhost");
		int rabbitPort = Integer.parseInt(getEnv("XPLAN_RABBIT_PORT", "5672"), 10);
		String rabbitVirtualHost = getEnv("XPLAN_RABBIT_VIRTUALHOST", "");
		String rabbitUsername = getEnv("XPLAN_RABBIT_USER", "guest");
		String rabbitPassword = getEnv("XPLAN_RABBIT_PASSWORD", "guest");
		String publicTopicName = getEnv("XPLAN_RABBIT_PUBLIC_TOPIC", "latlon.public");
		String publicTopicRoutingPrefix = getEnv("XPLAN_RABBIT_PUBLIC_TOPIC_ROUTING_PREFIX", "");

		RabbitConfig rabbitConfig = new RabbitConfig(rabbitHost, rabbitPort, rabbitVirtualHost, rabbitUsername,
				rabbitPassword, publicTopicName, publicTopicRoutingPrefix);
		return new TestConfig(dokumenteApiBaseUrl, managerApiBaseUrl, validatorApiBaseUrl, apiUsername, apiPassword,
				validatorWebBaseUrl, validatorWebUsername, validatorWebPassword, rabbitConfig);
	}

	private static String getEnv(String key, String defaultValue) {
		String value = System.getenv(key);
		if (value == null || value.isBlank()) {
			value = System.getProperty(key, defaultValue);
		}
		return value;
	}
}
