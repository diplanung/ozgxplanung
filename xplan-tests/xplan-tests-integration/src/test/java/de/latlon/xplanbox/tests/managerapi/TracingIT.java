/*-
 * #%L
 * xplan-validator-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.tests.managerapi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import de.latlon.xplanbox.tests.FeignUtils;
import de.latlon.xplanbox.tests.TestConfig;
import de.latlon.xplanbox.tests.managerapi.ManagerApi.TraceInfo;
import feign.Feign;
import feign.auth.BasicAuthRequestInterceptor;
import feign.jackson.JacksonDecoder;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
class TracingIT {

	@Test
	void validateTraceIdAvailableInLog4jThreadContext() throws Exception {
		TestConfig testConfig = TestConfig.getTestConfig();
		BasicAuthRequestInterceptor interceptor = testConfig.apiUsername() != null
				? new BasicAuthRequestInterceptor(testConfig.apiUsername(), testConfig.apiPassword()) : null;
		ManagerApi api = Feign.builder()
			.client(FeignUtils.unsecuredClient())
			.decoder(new JacksonDecoder())
			.requestInterceptor(interceptor)
			.target(ManagerApi.class, testConfig.managerApiBaseUrl());

		TraceInfo traceInfo1 = api.traceinfo("00-5ba1163f06d429e9e8dd760c43db3fe8-49a43a5e6e59ea22-00");
		assertEquals("5ba1163f06d429e9e8dd760c43db3fe8", traceInfo1.traceId());

		TraceInfo traceInfo2 = api.traceinfo("00-3f5bf112d19161544a178fdec98e37d6-55d05878040099f8-00");
		assertEquals("3f5bf112d19161544a178fdec98e37d6", traceInfo2.traceId());
	}

}
