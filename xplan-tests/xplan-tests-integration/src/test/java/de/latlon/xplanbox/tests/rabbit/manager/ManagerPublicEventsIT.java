/*-
 * #%L
 * xplan-tests-integration - Modul zur Gruppierung aller Module zum testen
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.tests.rabbit.manager;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Path;
import java.nio.file.Paths;

import de.latlon.xplanbox.tests.FeignUtils;
import de.latlon.xplanbox.tests.TestConfig;
import de.latlon.xplanbox.tests.rabbit.AbstractRabbitITExecution;
import feign.Feign;
import feign.auth.BasicAuthRequestInterceptor;
import feign.jackson.JacksonDecoder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 8.0
 */
@DisabledIfEnvironmentVariable(named = "SKIP_RABBIT_TESTS", matches = "(1|true)")
class ManagerPublicEventsIT extends AbstractRabbitITExecution {

	private TestConfig testConfig = TestConfig.getTestConfig();

	@Test
	void importAndVerifyPublicEvents() throws Exception {
		ManagerApiV2 v2Api = Feign.builder()
			.client(FeignUtils.unsecuredClient())
			// .logger(new Logger.ErrorLogger())
			// .logLevel(Level.FULL)
			.requestInterceptor(new BasicAuthRequestInterceptor(testConfig.apiUsername(), testConfig.apiPassword()))
			.encoder(new SimpleBodyFileEncoder())
			.decoder(new JacksonDecoder())
			.target(ManagerApiV2.class, testConfig.managerApiBaseUrl());

		startRabbitPublicEventsReception(testConfig.rabbitConfig());

		Path f = Paths.get(getClass().getResource("/BPlan001_5-4.zip").toURI());
		ManagerApiV2.ValidateResponse r = v2Api.importPlan(f);

		assertThat(r.uuid()).isNotBlank();
		assertThat(r.statusLink()).isNotNull();

		// verify status (probably not yet finished but who knows...)
		ManagerApiV2.StatusResponse statusResponse = v2Api.status(r.uuid());
		assertThat(statusResponse.status()).isIn("VALIDATION_REQUESTED", "VALIDATION_FINISHED", "IMPORT_REQUESTED",
				"IMPORT_FINISHED");

		// awaiting 10 events for this uuid
		ReceivedPublicEvent expectedEventValidationStarted = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"VALIDATION_STARTED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":null,\"error\":null}");

		ReceivedPublicEvent expectedEventSynValidationStarted = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.syntaktisch",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"SYNTACTIC_VALIDATION_STARTED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":null,\"error\":null}");
		ReceivedPublicEvent expectedEventSynValidationFinished = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.syntaktisch",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"SYNTACTIC_VALIDATION_FINISHED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":true,\"error\":null}");

		ReceivedPublicEvent expectedEventRefValidationStarted = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.referenzen",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"REFERENCES_STARTED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":null,\"error\":null}");
		ReceivedPublicEvent expectedEventRefValidationFinished = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.referenzen",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"REFERENCES_FINISHED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":true,\"error\":null}");

		ReceivedPublicEvent expectedEventGeomValidationStarted = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.geometrisch",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"GEOMETRIC_VALIDATION_STARTED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":null,\"error\":null}");
		ReceivedPublicEvent expectedEventGeomValidationFinished = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.geometrisch",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"GEOMETRIC_VALIDATION_FINISHED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":true,\"error\":null}");

		ReceivedPublicEvent expectedEventSemValidationStarted = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.semantisch",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"SEMANTIC_VALIDATION_STARTED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":null,\"error\":null}");
		ReceivedPublicEvent expectedEventSemValidationFinished = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation.semantisch",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"SEMANTIC_VALIDATION_FINISHED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":true,\"error\":null}");

		ReceivedPublicEvent expectedEventValidationFinished = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix() + "xplanbox.validation",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"VALIDATION_FINISHED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":true,\"error\":null}");

		ReceivedPublicEvent expectedEventImportStarted = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix()
						+ "routing.suffix.for.import-started.to.be.determined",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"IMPORT_STARTED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":true,\"error\":null}");

		ReceivedPublicEvent expectedEventImportFinished = new ReceivedPublicEvent(
				testConfig.rabbitConfig().publicTopicRoutingPrefix()
						+ "routing.suffix.for.import-finished.to.be.determined",
				"{\"apiVersion\":\"1.0\",\"eventType\":\"IMPORT_FINISHED\",\"uuid\":\"" + r.uuid()
						+ "\",\"valid\":true,\"error\":null}");

		waitingIfNeeded(30,
				() -> assertThat(receivedEventsFor(r.uuid())).containsExactly(expectedEventValidationStarted,
						expectedEventSynValidationStarted, expectedEventSynValidationFinished,
						expectedEventRefValidationStarted, expectedEventRefValidationFinished,
						expectedEventGeomValidationStarted, expectedEventGeomValidationFinished,
						expectedEventSemValidationStarted, expectedEventSemValidationFinished,
						expectedEventValidationFinished, expectedEventImportStarted, expectedEventImportFinished));

		// verify status
		statusResponse = v2Api.status(r.uuid());
		// now surely finished
		assertThat(statusResponse.status()).isEqualTo("IMPORT_FINISHED");
	}

}
