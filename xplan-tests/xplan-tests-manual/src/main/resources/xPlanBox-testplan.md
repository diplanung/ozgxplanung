# **Testplan der xPlanBox**

# Inhaltsverzeichnis 

## [XPlanManagerWeb](#komponente-XPlanManagerWeb) 
* [Plan-Funktion: Validieren](#Plan-Funktion-Validieren) 
* [Plan-Funktion: Hinzufügen](#Plan-Funktion-Hinzufügen) 
* [Plan-Funktion: Import](#Plan-Funktion-Import) 
* [Plan-Liste](#Plan-Liste) 
* [Plan-Funktion: Kartenvorschau](#Plan-Funktion-Kartenvorschau) 
* [Plan-Funktion: Plan publizieren (Transformation nach INSPIRE PLU)](#Plan-Funktion-Plan-publizieren-Transformation-nach-INSPIRE-PLU) 
* [Plan-Funktion: Editieren](#Plan-Funktion-Editieren) 
* [Plan-Funktion: Export](#Plan-Funktion-Export) 
* [Plan-Funktion: Entfernen](#Plan-Funktion-Entfernen) 
* [XPlanManager-Funktion: Anzeigefilter](#XPlanManager-Funktion-Anzeigefilter) 


## [XPlanValidatorWeb](#komponente-XPlanValidatorWeb) 
* [Ausgabe von Syntaxfehlern](#Ausgabe-von-Syntaxfehlern) 
* [Webschnittstelle XPlanValidator](#Webschnittstelle-XPlanValidator) 
* [Planarchiv auswählen](#Planarchiv-auswählen) 
* [Eingabe einer Bezeichnung für den Validierungsdurchlauf](#Eingabe-einer-Bezeichnung-für-den-Validierungsdurchlauf) 
* [Auswahl eines Validierungstyps](#Auswahl-eines-Validierungstyps) 
* [Auswahl eines Profils](#Auswahl-eines-Profils) 
* [Validierung starten und abbrechen](#Validierung-starten-und-abbrechen) 
* [Download der Validierungsergebnisse](#Download-der-Validierungsergebnisse) 
* [Schaltfläche um einen weiteren Plan zu validieren](#Schaltfläche-um-einen-weiteren-Plan-zu-validieren) 


## [XPlanWMS](#komponente-XPlanWMS) 
* [Transparente Zeichenvorschriften im XPlanWMS ermöglichen](#Transparente-Zeichenvorschriften-im-XPlanWMS-ermöglichen) 
* [Unterstützung von Planarchivierung](#Unterstützung-von-Planarchivierung) 
* [GetMap URL für spezifischen Plan über GetFeature zusammenstellen](#GetMap-URL-für-spezifischen-Plan-über-GetFeature-zusammenstellen) 
* [Sortierung der Visualisierung nach anderem Datumsfeld](#Sortierung-der-Visualisierung-nach-anderem-Datumsfeld) 
* [GetFeatureInfo-Ausgaben des WMS](#GetFeatureInfo-Ausgaben-des-WMS) 
* [Visualisierung von importierten XPlanGML-Rasterdaten](#Visualisierung-von-importierten-XPlanGML-Rasterdaten) 


## [Automatisierte SoapUI-Tests](#komponente-Automatisierte-SoapUI-Tests) 
* [Ausführung der automatisierten SoapUI-Tests](#Ausführung-der-automatisierten-SoapUI-Tests) 


## [XPlanCLI](#komponente-XPlanCLI) 
* [XPlanCLI - Hilfe aufrufen](#XPlanCLI---Hilfe-aufrufen) 
* [XPlanCLI - Command 'validate' - Hilfe aufrufen](#XPlanCLI---Command-validate---Hilfe-aufrufen) 
* [XPlanCLI - Command 'validate' - Subcommand 'file' - Hilfe aufrufen](#XPlanCLI---Command-validate---Subcommand-file---Hilfe-aufrufen) 
* [XPlanCLI - Command 'validate' - Subcommand 'file' - Ausführung (Validierungsart)](#XPlanCLI---Command-validate---Subcommand-file---Ausführung-Validierungsart) 
* [XPlanCLI - Command 'validate' - Subcommand 'file' - Ausführung (Validierungsoptionen)](#XPlanCLI---Command-validate---Subcommand-file---Ausführung-Validierungsoptionen) 
* [XPlanCLI - Command 'validate' - Subcommand 'file' - Ablage der Validierungsergebnisse](#XPlanCLI---Command-validate---Subcommand-file---Ablage-der-Validierungsergebnisse) 
* [XPlanCLI - Command 'validate' - Subcommand 'db' - Hilfe aufrufen](#XPlanCLI---Command-validate---Subcommand-db---Hilfe-aufrufen) 
* [XPlanCLI - Command 'validate' - Subcommand 'db' - Ausführung](#XPlanCLI---Command-validate---Subcommand-db---Ausführung) 
* [XPlanCLI - Command 'manage' - Hilfe aufrufen](#XPlanCLI---Command-manage---Hilfe-aufrufen) 
* [XPlanCLI - Command 'manage' - Subcommand 'import' - Hilfe aufrufen](#XPlanCLI---Command-manage---Subcommand-import---Hilfe-aufrufen) 
* [XPlanCLI - Command 'manage' - Subcommand 'import' - Ausführung](#XPlanCLI---Command-manage---Subcommand-import---Ausführung) 
* [XPlanCLI - Command 'manage' - Subcommand 'list' - Hilfe aufrufen](#XPlanCLI---Command-manage---Subcommand-list---Hilfe-aufrufen) 
* [XPlanCLI - Command 'manage' - Subcommand 'list' - Ausführung](#XPlanCLI---Command-manage---Subcommand-list---Ausführung) 
* [XPlanCLI - Command 'manage' - Subcommand 'export' - Hilfe aufrufen](#XPlanCLI---Command-manage---Subcommand-export---Hilfe-aufrufen) 
* [XPlanCLI - Command 'manage' - Subcommand 'export' - Ausführung](#XPlanCLI---Command-manage---Subcommand-export---Ausführung) 
* [XPlanCLI - Command 'manage' - Subcommand 'delete' - Hilfe aufrufen](#XPlanCLI---Command-manage---Subcommand-delete---Hilfe-aufrufen) 
* [XPlanCLI - Command 'manage' - Subcommand 'delete' - Ausführung](#XPlanCLI---Command-manage---Subcommand-delete---Ausführung) 
* [XPlanCLI - Command 'admin' - Hilfe aufrufen](#XPlanCLI---Command-admin---Hilfe-aufrufen) 
* [XPlanCLI - Command 'admin' - Subcommand 'evaluation-db-update' - Hilfe aufrufen](#XPlanCLI---Command-admin---Subcommand-evaluation-db-update---Hilfe-aufrufen) 
* [XPlanCLI - Command 'admin' - Subcommand 'evaluation-db-update' - Ausführung](#XPlanCLI---Command-admin---Subcommand-evaluation-db-update---Ausführung) 
* [XPlanCLI - Command 'admin' - Subcommand 'resynthesize' - Hilfe aufrufen](#XPlanCLI---Command-admin---Subcommand-resynthesize---Hilfe-aufrufen) 
* [XPlanCLI - Command 'admin' - Subcommand 'resynthesize' - Ausführung](#XPlanCLI---Command-admin---Subcommand-resynthesize---Ausführung) 
* [XPlanCLI - Command 'admin' - Subcommand 'sortdate-update' - Hilfe aufrufen](#XPlanCLI---Command-admin---Subcommand-sortdate-update---Hilfe-aufrufen) 
* [XPlanCLI - Command 'admin' - Subcommand 'sortdate-update' - Ausführung](#XPlanCLI---Command-admin---Subcommand-sortdate-update---Ausführung) 


## [DB-Aktualisierung](#komponente-DB-Aktualisierung) 
* [Ausführung der SQL-Skripte zur Aktualisierung des Datenbankschemas](#Ausführung-der-SQL-Skripte-zur-Aktualisierung-des-Datenbankschemas) 
* [(Optional) Ausführen des Kommandozeilenwerkzeug reSynthesizer](#Optional-Ausführen-des-Kommandozeilenwerkzeug-reSynthesizer) 
* [(Optional) Ausführen des Kommandozeilenwerkzeug EvaluationSchemaSynchronizer](#Optional-Ausführen-des-Kommandozeilenwerkzeug-EvaluationSchemaSynchronizer) 


## Komponente XPlanManagerWeb

### Plan-Funktion: Validieren

#### Testschritt 1:
- **Aktion**: Der Benutzer klickt neben einem hochgeladenen Plan auf den Button Validieren.
- **Erwartetes Ergebnis**: Ein neues Fenster mit dem XPlanValidator öffnet sich.

#### Testschritt 2:
- **Aktion**: Der Benutzer vergibt eine Bezeichnung.
- **Erwartetes Ergebnis**: Die Bezeichnung wird im Feld dargestellt.

#### Testschritt 3:
- **Aktion**: Der Benutzer wählt einen Validierungstyp aus.
- **Erwartetes Ergebnis**: Der Validierungstyp wird im Feld dargestellt.

#### Testschritt 4:
- **Aktion**: Der Benutzer wählt ein Profil aus.
- **Erwartetes Ergebnis**: Das Profil wird im Feld dargestellt.

#### Testschritt 5:
- **Aktion**: Der Benutzer startet die Validierung.
- **Erwartetes Ergebnis**: Das Validierungsergebnis wird dargestellt.

#### Testschritt 6:
- **Aktion**: Der Benutzer kehrt zu der Seite mit dem importierten Plan zurück.
- **Erwartetes Ergebnis**: Die Web-Oberfläche des XPlanManagers wird angezeigt.

#### Testschritt 7:
- **Aktion**: Die Schaltfläche Validieren je nach Ergebnis rot (Validierung fehlgeschlagen).
- **Erwartetes Ergebnis**: Bei erfolgreicher Validierung wird die Schaltfläche Import freigegeben.

#### Testschritt 8:
- **Aktion**: Die Schaltfläche Validieren je nach Ergebnis grün (Validierung erfolgreich) eingefärbt.
- **Erwartetes Ergebnis**: Bei fehlgeschlagener Validierung wird die Schaltfläche Import nicht freigegeben.

### Plan-Funktion: Hinzufügen

#### Testschritt 1:
- **Aktion**: Der Benutzer klickt auf den Button Datei auswählen.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster zur Auswahl eines Planarchivs.

#### Testschritt 2:
- **Aktion**: Der Benutzer wählt eine beliebige Datei (außer ein Plan im Zip-Format) aus klickt auf Öffnen.
- **Erwartetes Ergebnis**: Das Fenster schließt sich. Die beliebige Datei wird zwischengelagert in der Weboberfläche angezeigt.

#### Testschritt 3:
- **Aktion**: Der Benutzer klickt auf den Button Hinzufügen.
- **Erwartetes Ergebnis**: Es wird ein Fenster geöffnet. Die Datei wird abgelehnt und nicht hoch geladen.

#### Testschritt 4:
- **Aktion**: Der Benutzer klickt auf den Button Schließen.
- **Erwartetes Ergebnis**: Das Fenster wird geschlossen.

#### Testschritt 5:
- **Aktion**: Der Benutzer klickt auf den Button Datei auswählen.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster zur Auswahl eines Planarchivs.

#### Testschritt 6:
- **Aktion**: Der Benutzer wählt einen Plan im Zip-Format aus.
- **Erwartetes Ergebnis**: Das Fenster schließt sich. Der Planname wird zwischengelagert in der Weboberfläche angezeigt.

#### Testschritt 7:
- **Aktion**: Der Benutzer klickt auf den Button Hinzufügen.
- **Erwartetes Ergebnis**: Der Plan wird hoch geladen. Nach Ende des Uploads wird ein neues Fenster geöffnet, mit der Meldung, dass der Upload abgeschlossen ist.

#### Testschritt 8:
- **Aktion**: Der Benutzer klickt auf Ok.
- **Erwartetes Ergebnis**: Das Fenster wird geschlossen. Der Plan wird angezeigt.

#### Testschritt 9:
- **Aktion**: Der Benutzer klickt auf Entfernen.
- **Erwartetes Ergebnis**: Es wird ein neues Fenster geöffnet, mit der Meldung, ob der Plan wirklich entfernt werden soll.

#### Testschritt 10:
- **Aktion**: Der Benutzer klickt auf Ok.
- **Erwartetes Ergebnis**: Es wird ein neues Fenster geöffnet, mit der Meldung, dass das Entfernen abgeschlossen ist.

#### Testschritt 11:
- **Aktion**: Der Benutzer klickt auf Ok.
- **Erwartetes Ergebnis**: Es ist kein Plan aufgelistet, welcher zur Validation aussteht.

### Plan-Funktion: Import

#### Testschritt 1:
- **Aktion**: Der Benutzer klickt neben einen syntaktisch, semmantisch oder geometrisch invaliden Plan auf den Button Import.
- **Erwartetes Ergebnis**: Der Button ist deaktiviert und kann nicht angeklickt werden.

#### Testschritt 2:
- **Aktion**: Der Benutzer klickt neben einen validen Plan mit Rasterdaten auf den Button Import.
- **Erwartetes Ergebnis**: Es öffnet sich ein Fenster zur Angabe des Rechtsstands.

#### Testschritt 4:
- **Aktion**: Der Benutzer wählt einen Rechtsstand aus oder klickt ohne Änderung den Button Weiter.
- **Erwartetes Ergebnis**: Es öffnet sich ein weiteres Fenster für die Analyse der Rasterdaten.

#### Testschritt 5:
- **Aktion**: Der Benutzer klickt den Button Weiter mit Rasterdaten, klickt den Button Weiter ohne Rasterdaten oder bricht den Import ab mit dem Button Abbrechen.
- **Erwartetes Ergebnis**: Beim klicken von den Button Weiter mit/ohne Rasterdaten wird Plan importiert, es öffnet sich ein Fenster mit der Meldung, dass der Import abgeschlossen ist.

#### Testschritt 6:
- **Aktion**: Der Benutzer wählt klickt den Button Ok.
- **Erwartetes Ergebnis**: Der Plan wird in der Plan-Liste angezeigt.

#### Testschritt 7:
- **Aktion**: Der Benutzer klickt neben einen validen Plan (im Format *.gml) auf Import.
- **Erwartetes Ergebnis**: Es öffnet sich ein Fenster zur Angabe des Rechtsstands.

#### Testschritt 9:
- **Aktion**: Der Benutzer wählt einen Rechtsstand aus oder klickt ohne Änderung den Button Weiter.
- **Erwartetes Ergebnis**: Beim klicken von des Button Weiter wird der Plan importiert; es öffnet sich ein Fenster mit der Meldung, dass der Import abgeschlossen ist.

#### Testschritt 10:
- **Aktion**: 
Der Benutzer wählt klickt den Button Ok.
- **Erwartetes Ergebnis**: Der Plan wird in der Plan-Liste angezeigt.

### Plan-Liste

#### Testschritt 1:
- **Aktion**: Der Benutzer überprüft die Oberfläche des XPlanManagers auf die in [1] aufgelisteten Punkte.
- **Erwartetes Ergebnis**: Die in [1] aufgelisteten Punkte sind vorhanden.

#### Testschritt 2:
- **Aktion**: Der Benutzer überprüft die tabellarische Anzeige des XPlanManagers auf die in [2] aufgelisteten Punkte.
- **Erwartetes Ergebnis**: Die in [2] aufgelisteten Punkte sind vorhanden.

Befehle 
<p style="box-sizing:border-box;color:rgba(0, 0, 0, 0.9);margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="box-sizing:border-box;font-size:12pt;">[1] Zur Unterstützung des Arbeitsablaufs bietet die Oberfläche dem Nutzer:</span> </p><ul style="box-sizing:border-box;padding:0px 0px 0px 20px;color:rgba(0, 0, 0, 0.9);margin-bottom:0cm;"><li style="box-sizing:border-box;margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="box-sizing:border-box;font-size:12pt;">eine tabellarische Anzeige aller XPlanGML-Dokumente.</span> </li> </ul><p style="box-sizing:border-box;color:rgba(0, 0, 0, 0.9);margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="box-sizing:border-box;font-size:12pt;">[2] Die Liste enthält die folgenden Eigenschaften:</span> </p><ul style="box-sizing:border-box;padding:0px 0px 0px 20px;color:rgba(0, 0, 0, 0.9);margin-bottom:0cm;"><li style="box-sizing:border-box;margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="box-sizing:border-box;font-size:12pt;">Name [sortierfähig]</span> </li><li style="box-sizing:border-box;margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="box-sizing:border-box;font-size:12pt;">ID [sortierfähig]</span> </li><li style="box-sizing:border-box;margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="box-sizing:border-box;font-size:12pt;">Nummer [sortierfähig]</span> </li><li style="box-sizing:border-box;margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="font-size:12pt;">XPlan GML Version [sortierfähig]</span><br> </li><li style="box-sizing:border-box;margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="box-sizing:border-box;font-size:12pt;">Planart [sortierfähig]</span> </li><li style="box-sizing:border-box;margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="box-sizing:border-box;font-size:12pt;">sonstige Planart [sortierfähig]</span> </li><li style="box-sizing:border-box;margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="box-sizing:border-box;font-size:12pt;">Rechtsstand [sortierfähig]</span> </li><li style="box-sizing:border-box;margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="box-sizing:border-box;font-size:12pt;">Datum Veröffentlichung [sortierfähig]</span> </li><li style="box-sizing:border-box;margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="box-sizing:border-box;font-size:12pt;">Datum Import [sortierfähig]</span> </li><li style="box-sizing:border-box;margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="box-sizing:border-box;font-size:12pt;">Planstatus [sortierfähig]</span> </li><li style="box-sizing:border-box;margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="box-sizing:border-box;font-size:12pt;">Gültigkeit (rot, grün)</span> </li><li style="box-sizing:border-box;margin:0cm 0cm 8pt;font-size:11pt;font-family:Calibri, sans-serif;"><span style="box-sizing:border-box;font-size:12pt;">Aktionen: Plan editieren, Kartenvorschau, Plan publizieren, Herunterladen, Entfernen.</span> </li> </ul><br><br> 

### Plan-Funktion: Kartenvorschau

#### Testschritt 1:
- **Aktion**: Der Benutzer klickt neben einen Plan auf den Button Kartenvorschau.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster. Der Plan wird in einer Vorschau angezeigt.

### Plan-Funktion: Plan publizieren (Transformation nach INSPIRE PLU)

#### Testschritt 1:
- **Aktion**: Der Benutzer klickt neben einen Plan auf den Button Plan publizieren.
- **Erwartetes Ergebnis**: Es öffnet sich ein Pop-up, welches den Vorgang bestätigt.

#### Testschritt 2:
- **Aktion**: Der Benutzer geht auf die xPlanBox Landingpage und öffnet die Capabilities der XPlanInspirePluDienste.
- **Erwartetes Ergebnis**: Die Capabilities des XPlanInspirePluWFS und XPlanInspirePluWMS werden erfolgreich angezeigt.

#### Testschritt 3:
- **Aktion**: Der Benutzer testet mit einer Geoinformationssystemssoftware wie z.B. QGIS, ob der in Testschritt 01 publizierte Plan durch die Dienste dargestellt wird.
- **Erwartetes Ergebnis**: Der publizierte Plan wird erfolgreich durch die Dienste dargestellt.

Befehle 
<div><li style="box-sizing:border-box;margin:0px;color:rgba(0, 0, 0, 0.9);">Die xPlanBox wurde mit der INSPIRE PLU Konfiguration aufgesetzt. </li><li style="box-sizing:border-box;outline:none;margin:0px;color:rgba(0, 0, 0, 0.9);">Die Web-basierte Benutzeroberfläche des XPlanManagers ist verfügbar und geöffnet. </li><li style="box-sizing:border-box;margin:0px;color:rgba(0, 0, 0, 0.9);">Der Prüffall-01 wurde erfolgreich ausgeführt. </li> </div> 

### Plan-Funktion: Editieren

#### Testschritt 1:
- **Aktion**: Der Benutzer überprüft die Möglichkeit der Editierbarkeit.
- **Erwartetes Ergebnis**: Hinter den Plänen wird je eine Schaltfläche editieren  angezeigt.

#### Testschritt 2:
- **Aktion**: Der Benutzer drückt auf die Schaltfläche editieren.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster mit einem Formular.

#### Testschritt 3:
- **Aktion**: Der Benutzer editiert die Stammdaten in den Basisdaten valide und bestätigt die Änderung durch die Schaltfläche Speichern.
- **Erwartetes Ergebnis**: Speicherung der Veränderungen.

#### Testschritt 4:
- **Aktion**: Der Benutzer editiert die Stammdaten in den Basisdaten nicht valide und bestätigt die Änderung durch die Schaltfläche Speichern.
- **Erwartetes Ergebnis**: Es öffnet sich eine Fehlermeldung mit dem Vermerk auf den Fehler.

#### Testschritt 5:
- **Aktion**: Der Benutzer folgt den Schritten 01 bis 02 von Prüffall 1.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster mit einem Formular.

#### Testschritt 6:
- **Aktion**: Der Benutzer klickt auf Änderung hinzufügen.
- **Erwartetes Ergebnis**: Es öffnet sich ein Dialog mit einem Formular.

#### Testschritt 7:
- **Aktion**: Der Benutzer editiert die Daten unter Neue Änderung anlegen ohne dabei falsche Eingaben zu machen und bestätigt das Ergebnis durch die Schaltfläche Speichern.
- **Erwartetes Ergebnis**: Die geänderten Daten werden in die Liste der Änderungen übernommen.

#### Testschritt 8:
- **Aktion**: Der Benutzer editiert die Daten unter Neue Änderung anlegen, wobei kein Planname angegeben wird, und bestätigt die Änderung durch die Schaltfläche Speichern.
- **Erwartetes Ergebnis**: Es wird eine Fehlermeldung mit dem Vermerk auf den fehlenden Plannamen angezeigt.

#### Testschritt 9:
- **Aktion**: Der Benutzer fügt einen Plannamen ein und bestätigt die Änderung durch die Schaltfläche Speichern.
- **Erwartetes Ergebnis**: Die geänderten Daten werden in die Liste der Änderungen übernommen.

#### Testschritt 22:
- **Aktion**: Der Benutzer folgt den Schritten 01 bis 02 von Prüffall 1.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster mit einem Formular.

#### Testschritt 23:
- **Aktion**: Der Benutzer klickt auf Text hinzufügen.
- **Erwartetes Ergebnis**: Es öffnet sich ein Dialog mit einem Formular.

#### Testschritt 24:
- **Aktion**: Der Benutzer editiert die Daten unter Neuen Text anlegen ohne dabei falsche Eingaben zu machen und bestätigt das Ergebnis durch die Schaltfläche Speichern.
- **Erwartetes Ergebnis**: Die geänderten Daten werden in die Liste der Texte übernommen.

#### Testschritt 10:
- **Aktion**: Der Benutzer folgt den Schritten 01 bis 02 von Prüffall 1.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster mit einem Formular.

#### Testschritt 11:
- **Aktion**: Der Benutzer folgt unter Dokumente den Schritten 02 und 03 von Prüffall 2 entsprechend.
- **Erwartetes Ergebnis**: Ergebnis siehe Prüffall 2 Schritt 02 und 03.

#### Testschritt 12:
- **Aktion**: Der Benutzer folgt den Schritten 01 bis 02 von Prüffall 1.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster mit einem Formular.

#### Testschritt 13:
- **Aktion**: Der Benutzer editiert die Angaben unter Rasterbasis ohne dabei falsche Eingaben zu machen und bestätigt das Ergebnis durch die Schaltfläche Speichern.
- **Erwartetes Ergebnis**: Speicherung der Veränderungen.

#### Testschritt 14:
- **Aktion**: Der Benutzer folgt den Schritten 01 bis 02 von Prüffall 1.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster mit einem Formular.

#### Testschritt 15:
- **Aktion**: Der Benutzer editiert lediglich das Datum der Rechtsverordnung in den Basisdaten und bestätigt die Änderung durch die Schaltfläche Speichern. Die Eingabe ist valide.
- **Erwartetes Ergebnis**: Die Reihenfolge der aufgelisteten Pläne auf der Web-basierten Benutzeroberfläche des XPlanManagers hat sich verändert.

#### Testschritt 16:
- **Aktion**: Der Benutzer editiert lediglich das Datum der Rechtsverordnung in den Basisdaten und bestätigt die Änderung durch die Schaltfläche Speichern. Die Eingabe ist nicht valide.
- **Erwartetes Ergebnis**: Es wird eine Fehlermeldung mit dem Vermerk auf den Fehler angezeigt.

#### Testschritt 17:
- **Aktion**: Der Benutzer folgt den Schritten 01 bis 02 von Prüffall 1.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster mit einem Formular.

#### Testschritt 18:
- **Aktion**: Der Benutzer editiert lediglich den Rechtsstand in den Basisdaten und bestätigt die Änderung durch die Schaltfläche Speichern.
- **Erwartetes Ergebnis**: Der bearbeitete Plan wird in der entsprechende Datenhaltung abgelegt und nur in der Kartenansicht des entsprechenden WMS-Dienstes angezeigt.

#### Testschritt 19:
- **Aktion**: Der Benutzer folgt den Schritten 01 bis 02 von Prüffall 1.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster mit einem Formular.

#### Testschritt 20:
- **Aktion**: Der Benutzer editiert bzw. löscht beliebig viele Stammdaten und bricht das Editieren durch Abbruch ab.
- **Erwartetes Ergebnis**: Die ursprünglichen Plandaten sind nicht verändert.

#### Testschritt 21:
- **Aktion**: Der Benutzer exportiert einen zuvor bearbeiteten Plan.
- **Erwartetes Ergebnis**: Das exportierte Planarchiv enthält die geänderten Daten.

### Plan-Funktion: Export

#### Testschritt 1:
- **Aktion**: Der Benutzer klickt neben einen Plan auf den Button Herunterladen.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster zur Auswahl des Speicherplatzes.

#### Testschritt 2:
- **Aktion**: Der Benutzer wählt ein Verzeichnis zum Speichern des Plans.
- **Erwartetes Ergebnis**: Das Verzeichnis wird im Fenster dargestellt.

#### Testschritt 3:
- **Aktion**: Der Benutzer klickt auf den Button Speichern.
- **Erwartetes Ergebnis**: Das Fenster schließt sich. Der Plan wird von der Datenbasis lokal gespeichert.

### Plan-Funktion: Entfernen

#### Testschritt 1:
- **Aktion**: Der Benutzer klickt neben einen Plan auf den Button Entfernen.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster zur Bestätigung des Vorgangs.

#### Testschritt 2:
- **Aktion**: Der Benutzer bestätigt mit OK.
- **Erwartetes Ergebnis**: Es öffnet sich ein Dialog mit dem Inhalt "Plan wird entfernt....".

#### Testschritt 3:
- **Aktion**: Der Benutzer wartet während der Löschvorgang durchgeführt wird.
- **Erwartetes Ergebnis**: Das Dialog-Fenster schließt sich. Der Plan wird aus der Datenbasis gelöscht und nicht mehr in der Plan-Liste dargestellt.

#### Testschritt 4:
- **Aktion**: Der Benutzer überprüft mit dem XPlanManager CLI (Parameter: -list), ob der zuvor ausgewählte Plan gelöscht wurde.
- **Erwartetes Ergebnis**: Der zuvor ausgewählte Plan wurde gelöscht.

### XPlanManager-Funktion: Anzeigefilter

#### Testschritt 2:
- **Aktion**: Der Benutzer sucht die Pläne eines bestimmten Planstatus (Suchfilter Planstatus).
- **Erwartetes Ergebnis**: Es werden alle der Suchanfrage entsprechenden Pläne angezeigt.

#### Testschritt 4:
- **Aktion**: Der Benutzer wählt als Suchfilter die Spalte Name und gibt einen Namen(steil) eines in der Planliste angezeigten Plans an.
- **Erwartetes Ergebnis**: Die zuvor angezeigte Liste ist auf die Pläne eingeschränkt, deren Namen mit der Nutzereingabe übereinstimmen.

#### Testschritt 5:
- **Aktion**: Der Benutzer wählt Alle Pläne anzeigen
- **Erwartetes Ergebnis**: Die zuvor gesetzten Auswahlkriterien werden zurückgesetzt, und es werden alle Pläne angezeigt.

[Nach oben](#Testplan-der-xPlanBox)
## Komponente XPlanValidatorWeb

### Ausgabe von Syntaxfehlern

#### Testschritt 1:
- **Aktion**: Der Benutzer klickt auf Datei auswählen.
- **Erwartetes Ergebnis**: Ein neues Fenster öffnet sich.

#### Testschritt 2:
- **Aktion**: Der Benutzer wählt ein ein Planarchiv mit Syntaxfehlern im xplan.gml aus und klickt auf OK.
- **Erwartetes Ergebnis**: Das Fenster schließt sich. Der Planname wird in der Web-basierten Benutzeroberfläche des XPlanValidators angezeigt.

#### Testschritt 3:
- **Aktion**: Der Benutzer startet die Validierung durch das Drücken des Buttons Validierung starten.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster mit dem Ergebnis der Validierung.

#### Testschritt 4:
- **Aktion**: Der Benutzer überprüft die Ausgabe der Syntaxfehler.
- **Erwartetes Ergebnis**: Die Syntaxfehler enthalten Zeilenangaben und den Hinweis, dass das Instanzobjekt nicht zum XPlanGML Schema passt und überprüft werden sollte.

### Webschnittstelle XPlanValidator

#### Testschritt 1:
- **Aktion**: Der Benutzer überprüft, ob die Web-basierte Benutzeroberfläche des XPlanValidators geöffnet ist.
- **Erwartetes Ergebnis**: Die Web-basierte Benutzeroberfläche des XPlanValidators ist geöffnet.

### Planarchiv auswählen

#### Testschritt 1:
- **Aktion**: Der Benutzer überprüft die Web-Schnittstelle (Eingabesicht) des XPlanValidators.
- **Erwartetes Ergebnis**: Die Eingabesicht hat eine Möglichkeit, ein Planarchiv auszuwählen.

#### Testschritt 2:
- **Aktion**: Der Benutzer klickt auf Datei auswählen.
- **Erwartetes Ergebnis**: Ein neues Fenster öffnet sich.

#### Testschritt 3:
- **Aktion**: Der Benutzer wählt ein Planarchiv (*.zip) aus und klickt auf OK.
- **Erwartetes Ergebnis**: Das Fenster schließt sich. Der Planname wird in der Web-basierten Benutzeroberfläche des XPlanValidators angezeigt.

#### Testschritt 4:
- **Aktion**: Der Benutzer wiederholt Schritt 01-04 mit einem Plan im Format *.gml.
- **Erwartetes Ergebnis**: Der Planname wird in der Web-basierten Benutzeroberfläche des XPlanValidators angezeigt.

### Eingabe einer Bezeichnung für den Validierungsdurchlauf

#### Testschritt 1:
- **Aktion**: Der Benutzer klickt auf den Button Hochladen und Validierungsoptionen einstellen.
- **Erwartetes Ergebnis**: Es öffnet sich ein Fenster mit Plan hochladen und dem Namen des ausgewählten Planarchivs.

#### Testschritt 2:
- **Aktion**: Der Benutzer klickt auf Abbrechen.
- **Erwartetes Ergebnis**: Das Fenster schließt sich, das Planarchiv ist aber immer noch ausgewählt.

#### Testschritt 3:
- **Aktion**: Der Benutzer klickt auf den Button Hochladen und Validierungsoptionen einstellen.
- **Erwartetes Ergebnis**: Es öffnet sich ein Fenster mit Plan hochladen und dem Namen des ausgewählten Planarchivs.

#### Testschritt 4:
- **Aktion**: Der Benutzer klickt auf Zur Validierung.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster mit den Validierungsoptionen.

#### Testschritt 5:
- **Aktion**: Der Benutzer überprüft die Web-Schnittstelle (Eingabesicht) des XPlanValidators.
- **Erwartetes Ergebnis**: Die Eingabesicht hat ein Eingabefeld Bezeichnung für den Report

#### Testschritt 6:
- **Aktion**: Der Benutzer gibt eine Bezeichnung in das Eingabefeld ein.
- **Erwartetes Ergebnis**: Im Validierungsbericht, sowie in den Reports, steht unter "Name" die eingegebene Bezeichnung.

### Auswahl eines Validierungstyps

#### Testschritt 1:
- **Aktion**: Der Benutzer überprüft die Web-Schnittstelle (Eingabesicht) des XPlanValidators.
- **Erwartetes Ergebnis**: Die Eingabesicht hat eine Auswahl an Validierungstypen.

#### Testschritt 2:
- **Aktion**: Der Benutzer wählt durch das anklicken eines Kästchens einen Validierungstyp aus.
- **Erwartetes Ergebnis**: Der ausgewählte Validierungstyp wird anhand eines Häckchens im Kästchen angezeigt.

### Auswahl eines Profils

#### Testschritt 1:
- **Aktion**: Der Benutzer überprüft die Web-Schnittstelle (Eingabesicht) des XPlanValidators.
- **Erwartetes Ergebnis**: Die Eingabesicht hat eine Auswahl an Profilen.

#### Testschritt 2:
- **Aktion**: Der Benutzer wählt durch das anklicken eines Kästchens ein Profil aus.
- **Erwartetes Ergebnis**: Das ausgewählte Profil wird anhand eines Häckchens im Kästchen angezeigt.

### Validierung starten und abbrechen

#### Testschritt 1:
- **Aktion**: Der Benutzer klickt auf den Button Validierung abbrechen.
- **Erwartetes Ergebnis**: Die Validierungsoptionen werden geschlossen, das Planarchiv gelöscht.

#### Testschritt 2:
- **Aktion**: Der Benutzer klickt auf den Button Validierung starten.
- **Erwartetes Ergebnis**: Ein neues Fenster mit dem Validierungsergebnis öffnet sich.

### Download der Validierungsergebnisse

#### Testschritt 1:
- **Aktion**: Der Benutzer wählt in der Rubrik Download den HTML-Bericht aus.
- **Erwartetes Ergebnis**: Der HTML-Bericht ist ausgewählt.

#### Testschritt 2:
- **Aktion**: Der Benutzer klickt auf Download.
- **Erwartetes Ergebnis**: Der Bericht wird als Zip-Datei zum Herunterladen angeboten.

#### Testschritt 3:
- **Aktion**: Der Benutzer wählt in der Rubrik Download den PDF-Bericht aus.
- **Erwartetes Ergebnis**: Der PDF-Bericht ist ausgewählt.

#### Testschritt 4:
- **Aktion**: Der Benutzer klickt auf Download.
- **Erwartetes Ergebnis**: Der Report wird als Zip-Datei zum Herunterladen angeboten.

#### Testschritt 15:
- **Aktion**: Der Benutzer wählt in der Rubrik Download den Geometriefehler als GeoJSON aus.
- **Erwartetes Ergebnis**: Der Geometriefehler als GeoJSON ist ausgewählt.

#### Testschritt 16:
- **Aktion**: Der Benutzer klickt auf Download.
- **Erwartetes Ergebnis**: Der Geometriefehler als GeoJSON wird als Zip-Datei zum Herunterladen angeboten.

#### Testschritt 13:
- **Aktion**: Der Benutzer wählt in der Rubrik Download den HTML-Bericht, PDF-Bericht sowie den Geometriefehler als GeoJSON aus.
- **Erwartetes Ergebnis**: Der HTML-Bericht, PDF-Bericht und Geometriefehler als GeoJSON sind ausgewählt.

#### Testschritt 14:
- **Aktion**: Der Benutzer klickt auf Download.
- **Erwartetes Ergebnis**: Die Berichte und der Geometriefehler als GeoJSON werden als Zip-Datei zum Herunterladen angeboten.

### Schaltfläche um einen weiteren Plan zu validieren

#### Testschritt 1:
- **Aktion**: Der Benutzer überprüft die Benutzeroberfläche.
- **Erwartetes Ergebnis**: Die Benutzeroberfläche enthält einen Button weiteren Plan validieren

#### Testschritt 2:
- **Aktion**: Der Benutzer klickt auf den Button weitere Plan validieren.
- **Erwartetes Ergebnis**: Der Benutzer wird auf die Eingabesicht weitergeleitet.

[Nach oben](#Testplan-der-xPlanBox)
## Komponente XPlanWMS

### Transparente Zeichenvorschriften im XPlanWMS ermöglichen

#### Testschritt 1:
- **Aktion**: Der Benutzer führt im Browser eine GetMap-Anfrage aus.
- **Erwartetes Ergebnis**: Die Kartengraphik wird angezeigt.

#### Testschritt 2:
- **Aktion**: Der Benutzer ruft eine GetMap-Anfrage mit dem Style gleich "default" auf.
- **Erwartetes Ergebnis**: Die angezeigte Kartengraphik enthält eine vollflächige Darstellung.

#### Testschritt 3:
- **Aktion**: Der Benutzer ruft eine GetMap-Anfrage mit dem Style gleich "vollflaechig" auf.
- **Erwartetes Ergebnis**: Die angezeigte Kartengraphik enthält eine vollflächige Darstellung.

#### Testschritt 5:
- **Aktion**: Der Benutzer ruft eine GetMap-Anfrage mit dem Style gleich "transparent" auf.
- **Erwartetes Ergebnis**: Die angezeigte Kartengraphik enthält eine transparente Darstellung.

### Unterstützung von Planarchivierung

#### Testschritt 1:
- **Aktion**: Der Benutzer importiert einen Plan.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster.

#### Testschritt 2:
- **Aktion**: Der Benutzer gibt einen Rechtsstand an.
- **Erwartetes Ergebnis**: Der Plan wird in der entsprechenden Datenhaltung abgelegt und nur in der Kartenansicht des entsprechenden WMS-Dienstes angezeigt.

#### Testschritt 3:
- **Aktion**: Der Benutzer führt die Schritte 01 und 02 mit unterschiedlichen Rechtsstand- Angaben durch.
- **Erwartetes Ergebnis**: Der Plan wird in der entsprechenden Datenhaltung abgelegt und nur in der Kartenansicht des entsprechenden WMS-Dienstes angezeigt.

Befehle 
<div><div><div><div><div><ul dir=auto><li>Die Web-basierte Benutzeroberfläche des XPlanManagerWeb ist verfügbar. </li><li>Der Benutzer hat die Berechtigung zum Import von Planarchiven. </li> </ul> </div> </div><br> </div> </div> </div> 

### GetMap URL für spezifischen Plan über GetFeature zusammenstellen

#### Testschritt 1:
- **Aktion**: Der Benutzer klickt hinter einem beliebigen Plan auf die Schaltfläche „Kartenvorschau“.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster.

#### Testschritt 2:
- **Aktion**: Der Benutzer klickt im neu geöffneten Fenster auf „Plan in neuem Fenster öffnen“ (GetMap-Anfrage).
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Browserfenster mit der entsprechenden Karte.

#### Testschritt 3:
- **Aktion**: Der Benutzer überprüft die Form der URL.
- **Erwartetes Ergebnis**: Die URL ist OGC-konform. (http...LAYERS=...)

#### Testschritt 4:
- **Aktion**: Der Benutzer überprüft die Anzahl der Layer in der URL.
- **Erwartetes Ergebnis**: Die Anzahl stimmt mit den im jeweiligen Plan enthaltenen Layern überein.

Befehle 
<div><ul><li>Die Web-basierte Benutzeroberfläche des XPlanManagers ist verfügbar und geöffnet. </li> </ul> </div> 

### Sortierung der Visualisierung nach anderem Datumsfeld

#### Testschritt 1:
- **Aktion**: Der Benutzer importiert einen bereits vorhandene Plan erneut.
- **Erwartetes Ergebnis**: Der Plan wird importiert.

#### Testschritt 2:
- **Aktion**: Der Benutzer drückt auf die Schaltfläche „editieren“ des neu importierten Plans.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster mit einem Formular.

#### Testschritt 3:
- **Aktion**: Der Benutzer verändert das „Rechtsverordungsdatum“ in ein zurückliegendes Datum und verändert ein Attribut. Die Änderung ist valide.
- **Erwartetes Ergebnis**: Die geänderten Daten sind gespeichert.

#### Testschritt 4:
- **Aktion**: Der Benutzer überprüft die Änderung mit Hilfe einer GetMap-Anfrage.
- **Erwartetes Ergebnis**: Der geänderte Plan wird auf der Karte im Hintergrund angezeigt.

#### Testschritt 5:
- **Aktion**: Der Benutzer importiert einen bereits vorhandene Plan erneut.
- **Erwartetes Ergebnis**: Der Plan wird importiert.

#### Testschritt 6:
- **Aktion**: Der Benutzer drückt auf die Schaltfläche „editieren“ des neu importierten Plans.
- **Erwartetes Ergebnis**: Es öffnet sich ein neues Fenster mit einem Formular.

#### Testschritt 7:
- **Aktion**: Der Benutzer verändert das „Rechtsverordungsdatum“ in ein zukünftiges Datum und verändert ein Attribut. Die Änderung erfolgt valide.
- **Erwartetes Ergebnis**: Speicherung der Änderung.

#### Testschritt 8:
- **Aktion**: Der Benutzer überprüft die Änderung mit Hilfe eines GetMap-Anfrage.
- **Erwartetes Ergebnis**: Der geänderte Plan wird auf der Karte im Vordergrund angezeigt.

Befehle 
<div><div><ul><li>Die Web-basierte Benutzeroberfläche des XPlanManagerWeb ist verfügbar. </li><li>Als Kriterium für die Sortierung in der Kartenansicht ist das Rechtsverordnungsdatum eingestellt. </li><li>Der Benutzer hat die Berechtigung zum Editieren von Planstammdaten. </li><li>Es ist bereits mindestens ein Plan importiert. </li> </ul> </div><br> </div> 

### GetFeatureInfo-Ausgaben des WMS

#### Testschritt 1:
- **Aktion**: Der Benutzer wählt die GetFeatureInfo-Funktion in QGIS aus.
- **Erwartetes Ergebnis**: Die GetFeatureInfo-Funktion ist in QGIS ausgewählt.

#### Testschritt 2:
- **Aktion**: Der Benutzer klickt in der Karte auf sichtbares Fachobjekt.
- **Erwartetes Ergebnis**: Ein sichtbares Fachobjekt ist ausgewählt.

#### Testschritt 3:
- **Aktion**: Der Benutzer kontrolliert die Ausgabe auf Lesbarkeit, richtige Wiedergabe der gewünschten Infos und Vollständigkeit.
- **Erwartetes Ergebnis**: Die Ausgabe ist lesbar und es werden die gewünschten Infos wiedergegeben.

### Visualisierung von importierten XPlanGML-Rasterdaten

#### Testschritt 1:
- **Aktion**: Der Benutzer importiert mit dem Befehl in [1] einen Plan mit Rasterdaten in den XPlanManager.
- **Erwartetes Ergebnis**: Der Plan wird in den XPlanManager importiert.

#### Testschritt 2:
- **Aktion**: Der Benutzer lässt sich den importierten Plan mit dem Befehl in [2] auflisten.
- **Erwartetes Ergebnis**: Der importierte Plan wird aufgelistet.

#### Testschritt 3:
- **Aktion**: Der Benutzer führt eine GetMap-Anfrage wie in [3] durch.
- **Erwartetes Ergebnis**: Der importierte Rasterplan wird dargestellt.

[Nach oben](#Testplan-der-xPlanBox)
## Komponente Automatisierte SoapUI-Tests

### Ausführung der automatisierten SoapUI-Tests

#### Testschritt 2:
- **Aktion**: Der Benutzer führt das SoapUI-Projekt xplan-validator-api-soapui-project.xml in SoapUI aus.
- **Erwartetes Ergebnis**: XPlanValidatorAPI TestSuite: Alle Tests werden erfolgreich bestanden. Profiles TestSuite: Alle Test werden bestanden, wenn mindestens ein Profil aktiviert ist.

#### Testschritt 1:
- **Aktion**: Der Benutzer führt das SoapUI-Projekt xplan-manager-api-soapui-project.xml in SoapUI aus.
- **Erwartetes Ergebnis**: XPlanManagerAPI TestSuite: Alle Tests werden erfolgreich bestanden. Profiles TestSuite: Alle Test werden bestanden, wenn mindestens ein Profil aktiviert ist. Codelists TestSuite: Alle Tests werden  bestanden, wenn die angegeben Codeliste konfiguriert ist.

#### Testschritt 5:
- **Aktion**: Der Benutzer führt das SoapUI-Projekt xplan-dokumente-api-soapui-project.xml in SoapUI aus
- **Erwartetes Ergebnis**: Alle Tests werden erfolgreich bestanden.

#### Testschritt 3:
- **Aktion**: Der Benutzer führt das SoapUI-Projekt xplan-webservices-soapui-project.xml in SoapUI aus.
- **Erwartetes Ergebnis**: xplan-wms: Alle Tests werden erfolgreich bestanden, Ausnahme: RESTAPI wird nur bestanden, wenn /config des xplan-wms erreichbar ist.  xplansyn-wfs, xplan-wfs: Alle Tests werden erfolgreich bestanden. xplan-inspireplu: Tests werden nur bestanden, wenn XPlanInspirePLU erreichbar ist. mapserver-wms, mapproxy-wms, mapproxy-wmts: Alle Tests werden erfolgreich bestanden.

[Nach oben](#Testplan-der-xPlanBox)
## Komponente XPlanCLI

### XPlanCLI - Hilfe aufrufen

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer ruft die Hilfe mit dem Befehl in [2] auf.
- **Erwartetes Ergebnis**: Die Ausgabe gibt Auskunft über die verfügbaren 

'commands' des XPlanCLI:

help:      
- Display help information about the specified command.
validate:
- Validate a plan or all plans in a database
manage: 
- Manage plans
admin:
- Administrate xPlanBox

Befehle 
<div><div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] ./xpb help<br> </div> </div> </div> </div> 

### XPlanCLI - Command 'validate' - Hilfe aufrufen

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer ruft die Hilfe des Command 'validate' mit dem Befehl in [2] auf.
- **Erwartetes Ergebnis**: Die Ausgabe gibt Auskunft über die verfügbaren 

'subcommands' des Command 'validate':

help:      
- Display help information about the specified command.
file:
- Validate a XPlanArchive or XPlanGML file.
db: 
- Validate XPlanGML in xPlanBox database.

Befehle 
<div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] ./xpb validate help<br> </div><span></span><br> </div> </div> 

### XPlanCLI - Command 'validate' - Subcommand 'file' - Hilfe aufrufen

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer ruft die Hilfe des Subcommand 'file' von Command 'validate' mit dem Befehl in [2] auf.
- **Erwartetes Ergebnis**: Die Ausgabe gibt Auskunft über die Nutzung des Subcommand 'file':

Usage: xpb validate file -f=&lt;file&gt; [-n=&lt;validationName&gt;] [-o=&lt;option&gt;[,&lt;option&gt;...]]... [-t=&lt;type&gt;[,&lt;type&gt;...]]... [COMMAND]
&nbsp;
Validate a XPlanArchive or XPlanGML file.
  -f, --file=&lt;file&gt;
  -n, --name=&lt;validationName&gt;
  -o, --option=&lt;option&gt;[,&lt;option&gt;...]
validation options, possible values are: skip-flaechenschluss, skip-geltungsbereich, skip-laufrichtung
  -t, --type=&lt;type&gt;[,&lt;type&gt;...]
values: syntax, geometric, semantic

Befehle 
<span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] ./xpb validate file help<br> </div><div><div><div> </div> </div> </div> 

### XPlanCLI - Command 'validate' - Subcommand 'file' - Ausführung (Validierungsart)

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer führt den Befehl [2] mit einem validen Planwerk aus.
- **Erwartetes Ergebnis**: Der Benutzer erhält eine Validierungsausgabe, dass das XPlan-Dokument valide ist.

#### Testschritt 3:
- **Aktion**: Der Benutzer führt den Befehl [2] mit einem invaliden Planwerk aus.
- **Erwartetes Ergebnis**: Der Benutzer erhält eine Validierungsausgabe, dass das XPlan-Dokument invalide ist.

#### Testschritt 4:
- **Aktion**: Der Benutzer führt den Befehl [3] mit einem validen Planwerk aus.
- **Erwartetes Ergebnis**: Der Benutzer erhält eine Validierungsausgabe, dass das XPlan-Dokument syntaktisch valide ist.

#### Testschritt 5:
- **Aktion**: Der Benutzer führt den Befehl [3] mit einem syntaktisch invaliden Planwerk aus.
- **Erwartetes Ergebnis**: Der Benutzer erhält eine Validierungsausgabe, dass das XPlan-Dokument syntaktisch invalide ist.

#### Testschritt 6:
- **Aktion**: Der Benutzer führt den Befehl [4] mit einem validen Planwerk aus.
- **Erwartetes Ergebnis**: Der Benutzer erhält eine Validierungsausgabe, dass das XPlan-Dokument geometrisch valide ist.

#### Testschritt 7:
- **Aktion**: Der Benutzer führt den Befehl [4] mit einem geometrisch invaliden Planwerk aus.
- **Erwartetes Ergebnis**: Der Benutzer erhält eine Validierungsausgabe, dass das XPlan-Dokument geometrisch invalide ist.

#### Testschritt 8:
- **Aktion**: Der Benutzer führt den Befehl [5] mit einem validen Planwerk aus.
- **Erwartetes Ergebnis**: Der Benutzer erhält eine Validierungsausgabe, dass das XPlan-Dokument semantisch valide ist.

#### Testschritt 9:
- **Aktion**: Der Benutzer führt den Befehl [5] mit einem semantisch invaliden Planwerk aus.
- **Erwartetes Ergebnis**: Der Benutzer erhält eine Validierungsausgabe, dass das XPlan-Dokument semantisch invalide ist.

Befehle 
<div><div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] Ohne Angabe einer Validierungsart: ./xpb validate file -f=&lt;file&gt; [-n=&lt;validationName&gt;]<br> </div><div>[3] Syntaktische Überprüfung: ./xpb validate file -f=&lt;file&gt; [-n=&lt;validationName&gt;] -t=syntax<br> </div><div>[4] Geometrische Überprüfung: ./xpb validate file -f=&lt;file&gt; [-n=&lt;validationName&gt;] -t=geometric<br> </div><span>[5] Semantische Überprüfung: ./xpb validate file -f=&lt;file&gt; [-n=&lt;validationName&gt;] -t=semantic</span><br> </div> </div> </div> 

### XPlanCLI - Command 'validate' - Subcommand 'file' - Ausführung (Validierungsoptionen)

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer führt den Befehl [2] aus.
- **Erwartetes Ergebnis**: Der Benutzer erhält eine Validierungsausgabe. Die geometrische Überprüfung der Flächenschlussbedingung wird übersprungen.

#### Testschritt 3:
- **Aktion**: Der Benutzer führt den Befehl [3] aus.
- **Erwartetes Ergebnis**: Der Benutzer erhält eine Validierungsausgabe. Die geometrische Überprüfung des Geltungsbereich wird übersprungen.

#### Testschritt 4:
- **Aktion**: Der Benutzer führt den Befehl [4] aus.
- **Erwartetes Ergebnis**: Der Benutzer erhält eine Validierungsausgabe. Die geometrische Überprüfung der Laufrichtung wird übersprungen.

Befehle 
<div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] Überspringen der Flächenschluss-Prüfung: ./xpb validate file -f=&lt;file&gt; [-n=&lt;validationName&gt;] -o=skip-flaechenschluss<br> </div><div>[3] Überspringen der Geltungsbereichs-Prüfung: ./xpb validate file -f=&lt;file&gt; [-n=&lt;validationName&gt;] -o=skip-geltungsbereich<br> </div><span>[4] Überspringen der Laufrichtungs-Prüfung: ./xpb validate file -f=&lt;file&gt; [-n=&lt;validationName&gt;] -o=skip-laufrichtung</span><br> </div> </div> 

### XPlanCLI - Command 'validate' - Subcommand 'file' - Ablage der Validierungsergebnisse

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-validator-cli-$VERSION/etc/.

#### Testschritt 2:
- **Aktion**: Der Benutzer überprüft, ob in der Datei [2] das Verzeichnis [3] für die erstellten Validierungsergebnisse angegeben ist.
- **Erwartetes Ergebnis**: Ein Verzeichnis ist nicht gesetzt, daher befinden sich die Validierungsergebnisse unter [4].

#### Testschritt 3:
- **Aktion**: Der Benutzer wechselt in das Temp-Verzeichnis mit Hilfe des Befehls [4].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-validator-cli-$VERSION/tmp/.

#### Testschritt 4:
- **Aktion**: Der Benutzer überprüft, ob das Validierungsergebnis als Archiv (HTML, XML und PDF) dort abgelegt worden ist.
- **Erwartetes Ergebnis**: Das Validierungsergebnis wurde als Archiv angelegt.

Befehle 
<div><span>[1] cd ~/xplan-validator-cli-$VERSION/etc/<br></span><div>[2] validatorConfiguration.properties<br> </div><div>[3] validationReportDirectory=&lt;directory&gt;<br> </div><span>[4] cd ~/xplan-validator-cli-$VERSION/tmp/</span><br> </div> 

### XPlanCLI - Command 'validate' - Subcommand 'db' - Hilfe aufrufen

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer ruft die Hilfe des Subcommand 'db' von Command 'validate' mit dem Befehl in [2] auf.
- **Erwartetes Ergebnis**: Die Ausgabe gibt Auskunft über die Nutzung des Subcommand 'file':
&nbsp;
Usage: xpb validate db [-d=&lt;database&gt;] [-h=&lt;host&gt;] [-p=&lt;port&gt;] [-P=&lt;password&gt;] [-r=&lt;rules&gt;] [-u=&lt;user&gt;] [COMMAND]

&nbsp;Validate XPlanGML in xPlanBox database.
  -d, --database=&lt;database&gt;   name of the xplanbox database (default: xplanbox)
  -h, --host=&lt;host&gt;   hostname of the database server (default: localhost)
  -p, --port=&lt;port&gt;   port of the database server (default: 5432)
  -P, --password=&lt;password&gt;   database user password
  -r, --rules=&lt;rules&gt;   directory containing the rules
  -u, --user=&lt;user&gt;   database user (default: postgres)

Befehle 
<div><div><div><div><div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] ./xpb validate db help<br> </div> </div> </div> </div> </div> </div> </div> 

### XPlanCLI - Command 'validate' - Subcommand 'db' - Ausführung

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer führt den Befehl [2] aus.
- **Erwartetes Ergebnis**: Alle in der Datenhaltung enthaltenen Planwerke werden validiert, anschließend wird das Ergebnis der Validierung in einer CSV-Datei zusammengefasst. Die erstellte CSV-Datei liegt unter /tmp.

Befehle 
<span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><span>[2] ./xpb validate db [-d=&lt;database&gt;][-h=&lt;host&gt;][-p=&lt;port&gt;][-P=&lt;password&gt;][-r=&lt;rules&gt;][-u=&lt;user&gt;]</span><br> 

### XPlanCLI - Command 'manage' - Hilfe aufrufen

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer ruft die Hilfe des Command 'validate' mit dem Befehl in [2] auf.
- **Erwartetes Ergebnis**: Die Ausgabe gibt Auskunft über die verfügbaren 

'subcommands' des Command 'validate':

help:      
- Display help information about the specified command.
list:
- List all plans that are available in the data storage.
import: 
- Import a single or multiple XPlanArchive(s) or XPlanGML file(s).
export:
- Export a single or multiple plan(s).
delete:
- Delete a single or multiple plan(s).
create-metadata:
- Create service metadata records.

Befehle 
<span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] ./xpb manage help<br> </div> 

### XPlanCLI - Command 'manage' - Subcommand 'import' - Hilfe aufrufen

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer ruft die Hilfe des Subcommand 'import' von Command 'manage' mit dem Befehl in [2] auf.
- **Erwartetes Ergebnis**: Usage: xpb manage import [-ov] [-c=&lt;crs&gt;] [--config=&lt;config&gt;][--workspace=&lt;workspace&gt;] -f=&lt;files&gt;[, &lt;files&gt;...][-f=&lt;files&gt;[, &lt;files&gt;...]]... [COMMAND]
&nbsp;
Import a single or multiple XPlanArchive(s) or XPlanGML file(s).
  -c, --crs=&lt;crs&gt;
       --config=&lt;config&gt;   Path to the XPLANBOX_CONFIG directory.
  -f, --file=&lt;files&gt;[, &lt;files&gt;...]   File(s) to import.
  -o, --force   Force import, ignores invalid or unknown raster format or CRS (default: false).
  -v, --verbose   Print the system log (default: false)
       --workspace=&lt;workspace&gt;   Path to the DEEGREE_WORKSPACE_ROOT directory.

Befehle 
<div><div><div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] ./xpb manage import help<br> </div> </div> </div> </div> </div> 

### XPlanCLI - Command 'manage' - Subcommand 'import' - Ausführung

#### Testschritt 2:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 1:
- **Aktion**: Der Benutzer importiert mit dem Befehl [2] ein Planwerk in die Datenhaltung der xPlanBox.
- **Erwartetes Ergebnis**: Das Planwerk wird in die Datenhaltung der xPlanBox importiert, je nach Konfiguration auch mit Geometriefehlern (unter Nutzung von [--force]).

Befehle 
<div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><span>[2] &nbsp;./xpb manage import [-ov][-c=&lt;crs&gt;][--config=&lt;config&gt;][--workspace=&lt;workspace&gt;] -f=&lt;files&gt;</span><br> </div> 

### XPlanCLI - Command 'manage' - Subcommand 'list' - Hilfe aufrufen

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer ruft die Hilfe des Subcommand 'list' von Command 'manage' mit dem Befehl in [2] auf.
- **Erwartetes Ergebnis**: Usage: xpb manage list [-v] [--config=&lt;config&gt;] [--workspace=&lt;workspace&gt;] [COMMAND]
&nbsp;
List all plans that are available in the data storage.
       --config=&lt;config&gt;   Path to the XPLANBOX_CONFIG directory.
  -v, --verbose   Print the system log (default: false)
       --workspace=&lt;workspace&gt;   Path to the DEEGREE_WORKSPACE_ROOT directory.

Befehle 
<div><div><div><div><div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] ./xpb manage list help<br> </div> </div> </div> </div> </div> </div> </div> 

### XPlanCLI - Command 'manage' - Subcommand 'list' - Ausführung

#### Testschritt 2:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 1:
- **Aktion**: Der Benutzer ruft mit dem Befehl [2] eine Auflistung der in der Datenhaltung vorliegenden Planwerke auf und überprüft somit, ob das durch Subcommand 'import'
importierte Planwerk vorhanden ist.
- **Erwartetes Ergebnis**: Die in der Datenhaltung der xPlanBox vorliegenden Planwerke werden aufgelistet und das durch Subcommand 'import'  importierte Planwerk wird angezeigt.

Befehle 
<div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><span>[2] ./xpb manage list [-v][--config=&lt;config&gt;][--workspace=&lt;workspace&gt;]</span><br> </div> 

### XPlanCLI - Command 'manage' - Subcommand 'export' - Hilfe aufrufen

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer ruft die Hilfe des Subcommand 'export' von Command 'manage' mit dem Befehl in [2] auf.
- **Erwartetes Ergebnis**: Usage: xpb manage export [-v] [--config=&lt;config&gt;] [-t=&lt;target&gt;] [--workspace=&lt;workspace&gt;] -i=&lt;ids&gt;[,&lt;ids&gt;...] [-i=&lt;ids&gt; [,&lt;ids&gt;...]]... [COMMAND]
&nbsp;
Export a single or multiple plan(s).
      --config=&lt;config&gt;   Path to the XPLANBOX_CONFIG directory.
  -i, --id=&lt;ids&gt;[,&lt;ids&gt;...]   Die ID des Plans der exportiert werden soll.
  -t, --target=&lt;target&gt;   Angabe des Verzeichnis in dem die exportierten XPlanArchive abgelegt werden sollen.
  -v, --verbose   Print the system log (default: false)
       --workspace=&lt;workspace&gt;   Path to the DEEGREE_WORKSPACE_ROOT directory.

Befehle 
<div><div><div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] ./xpb manage export help<br> </div> </div> </div> </div> </div> 

### XPlanCLI - Command 'manage' - Subcommand 'export' - Ausführung

#### Testschritt 2:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 1:
- **Aktion**: Der Benutzer exportiert ein Planwerk mit Hilfe des Befehls in [2] aus der Datenhaltung der xPlanBox.
- **Erwartetes Ergebnis**: Das Planwerk wird erfolgreich exportiert und im angegebenen Verzeichnis angezeigt.

Befehle 
<div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><span>[2] ./xpb manage export [-v][--config=&lt;config&gt;][-t=&lt;target&gt;][--workspace=&lt;workspace&gt;]-i=&lt;ids&gt;</span><br><div><ul> </ul> </div> </div><br> </div> 

### XPlanCLI - Command 'manage' - Subcommand 'delete' - Hilfe aufrufen

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer ruft die Hilfe des Subcommand 'delete' von Command 'manage' mit dem Befehl in [2] auf.
- **Erwartetes Ergebnis**: Usage: xpb manage delete [-v] [--config=&lt;config&gt;] [--workspace=&lt;workspace&gt;]
                         -i=&lt;ids&gt;[,&lt;ids&gt;...] [-i=&lt;ids&gt;[,&lt;ids&gt;...]]... [COMMAND]

Delete a single or multiple plan(s).
      --config=&lt;config&gt;   Path to the XPLANBOX_CONFIG directory.
  -i, --id=&lt;ids&gt;[,&lt;ids&gt;...]  Die ID des Plans der geloescht werden soll.
  -v, --verbose  Print the system log (default: false)
      --workspace=&lt;workspace&gt;  Path to the DEEGREE_WORKSPACE_ROOT directory.

Befehle 
<div><div><div><div><div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] ./xpb manage delete help<br> </div> </div> </div> </div> </div> </div> </div> 

### XPlanCLI - Command 'manage' - Subcommand 'delete' - Ausführung

#### Testschritt 2:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 1:
- **Aktion**: Der Benutzer löscht mit dem Befehl [2] ein Planwerk aus der Datenhaltung der xPlanBox.
- **Erwartetes Ergebnis**: Das Planwerk wird aus der Datenhaltung der xPlanBox gelöscht, geprüft werden kann dies mit erneuter Ausführung des Subcommand 'list'.

Befehle 
<div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><span>[2] ./xpb manage delete [-v][--config=&lt;config&gt;][--workspace=&lt;workspace&gt;]-i=&lt;ids&gt;</span><br><div><ul> </ul> </div> </div><br> </div> 

### XPlanCLI - Command 'admin' - Hilfe aufrufen

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer ruft die Hilfe des Command 'validate' mit dem Befehl in [2] auf.
- **Erwartetes Ergebnis**: Die Ausgabe gibt Auskunft über die verfügbaren 

'subcommands' des Command 'admin':

help:      
- Display help information about the specified command.
district-update:
- Update column district of table xplanmgr.plans.
evaluation-db-update: 
- EvaluationSchemaSynchronizer
resynthesize:
- Reads the XPlanGML data and updates the re-synthesized data in the xplansyn schema.
sortdate-update:
- Update sort date.

Befehle 
<div><div><div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] ./xpb admin help<br> </div> </div> </div> </div> </div> 

### XPlanCLI - Command 'admin' - Subcommand 'evaluation-db-update' - Hilfe aufrufen

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer ruft die Hilfe des Subcommand 'evaluation-db-update' von Command 'admin' mit dem Befehl in [2] auf.
- **Erwartetes Ergebnis**: Usage: xpb admin evaluation-db-update [-d=&lt;database&gt;] [-h=&lt;host&gt;] [-p=&lt;port&gt;] [-P=&lt;password&gt;] [-t=&lt;type&gt;] [-u=&lt;user&gt;] [COMMAND]
&nbsp;
EvaluationSchemaSynchronizer
  -d, --database=&lt;database&gt;   name of the xplanbox database (default: xplanbox)
  -h, --host=&lt;host&gt;   hostname of the database server (default: localhost)
  -p, --port=&lt;port&gt;   port of the database server (default: 5432)
  -P, --password=&lt;password&gt;   database user password
  -t, --type=&lt;type&gt;   one of 'ALL' or 'SYNC' (default: SYNC); 'SYNC' synchronizes plans logged in xplanevaluation. planTableLog, 'ALL' synchronizes all available plans.
  -u, --user=&lt;user&gt;   database user

Befehle 
<div><div><div><div><div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] ./xpb admin evaluation-db-update help<br> </div> </div> </div> </div> </div> </div> </div> 

### XPlanCLI - Command 'admin' - Subcommand 'evaluation-db-update' - Ausführung

#### Testschritt 3:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 1:
- **Aktion**: Der Benutzer führt den Befehl [2] aus.
- **Erwartetes Ergebnis**: Alle in der Datenbasis enthaltenen Planwerke werden aus dem XPlanSyn-Schema in das Auswerteschema der xPlanBox überführt.

#### Testschritt 2:
- **Aktion**: Der Benutzer importiert ein neues Planwerk in die Datenhaltung der xPlanBox und führt anschließend den Befehl [3] aus.
- **Erwartetes Ergebnis**: Alle in der Datenbasis enthaltenen Planwerke, die seit der letzten Ausführung des XPlanAuswerteschemaCLI verändert oder hinzugefügt wurden, werden mit dem Auswerteschmema synchronisiert und überführt.

Befehle 
<span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] xpb admin evaluation-db-update [-d=&lt;database&gt;] [-h=&lt;host&gt;] [-p=&lt;port&gt;] [-P=&lt;password&gt;] -t=ALL [-u=&lt;user&gt;]<br> </div><div><ul><li>Der Befehl in [2] darf nur einmal, initial, ausgeführt werden! </li> </ul> </div><span>[3] xpb admin evaluation-db-update [-d=&lt;database&gt;] [-h=&lt;host&gt;] [-p=&lt;port&gt;] [-P=&lt;password&gt;] -t=SYNC [-u=&lt;user&gt;]</span><br> 

### XPlanCLI - Command 'admin' - Subcommand 'resynthesize' - Hilfe aufrufen

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer ruft die Hilfe des Subcommand 'resynthesize' von Command 'admin' mit dem Befehl in [2] auf.
- **Erwartetes Ergebnis**: Usage: xpb admin resynthesize [-v] [--config=&lt;config&gt;] [--workspace=&lt;workspace&gt;] [-i=&lt;ids&gt;[,&lt;ids&gt;...]]... [COMMAND]

&nbsp;Reads the XPlanGML data and updates the re-synthesized data in the xplansyn schema.
      --config=&lt;config&gt;   Path to the XPLANBOX_CONFIG directory.
  -i, --id=&lt;ids&gt;[,&lt;ids&gt;...]   The ID of a plan in the XPlanManager of the plan to re-synthesize. If missing all plans are re-synthesized.
  -v, --verbose   Print the system log (default: false)
      --workspace=&lt;workspace&gt;   Path to the DEEGREE_WORKSPACE_ROOT directory.

Befehle 
<div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] ./xpb admin resynthesize help<br> </div><span>[3] ./xpb admin resynthesize [-v][--config=&lt;config&gt;][--workspace=&lt;workspace&gt;][-i=&lt;ids&gt;]</span><br> </div> 

### XPlanCLI - Command 'admin' - Subcommand 'resynthesize' - Ausführung

#### Testschritt 2:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 1:
- **Aktion**: Der Benutzer führt den Befehl [2] aus.
- **Erwartetes Ergebnis**: Liest die XPlanGMLs und speichert die resynthetisierten Planwerke im Schema 'xplansyn'.

Befehle 
<div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><span>[2] ./xpb admin resynthesize [-v][--config=&lt;config&gt;][--workspace=&lt;workspace&gt;][-i=&lt;ids&gt;]</span><br> </div> 

### XPlanCLI - Command 'admin' - Subcommand 'sortdate-update' - Hilfe aufrufen

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer ruft die Hilfe des Subcommand 'sortdate-update' von Command 'admin' mit dem Befehl in [2] auf.
- **Erwartetes Ergebnis**: Usage: xpb admin sortdate-update [-v] [--config=&lt;config&gt;] [--workspace=&lt;workspace&gt;] [COMMAND]

Update sort date.
      --config=&lt;config&gt;   Path to the XPLANBOX_CONFIG directory.
  -v, --verbose   Print the system log (default: false)
      --workspace=&lt;workspace&gt;   Path to the DEEGREE_WORKSPACE_ROOT directory.

Befehle 
<div><div><div><div><div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><div>[2] ./xpb admin sortdate-update help<br> </div> </div> </div> </div> </div> </div> </div> 

### XPlanCLI - Command 'admin' - Subcommand 'sortdate-update' - Ausführung

#### Testschritt 1:
- **Aktion**: Der Benutzer wechselt in das Verzeichnis des XPlanCLI mit Hilfe des Befehls [1].
- **Erwartetes Ergebnis**: Der Benutzer befindet sich in dem Verzeichnis ~/xplan-cli-tools-$VERSION/bin.

#### Testschritt 2:
- **Aktion**: Der Benutzer führt den Befehl [2] aus.
- **Erwartetes Ergebnis**: Aktualisiert die Spalte 'sortDateUpdate' der Tabelle 'xplanmgr.plans'.

Befehle 
<div><div><span>[1] ~/xplan-cli-tools-$VERSION/bin<br></span><span>[2] ./xpb admin sortdate-update [-v][--config=&lt;config&gt;][--workspace=&lt;workspace&gt;]</span><br> </div> </div> 

[Nach oben](#Testplan-der-xPlanBox)
[Nach oben](#Testplan-der-xPlanBox)
## Komponente DB-Aktualisierung

### Ausführung der SQL-Skripte zur Aktualisierung des Datenbankschemas

#### Testschritt 1:
- **Aktion**: Der Benutzer führt den SQL-Befehl SELECT tag FROM databasechangelog WHERE versionid='neu'
- **Erwartetes Ergebnis**: Die SQL-Abfrage liefert kein Ergebnis, da es die Tabelle databasechangelog in der Version 'neu' noch nicht gibt.

#### Testschritt 2:
- **Aktion**: Der Benutzer führt die DB-Skripte zur Aktualisierung des Datenbankschemas zur XPlanBox Version 'neu' aus.
- **Erwartetes Ergebnis**: Es treten keine Fehlermeldungen auf.

#### Testschritt 3:
- **Aktion**: Der Benutzer führt den SQL-Befehl SELECT tag FROM databasechangelog WHERE versionid='neu'
- **Erwartetes Ergebnis**: Die SQL-Abfrage liefert genau einen Treffer mit der Version 'neu'.

### (Optional) Ausführen des Kommandozeilenwerkzeug reSynthesizer

#### Testschritt 1:
- **Aktion**: Der Benutzer führt den reSynthesiser aus (siehe 6. XPlanUpdateDataCLI)

- **Erwartetes Ergebnis**: Es treten keine Fehlermeldungen auf.

#### Testschritt 2:
- **Aktion**: Der Benutzer kontrolliert die in der Datenhaltung vorliegenden Daten auf Vollständigkeit.
- **Erwartetes Ergebnis**: Alle vorherigen Daten sind auch im neuen XPlanSyn-Schema vorhanden.

### (Optional) Ausführen des Kommandozeilenwerkzeug EvaluationSchemaSynchronizer

#### Testschritt 1:
- **Aktion**: Falls vorhanden, muss der Benutzer die Datenbankschemas 'xplanevaluationxplansynpre', 'xplanevaluationxplansyn' und 'xplanevaluationxplansynarchive' löschen.
- **Erwartetes Ergebnis**: Die Datenbankschemas können erfolgreich gelöscht werden.

#### Testschritt 2:
- **Aktion**: Der Benutzer legt die Datenbankschemas mit Hilfe der SQL-Skipte im EvaluationSchemaSynchronizer an.
- **Erwartetes Ergebnis**: Es treten keine Fehlermeldungen auf, die Datenbankschemas existieren.

#### Testschritt 3:
- **Aktion**: Der Benutzer führt den EvaluationSchemaSynchronizer mit der Option --type ALL aus (siehe 8. XPlanAuswerteschemaCLI).
- **Erwartetes Ergebnis**: Es treten keine Fehlermeldungen auf.

#### Testschritt 4:
- **Aktion**: Der Benutzer kontrolliert die in der Datenhaltung vorliegenden Daten darauf, dass die im jeweiligen XPlanSyn-Schema gespeicherten Daten auch dem neu erstellten Auswerteschema gleichen.
- **Erwartetes Ergebnis**: Die im XPlanSyn-Schema vorliegenden Daten gleichen dem jeweiligen Auswerteschema.

[Nach oben](#Testplan-der-xPlanBox)
