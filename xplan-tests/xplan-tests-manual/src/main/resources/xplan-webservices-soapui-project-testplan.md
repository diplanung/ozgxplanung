# Authorization-Setup
## Set Credentials
* Groovy Script
## JWT Authentication
* checkIfJwtAuthEnabled
* retrieveToken
* retrieveToken Property Transfer
* setAuthorizationHeaders
* Cleanup
# xplan-wms
## wms
* GetCapabilities
* GetMap
## wmspre
* GetCapabilities
* GetMap
## wmsarchive
* GetCapabilities
* GetMap
## bpwms
* GetCapabilities
* GetMap
## fpwms
* GetCapabilities
* GetMap
## lpwms
* GetCapabilities
* GetMap
## rpwms
* GetCapabilities
* GetMap
## sowms
* GetCapabilities
* GetMap
## bpwmspre
* GetCapabilities
* GetMap
## fpwmspre
* GetCapabilities
* GetMap
## lpwmspre
* GetCapabilities
* GetMap
## rpwmspre
* GetCapabilities
* GetMap
## sowmspre
* GetCapabilities
* GetMap
## bpwmsarchive
* GetCapabilities
* GetMap
## fpwmsarchive
* GetCapabilities
* GetMap
## lpwmsarchive
* GetCapabilities
* GetMap
## rpwmsarchive
* GetCapabilities
* GetMap
## sowmsarchive
* GetCapabilities
* GetMap
## GetAttachment
* Import BPlan41_HH_Bergedorf110_1Aend_soapui-webservices.zip
* GetFeatureInfo-xml
* Property Transfer
* getAttachment
* Delete BPlan41_HH_Bergedorf110_1Aend_soapui-webservices.zip
* Cleanup Properties
## PlanwerkWMS
* Import Eidelstedt_4_V4-soapui-webservices.zip
* Property Transfer
* GetCapabilities
* GetCapabilities with whitespaces
* Delete Eidelstedt_4_V4-soapui-webservices.zip
* GetCapabilities should fail
* Cleanup Properties
## GetMap
* Import Eidelstedt_4_V4-soapui-webservices.zip
* Property Transfer
* GetMap
* Delete Eidelstedt_4_V4-soapui-webservices.zip
* Cleanup Properties
## GetFeatureInfo
* OpenAPIInfo
* Import Eidelstedt_4_V4-soapui-webservices.zip
* Import BPlan002_5-3.zip
* Import BPlan004_6-0.zip
* Property Transfer
* GetFeatureInfo-html BP_BaugebietsTeilFlaeche
* GetFeatureInfo-html BP_Blan
* GetFeatureInfo-xml BP_BaugebietsTeilFlaeche
* GetFeatureInfo-xml BP_Plan
* Delete Eidelstedt_4_V4-soapui-webservices.zip
* Delete BPlan002_5-3.zip
* Delete BPlan004_6-0.zip
* Cleanup Properties
## RESTAPI
* GET /config
* GET /config/restart
* GET /config/restart invalidApiKey
* GET /config/update
* GET /config/update invalidApiKey
* DELETE /planwerkwmsapi/{planId}
* DELETE /planwerkwmsapi/{planId} invalidApiKey
# xplansyn-wfs
## xplansynwfs
* GetCapabilities
## xplansynwfspre
* GetCapabilities
## xplansynwfsarchive
* GetCapabilities
## StoredQuery
* Import Eidelstedt4V4-with-nummer-soapui-webservices.zip
* Property Transfer
* XPlanSynWFS ListStoredQueries
* XPlanSynWFS DescribeStoredQueries
* XPlanSynWFS PlanId
* XPlanSynWFS PlanIdWithSrsName
* XPlanSynWFS PlanName
* XPlanSynWFS PlanNameWithSrsName
* XPlanSynWFS PlanNameAndType
* Delete Eidelstedt4V4-with-nummer-soapui-webservices.zip
* Cleanup Properties
## GetFeature
* OpenAPIInfo
* Import Osdorf48_Test_60.zip
* Import BPlan004_6-0.zip
* Import BP_5.1_Textsortierung_1.zip
* Import BP_5.2_Textsortierung_2.zip
* Import BP_5.1_Textsortierung_3.zip
* Import BP_5.1_Textsortierung_4.zip
* Property Transfer
* GetFeature 2.0.0 BP_Plan
* GetFeature 1.1.0 BP_Plan
* GetFeature 2.0.0 BP_Bereich IsSurfaceFilter
* GetFeature BP_GruenFlaeche
* GetFeature Archive
* GetFeature BP_Plan Textsortierung_1
* GetFeature BP_Plan Textsortierung_2
* GetFeature BP_Plan Textsortierung_3
* GetFeature BP_Plan Textsortierung_4
* Delete Osdorf48_Test_60.zip
* Delete BPlan004_6-0.zip
* Delete BP_5.1_Textsortierung_1.zip
* Delete BP_5.2_Textsortierung_2.zip
* Delete BP_5.1_Textsortierung_3.zip
* Delete BP_5.1_Textsortierung_4.zip
* Cleanup Properties
# xplan-wfs
## wfs40
* GetCapabilities
## wfs41
* GetCapabilities
## wfs50
* GetCapabilities
## wfs51
* GetCapabilities
## wfs52
* GetCapabilities
## wfs53
* GetCapabilities
## wfs54
* GetCapabilities
## wfs60
* GetCapabilities
## wfs40pre
* GetCapabilities
## wfs41pre
* GetCapabilities
## wfs50pre
* GetCapabilities
## wfs51pre
* GetCapabilities
## wfs52pre
* GetCapabilities
## wfs53pre
* GetCapabilities
## wfs54pre
* GetCapabilities
## wfs60pre
* GetCapabilities
## wfs40archive
* GetCapabilities
## wfs41archive
* GetCapapbilities
## wfs50archive
* GetCapabilities
## wfs51archive
* GetCapabilities
## wfs52archive
* GetCapabilities
## wfs53archive
* GetCapabilities
## wfs54archive
* GetCapabilities
## wfs60archive
* GetCapabilities
## StoredQuery
* Import Eidelstedt4V4-with-nummer-soapui-webservices.zip
* Property Transfer
* XPlanWFS ListStoredQueries
* XPlanWFS DescribeStoredQueries
* XPlanWFS PlanId
* XPlanWFS PlanIdWithSrsName
* XPlanWFS PlanName
* XPlanWFS PlanNameWithSrsName
* Delete Eidelstedt4V4-with-nummer-soapui-webservices.zip
* Cleanup Properties
## StoredQuery-ArchivedPlan
* Import Osdorf48_Test_60
* Property Transfer
* XPlanWFS ListStoredQueries
* XPlanWFS DescribeStoredQueries
* XPlanWFS PlanName
* XPlanWFS PlanNameWithSrsName
* Delete Osdorf48_Test_60
* Cleanup Properties
## GetFeature
* OpenAPIInfo
* Import BPlan004_6-0.zip
* Property Transfer
* GetFeature 2.0.0 BP_Plan
* GetFeature 1.1.0 BP_Plan
* Delete BPlan004_6-0.zip
* Cleanup Properties
## GetFeature-CompositeCurve
* Import BPlan004_6-0_CompositeCurve.xml
* Property Transfer
* GetFeature 2.0.0 BP_StrassenbegrenzungsLinie
* Delete BPlan004_6-0_CompositeCurve.xml
* Cleanup Properties
# xplan-inspireplu
## viewservice
* GetCapabilities
## downloadservice
* GetCapabilities
# mapserver-wms
## GetCapabilities
* GetCapabilities
## GetMap
* GetMap
# mapproxy-wms
## GetCapabilities
* GetCapabilities
## GetMap
* GetMap - Raster
* GetMap - Vektor
# mapproxy-wmts
## GetCapabilities
* GetCapabilities
## GetTile
* GetTile - bp_raster
* GetTile - bp_objekte
* REST GetTile - bp_raster
* REST GetTile - bp_objekte
# Codelists TestSuite
## externalCodelists
* POST BP 5.2 XX importPlan
* Property Transfer
* GetFeatureByPlanNameAndType
* DEL BP 5.2 pl deletePlan
* Cleanup
# Cleanup
## JWT Authentication
* removeAuthorizationHeaders
