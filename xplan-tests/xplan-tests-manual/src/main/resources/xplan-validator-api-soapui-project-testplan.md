# Authorization Setup
## JWT Authentication
* checkIfJwtAuthEnabled
* retrieveToken
* retrieveToken Property Transfer
* setAuthorizationHeaders
* Cleanup
# XPlanValidatorAPI v1 TestSuite
## / TestCase
* GET XX X.X XX openAPI
## /info TestCase
* GET XX X.X XX showConfig
## /validate TestCase
* POST BP 5.0 XX validatePlanExternalEntityExpectError400
* POST BP 5.1 XX validatePlan
* POST BP 5.1 sF validatePlan
* POST BP 5.1 sG validatePlan
* POST BP 5.1 sGeo validatePlan
* POST BP 5.1 sS validatePlan
* POST BP 5.1 nV validatePlan
* POST BP 5.1 nV,sF,sG,sGeo,sS validatePlan
* POST BP 5.1 XX validatePlanRequestBodyText/XML
* POST BP 5.1 XX validatePlanRequestBodyApplication/GML+XML
* POST BP 5.1 XX validatePlanRequestBodyApplication/ZIP
* POST BP 5.1 XX validatePlanRequestBodyApplication/ZIPContentTypeText/XMLExpectError400
* POST BP 5.1 XX validatePlanAcceptHeaderApplication/JSON
* POST BP 5.1 XX validatePlanAcceptHeaderApplication/XML
* POST BP 5.1 XX validatePlanAcceptHeaderText/XML
* POST BP 5.1 XX validatePlanAcceptHeaderApplication/PDF
* POST BP 5.1 XX validatePlanAcceptHeaderApplication/ZIP
* POST BP 5.2 XX validatePlanMissingAndUncheckedReference
* POST BP 5.3 XX validatePlan
* POST BP 5.3 XX validateMultiplePlansWfsFcAdditionalObjects
* POST BP 5.3 XX validateMultiplePlansWfsFcGml
* POST BP 5.3 XX validateMultiplePlansWfsFcZip
* POST BP 5.3 XX validatePlanWfsFcGml
* POST BP 5.3 XX validatePlanWfsFcZip
* POST BP 5.3 XX validatePlanInvalidNameXSSExpectError
* POST BP 5.3 XX validatePlanInvalidXFilenameXSSExpectError
* POST BP 6.0 XX validatePlanUnsupportedContentTypeExpectError
* POST BP 6.0 sGeo validatePlan
* POST BP 6.0 XX validatePlanWithSyntacticError
* POST BP 6.0 XX validatePlanValidationErrors
* POST BP 6.0 pf validatePlanExpectError400
* POST BP 6.0.2 XX validatePlan
* POST BP 6.0.2 XX validatePlanExpectError
* POST BP 6.0.2 XX validatePlanExpectErrorNoBereich
* POST FP 6.0.2 XX validatePlan
* POST FP 6.0.2 XX validatePlanExpectError
* POST XX X.X XX validateFileUnsupportedContentTypeExpectError
# Workflow v1 TestSuite
## Raster evaluation TestCase
* POST BP 6.0.2 XX validatePlan
* POST BP 6.0.2 XX validatePlanWithMissingPNG
* POST BP 6.0.2 XX validatePlanWithWrongCRS
* POST BP 6.0.2 XX validatePlanWithNotSupportedFormatExpectError415
## Validation invalid TestCase
* POST BP 5.2 XX validatePlanGeometricInvalid
* POST BP 6.0 XX validatePlanSemanticInvalid
* POST BP 6.0 XX validatePlanSyntacticInvalid
# Profiles v1 TestSuite
## /info TestCase
* GET XX X.X XX showConfig
* Property Transfer
## /validate TestCase
* POST BP 6.0 pf validatePlanWithProfil
## Cleanup Properties
* Property Transfer
# XPlanValidatorAPI v2 TestSuite
## / TestCase
* GET XX X.X XX openAPI
* GET XX X.X XX invalidAcceptHeaderExpectError406
## /info TestCase
* GET XX X.X XX showConfig
* GET XX X.X XX invalidAcceptHeaderExpectError406
## /validate TestCase
* POST BP 6.0.2 XX validatePlan
* POST BP 6.0.2 XX validatePlan Property Transfer
* POST BP 6.0.2 XX mismatchingContentTypeAndFileType
* POST BP 6.0.2 XX mismatchingContentTypeAndFileType Property Transfer
* POST BP 6.0.2 XX invalidQueryParamExpectError400
* POST BP 6.0.2 XX invalidAcceptHeaderExpectError406
* POST BP 6.0 XX unsupportedMediaTypeZipExpectError415
* POST BP X.X XX unsupportedMediaTypeOdtExpectError415
## /status/{uuid} TestCase
* GET BP 6.0.2 XX pollStatus
* pollStatus
* GET BP 6.0.2 XX retrieveStatus
* transferUrls
* GET BP 6.0.2 XX pollStatusMismatchingTypes
* pollStatusMismatchingTypes
* GET BP 6.0.2 XX mismatchingContentTypeAndFileType
* GET BP 6.0.2 XX invalidUuidExpectError404
* GET BP 6.0.2 XX invalidAcceptHeaderExpectError406
* Cleanup Properties
## /report/{uuid} TestCase
* GET BP 6.0.2 XX retrieveReportJson
* GET BP 6.0.2 XX retrieveReportPdf
* GET BP 6.0.2 XX invalidUuidExpectError404
* GET BP 6.0.2 XX invalidAcceptHeaderExpectError406
## /report/{uuid}/geomfindings TestCase
* GET BP 6.0.2 XX retrieveGeomfindings
* GET BP 6.0.2 XX invalidUuidExpectError404
* GET BP 6.0.2 XX invalidAcceptHeaderExpectError406
## /report/{uuid}/geomfindings.json TestCase
* GET BP 6.0.2 XX retrieveGeomfindings
* GET BP 6.0.2 XX retrieveGeomfindingsNoAcceptHeader
* GET BP 6.0.2 XX invalidUuidExpectError404
* GET BP 6.0.2 XX invalidAcceptHeaderExpectError406
## Cleanup Properties
* Property Transfer
# Workflow v2 TestSuite
## Raster evaluation TestCase
* POST BP 6.0.2 XX validatePlan
* POST BP 6.0.2 XX validatePlan Property Transfer
* GET BP 6.0.2 XX pollStatus
* pollStatus
* GET BP 6.0.2 XX retrieveStatus
* GET BP 6.0.2 XX retrieveReportJson
* GET BP 6.0.2 XX retrieveReportPdf
* POST BP 6.0.2 XX validatePlanWithNotSupportedFormatExpectError415
* Cleanup Properties
## Raster evaluation missing PNG TestCase
* POST BP 6.0.2 XX validatePlan
* POST BP 6.0.2 XX validatePlan Property Transfer
* GET BP 6.0.2 XX pollStatus
* pollStatus
* GET BP 6.0.2 XX retrieveStatus
* GET BP 6.0.2 XX retrieveReportJson
* GET BP 6.0.2 XX retrieveReportPdf
* Cleanup Properties
## Raster evaluation wrong CRS TestCase
* POST BP 6.0.2 XX validatePlan
* POST BP 6.0.2 XX validatePlan Property Transfer
* GET BP 6.0.2 XX pollStatus
* pollStatus
* GET BP 6.0.2 XX retrieveStatus
* GET BP 6.0.2 XX retrieveReportJson
* GET BP 6.0.2 XX retrieveReportPdf
* Cleanup Properties
## Profiles TestCase
* GET XX X.X XX showConfig
* GET XX X.X XX showConfig Property Transfer
* POST BP 6.0.2 pf validatePlan
* POST BP 6.0.2 pf validatePlan Property Transfer
* GET BP 6.0.2 XX pollStatus
* pollStatus
* GET BP 6.0.2 XX retrieveStatus
* GET BP 6.0.2 XX retrieveReportJson
* GET BP 6.0.2 XX retrieveReportPdf
* Cleanup Properties
## Validation geometric error TestCase
* POST BP 5.2 XX validatePlan
* POST BP 5.2 XX validatePlan Property Transfer
* GET BP 5.2 XX pollStatus
* pollStatus
* GET BP 5.2 XX retrieveStatus
* GET BP 5.2 XX retrieveReportJson
* GET BP 5.2 XX retrieveReportPdf
* GET BP 5.2 XX retrieveGeomfindings
* Cleanup Properties
## Validation semantic error TestCase
* POST BP 6.0 XX validatePlan
* POST BP 6.0 XX validatePlan Property Transfer
* GET BP 6.0 XX pollStatus
* pollStatus
* GET BP 6.0 XX retrieveStatus
* GET BP 6.0 XX retrieveReportJson
* GET BP 6.0 XX retrieveReportPdf
* Cleanup Properties
## Validation syntactic error TestCase
* POST BP 6.0 XX validatePlan
* POST BP 6.0 XX validatePlan Property Transfer
* GET BP 6.0 XX pollStatus
* pollStatus
* GET BP 6.0 XX retrieveStatus
* GET BP 6.0 XX retrieveReportJson
* GET BP 6.0 XX retrieveReportPdf
* Cleanup Properties
## XPlanGML with multiple plans TestCase
* POST BP 5.4 XX validatePlan
* POST BP 5.4 XX validatePlan Property Transfer
* GET BP 5.4 XX pollStatus
* pollStatus
* GET BP 5.4 XX retrieveStatus
* GET BP 5.4 XX retrieveReportJson
* GET BP 5.4 XX retrieveReportPdf
* Cleanup Properties
## XPlanGML with multiple plans syntactic error TestCase
* POST BP 5.4 XX validatePlan
* POST BP 5.4 XX validatePlan Property Transfer
* GET BP 5.4 XX pollStatus
* pollStatus
* GET BP 5.4 XX retrieveStatus
* GET BP 5.4 XX retrieveReportJson
* GET BP 5.4 XX retrieveReportPdf
* Cleanup Properties
## XPlanGML with multiple plans geometric error TestCase
* POST BP 5.4 XX validatePlan
* POST BP 5.4 XX validatePlan Property Transfer
* GET BP 5.4 XX pollStatus
* pollStatus
* GET BP 5.4 XX retrieveStatus
* GET BP 5.4 XX retrieveReportJson
* GET BP 5.4 XX retrieveReportPdf
* Cleanup Properties
## XPlanGML with multiple plans semantic error TestCase
* POST BP 5.4 XX validatePlan
* POST BP 5.4 XX validatePlan Property Transfer
* GET BP 5.4 XX pollStatus
* pollStatus
* GET BP 5.4 XX retrieveStatus
* GET BP 5.4 XX retrieveReportJson
* GET BP 5.4 XX retrieveReportPdf
* Cleanup Properties
## XPlanGML with multiple plans skip errors TestCase
* POST BP 5.4 sGeo,sS,sF,sG,sL validatePlan
* POST BP 5.4 XX validatePlan Property Transfer
* GET BP 5.4 XX pollStatus
* pollStatus
* GET BP 5.4 XX retrieveStatus
* GET BP 5.4 XX retrieveReportJson
* GET BP 5.4 XX retrieveReportPdf
* Cleanup Properties
# Cleanup
## JWT Authentication
* removeAuthorizationHeaders
