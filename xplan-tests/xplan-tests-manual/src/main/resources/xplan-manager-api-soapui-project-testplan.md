# Authorization Setup
## JWT Authentication
* checkIfJwtAuthEnabled
* retrieveToken
* retrieveToken Property Transfer
* setAuthorizationHeaders
* Cleanup
# XPlanManagerAPI v1 TestSuite
## / TestCase
* GET XX X.X XX openAPI
## /info TestCase
* GET XX X.X XX showConfig
* Property Transfer
## /plan TestCase
* POST BP 4.0 sF,sG,sS importPlanInvalidLaufrichtungExpectError422
* POST BP 4.0 sF,sL,sS importPlan
* POST BP 4.1 sF,sS,pf importPlanInvalidProfilExpectError400
* POST BP 4.1 sF,sS importPlan
* POST BP 4.1 sF,sS importPlanMimeTypeV2
* POST BP 4.1 XX importInvalidPlanExpectError422
* POST BP 5.0 sF,sG,Ss importPlan
* POST BP 5.0 XX importInvalidPlanWithEntityExpectError400
* POST BP 5.0 XX importPlanWithAenderung/Dokument
* POST BP 5.1 sF,sS importPlanInvalidText/XMLExpectError400
* POST BP 5.2 sF importInvalidPlanMultiplePlaeneExpectError400
* POST BP 5.2 sF,sG,Ss importPlanWIthoutBereich
* POST BP 5.2 sF,sL,Ss importPlan
* POST BP 5.2 sF,sL,sS importPlanInvalidLaufrichtungExpectError422
* POST BP 5.2 sF,Ss importPlanWithTwoBereiche
* POST BP 5.2 XX importPlanMissingReferenceExpectError400
* POST BP 5.2 XX importPlanAsGmlMissingReferenceExpectError400
* POST BP 5.2 XX importPlanHttpReferenceExpectError500
* POST BP 5.2 XX importPlanAsGmlHttpReferenceExpectError500
* POST BP 5.3 XX importMultiplePlansWfsFcAdditionalObjectsGml
* POST BP 5.3 XX importPlan
* POST BP 5.3 XX importPlanInvalidXSSExpectError400
* POST BP 5.3 XX importPlanWithText
* POST BP 5.3 XX importPlanWfsFcZip
* POST BP 5.4 sF,sG,Ss importPlanMultiplePlaene
* POST BP 6.0 sF importPlanAsGML
* POST BP 6.0 XX importPlan
* POST BP 6.0 XX importPlanWithPngAndAux
* POST BP 6.0 XX importPlanWithGeoTiff
* POST BP 6.0 XX importPlanWithGeoTiffAndAux
* POST BP 6.0.2 XX importPlan
* POST BP 6.0.2 XX importPlanNoBereichExpectError422
* POST BP 6.0 XX importPlanUnsupportedContentTypeExpectError415
* POST BP 6.0 XX importPlanWithSyntacticErrorExpectError422
* POST FP 5.0 sF,sG,Ss importPlan
* POST FP 6.0.2 XX importPlan
* POST LP 6.0 sF,sG,Ss importPlan
* POST RP 5.1 sF,sG importPlan
* POST SO 5.3 sF,sG,Ss,iI importPlan
* POST XX X.X XX importFileUnsupportedContentTypeExpectError415
* Property Transfer
## /plan/name/{planName} TestCase
* GET BP 4.1 pN currentStatus
* GET XX X.X pN invalidPlanName
* GET BP 4.1 pN invalidAcceptHeaderExpectError406
## /plans TestCase
* GET BP 4.1 pN exactMatch
* GET BP 4.1 pN lowercaseExactMatch
* GET BP 5.4 pI findById
* GET BP 5.4 pI,pN invalidPlanNameExpectError400
## /plan/{planId}/aenderungen/ TestCase
* GET BP 5.0 pI currentStatus
* PUT BP 5.0 pI modifyStatusXSSExpectError400
* PUT BP 5.0 pI modifyStatus
* GET BP 5.0 pI verifyStatus
* PUT BP 5.0 pI modifyStatusEmpty
* GET BP 5.0 pI verifyStatusEmpty
* GET FP 5.0 pI currentStatus
* PUT FP 5.0 pI modifyStatus
* GET FP 5.0 pI verifyStatus
* GET LP 6.0 pI currentStatus
* PUT LP 6.0 pI modifyStatus
* GET LP 6.0 pI verifyStatus
* GET RP 5.1 pI currentStatus
* PUT RP 5.1 pI modifyStatus
* GET RP 5.1 pI verifyStatus
* GET SO 5.3 pI currentStatus
* PUT SO 5.3 pI modifyStatus
* GET SO 5.3 pI verifyStatus
## /plan/{planId}/archive TestCase
* GET BP 5.4 pI getArchiveByWithoutAcceptHeader
* GET BP 5.4 pI getArchiveByIdExplicitAcceptHeader
* GET BP 5.4 pI getArchiveByIdInvalidAcceptHeaderExpectError
* GET BP 5.4 pI getArchiveByIdInvalidPlanIdExpectError
* GET BP 5.4 pI getArchiveByUnknownPlanIdExpectError
## /plan/{planId}/basisdaten/ TestCase
* GET BP 4.1 pI currentStatus
* PUT BP 4.1 pI modifyStatusXSSExpectError400
* PUT BP 4.1 pI modifyStatus
* GET BP 4.1 pI verifyStatus
* GET FP 5.0 pI currentStatus
* PUT FP 5.0 pI modifyStatus
* GET FP 5.0 pI verifyStatus
* GET LP 6.0 pI currentStatus
* PUT LP 6.0 pI modifyStatus
* GET LP 6.0 pI verifyStatus
* GET RP 5.1 pI currentStatus
* PUT RP 5.1 pI modifyStatus
* GET RP 5.1 pI verifyStatus
* GET SO 5.3 pI currentStatus
* PUT SO 5.3 pI modifyStatus
* GET SO 5.3 pI verifyStatus
## /plan/{planId}/dokument/ TestCase
* GET BP 5.0 pI currentStatus
* POST BP 5.0 pI modifyStatus
* skipNextStepIfJdbcUrlMissing
* JDBC BP 5.0 XX checkDokument
* GET BP 5.0 pI verifyStatus
* POST BP 5.0 pI expectErrorDokumentAlreadyExists
* GET FP 5.0 pI currentStatus
* POST FP 5.0 pI modifyStatus
* skipNextStepIfJdbcUrlMissing 2
* JDBC FP 5.0 XX checkDokument
* GET FP 5.0 pI verifyStatus
* GET LP 6.0 pI currentStatus
* POST LP 6.0 pI modifyStatus
* skipNextStepIfJdbcUrlMissing 3
* JDBC LP 6.0 XX checkDokument
* GET LP 6.0 pI verifyStatus
* GET RP 5.1 pI currentStatus
* POST RP 5.1 pI modifyStatus
* skipNextStepIfJdbcUrlMissing 4
* JDBC RP 5.1 XX checkDokument
* GET RP 5.1 pI verifyStatus
* GET SO 5.3 pI currentStatus
* POST SO 5.3 pI modifyStatus
* skipNextStepIfJdbcUrlMissing 5
* JDBC SO 5.3 XX checkDokument
* GET SO 5.3 pI verifyStatus
* Property Transfer
## /plan/{planId}/dokument/{id} TestCase
* GET BP 5.0 pI,id currentStatus
* PUT BP 5.0 pI,id modifyStatusXSSExpectError400
* PUT BP 5.0 pI,id modifyStatus
* skipNextStepIfJdbcUrlMissing
* JDBC BP 5.0 XX checkDokument
* Property Transfer BP 5.0
* PUT BP 5.0 pI,id modifyStatusExpectError
* DEL BP 5.0 pI,id modifyStatus
* GET BP 5.0 pI,id verifyStatus
* PUT FP 5.0 pI,id modifyStatus
* skipNextStepIfJdbcUrlMissing 2
* JDBC FP 5.0 XX checkDokument
* Property Transfer FP 5.0
* GET FP 5.0 pI,id currentStatus
* DEL FP 5.0 pI,id modifyStatus
* GET FP 5.0 pI,id verifyStatusExpectError
* PUT LP 6.0 pI,id modifyStatus
* skipNextStepIfJdbcUrlMissing 3
* JDBC LP 6.0 XX checkDokument
* Property Transfer LP 6.0
* GET LP 6.0 pI,id currentStatus
* DEL LP 6.0 pI,id modifyStatus
* GET LP 6.0 pI,id verifyStatusExpectError
* PUT RP 5.1 pI,id modifyStatus
* skipNextStepIfJdbcUrlMissing 4
* JDBC RP 5.1 XX checkDokument
* Property Transfer RP 5.1
* GET RP 5.1 pI,id currentStatus
* DEL RP 5.1 pI,id modifyStatus
* GET RP 5.1 pI,id verifyStatusExpectError
* PUT SO 5.3 pI,id modifyStatus
* skipNextStepIfJdbcUrlMissing 5
* JDBC SO 5.3 XX checkDokument
* Property Transfer SO 5.3
* GET SO 5.3 pI,id currentStatus
* DEL SO 5.3 pI,id modifyStatus
* GET SO 5.3 pI,id verifyStatusExpectError
* skipNextStepIfJdbcUrlMissing 6
* JDBC XX XX XX checkDokumentDelete
* Cleanup
## /plan/{planId}/gueltigkeit/ TestCase
* GET BP 4.1 pI currentStatus
* PUT BP 4.1 pI modifyStatus
* GET BP 4.1 pI verifyStatus
* PUT BP 4.1 pI modifyStatusNull
* GET BP 4.1 pI verifyStatusNull
## /plan/{planId}/rasterbasis TestCase
* GET BP 5.3 pI currentStatus
* GET BP 5.2 pI currentStatusWithoutBereich
* POST BP 5.3 pI modifyStatus
* skipNextStepIfJdbcUrlMissing
* JDBC BP 5.3 XX checkRasterbasis
* POST BP 5.2 pI modifyStatusWithoutBereichExpectError
* GET BP 5.3 pI verifyStatus
* POST BP 5.3 pI modifyStatusWithoutBereichNummerExpectError
* POST XX X.X XX modifyStatusWithInvalidPlanIdExpectError
* GET BP 5.3 pI currentStatusWithBereiche
* POST BP 5.3 pI modifyStatusWithBereiche
* GET BP 5.3 pI verifyStatusBereiche
* POST BP 6.0 pI modifyStatusGML
* GET BP 6.0 pI currentStatusGML
* GET FP 5.0 pI currentStatus
* POST FP 5.0 pI modifyStatus
* skipNextStepIfJdbcUrlMissing 2
* JDBC FP 5.0 XX checkRasterbasis
* GET FP 5.0 pI verifyStatus
* GET LP 6.0 pI currentStatus
* POST LP 6.0 pI modifyStatusXSSExpectError400
* POST LP 6.0 pI modifyStatus
* skipNextStepIfJdbcUrlMissing 3
* JDBC LP 6.0 XX checkRasterbasis
* GET LP 6.0 pI verifyStatus
* GET RP 5.1 pI currentStatus
* POST RP 5.1 pI modifyStatus
* skipNextStepIfJdbcUrlMissing 4
* JDBC RP 5.1 XX checkRasterbasis
* GET RP 5.1 pI verifyStatus
* GET SO 5.3 pI currentStatus
* POST SO 5.3 pI modifyStatus
* skipNextStepIfJdbcUrlMissing 5
* JDBC SO 5.3 XX checkRasterbasis
* GET SO 5.3 pI verifyStatus
* Property Transfer
## /plan/{planId}/rasterbasis/{id} TestCase
* GET BP 5.3 pI,id currentStatus
* PUT BP 5.3 pI,id modifyStatus
* Property Transfer BP 5.3
* skipNextStepIfJdbcUrlMissing
* JDBC BP 5.3 XX checkRasterbasis
* PUT BP 5.3 pI,id modifyStatusUnsupportedContentTypeExpectError
* GET BP 5.3 pI,id verifyStatus
* DEL BP 5.3 pI,id modifyStatus
* GET BP 5.3 pI,id verifyStatusExpectError404
* GET BP 5.3 pI,id verifyStatusExpectError404Part2
* PUT BP 5.3 pI,id modifyStatusWithoutBereichNummerExpectError400
* PUT BP 5.3 pI,id modifyStatusWithInvalidPlanIdExpectError404
* GET BP 5.2 pI,id currentStatusWithBereiche
* DEL BP 5.2 pI,id modifyStatusWithBereiche
* GET BP 5.2 pI currentStatusWithBereiche
* PUT BP 5.2 pI,id modifyStatusWithBereiche
* GET BP 5.2 pI,id verifyStatusWithBereicheBereicheExpectError404
* GET BP 5.2 pI,id verifyStatusWithBereicheBereicheExpectError404Part2
* GET BP 6.0 pI,id currentStatusGML
* DEL BP 6.0 pI,id modifyStatusGML
* PUT FP 5.0 pI,id modifyStatus
* Property Transfer FP 5.0
* skipNextStepIfJdbcUrlMissing 2
* JDBC FP 5.0 XX checkRasterbasis
* GET FP 5.0 pI,id currentStatus
* DEL FP 5.0 pI,id modifyStatus
* GET FP 5.0 pI,id verifyStatusExpectError404
* PUT LP 6.0 pI,id modifyStatus
* Property Transfer LP 6.0
* skipNextStepIfJdbcUrlMissing 3
* JDBC LP 6.0 XX checkRasterbasis
* GET LP 6.0 pI,id currentStatus
* DEL LP 6.0 pI,id modifyStatus
* GET LP 6.0 pI,id verifyStatusExpectError404
* PUT RP 5.1 pI,id modifyStatus
* Property Transfer RP 5.1
* skipNextStepIfJdbcUrlMissing 4
* JDBC RP 5.1 XX checkRasterbasis
* GET RP 5.1 pI,id currentStatus
* DEL RP 5.1 pI,id modifyStatus
* GET RP 5.1 pI,id verifyStatusExpectError404
* PUT SO 5.3 pI,id modifyStatus
* Property Transfer SO 5.3
* skipNextStepIfJdbcUrlMissing 5
* JDBC SO 5.3 XX checkRasterbasis
* GET SO 5.3 pI,id currentStatus
* DEL SO 5.3 pI,id modifyStatus
* GET SO 5.3 pI,id verifyStatusExpectError404
* skipNextStepIfJdbcUrlMissing 6
* JDBC XX XX XX checkRasterbasisDelete
* Cleanup
## /plan/{planId}/text/ TestCase
* GET BP 5.3 pI currentStatus
* POST BP 5.3 pI modifyStatus
* GET BP 5.3 pI verifyStatus
* GET BP 5.4 pI currentStatusMultiplePlaene
* GET BP 6.0 pI currentStatus
* POST BP 6.0 pI modifyStatus
* skipNextStepIfJdbcUrlMissing
* JDBC BP 6.0 XX checkText
* GET BP 6.0 pI verifyStatus
* GET FP 5.0 pI currentStatus
* POST FP 5.0 pI modifyStatus
* skipNextStepIfJdbcUrlMissing 2
* JDBC FP 5.0 XX checkText
* GET FP 5.0 pI verifyStatus
* GET LP 6.0 pI currentStatus
* POST LP 6.0 pI modifyStatus
* skipNextStepIfJdbcUrlMissing 3
* JDBC LP 6.0 XX checkText
* GET LP 6.0 pI verifyStatus
* GET RP 5.1 pI currentStatus
* POST RP 5.1 pI modifyStatus
* skipNextStepIfJdbcUrlMissing 4
* JDBC RP 5.1 XX checkText
* GET RP 5.1 pI verifyStatus
* GET SO 5.3 pI currentStatus
* POST SO 5.3 pI modifyStatus
* skipNextStepIfJdbcUrlMissing 5
* JDBC SO 5.3 XX checkText
* GET SO 5.3 pI verifyStatus
* Property Transfer
## /plan/{planId}/text/{id} TestCase
* GET BP 5.3 pI,id currentStatus
* PUT BP 5.3 pI,id modifyStatusXSSExpectError400
* PUT BP 5.3 pI,id modifyStatus
* skipNextStepIfJdbcUrlMissing
* JDBC BP 5.3 XX checkText
* PUT BP 5.3 pI,id modifyStatusUnsupportedContentTypeExpectError415
* GET BP 5.3 pI,id verifyStatus
* GET BP 5.3 pI,id verifyStatusExpectError
* PUT FP 5.0 pI,id modifyStatus
* skipNextStepIfJdbcUrlMissing 2
* JDBC FP 5.0 XX checkText
* GET FP 5.0 pI,id currentStatus
* PUT LP 6.0 pI,id modifyStatus
* skipNextStepIfJdbcUrlMissing 3
* JDBC LP 6.0 XX checkText
* GET LP 6.0 pI,id currentStatus
* PUT RP 5.1 pI,id modifyStatus
* skipNextStepIfJdbcUrlMissing 4
* JDBC RP 5.1 XX checkText
* GET RP 5.1 pI,id currentStatus
* PUT SO 5.3 pI,id modifyStatus
* skipNextStepIfJdbcUrlMissing 5
* JDBC SO 5.3 XX checkText
* GET SO 5.3 pI,id currentStatus
## /plan/{planId} TestCase
* GET BP 4.1 pI currentStatus
* GET BP 4.1 pI currentStatusAcceptApplication/XML
* GET BP 4.1 pI currentStatusAcceptApplication/ZIP
* GET BP 4.1 pI getById currentStatusInvalidPlanIdExpectError400
* GET SO 5.3 iI internalId
* DEL BP 4.0 pI deletePlan
* DEL BP 4.1 pI deletePlan
* DEL BP 4.1 pI deletePlanMimeTypeV2
* DEL BP 5.0 pI deletePlan
* DEL BP 5.0 pI deletePlanWithAenderung
* DEL BP 5.0 pI deletePlanWithDokument
* DEL BP 5.2 pI deletePlanWithBereiche
* DEL BP 5.2 pI deletePlanWithoutBereich
* DEL BP 5.2 pI deletePlan
* DEL BP 5.3 pI deletePlanWithText
* DEL BP 5.3 pI deletePlan
* DEL BP 5.3 pI deleteMultiplePlansWfsFcAdditionalObjects0
* DEL BP 5.3 pI deleteMultiplePlansWfsFcAdditionalObjects1
* DEL BP 5.3 pI deletePlanWfsFc
* DEL BP 5.4 pI deletePlanWithMultiplePlaeneHafen11
* DEL BP 5.4 pI deletePlanWithMultiplePlaeneHafen14
* DEL BP 6.0 pI deletePlan
* DEL BP 6.0 pI deletePlanWithPngAndAux
* DEL BP 6.0 pI deletePlanWithGeoTiff
* DEL BP 6.0 pI deletePlanWithGeoTiffAndAux
* DEL BP 6.0.2 pI deletePlan
* DEL BP 6.0 pI deletePlanAsGML
* DEL BP 6.0 pI deletePlanInvalidPlanIdExpectError400
* DEL BP 6.0 pI deletePlanNotExistingPlanIdExpectError404
* DEL FP 5.0 pI deletePlan
* DEL FP 6.0.2 pI deletePlan
* DEL SO 5.3 pI deletePlan
* DEL LP 6.0 pI deletePlan
* DEL RP 5.1 pI deletePlan
## Cleanup Properties
* Property Transfer
# Workflow v1 TestSuite
## Raster evaluation TestCase
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlan
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlan Property Transfer
* DEL BP 6.0 pI deletePlan
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlanFailingValidationExpectError422
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlanWithMissingPNGExpectError400
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlanWithWrongCRSExpectError400
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlanWithNotSupportedFormatExpectError415
* Cleanup Properties
## Validation invalid TestCase
* POST BP 5.2 XX importPlanGeometricInvalid422
* POST BP 6.0 XX importPlanSemanticInvalidExpectError422
* POST BP 6.0 XX importPlanSyntacticInvalidExpectError422
# Profiles v1 TestSuite
## /info TestCase
* GET XX X.X XX showConfig
* Property Transfer
## /plan TestCase
* POST BP 4.1 sF,sS,pf importPlanWithProfil
* Property Transfer
## /plan/{planId} TestCase
* DEL BP 4.1 pI deletePlanWithProfil
## Cleanup Properties
* Property Transfer
# XPlanManagerAPI v2 TestSuite
## / TestCase
* GET XX X.X XX openAPI
* GET XX X.X XX invalidAcceptHeaderExpectError406
## /info TestCase
* GET XX X.X XX showConfig
* GET XX X.X XX invalidAcceptHeaderExpectError406
## /plan TestCase
* POST BP 6.0.2 xF,sS,sF,sG,sL,iI,pS importPlan
* POST BP 6.0.2 xF,sS,sF,sG,sL,iI,pS importPlan Property Transfer
* POST BP 6.0.2 pf invalidProfile
* POST BP 6.0.2 pf invalidProfile Property Transfer
* POST BP 6.0.2 XX mismatchingContentTypeAndFileType
* POST BP 6.0.2 XX mismatchingContentTypeAndFileType Property Transfer
* POST BP 6.0.2 XX invalidXSSExpectError400
* POST BP 6.0.2 XX invalidAcceptHeaderExpectError406
* POST BP 6.0 XX unsupportedMediaTypeZipExpectError415
* POST XX X.X XX unsupportedMediaTypeOdtExpectError415
## /status/{uuid} TestCase
* GET BP 6.0.2 XX pollStatus
* pollStatus
* GET BP 6.0.2 XX retrieveStatus
* GET BP 6.0.2 XX retrieveStatus Property Transfer
* GET BP 6.0.2 XX pollStatusInvalidProfile
* pollStatusInvalidProfile
* GET BP 6.0.2 XX invalidProfile
* GET BP 6.0.2 XX pollStatusMismatchingTypes
* pollStatusMismatchingTypes
* GET BP 6.0.2 XX mismatchingContentTypeAndFileType
* GET BP 6.0.2 XX invalidUuidExpectError404
* GET BP 6.0.2 XX invalidAcceptHeaderExpectError406
## /report/{uuid} TestCase
* GET BP 6.0.2 XX retrieveReportJson
* GET BP 6.0.2 XX retrieveReportPdf
* GET BP 6.0.2 XX invalidUuidExpectError404
* GET BP 6.0.2 XX invalidAcceptHeaderExpectError406
## /report/{uuid}/geomfindings TestCase
* GET BP 6.0.2 XX retrieveGeomfindings
* GET BP 6.0.2 XX invalidUuidExpectError404
* GET BP 6.0.2 XX invalidAcceptHeaderExpectError406
## /report/{uuid}/geomfindings.json TestCase
* GET BP 6.0.2 XX retrieveGeomfindings
* GET BP 6.0.2 XX retrieveGeomfindingsNoAcceptHeader
* GET BP 6.0.2 XX invalidUuidExpectError404
* GET BP 6.0.2 XX invalidAcceptHeaderExpectError406
## /plans TestCase
* GET BP 6.0.2 pN getByPlanName
* GET BP 6.0.2 pI getByPlanId
* GET BP 6.0.2 pN invalidAcceptHeaderExpectError406
## /plan/{planId}/archive TestCase
* GET BP 6.0.2 pI getArchiveById
* GET BP 6.0.2 pI invalidPlanIdNotIntValueExpectError400
* GET BP 6.0.2 pI notExistingPlanIdExpectError404
* GET BP 6.0.2 pI invalidAcceptHeaderExpectError406
## /plan/{planId}/basisdaten TestCase
* GET BP 6.0.2 pI getBasisdaten
* GET BP 6.0.2 pI invalidPlanIdNotIntValueExpectError400
* GET BP 6.0.2 pI notExistingPlanIdExpectError404
* GET BP 6.0.2 pI invalidAcceptHeaderExpectError406
* PUT BP 6.0.2 pI replaceBasisdaten
* PUT BP 6.0.2 pI invalidPlanIdNotIntValueExpectError400
* PUT BP 6.0.2 pI notExistingPlanIdExpectError404
* PUT BP 6.0.2 pI invalidAcceptHeaderExpectError406
* PUT BP 6.0.2 pI xssExpectError400
## /plan/{planId}/aenderungen TestCase
* GET BP 6.0.2 pI getAenderungen
* GET BP 6.0.2 pI invalidPlanIdNotIntValueExpectError400
* GET BP 6.0.2 pI notExistingPlanIdExpectError404
* GET BP 6.0.2 pI invalidAcceptHeaderExpectError406
* PUT BP 6.0.2 pI replaceAenderung
* PUT BP 6.0.2 pI invalidPlanIdNotIntValueExpectError400
* PUT BP 6.0.2 pI notExistingPlanIdExpectError404
* PUT BP 6.0.2 pI invalidAcceptHeaderExpectError406
* PUT BP 6.0.2 pI xssExpectError400
## /plan/{planId}/text TestCase
* GET BP 6.0.2 pI getTexte
* GET BP 6.0.2 pI invalidPlanIdNotIntValueExpectError400
* GET BP 6.0.2 pI notExistingPlanIdExpectError404
* GET BP 6.0.2 pI invalidAcceptHeaderExpectError406
* POST BP 6.0.2 pI addText
* POST BP 6.0.2 pI addText Property Transfer
* POST BP 6.0.2 pI invalidPlanIdNotIntValueExpectError400
* POST BP 6.0.2 pI notExistingPlanIdExpectError404
* POST BP 6.0.2 pI invalidAcceptHeaderExpectError406
* POST BP 6.0.2 pI invalidMediaTypeExpectError415
* POST BP 6.0.2 pI invalidContentInJsonExpectError400
## /plan/{planId}/text/{id} TestCase
* GET BP 6.0.2 pI,id getTextById
* GET BP 6.0.2 pI,id invalidPlanIdNotIntValueExpectError400
* GET BP 6.0.2 pI,id notExistingIdExpectError404
* GET BP 6.0.2 pI,id invalidAcceptHeaderExpectError406
* PUT BP 6.0.2 pI,id replaceTextById
* PUT BP 6.0.2 pI,id invalidPlanIdNotIntValueExpectError400
* PUT BP 6.0.2 pI,id notExistingIdExpectError404
* PUT BP 6.0.2 pI,id invalidAcceptHeaderExpectError406
* PUT BP 6.0.2 pI,id invalidMediaTypeExpectError415
* PUT BP 6.0.2 pI,id invalidContentInJsonExpectError400
## /plan/{planId}/dokument TestCase
* GET BP 6.0.2 pI getDokumente
* GET BP 6.0.2 pI invalidPlanIdNotIntValueExpectError400
* GET BP 6.0.2 pI notExistingPlanIdExpectError404
* GET BP 6.0.2 pI invalidAcceptHeaderExpectError406
* POST BP 6.0.2 pI addDokument
* POST BP 6.0.2 pI addDokument Property Transfer
* POST BP 6.0.2 pI invalidPlanIdNotIntValueExpectError400
* POST BP 6.0.2 pI notExistingPlanIdExpectError404
* POST BP 6.0.2 pI invalidAcceptHeaderExpectError406
* POST BP 6.0.2 pI invalidMediaTypeExpectError415
* POST BP 6.0.2 pI invalidContentInJsonExpectError400
## /plan/{planId}/dokument/{id} TestCase
* GET BP 6.0.2 pI,id getDokumentById
* GET BP 6.0.2 pI,id invalidPlanIdNotIntValueExpectError400
* GET BP 6.0.2 pI,id notExistingIdExpectError404
* GET BP 6.0.2 pI,id invalidAcceptHeaderExpectError406
* PUT BP 6.0.2 pI,id invalidPlanIdNotIntValueExpectError400
* PUT BP 6.0.2 pI,id notExistingIdExpectError404
* PUT BP 6.0.2 pI,id invalidAcceptHeaderExpectError406
* PUT BP 6.0.2 pI,id invalidMediaTypeExpectError415
* PUT BP 6.0.2 pI,id invalidContentInJsonExpectError400
* PUT BP 6.0.2 pI,id replaceDokumentById
* PUT BP 6.0.2 pI,id replaceDokumentById Property Transfer
* DEL BP 6.0.2 pI,id invalidPlanIdNotIntValueExpectError400
* DEL BP 6.0.2 pI,id notExistingIdExpectError404
* DEL BP 6.0.2 pI,id invalidAcceptHeaderExpectError406
* DEL BP 6.0.2 pI,id deleteDokumentById
* Cleanup Properties
## /plan/{planId}/rasterbasis TestCase
* GET BP 6.0.2 pI getRasterBasis
* GET BP 6.0.2 pI invalidPlanIdNotIntValueExpectError400
* GET BP 6.0.2 pI notExistingPlanIdExpectError404
* GET BP 6.0.2 pI invalidAcceptHeaderExpectError406
* POST BP 6.0.2 pI addRasterBasis
* POST BP 6.0.2 pI addRasterBasis Property Transfer
* POST BP 6.0.2 pI invalidPlanIdNotIntValueExpectError400
* POST BP 6.0.2 pI notExistingPlanIdExpectError404
* POST BP 6.0.2 pI invalidAcceptHeaderExpectError406
* POST BP 6.0.2 pI invalidMediaTypeExpectError415
* POST BP 6.0.2 pI invalidContentInJsonExpectError400
## /plan/{planId}/rasterbasis/{id} TestCase
* GET BP 6.0.2 pI,id getRasterbasisById
* GET BP 6.0.2 pI,id invalidPlanIdNotIntValueExpectError400
* GET BP 6.0.2 pI,id notExistingIdExpectError404
* GET BP 6.0.2 pI,id invalidAcceptHeaderExpectError406
* PUT BP 6.0.2 pI,id invalidPlanIdNotIntValueExpectError400
* PUT BP 6.0.2 pI,id notExistingIdExpectError404
* PUT BP 6.0.2 pI,id invalidAcceptHeaderExpectError406
* PUT BP 6.0.2 pI,id invalidMediaTypeExpectError415
* PUT BP 6.0.2 pI,id invalidContentInJsonExpectError400
* PUT BP 6.0.2 pI,id replaceRasterbasisById
* PUT BP 6.0.2 pI,id replaceRasterbasisById Property Transfer
* DEL BP 6.0.2 pI,id invalidPlanIdNotIntValueExpectError400
* DEL BP 6.0.2 pI,id notExistingIdExpectError404
* DEL BP 6.0.2 pI,id invalidAcceptHeaderExpectError406
* DEL BP 6.0.2 pI,id deleteRasterbasisById
* Cleanup Properties
## /plan/{planId} TestCase
* GET BP 6.0.2 pI acceptApplicationJson
* GET BP 6.0.2 pI invalidPlanIdNotIntValueExpectError400
* GET BP 6.0.2 pI notExistingPlanIdExpectError404
* GET BP 6.0.2 pI invalidAcceptHeaderExpectError406
* DEL BP 6.0.2 pI invalidPlanIdNotIntValueExpectError400
* DEL BP 6.0.2 pI notExistingPlanIdExpectError404
* DEL BP 6.0.2 pI invalidAcceptHeaderExpectError406
* DEL BP 6.0.2 pI deletePlan
## Cleanup Properties
* Property Transfer
# Workflow v2 TestSuite
## Raster evaluation TestCase
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlan
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlan Property Transfer
* GET BP 6.0 XX pollStatus
* pollStatus
* GET BP 6.0 XX retrieveStatus
* GET BP 6.0 XX retrieveStatus Property Transfer
* GET BP 6.0 XX retrieveReportJson
* GET BP 6.0 XX retrieveReportPdf
* DEL BP 6.0 pI deletePlan
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlanNotSupportedFormatExpectError415
* Cleanup Properties
## Raster evaluation missing PNG TestCase
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlan
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlan Property Transfer
* GET BP 6.0 XX pollStatus
* pollStatus
* GET BP 6.0 XX retrieveStatus
* GET BP 6.0 XX retrieveReportJson
* GET BP 6.0 XX retrieveReportPdf
* Cleanup Properties
## Raster evaluation wrong CRS TestCase
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlan
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlan Property Transfer
* GET BP 6.0 XX pollStatus
* pollStatus
* GET BP 6.0 XX retrieveStatus
* GET BP 6.0 XX retrieveReportJson
* GET BP 6.0 XX retrieveReportPdf
* Cleanup Properties
## Profiles TestCase
* GET XX X.X XX showConfig
* GET XX X.X XX showConfig Property Transfer
* POST BP 6.0.2 xF,sS,sF,sG,sL,iI,pS,pf importPlan
* POST BP 6.0.2 xF,sS,sF,sG,sL,iI,pS,pf importPlan Property Transfer
* GET BP 6.0.2 XX pollStatus
* pollStatus
* GET BP 6.0.2 XX retrieveStatus
* GET BP 6.0.2 XX retrieveStatus Property Transfer
* GET BP 6.0.2 XX retrieveReportJson
* GET BP 6.0.2 XX retrieveReportPdf
* DEL BP 6.0.2 XX deletePlan
* Cleanup Properties
## Validation geometric error TestCase
* POST BP 5.2 XX importPlan
* POST BP 5.2 XX importPlan Property Transfer
* GET BP 5.2 XX pollStatus
* pollStatus
* GET BP 5.2 XX retrieveStatus
* GET BP 5.2 XX retrieveReportJson
* GET BP 5.2 XX retrieveReportPdf
* GET BP 5.2 XX retrieveGeomfindings
* Cleanup Properties
## Validation semantic error TestCase
* POST BP 6.0 XX importPlan
* POST BP 6.0 XX importPlan Property Transfer
* GET BP 6.0 XX pollStatus
* pollStatus
* GET BP 6.0 XX retrieveStatus
* GET BP 6.0 XX retrieveReportJson
* GET BP 6.0 XX retrieveReportPdf
* Cleanup Properties
## Validation syntactic error TestCase
* POST BP 6.0 XX importPlan
* POST BP 6.0 XX importPlan Property Transfer
* GET BP 6.0 XX pollStatus
* pollStatus
* GET BP 6.0 XX retrieveStatus
* GET BP 6.0 XX retrieveReportJson
* GET BP 6.0 XX retrieveReportPdf
* Cleanup Properties
## XPlanGML with multiple plans TestCase
* POST BP 5.4 XX importPlan
* POST BP 5.4 XX importPlan Property Transfer
* GET BP 5.4 XX pollStatus
* pollStatus
* GET BP 5.4 XX retrieveStatus
* GET BP 5.4 XX retrieveStatus Property Transfer
* GET BP 5.4 XX retrieveReportJson
* GET BP 5.4 XX retrieveReportPdf
* DEL BP 5.4 XX deleteFirstPlan
* DEL BP 5.4 XX deleteSecondPlan
* Cleanup Properties
## XPlanGML with multiple plans syntactic error TestCase
* POST BP 5.4 XX importPlan
* POST BP 5.4 XX importPlan Property Transfer
* GET BP 5.4 XX pollStatus
* pollStatus
* GET BP 5.4 XX retrieveStatus
* GET BP 5.4 XX retrieveReportJson
* GET BP 5.4 XX retrieveReportPdf
* Cleanup Properties
## XPlanGML with multiple plans geometric error TestCase
* POST BP 5.4 XX importPlan
* POST BP 5.4 XX importPlan Property Transfer
* GET BP 5.4 XX pollStatus
* pollStatus
* GET BP 5.4 XX retrieveStatus
* GET BP 5.4 XX retrieveReportJson
* GET BP 5.4 XX retrieveReportPdf
* Cleanup Properties
## XPlanGML with multiple plans semantic error TestCase
* POST BP 5.4 XX importPlan
* POST BP 5.4 XX importPlan Property Transfer
* GET BP 5.4 XX pollStatus
* pollStatus
* GET BP 5.4 XX retrieveStatus
* GET BP 5.4 XX retrieveReportJson
* GET BP 5.4 XX retrieveReportPdf
* Cleanup Properties
## XPlanGML with multiple plans skip errors TestCase
* POST BP 5.4 sS,sF,sG,sL importPlan
* POST BP 5.4 XX importPlan Property Transfer
* GET BP 5.4 XX pollStatus
* pollStatus
* GET BP 5.4 XX retrieveStatus
* GET BP 5.4 XX retrieveStatus Property Transfer
* GET BP 5.4 XX retrieveReportJson
* GET BP 5.4 XX retrieveReportPdf
* DEL BP 5.4 XX deleteFirstPlan
* DEL BP 5.4 XX deleteSecondPlan
* Cleanup Properties
## Security with JWT TestCase
* checkPreconditions
* GET XX X.X XX openAPI
* retrieveToken - userNotDortmund
* retrieveToken - userDortmund
* retrieveToken Property Transfer
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlan Dortmund
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlan Dortmund Property Transfer
* GET BP 6.0 XX pollStatus
* pollStatus
* GET BP 6.0 XX retrieveStatus
* GET BP 6.0 XX retrieveStatus Property Transfer
* POST BP 6.0 xF,sS,sF,sG,sL,iI,pS importPlan NotDortmund ExpectError403
* DEL BP 6.0 pI deletePlan NotDortmund ExpectError403
* DEL BP 6.0 pI deletePlan Dortmund
* Cleanup Properties
# Cleanup
## JWT Authentication
* removeAuthorizationHeaders
