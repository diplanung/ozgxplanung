/*-
 * #%L
 * xplan-cli - Kommandozeilenwerkzeuge fuer die xPlanBox
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.cli.manage;

import de.latlon.xplan.manager.XPlanManager;
import de.latlon.xplanbox.cli.XPlanCliSubcommand;
import de.latlon.xplanbox.cli.manage.config.ManageContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public abstract class ManagerSubcommand extends XPlanCliSubcommand {

	private AnnotationConfigApplicationContext applicationContext;

	public Integer callSubcommand(ApplicationContext applicationContext) {
		XPlanManager xPlanManager = applicationContext.getBean(XPlanManager.class);
		return callSubcommand(xPlanManager);
	}

	public void register(AnnotationConfigApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
		applicationContext.register(ManageContext.class);
	}

	abstract Integer callSubcommand(XPlanManager xPlanManager);

	protected <T extends Object> T getBean(Class<T> _class) {
		return applicationContext.getBean(_class);
	}

}
