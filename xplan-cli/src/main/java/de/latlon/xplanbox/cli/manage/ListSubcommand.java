/*-
 * #%L
 * xplan-cli - Kommandozeilenwerkzeuge fuer die xPlanBox
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.cli.manage;

import java.util.List;

import de.latlon.xplan.core.manager.db.model.Gemeinde;
import de.latlon.xplan.manager.XPlanManager;
import de.latlon.xplan.manager.database.XPlanDao;
import de.latlon.xplan.manager.web.shared.XPlan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
@Component
@CommandLine.Command(name = "list", description = "List all plans that are available in the data storage.",
		subcommands = { CommandLine.HelpCommand.class })
public class ListSubcommand extends ManagerSubcommand {

	private static final Logger LOG = LoggerFactory.getLogger(ListSubcommand.class);

	@Override
	public Integer callSubcommand(XPlanManager xPlanManager) {
		System.out.println("---------------------------------------");
		try {
			XPlanDao xPlanDao = getBean(XPlanDao.class);
			List<XPlan> xPlanList = xPlanDao.getXPlanList();
			printList(xPlanList, xPlanDao);
			return 0;
		}
		catch (Exception e) {
			LOG.debug("Auflisten der Plaene fehlgeschlagen.", e);
			LOG.error("Auflisten der Plaene fehlgeschlagen. Fehlermeldung: " + e.getLocalizedMessage());
			return 1;
		}
	}

	private void printList(List<XPlan> xpList, XPlanDao xPlanDao) {
		System.out.println("Anzahl Plaene: " + xpList.size());
		for (XPlan plan : xpList) {
			List<Gemeinde> gemeinden = xPlanDao.retrieveGemeindeOfPlanWithId(plan.getId());
			System.out.print("- Id: " + plan.getId());
			System.out.print(", Version: " + plan.getVersion());
			System.out.print(", Typ: " + plan.getType());
			System.out.print(", Name: " + plan.getName());
			System.out.print(", Nummer: " + plan.getNumber());
			for (Gemeinde gemeinde : gemeinden) {
				System.out.print(", AGS/RS: " + gemeinde.getAgs() + "/" + gemeinde.getRs());
			}
			System.out.print(", Raster: " + (plan.isRaster() ? "ja" : "nein"));
			System.out.print(", Veroeffentlichungsdatum: " + plan.getReleaseDate());
			System.out.println(", Importiert: " + plan.getImportDate());
		}
	}

}
