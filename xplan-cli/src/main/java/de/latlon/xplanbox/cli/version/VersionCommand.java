/*-
 * #%L
 * xplan-cli - Kommandozeilenwerkzeuge fuer die xPlanBox
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.cli.version;

import picocli.CommandLine;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.1
 */
@CommandLine.Command(name = "version", aliases = { "-v", "-V", "v" },
		version = { "xPlanBox %s", "JVM: ${java.version} (${java.vendor} ${java.vm.name} ${java.vm.version})",
				"OS: ${os.name} ${os.version} ${os.arch}" })
public class VersionCommand implements Runnable {

	@Override
	public void run() {
		CommandLine commandLine = new CommandLine(new VersionCommand());
		commandLine.printVersionHelp(System.out, commandLine.getColorScheme().ansi(), parseVersion());
	}

	public String parseVersion() {
		Package thisPackage = getClass().getPackage();
		return thisPackage.getImplementationVersion();
	}

}
