#!/bin/bash
set -e

echo "xplan-cli-docker initialization..."

/xplan-volume-init/setupVolumes.sh

ALL_ADDITIONAL_ARGS=$(env | grep ^JAVA_ADDITIONAL_ | sed 's/^JAVA_ADDITIONAL_[[:alnum:]_]*=//' | tr '\n' ' ')

if [ -n "${ALL_ADDITIONAL_ARGS}" ]; then
    JAVA_OPTS=$(eval "echo $ALL_ADDITIONAL_ARGS")
    echo "xPlanBox JAVA_OPTS set: $JAVA_OPTS"
    export JAVA_OPTS=$JAVA_OPTS
fi

export DEEGREE_WORKSPACE_ROOT=$XPLANBOX_VOLUMES/xplan-workspaces
export XPLANBOX_CONFIG=$XPLANBOX_VOLUMES/xplan-manager-config

echo "Execute: xpb $@"

exec /xplanbox/xplan-cli/bin/xpb "$@"