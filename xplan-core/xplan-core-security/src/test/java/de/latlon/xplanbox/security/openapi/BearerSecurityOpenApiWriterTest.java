/*-
 * #%L
 * xplan-core-security - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.security.openapi;

import static de.latlon.xplanbox.security.openapi.BearerSecurityOpenApiWriter.SECURITY_KEY_BEARER;
import static io.swagger.v3.oas.models.security.SecurityScheme.Type.HTTP;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Map;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class BearerSecurityOpenApiWriterTest {

	private final BearerSecurityOpenApiWriter bearerSecurityOpenApiWriter = new BearerSecurityOpenApiWriter();

	@Test
	void testAddSecurity() {
		OpenAPI openAPI = new OpenAPI();
		bearerSecurityOpenApiWriter.addSecurity(openAPI);

		List<SecurityRequirement> securityRequirements = openAPI.getSecurity();
		assertThat(securityRequirements.size()).isEqualTo(1);
		assertThat(securityRequirements.get(0).containsKey(SECURITY_KEY_BEARER)).isTrue();

		Map<String, SecurityScheme> securitySchemes = openAPI.getComponents().getSecuritySchemes();
		assertThat(securitySchemes.size()).isEqualTo(1);
		assertThat(securitySchemes.get(SECURITY_KEY_BEARER).getType()).isEqualTo(HTTP);
		assertThat(securitySchemes.get(SECURITY_KEY_BEARER).getScheme()).isEqualTo("bearer");
		assertThat(securitySchemes.get(SECURITY_KEY_BEARER).getBearerFormat()).isEqualTo("JWT");

	}

}
