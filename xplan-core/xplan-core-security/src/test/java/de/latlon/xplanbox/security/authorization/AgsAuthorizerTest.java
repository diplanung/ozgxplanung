/*-
 * #%L
 * xplan-core-security - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.security.authorization;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@Import(AgsAuthorizerTest.AgsAuthorizerTestContext.class)
@SpringBootTest(classes = AgsAuthorizer.class)
public class AgsAuthorizerTest {

	@Autowired
	private AgsAuthorizer agsAuthorizer;

	@TestConfiguration
	public static class AgsAuthorizerTestContext {

		@Bean
		public AgsAccessor agsAccessor() {
			AgsAccessor agsAccessor = mock(AgsAccessor.class);
			when(agsAccessor.retrieveAgsFromPlan("1")).thenReturn(Collections.singleton("02000000"));
			when(agsAccessor.retrieveAgsFromPlan("2")).thenReturn(Collections.singleton("088623416"));
			return agsAccessor;
		}

	}

	@Test
	public void testHasAgs_gml_matchingAgs() throws Exception {
		File file = new File(getClass().getResource("BPlan001_6-0.gml").toURI());
		JwtAuthenticationToken authentication = createAuthentication("02000000");
		boolean hasAgs = agsAuthorizer.hasAgs(file, "GML", authentication);

		assertThat(hasAgs).isTrue();
	}

	@Test
	public void testHasAgs_gml_nonMatchingAgs() throws Exception {
		File file = new File(getClass().getResource("BPlan001_6-0.gml").toURI());
		JwtAuthenticationToken authentication = createAuthentication("088623416");
		boolean hasAgs = agsAuthorizer.hasAgs(file, "GML", authentication);

		assertThat(hasAgs).isFalse();
	}

	@Test
	public void testHasAgs_gml_emptyAgsList() throws Exception {
		File file = new File(getClass().getResource("BPlan001_6-0.gml").toURI());
		JwtAuthenticationToken authentication = mock(JwtAuthenticationToken.class);
		OAuth2UserAuthority authority = new OAuth2UserAuthority("role",
				Collections.singletonMap("ags", Collections.emptyList()));
		when(authentication.getAuthorities()).thenReturn(Collections.singletonList(authority));

		boolean hasAgs = agsAuthorizer.hasAgs(file, "GML", authentication);

		assertThat(hasAgs).isFalse();
	}

	@Test
	public void testHasAgs_plaNId_matchingAgs() throws Exception {

		JwtAuthenticationToken authentication = createAuthentication("02000000");
		boolean hasAgs = agsAuthorizer.hasAgs("1", authentication);

		assertThat(hasAgs).isTrue();
	}

	private static JwtAuthenticationToken createAuthentication(String TODO) {
		JwtAuthenticationToken authentication = mock(JwtAuthenticationToken.class);
		OAuth2UserAuthority authority = new OAuth2UserAuthority("role",
				Collections.singletonMap("ags", Collections.singletonList(TODO)));
		when(authentication.getAuthorities()).thenReturn(Collections.singletonList(authority));
		return authentication;
	}

}
