/*-
 * #%L
 * xplan-core-security - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.security.authorization;

import static de.latlon.xplanbox.security.authorization.AgsAuthorizer.OriginFileType.GML;
import static de.latlon.xplanbox.security.authorization.AgsAuthorizer.OriginFileType.ZIP;
import static org.assertj.core.api.Assertions.assertThat;

import javax.xml.stream.XMLStreamException;
import java.io.InputStream;
import java.util.Set;

import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class AgsParserTest {

	private final AgsParser agsParser = new AgsParser();

	@Test
	public void testParseAgs_fromGml() throws XMLStreamException {
		InputStream gml = getClass().getResourceAsStream("BPlan001_6-0.gml");
		Set<String> ags = agsParser.parseAgs(gml, GML);

		assertThat(ags).containsExactly("02000000");
	}

	@Test
	public void testParseAgs_fromZip() throws XMLStreamException {
		InputStream gml = getClass().getResourceAsStream("BPlan001_6-0.zip");
		Set<String> ags = agsParser.parseAgs(gml, ZIP);

		assertThat(ags).containsExactly("02000000");
	}

}
