/*-
 * #%L
 * xplan-core-security - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.security.authorization;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.security.oauth2.jwt.Jwt;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class AgsAndRealmAccessJwtGrantedAuthoritiesConverterTest {

	public static final String AGS = "0569432";

	private final AgsAndRealmAccessJwtGrantedAuthoritiesConverter converter = new AgsAndRealmAccessJwtGrantedAuthoritiesConverter();

	@Test
	public void testConvert_notAdminAndNoAgs() {
		Jwt jwt = Mockito.mock(Jwt.class);
		Map<String, List<String>> realmAccessClaims = Collections.singletonMap("roles",
				List.of("default-roles-xplanbox"));
		when(jwt.getClaim("realm_access")).thenReturn(realmAccessClaims);

		Collection<GrantedAuthority> authorities = converter.convert(jwt);
		assertThat(authorities.size()).isEqualTo(1);
		GrantedAuthority authority = authorities.stream().findFirst().get();
		assertThat(authority).isInstanceOf(SimpleGrantedAuthority.class);
		assertThat(authority.getAuthority()).isEqualTo("ROLE_default-roles-xplanbox");
	}

	@Test
	public void testConvert_AdminAndNoAgs() {
		Jwt jwt = Mockito.mock(Jwt.class);
		Map<String, List<String>> realmAccessClaims = Collections.singletonMap("roles",
				List.of("XPLANBOX_ADMIN", "default-roles-xplanbox"));
		when(jwt.getClaim("realm_access")).thenReturn(realmAccessClaims);

		Collection<GrantedAuthority> authorities = converter.convert(jwt);
		assertThat(authorities.size()).isEqualTo(2);
		authorities.forEach(authority -> assertThat(authority).isInstanceOf(SimpleGrantedAuthority.class));
		authorities.forEach(authority -> assertThat(authority.getAuthority())
			.matches("ROLE_default-roles-xplanbox|ROLE_XPLANBOX_ADMIN"));
	}

	@Test
	public void testConvert_notAdminAndAgs() {
		Jwt jwt = Mockito.mock(Jwt.class);
		Map<String, List<String>> realmAccessClaim = Collections.singletonMap("roles",
				List.of("default-roles-xplanbox"));
		when(jwt.getClaim("realm_access")).thenReturn(realmAccessClaim);
		List<String> agsClaim = Collections.singletonList(AGS);
		when(jwt.getClaimAsStringList("ags")).thenReturn(agsClaim);

		Collection<GrantedAuthority> authorities = converter.convert(jwt);
		assertThat(authorities.size()).isEqualTo(1);
		GrantedAuthority authority = authorities.stream().findFirst().get();
		assertThat(authority).isInstanceOf(OAuth2UserAuthority.class);
		assertThat(authority.getAuthority()).isEqualTo("ROLE_default-roles-xplanbox");
		assertThat(((List) ((OAuth2UserAuthority) authority).getAttributes().get("ags"))).contains(AGS);

	}

}
