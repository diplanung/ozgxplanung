/*-
 * #%L
 * xplan-core-security - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.security.authorization;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@Component
@ConditionalOnBean(AgsAccessor.class)
public class AgsAuthorizer {

	@Autowired
	private AgsAccessor agsAccessor;

	public enum OriginFileType {

		ZIP, GML

	}

	public boolean hasAgs(File body, String fileType, JwtAuthenticationToken authentication)
			throws XMLStreamException, IOException {
		Set<String> agsFromPlan = parseAgs(body, OriginFileType.valueOf(fileType));
		return hasAgs(agsFromPlan, authentication);
	}

	public boolean hasAgs(String planId, JwtAuthenticationToken authentication) {
		Set<String> agsFromPlan = agsAccessor.retrieveAgsFromPlan(planId);
		return hasAgs(agsFromPlan, authentication);
	}

	private static boolean hasAgs(Set<String> agsFromPlan, JwtAuthenticationToken authentication) {
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		return authorities.stream()
			.anyMatch(authority -> authority instanceof OAuth2UserAuthority
					&& ((OAuth2UserAuthority) authority).getAttributes().containsKey("ags")
					&& ((List<String>) ((OAuth2UserAuthority) authority).getAttributes().get("ags")).stream()
						.anyMatch(agsFromPlan::contains));
	}

	private static Set<String> parseAgs(File gmlOrArchive, OriginFileType originalFileType)
			throws XMLStreamException, IOException {
		try (FileInputStream archiveOfGmlStream = new FileInputStream(gmlOrArchive)) {
			AgsParser agsParser = new AgsParser();
			return agsParser.parseAgs(archiveOfGmlStream, originalFileType);
		}
	}

}
