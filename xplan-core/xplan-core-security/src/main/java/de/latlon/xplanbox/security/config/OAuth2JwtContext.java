/*-
 * #%L
 * xplan-core-security - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.security.config;

import static jakarta.servlet.DispatcherType.ERROR;
import static jakarta.servlet.DispatcherType.FORWARD;

import de.latlon.xplanbox.security.authorization.AgsAndRealmAccessJwtGrantedAuthoritiesConverter;
import de.latlon.xplanbox.security.openapi.BearerSecurityOpenApiWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@EnableWebSecurity
@EnableMethodSecurity
@Configuration
@Profile("enableOAuth2JWT")
@ComponentScan(basePackages = { "de.latlon.xplanbox.security.authorization" })
public class OAuth2JwtContext {

	private static final Logger LOG = LoggerFactory.getLogger(OAuth2JwtContext.class);

	@Autowired
	private ApplicationContext applicationContext;

	@Bean
	public JwtAuthenticationConverter jwtAuthenticationConverter() {
		JwtAuthenticationConverter converter = new JwtAuthenticationConverter();
		converter.setJwtGrantedAuthoritiesConverter(new AgsAndRealmAccessJwtGrantedAuthoritiesConverter());
		return converter;
	}

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http, JwtAuthenticationConverter jwtAuthenticationConverter)
			throws Exception {
		LOG.info("Configure security.");
		http.authorizeHttpRequests(authorize -> authorize.dispatcherTypeMatchers(FORWARD, ERROR)
			.permitAll()
			.requestMatchers("/xmanager/api/v1", "/xvalidator/api/v1", "/xdokumente/api/v1", "/api/v2", "/actuator/**",
					"/internal/api/traceInfo")
			.permitAll()
			.anyRequest()
			.authenticated())
			.oauth2ResourceServer(
					(oauth2) -> oauth2.jwt(jwt -> jwt.jwtAuthenticationConverter(jwtAuthenticationConverter)));
		return http.build();
	}

	@Bean
	public BearerSecurityOpenApiWriter bearerSecurityOpenApi() {
		return new BearerSecurityOpenApiWriter();
	}

}
