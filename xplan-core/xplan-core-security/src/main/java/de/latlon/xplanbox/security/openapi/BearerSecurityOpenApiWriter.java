/*-
 * #%L
 * xplan-core-security - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.security.openapi;

import static io.swagger.v3.oas.models.security.SecurityScheme.Type.HTTP;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class BearerSecurityOpenApiWriter {

	static final String SECURITY_KEY_BEARER = "Bearer";

	public void addSecurity(OpenAPI openApi) {
		SecurityRequirement securityItem = new SecurityRequirement().addList(SECURITY_KEY_BEARER);
		openApi.addSecurityItem(securityItem);
		SecurityScheme securityScheme = new SecurityScheme().name(SECURITY_KEY_BEARER)
			.type(HTTP)
			.scheme("bearer")
			.bearerFormat("JWT");
		getComponents(openApi).addSecuritySchemes(SECURITY_KEY_BEARER, securityScheme);
	}

	private Components getComponents(OpenAPI openApi) {
		if (openApi.getComponents() == null)
			openApi.components(new Components());
		return openApi.getComponents();
	}

}
