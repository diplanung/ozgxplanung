/*-
 * #%L
 * xplan-core-security - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.security.authorization;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import de.latlon.xplan.commons.util.XmlUtils;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class AgsParser {

	private static final String MAIN_FILE = "xplan.gml";

	private boolean foundPlan = false;

	private boolean foundXpGemeinde = false;

	private boolean foundAgs = false;

	private int startedElementsFromPlan = 0;

	private int startedElementsFromXpGemeinde = 0;

	private final Set<String> ags = new HashSet<>();

	public Set<String> parseAgs(InputStream gmlOrArchiveStream, AgsAuthorizer.OriginFileType originFile)
			throws XMLStreamException {
		try {
			switch (originFile) {
				case GML -> parseAgsFromGml(gmlOrArchiveStream);
				case ZIP -> parseAgsFromArchive(gmlOrArchiveStream);
			}
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
		return ags;
	}

	private void parseAgsFromArchive(InputStream gmlOrArchiveStream) throws IOException, XMLStreamException {
		ZipInputStream zipInputStream = new ZipInputStream(gmlOrArchiveStream, StandardCharsets.UTF_8);
		ZipEntry entry;
		while ((entry = zipInputStream.getNextEntry()) != null) {
			if (MAIN_FILE.equals(entry.getName())) {
				parseAgsFromGml(zipInputStream);
			}
		}
	}

	private void parseAgsFromGml(InputStream stream) throws XMLStreamException {
		XMLStreamReader reader = null;
		try {
			reader = XmlUtils.createXMLInputFactory().createXMLStreamReader(stream);
			while (reader.hasNext()) {
				parse(reader);
				reader.next();
			}
		}
		finally {
			closeQuietly(reader);
		}
	}

	private void parse(XMLStreamReader reader) {
		switch (reader.getEventType()) {
			case XMLEvent.START_ELEMENT:
				foundStartElement(reader.getLocalName());
				break;
			case XMLEvent.END_ELEMENT:
				foundEndElement();
				break;
			case XMLEvent.SPACE:
			case XMLEvent.CHARACTERS:
				String text = new String(reader.getTextCharacters(), reader.getTextStart(), reader.getTextLength());
				foundCharacters(text);
				break;
		}
	}

	private void foundStartElement(String localName) {
		if (localName.endsWith("_Plan"))
			foundPlan = true;
		if (foundPlan)
			startedElementsFromPlan++;
		if ("XP_Gemeinde".equals(localName))
			foundXpGemeinde = true;
		if (foundPlan && foundXpGemeinde && "ags".equals(localName))
			foundAgs = true;
		if (foundXpGemeinde)
			startedElementsFromXpGemeinde++;
	}

	private void foundEndElement() {
		if (foundXpGemeinde)
			if (startedElementsFromXpGemeinde == 0)
				foundXpGemeinde = false;
			else
				startedElementsFromXpGemeinde--;
		if (foundPlan)
			if (startedElementsFromPlan == 0)
				foundPlan = false;
			else
				startedElementsFromPlan--;
	}

	private void foundCharacters(String text) {
		if (foundAgs) {
			ags.add(text);
			foundAgs = false;
		}
	}

	private void closeQuietly(XMLStreamReader xmlReader) {
		if (xmlReader != null) {
			try {
				xmlReader.close();
			}
			catch (XMLStreamException e) {
				// nothing to do
			}
		}
	}

}
