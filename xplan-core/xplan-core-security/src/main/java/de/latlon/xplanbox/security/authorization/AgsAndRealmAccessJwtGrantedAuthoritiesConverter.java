/*-
 * #%L
 * xplan-core-security - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.security.authorization;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.security.oauth2.jwt.Jwt;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public final class AgsAndRealmAccessJwtGrantedAuthoritiesConverter
		implements Converter<Jwt, Collection<GrantedAuthority>> {

	@Override
	public Collection<GrantedAuthority> convert(Jwt jwt) {
		Map<String, List<String>> realmAccess = jwt.getClaim("realm_access");

		Map<String, Object> attributes = createAttributesWithAgs(jwt);
		return realmAccess.get("roles").stream().map(role -> {
			if (attributes != null)
				return new OAuth2UserAuthority("ROLE_" + role, attributes);
			return new SimpleGrantedAuthority("ROLE_" + role);
		}).collect(Collectors.toList());
	}

	private Map<String, Object> createAttributesWithAgs(Jwt jwt) {
		List<String> agsList = jwt.getClaimAsStringList("ags");
		if (agsList == null || agsList.isEmpty())
			return null;
		return Collections.singletonMap("ags", agsList);
	}

}
