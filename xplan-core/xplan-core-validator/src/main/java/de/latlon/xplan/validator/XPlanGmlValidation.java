/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator;

import static de.latlon.xplan.validator.report.SkipCode.SKIPPED;
import static de.latlon.xplan.validator.web.shared.ValidationType.GEOMETRIC;
import static de.latlon.xplan.validator.web.shared.ValidationType.REFERENCES;

import javax.xml.stream.XMLStreamException;
import java.util.Map;

import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.feature.XPlanFeatureCollections;
import de.latlon.xplan.commons.feature.XPlanGmlParser;
import de.latlon.xplan.commons.feature.XPlanGmlParserBuilder;
import de.latlon.xplan.validator.geometric.GeometricValidator;
import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.listener.ValidationProcessListener;
import de.latlon.xplan.validator.planinfo.PlanInfo;
import de.latlon.xplan.validator.planinfo.PlanInfoParser;
import de.latlon.xplan.validator.planinfo.PlanInfoReport;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.web.shared.ValidationSettings;
import org.deegree.commons.xml.XMLParsingException;
import org.deegree.cs.exceptions.UnknownCRSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class XPlanGmlValidation {

	private static final Logger LOG = LoggerFactory.getLogger(XPlanGmlValidation.class);

	private final XPlanGmlParserBuilder gmlParserBuilder;

	private final XPlanArchive archive;

	private final ValidationSettings validationSettings;

	private final GeometricValidator geometricValidator;

	private final PlanInfoParser planInfoParser;

	private XPlanGmlParser parser;

	private XPlanFeatureCollections xPlanFeatureCollections;

	private PlanInfoReport planInfoReport;

	private final ValidationProcessListener validationProcessListener;

	public XPlanGmlValidation(GeometricValidator geometricValidator, PlanInfoParser planInfoParser,
			XPlanArchive archive, ValidationSettings validationSettings,
			ValidationProcessListener validationProcessListener) {
		this.geometricValidator = geometricValidator;
		this.planInfoParser = planInfoParser;
		this.archive = archive;
		this.validationSettings = validationSettings;
		this.validationProcessListener = validationProcessListener;
		this.gmlParserBuilder = XPlanGmlParserBuilder.newBuilder();
	}

	public void executeGmlValidation(String uuid, ValidatorReport report) throws ValidatorException {
		boolean skipGeometric = !validationSettings.getValidationTypes().contains(GEOMETRIC);
		// prepare
		prepareGmlValidation(skipGeometric);
		// build
		buildGmlValidation(skipGeometric);
		// validate
		parseGmlValidation(uuid, report, skipGeometric);
	}

	public XPlanGmlParserBuilder getGmlParserBuilder() {
		return gmlParserBuilder;
	}

	public XPlanArchive getArchive() {
		return archive;
	}

	public ValidationSettings getValidationSettings() {
		return validationSettings;
	}

	private void buildParser() {
		this.parser = gmlParserBuilder.build();
	}

	public XPlanGmlParser getParser() {
		return parser;
	}

	public void parse() throws ValidatorException {
		try {
			this.xPlanFeatureCollections = parser.parseXPlanFeatureCollectionAllowMultipleInstances(archive);
		}
		catch (XMLStreamException | XMLParsingException | UnknownCRSException e) {
			LOG.error("Plan could not be parsed. Reason {}", e.getMessage(), e);
			throw new ValidatorException(
					"GML basierte Validierung wurde aufgrund von schwerwiegenden Fehlern abgebrochen", e);
		}
	}

	public XPlanFeatureCollections getxPlanFeatureCollections() {
		return xPlanFeatureCollections;
	}

	public PlanInfoReport getPlanInfoReport() {
		return planInfoReport;
	}

	private void prepareGmlValidation(boolean skipGeometric) {
		planInfoParser.prepareBuilder(this);
		if (!skipGeometric)
			geometricValidator.prepareBuilder(this);
	}

	private void buildGmlValidation(boolean skipGeometric) {
		buildParser();
		if (!skipGeometric)
			geometricValidator.afterParserBuild(this);
	}

	private void parseGmlValidation(String uuid, ValidatorReport report, boolean skipGeometric)
			throws ValidatorException {
		parse();
		validationProcessListener.validationPartStarted(uuid, REFERENCES);
		this.planInfoReport = planInfoParser.parsePlanInfo(this);
		validationProcessListener.validationPartFinished(uuid, REFERENCES,
				this.planInfoReport.getPlanInfos().values().stream().allMatch(PlanInfo::isValid));
		report.setPlanInfoReport(planInfoReport);
		if (skipGeometric) {
			report.getPlanInfoReport()
				.getPlanInfos()
				.forEach((planGmlId, planInfo) -> planInfo
					.geometricValidatorResult(new GeometricValidatorResult(SKIPPED)));
		}
		else {
			validationProcessListener.validationPartStarted(uuid, GEOMETRIC);
			Map<String, GeometricValidatorResult> validatorResult = geometricValidator.validateGeometry(this);
			report.getPlanInfoReport()
				.getPlanInfos()
				.forEach((planGmlId, planInfo) -> planInfo.geometricValidatorResult(validatorResult.get(planGmlId)));
			validationProcessListener.validationPartFinished(uuid, GEOMETRIC,
					validatorResult.values().stream().allMatch(GeometricValidatorResult::isValid));
		}
	}

}
