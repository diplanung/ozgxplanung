/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.report;

import java.util.Date;

import de.latlon.xplan.validator.planinfo.PlanInfo;
import de.latlon.xplan.validator.planinfo.PlanInfoReport;
import de.latlon.xplan.validator.syntactic.report.SyntacticValidatorResult;

/**
 * A validation report contains all ValidationResults of one Validation
 *
 * @author <a href="mailto:erben@lat-lon.de">Alexander Erben</a>
 * @version $Revision: $, $Date: $
 */
public class ValidatorReport {

	private SyntacticValidatorResult syntacticValidatorResult;

	private String validationName;

	private String archiveName;

	private Date date;

	private PlanInfoReport planInfoReport;

	public ValidatorReport() {
	}

	private boolean hasMultipleXPlanElements;

	/**
	 * @return result of syntactic validator, maybe <code>null</code>.
	 */
	public SyntacticValidatorResult getSyntacticValidatorResult() {
		return syntacticValidatorResult;
	}

	/**
	 * sets result of syntactic validator.
	 * @param result to set, maybe <code>null</code>.
	 */
	public void setSyntacticValidatorResult(SyntacticValidatorResult result) {
		syntacticValidatorResult = result;
	}

	/**
	 * @param validationName name of the validation run
	 */
	public void setValidationName(String validationName) {
		this.validationName = validationName;
	}

	/**
	 * @return name of the validation run
	 */
	public String getValidationName() {
		return validationName;
	}

	/**
	 * @param archiveName name of the archive or file
	 */
	public void setArchiveName(String archiveName) {
		this.archiveName = archiveName;
	}

	/**
	 * @return name of the archive or file
	 */
	public String getArchiveName() {
		return archiveName;
	}

	/**
	 * @param date date of the validation run
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return date of the validation run
	 */
	public Date getDate() {
		return date;
	}

	public PlanInfoReport getPlanInfoReport() {
		return planInfoReport;
	}

	public void setPlanInfoReport(PlanInfoReport planInfoReport) {
		this.planInfoReport = planInfoReport;
	}

	/**
	 * @param hasMultipleXPlanElements <code>true</code> if the XPLanArchive contains
	 * multiple XPlanElements, <code>false</code> otherwise
	 */
	public void setHasMultipleXPlanElements(boolean hasMultipleXPlanElements) {
		this.hasMultipleXPlanElements = hasMultipleXPlanElements;
	}

	/**
	 * @return <code>true</code> if the XPLanArchive contains multiple XPlanElements,
	 * <code>false</code> otherwise
	 */
	public boolean hasMultipleXPlanElements() {
		return this.hasMultipleXPlanElements;
	}

	/**
	 * @return <code>true</code>, if all validator results are either <code>null</code> or
	 * valid.
	 */
	public boolean isValid() {
		boolean finalResult = true;
		if (syntacticValidatorResult != null)
			finalResult = syntacticValidatorResult.isValid();
		return finalResult && planInfoReport.getPlanInfos().values().stream().allMatch(PlanInfo::isValid);
	}

}
