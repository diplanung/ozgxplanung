/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator;

import static de.latlon.xplan.validator.report.SkipCode.SKIPPED;
import static de.latlon.xplan.validator.report.SkipCode.SYNTAX_ERRORS;
import static de.latlon.xplan.validator.report.SkipCode.UNSUPPORTED;
import static de.latlon.xplan.validator.web.shared.ValidationType.SEMANTIC;
import static de.latlon.xplan.validator.web.shared.ValidationType.SYNTACTIC;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import de.latlon.xplan.commons.archive.SemanticValidableXPlanArchive;
import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.commons.feature.XPlanFeatureCollection;
import de.latlon.xplan.validator.geometric.GeometricValidator;
import de.latlon.xplan.validator.listener.InactiveValidationProcessListener;
import de.latlon.xplan.validator.listener.ValidationProcessListener;
import de.latlon.xplan.validator.planinfo.PlanInfo;
import de.latlon.xplan.validator.planinfo.PlanInfoParser;
import de.latlon.xplan.validator.planinfo.PlanInfoReport;
import de.latlon.xplan.validator.report.ReportArchiveGenerator;
import de.latlon.xplan.validator.report.ReportGenerationException;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.report.ValidatorResult;
import de.latlon.xplan.validator.semantic.SemanticValidator;
import de.latlon.xplan.validator.semantic.configuration.SemanticValidationOptions;
import de.latlon.xplan.validator.semantic.configuration.metadata.RulesMetadata;
import de.latlon.xplan.validator.semantic.profile.SemanticProfileValidator;
import de.latlon.xplan.validator.semantic.report.InvalidFeaturesResult;
import de.latlon.xplan.validator.semantic.report.RuleResult;
import de.latlon.xplan.validator.semantic.report.SemanticValidatorResult;
import de.latlon.xplan.validator.syntactic.SyntacticValidator;
import de.latlon.xplan.validator.syntactic.report.SyntacticValidatorResult;
import de.latlon.xplan.validator.web.shared.ValidationOption;
import de.latlon.xplan.validator.web.shared.ValidationSettings;
import de.latlon.xplan.validator.web.shared.ValidationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Performs semantic, geometric and syntactic validation for the CLI
 *
 * @author <a href="mailto:erben@lat-lon.de">Alexander Erben</a>
 */
public class XPlanValidator {

	private static final Logger LOG = LoggerFactory.getLogger(XPlanValidator.class);

	private final GeometricValidator geometricValidator;

	private final SyntacticValidator syntacticValidator;

	private final SemanticValidator semanticValidator;

	private final List<SemanticProfileValidator> semanticProfileValidators;

	private final ReportArchiveGenerator reportArchiveGenerator;

	private final PlanInfoParser planInfoParser;

	private final XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();

	private final ValidationProcessListener validationProcessListener;

	public XPlanValidator(GeometricValidator geometricValidator, SyntacticValidator syntacticValidator,
			SemanticValidator semanticValidator, List<SemanticProfileValidator> semanticProfileValidators,
			ReportArchiveGenerator reportArchiveGenerator, PlanInfoParser planInfoParser) {
		this(geometricValidator, syntacticValidator, semanticValidator, semanticProfileValidators,
				reportArchiveGenerator, planInfoParser, new InactiveValidationProcessListener());
	}

	public XPlanValidator(GeometricValidator geometricValidator, SyntacticValidator syntacticValidator,
			SemanticValidator semanticValidator, List<SemanticProfileValidator> semanticProfileValidators,
			ReportArchiveGenerator reportArchiveGenerator, PlanInfoParser planInfoParser,
			ValidationProcessListener validationProcessListener) {
		this.geometricValidator = geometricValidator;
		this.syntacticValidator = syntacticValidator;
		this.semanticValidator = semanticValidator;
		this.semanticProfileValidators = semanticProfileValidators;
		this.reportArchiveGenerator = reportArchiveGenerator;
		this.planInfoParser = planInfoParser;
		this.validationProcessListener = validationProcessListener;
	}

	/**
	 * Validate a plan archive
	 * @param validationSettings to apply, never <code>null</code>
	 * @param planArchive to validate, never <code>null</code> and must point to a zip
	 * file with a gml plan
	 * @return <link>ValidatorReport</link>
	 * @throws ValidatorException
	 * @throws IOException
	 * @throws ReportGenerationException
	 */
	public ValidatorReport validate(ValidationSettings validationSettings, File planArchive, String archiveName)
			throws ValidatorException, IOException, ReportGenerationException {
		XPlanArchive archive = archiveCreator.createXPlanArchive(planArchive);
		ValidatorReport report = validate(UUID.randomUUID().toString(), validationSettings, archive, archiveName);
		writeReport(report);
		LOG.info("Archiv mit Validierungsergebnissen wird erstellt.");
		Path validationReportDirectory = createZipArchive(validationSettings, report);
		LOG.info("Archiv mit Validierungsergebnissen wurde unter {} abgelegt.", validationReportDirectory);
		return report;
	}

	/**
	 * Validate a plan archive, but does not write the report
	 * @param validationSettings to apply, never <code>null</code>
	 * @param planArchive to validate, never <code>null</code> and must point to a zip
	 * file with a gml plan
	 * @return <link>ValidatorReport</link>
	 * @throws ValidatorException
	 * @throws IOException
	 */
	public ValidatorReport validateNotWriteReport(ValidationSettings validationSettings, File planArchive,
			String archiveName) throws ValidatorException, IOException {
		XPlanArchive archive = archiveCreator.createXPlanArchive(planArchive);
		return validateNotWriteReport(null, validationSettings, archive, archiveName);
	}

	/**
	 * Validate a plan archive, but does not write the report
	 * @param validationSettings to apply, never <code>null</code>
	 * @param planArchive to validate, never <code>null</code>
	 * @return <link>ValidatorReport</link>
	 * @throws ValidatorException
	 */
	public ValidatorReport validateNotWriteReport(String uuid, ValidationSettings validationSettings,
			XPlanArchive planArchive, String archiveName) throws ValidatorException {
		ValidatorReport validationReport = validate(uuid, validationSettings, planArchive, archiveName);
		validationReport.setHasMultipleXPlanElements(planArchive.hasMultipleXPlanElements());
		return validationReport;
	}

	private void writeReport(ValidatorReport report) {
		writeReport(report.getSyntacticValidatorResult());
		report.getPlanInfoReport().getPlanInfos().values().forEach(planInfo -> {
			LOG.info("Validierung des Plans {}", planInfo.getName());
			writeReport(planInfo.getSemanticValidatorResult());
			writeReport(planInfo.getGeometricValidatorResult());
		});
	}

	private void writeReport(ValidatorResult result) {
		if (result != null) {
			String validityMessage = result.isValid() ? "valide" : "nicht valide";
			LOG.info("{} hat ergeben: Dokument ist {}", result.getType(), validityMessage);
		}
	}

	private ValidatorReport validate(String uuid, ValidationSettings validationSettings, XPlanArchive archive,
			String archiveName) throws ValidatorException {
		try {
			validationProcessListener.validationStarted(uuid);
			List<SemanticValidationOptions> semanticValidationOptions = extractSemanticValidationOptions(
					validationSettings);

			ValidatorReport report = new ValidatorReport();
			report.setValidationName(validationSettings.getValidationName());
			report.setArchiveName(archiveName);
			report.setDate(new Date());

			List<ValidationType> validationType = getValidationType(validationSettings);
			validationProcessListener.validationPartStarted(uuid, SYNTACTIC);
			SyntacticValidatorResult syntacticValidatorResult = validateSyntactic(archive, report);
			validationProcessListener.validationPartFinished(uuid, SYNTACTIC, syntacticValidatorResult.isValid());
			if (!syntacticValidatorResult.isValid()) {
				report.setPlanInfoReport(new PlanInfoReport().skipCode(SYNTAX_ERRORS));
				return report;
			}

			XPlanGmlValidation xPlanGmlValidation = new XPlanGmlValidation(geometricValidator, planInfoParser, archive,
					validationSettings, validationProcessListener);
			xPlanGmlValidation.executeGmlValidation(uuid, report);

			valideSemanticAndProfiles(uuid, validationSettings, archive, xPlanGmlValidation, semanticValidationOptions,
					validationType, report);
			validationProcessListener.validationFinished(uuid, report.isValid());
			return report;
		}
		catch (ValidatorException e) {
			validationProcessListener.validationFailed(uuid, e.getMessage());
			throw e;
		}
	}

	private SyntacticValidatorResult validateSyntactic(XPlanArchive archive, ValidatorReport report) {
		SyntacticValidatorResult syntacticallyResult = validateSyntacticallyAndWriteResult(archive);
		report.setSyntacticValidatorResult(syntacticallyResult);
		return syntacticallyResult;

	}

	private void valideSemanticAndProfiles(String uuid, ValidationSettings validationSettings, XPlanArchive archive,
			XPlanGmlValidation gmlValidation, List<SemanticValidationOptions> semanticValidationOptions,
			List<ValidationType> validationTypes, ValidatorReport report) throws ValidatorException {
		boolean multiplePlansReferencesWithMultiplePlans = archive.hasMultipleXPlanElements()
				&& archive.hasVerbundenerPlanBereich();

		List<String> profiles = validationSettings.getProfiles();
		List<XPlanFeatureCollection> xPlanGmlInstances = gmlValidation.getxPlanFeatureCollections()
			.getxPlanGmlInstances();
		for (XPlanFeatureCollection xPlanGmlInstance : xPlanGmlInstances) {
			SemanticValidableXPlanArchive instanceAsArchive = xPlanGmlInstance.getAsArchive(archive);
			PlanInfo planInfo = report.getPlanInfoReport().getPlanInfos().get(xPlanGmlInstance.getPlanId());
			if (multiplePlansReferencesWithMultiplePlans) {
				LOG.info("Das XPlan GML Dokument beinhaltet Referenzen auf andere Plaene ueber die Relation "
						+ "verbundenerPlan. Die semantische Validierung und Validierung von Profilen "
						+ "wird derzeit nicht unterstuetzt.");
				planInfo.semanticValidatorResult(new SemanticValidatorResult(UNSUPPORTED));
			}
			else if (!validationTypes.contains(SEMANTIC))
				planInfo.semanticValidatorResult(new SemanticValidatorResult(SKIPPED));
			else {
				validationProcessListener.validationPartStarted(uuid, SEMANTIC);
				SemanticValidatorResult semanticallyResult = validateSemanticallyAndWriteResult(semanticValidator,
						instanceAsArchive, semanticValidationOptions);
				planInfo.semanticValidatorResult(semanticallyResult);
				validationProcessListener.validationPartFinished(uuid, SEMANTIC, semanticallyResult.isValid());
			}
			validateSemanticProfiles(uuid, instanceAsArchive, profiles, planInfo);
		}
	}

	private void validateSemanticProfiles(String uuid, SemanticValidableXPlanArchive archive, List<String> profiles,
			PlanInfo planInfo) throws ValidatorException {
		for (String profileId : profiles) {
			Optional<SemanticProfileValidator> profileValidator = semanticProfileValidators.stream()
				.filter(semanticProfileValidator -> semanticProfileValidator.getId().equals(profileId))
				.findFirst();
			if (profileValidator.isPresent()) {
				validationProcessListener.validationProfileStarted(uuid, profileId);
				SemanticValidatorResult semanticValidatorResult = validateSemanticallyAndWriteResult(
						profileValidator.get(), archive, Collections.emptyList());
				planInfo.addSemanticProfileValidatorResults(semanticValidatorResult);
				validationProcessListener.validationProfileFinished(uuid, profileId, semanticValidatorResult.isValid());
			}
			else {
				throw new ValidatorException("Profile with id " + profileId + " does not exist");
			}
		}
	}

	private List<ValidationType> getValidationType(ValidationSettings validationSettings) {
		if (validationSettings == null || validationSettings.getValidationTypes() == null
				|| validationSettings.getValidationTypes().isEmpty())
			return Collections.emptyList();
		return validationSettings.getValidationTypes();
	}

	/**
	 * Perform semantic validation of the given archive
	 * @param archive archive to validate, never <code>null</code>
	 * @param semanticValidationOptions {@link List} of {@link SemanticValidationOptions},
	 * considered by the validation, may be empty, but never <code>null</code>
	 * @return the created report
	 */
	private SemanticValidatorResult validateSemanticallyAndWriteResult(SemanticValidator semanticValidator,
			SemanticValidableXPlanArchive archive, List<SemanticValidationOptions> semanticValidationOptions) {
		ValidatorResult result = semanticValidator.validateSemantic(archive, semanticValidationOptions);
		SemanticValidatorResult validatorResult = (SemanticValidatorResult) result;
		log(validatorResult);
		return validatorResult;
	}

	/**
	 * Perform syntactic validation of the given archive
	 * @param archive archive to validate, never <code>null</code>
	 * @return the created report, never <code>null</code>
	 */
	SyntacticValidatorResult validateSyntacticallyAndWriteResult(XPlanArchive archive) {
		ValidatorResult result = syntacticValidator.validateSyntax(archive);
		SyntacticValidatorResult validatorResult = (SyntacticValidatorResult) result;
		log(validatorResult);
		return validatorResult;
	}

	private void log(SemanticValidatorResult validatorResult) {
		RulesMetadata rulesMetadata = validatorResult.getRulesMetadata();
		if (rulesMetadata != null) {
			LOG.info("Informationen zur semantischen Validierung:");
			if (rulesMetadata.getName() != null)
				LOG.info("  - Name: {}", rulesMetadata.getName());
			if (rulesMetadata.getDescription() != null)
				LOG.info("  - Beschreibung: {}", rulesMetadata.getDescription());
			LOG.info("  - Version: {}", rulesMetadata.getVersion());
			LOG.info("  - Quelle: {}", rulesMetadata.getSource());
		}
		List<RuleResult> ruleResults = validatorResult.getRules();
		LOG.info("Ergebnisse der semantischen Validierung: {}", ruleResults.size());
		for (RuleResult ruleResult : ruleResults) {
			if (ruleResult.isValid()) {
				LOG.info("  - Erfolgreich: {}", ruleResult.getMessage());
			}
			else {
				List<InvalidFeaturesResult> invalidFeatures = ruleResult.getInvalidFeaturesResults();
				invalidFeatures.stream().forEach(invalidRuleResult -> {
					String gmlIds = invalidRuleResult.getGmlIds().stream().collect(Collectors.joining(", "));
					LOG.info("  - {}: {}, Features: {}", invalidRuleResult.getResultType(),
							invalidRuleResult.getMessage(), gmlIds);
				});
			}
		}
	}

	private void log(SyntacticValidatorResult validatorResult) {
		List<String> messages = validatorResult.getMessages();
		LOG.info("Ergebnisse der syntaktischen Validierung: {}", messages.size());
		for (String mess : messages)
			LOG.info("  - {}", mess);
	}

	private List<SemanticValidationOptions> extractSemanticValidationOptions(ValidationSettings validationSettings) {
		List<SemanticValidationOptions> semanticValidationOptions = new ArrayList<>();
		List<ValidationOption> extendedOptions = validationSettings.getExtendedOptions();
		if (extendedOptions != null)
			for (ValidationOption validationOption : extendedOptions) {
				SemanticValidationOptions semanticValidationOption = SemanticValidationOptions
					.getByOption(validationOption);
				if (!SemanticValidationOptions.NONE.equals(semanticValidationOption))
					semanticValidationOptions.add(semanticValidationOption);
			}
		return semanticValidationOptions;
	}

	private Path createZipArchive(ValidationSettings validationSettings, ValidatorReport report)
			throws ReportGenerationException {
		String validationName = validationSettings.getValidationName();
		return reportArchiveGenerator.generateZipArchive(report, validationName);
	}

}
