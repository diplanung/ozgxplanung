/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.report.geojson.jts;

import static org.locationtech.jts.geom.Geometry.TYPENAME_GEOMETRYCOLLECTION;
import static org.locationtech.jts.geom.Geometry.TYPENAME_LINEARRING;
import static org.locationtech.jts.geom.Geometry.TYPENAME_LINESTRING;
import static org.locationtech.jts.geom.Geometry.TYPENAME_MULTILINESTRING;
import static org.locationtech.jts.geom.Geometry.TYPENAME_MULTIPOINT;
import static org.locationtech.jts.geom.Geometry.TYPENAME_MULTIPOLYGON;
import static org.locationtech.jts.geom.Geometry.TYPENAME_POINT;
import static org.locationtech.jts.geom.Geometry.TYPENAME_POLYGON;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import de.latlon.xplan.validator.report.geojson.model.Geometry;
import de.latlon.xplan.validator.report.geojson.model.GeometryCollection;
import de.latlon.xplan.validator.report.geojson.model.GeometryElement;
import de.latlon.xplan.validator.report.geojson.model.LineString;
import de.latlon.xplan.validator.report.geojson.model.LineStringCoordinates;
import de.latlon.xplan.validator.report.geojson.model.LinearRing;
import de.latlon.xplan.validator.report.geojson.model.MultiLineString;
import de.latlon.xplan.validator.report.geojson.model.MultiPoint;
import de.latlon.xplan.validator.report.geojson.model.MultiPolygon;
import de.latlon.xplan.validator.report.geojson.model.Point;
import de.latlon.xplan.validator.report.geojson.model.Polygon;
import de.latlon.xplan.validator.report.geojson.model.Position;
import org.locationtech.jts.geom.Coordinate;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class JtsToGeoJsonGeometryBuilder {

	private JtsToGeoJsonGeometryBuilder() {
	}

	/**
	 * @param geom may be <code>null</code>
	 * @return <code>null</code> if the passed geom is <code>null</code>
	 */
	public static Geometry createGeometry(org.locationtech.jts.geom.Geometry geom) {
		if (geom == null)
			return null;
		return geometry(geom);
	}

	private static Geometry geometry(org.locationtech.jts.geom.Geometry geom) {
		switch (geom.getGeometryType()) {
			case TYPENAME_POINT -> {
				return geometry((org.locationtech.jts.geom.Point) geom);
			}
			case TYPENAME_LINEARRING -> {
				return geometry((org.locationtech.jts.geom.LinearRing) geom);
			}
			case TYPENAME_LINESTRING -> {
				return geometry((org.locationtech.jts.geom.LineString) geom);
			}
			case TYPENAME_POLYGON -> {
				return geometry((org.locationtech.jts.geom.Polygon) geom);
			}
			case TYPENAME_GEOMETRYCOLLECTION -> {
				return geometry((org.locationtech.jts.geom.GeometryCollection) geom);
			}
			case TYPENAME_MULTIPOINT -> {
				return geometry((org.locationtech.jts.geom.MultiPoint) geom);
			}
			case TYPENAME_MULTILINESTRING -> {
				return geometry((org.locationtech.jts.geom.MultiLineString) geom);
			}
			case TYPENAME_MULTIPOLYGON -> {
				return geometry((org.locationtech.jts.geom.MultiPolygon) geom);
			}
			default -> throw new IllegalArgumentException(
					"Could not export geometry " + geom.getGeometryType() + " as GeoJSON");
		}
	}

	private static GeometryCollection geometry(org.locationtech.jts.geom.GeometryCollection geom) {
		GeometryCollection geometryCollection = new GeometryCollection();
		for (int geomIndex = 0; geomIndex < geom.getNumGeometries(); geomIndex++) {
			geometryCollection.addGeometriesItem((GeometryElement) geometry(geom.getGeometryN(geomIndex)));
		}
		return geometryCollection;
	}

	private static MultiPoint geometry(org.locationtech.jts.geom.MultiPoint geom) {
		MultiPoint multiPoint = new MultiPoint();
		for (int geomIndex = 0; geomIndex < geom.getNumGeometries(); geomIndex++) {
			org.locationtech.jts.geom.Point point = (org.locationtech.jts.geom.Point) geom.getGeometryN(geomIndex);
			multiPoint.addPositionItem(position(point));
		}
		return multiPoint;
	}

	private static MultiLineString geometry(org.locationtech.jts.geom.MultiLineString geom) {
		MultiLineString multiLineString = new MultiLineString();
		for (int geomIndex = 0; geomIndex < geom.getNumGeometries(); geomIndex++) {
			org.locationtech.jts.geom.LineString lineString = (org.locationtech.jts.geom.LineString) geom
				.getGeometryN(geomIndex);
			multiLineString.addLineStringCoordinate(lineStringCoordinates(lineString));
		}
		return multiLineString;
	}

	private static MultiPolygon geometry(org.locationtech.jts.geom.MultiPolygon geom) {
		MultiPolygon multiPolygon = new MultiPolygon();
		for (int geomIndex = 0; geomIndex < geom.getNumGeometries(); geomIndex++) {
			org.locationtech.jts.geom.Polygon polygon = (org.locationtech.jts.geom.Polygon) geom
				.getGeometryN(geomIndex);
			multiPolygon.addLinearRing(linearRings(polygon));
		}
		return multiPolygon;
	}

	private static LineString geometry(org.locationtech.jts.geom.LineString geom) {
		LineStringCoordinates coordinates = lineStringCoordinates(geom);
		return new LineString().coordinates(coordinates);
	}

	private static Polygon geometry(org.locationtech.jts.geom.LinearRing ring) {
		List<LinearRing> linearRings = Collections.singletonList(linearRing(ring));
		return new Polygon().linearRings(linearRings);
	}

	private static Polygon geometry(org.locationtech.jts.geom.Polygon polygon) {
		List<LinearRing> linearRings = linearRings(polygon);
		return new Polygon().linearRings(linearRings);
	}

	private static Point geometry(org.locationtech.jts.geom.Point point) {
		return new Point().position(position(point));
	}

	private static List<LinearRing> linearRings(org.locationtech.jts.geom.Polygon polygon) {
		List<LinearRing> linearRings = new ArrayList<>();
		linearRings.add(linearRing(polygon.getExteriorRing()));
		for (int ringIndex = 0; ringIndex < polygon.getNumInteriorRing(); ringIndex++) {
			linearRings.add(linearRing(polygon.getInteriorRingN(ringIndex)));
		}
		return linearRings;
	}

	private static LinearRing linearRing(org.locationtech.jts.geom.LinearRing ring) {
		LinearRing linearRing = new LinearRing();
		Arrays.stream(ring.getCoordinates()).forEach(controlPoint -> linearRing.add(position(controlPoint)));
		return linearRing;
	}

	private static LineStringCoordinates lineStringCoordinates(org.locationtech.jts.geom.LineString geom) {
		LineStringCoordinates coordinates = new LineStringCoordinates();
		Arrays.stream(geom.getCoordinates()).forEach(coordinate -> coordinates.add(position(coordinate)));
		return coordinates;
	}

	private static Position position(Coordinate coordinate) {
		Position position = new Position();
		position.add(BigDecimal.valueOf(coordinate.getX()));
		position.add(BigDecimal.valueOf(coordinate.getY()));
		return position;
	}

	private static Position position(org.locationtech.jts.geom.Point point) {
		Position position = new Position();
		position.add(BigDecimal.valueOf(point.getX()));
		position.add(BigDecimal.valueOf(point.getY()));
		return position;
	}

}
