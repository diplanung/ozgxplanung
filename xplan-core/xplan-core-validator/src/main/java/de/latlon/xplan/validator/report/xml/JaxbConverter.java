/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.report.xml;

import static de.latlon.xplan.validator.report.ReportUtils.asLabel;
import static de.latlon.xplan.validator.report.ReportUtils.createValidLabel;
import static de.latlon.xplan.validator.semantic.report.ValidationResultType.ERROR;
import static de.latlon.xplan.validator.semantic.report.ValidationResultType.WARNING;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.planinfo.PlanInfo;
import de.latlon.xplan.validator.planinfo.PlanInfoReport;
import de.latlon.xplan.validator.report.ErrorsType;
import de.latlon.xplan.validator.report.ExternalReferenceType;
import de.latlon.xplan.validator.report.ExternalReferencesType;
import de.latlon.xplan.validator.report.GeomType;
import de.latlon.xplan.validator.report.InvalidFeaturesType;
import de.latlon.xplan.validator.report.MessagesType;
import de.latlon.xplan.validator.report.ObjectFactory;
import de.latlon.xplan.validator.report.Plan;
import de.latlon.xplan.validator.report.PlanType;
import de.latlon.xplan.validator.report.RuleType;
import de.latlon.xplan.validator.report.RulesMetadataType;
import de.latlon.xplan.validator.report.RulesType;
import de.latlon.xplan.validator.report.SemType;
import de.latlon.xplan.validator.report.SkipCode;
import de.latlon.xplan.validator.report.SynType;
import de.latlon.xplan.validator.report.ValidationReport;
import de.latlon.xplan.validator.report.ValidationType;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.report.WarningsType;
import de.latlon.xplan.validator.report.reference.ExternalReferenceReport;
import de.latlon.xplan.validator.report.reference.ExternalReferenceStatus;
import de.latlon.xplan.validator.semantic.configuration.metadata.RulesMetadata;
import de.latlon.xplan.validator.semantic.report.InvalidFeaturesResult;
import de.latlon.xplan.validator.semantic.report.RuleResult;
import de.latlon.xplan.validator.semantic.report.SemanticValidatorResult;
import de.latlon.xplan.validator.syntactic.report.SyntacticValidatorResult;

/**
 * Converts the internal used {@link ValidatorReport} instances to jaxb
 *
 * @deprecated will be removed in a future version.
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version $Revision: $, $Date: $
 */
@Deprecated
public class JaxbConverter {

	/**
	 * Converts the passed report to jaxb including the name and plan name of the
	 * validation.
	 * @param report to convert, never <code>null</code>
	 * @return the converted {@link JaxbConverter} instance, never <code>null</code>
	 */
	public ValidationReport convertValidationReport(ValidatorReport report) {
		ValidationReport validationReportType = new ValidationReport();
		validationReportType.setDate(toCalendar(report.getDate()));
		validationReportType.setName(report.getValidationName());
		validationReportType.setFileName(report.getArchiveName());
		validationReportType.setSyn(convertSyntacticResults(report));

		PlanInfoReport planInfoReport = report.getPlanInfoReport();
		planInfoReport.getPlanInfos().values().forEach(planInfo -> {
			Plan plan = new Plan();
			plan.setIsValid(planInfo.isValid());
			plan.setPlan(convertPlanType(planInfo));
			plan.setExternalReferences(convertExternalReferences(planInfo));
			plan.setValidation(convertSemanticResults(planInfo));
			validationReportType.getPlan().add(plan);
		});
		return validationReportType;
	}

	private ExternalReferencesType convertExternalReferences(PlanInfo planInfo) {
		ExternalReferenceReport externalReferenceReport = planInfo.getExternalReferenceReport();
		if (externalReferenceReport == null)
			return null;

		ObjectFactory objectFactory = new ObjectFactory();
		ExternalReferencesType externalReferencesType = objectFactory.createExternalReferencesType();

		SkipCode skipCode = externalReferenceReport.getSkipCode();
		if (skipCode != null) {
			String skipMessage = skipCode.getMessage();
			externalReferencesType.setSkipMessage(skipMessage);
		}
		Map<String, ExternalReferenceStatus> externalReferences = externalReferenceReport.getReferencesAndStatus();
		externalReferences.forEach((name, status) -> {
			ExternalReferenceType reference = new ExternalReferenceType();
			reference.setValue(name);
			reference.setStatus(de.latlon.xplan.validator.report.ExternalReferenceStatus.valueOf(status.name()));
			externalReferencesType.getExternalReference().add(reference);
		});
		return externalReferencesType;
	}

	private ValidationType convertSemanticResults(PlanInfo planInfo) {
		ObjectFactory objectFactory = new ObjectFactory();
		ValidationType jaxbValidation = objectFactory.createValidationType();
		if (planInfo.getSemanticValidatorResult() != null)
			convertResultToJaxb(planInfo.getSemanticValidatorResult(), jaxbValidation);
		if (!planInfo.getSemanticProfileValidatorResults().isEmpty())
			convertResultToJaxb(planInfo.getSemanticProfileValidatorResults(), jaxbValidation);
		if (planInfo.getGeometricValidatorResult() != null)
			convertResultToJaxb(planInfo.getGeometricValidatorResult(), jaxbValidation);
		return jaxbValidation;
	}

	private SynType convertSyntacticResults(ValidatorReport report) {
		if (report.getSyntacticValidatorResult() != null) {
			ObjectFactory objectFactory = new ObjectFactory();
			SynType jaxbValidation = objectFactory.createSynType();
			convertResultToJaxb(report.getSyntacticValidatorResult(), jaxbValidation);
			return jaxbValidation;
		}
		return null;
	}

	private PlanType convertPlanType(PlanInfo planInfo) {
		ObjectFactory objectFactory = new ObjectFactory();
		PlanType pt = objectFactory.createPlanType();
		pt.setName(planInfo.getName());
		pt.setVersion(asLabel(planInfo.getVersion()));
		return pt;
	}

	private void convertResultToJaxb(GeometricValidatorResult result, ValidationType val) {
		ObjectFactory objectFactory = new ObjectFactory();
		GeomType geomType = objectFactory.createGeomType();

		if (result.isSkipped()) {
			geomType.setResult(result.getSkipCode().getMessage());
		}
		else {
			WarningsType warningsXml = objectFactory.createWarningsType();
			warningsXml.getWarning().addAll(result.getWarnings());

			ErrorsType errorsXml = objectFactory.createErrorsType();
			errorsXml.getError().addAll(result.getErrors());

			geomType.setWarnings(warningsXml);
			geomType.setErrors(errorsXml);
			geomType.setResult(createValidLabel(result.isValid()));
			if (result.getValidatorDetail() != null)
				geomType.setDetails(result.getValidatorDetail().toString());
		}

		val.setGeom(geomType);
	}

	private void convertResultToJaxb(SemanticValidatorResult result, ValidationType validationType) {
		SemType semType = convertToSemType(result);
		validationType.setSem(semType);
	}

	private void convertResultToJaxb(List<SemanticValidatorResult> semanticProfileValidatorResults,
			ValidationType validationType) {
		semanticProfileValidatorResults.sort(Comparator.comparing(o -> o.getRulesMetadata().getName()));
		semanticProfileValidatorResults.forEach(semanticValidatorResult -> {
			SemType semType = convertToSemType(semanticValidatorResult);
			validationType.getProfile().add(semType);
		});
	}

	private SemType convertToSemType(SemanticValidatorResult result) {
		ObjectFactory objectFactory = new ObjectFactory();
		SemType semType = objectFactory.createSemType();

		RulesMetadata rulesMetadata = result.getRulesMetadata();
		if (rulesMetadata != null) {
			RulesMetadataType rulesMetadataType = objectFactory.createRulesMetadataType();
			rulesMetadataType.setName(rulesMetadata.getName());
			rulesMetadataType.setDescription(rulesMetadata.getDescription());
			rulesMetadataType.setVersion(rulesMetadata.getVersion());
			rulesMetadataType.setSource(rulesMetadata.getSource());
			semType.setRulesMetadata(rulesMetadataType);
		}

		if (result.isSkipped()) {
			semType.setResult(result.getSkipCode().getMessage());
		}
		else {
			RulesType rulesXML = objectFactory.createRulesType();
			List<RuleType> rulesListXML = rulesXML.getRule();
			for (RuleResult rule : result.getRules()) {
				RuleType ruleXML = objectFactory.createRuleType();
				ruleXML.setName(rule.getName());
				ruleXML.setIsValid(rule.isValid());
				ruleXML.setMessage(rule.getMessage());
				addWarnedFeatures(ruleXML, rule.getInvalidFeaturesResultsByType(WARNING));
				addErroredFeatures(ruleXML, rule.getInvalidFeaturesResultsByType(ERROR));
				rulesListXML.add(ruleXML);
			}
			semType.setRules(rulesXML);

			semType.setResult(createValidLabel(result.isValid()));
			if (result.getValidatorDetail() != null)
				semType.setDetails(result.getValidatorDetail().toString());
		}
		return semType;
	}

	private void addWarnedFeatures(RuleType ruleXML, List<InvalidFeaturesResult> warnedFeatures) {
		ObjectFactory objectFactory = new ObjectFactory();
		for (InvalidFeaturesResult warnedFeature : warnedFeatures) {
			InvalidFeaturesType invalidFeaturesType = createInvalidFeaturesType(objectFactory, warnedFeature);
			ruleXML.getWarnedFeatures().add(invalidFeaturesType);
		}
	}

	private void addErroredFeatures(RuleType ruleXML, List<InvalidFeaturesResult> erroredFeatures) {
		ObjectFactory objectFactory = new ObjectFactory();
		for (InvalidFeaturesResult erroredFeature : erroredFeatures) {
			InvalidFeaturesType invalidFeaturesType = createInvalidFeaturesType(objectFactory, erroredFeature);
			ruleXML.getErroredFeatures().add(invalidFeaturesType);
		}
	}

	private InvalidFeaturesType createInvalidFeaturesType(ObjectFactory objectFactory,
			InvalidFeaturesResult invalidFeaturesResult) {
		InvalidFeaturesType invalidFeaturesType = objectFactory.createInvalidFeaturesType();
		invalidFeaturesType.setMessage(invalidFeaturesResult.getMessage());
		invalidFeaturesType.getGmlid().addAll(invalidFeaturesResult.getGmlIds());
		return invalidFeaturesType;
	}

	private void convertResultToJaxb(SyntacticValidatorResult result, SynType synType) {
		ObjectFactory objectFactory = new ObjectFactory();

		MessagesType messagesXml = objectFactory.createMessagesType();
		messagesXml.getMessage().addAll(result.getMessages());

		synType.setMessages(messagesXml);
		synType.setResult(createValidLabel(result.isValid()));
		if (result.getValidatorDetail() != null)
			synType.setDetails(result.getValidatorDetail().toString());
	}

	private XMLGregorianCalendar toCalendar(Date date) {
		if (date == null)
			return null;

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		}
		catch (DatatypeConfigurationException e) {
			throw new RuntimeException(e);
		}
	}

}
