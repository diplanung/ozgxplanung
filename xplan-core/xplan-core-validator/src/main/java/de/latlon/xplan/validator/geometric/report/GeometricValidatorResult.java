/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.geometric.report;

import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.ERROR;
import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.WARNING;
import static de.latlon.xplan.validator.i18n.ValidationMessages.getMessage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.latlon.xplan.validator.geometric.result.GeometricValidationFinding;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel;
import de.latlon.xplan.validator.geometric.result.GeometricValidationRule;
import de.latlon.xplan.validator.report.SkipCode;
import de.latlon.xplan.validator.report.ValidatorDetail;
import de.latlon.xplan.validator.report.ValidatorResult;

/**
 * contains the validator result of the geometric validator
 *
 * @author Florian Bingel
 */

public class GeometricValidatorResult extends ValidatorResult {

	private static final String VALIDATION_TYPE_NAME = getMessage("validationType_geometric");

	private final List<GeometricValidationRule> rules;

	public GeometricValidatorResult() {
		super((ValidatorDetail) null);
		this.rules = new ArrayList<>();
	}

	public GeometricValidatorResult(SkipCode skipCode) {
		super(skipCode);
		this.rules = new ArrayList<>();
	}

	public GeometricValidatorResult(List<GeometricValidationRule> planRules) {
		super((ValidatorDetail) null);
		this.rules = planRules;
	}

	@Override
	public String getType() {
		return VALIDATION_TYPE_NAME;
	}

	@Override
	public boolean isValid() {
		return rules.stream()
			.noneMatch(rule -> rule.getFindings().stream().anyMatch(finding -> ERROR.equals(finding.getLevel())));
	}

	public List<String> getWarnings() {
		return collectMessagesByLevel(rules, WARNING);
	}

	public List<String> getErrors() {
		return collectMessagesByLevel(rules, ERROR);
	}

	public List<GeometricValidationRule> getRules() {
		return rules;
	}

	@Override
	public String toString() {
		return "GeometricValidatorResult{" + "planRules=" + rules + '}';
	}

	private List<String> collectMessagesByLevel(Collection<GeometricValidationRule> rules,
			GeometricValidationFindingLevel level) {
		return rules.stream()
			.flatMap(r -> r.getFindings()
				.stream()
				.filter(finding -> level.equals(finding.getLevel()))
				.map(GeometricValidationFinding::getMessage)
				.toList()
				.stream())
			.toList();
	}

}
