/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.report.geojson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.latlon.xplan.validator.geometric.result.GeometricValidationFinding;
import de.latlon.xplan.validator.geometric.result.GeometricValidationRule;
import de.latlon.xplan.validator.planinfo.PlanInfo;
import de.latlon.xplan.validator.report.ReportGenerationException;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.report.geojson.model.Feature;
import de.latlon.xplan.validator.report.geojson.model.FeatureCollection;
import de.latlon.xplan.validator.report.geojson.model.Geometry;
import org.deegree.cs.coordinatesystems.ICRS;
import org.deegree.cs.exceptions.TransformationException;
import org.deegree.cs.exceptions.UnknownCRSException;
import org.deegree.cs.persistence.CRSManager;
import org.deegree.geometry.GeometryTransformer;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public final class GeoJsonBuilder {

	public static final String DEFAULT_CRS_GEOJSON = "http://www.opengis.net/def/crs/OGC/1.3/CRS84";

	private GeoJsonBuilder() {
	}

	/**
	 * @param report never <code>null</code>
	 * @return the GeoJson {@link FeatureCollection} containing all geometric ERRORS and
	 * WARNINGS
	 */
	public static FeatureCollection createGeoJsonFailures(ValidatorReport report) throws ReportGenerationException {
		if (reportHasGeometricFindings(report)) {
			FeatureCollection featureCollection = new FeatureCollection();
			Map<String, PlanInfo> planInfoReport = report.getPlanInfoReport().getPlanInfos();
			for (Map.Entry<String, PlanInfo> entry : planInfoReport.entrySet()) {
				PlanInfo planInfo = entry.getValue();
				List<GeometricValidationRule> rules = planInfo.getGeometricValidatorResult().getRules();
				for (GeometricValidationRule rule : rules) {
					if (!rule.getFindings().isEmpty()) {
						for (GeometricValidationFinding finding : rule.getFindings()) {
							Feature feature = createFeature(rule, finding, planInfo);
							featureCollection.addFeaturesItem(feature);
						}
					}
				}
			}
			return featureCollection;
		}
		return null;
	}

	private static Feature createFeature(GeometricValidationRule rule, GeometricValidationFinding finding,
			PlanInfo planInfo) throws ReportGenerationException {
		Map<String, Object> properties = new HashMap<>();
		properties.put("id", rule.getId());
		properties.put("title", rule.getTitle());
		properties.put("planName", planInfo.getName());
		properties.put("level", finding.getLevel().name());
		properties.put("message", finding.getMessage());
		properties.put("gmlIds", String.join(", ", finding.getGmlIds()));
		Geometry geometry = GeoJsonGeometryBuilder.createGeometry(transformToWgs84(finding.getMarkerGeom()));
		return new Feature().geometry(geometry).properties(properties);
	}

	private static org.deegree.geometry.Geometry transformToWgs84(org.deegree.geometry.Geometry markerGeom)
			throws ReportGenerationException {
		if (markerGeom == null)
			return null;
		try {
			ICRS targetCrs = CRSManager.lookup(DEFAULT_CRS_GEOJSON);
			GeometryTransformer geometryTransformer = new GeometryTransformer(targetCrs);
			return geometryTransformer.transform(markerGeom);
		}
		catch (TransformationException | UnknownCRSException e) {
			throw new ReportGenerationException(e);
		}
	}

	private static boolean reportHasGeometricFindings(ValidatorReport report) {
		return report.getPlanInfoReport() != null && !report.getPlanInfoReport().getPlanInfos().isEmpty()
				&& report.getPlanInfoReport()
					.getPlanInfos()
					.values()
					.stream()
					.anyMatch(planInfo -> planInfo.getGeometricValidatorResult() != null
							&& planInfo.getGeometricValidatorResult()
								.getRules()
								.stream()
								.anyMatch(rules -> !rules.getFindings().isEmpty()));
	}

}
