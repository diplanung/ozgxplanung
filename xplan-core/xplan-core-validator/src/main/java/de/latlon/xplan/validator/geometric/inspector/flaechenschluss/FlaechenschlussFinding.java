/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.geometric.inspector.flaechenschluss;

import static de.latlon.xplan.validator.geometric.inspector.flaechenschluss.FlaechenschlussFinding.FindingType.LUECKE_ERROR;
import static de.latlon.xplan.validator.geometric.inspector.flaechenschluss.FlaechenschlussFinding.FindingType.LUECKE_WARNING;
import static de.latlon.xplan.validator.i18n.ValidationMessages.format;
import static de.latlon.xplan.validator.i18n.ValidationMessages.getMessage;

import java.util.List;
import java.util.stream.Collectors;

import de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel;
import org.deegree.geometry.Geometry;

/**
 *
 * Aggregates same findings for Flaechenschlussbedingung of multiple types in one finidng.
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public record FlaechenschlussFinding(List<String> gmlIds, List<OptimisedFlaechenschlussInspector.TestStep> testSteps,
		FindingType findingType, Geometry markerGeom) {

	enum FindingType {

		LUECKE_WARNING(GeometricValidationFindingLevel.WARNING), LUECKE_ERROR(GeometricValidationFindingLevel.ERROR),
		INVALID_CP_WARNING(GeometricValidationFindingLevel.WARNING),
		INVALID_CP_ERROR(GeometricValidationFindingLevel.ERROR);

		private final GeometricValidationFindingLevel level;

		FindingType(GeometricValidationFindingLevel level) {
			this.level = level;
		}

		public GeometricValidationFindingLevel getLevel() {
			return level;
		}

	}

	/**
	 * @return the message used for the aggregated findings, never <code>null</code>
	 */
	String createMessage() {
		String conditions = testSteps.stream().map(testStep -> {
			switch (testStep) {
				case GELTUNGSBEREICH_BEREICH -> {
					return getMessage("FlaechenschlussInspector_geltungsbereich_bereich");
				}
				case GELTUNGSBEREICH_PLAN -> {
					return getMessage("FlaechenschlussInspector_geltungsbereich_plan");
				}
				default -> {
					return getMessage("FlaechenschlussInspector_flaechenschluss");
				}
			}
		}).collect(Collectors.joining(", "));
		String gmlIdsAsString = gmlIds.stream().collect(Collectors.joining(", "));
		if (LUECKE_WARNING.equals(findingType))
			return format("FlaechenschlussInspector_warning_Luecke", gmlIdsAsString, conditions, markerGeom);
		else if (LUECKE_ERROR.equals(findingType))
			return format("FlaechenschlussInspector_error_Luecke", gmlIdsAsString, conditions, markerGeom);
		return format("FlaechenschlussInspector_error", gmlIdsAsString, conditions, markerGeom);
	}

}
