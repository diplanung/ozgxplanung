/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.geometric.inspector.broken;

import static de.latlon.xplan.validator.i18n.ValidationMessages.getMessage;

import java.util.List;
import java.util.Map;

import de.latlon.xplan.commons.XPlanVersion;
import de.latlon.xplan.commons.feature.XPlanGmlParser;
import de.latlon.xplan.validator.XPlanGmlValidation;
import de.latlon.xplan.validator.geometric.GeometricValidationRetriever;
import de.latlon.xplan.validator.geometric.ValidationResultContext;
import de.latlon.xplan.validator.geometric.inspector.GeometricFeatureInspector;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFinding;
import de.latlon.xplan.validator.geometric.result.GeometricValidationRule;
import org.deegree.feature.Feature;
import org.deegree.gml.feature.FeatureInspectionException;

/**
 * Parses all XP_VerbundenerPlan/verbundenerPlan/@xlink:href of
 *
 * <pre>
 * BP_Plan/aendert
 * BP_Plan/wurdeGeaendertVon
 * BP_Plan/aendertPlan
 * BP_Plan/wurdeGeaendertVonPlan
 * BP_Plan/aendertPlanBereich
 * BP_Plan/wurdeGeaendertVonPlanBereich
 * BP_Bereich/aendertPlan
 * BP_Bereich/wurdeGeaendertVonPlan
 * BP_Bereich/aendertPlanBereich
 * BP_Bereich/wurdeGeaendertVonPlanBereich
 * </pre>
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class BrokenGeometriesInspector implements GeometricFeatureInspector, GeometricValidationRetriever {

	private final ValidationResultContext validationResultContext = new ValidationResultContext(
			getMessage("BrokenGeometriesInspector_id"), getMessage("BrokenGeometriesInspector_title"));

	private XPlanGmlParser xPlanGmlParser;

	@Override
	public boolean applicableForVersion(XPlanVersion version) {
		return true;
	}

	List<GeometricValidationFinding> getFindings() {
		return validationResultContext.getFindings();
	}

	@Override
	public Map<String, GeometricValidationRule> getValidationResult(XPlanGmlValidation gmlValidation) {
		return validationResultContext.createValidationResult(gmlValidation, false);
	}

	@Override
	public void setXPlanGmlParser(XPlanGmlParser xPlanGmlParser) {
		this.xPlanGmlParser = xPlanGmlParser;
	}

	@Override
	public boolean checkGeometricRule() {
		List<String> brokenGeometryErrors = xPlanGmlParser.getSkippedBrokenGeometryErrors();
		for (String brokenGeometryError : brokenGeometryErrors) {
			validationResultContext.addError(brokenGeometryError);
		}
		return validationResultContext.hasNoErroneousFindings();
	}

	@Override
	public void startParsing(String fid) {
	}

	@Override
	public Feature inspect(Feature feature) throws FeatureInspectionException {
		return feature;
	}

}
