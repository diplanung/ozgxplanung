/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.planinfo;

import static de.latlon.xplan.validator.report.SkipCode.INTERNAL_ERRORS;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.feature.XPlanFeatureCollection;
import de.latlon.xplan.commons.util.FeatureCollectionUtils;
import de.latlon.xplan.manager.web.shared.RasterEvaluationResult;
import de.latlon.xplan.validator.GmlParserAdopter;
import de.latlon.xplan.validator.XPlanGmlValidation;
import de.latlon.xplan.validator.reference.ExternalReferenceEvaluator;
import de.latlon.xplan.validator.report.reference.ExternalReferenceReport;
import de.latlon.xplan.validator.web.shared.ValidationSettings;
import de.latlon.xplanbox.core.raster.evaluation.XPlanRasterEvaluator;
import org.deegree.feature.Feature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class PlanInfoParser implements GmlParserAdopter {

	private static final Logger LOG = LoggerFactory.getLogger(PlanInfoParser.class);

	private final Optional<XPlanRasterEvaluator> xPlanRasterEvaluator;

	public PlanInfoParser(Optional<XPlanRasterEvaluator> xPlanRasterEvaluator) {
		this.xPlanRasterEvaluator = xPlanRasterEvaluator;
	}

	@Override
	public void prepareBuilder(XPlanGmlValidation gmlValidation) {
		gmlValidation.getGmlParserBuilder().withSkipResolveReferences(true).withSkipBrokenGeometries(true);
	}

	@Override
	public void afterParserBuild(XPlanGmlValidation xPlanGmlValidation) {
		// nothing to do
	}

	public PlanInfoReport parsePlanInfo(XPlanGmlValidation gmlValidation) {
		try {
			XPlanArchive archive = gmlValidation.getArchive();
			Map<String, PlanInfo> planGmlIdToPlanInfos = new HashMap<>();
			for (XPlanFeatureCollection xPlanFeatureCollection : gmlValidation.getxPlanFeatureCollections()
				.getxPlanGmlInstances()) {
				Feature planFeature = FeatureCollectionUtils.findPlanFeature(xPlanFeatureCollection.getFeatures(),
						xPlanFeatureCollection.getType());
				PlanInfo planInfo = new PlanInfo(planFeature.getId())
					.name(FeatureCollectionUtils.retrievePlanName(xPlanFeatureCollection.getFeatures(),
							xPlanFeatureCollection.getType()))
					.version(xPlanFeatureCollection.getVersion())
					.type(xPlanFeatureCollection.getType())
					.bboxIn4326(xPlanFeatureCollection.getBboxIn4326())
					.externalReferenceReport(parseExternalReferences(xPlanFeatureCollection, archive))
					.rasterEvaluationResults(parseRasterEvaluation(xPlanFeatureCollection, archive,
							gmlValidation.getValidationSettings()));
				planGmlIdToPlanInfos.put(planFeature.getId(), planInfo);
			}
			return new PlanInfoReport().planInfos(planGmlIdToPlanInfos);
		}
		catch (IOException e) {
			LOG.error("Plan could not be parsed. Reason {}", e.getMessage(), e);
			return new PlanInfoReport().skipCode(INTERNAL_ERRORS);
		}
	}

	private ExternalReferenceReport parseExternalReferences(XPlanFeatureCollection features, XPlanArchive archive) {
		ExternalReferenceEvaluator externalReferenceEvaluator = new ExternalReferenceEvaluator();
		return externalReferenceEvaluator.parseAndAddExternalReferences(features, archive);
	}

	private List<RasterEvaluationResult> parseRasterEvaluation(XPlanFeatureCollection featureCollection,
			XPlanArchive archive, ValidationSettings validationSettings) throws IOException {
		if (xPlanRasterEvaluator.isPresent() && !validationSettings.isSkipRasterEvaluation()) {
			return xPlanRasterEvaluator.get().evaluateRasterdata(archive, featureCollection);
		}
		return Collections.emptyList();
	}

}
