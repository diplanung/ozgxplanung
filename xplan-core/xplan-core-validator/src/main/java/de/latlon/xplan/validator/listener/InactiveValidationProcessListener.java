/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.listener;

import de.latlon.xplan.validator.web.shared.ValidationType;

/**
 * Implements {@link ValidationProcessListener with empty methods
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class InactiveValidationProcessListener implements ValidationProcessListener {

	@Override
	public void validationStarted(String uuid) {

	}

	@Override
	public void validationPartStarted(String uuid, ValidationType validationType) {

	}

	@Override
	public void validationPartFinished(String uuid, ValidationType validationType, Boolean isValid) {

	}

	@Override
	public void validationProfileStarted(String uuid, String profileId) {

	}

	@Override
	public void validationProfileFinished(String uuid, String profileId, Boolean isValid) {

	}

	@Override
	public void validationFinished(String uuid, Boolean valid) {

	}

	@Override
	public void validationFailed(String uuid, String error) {

	}

}
