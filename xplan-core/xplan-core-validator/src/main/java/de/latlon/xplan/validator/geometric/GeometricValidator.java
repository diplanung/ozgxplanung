/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.geometric;

import de.latlon.xplan.validator.GmlParserAdopter;
import de.latlon.xplan.validator.XPlanGmlValidation;
import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;

import java.util.Map;

/**
 * Validates <link>XPlanArchives</link> geometrically
 *
 * @author <a href="mailto:erben@lat-lon.de">Alexander Erben</a>
 * @version $Revision: $, $Date: $
 */
public interface GeometricValidator extends GmlParserAdopter {

	/**
	 * Validate geometrically and return a <link>List</link> of error messages
	 * <code>null</code>
	 * @param gmlValidation containing the parsed feature collection and infos required to
	 * valide geometrically, never <code>null</code>
	 * @return a <link>ValidatorReport</link> containing the result of the validation
	 */
	Map<String, GeometricValidatorResult> validateGeometry(XPlanGmlValidation gmlValidation);

}
