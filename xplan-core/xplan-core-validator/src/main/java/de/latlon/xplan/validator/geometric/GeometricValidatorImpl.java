/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.geometric;

import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.ERROR;
import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.WARNING;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.latlon.xplan.commons.XPlanType;
import de.latlon.xplan.commons.XPlanVersion;
import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.feature.XPlanGmlParser;
import de.latlon.xplan.validator.GmlParserAdopter;
import de.latlon.xplan.validator.XPlanGmlValidation;
import de.latlon.xplan.validator.geometric.inspector.GeometricFeatureInspector;
import de.latlon.xplan.validator.geometric.inspector.aenderungen.AenderungenInspector;
import de.latlon.xplan.validator.geometric.inspector.broken.BrokenGeometriesInspector;
import de.latlon.xplan.validator.geometric.inspector.doppelbelegung.DoppelbelegungInspector;
import de.latlon.xplan.validator.geometric.inspector.flaechenschluss.OptimisedFlaechenschlussInspector;
import de.latlon.xplan.validator.geometric.inspector.geltungsbereich.GeltungsbereichInspector;
import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFinding;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel;
import de.latlon.xplan.validator.geometric.result.GeometricValidationRule;
import de.latlon.xplan.validator.web.shared.ValidationOption;
import org.deegree.gml.feature.FeatureInspector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Validates <link>XPlanArchives</link> geometrically
 *
 * @author <a href="mailto:schneider@occamlabs.de">Markus Schneider</a>
 */
public class GeometricValidatorImpl implements GeometricValidator, GmlParserAdopter {

	private static final Logger LOG = LoggerFactory.getLogger(GeometricValidatorImpl.class);

	public static final String SKIP_FLAECHENSCHLUSS_OPTION = "skip-flaechenschluss";

	public static final String SKIP_GELTUNGSBEREICH_OPTION = "skip-geltungsbereich";

	public static final String SKIP_LAUFRICHTUNG_OPTION = "skip-laufrichtung";

	public static final ValidationOption SKIP_FLAECHENSCHLUSS = new ValidationOption(SKIP_FLAECHENSCHLUSS_OPTION,
			Boolean.toString(true));

	public static final ValidationOption SKIP_GELTUNGSBEREICH = new ValidationOption(SKIP_GELTUNGSBEREICH_OPTION,
			Boolean.toString(true));

	public static final ValidationOption SKIP_LAUFRICHTUNG = new ValidationOption(SKIP_LAUFRICHTUNG_OPTION,
			Boolean.toString(true));

	public static final ArrayList<ValidationOption> SKIP_OPTIONS = new ArrayList<>();

	static {
		SKIP_OPTIONS.add(SKIP_FLAECHENSCHLUSS);
		SKIP_OPTIONS.add(SKIP_GELTUNGSBEREICH);
	}

	private final boolean treatAenderungIntegrityAsFailure;

	public GeometricValidatorImpl() {
		this(false);
	}

	public GeometricValidatorImpl(boolean treatAenderungIntegrityAsFailure) {
		this.treatAenderungIntegrityAsFailure = treatAenderungIntegrityAsFailure;
	}

	@Override
	public void prepareBuilder(XPlanGmlValidation gmlValidation) {
		XPlanArchive archive = gmlValidation.getArchive();
		List<ValidationOption> voOptions = gmlValidation.getValidationSettings().getExtendedOptions();
		boolean skipOrientation = isOptionTrue(voOptions, SKIP_LAUFRICHTUNG_OPTION);
		XPlanGeometryInspector geometryInspector = new XPlanGeometryInspector(skipOrientation);
		List<GeometricFeatureInspector> featureInspectors = createInspectorsForVersion(archive.getVersion(),
				archive.getType(), voOptions);
		ArrayList<FeatureInspector> allFeatureInspectors = new ArrayList<>(featureInspectors);
		allFeatureInspectors.add(geometryInspector);
		gmlValidation.getGmlParserBuilder()
			.withSkipResolveReferences(true)
			.withSkipBrokenGeometries(true)
			.withDefaultCrs(archive.getCrs())
			.withFeatureInspectors(allFeatureInspectors)
			.withGeometryInspectors(geometryInspector);
	}

	@Override
	public void afterParserBuild(XPlanGmlValidation gmlValidation) {
		XPlanGmlParser parser = gmlValidation.getParser();
		parser.getFeatureInspectors().forEach(fi -> {
			if (fi instanceof GeometricFeatureInspector)
				((GeometricFeatureInspector) fi).setXPlanGmlParser(parser);
		});
	}

	@Override
	public Map<String, GeometricValidatorResult> validateGeometry(XPlanGmlValidation gmlValidation) {
		ValidatorResult result = checkInspectors(gmlValidation);
		Map<String, GeometricValidatorResult> geometricValidatorResultMap = new HashMap<>();
		result.planRules.forEach(
				(planGmlId, rules) -> geometricValidatorResultMap.put(planGmlId, new GeometricValidatorResult(rules)));
		return geometricValidatorResultMap;
	}

	private ValidatorResult checkInspectors(XPlanGmlValidation gmlValidation) {
		ValidatorResult result = new ValidatorResult();
		long begin = System.currentTimeMillis();
		LOG.info("- Einlesen der Features (+ Geometrievalidierung)...");
		List<GeometricValidationRetriever> featureInspectors = gmlValidation.getParser()
			.getFeatureInspectors()
			.stream()
			.filter(fi -> fi instanceof GeometricValidationRetriever)
			.map(fi -> (GeometricValidationRetriever) fi)
			.toList();

		featureInspectors.forEach(inspector -> checkAndAddRules(inspector, gmlValidation, result));
		result.elapsed = System.currentTimeMillis() - begin;

		logResult(result);
		return result;
	}

	private void checkAndAddRules(GeometricValidationRetriever inspector, XPlanGmlValidation gmlValidation,
			ValidatorResult result) {
		if (inspector instanceof GeometricFeatureInspector)
			((GeometricFeatureInspector) inspector).checkGeometricRule();
		Map<String, GeometricValidationRule> validationResult = inspector.getValidationResult(gmlValidation);
		result.addPlanRules(validationResult);
	}

	private List<GeometricFeatureInspector> createInspectorsForVersion(XPlanVersion version, XPlanType type,
			List<ValidationOption> voOptions) {
		List<GeometricFeatureInspector> inspectors = new ArrayList<>();
		if (!isOptionTrue(voOptions, SKIP_FLAECHENSCHLUSS_OPTION))
			inspectors.add(new OptimisedFlaechenschlussInspector(version, type));
		if (!isOptionTrue(voOptions, SKIP_GELTUNGSBEREICH_OPTION))
			inspectors.add(new GeltungsbereichInspector(version));
		inspectors.add(new DoppelbelegungInspector());
		inspectors.add(new AenderungenInspector(treatAenderungIntegrityAsFailure));
		inspectors.add(new BrokenGeometriesInspector());
		return inspectors.stream()
			.filter(inspector -> inspector.applicableForVersion(version))
			.collect(Collectors.toList());
	}

	private boolean isOptionTrue(List<ValidationOption> voOptions, String optionName) {
		if (voOptions == null)
			return false;
		for (ValidationOption voOption : voOptions) {
			if (optionName.equals(voOption.getName())) {
				if (voOption.getArgument() != null)
					return Boolean.valueOf(voOption.getArgument());
			}
		}
		return false;
	}

	private void logResult(ValidatorResult result) {
		LOG.info("Ergebnisse der geometrischen Validierung:");
		if (result.isValid()) {
			LOG.info("OK [{} ms]: {} Features", result.elapsed, result.noOfFeatures);
			logWarnings(result);
		}
		else {
			logWarnings(result);
			logErrorMessages(result);
		}
	}

	private void logWarnings(ValidatorResult result) {
		List<String> warnings = result.getWarnings();
		if (!warnings.isEmpty()) {
			LOG.info("Geometrie-Warnungen: {}", warnings.size());
			for (String warning : warnings)
				LOG.info(" - {}", warning);
		}
	}

	private void logErrorMessages(ValidatorResult result) {
		List<String> errors = result.getErrors();
		LOG.info("Geometrie-Fehler: {}", errors.size());
		for (String error : errors)
			LOG.info(" - {}", error);
	}

	private static class ValidatorResult {

		private long elapsed;

		private int noOfFeatures;

		private final Map<String, List<GeometricValidationRule>> planRules = new HashMap<>();

		private void setNoOfXPlanInstances(int noOfFeatures) {
			this.noOfFeatures = noOfFeatures;
		}

		public boolean isValid() {
			return isValid(planRules.values().stream().flatMap(List::stream).toList());
		}

		public void addPlanRules(Map<String, GeometricValidationRule> planRules) {
			if (planRules != null) {
				planRules.forEach((planGmlId, rule) -> {
					if (!this.planRules.containsKey(planGmlId))
						this.planRules.put(planGmlId, new ArrayList<>());
					this.planRules.get(planGmlId).add(rule);
				});
			}
		}

		private List<String> getErrors() {
			return collectMessagesByLevel(planRules.values().stream().flatMap(List::stream).toList(), ERROR);
		}

		private List<String> getWarnings() {
			return collectMessagesByLevel(planRules.values().stream().flatMap(List::stream).toList(), WARNING);
		}

		private List<String> collectMessagesByLevel(Collection<GeometricValidationRule> rules,
				GeometricValidationFindingLevel level) {
			return rules.stream()
				.flatMap(rule -> rule.getFindings()
					.stream()
					.filter(finding -> level.equals(finding.getLevel()))
					.map(GeometricValidationFinding::getMessage))
				.toList();
		}

		private boolean isValid(Collection<GeometricValidationRule> rules) {
			return rules.stream()
				.noneMatch(rule -> rule.getFindings().stream().anyMatch(finding -> ERROR.equals(finding.getLevel())));
		}

	}

}
