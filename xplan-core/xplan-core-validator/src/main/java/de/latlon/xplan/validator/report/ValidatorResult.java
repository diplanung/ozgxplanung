/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.report;

/**
 * Encapsulates a single validation result.
 *
 * @author <a href="mailto:erben@lat-lon.de">Alexander Erben</a>
 * @version $Revision: $, $Date: $
 */
public abstract class ValidatorResult {

	private SkipCode skipCode;

	private ValidatorDetail validatorDetails;

	/**
	 * @param validatorDetails some details about the validation, may be <code>null</code>
	 */
	public ValidatorResult(ValidatorDetail validatorDetails) {
		this.validatorDetails = validatorDetails;
	}

	/**
	 * Instantiates a {@link ValidatorResult} for a skipped validation.
	 * @param skipCode the reason why the validation was skipped, never <code>null</code>
	 */
	public ValidatorResult(SkipCode skipCode) {
		this.skipCode = skipCode;
	}

	/**
	 * @return <code>true</code> if the validation results is valid, <code>false</code>
	 * otherwise
	 */
	public abstract boolean isValid();

	/**
	 * @return <code>true</code> if the validation was skipped, <code>false</code>
	 * otherwise
	 */
	public boolean isSkipped() {
		return skipCode != null;
	}

	/**
	 * @return the reason why the validation was skipped, may be <code>null</code> (if not
	 * skipped)
	 */
	public SkipCode getSkipCode() {
		return skipCode;
	}

	/**
	 * @return some details about the validation, may be <code>null</code>
	 */
	public ValidatorDetail getValidatorDetail() {
		return validatorDetails;
	}

	public abstract String getType();

	@Override
	public String toString() {
		if (skipCode != null)
			return "Skipped: " + skipCode;
		return "ValidatorResult{" + "isValid=" + isValid() + ", validatorDetails=" + validatorDetails + '}';
	}

}
