/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.geometric.result;

import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.ERROR;

import java.util.ArrayList;
import java.util.List;

import org.deegree.geometry.Geometry;
import org.deegree.geometry.io.WKTWriter;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class GeometricValidationFinding {

	private String message;

	private GeometricValidationFindingLevel level = ERROR;

	private List<String> gmlIds = new ArrayList<>();

	private Geometry markerGeom;

	public GeometricValidationFinding message(String message) {
		this.message = message;
		return this;
	}

	public GeometricValidationFinding level(GeometricValidationFindingLevel level) {
		this.level = level;
		return this;
	}

	public GeometricValidationFinding markerGeom(Geometry markerGeom) {
		this.markerGeom = markerGeom;
		return this;
	}

	public GeometricValidationFinding gmlIds(String... gmlIds) {
		this.gmlIds = List.of(gmlIds);
		return this;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public GeometricValidationFindingLevel getLevel() {
		return level;
	}

	public void setLevel(GeometricValidationFindingLevel level) {
		this.level = level;
	}

	public List<String> getGmlIds() {
		return gmlIds;
	}

	public void setGmlIds(List<String> gmlIds) {
		this.gmlIds = gmlIds;
	}

	public Geometry getMarkerGeom() {
		return markerGeom;
	}

	public void setMarkerGeom(Geometry markerGeom) {
		this.markerGeom = markerGeom;
	}

	public String getMarkerGeomAsWkt() {
		if (markerGeom == null)
			return null;
		return WKTWriter.write(markerGeom);
	}

	@Override
	public String toString() {
		return "GeometricValidationFinding{" + "message='" + message + '\'' + ", level=" + level + ", gmlIds=" + gmlIds
				+ ", markerGeom=" + getMarkerGeomAsWkt() + '}';
	}

}
