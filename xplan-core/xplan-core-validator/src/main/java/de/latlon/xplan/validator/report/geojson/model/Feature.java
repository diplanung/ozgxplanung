/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
/*
 * GeoJSON format
 * This document defines the GeoJSON format as an OpenAPI. It contains the definitions for 'Feature' object and 'FeatureCollection' objects, as well as the definitions for all 'Geometry' objects. It conforms with the 'RFC-7946' standard from IETF (August 2016 version) Kudos to @bubbobne and @idkw whose code helped me not start from scratch https://gist.github.com/bubbobne/fe5f2db65acf039be6a9fd92fc9c7233 
 *
 * OpenAPI spec version: 1.0.1
 * Contact: zitoun@gmail.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package de.latlon.xplan.validator.report.geojson.model;

import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.FEATURE;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.validation.constraints.NotNull;

/**
 * GeoJSon &#x27;Feature&#x27; object. Generated from OpenAPI document
 * https://app.swaggerhub.com/apis/OlivierMartineau/GeoJSON/1.0.1
 *
 * @since 8.0
 */
@Schema(description = "GeoJSon 'Feature' object")
@Generated(value = "io.swagger.codegen.v3.generators.java.JavaJerseyServerCodegen",
		date = "2024-09-09T05:36:15.967036504Z[GMT]")
public class Feature extends GeoJsonObject {

	@JsonProperty("geometry")
	private Geometry geometry = null;

	@JsonProperty("properties")
	private Map<String, Object> properties = new HashMap<>();

	@JsonProperty("id")
	private Object id = null;

	public Feature() {
		setType(FEATURE);
	}

	public Feature geometry(Geometry geometry) {
		this.geometry = geometry;
		return this;
	}

	/**
	 * Get geometry
	 * @return geometry
	 **/
	@JsonProperty("geometry")
	@Schema(required = true, description = "")
	@NotNull
	public Geometry getGeometry() {
		return geometry;
	}

	public void setGeometry(Geometry geometry) {
		this.geometry = geometry;
	}

	public Feature properties(Map<String, Object> properties) {
		this.properties = properties;
		return this;
	}

	/**
	 * Get properties
	 * @return properties
	 **/
	@JsonProperty("properties")
	@Schema(required = true, description = "")
	public Map<String, Object> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}

	public Feature id(Object id) {
		this.id = id;
		return this;
	}

	/**
	 * Get id
	 * @return id
	 **/
	@JsonProperty("id")
	@Schema(description = "")
	@NotNull
	public Object getId() {
		return id;
	}

	public void setId(Object id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Feature feature = (Feature) o;
		return Objects.equals(this.geometry, feature.geometry) && Objects.equals(this.properties, feature.properties)
				&& Objects.equals(this.id, feature.id) && super.equals(o);
	}

	@Override
	public int hashCode() {
		return Objects.hash(geometry, properties, id, super.hashCode());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Feature {\n");
		sb.append("    ").append(toIndentedString(super.toString())).append("\n");
		sb.append("    geometry: ").append(toIndentedString(geometry)).append("\n");
		sb.append("    properties: ").append(toIndentedString(properties)).append("\n");
		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces (except the
	 * first line).
	 */
	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
