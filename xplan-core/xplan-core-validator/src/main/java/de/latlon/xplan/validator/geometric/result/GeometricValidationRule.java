/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.geometric.result;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class GeometricValidationRule {

	private String id;

	private String title;

	private List<GeometricValidationFinding> findings = new ArrayList<>();

	public GeometricValidationRule(String id, String title) {
		this.id = id;
		this.title = title;
	}

	public GeometricValidationRule findings(List<GeometricValidationFinding> findings) {
		if (findings != null)
			this.findings = findings;
		return this;
	}

	public GeometricValidationRule addFinding(GeometricValidationFinding finding) {
		this.findings.add(finding);
		return this;
	}

	public String getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public List<GeometricValidationFinding> getFindings() {
		if (findings == null)
			return Collections.emptyList();
		return findings;
	}

	@Override
	public String toString() {
		return "GeometricValidationRule{" + "id='" + id + '\'' + ", title='" + title + '\'' + ", findings=" + findings
				+ '}';
	}

}
