/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.planinfo;

import java.util.ArrayList;
import java.util.List;

import de.latlon.xplan.commons.XPlanType;
import de.latlon.xplan.commons.XPlanVersion;
import de.latlon.xplan.manager.web.shared.RasterEvaluationResult;
import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.report.reference.ExternalReferenceReport;
import de.latlon.xplan.validator.semantic.report.SemanticValidatorResult;
import org.deegree.geometry.Envelope;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class PlanInfo {

	private final String planGmlId;

	private String name;

	private XPlanVersion version;

	private XPlanType type;

	private Envelope bboxIn4326;

	private ExternalReferenceReport externalReferenceReport;

	private List<RasterEvaluationResult> rasterEvaluationResults = new ArrayList<>();

	private SemanticValidatorResult semanticValidatorResult;

	private GeometricValidatorResult geometricValidatorResult;

	private final List<SemanticValidatorResult> semanticProfileValidatorResults = new ArrayList<>();

	public PlanInfo(String planGmlId) {
		this.planGmlId = planGmlId;
	}

	public String getPlanGmlId() {
		return planGmlId;
	}

	public PlanInfo name(String name) {
		this.name = name;
		return this;
	}

	public String getName() {
		return name;
	}

	public PlanInfo version(XPlanVersion version) {
		this.version = version;
		return this;
	}

	public XPlanVersion getVersion() {
		return version;
	}

	public PlanInfo type(XPlanType type) {
		this.type = type;
		return this;
	}

	public XPlanType getType() {
		return type;
	}

	public PlanInfo externalReferenceReport(ExternalReferenceReport externalReferenceReport) {
		this.externalReferenceReport = externalReferenceReport;
		return this;
	}

	public ExternalReferenceReport getExternalReferenceReport() {
		return externalReferenceReport;
	}

	public PlanInfo rasterEvaluationResults(List<RasterEvaluationResult> rasterEvaluationResults) {
		if (rasterEvaluationResults != null)
			this.rasterEvaluationResults = rasterEvaluationResults;
		return this;
	}

	public List<RasterEvaluationResult> getRasterEvaluationResults() {
		return rasterEvaluationResults;
	}

	public PlanInfo bboxIn4326(Envelope bboxIn4326) {
		this.bboxIn4326 = bboxIn4326;
		return this;
	}

	public Envelope getBboxIn4326() {
		return bboxIn4326;
	}

	public PlanInfo semanticValidatorResult(SemanticValidatorResult semanticValidatorResult) {
		this.semanticValidatorResult = semanticValidatorResult;
		return this;
	}

	public SemanticValidatorResult getSemanticValidatorResult() {
		return semanticValidatorResult;
	}

	/**
	 * @return result of semantic validation profiles, may be empty but never
	 * <code>null</code>.
	 */
	public List<SemanticValidatorResult> getSemanticProfileValidatorResults() {
		return semanticProfileValidatorResults;
	}

	public PlanInfo geometricValidatorResult(GeometricValidatorResult geometricValidatorResult) {
		this.geometricValidatorResult = geometricValidatorResult;
		return this;
	}

	public GeometricValidatorResult getGeometricValidatorResult() {
		return geometricValidatorResult;
	}

	/**
	 * @param semanticProfileValidatorResult to add, never <code>null</code>.
	 */
	public void addSemanticProfileValidatorResults(SemanticValidatorResult semanticProfileValidatorResult) {
		this.semanticProfileValidatorResults.add(semanticProfileValidatorResult);
	}

	public boolean isValid() {
		return (getSemanticValidatorResult() == null || getSemanticValidatorResult().isValid())
				&& (getGeometricValidatorResult() == null || getGeometricValidatorResult().isValid())
				&& (getSemanticProfileValidatorResults().isEmpty()
						|| getSemanticProfileValidatorResults().stream().anyMatch(SemanticValidatorResult::isValid));
	}

}
