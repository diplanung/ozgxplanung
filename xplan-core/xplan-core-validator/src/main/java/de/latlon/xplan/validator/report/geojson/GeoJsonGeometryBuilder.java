/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.report.geojson;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import de.latlon.xplan.validator.report.geojson.model.Geometry;
import de.latlon.xplan.validator.report.geojson.model.GeometryCollection;
import de.latlon.xplan.validator.report.geojson.model.GeometryElement;
import de.latlon.xplan.validator.report.geojson.model.LineString;
import de.latlon.xplan.validator.report.geojson.model.LineStringCoordinates;
import de.latlon.xplan.validator.report.geojson.model.LinearRing;
import de.latlon.xplan.validator.report.geojson.model.MultiLineString;
import de.latlon.xplan.validator.report.geojson.model.MultiPoint;
import de.latlon.xplan.validator.report.geojson.model.MultiPolygon;
import de.latlon.xplan.validator.report.geojson.model.Point;
import de.latlon.xplan.validator.report.geojson.model.Polygon;
import de.latlon.xplan.validator.report.geojson.model.Position;
import org.deegree.geometry.composite.CompositeGeometry;
import org.deegree.geometry.multi.MultiGeometry;
import org.deegree.geometry.multi.MultiSurface;
import org.deegree.geometry.primitive.Curve;
import org.deegree.geometry.primitive.GeometricPrimitive;
import org.deegree.geometry.primitive.Ring;
import org.deegree.geometry.primitive.Surface;
import org.deegree.geometry.primitive.segments.LineStringSegment;
import org.deegree.geometry.standard.multi.DefaultMultiGeometry;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class GeoJsonGeometryBuilder {

	private GeoJsonGeometryBuilder() {
	}

	/**
	 * @param geom may be <code>null</code>
	 * @return <code>null</code> if the passed geom is <code>null</code>
	 */
	public static Geometry createGeometry(org.deegree.geometry.Geometry geom) {
		if (geom == null)
			return null;
		return geometry(geom);
	}

	private static Geometry geometry(org.deegree.geometry.Geometry geom) {
		switch (geom.getGeometryType()) {
			case PRIMITIVE_GEOMETRY -> {
				return geometry((GeometricPrimitive) geom);
			}
			case COMPOSITE_GEOMETRY -> {
				return geometry((CompositeGeometry) geom);
			}
			case MULTI_GEOMETRY -> {
				return geometry((MultiGeometry) geom);
			}
			default -> throw new IllegalArgumentException(
					"Could not export geometry " + geom.getGeometryType() + " as GeoJSON");
		}
	}

	private static GeometryElement geometry(GeometricPrimitive geom) {
		switch (geom.getPrimitiveType()) {
			case Point -> {
				return geometry((org.deegree.geometry.primitive.Point) geom);
			}
			case Curve -> {
				return geometry((Curve) geom);
			}
			case Surface -> {
				return geometry((Surface) geom);
			}
			default -> throw new IllegalArgumentException(
					"Could not export geometry " + geom.getPrimitiveType() + " as GeoJSON");
		}
	}

	private static GeometryCollection geometry(CompositeGeometry geom) {
		GeometryCollection geometryCollection = new GeometryCollection();
		geom.forEach(g -> geometryCollection.addGeometriesItem(geometry((GeometricPrimitive) g)));
		return geometryCollection;
	}

	private static Geometry geometry(MultiGeometry geom) {
		switch (geom.getMultiGeometryType()) {
			case MULTI_POINT -> {
				return geometry((org.deegree.geometry.multi.MultiPoint) geom);
			}
			case MULTI_LINE_STRING -> {
				return geometry((org.deegree.geometry.multi.MultiLineString) geom);
			}
			case MULTI_CURVE -> {
				return geometry((org.deegree.geometry.multi.MultiCurve<?>) geom);
			}
			case MULTI_POLYGON -> {
				return geometry((org.deegree.geometry.multi.MultiPolygon) geom);
			}
			case MULTI_SURFACE -> {
				return geometry((org.deegree.geometry.multi.MultiSurface<?>) geom);
			}
			case MULTI_GEOMETRY -> {
				return geometry((DefaultMultiGeometry<org.deegree.geometry.Geometry>) geom);
			}
			default -> throw new IllegalArgumentException(
					"Could not export geometry " + geom.getMultiGeometryType() + " as GeoJSON");
		}
	}

	private static GeometryCollection geometry(DefaultMultiGeometry<org.deegree.geometry.Geometry> geom) {
		GeometryCollection geometryCollection = new GeometryCollection();
		geom.forEach(g -> {
			Geometry geometry = geometry(g);
			if (geometry instanceof GeometryElement)
				geometryCollection.addGeometriesItem((GeometryElement) geometry);
			else
				throw new IllegalArgumentException(
						"Could not export geometry " + geom.getGeometryType() + " as GeoJSON");
		});
		return geometryCollection;
	}

	private static LineString geometry(Curve geom) {
		LineStringCoordinates coordinates = lineStringCoordinates(geom);
		return new LineString().coordinates(coordinates);
	}

	private static Polygon geometry(Surface geom) {
		switch (geom.getSurfaceType()) {
			case Polygon -> {
				return geometry((org.deegree.geometry.primitive.Polygon) geom);
			}
			default -> throw new IllegalArgumentException(
					"Could not export geometry " + geom.getSurfaceType() + " as GeoJSON");
		}
	}

	private static MultiPoint geometry(org.deegree.geometry.multi.MultiPoint geom) {
		MultiPoint multiPoint = new MultiPoint();
		geom.stream().forEach(point -> multiPoint.addPositionItem(position(point)));
		return multiPoint;
	}

	private static MultiLineString geometry(org.deegree.geometry.multi.MultiLineString geom) {
		MultiLineString multiLineString = new MultiLineString();
		geom.forEach(lineString -> multiLineString.addLineStringCoordinate(lineStringCoordinates(lineString)));
		return multiLineString;
	}

	private static MultiLineString geometry(org.deegree.geometry.multi.MultiCurve<?> geom) {
		MultiLineString multiLineString = new MultiLineString();
		geom.forEach(lineString -> multiLineString.addLineStringCoordinate(lineStringCoordinates(lineString)));
		return multiLineString;
	}

	private static MultiPolygon geometry(org.deegree.geometry.multi.MultiPolygon geom) {
		MultiPolygon multiPolygon = new MultiPolygon();
		geom.forEach(polygon -> multiPolygon.addLinearRing(linearRings(polygon)));
		return multiPolygon;
	}

	private static MultiPolygon geometry(MultiSurface<?> geom) {
		MultiPolygon multiPolygon = new MultiPolygon();
		geom.forEach(surface -> multiPolygon.addLinearRing(linearRings(surface)));
		return multiPolygon;
	}

	private static Polygon geometry(org.deegree.geometry.primitive.Polygon polygon) {
		List<LinearRing> linearRings = linearRings(polygon);
		return new Polygon().linearRings(linearRings);
	}

	private static Point geometry(org.deegree.geometry.primitive.Point point) {
		return new Point().position(position(point));
	}

	private static List<LinearRing> linearRings(org.deegree.geometry.primitive.Polygon polygon) {
		List<LinearRing> linearRings = new ArrayList<>();
		linearRings.add(linearRing(polygon.getExteriorRing()));
		polygon.getInteriorRings().forEach(interiorRing -> linearRings.add(linearRing(interiorRing)));
		return linearRings;
	}

	private static List<LinearRing> linearRings(Surface surface) {
		switch (surface.getSurfaceType()) {
			case Polygon -> {
				return linearRings((org.deegree.geometry.primitive.Polygon) surface);
			}
			default -> throw new IllegalArgumentException(
					"Could not export geometry " + surface.getSurfaceType() + " as GeoJSON");
		}
	}

	private static LinearRing linearRing(Ring ring) {
		LinearRing linearRing = new LinearRing();
		ring.getControlPoints().forEach(controlPoint -> linearRing.add(position(controlPoint)));
		return linearRing;
	}

	private static LineStringCoordinates lineStringCoordinates(org.deegree.geometry.primitive.Curve geom) {
		LineStringCoordinates coordinates = new LineStringCoordinates();
		geom.getCurveSegments()
			.forEach(curveSegment -> ((LineStringSegment) curveSegment).getControlPoints()
				.forEach(controlPoint -> coordinates.add(position(controlPoint))));
		return coordinates;
	}

	private static Position position(org.deegree.geometry.primitive.Point point) {
		Position position = new Position();
		position.add(BigDecimal.valueOf(point.get0()));
		position.add(BigDecimal.valueOf(point.get1()));
		return position;
	}

}
