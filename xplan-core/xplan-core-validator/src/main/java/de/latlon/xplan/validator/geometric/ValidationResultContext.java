/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.geometric;

import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.ERROR;
import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.WARNING;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import de.latlon.xplan.commons.feature.XPlanFeatureCollection;
import de.latlon.xplan.validator.XPlanGmlValidation;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFinding;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel;
import de.latlon.xplan.validator.geometric.result.GeometricValidationRule;
import org.deegree.geometry.Geometry;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class ValidationResultContext {

	private final List<GeometricValidationFinding> findings = new ArrayList<>();

	private final String ruleId;

	private final String ruleTitle;

	public ValidationResultContext(String ruleId, String ruleTitle) {
		this.ruleId = ruleId;
		this.ruleTitle = ruleTitle;
	}

	public Map<String, GeometricValidationRule> createValidationResult(XPlanGmlValidation gmlValidation,
			boolean isOfficial) {
		if (!isOfficial && findings.isEmpty())
			return Collections.emptyMap();

		Map<String, GeometricValidationRule> planRules = new HashMap<>();

		// add rule to each plan result
		gmlValidation.getxPlanFeatureCollections().getxPlanGmlInstances().forEach(gmlInstance -> {
			String planGmlId = gmlInstance.getPlanId();
			if (!planRules.containsKey(planGmlId))
				planRules.put(planGmlId, new GeometricValidationRule(ruleId, ruleTitle));
		});

		// add findings (if available)
		for (GeometricValidationFinding finding : getFindings()) {
			List<String> gmlIds = finding.getGmlIds();
			List<XPlanFeatureCollection> gmlInstances = gmlValidation.getxPlanFeatureCollections()
				.getxPlanGmlInstances();
			Optional<XPlanFeatureCollection> gmlInstanceOfRule = gmlInstances.stream()
				.filter(gmlInstance -> gmlInstance.getFeatures()
					.stream()
					.anyMatch(feature -> gmlIds.contains(feature.getId())))
				.findFirst();
			if (gmlInstanceOfRule.isPresent()) {
				String planGmlId = gmlInstanceOfRule.get().getPlanId();
				if (!planRules.containsKey(planGmlId))
					planRules.put(planGmlId, new GeometricValidationRule(ruleId, ruleTitle));
				planRules.get(planGmlId).addFinding(finding);
			}
			else {
				// add to all plan results
				planRules.forEach((planGmlId, planRule) -> {
					planRule.addFinding(finding);
				});
			}
		}
		return planRules;
	}

	public void add(GeometricValidationFindingLevel level, String msg, Geometry markerGeom, List<String> gmlIds) {
		findings.add(new GeometricValidationFinding().message(msg)
			.level(level)
			.markerGeom(markerGeom)
			.gmlIds(gmlIds.toArray(String[]::new)));
	}

	public void addError(String msg, String... gmlIds) {
		findings.add(new GeometricValidationFinding().message(msg).level(ERROR).gmlIds(gmlIds));
	}

	public void addError(String msg, Geometry markerGeom, String... gmlIds) {
		findings.add(new GeometricValidationFinding().message(msg).markerGeom(markerGeom).gmlIds(gmlIds));
	}

	public void addWarning(String msg, String... gmlIds) {
		findings.add(new GeometricValidationFinding().message(msg).level(WARNING).gmlIds(gmlIds));
	}

	public void addWarning(String msg, Geometry markerGeom, String... gmlIds) {
		findings
			.add(new GeometricValidationFinding().message(msg).level(WARNING).markerGeom(markerGeom).gmlIds(gmlIds));
	}

	public List<GeometricValidationFinding> getFindings() {
		return findings;
	}

	public boolean hasNoErroneousFindings() {
		return findings.stream().noneMatch(finding -> ERROR.equals(finding.getLevel()));
	}

	public boolean hasFindings() {
		return !findings.isEmpty();
	}

	public List<GeometricValidationFinding> getErroneousFindings() {
		return findings.stream().filter(finding -> ERROR.equals(finding.getLevel())).toList();
	}

	public boolean containsSameFinding(String msg, Geometry markerGeom) {
		return findings.stream()
			.anyMatch(finding -> Objects.equals(finding.getMessage(), msg)
					&& Objects.equals(finding.getMarkerGeom(), markerGeom));
	}

}
