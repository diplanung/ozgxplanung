/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.report.pdf;

import static de.latlon.xplan.validator.i18n.ValidationMessages.format;
import static de.latlon.xplan.validator.i18n.ValidationMessages.getMessage;
import static de.latlon.xplan.validator.report.ReportUtils.asLabel;
import static de.latlon.xplan.validator.report.ReportUtils.createValidLabel;
import static de.latlon.xplan.validator.semantic.report.ValidationResultType.WARNING;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFinding;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel;
import de.latlon.xplan.validator.geometric.result.GeometricValidationRule;
import de.latlon.xplan.validator.planinfo.PlanInfo;
import de.latlon.xplan.validator.planinfo.PlanInfoReport;
import de.latlon.xplan.validator.report.SkipCode;
import de.latlon.xplan.validator.report.ValidatorDetail;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.report.ValidatorResult;
import de.latlon.xplan.validator.report.reference.ExternalReferenceReport;
import de.latlon.xplan.validator.report.reference.ExternalReferenceStatus;
import de.latlon.xplan.validator.semantic.configuration.metadata.RulesMetadata;
import de.latlon.xplan.validator.semantic.report.InvalidFeaturesResult;
import de.latlon.xplan.validator.semantic.report.RuleResult;
import de.latlon.xplan.validator.semantic.report.SemanticValidatorResult;
import de.latlon.xplan.validator.syntactic.report.SyntacticValidatorResult;

/**
 * Use OPenPDF to build a validation report
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version $Revision: $, $Date: $
 */
class ReportBuilder {

	private static final String LABEL_HINT = getMessage("report_pdf_hint");

	private static final String LABEL_WARNING = getMessage("report_pdf_warning");

	private static final String LABEL_ERROR = getMessage("report_pdf_error");

	private static final String LABEL_OK = getMessage("report_pdf_ok");

	private static final Font FONT_TEXT = new Font(Font.HELVETICA, 10);

	private static final Font FONT_H1 = new Font(Font.HELVETICA, 14, Font.BOLD);

	private static final Font FONT_H2 = new Font(Font.HELVETICA, 12, Font.BOLD);

	/**
	 * Write pdf report to the passed {@link Document}.
	 * @param report the validation report to serialize, never <code>null</code>
	 * @param document to write into, never <code>nulll</code>
	 * @throws IllegalArgumentException if the passed report is <code>null</code>
	 */
	void writeReport(ValidatorReport report, Document document) {
		checkReportParam(report);

		appendMetadataSection(report, document);
		appendSyntacticRules(report.getSyntacticValidatorResult(), document);

		Collection<PlanInfo> planInfos = report.getPlanInfoReport().getPlanInfos().values();
		for (PlanInfo planInfo : planInfos) {
			appendHeader("Validierungergebnis des Planwerkes " + planInfo.getName(), document, FONT_H1);
			appendPlanMetadataSection(planInfo, document);
			appendExternalReferenceReport(report.getPlanInfoReport(), document);
			appendGeometricRules(planInfo.getGeometricValidatorResult(), document);
			appendSemanticValidatorResult(planInfo.getSemanticValidatorResult(), document);

			List<SemanticValidatorResult> semanticProfileValidatorResults = planInfo
				.getSemanticProfileValidatorResults();
			if (!semanticProfileValidatorResults.isEmpty()) {
				semanticProfileValidatorResults.sort(Comparator.comparing(o -> o.getRulesMetadata().getName()));
				for (SemanticValidatorResult profileSemanticValidatorResult : semanticProfileValidatorResults) {
					appendHeaderAndResultOfProfile(profileSemanticValidatorResult, document);
					appendSemanticValidatorResult(profileSemanticValidatorResult, document);
				}
			}
		}

	}

	private void checkReportParam(ValidatorReport report) {
		if (report == null)
			throw new IllegalArgumentException("Report must not be null!");
	}

	private void appendMetadataSection(ValidatorReport report, Document document) {
		PdfPTable table = new PdfPTable(2);
		table.setWidths(new int[] { 25, 75 });
		table.setTotalWidth(tableWidth(document));
		table.setLockedWidth(true);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		table.getDefaultCell().setPaddingBottom(10);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

		table.addCell(createTableCell("Validierungsname:", FONT_TEXT));
		table.addCell(createTableCell(report.getValidationName(), FONT_TEXT));
		table.addCell(createTableCell("Dateiname:", FONT_TEXT));
		table.addCell(createTableCell(report.getArchiveName(), FONT_TEXT));

		document.add(table);
	}

	private void appendPlanMetadataSection(PlanInfo planInfo, Document document) {
		PdfPTable table = new PdfPTable(2);
		table.setWidths(new int[] { 25, 75 });
		table.setTotalWidth(tableWidth(document));
		table.setLockedWidth(true);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		table.getDefaultCell().setPaddingBottom(10);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

		table.addCell(createTableCell("Planname:", FONT_TEXT));
		table.addCell(createTableCell(planInfo.getName(), FONT_TEXT));
		table.addCell(createTableCell("XPlanGML-Version:", FONT_TEXT));
		table.addCell(createTableCell(asLabel(planInfo.getVersion()), FONT_TEXT));
		table.addCell(createTableCell("Planart:", FONT_TEXT));
		String planType = planInfo.getType() != null ? planInfo.getType().name() : "-";
		table.addCell(createTableCell(planType, FONT_TEXT));
		table.addCell(createTableCell("Validierungsergebnis:", FONT_TEXT));
		table.addCell(createTableCell(createValidLabel(planInfo.isValid()), FONT_TEXT));

		document.add(table);
	}

	private void appendExternalReferenceReport(PlanInfoReport report, Document document) {
		if (report == null || report.getPlanInfos().isEmpty())
			return;
		List<ExternalReferenceReport> externalReferenceReports = report.getPlanInfos()
			.values()
			.stream()
			.map(PlanInfo::getExternalReferenceReport)
			.filter(Objects::nonNull)
			.toList();
		if (!externalReferenceReports.isEmpty()) {
			appendHeader(getMessage("report_pdf_externalReferences"), document);

			PdfPTable table = new PdfPTable(1);
			table.setWidths(new int[] { 100 });
			table.setTotalWidth(tableWidth(document));
			table.setLockedWidth(true);
			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			table.getDefaultCell().setPaddingBottom(5);
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

			Set<SkipCode> skipCode = externalReferenceReports.stream()
				.map(ExternalReferenceReport::getSkipCode)
				.filter(Objects::nonNull)
				.collect(Collectors.toSet());
			if (!skipCode.isEmpty()) {
				document.add(new Paragraph(skipCode.stream().findFirst().get().getMessage()));
			}
			List<Map<String, ExternalReferenceStatus>> references = externalReferenceReports.stream()
				.map(ExternalReferenceReport::getReferencesAndStatus)
				.filter(Objects::nonNull)
				.toList();
			if (!references.isEmpty()) {
				references.forEach(externalReferences -> {
					externalReferences.forEach((name, status) -> {
						String nameAndStatus = String.format("%s (%s)", name, status.getLabel());
						table.addCell(new Paragraph(nameAndStatus, FONT_TEXT));
					});
				});
			}
			document.add(table);
		}
	}

	private void appendSemanticValidatorResult(SemanticValidatorResult result, Document document) {
		if (result != null) {
			appendHeaderAndResult(result, document);
			appendDetailsHint(result, document);
			appendRulesMetadata(result, document);
			appendNumberOfRules(result, document);
			appendNumberOfFailedRules(result, document);
			appendNumberOfValidRules(result, document);
			appendSemanticValidatorRules(result.getRules(), document);
		}
	}

	private void appendRulesMetadata(SemanticValidatorResult semanticValidatorResult, Document document) {
		RulesMetadata rulesMetadata = semanticValidatorResult.getRulesMetadata();
		if (rulesMetadata != null) {
			if (rulesMetadata.getName() != null) {
				String name = String.format(" Name: %s", rulesMetadata.getName());
				appendText(name, document);
			}
			if (rulesMetadata.getDescription() != null) {
				String description = String.format(" Beschreibung: %s", rulesMetadata.getDescription());
				appendText(description, document);
			}
			String version = format("report_pdf_versionRules", rulesMetadata.getVersion());
			appendText(version, document);
			String source = format("report_pdf_sourceRules", rulesMetadata.getSource());
			appendText(source, document);
		}
	}

	private void appendNumberOfRules(SemanticValidatorResult semanticValidatorResult, Document document) {
		int noOfRules = semanticValidatorResult.getRules().size();
		String text = format("report_pdf_noOfCheckedRules", noOfRules);
		appendText(text, document);
	}

	private void appendNumberOfFailedRules(SemanticValidatorResult semanticValidatorResult, Document document) {
		long noOfRules = semanticValidatorResult.getRules().stream().filter(r -> !r.isValid()).count();
		String text = format("report_pdf_noOfInvalidRules", noOfRules);
		appendText(text, document);
	}

	private void appendNumberOfValidRules(SemanticValidatorResult semanticValidatorResult, Document document) {
		long noOfRules = semanticValidatorResult.getRules().stream().filter(r -> r.isValid()).count();
		String text = format("report_pdf_noOfValidRules", noOfRules);
		appendText(text, document);
	}

	private void appendSyntacticRules(SyntacticValidatorResult result, Document document) {
		if (result != null) {
			appendHeaderAndResult(result, document);
			appendDetailsHint(result, document);

			PdfPTable table = createValidationResultTable(document, 15, 75);
			for (String message : result.getMessages()) {
				table.addCell(new Paragraph(LABEL_HINT, FONT_TEXT));
				table.addCell(new Paragraph(message, FONT_TEXT));
			}
			document.add(table);
		}
	}

	private void appendGeometricRules(GeometricValidatorResult result, List<GeometricValidationRule> rules,
			Document document) {
		if (result != null && rules != null && !rules.isEmpty()) {
			appendHeaderAndResult(result, document);
			appendDetailsHint(result, document);
			List<String> warnings = collectMessagesByLevel(rules, GeometricValidationFindingLevel.WARNING);
			List<String> errors = collectMessagesByLevel(rules, GeometricValidationFindingLevel.ERROR);

			PdfPTable table = createValidationResultTable(document, 15, 75);
			for (String message : warnings) {
				table.addCell(new Paragraph(LABEL_WARNING, FONT_TEXT));
				table.addCell(new Paragraph(message, FONT_TEXT));
			}
			for (String message : errors) {
				table.addCell(new Paragraph(LABEL_ERROR, FONT_TEXT));
				table.addCell(new Paragraph(message, FONT_TEXT));
			}
			document.add(table);
		}
	}

	private List<String> collectMessagesByLevel(Collection<GeometricValidationRule> rules,
			GeometricValidationFindingLevel level) {
		return rules.stream()
			.flatMap(r -> r.getFindings()
				.stream()
				.filter(finding -> level.equals(finding.getLevel()))
				.map(GeometricValidationFinding::getMessage)
				.toList()
				.stream())
			.toList();
	}

	private void appendGeometricRules(GeometricValidatorResult result, Document document) {
		if (result != null) {
			appendHeaderAndResult(result, document);
			appendDetailsHint(result, document);

			PdfPTable table = createValidationResultTable(document, 15, 75);
			for (String message : result.getWarnings()) {
				table.addCell(new Paragraph(LABEL_WARNING, FONT_TEXT));
				table.addCell(new Paragraph(message, FONT_TEXT));
			}
			for (String message : result.getErrors()) {
				table.addCell(new Paragraph(LABEL_ERROR, FONT_TEXT));
				table.addCell(new Paragraph(message, FONT_TEXT));
			}
			document.add(table);
		}
	}

	private void appendHeaderAndResult(ValidatorResult result, Document document) {
		PdfPTable table = new PdfPTable(2);
		table.setWidths(new int[] { 75, 25 });
		table.setTotalWidth(tableWidth(document));
		table.setLockedWidth(true);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		table.getDefaultCell().setPaddingTop(7);
		table.getDefaultCell().setPaddingBottom(5);
		table.getDefaultCell().setBorder(Rectangle.BOTTOM);

		table.addCell(new Paragraph(result.getType(), FONT_H2));
		table.addCell(new Paragraph(getResultMessage(result), FONT_H2));
		document.add(table);
	}

	private void appendHeader(String header, Document document) {
		appendHeader(header, document, FONT_H2);
	}

	private void appendHeader(String header, Document document, Font font) {
		PdfPTable table = new PdfPTable(1);
		table.setWidths(new int[] { 100 });
		table.setTotalWidth(tableWidth(document));
		table.setLockedWidth(true);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		table.getDefaultCell().setPaddingTop(7);
		table.getDefaultCell().setPaddingBottom(5);
		table.getDefaultCell().setBorder(Rectangle.BOTTOM);

		table.addCell(new Paragraph(header, font));
		document.add(table);
	}

	private void appendHeaderAndResultOfProfile(SemanticValidatorResult result, Document document) {
		String text = "Profil " + result.getRulesMetadata().getName();
		document.add(new Paragraph(text, FONT_H2));
		document.add(new Paragraph(getResultMessage(result), FONT_H2));
	}

	private void appendDetailsHint(ValidatorResult validatorResult, Document document) {
		if (validatorResult != null && validatorResult.getValidatorDetail() != null) {
			ValidatorDetail detailsHint = validatorResult.getValidatorDetail();
			appendText(detailsHint.toString(), document);
		}
	}

	private void appendSemanticValidatorRules(List<RuleResult> ruleResults, Document document) {
		appendText(getMessage("report_pdf_details"), document);

		PdfPTable table = createValidationResultTable(document, 15, 15, 70);
		for (RuleResult ruleResult : ruleResults) {
			List<InvalidFeaturesResult> invalidFeaturesResults = ruleResult.getInvalidFeaturesResults();
			if (invalidFeaturesResults.isEmpty()) {
				appendSemanticValidatorRuleResult(LABEL_OK, ruleResult.getName(), ruleResult.getMessage(),
						Collections.emptyList(), table);
			}
			else {
				for (InvalidFeaturesResult invalidRuleResult : invalidFeaturesResults) {
					String label = WARNING.equals(invalidRuleResult.getResultType()) ? LABEL_WARNING : LABEL_ERROR;
					appendSemanticValidatorRuleResult(label, ruleResult.getName(), invalidRuleResult.getMessage(),
							invalidRuleResult.getGmlIds(), table);
				}
			}
		}
		document.add(table);
	}

	private void appendSemanticValidatorRuleResult(String label, String name, String message, List<String> gmlIds,
			PdfPTable table) {
		StringBuilder messageBuilder = new StringBuilder(message);
		if (!gmlIds.isEmpty()) {
			messageBuilder.append(getMessage("report_pdf_gmlIds"));
			messageBuilder.append(gmlIds.stream().collect(Collectors.joining(", ")));
		}
		table.addCell(new Paragraph(label, FONT_TEXT));
		table.addCell(new Paragraph(name, FONT_TEXT));
		table.addCell(new Paragraph(messageBuilder.toString(), FONT_TEXT));
	}

	private void appendText(String text, Document document) {
		if (text != null) {
			Paragraph paragraph = new Paragraph(text, FONT_TEXT);
			paragraph.setIndentationLeft(indentation(document, 10));
			document.add(paragraph);
		}
	}

	private PdfPTable createValidationResultTable(Document document, int... widths) {
		PdfPTable table = new PdfPTable(widths.length);
		table.setWidths(widths);
		table.setTotalWidth(tableWidth(document) - 30);
		table.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.setSpacingBefore(5);
		table.setLockedWidth(true);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		table.getDefaultCell().setPaddingBottom(5);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		return table;
	}

	private PdfPCell createTableCell(String text, Font font) {
		String textToAdd = text != null ? text : "-";
		PdfPCell cell = new PdfPCell(new Paragraph(textToAdd, font));
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setBorder(Rectangle.NO_BORDER);
		return cell;
	}

	private String getResultMessage(ValidatorResult result) {
		if (result.isSkipped()) {
			return result.getSkipCode().getMessage();
		}
		return createValidLabel(result.isValid());
	}

	private float tableWidth(Document document) {
		float width = document.getPageSize().getWidth();
		return width - 72;
	}

	private float indentation(Document document, int indentation) {
		float left = document.getPageSize().getLeft();
		return left + indentation;
	}

}
