﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_7F6CB5F1-9F25-4D80-B271-41DC610C22A7" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/2 http://www.xplanungwiki.de/upload/XPlanGML/5.2/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/5/2">
<!-- Gml_27BB7449-1EFB-4947-BA10-DDF479C4D9C1: Schnittpunkte mit dem Umring des Geltungsbereich: (574515.3505587532,5947128.14535072),(574677.9877482426,5947128.829007594) -->
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>574369.114 5947113.1631</gml:lowerCorner>
      <gml:upperCorner>574973.8737 5947514.7064</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="Gml_7F15F4F7-92AA-4604-A944-B5296D79706A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574369.114 5947113.1631</gml:lowerCorner>
          <gml:upperCorner>574973.8737 5947514.7064</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>2_Schnittpunkte_Geltungsbereich_Polygon</xplan:name>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_A1A9C0F9-34D8-4D40-A444-D90CBC403BDA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574948.6922 5947223.8427 574972.718 5947414.4767 574973.8737 5947423.6464 
574968.0287 5947423.9573 574611.3662 5947502.9334 574463.3403 5947514.7064 
574449.422 5947501.569 574447.491 5947494.794 574439.535 5947466.864 
574437.479 5947459.643 574436.408 5947455.882 574436.937 5947455.702 
574438.315 5947455.235 574436.025 5947445.66 574432.5307 5947431.0496 
574434.648 5947430.501 574436.211 5947430.096 574433.582 5947418.761 
574431.252 5947408.716 574445.288 5947406.526 574438.487 5947382.289 
574427.052 5947341.535 574420.762 5947319.113 574413.387 5947292.703 
574406.238 5947267.102 574392.093 5947216.471 574372.303 5947217.945 
574369.114 5947218.0566 574369.3581 5947196.2684 574370.2471 5947116.9213 
574370.2892 5947113.1631 574375.1651 5947113.2373 574375.008 5947122.642 
574428.001 5947123.528 574479.523 5947127.996 574493.66 5947128.054 
574498.8049 5947128.0757 574566.816 5947128.362 574660.776 5947128.758 
574702.71 5947128.931 574748.898 5947129.118 574748.851 5947140.899 
574748.66 5947189.2228 574748.634 5947195.807 574749.3333 5947198.2846 
574749.382 5947198.457 574749.678 5947198.453 574767.01 5947198.217 
574786.906 5947197.945 574823.227 5947205.023 574854.016 5947212.936 
574885.02 5947213.359 574947.4293 5947213.8217 574948.6922 5947223.8427 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:bereich xlink:href="#Gml_07DEBC3D-B397-49CD-B684-FAB7DBFDD2D5" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="Gml_07DEBC3D-B397-49CD-B684-FAB7DBFDD2D5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574369.114 5947113.1631</gml:lowerCorner>
          <gml:upperCorner>574973.8737 5947514.7064</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_69F3DD9C-F84F-4B3D-93CA-5AE53ED24885" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574948.6922 5947223.8427 574972.718 5947414.4767 574973.8737 5947423.6464 
574968.0287 5947423.9573 574611.3662 5947502.9334 574463.3403 5947514.7064 
574449.422 5947501.569 574447.491 5947494.794 574439.535 5947466.864 
574437.479 5947459.643 574436.408 5947455.882 574436.937 5947455.702 
574438.315 5947455.235 574436.025 5947445.66 574432.5307 5947431.0496 
574434.648 5947430.501 574436.211 5947430.096 574433.582 5947418.761 
574431.252 5947408.716 574445.288 5947406.526 574438.487 5947382.289 
574427.052 5947341.535 574420.762 5947319.113 574413.387 5947292.703 
574406.238 5947267.102 574392.093 5947216.471 574372.303 5947217.945 
574369.114 5947218.0566 574369.3581 5947196.2684 574370.2471 5947116.9213 
574370.2892 5947113.1631 574375.1651 5947113.2373 574375.008 5947122.642 
574428.001 5947123.528 574479.523 5947127.996 574493.66 5947128.054 
574498.8049 5947128.0757 574566.816 5947128.362 574660.776 5947128.758 
574702.71 5947128.931 574748.898 5947129.118 574748.851 5947140.899 
574748.66 5947189.2228 574748.634 5947195.807 574749.3333 5947198.2846 
574749.382 5947198.457 574749.678 5947198.453 574767.01 5947198.217 
574786.906 5947197.945 574823.227 5947205.023 574854.016 5947212.936 
574885.02 5947213.359 574947.4293 5947213.8217 574948.6922 5947223.8427 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#Gml_27BB7449-1EFB-4947-BA10-DDF479C4D9C1" />
      <xplan:gehoertZuPlan xlink:href="#Gml_7F15F4F7-92AA-4604-A944-B5296D79706A" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gebiet gml:id="Gml_27BB7449-1EFB-4947-BA10-DDF479C4D9C1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574489.4277 5947124.7342</gml:lowerCorner>
          <gml:upperCorner>574748.8854 5947227.2604</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_07DEBC3D-B397-49CD-B684-FAB7DBFDD2D5" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_DE8B877F-4875-495B-B5C9-A9F31788F743" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_50043A6D-9F03-41BC-9272-1C481879B8AA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">574677.8346 5947135.3241 574748.8854 5947132.571 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_44F33864-A514-4B90-8D4F-6D30AFD85792" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">574748.8854 5947132.571 574748.851 5947140.899 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D35010D6-5F56-47ED-A823-31F54BF9A194" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">574748.851 5947140.899 574748.7661 5947162.6273 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C0472EFF-23A3-4431-9CB0-C5361BB6ED11" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">574748.7661 5947162.6273 574677.1156 5947165.8172 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B4E7496B-0344-4D18-AC55-816D33BCB958" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">574677.1156 5947165.8172 574675.8353 5947220.1166 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7E79AC0B-179D-4CE6-87A1-2CD21588C16B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">574675.8353 5947220.1166 574496.0506 5947227.2604 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_32821057-C2CC-46B4-AA88-F4C4AF077B3A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">574496.0506 5947227.2604 574489.4277 5947155.5979 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E7943510-C54D-4272-9A65-91F0FCE0CFFB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">574489.4277 5947155.5979 574502.0616 5947136.442 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_A0B0E816-C8C3-40E5-B224-C8CD465F20C1" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">574502.0616 5947136.442 574516.438240449 5947127.72462079 574533.0179 5947124.9326 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DCABDF27-5F0B-4BB1-A151-D0CE26191DA7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">574533.0179 5947124.9326 574566.816 5947128.362 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BAB61062-6C0F-4FCA-9260-195E331241BD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">574566.816 5947128.362 574678.0843 5947124.7342 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C77F8460-0742-4D7F-930A-7DBAEB2D8277" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">574678.0843 5947124.7342 574677.8346 5947135.3241 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:SO_Gebiet>
  </gml:featureMember>
</xplan:XPlanAuszug>
