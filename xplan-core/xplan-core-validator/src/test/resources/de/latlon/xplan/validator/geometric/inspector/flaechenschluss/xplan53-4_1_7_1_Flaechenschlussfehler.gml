<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<!--
Testplan für Version 5.3, der folgende Neuerungen beinhaltet: 
XP_ArtHoehenbezug=3500 (CR-016)
BP_FlaecheOhneFestsetzung (CR-037)
MinGRWohneinheit (CR-045)
BP_Rechtsstand=50001 (CR-52)
BP_ZentralerVersorgungsbereich (CR-021)
BP_Plan mit neuen Veränderungssperre-Attributen (CR-051)
-->
<xplan:XPlanAuszug xmlns:adv="http://www.adv-online.de/nas" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xplan="http://www.xplanung.de/xplangml/5/3" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wfs="http://www.opengis.net/wfs/2.0" gml:id="id03cca746-e25b-49ea-a012-beaf85a63818" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/3 file:///C:/_Lokale_Daten_ungesichert/XPlanung/5_3/XPlanung-Operationen.xsd">
	<gml:boundedBy>
		<gml:Envelope srsName="EPSG:25832">
			<gml:lowerCorner>557243.062 5936905.959</gml:lowerCorner>
			<gml:upperCorner>557352.03 5937000.801</gml:upperCorner>
		</gml:Envelope>
	</gml:boundedBy>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_0fc51f62-6a65-474f-8284-5ce22a0c6b3a">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557332.326 5936921.811</gml:lowerCorner>
					<gml:upperCorner>557332.326 5936921.811</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_667a1561-6dd1-4852-b856-794fa0033c11">
					<gml:pos>557332.326 5936921.811</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_1a78c54e-9179-422e-b8e9-eb64dc7097b2">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557311.865 5936971.444</gml:lowerCorner>
					<gml:upperCorner>557311.865 5936971.444</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_eb8b926e-4648-4055-8693-50a6f6cbc780">
					<gml:pos>557311.865 5936971.444</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>2000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_2c5cd4ae-8269-4b2c-aa97-0a20d083a6a1">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557264.185 5936959.893</gml:lowerCorner>
					<gml:upperCorner>557264.185 5936959.893</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_44abde69-ead4-462e-9dde-bfd6402a7b71">
					<gml:pos>557264.185 5936959.893</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_35b41d25-91e7-4fbb-a736-2b8d45b4d92c">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557319.737 5936917.872</gml:lowerCorner>
					<gml:upperCorner>557319.737 5936917.872</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_225626dc-a093-4cc6-b50f-a1c024bab6d5">
					<gml:pos>557319.737 5936917.872</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_399137a3-6ed8-469f-8467-1a299a626b6e">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557269.976 5936956.155</gml:lowerCorner>
					<gml:upperCorner>557269.976 5936956.155</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_0332dc5c-dc2c-40a4-9034-9fc88b5296e0">
					<gml:pos>557269.976 5936956.155</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_45ac081e-d914-4b32-bd23-8f8b47fd6337">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557334.822 5936957.891</gml:lowerCorner>
					<gml:upperCorner>557334.822 5936957.891</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_9bcd8eee-dad6-45c3-8d56-9f80c6047324">
					<gml:pos>557334.822 5936957.891</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_58198dc5-410e-4899-aab1-6c9b49380d89">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557332.34 5936935.78</gml:lowerCorner>
					<gml:upperCorner>557332.34 5936935.78</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_6023c8b4-4942-4740-87d1-3aa9b09ca39e">
					<gml:pos>557332.34 5936935.78</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_6a9c220b-0e7d-4092-8efd-b3f49b47ad9a">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557295.236 5936933.449</gml:lowerCorner>
					<gml:upperCorner>557295.236 5936933.449</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_4052e2d5-2518-4a32-9a52-fcc1ca4442d2">
					<gml:pos>557295.236 5936933.449</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_8ad15a94-adc2-45a0-8941-f65e1789a6e1">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557337.513 5936969.295</gml:lowerCorner>
					<gml:upperCorner>557337.513 5936969.295</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_455a79de-2970-4117-a4e2-8ee7ee831699">
					<gml:pos>557337.513 5936969.295</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_93b6492f-38dd-4407-91e3-59e54f9ad479">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557252.747 5936958.095</gml:lowerCorner>
					<gml:upperCorner>557252.747 5936958.095</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_cc338897-5801-4c83-be2b-cf00280aa057">
					<gml:pos>557252.747 5936958.095</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>2000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_9d37066a-fc9c-49d1-a174-26ebe698544b">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557335.055 5936955.002</gml:lowerCorner>
					<gml:upperCorner>557335.055 5936955.002</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_1aca33f5-aa8f-4267-a878-666e3356b734">
					<gml:pos>557335.055 5936955.002</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_b257263b-ef36-4e79-aec6-40828d294b89">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557317.273 5936926.333</gml:lowerCorner>
					<gml:upperCorner>557317.273 5936926.333</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_39484e80-df87-4ebf-a116-b02ab47a86cb">
					<gml:pos>557317.273 5936926.333</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_b3cd5c23-f203-430a-a3fa-c48c87dcd2bb">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557333 5936941.461</gml:lowerCorner>
					<gml:upperCorner>557333 5936941.461</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_baf5f583-7298-4439-878f-c461eee22937">
					<gml:pos>557333 5936941.461</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_b6df3477-0838-4dbf-9759-2465791431bc">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557297.018 5936968.178</gml:lowerCorner>
					<gml:upperCorner>557297.018 5936968.178</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_e6777a01-b2a2-4ad3-bcca-f4ed629b0b03">
					<gml:pos>557297.018 5936968.178</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>2000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_b848ccef-9c0a-48b5-9860-c564d3917061">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557335.397 5936951.59</gml:lowerCorner>
					<gml:upperCorner>557335.397 5936951.59</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_cc418e1c-abd1-47d2-b645-3b847ba2ae88">
					<gml:pos>557335.397 5936951.59</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_dbf33225-0a65-465d-98d5-c0be7d6b617e">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557328.375 5936928.357</gml:lowerCorner>
					<gml:upperCorner>557328.375 5936928.357</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_883a44c2-b3b8-4e5d-8598-bafed529f704">
					<gml:pos>557328.375 5936928.357</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_e012746e-38b8-4d63-94f8-18569750e3d9">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557259.315 5936950.212</gml:lowerCorner>
					<gml:upperCorner>557259.315 5936950.212</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_a975b2dc-f9a4-4bc4-91c8-df77cfabe5c5">
					<gml:pos>557259.315 5936950.212</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_e4d0b31a-ea35-4506-b96f-9ab941d6174d">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557315.82 5936977.616</gml:lowerCorner>
					<gml:upperCorner>557315.82 5936977.616</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_b7952f42-1010-41dd-b546-eae6546ac983">
					<gml:pos>557315.82 5936977.616</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>2000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_e5c95662-3bad-405c-9f9c-05645dbbfba9">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557275.595 5936943.471</gml:lowerCorner>
					<gml:upperCorner>557275.595 5936943.471</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_91debb58-8b47-4775-b174-c906f2302b3f">
					<gml:pos>557275.595 5936943.471</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_e6859531-f92b-44b0-9e23-d56e82a63759">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557334.972 5936939.567</gml:lowerCorner>
					<gml:upperCorner>557334.972 5936939.567</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_21fecae0-2a9f-4a6e-9819-357473d16f90">
					<gml:pos>557334.972 5936939.567</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_ebce8a69-eb87-4165-8691-eb8c5d2480a4">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557334.275 5936972.765</gml:lowerCorner>
					<gml:upperCorner>557334.275 5936972.765</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_55ff7a59-8461-4905-8712-4850efff1528">
					<gml:pos>557334.275 5936972.765</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_ed47f67d-ed30-4e36-985a-2a2611445764">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557273.674 5936962.69</gml:lowerCorner>
					<gml:upperCorner>557273.674 5936962.69</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_8d7574a5-5d55-456e-9776-dfe9a1d783b5">
					<gml:pos>557273.674 5936962.69</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>1000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_f1860701-0776-4cd6-af09-791abcbb6f80">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557286.207 5936969.124</gml:lowerCorner>
					<gml:upperCorner>557286.207 5936969.124</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_eadd7333-bae9-4379-ac48-d113aa443e6e">
					<gml:pos>557286.207 5936969.124</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
			<xplan:massnahme>2000</xplan:massnahme>
			<xplan:gegenstand>1000</xplan:gegenstand>
			<xplan:istAusgleich>false</xplan:istAusgleich>
		</xplan:BP_AnpflanzungBindungErhaltung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_BauGrenze gml:id="GML_0a9d432b-b65e-454c-9f35-e8c4f55acd0f">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557310.823 5936931.207</gml:lowerCorner>
					<gml:upperCorner>557327.575 5936967.95</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:ebene>0</xplan:ebene>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Curve gml:id="GML_df84cd17-9b09-4522-95de-a03c76a20a6d">
					<gml:segments>
						<gml:LineStringSegment>
							<gml:posList srsDimension="2" count="5">557323.539 5936931.207 557327.575 5936966.477 557314.892 5936967.95 557310.823 5936932.677 557323.539 5936931.207 </gml:posList>
						</gml:LineStringSegment>
					</gml:segments>
				</gml:Curve>
			</xplan:position>
		</xplan:BP_BauGrenze>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_BauGrenze gml:id="GML_954e03f8-4888-459e-b192-66edf5cd4b10">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557284.229 5936937.668</gml:lowerCorner>
					<gml:upperCorner>557299.272 5936959.488</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:ebene>0</xplan:ebene>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Curve gml:id="GML_8e114075-4df3-40be-a68e-53aa32eebda6">
					<gml:segments>
						<gml:LineStringSegment>
							<gml:posList srsDimension="2" count="5">557296.947 5936937.668 557299.272 5936958.036 557286.554 5936959.488 557284.229 5936939.12 557296.947 5936937.668 </gml:posList>
						</gml:LineStringSegment>
					</gml:segments>
				</gml:Curve>
			</xplan:position>
		</xplan:BP_BauGrenze>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_BaugebietsTeilFlaeche gml:id="GML_c3e0bb6a-9e1d-4461-8995-fe1eebf5bfcd">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557273.326 5936921.698</gml:lowerCorner>
					<gml:upperCorner>557330.77 5936982.349</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:ebene>0</xplan:ebene>
			<xplan:hoehenangabe>
				<xplan:XP_Hoehenangabe>
					<xplan:hoehenbezug>1000</xplan:hoehenbezug>
					<xplan:bezugspunkt>2000</xplan:bezugspunkt>
					<xplan:h uom="m">50</xplan:h>
				</xplan:XP_Hoehenangabe>
			</xplan:hoehenangabe>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:wirdDargestelltDurch xlink:href="#GML_498bba9c-faec-46f2-a7d3-17a576053da7"/>
			<xplan:wirdDargestelltDurch xlink:href="#GML_8b8ff5aa-cafb-4740-a12a-adc5ee16856b"/>
			<xplan:wirdDargestelltDurch xlink:href="#GML_0c35b24f-6cf5-4e98-9460-a7b2f4e37766"/>
			<xplan:wirdDargestelltDurch xlink:href="#GML_ed1db422-4904-4271-a048-1832df70c070"/>
			<xplan:wirdDargestelltDurch xlink:href="#GML_09ae3866-c6fc-427c-baf1-2c88200f54b6"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:refTextInhalt xlink:href="#GML_684fb51c-3d0f-4993-8ff0-dc3b62686b88"/>
			<xplan:refTextInhalt xlink:href="#GML_15da3577-65bc-4d3e-b36d-69b3364d8027"/>
			<xplan:refTextInhalt xlink:href="#GML_3c86cc25-e5d6-4672-b5f3-f433f0ade539"/>
			<xplan:refTextInhalt xlink:href="#GML_6ffd7105-ba11-41bd-8ec5-c5c1bcc962c4"/>
			<xplan:refTextInhalt xlink:href="#GML_102950a6-e58f-492e-a576-a383c2902330"/>
			<xplan:refTextInhalt xlink:href="#GML_60722a9c-5b21-4eb7-983f-ffe65830a990"/>
			<xplan:refTextInhalt xlink:href="#GML_91d49462-d233-47df-9ac1-f4d37c68af8f"/>
			<xplan:refTextInhalt xlink:href="#GML_c6413128-a7f6-4420-86fa-5fceb7ce283b"/>
			<xplan:refTextInhalt xlink:href="#GML_5d50c47d-d8a4-40ad-93fb-b17bcc1b6849"/>
			<xplan:position>
						<gml:Polygon srsName="EPSG:25832" gml:id="GML_cde23e34-38f8-44d9-985e-cde564222f94">
							<gml:exterior>
								<gml:LinearRing>
									<gml:posList srsDimension="2" count="25">557327.161 5936981.911 557321.72 5936982.349 557315.876 5936981.17 557303.85 5936976.479 557301.814 5936975.865 557282.799 5936970.132 557282.027 5936969.9 557281.151 5936962.053 557275.7 5936962.653 557273.326 5936942.061 557278.277 5936939.821 557281.666 5936938.219 557284.957 5936936.594 557291.454 5936933.297 557303.403 5936926.263 557310.6 5936921.698 557311.567 5936930.039 557326.285 5936928.337 557330.77 5936967.193 557330.759 5936967.721 557330.62 5936968.23 557330.36 5936968.69 557329.996 5936969.071 557326.077 5936972.271 557327.161 5936981.911 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
			</xplan:position>
			<xplan:flaechenschluss>true</xplan:flaechenschluss>
			<xplan:MinGRWohneinheit uom="m2">600</xplan:MinGRWohneinheit>
			<xplan:GFZ>0.4</xplan:GFZ>
			<xplan:Z>2</xplan:Z>
			<xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
		</xplan:BP_BaugebietsTeilFlaeche>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_Bereich gml:id="GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8">
			<xplan:nummer>1</xplan:nummer>			
			<xplan:planinhalt xlink:href="#GML_ed47f67d-ed30-4e36-985a-2a2611445764"/>
			<xplan:planinhalt xlink:href="#GML_8ad15a94-adc2-45a0-8941-f65e1789a6e1"/>
			<xplan:planinhalt xlink:href="#GML_ce98283b-08ad-46e9-b702-c0496a87ea63"/>
			<xplan:planinhalt xlink:href="#GML_435c123d-74af-4da3-9b08-2230d60f4afc"/>
			<xplan:planinhalt xlink:href="#GML_e5c95662-3bad-405c-9f9c-05645dbbfba9"/>
			<xplan:planinhalt xlink:href="#GML_b257263b-ef36-4e79-aec6-40828d294b89"/>
			<xplan:planinhalt xlink:href="#GML_b6df3477-0838-4dbf-9759-2465791431bc"/>
			<xplan:planinhalt xlink:href="#GML_377d43a1-8c65-4039-bd90-889d75a76b87"/>
			<xplan:planinhalt xlink:href="#GML_0a9d432b-b65e-454c-9f35-e8c4f55acd0f"/>
			<xplan:planinhalt xlink:href="#GML_399137a3-6ed8-469f-8467-1a299a626b6e"/>
			<xplan:planinhalt xlink:href="#GML_e4d0b31a-ea35-4506-b96f-9ab941d6174d"/>
			<xplan:planinhalt xlink:href="#GML_11a36ea8-653e-4b2a-9b24-ba5429c9faa6"/>
			<xplan:planinhalt xlink:href="#GML_c3e0bb6a-9e1d-4461-8995-fe1eebf5bfcd"/>
			<xplan:planinhalt xlink:href="#GML_e6859531-f92b-44b0-9e23-d56e82a63759"/>
			<xplan:planinhalt xlink:href="#GML_f1860701-0776-4cd6-af09-791abcbb6f80"/>
			<xplan:planinhalt xlink:href="#GML_f5c89321-8a1b-4562-8364-b2cccf41dc3c"/>
			<xplan:planinhalt xlink:href="#GML_488c129a-c949-4c2b-8532-298829f8815f"/>
			<xplan:planinhalt xlink:href="#GML_e9a3d8e9-50dc-4b84-bf0a-101182add402"/>
			<xplan:planinhalt xlink:href="#GML_5751cd04-1a14-4c04-86e8-3d08e3cfc690"/>
			<xplan:planinhalt xlink:href="#GML_3fe2035b-5c40-4fbd-b937-bbca2b9b1d4e"/>
			<xplan:planinhalt xlink:href="#GML_93b6492f-38dd-4407-91e3-59e54f9ad479"/>
			<xplan:planinhalt xlink:href="#GML_9d37066a-fc9c-49d1-a174-26ebe698544b"/>
			<xplan:planinhalt xlink:href="#GML_d1b7b2a9-184e-46c1-b326-df611ecee4b2"/>
			<xplan:planinhalt xlink:href="#GML_209d05c2-6a1c-4d7c-8139-9c7f71dfcc13"/>
			<xplan:planinhalt xlink:href="#GML_45ac081e-d914-4b32-bd23-8f8b47fd6337"/>
			<xplan:planinhalt xlink:href="#GML_b848ccef-9c0a-48b5-9860-c564d3917061"/>
			<xplan:planinhalt xlink:href="#GML_dbf33225-0a65-465d-98d5-c0be7d6b617e"/>
			<xplan:planinhalt xlink:href="#GML_f553de4c-97e6-4f45-a5c2-a89886d18301"/>
			<xplan:planinhalt xlink:href="#GML_b3cd5c23-f203-430a-a3fa-c48c87dcd2bb"/>
			<xplan:planinhalt xlink:href="#GML_382429ee-f25f-4d00-b826-8e804ddbdebc"/>
			<xplan:planinhalt xlink:href="#GML_ebce8a69-eb87-4165-8691-eb8c5d2480a4"/>
			<xplan:planinhalt xlink:href="#GML_1a78c54e-9179-422e-b8e9-eb64dc7097b2"/>
			<xplan:planinhalt xlink:href="#GML_09d5413d-ccd7-40a1-8298-80bb22850dd3"/>
			<xplan:planinhalt xlink:href="#GML_954e03f8-4888-459e-b192-66edf5cd4b10"/>
			<xplan:planinhalt xlink:href="#GML_6a9c220b-0e7d-4092-8efd-b3f49b47ad9a"/>
			<xplan:planinhalt xlink:href="#GML_e012746e-38b8-4d63-94f8-18569750e3d9"/>
			<xplan:planinhalt xlink:href="#GML_5824d0ca-ea90-441f-830c-c070f1a0aa05"/>
			<xplan:planinhalt xlink:href="#GML_35b41d25-91e7-4fbb-a736-2b8d45b4d92c"/>
			<xplan:planinhalt xlink:href="#GML_0fc51f62-6a65-474f-8284-5ce22a0c6b3a"/>
			<xplan:planinhalt xlink:href="#GML_58198dc5-410e-4899-aab1-6c9b49380d89"/>
			<xplan:planinhalt xlink:href="#GML_2c5cd4ae-8269-4b2c-aa97-0a20d083a6a1"/>
			<xplan:planinhalt xlink:href="#GML_7ef4531b-e85e-40e4-8c09-0e19a3ec5177"/>
			<xplan:praesentationsobjekt xlink:href="#GML_042e6f7a-e1c3-4569-b5a5-de1b996e5ba7"/>
			<xplan:praesentationsobjekt xlink:href="#GML_730b4676-8840-4cbd-a2e0-7e351cc61965"/>
			<xplan:praesentationsobjekt xlink:href="#GML_2dea9178-9249-4fd3-b4ba-ba48bfc27ecd"/>
			<xplan:praesentationsobjekt xlink:href="#GML_498bba9c-faec-46f2-a7d3-17a576053da7"/>
			<xplan:praesentationsobjekt xlink:href="#GML_8b8ff5aa-cafb-4740-a12a-adc5ee16856b"/>
			<xplan:praesentationsobjekt xlink:href="#GML_7087c593-4639-4fef-8b91-773ed64df8cf"/>
			<xplan:praesentationsobjekt xlink:href="#GML_0c35b24f-6cf5-4e98-9460-a7b2f4e37766"/>
			<xplan:praesentationsobjekt xlink:href="#GML_ed1db422-4904-4271-a048-1832df70c070"/>
			<xplan:praesentationsobjekt xlink:href="#GML_a39fac97-534a-4266-9e20-b25c1008a015"/>
			<xplan:praesentationsobjekt xlink:href="#GML_5e033921-530a-47b1-84e3-a508f45c2d51"/>
			<xplan:praesentationsobjekt xlink:href="#GML_9fa3a84d-a7ed-4e6d-b75e-8611dbbe3097"/>
			<xplan:praesentationsobjekt xlink:href="#GML_09ae3866-c6fc-427c-baf1-2c88200f54b6"/>
			<xplan:praesentationsobjekt xlink:href="#GML_ab6dc9c7-febf-4465-b4c6-fc60838ed212"/>
			<xplan:praesentationsobjekt xlink:href="#GML_794de089-2788-4cb4-ab79-3ba5f7639710"/>
			<xplan:praesentationsobjekt xlink:href="#GML_b87933a3-7241-47d2-b1f3-ccceb7edeee5"/>
			<xplan:praesentationsobjekt xlink:href="#GML_c47097ae-79f6-4001-841b-da9b80979252"/>
			<xplan:gehoertZuPlan xlink:href="#GML_fe4856a1-1a37-4eec-bae5-1685f1a2feb2"/>
		</xplan:BP_Bereich>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_EingriffsBereich gml:id="GML_435c123d-74af-4da3-9b08-2230d60f4afc">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557248.588 5936911.596</gml:lowerCorner>
					<gml:upperCorner>557339.808 5936982.349</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:text>Grundstücke denen Flächen mit landschaftspflegerischen Ausgleichs- und Ersatzmaßnahmen zugeordnet sind</xplan:text>
			<xplan:gliederung2>Z</xplan:gliederung2>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:wirdDargestelltDurch xlink:href="#GML_9fa3a84d-a7ed-4e6d-b75e-8611dbbe3097"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:refTextInhalt xlink:href="#GML_c6413128-a7f6-4420-86fa-5fceb7ce283b"/>
			<xplan:position>
						<gml:Polygon srsName="EPSG:25832" gml:id="GML_6e77ae96-9884-48be-855d-71d274399c4b">
							<gml:exterior>
								<gml:LinearRing>
									<gml:posList srsDimension="2" count="54">557248.791 5936962.546 557248.721 5936962.244 557248.628 5936961.678 557248.588 5936961.154 557248.596 5936960.519 557248.665 5936959.903 557248.816 5936959.205 557249.051 5936958.51 557250.405 5936954.713 557250.635 5936954.194 557250.89 5936953.7 557251.197 5936953.184 557251.493 5936952.746 557251.875 5936952.249 557252.304 5936951.762 557252.673 5936951.391 557253.172 5936950.948 557253.708 5936950.537 557254.369 5936950.105 557255.024 5936949.748 557256.016 5936949.325 557264.737 5936945.806 557273.326 5936942.061 557278.277 5936939.821 557281.666 5936938.219 557284.957 5936936.594 557291.454 5936933.297 557303.403 5936926.263 557310.6 5936921.698 557314.46 5936919.234 557318.283 5936916.733 557322.093 5936914.178 557325.854 5936911.596 557326.21 5936913.258 557333.18 5936912.819 557334.723 5936925.766 557339.07 5936940.522 557339.808 5936948.038 557339.627 5936968.536 557338.756 5936972.394 557336.499 5936975.641 557331.253 5936980.259 557327.65 5936981.872 557321.72 5936982.349 557315.876 5936981.17 557303.85 5936976.479 557301.814 5936975.865 557282.799 5936970.132 557282.027 5936969.9 557281.355 5936969.697 557273.435 5936969.011 557265.387 5936967.74 557248.878 5936962.848 557248.791 5936962.546 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
			</xplan:position>
			<xplan:flaechenschluss>false</xplan:flaechenschluss>
		</xplan:BP_EingriffsBereich>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_GruenFlaeche gml:id="GML_382429ee-f25f-4d00-b826-8e804ddbdebc">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557310.6 5936911.596</gml:lowerCorner>
					<gml:upperCorner>557339.808 5936981.911</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:text>Garten</xplan:text>
			<xplan:ebene>0</xplan:ebene>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:wirdDargestelltDurch xlink:href="#GML_5e033921-530a-47b1-84e3-a508f45c2d51"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:refTextInhalt xlink:href="#GML_f145a635-83f9-4455-bf23-fadadbb78936"/>
			<xplan:position>
						<gml:Polygon srsName="EPSG:25832" gml:id="GML_c237f7d4-6464-4f86-95b0-4f7149cf90be">
							<gml:exterior>
								<gml:LinearRing>
									<gml:posList srsDimension="2" count="25">557314.46 5936919.234 557318.283 5936916.733 557322.093 5936914.178 557325.854 5936911.596 557326.21 5936913.258 557333.18 5936912.819 557334.723 5936925.766 557339.07 5936940.522 557339.808 5936948.038 557339.627 5936968.536 557338.756 5936972.394 557336.499 5936975.641 557331.253 5936980.259 557327.65 5936981.872 557327.161 5936981.911 557326.077 5936972.271 557329.996 5936969.071 557330.36 5936968.69 557330.62 5936968.23 557330.759 5936967.721 557330.77 5936967.193 557326.285 5936928.337 557311.567 5936930.039 557310.6 5936921.698 557314.46 5936919.234 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
			</xplan:position>
			<xplan:flaechenschluss>true</xplan:flaechenschluss>
			<xplan:nutzungsform>1000</xplan:nutzungsform>
		</xplan:BP_GruenFlaeche>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_GruenFlaeche gml:id="GML_5824d0ca-ea90-441f-830c-c070f1a0aa05">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557248.588 5936942.061</gml:lowerCorner>
					<gml:upperCorner>557282.027 5936969.9</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:text>Garten</xplan:text>
			<xplan:ebene>0</xplan:ebene>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:wirdDargestelltDurch xlink:href="#GML_a39fac97-534a-4266-9e20-b25c1008a015"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:refTextInhalt xlink:href="#GML_f145a635-83f9-4455-bf23-fadadbb78936"/>
			<xplan:position>
						<gml:Polygon srsName="EPSG:25832" gml:id="GML_8de294a1-7355-4dc0-a5c6-c048f219c028">
							<gml:exterior>
								<gml:LinearRing>
									<gml:posList srsDimension="2" count="31">557281.355 5936969.697 557273.435 5936969.011 557265.387 5936967.74 557248.878 5936962.848 557248.791 5936962.546 557248.721 5936962.244 557248.628 5936961.678 557248.588 5936961.154 557248.596 5936960.519 557248.665 5936959.903 557248.816 5936959.205 557249.051 5936958.51 557250.405 5936954.713 557250.635 5936954.194 557250.89 5936953.7 557251.197 5936953.184 557251.493 5936952.746 557251.875 5936952.249 557252.304 5936951.762 557252.673 5936951.391 557253.172 5936950.948 557253.708 5936950.537 557254.369 5936950.105 557255.024 5936949.748 557256.016 5936949.325 557264.737 5936945.806 557273.326 5936942.061 557275.7 5936962.653 557281.151 5936962.053 557282.027 5936969.9 557281.355 5936969.697 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
			</xplan:position>
			<xplan:flaechenschluss>true</xplan:flaechenschluss>
			<xplan:nutzungsform>1000</xplan:nutzungsform>
		</xplan:BP_GruenFlaeche>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_HoehenMass gml:id="GML_11a36ea8-653e-4b2a-9b24-ba5429c9faa6">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557279.643 5936957.718</gml:lowerCorner>
					<gml:upperCorner>557279.643 5936957.718</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
			<xplan:hoehenangabe>
				<xplan:XP_Hoehenangabe>
					<xplan:hoehenbezug>1000</xplan:hoehenbezug>
					<xplan:h uom="m">39.6</xplan:h>
				</xplan:XP_Hoehenangabe>
			</xplan:hoehenangabe>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_3cb49006-831e-41ba-a8f7-14c1758fcfa8">
					<gml:pos>557279.643 5936957.718</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
		</xplan:BP_HoehenMass>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_HoehenMass gml:id="GML_3fe2035b-5c40-4fbd-b937-bbca2b9b1d4e">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557302.401 5936982.865</gml:lowerCorner>
					<gml:upperCorner>557302.401 5936982.865</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
			<xplan:hoehenangabe>
				<xplan:XP_Hoehenangabe>
					<xplan:hoehenbezug>3500</xplan:hoehenbezug>
					<xplan:h uom="m">39.2</xplan:h>
				</xplan:XP_Hoehenangabe>
			</xplan:hoehenangabe>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_7ebf9710-5c2c-43a9-a9e0-403bf7f47759">
					<gml:pos>557302.401 5936982.865</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
		</xplan:BP_HoehenMass>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_HoehenMass gml:id="GML_ce98283b-08ad-46e9-b702-c0496a87ea63">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557341.29 5936931.253</gml:lowerCorner>
					<gml:upperCorner>557341.29 5936931.253</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
			<xplan:hoehenangabe>
				<xplan:XP_Hoehenangabe>
					<xplan:hoehenbezug>1000</xplan:hoehenbezug>
					<xplan:h uom="m">39.1</xplan:h>
				</xplan:XP_Hoehenangabe>
			</xplan:hoehenangabe>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_70492997-57b9-46c5-a45f-2e42352bf4ba">
					<gml:pos>557341.29 5936931.253</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
		</xplan:BP_HoehenMass>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_HoehenMass gml:id="GML_f5c89321-8a1b-4562-8364-b2cccf41dc3c">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557273.66 5936937.873</gml:lowerCorner>
					<gml:upperCorner>557273.66 5936937.873</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
			<xplan:hoehenangabe>
				<xplan:XP_Hoehenangabe>
					<xplan:hoehenbezug>1000</xplan:hoehenbezug>
					<xplan:h uom="m">38.8</xplan:h>
				</xplan:XP_Hoehenangabe>
			</xplan:hoehenangabe>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_4c10da30-1e39-4bbc-8d6a-f7d1e9e9dbfd">
					<gml:pos>557273.66 5936937.873</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
		</xplan:BP_HoehenMass>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_KennzeichnungsFlaeche gml:id="GML_d1b7b2a9-184e-46c1-b326-df611ecee4b2">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557248.588 5936911.596</gml:lowerCorner>
					<gml:upperCorner>557339.808 5936982.349</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>5000</xplan:rechtscharakter>
			<xplan:refTextInhalt xlink:href="#GML_c6413128-a7f6-4420-86fa-5fceb7ce283b"/>
			<xplan:refTextInhalt xlink:href="#GML_62103eff-5b29-4ba8-bc3f-517b910f8e53"/>
			<xplan:position>
						<gml:Polygon srsName="EPSG:25832" gml:id="GML_f55f17cd-22ec-4c06-a374-62e7c03d7f29">
							<gml:exterior>
								<gml:LinearRing>
									<gml:posList srsDimension="2" count="54">557248.791 5936962.546 557248.721 5936962.244 557248.628 5936961.678 557248.588 5936961.154 557248.596 5936960.519 557248.665 5936959.903 557248.816 5936959.205 557249.051 5936958.51 557250.405 5936954.713 557250.635 5936954.194 557250.89 5936953.7 557251.197 5936953.184 557251.493 5936952.746 557251.875 5936952.249 557252.304 5936951.762 557252.673 5936951.391 557253.172 5936950.948 557253.708 5936950.537 557254.369 5936950.105 557255.024 5936949.748 557256.016 5936949.325 557264.737 5936945.806 557273.326 5936942.061 557278.277 5936939.821 557281.666 5936938.219 557284.957 5936936.594 557291.454 5936933.297 557303.403 5936926.263 557310.6 5936921.698 557314.46 5936919.234 557318.283 5936916.733 557322.093 5936914.178 557325.854 5936911.596 557326.21 5936913.258 557333.18 5936912.819 557334.723 5936925.766 557339.07 5936940.522 557339.808 5936948.038 557339.627 5936968.536 557338.756 5936972.394 557336.499 5936975.641 557331.253 5936980.259 557327.65 5936981.872 557321.72 5936982.349 557315.876 5936981.17 557303.85 5936976.479 557301.814 5936975.865 557282.799 5936970.132 557282.027 5936969.9 557281.355 5936969.697 557273.435 5936969.011 557265.387 5936967.74 557248.878 5936962.848 557248.791 5936962.546 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
			</xplan:position>
			<xplan:flaechenschluss>false</xplan:flaechenschluss>
			<xplan:zweckbestimmung>8000</xplan:zweckbestimmung>
		</xplan:BP_KennzeichnungsFlaeche>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_NebenanlagenFlaeche gml:id="GML_09d5413d-ccd7-40a1-8298-80bb22850dd3">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557281.151 5936959.004</gml:lowerCorner>
					<gml:upperCorner>557309.674 5936975.865</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:wirdDargestelltDurch xlink:href="#GML_ab6dc9c7-febf-4465-b4c6-fc60838ed212"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:refTextInhalt xlink:href="#GML_6ffd7105-ba11-41bd-8ec5-c5c1bcc962c4"/>
			<xplan:position>
						<gml:Polygon srsName="EPSG:25832" gml:id="GML_a75edf4d-8903-4141-b104-710b2e2c2c58">
							<gml:exterior>
								<gml:LinearRing>
									<gml:posList srsDimension="2" count="8">557282.799 5936970.132 557282.027 5936969.9 557281.151 5936962.053 557307.997 5936959.004 557309.674 5936974.034 557302.158 5936974.881 557301.814 5936975.865 557282.799 5936970.132 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
			</xplan:position>
			<xplan:flaechenschluss>false</xplan:flaechenschluss>
			<xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
		</xplan:BP_NebenanlagenFlaeche>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_NutzungsartenGrenze gml:id="GML_5751cd04-1a14-4c04-86e8-3d08e3cfc690">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557328.596 5936947.081</gml:lowerCorner>
					<gml:upperCorner>557339.71 5936948.363</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Curve gml:id="GML_486dce28-e668-4fe6-8c8a-a9b817b1fa9d">
					<gml:segments>
						<gml:LineStringSegment>
							<gml:posList srsDimension="2" count="2">557328.596 5936948.363 557339.71 5936947.081 </gml:posList>
						</gml:LineStringSegment>
					</gml:segments>
				</gml:Curve>
			</xplan:position>
			<xplan:typ>9999</xplan:typ>
		</xplan:BP_NutzungsartenGrenze>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_NutzungsartenGrenze gml:id="GML_e9a3d8e9-50dc-4b84-bf0a-101182add402">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557328.471 5936945.991</gml:lowerCorner>
					<gml:upperCorner>557339.58 5936947.274</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Curve gml:id="GML_532e00f2-f9c0-4b32-81f6-5e4dda715cc2">
					<gml:segments>
						<gml:LineStringSegment>
							<gml:posList srsDimension="2" count="2">557328.471 5936947.274 557339.58 5936945.991 </gml:posList>
						</gml:LineStringSegment>
					</gml:segments>
				</gml:Curve>
			</xplan:position>
			<xplan:typ>9999</xplan:typ>
		</xplan:BP_NutzungsartenGrenze>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_Plan gml:id="GML_fe4856a1-1a37-4eec-bae5-1685f1a2feb2">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557243.062 5936905.959</gml:lowerCorner>
					<gml:upperCorner>557352.03 5937000.801</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:name>xplan53-4_1_7_1</xplan:name>
			<xplan:technHerstellDatum>2020-09-10</xplan:technHerstellDatum>
			<xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
			<xplan:raeumlicherGeltungsbereich>
				<gml:MultiSurface srsName="EPSG:25832" gml:id="GML_306679b7-39fa-4cf4-9907-c61c6102d458">
					<gml:surfaceMember>
								<gml:Polygon srsName="EPSG:25832" gml:id="GML_4908ccda-9296-4249-a752-c9fdb3698731">
									<gml:exterior>
										<gml:LinearRing>
											<gml:posList srsDimension="2" count="12">557249.959 5936945.345 557268.735 5936937.278 557287.56 5936929.18 557291.839 5936926.58 557315.773 5936912.04 557322.859 5936907.734 557341.699 5936905.959 557342.14 5936914.554 557352.03 5937000.801 557345.86 5936998.938 557243.062 5936967.903 557249.959 5936945.345 </gml:posList>
										</gml:LinearRing>
									</gml:exterior>
								</gml:Polygon>
					</gml:surfaceMember>
				</gml:MultiSurface>
			</xplan:raeumlicherGeltungsbereich>
			<xplan:texte xlink:href="#GML_c6413128-a7f6-4420-86fa-5fceb7ce283b"/>
			<xplan:texte xlink:href="#GML_684fb51c-3d0f-4993-8ff0-dc3b62686b88"/>
			<xplan:texte xlink:href="#GML_5ba5da80-515e-44f5-95dc-8f4270638840"/>
			<xplan:texte xlink:href="#GML_8270cc97-0729-4664-bea1-6e4d70064d09"/>
			<xplan:texte xlink:href="#GML_15da3577-65bc-4d3e-b36d-69b3364d8027"/>
			<xplan:texte xlink:href="#GML_8a5370e5-d396-4fb2-8b78-09bfcbb1ff4c"/>
			<xplan:texte xlink:href="#GML_62103eff-5b29-4ba8-bc3f-517b910f8e53"/>
			<xplan:texte xlink:href="#GML_c2ed4d85-41ef-470d-a176-f3cdd45a7066"/>
			<xplan:texte xlink:href="#GML_3c86cc25-e5d6-4672-b5f3-f433f0ade539"/>
			<xplan:texte xlink:href="#GML_6ffd7105-ba11-41bd-8ec5-c5c1bcc962c4"/>
			<xplan:texte xlink:href="#GML_102950a6-e58f-492e-a576-a383c2902330"/>
			<xplan:texte xlink:href="#GML_60722a9c-5b21-4eb7-983f-ffe65830a990"/>
			<xplan:texte xlink:href="#GML_7d7e15fb-c55d-47ce-9a16-dd72dfd39d6f"/>
			<xplan:texte xlink:href="#GML_91d49462-d233-47df-9ac1-f4d37c68af8f"/>
			<xplan:texte xlink:href="#GML_24d91abb-1f19-4a7d-8903-b1aea424e2c1"/>
			<xplan:texte xlink:href="#GML_5d50c47d-d8a4-40ad-93fb-b17bcc1b6849"/>
			<xplan:texte xlink:href="#GML_f145a635-83f9-4455-bf23-fadadbb78936"/>
			<xplan:texte xlink:href="#GML_a7e0ecd6-ba53-44a2-9103-9bf204c7119f"/>
			<xplan:gemeinde>
				<xplan:XP_Gemeinde>
					<xplan:ags>02000000</xplan:ags>
					<xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
					<xplan:ortsteilName>221</xplan:ortsteilName>
				</xplan:XP_Gemeinde>
			</xplan:gemeinde>
			<xplan:planArt>1000</xplan:planArt>
			<xplan:verfahren>1000</xplan:verfahren>
			<xplan:rechtsstand>50001</xplan:rechtsstand>
			<xplan:aenderungenBisDatum>2020-09-10</xplan:aenderungenBisDatum>
			<xplan:aufstellungsbeschlussDatum>2017-09-18</xplan:aufstellungsbeschlussDatum>
			<xplan:veraenderungssperreBeschlussDatum>2017-09-18</xplan:veraenderungssperreBeschlussDatum>
			<xplan:veraenderungssperreDatum>2017-09-19</xplan:veraenderungssperreDatum>
			<xplan:veraenderungssperreEndDatum>2019-09-18</xplan:veraenderungssperreEndDatum>
			<xplan:verlaengerungVeraenderungssperre>2000</xplan:verlaengerungVeraenderungssperre>
			<xplan:auslegungsStartDatum>2019-03-07</xplan:auslegungsStartDatum>
			<xplan:auslegungsEndDatum>2019-04-08</xplan:auslegungsEndDatum>
			<xplan:satzungsbeschlussDatum>2019-04-25</xplan:satzungsbeschlussDatum>
			<xplan:rechtsverordnungsDatum>2020-07-10</xplan:rechtsverordnungsDatum>
			<xplan:inkrafttretensDatum>2020-08-01</xplan:inkrafttretensDatum>
			<xplan:veraenderungssperre>true</xplan:veraenderungssperre>
			<xplan:staedtebaulicherVertrag>false</xplan:staedtebaulicherVertrag>
			<xplan:erschliessungsVertrag>false</xplan:erschliessungsVertrag>
			<xplan:durchfuehrungsVertrag>false</xplan:durchfuehrungsVertrag>
			<xplan:gruenordnungsplan>false</xplan:gruenordnungsplan>
			<xplan:bereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
		</xplan:BP_Plan>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_FlaecheOhneFestsetzung gml:id="GML_488c129a-c949-4c2b-8532-298829f8815f">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557243.062 5936905.959</gml:lowerCorner>
					<gml:upperCorner>557352.03 5937000.801</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:ebene>0</xplan:ebene>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>5000</xplan:rechtscharakter>
			<xplan:position>
						<gml:Polygon srsName="EPSG:25832" gml:id="GML_26d807d2-4494-4127-a9f8-f7ebd8477fa3">
							<gml:exterior>
								<gml:LinearRing>
									<gml:posList srsDimension="2" count="12">557249.959 5936945.345 557268.735 5936937.278 557287.56 5936929.18 557291.839 5936926.58 557315.773 5936912.04 557322.859 5936907.734 557341.699 5936905.959 557342.14 5936914.554 557352.03 5937000.801 557345.86 5936998.938 557243.062 5936967.903 557249.959 5936945.345 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
							<gml:interior>
								<gml:LinearRing>
									<gml:posList srsDimension="2" count="54">557248.878 5936962.848 557265.387 5936967.74 557273.435 5936969.011 557281.355 5936969.697 557282.027 5936969.9 557282.799 5936970.132 557301.814 5936975.865 557303.85 5936976.479 557315.876 5936981.17 557321.72 5936982.349 557327.65 5936981.872 557331.253 5936980.259 557336.499 5936975.641 557338.756 5936972.394 557339.627 5936968.536 557339.808 5936948.038 557339.07 5936940.522 557334.723 5936925.766 557333.18 5936912.819 557326.21 5936913.258 557325.854 5936911.596 557322.093 5936914.178 557318.283 5936916.733 557314.46 5936919.234 557310.6 5936921.698 557303.403 5936926.263 557291.454 5936933.297 557284.957 5936936.594 557281.666 5936938.219 557278.277 5936939.821 557273.326 5936942.061 557264.737 5936945.806 557256.016 5936949.325 557255.024 5936949.748 557254.369 5936950.105 557253.708 5936950.537 557253.172 5936950.948 557252.673 5936951.391 557252.304 5936951.762 557251.875 5936952.249 557251.493 5936952.746 557251.197 5936953.184 557250.89 5936953.7 557250.635 5936954.194 557250.405 5936954.713 557249.051 5936958.51 557248.816 5936959.205 557248.665 5936959.903 557248.596 5936960.519 557248.588 5936961.154 557248.628 5936961.678 557248.721 5936962.244 557248.791 5936962.546 557248.878 5936962.848 </gml:posList>
								</gml:LinearRing>
							</gml:interior>
						</gml:Polygon>
			</xplan:position>
			<xplan:flaechenschluss>false</xplan:flaechenschluss>
		</xplan:BP_FlaecheOhneFestsetzung>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_StrassenbegrenzungsLinie gml:id="GML_209d05c2-6a1c-4d7c-8139-9c7f71dfcc13">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557248.588 5936911.596</gml:lowerCorner>
					<gml:upperCorner>557339.808 5936982.349</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:position>
				<gml:Curve gml:id="GML_29318828-b4b9-4802-b6d4-51693eb3725b">
					<gml:segments>
						<gml:LineStringSegment>
							<gml:posList srsDimension="2" count="54">557248.791 5936962.546 557248.721 5936962.244 557248.628 5936961.678 557248.588 5936961.154 557248.596 5936960.519 557248.665 5936959.903 557248.816 5936959.205 557249.051 5936958.51 557250.405 5936954.713 557250.635 5936954.194 557250.89 5936953.7 557251.197 5936953.184 557251.493 5936952.746 557251.875 5936952.249 557252.304 5936951.762 557252.673 5936951.391 557253.172 5936950.948 557253.708 5936950.537 557254.369 5936950.105 557255.024 5936949.748 557256.016 5936949.325 557264.737 5936945.806 557273.326 5936942.061 557278.277 5936939.821 557281.666 5936938.219 557284.957 5936936.594 557291.454 5936933.297 557303.403 5936926.263 557310.6 5936921.698 557314.46 5936919.234 557318.283 5936916.733 557322.093 5936914.178 557325.854 5936911.596 557326.21 5936913.258 557333.18 5936912.819 557334.723 5936925.766 557339.07 5936940.522 557339.808 5936948.038 557339.627 5936968.536 557338.756 5936972.394 557336.499 5936975.641 557331.253 5936980.259 557327.65 5936981.872 557321.72 5936982.349 557315.876 5936981.17 557303.85 5936976.479 557301.814 5936975.865 557282.799 5936970.132 557282.027 5936969.9 557281.355 5936969.697 557273.435 5936969.011 557265.387 5936967.74 557248.878 5936962.848 557248.791 5936962.546 </gml:posList>
						</gml:LineStringSegment>
					</gml:segments>
				</gml:Curve>
			</xplan:position>
		</xplan:BP_StrassenbegrenzungsLinie>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_102950a6-e58f-492e-a576-a383c2902330">
			<xplan:schluessel>§2 Nr.16</xplan:schluessel>
			<xplan:text>Im reinen Wohngebiet sind in die Außenfassaden der Wohngebäude mindestens drei Fledermausspaltkästen mit Quartierseignung und zwei Niststeine für Halbhöhlenbrüter an fachlich geeigneter Stelle baulich zu integrieren und dauerhaft zu erhalten. In dem zu erhaltenden Baumbestand sind mindestens drei Nischenbrüterhöhlen an fachlich geeigneter Stelle anzubringen und dauerhaft zu unterhalten.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_15da3577-65bc-4d3e-b36d-69b3364d8027">
			<xplan:schluessel>§2 Nr.2</xplan:schluessel>
			<xplan:text>Im reinen Wohngebiet werden alle Ausnahmen nach § 3 Absatz 3 der Baunutzungsverordnung vom 21. November 2017 (BGBl. I S. 3787) ausgeschlossen.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_24d91abb-1f19-4a7d-8903-b1aea424e2c1">
			<xplan:schluessel>§2 Nr.14</xplan:schluessel>
			<xplan:text>Für festgesetzte Anpflanzungen von Bäumen und Sträuchern sowie für Ersatzpflanzungen sind standortgerechte einheimische Laubgehölze zu verwenden und dauerhaft zu erhalten. Großkronige Bäume müssen einen Stammumfang von mindestens 18 cm, kleinkronige Bäume einen Stammumfang von mindestens 16 cm, jeweils in 1 m Höhe über dem Erdboden gemessen, aufweisen. Im Kronenbereich jedes Baumes ist eine offene Vegetationsfläche von mindestens 12 m² anzulegen und zu begrünen.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_3c86cc25-e5d6-4672-b5f3-f433f0ade539">
			<xplan:schluessel>§2 Nr.4</xplan:schluessel>
			<xplan:text>Im Plangebiet ist durch geeignete bauliche Schallschutzmaßnahmen wie zum Beispiel Doppelfassaden, verglaste Vorbauten, Loggien, Wintergärten, besondere Fensterkon-struktionen oder in ihrer Wirkung vergleichbare Maßnahmen sicherzustellen, dass durch diese baulichen Maßnahmen insgesamt eine Schallpegeldifferenz erreicht wird, die es ermöglicht, dass in Schlafräumen ein Innenraumpegel bei teilgeöffneten Fenstern von 30 dB(A) während der Nachtzeit nicht überschritten wird. Erfolgt die bauliche Schall-schutzmaßnahme in Form von verglasten Vorbauten, muss dieser Innenraumpegel bei teilgeöffneten Bauteilen erreicht werden. Wohn-/Schlafräume in Einzimmerwohnungen und Kinderzimmer sind wie Schlafräume zu beurteilen.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_5ba5da80-515e-44f5-95dc-8f4270638840">
			<xplan:schluessel>§2 Nr.1</xplan:schluessel>
			<xplan:text>Im Bereich des Vorhaben- und Erschließungsplans (Vorhabengebiet) sind im reinen Wohngebiet im Rahmen der festgesetzten Nutzungen nur solche Vorhaben zulässig, zu deren Durchführung sich die Vorhabenträgerin im Durchführungsvertrag verpflichtet.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_5d50c47d-d8a4-40ad-93fb-b17bcc1b6849">
			<xplan:schluessel>§2 Nr.6</xplan:schluessel>
			<xplan:text>Geh- und Fahrwege sowie Terrassen- und Platzflächen sind in wasser- und luftdurchlässigem Aufbau herzustellen. Dabei sind nur Baustoffe zu verwenden, die keine auswaschbaren, wassergefährdenden Stoffe enthalten. Feuerwehrumfahrten und Feuerwehraufstellflächen sind in vegetationsfähigem Aufbau herzustellen.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_60722a9c-5b21-4eb7-983f-ffe65830a990">
			<xplan:schluessel>§2 Nr.8</xplan:schluessel>
			<xplan:text>Im reinen Wohngebiet sind Stellplätze nur innerhalb der dafür vorgesehenen Fläche zulässig.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_62103eff-5b29-4ba8-bc3f-517b910f8e53">
			<xplan:schluessel>§2 Nr.17</xplan:schluessel>
			<xplan:text>Im Vorhabengebiet sind zur Außenbeleuchtung nur Beleuchtungsanlagen zulässig, die ein für Insekten wirkungsarmes Lichtspektrum aufweisen. Die Lichtquellen sind zur Umgebung und zum Baumbestand hin abzuschirmen. </xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_684fb51c-3d0f-4993-8ff0-dc3b62686b88">
			<xplan:schluessel>§2 Nr.3</xplan:schluessel>
			<xplan:text>Im reinen Wohngebiet können Überschreitungen der Baugrenzen durch Vorbauten, Erker und Balkone um bis zu 1 m und durch Terrassen um bis zu 2,7 m zugelassen werden, wenn zum Erhalt oder zur Anpflanzung festgesetzte Einzelbäume nicht beeinträchtigt werden.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_6ffd7105-ba11-41bd-8ec5-c5c1bcc962c4">
			<xplan:schluessel>§2 Nr.15</xplan:schluessel>
			<xplan:text>Im reinen Wohngebiet ist je vier Stellplätze ein großkroniger Baum anzupflanzen. Stellplatzanlagen sind mit Hecken oder frei wachsenden Sträuchern einzufassen.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_7d7e15fb-c55d-47ce-9a16-dd72dfd39d6f">
			<xplan:schluessel>§2 Nr.18</xplan:schluessel>
			<xplan:text>Für Ausgleichsmaßnahmen des zu erwartenden Eingriffs in Natur und Landschaft wird der mit „Z“ bezeichneten Fläche die außerhalb des Bebauungsplangebietes in Wedel liegende Fläche des Flurstücks 7/31 der Gemarkung Wedel zugeordnet.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_8270cc97-0729-4664-bea1-6e4d70064d09">
			<xplan:schluessel>§2 Nr.12</xplan:schluessel>
			<xplan:text>Für die zu erhaltenden Bäume sind bei Abgang Ersatzpflanzungen mit großkronigen Bäumen vorzunehmen.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_8a5370e5-d396-4fb2-8b78-09bfcbb1ff4c">
			<xplan:schluessel>§2 Nr.10</xplan:schluessel>
			<xplan:text>Als an die öffentlichen Wege angrenzende Einfriedungen sind nur Hecken oder durchbrochene Zäune in Verbindung mit außenseitig zugeordneten Hecken zulässig. Notwendige Unterbrechungen für Zufahrten und Eingänge sind zulässig.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_91d49462-d233-47df-9ac1-f4d37c68af8f">
			<xplan:schluessel>§2 Nr.5</xplan:schluessel>
			<xplan:text>Entlang der mit „(A)“ gekennzeichneten Fassaden ist für den Außenbereich einer Wohnung entweder durch dessen Orientierung zur lärmabgewandten Gebäudeseite oder durch bauliche Schallschutzmaßnahmen, wie zum Beispiel verglaste Vorbauten, Loggien oder Wintergärten mit teilgeöffneten Bauteilen, sicherzustellen, dass durch diese baulichen Maßnahmen insgesamt eine Schallpegelminderung erreicht wird, die es ermöglicht, dass in dem der Wohnung zugehörigen Außenbereich ein Tagpegel von kleiner 65 dB(A) erreicht wird.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_a7e0ecd6-ba53-44a2-9103-9bf204c7119f">
			<xplan:schluessel>§2 Nr.13</xplan:schluessel>
			<xplan:text>Im Wurzelbereich der zu erhaltenden Bäume sind Nebenanlagen, Garagen und Stellplätze unzulässig.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_c2ed4d85-41ef-470d-a176-f3cdd45a7066">
			<xplan:schluessel>§2 Nr.11</xplan:schluessel>
			<xplan:text>Außerhalb von öffentlichen Straßenverkehrsflächen sind Geländeaufhöhungen und Abgrabungen im Kronenbereich zu erhaltender Bäume unzulässig.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_c6413128-a7f6-4420-86fa-5fceb7ce283b">
			<xplan:schluessel>§2 Nr.7</xplan:schluessel>
			<xplan:text>Das auf den privaten Grundstücksflächen anfallende Niederschlagswasser ist örtlich zu versickern, sofern es nicht gesammelt und genutzt wird.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextAbschnitt gml:id="GML_f145a635-83f9-4455-bf23-fadadbb78936">
			<xplan:schluessel>§2 Nr.9</xplan:schluessel>
			<xplan:text>Innerhalb der östlichen privaten Grünfläche mit der Zweckbestimmung „Garten“ ist die Anlage einer 1 m breiten Zuwegung in wasser- und luftdurchlässiger Bauweise zulässig.</xplan:text>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
		</xplan:BP_TextAbschnitt>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_377d43a1-8c65-4039-bd90-889d75a76b87">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557314.892 5936930.957</gml:lowerCorner>
					<gml:upperCorner>557329.391 5936970.109</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gliederung2>(A)</xplan:gliederung2>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:wirdDargestelltDurch xlink:href="#GML_b87933a3-7241-47d2-b1f3-ccceb7edeee5"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:refTextInhalt xlink:href="#GML_91d49462-d233-47df-9ac1-f4d37c68af8f"/>
			<xplan:position>
						<gml:Polygon srsName="EPSG:25832" gml:id="GML_e686d491-008b-4f68-a97a-82ddb5b41017">
							<gml:exterior>
								<gml:LinearRing>
									<gml:posList srsDimension="2" count="7">557329.391 5936968.426 557315.101 5936970.109 557314.892 5936967.95 557327.575 5936966.477 557323.539 5936931.207 557325.444 5936930.957 557329.391 5936968.426 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
			</xplan:position>
			<xplan:flaechenschluss>false</xplan:flaechenschluss>
		</xplan:BP_TextlicheFestsetzungsFlaeche>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7ef4531b-e85e-40e4-8c09-0e19a3ec5177">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557310.823 5936931.207</gml:lowerCorner>
					<gml:upperCorner>557327.575 5936967.95</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:ebene>0</xplan:ebene>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:wirdDargestelltDurch xlink:href="#GML_2dea9178-9249-4fd3-b4ba-ba48bfc27ecd"/>
			<xplan:wirdDargestelltDurch xlink:href="#GML_794de089-2788-4cb4-ab79-3ba5f7639710"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:refTextInhalt xlink:href="#GML_684fb51c-3d0f-4993-8ff0-dc3b62686b88"/>
			<xplan:refTextInhalt xlink:href="#GML_91d49462-d233-47df-9ac1-f4d37c68af8f"/>
			<xplan:position>
						<gml:Polygon srsName="EPSG:25832" gml:id="GML_9096f46b-8c9d-468d-82da-245800ea9048">
							<gml:exterior>
								<gml:LinearRing>
									<gml:posList srsDimension="2" count="5">557310.823 5936932.677 557323.539 5936931.207 557327.575 5936966.477 557314.892 5936967.95 557310.823 5936932.677 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
			</xplan:position>
			<xplan:flaechenschluss>false</xplan:flaechenschluss>
			<xplan:dachgestaltung>
				<xplan:BP_Dachgestaltung>
					<xplan:DNmin uom="grad">30</xplan:DNmin>
					<xplan:DNmax uom="grad">40</xplan:DNmax>
				</xplan:BP_Dachgestaltung>
			</xplan:dachgestaltung>
			<xplan:DNmin uom="grad">30</xplan:DNmin>
			<xplan:DNmax uom="grad">40</xplan:DNmax>
			<xplan:dachform>3100</xplan:dachform>
		</xplan:BP_UeberbaubareGrundstuecksFlaeche>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:BP_ZentralerVersorgungsbereich gml:id="GML_f553de4c-97e6-4f45-a5c2-a89886d18301">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557284.229 5936937.668</gml:lowerCorner>
					<gml:upperCorner>557299.272 5936959.488</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:ebene>0</xplan:ebene>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:wirdDargestelltDurch xlink:href="#GML_7087c593-4639-4fef-8b91-773ed64df8cf"/>
			<xplan:wirdDargestelltDurch xlink:href="#GML_c47097ae-79f6-4001-841b-da9b80979252"/>
			<xplan:rechtscharakter>1000</xplan:rechtscharakter>
			<xplan:refTextInhalt xlink:href="#GML_684fb51c-3d0f-4993-8ff0-dc3b62686b88"/>
			<xplan:position>
						<gml:Polygon srsName="EPSG:25832" gml:id="GML_4558cd4f-f4c9-4aaf-a7ee-5a4d17a194d8">
							<gml:exterior>
								<gml:LinearRing>
									<gml:posList srsDimension="2" count="5">557284.229 5936939.12 557296.947 5936937.668 557299.272 5936958.036 557286.554 5936959.488 557284.229 5936939.12 </gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:Polygon>
			</xplan:position>
			<xplan:flaechenschluss>false</xplan:flaechenschluss>
		</xplan:BP_ZentralerVersorgungsbereich>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_LPO gml:id="GML_042e6f7a-e1c3-4569-b5a5-de1b996e5ba7">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557325.444 5936930.957</gml:lowerCorner>
					<gml:upperCorner>557327.235 5936948.315</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:position>
				<gml:Curve gml:id="GML_ab02ea3f-46b8-446c-96ce-6f238fb4bf45">
					<gml:segments>
						<gml:LineStringSegment>
							<gml:posList srsDimension="2" count="2">557327.235 5936948.315 557325.444 5936930.957 </gml:posList>
						</gml:LineStringSegment>
					</gml:segments>
				</gml:Curve>
			</xplan:position>
		</xplan:XP_LPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_LPO gml:id="GML_730b4676-8840-4cbd-a2e0-7e351cc61965">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557315.101 5936954.898</gml:lowerCorner>
					<gml:upperCorner>557329.325 5936970.109</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:position>
				<gml:Curve gml:id="GML_c58dbb0e-d49c-4b7d-87f7-65f9ca8e159e">
					<gml:segments>
						<gml:LineStringSegment>
							<gml:posList srsDimension="2" count="3">557327.869 5936954.898 557329.325 5936968.52 557315.101 5936970.109 </gml:posList>
						</gml:LineStringSegment>
					</gml:segments>
				</gml:Curve>
			</xplan:position>
		</xplan:XP_LPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_PPO gml:id="GML_09ae3866-c6fc-427c-baf1-2c88200f54b6">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557304.757 5936953.719</gml:lowerCorner>
					<gml:upperCorner>557304.757 5936953.719</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:art>besondereArtDerBaulNutzung</xplan:art>
			<xplan:index>0</xplan:index>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:dientZurDarstellungVon xlink:href="#GML_c3e0bb6a-9e1d-4461-8995-fe1eebf5bfcd"/>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_c85738e0-f404-46f4-b786-20f8ee499074">
					<gml:pos>557304.757 5936953.719</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
			<xplan:skalierung>1</xplan:skalierung>
		</xplan:XP_PPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_PPO gml:id="GML_0c35b24f-6cf5-4e98-9460-a7b2f4e37766">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557302.026 5936942.476</gml:lowerCorner>
					<gml:upperCorner>557302.026 5936942.476</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:art>GFZ</xplan:art>
			<xplan:index>0</xplan:index>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:dientZurDarstellungVon xlink:href="#GML_c3e0bb6a-9e1d-4461-8995-fe1eebf5bfcd"/>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_a89cf332-aaae-48bd-a4df-5b4983491d62">
					<gml:pos>557302.026 5936942.476</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
			<xplan:skalierung>0.787402</xplan:skalierung>
		</xplan:XP_PPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_PPO gml:id="GML_2dea9178-9249-4fd3-b4ba-ba48bfc27ecd">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557317.639 5936959.231</gml:lowerCorner>
					<gml:upperCorner>557317.639 5936959.231</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:art>dachform</xplan:art>
			<xplan:index>0</xplan:index>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:dientZurDarstellungVon xlink:href="#GML_7ef4531b-e85e-40e4-8c09-0e19a3ec5177"/>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_b7be627e-67bb-4015-8398-b73bcdf8b0d9">
					<gml:pos>557317.639 5936959.231</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
			<xplan:skalierung>1</xplan:skalierung>
		</xplan:XP_PPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_PPO gml:id="GML_498bba9c-faec-46f2-a7d3-17a576053da7">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557304.756 5936953.719</gml:lowerCorner>
					<gml:upperCorner>557304.756 5936953.719</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:dientZurDarstellungVon xlink:href="#GML_c3e0bb6a-9e1d-4461-8995-fe1eebf5bfcd"/>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_8dc51aa1-bd42-4fab-ab39-716dd90f3cd2">
					<gml:pos>557304.756 5936953.719</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
			<xplan:skalierung>1</xplan:skalierung>
		</xplan:XP_PPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_PPO gml:id="GML_5e033921-530a-47b1-84e3-a508f45c2d51">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557335.524 5936929.288</gml:lowerCorner>
					<gml:upperCorner>557335.524 5936929.288</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:art>nutzungsform</xplan:art>
			<xplan:art>text</xplan:art>
			<xplan:index>0</xplan:index>
			<xplan:index>0</xplan:index>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:dientZurDarstellungVon xlink:href="#GML_382429ee-f25f-4d00-b826-8e804ddbdebc"/>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_93b52be8-d6cb-4f14-8830-c286e9683e96">
					<gml:pos>557335.524 5936929.288</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:drehwinkel uom="grad">84</xplan:drehwinkel>
			<xplan:skalierung>0.5</xplan:skalierung>
		</xplan:XP_PPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_PPO gml:id="GML_7087c593-4639-4fef-8b91-773ed64df8cf">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557289.469 5936953.621</gml:lowerCorner>
					<gml:upperCorner>557289.469 5936953.621</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:art>dachform</xplan:art>
			<xplan:index>0</xplan:index>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:dientZurDarstellungVon xlink:href="#GML_f553de4c-97e6-4f45-a5c2-a89886d18301"/>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_63d19af6-38d6-4b9c-beb7-176495f2afc1">
					<gml:pos>557289.469 5936953.621</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
			<xplan:skalierung>1</xplan:skalierung>
		</xplan:XP_PPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_PPO gml:id="GML_794de089-2788-4cb4-ab79-3ba5f7639710">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557314.121 5936936.501</gml:lowerCorner>
					<gml:upperCorner>557314.121 5936936.501</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:art>DNmax</xplan:art>
			<xplan:art>DNmin</xplan:art>
			<xplan:index>0</xplan:index>
			<xplan:index>0</xplan:index>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:dientZurDarstellungVon xlink:href="#GML_7ef4531b-e85e-40e4-8c09-0e19a3ec5177"/>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_b27df56e-52bf-496c-9e20-fb9945775513">
					<gml:pos>557314.121 5936936.501</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
			<xplan:skalierung>1</xplan:skalierung>
		</xplan:XP_PPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_PPO gml:id="GML_8b8ff5aa-cafb-4740-a12a-adc5ee16856b">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557304.528 5936946.56</gml:lowerCorner>
					<gml:upperCorner>557304.528 5936946.56</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:art>Z</xplan:art>
			<xplan:index>0</xplan:index>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:dientZurDarstellungVon xlink:href="#GML_c3e0bb6a-9e1d-4461-8995-fe1eebf5bfcd"/>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_e9ae89a6-f418-4f08-b93a-ad486f36fef9">
					<gml:pos>557304.528 5936946.56</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
			<xplan:skalierung>0.7</xplan:skalierung>
		</xplan:XP_PPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_PPO gml:id="GML_9fa3a84d-a7ed-4e6d-b75e-8611dbbe3097">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557278.714 5936951.35</gml:lowerCorner>
					<gml:upperCorner>557278.714 5936951.35</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:art>gliederung2</xplan:art>
			<xplan:index>0</xplan:index>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:dientZurDarstellungVon xlink:href="#GML_435c123d-74af-4da3-9b08-2230d60f4afc"/>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_79e57a00-7692-48ba-934b-0bf8ab089cbb">
					<gml:pos>557278.714 5936951.35</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
			<xplan:skalierung>1</xplan:skalierung>
		</xplan:XP_PPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_PPO gml:id="GML_a39fac97-534a-4266-9e20-b25c1008a015">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557268.287 5936956.688</gml:lowerCorner>
					<gml:upperCorner>557268.287 5936956.688</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:art>nutzungsform</xplan:art>
			<xplan:art>text</xplan:art>
			<xplan:index>0</xplan:index>
			<xplan:index>0</xplan:index>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:dientZurDarstellungVon xlink:href="#GML_5824d0ca-ea90-441f-830c-c070f1a0aa05"/>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_bdbc69d0-1397-4b26-9606-91288b9c1668">
					<gml:pos>557268.287 5936956.688</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:drehwinkel uom="grad">18</xplan:drehwinkel>
			<xplan:skalierung>0.5</xplan:skalierung>
		</xplan:XP_PPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_PPO gml:id="GML_ab6dc9c7-febf-4465-b4c6-fc60838ed212">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557302.835 5936966.026</gml:lowerCorner>
					<gml:upperCorner>557302.835 5936966.026</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:art>zweckbestimmung</xplan:art>
			<xplan:index>0</xplan:index>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:dientZurDarstellungVon xlink:href="#GML_09d5413d-ccd7-40a1-8298-80bb22850dd3"/>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_d5a3bf6b-4105-4bcd-91cf-be31c86f3dc6">
					<gml:pos>557302.835 5936966.026</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
			<xplan:skalierung>1</xplan:skalierung>
		</xplan:XP_PPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_PPO gml:id="GML_b87933a3-7241-47d2-b1f3-ccceb7edeee5">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557308.648 5936953.111</gml:lowerCorner>
					<gml:upperCorner>557308.648 5936953.111</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:art>gliederung2</xplan:art>
			<xplan:index>0</xplan:index>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:dientZurDarstellungVon xlink:href="#GML_377d43a1-8c65-4039-bd90-889d75a76b87"/>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_443b40ab-c168-42c3-88b6-ed2eeabae2e5">
					<gml:pos>557308.648 5936953.111</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:drehwinkel uom="grad">263.32</xplan:drehwinkel>
			<xplan:skalierung>1</xplan:skalierung>
		</xplan:XP_PPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_PPO gml:id="GML_c47097ae-79f6-4001-841b-da9b80979252">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557288.157 5936947.063</gml:lowerCorner>
					<gml:upperCorner>557288.157 5936947.063</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:art>DNmax</xplan:art>
			<xplan:art>DNmin</xplan:art>
			<xplan:index>0</xplan:index>
			<xplan:index>0</xplan:index>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:dientZurDarstellungVon xlink:href="#GML_f553de4c-97e6-4f45-a5c2-a89886d18301"/>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_11bbe558-0ee5-4908-8d43-35074891470d">
					<gml:pos>557288.157 5936947.063</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
			<xplan:skalierung>1</xplan:skalierung>
		</xplan:XP_PPO>
	</gml:featureMember>
	<gml:featureMember>
		<xplan:XP_PPO gml:id="GML_ed1db422-4904-4271-a048-1832df70c070">
			<gml:boundedBy>
				<gml:Envelope srsName="EPSG:25832">
					<gml:lowerCorner>557301.588 5936937.333</gml:lowerCorner>
					<gml:upperCorner>557301.588 5936937.333</gml:upperCorner>
				</gml:Envelope>
			</gml:boundedBy>
			<xplan:art>hoehenangabe</xplan:art>
			<xplan:index>0</xplan:index>
			<xplan:gehoertZuBereich xlink:href="#GML_717e6e84-7220-440d-8dee-9eaa9d1b23c8"/>
			<xplan:dientZurDarstellungVon xlink:href="#GML_c3e0bb6a-9e1d-4461-8995-fe1eebf5bfcd"/>
			<xplan:position>
				<gml:Point srsName="EPSG:25832" gml:id="GML_fec6ac20-6586-42c0-8532-4a4f87023554">
					<gml:pos>557301.588 5936937.333</gml:pos>
				</gml:Point>
			</xplan:position>
			<xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
			<xplan:skalierung>1</xplan:skalierung>
		</xplan:XP_PPO>
	</gml:featureMember>	
</xplan:XPlanAuszug>
