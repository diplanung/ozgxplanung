﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_059C7CD6-017A-4613-8CE2-0F93A45F5DF6" xsi:schemaLocation="http://www.xplanung.de/xplangml/6/0 http://repository.gdi-de.org/schemas/de.xleitstelle.xplanung/6.0/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/6/0">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>557082.896 5936688.969</gml:lowerCorner>
      <gml:upperCorner>557332.925 5936947.632</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="Gml_66C1275A-5865-4A51-B4F4-5FD57E52B5A6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557082.896 5936688.969</gml:lowerCorner>
          <gml:upperCorner>557332.925 5936947.632</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan_Wirksamkeit_6-0-2</xplan:name>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_829AAF53-955A-410B-AB7C-F1D93EB7472D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C6659AF8-ED52-4D2F-87C9-38B3EE7AC038" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557094.392 5936696.867 557101.914 5936688.969 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7CCE542E-9D96-4DE9-955D-7387204B2552" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557101.914 5936688.969 557129.793 5936689.258 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E219DC87-C9B1-44E2-ACA3-2EA80F5A52D4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557129.793 5936689.258 557151.675 5936689.487 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_29765FAF-7B24-4C03-970F-2903A95ECDF7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557151.675 5936689.487 557172.06 5936689.699 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_40B0D991-E73C-412B-A252-48B0ABFA5DD7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557172.06 5936689.699 557186.448 5936689.848 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4162B7C2-6A52-4E13-9EA2-48D468429405" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557186.448 5936689.848 557201.437 5936690.005 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_97B04468-5FE8-4E2F-BAF2-B55D7694A365" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557201.437 5936690.005 557218.624 5936690.183 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CC6B70A9-237D-4839-8FB5-89CCB4089813" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557218.624 5936690.183 557230.716 5936690.308 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_14600E2D-FAB5-49CC-9736-60FC50D8F8D0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557230.716 5936690.308 557232.757 5936690.46 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FEE1658A-4EE1-40F9-916F-4E0C9014C766" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557232.757 5936690.46 557246.445 5936691.482 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_68A98829-C282-4F8F-80ED-50817DDE5406" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557246.445 5936691.482 557260.981 5936692.567 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A8FB2873-CC26-4248-87FE-6A8C49315E4C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557260.981 5936692.567 557276.423 5936693.718 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C3E74AD6-DFF8-4C7B-923B-51E0238F4FF6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557276.423 5936693.718 557316.405 5936696.699 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B27E2938-4E36-478A-B60B-AC9348E4CBA2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557316.405 5936696.699 557319.183 5936697.419 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7ECF6D69-FFBC-4794-B51D-20BF17B8F57B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557319.183 5936697.419 557321.606 5936699.002 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E84E70E6-E278-454C-976D-D24A58F60847" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557321.606 5936699.002 557323.248 5936701.408 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C1037844-15D6-4777-ADF8-C12C16467880" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557323.248 5936701.408 557323.977 5936704.207 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_21AE8199-F0AB-43E4-82E2-5A8C48005504" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557323.977 5936704.207 557324.872 5936715.209 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CD9DCEC6-917C-4EAC-ADBA-B60AF3D546CE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557324.872 5936715.209 557323.103 5936715.025 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A2E99E27-0860-4498-8C2D-AC1E1483D377" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557323.103 5936715.025 557324.48 5936731.963 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4D60E116-B091-4DA0-9B2F-43326C0E924E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557324.48 5936731.963 557326.254 5936732.199 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_08DF09AD-D7AB-4E5A-8C2B-49CD1C4A3825" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557326.254 5936732.199 557327.353 5936749.254 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0ECDCC78-CE5B-4E65-B683-5BF3894B8DA2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557327.353 5936749.254 557328.457 5936766.407 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_844F0B38-084E-4804-87C4-CB4F0EEF101D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557328.457 5936766.407 557328.652 5936769.424 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_16447DD4-ECD0-49E0-B16E-396C889B274B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557328.652 5936769.424 557329.558 5936783.484 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FC47D396-7BC4-4B4A-86F7-41ECD6324CBE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557329.558 5936783.484 557329.705 5936787.686 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_46DAAE1B-0AE9-482D-884A-75725F2F6B62" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557329.705 5936787.686 557330.163 5936800.699 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BAD95531-B8B3-4932-A611-327C9DAA8812" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557330.163 5936800.699 557330.701 5936816.037 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_458E2C18-870F-4F70-BA2A-9365E2B34C9E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557330.701 5936816.037 557331.241 5936831.464 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CE80D54F-1A1B-4034-A440-1162F1EE07A3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557331.241 5936831.464 557331.621 5936842.299 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_49CCD040-45B8-4D3A-9DE1-A4FB742E3927" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557331.621 5936842.299 557331.858 5936849.068 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DE062C66-4ACC-420E-B106-E8DB7A291344" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557331.858 5936849.068 557332.205 5936858.941 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_226F781D-3487-46DA-8457-82A334A82F75" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557332.205 5936858.941 557332.312 5936861.98 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_012291C9-3B25-40E6-9891-48A8E91973B9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557332.312 5936861.98 557332.925 5936879.46 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D3205136-65A3-4CB8-B7F6-0FA3C0561B7D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557332.925 5936879.46 557332.236 5936886.429 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_98E034D5-119F-433C-ACE6-4FA05122E318" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557332.236 5936886.429 557329.738 5936892.991 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E520BE4B-B24E-459E-BFBE-CD0D70E68C42" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557329.738 5936892.991 557325.621 5936898.652 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1C1A2EC7-E369-4527-A1BC-5A4CF48C7590" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557325.621 5936898.652 557320.152 5936903.06 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_22697FAB-ADEB-4896-97C8-83DCB6FF5190" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557320.152 5936903.06 557312.969 5936907.425 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4F6A1325-9594-4165-88FC-085414B53A67" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557312.969 5936907.425 557289.035 5936921.965 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_50A22172-EC27-435D-99E1-CC3AE685B37E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557289.035 5936921.965 557285.079 5936924.369 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F781C2A1-B911-416B-A5E3-AE5B0811A7EB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557285.079 5936924.369 557266.601 5936932.317 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F2CD4790-26C8-4530-BFC2-068579DF7D9A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557266.601 5936932.317 557248.134 5936940.262 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_49657DF6-69D2-4288-90CF-DFF090F907AB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557248.134 5936940.262 557238.684 5936943.809 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B9777916-DA06-4CD1-A36A-E9EC7FD10FB0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557238.684 5936943.809 557233.495 5936945.128 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9CE74F16-D230-4602-AC1B-57A676E2B0AB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557233.495 5936945.128 557228.908 5936946.294 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_41BB409F-B32B-4C24-A68E-5F94E82FFC33" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557228.908 5936946.294 557218.884 5936947.544 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4D4C46DC-8DDC-4D46-AA5B-831C4CCBA414" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557218.884 5936947.544 557208.848 5936947.632 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D3432D03-AABB-48FF-8AD3-535ECA33E780" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557208.848 5936947.632 557181.21 5936936.938 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A8CF5192-E208-40C3-9F8B-59B398832A14" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557181.21 5936936.938 557169.523 5936932.518 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C4C361DC-3789-4FF6-A4C2-490B466C8FC3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557169.523 5936932.518 557167.711 5936931.674 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_948D19BE-A950-4F9F-AC79-508B50C9BB7D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557167.711 5936931.674 557117.719 5936908.398 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_32C20D27-A308-4814-9485-960E15BA44C3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557117.719 5936908.398 557105.291 5936902.612 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A1AED362-87E5-41FB-B321-04FC55A64B7E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557105.291 5936902.612 557085.808 5936893.54 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2150F0EA-430C-4089-B752-494129B8A865" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557085.808 5936893.54 557082.896 5936887.295 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_73686A71-D76D-44B9-BA5A-01A518790D01" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557082.896 5936887.295 557089.988 5936860.845 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A41E9794-A72E-43D2-80D0-07B0239ADD09" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557089.988 5936860.845 557092.284 5936850.813 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_07FD4F82-913B-44A6-9461-69C3ACC842D1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557092.284 5936850.813 557093.792 5936843.489 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B807E441-48C4-4F38-B5C0-1FC039205BE1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557093.792 5936843.489 557095.881 5936832.656 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3711B654-EDE3-4C9D-A01F-B3939AEEE31D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557095.881 5936832.656 557096.392 5936828.69 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_459DD8C3-5E81-4159-9AB2-4C0EDDA39BE5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557096.392 5936828.69 557097.492 5936820.181 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E991F01C-7EF9-4E25-9A89-C05C96A1F4F1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557097.492 5936820.181 557098.17 5936811.708 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0708D5F6-F357-4B03-9A55-EA5278B2921A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557098.17 5936811.708 557098.318 5936809.862 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0002927E-437D-4789-9F90-128CE7622569" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557098.318 5936809.862 557098.435 5936807.666 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_3BC337CB-89F8-42EA-8EE4-1621DEDC0B0A" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">557098.435 5936807.666 557098.924968697 5936798.92075357 557099.162 5936790.165 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_1C6A2F26-1D2C-4D9A-8444-06F2902EB916" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">557099.162 5936790.165 557099.101000379 5936779.42601613 557098.658 5936768.696 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B501E0D4-A77C-4B28-BFC4-8B3FB93C14C3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557098.658 5936768.696 557097.449 5936748.336 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0184EE3C-B35B-4116-A6D0-03C99B7E3F92" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557097.449 5936748.336 557096.532 5936732.911 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3F126A4C-C464-4EA1-AC89-B802AB2D8128" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557096.532 5936732.911 557096.286 5936728.762 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4759FBF5-0FF3-4F60-A351-593842FCE5FD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557096.286 5936728.762 557095.273 5936711.711 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D930B946-069A-4AF8-BFD7-D7BC3947A928" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557095.273 5936711.711 557094.392 5936696.867 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:bereich xlink:href="#Gml_612F9C72-83DD-4220-98A7-7AB487797D85" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="Gml_612F9C72-83DD-4220-98A7-7AB487797D85">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557082.896 5936688.969</gml:lowerCorner>
          <gml:upperCorner>557332.925 5936947.632</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_F24317DB-5C25-46EC-8772-A7423B655F38" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1B27F5CB-652A-4FFB-81E0-577FD8260F1A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557094.392 5936696.867 557101.914 5936688.969 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5CFC80AA-7ED8-4F3B-8F23-B15C07F9530D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557101.914 5936688.969 557129.793 5936689.258 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7C2DAF4B-3BCC-4667-A91C-1164D7202104" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557129.793 5936689.258 557151.675 5936689.487 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1153F531-01E7-4883-B31D-F626CF556F79" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557151.675 5936689.487 557172.06 5936689.699 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_23A31DA0-5E09-4882-BF16-3A2BC47A97AD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557172.06 5936689.699 557186.448 5936689.848 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BD3ACBD6-3155-4C3C-B769-9E035F0FFAC3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557186.448 5936689.848 557201.437 5936690.005 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D047DC69-812E-4241-997C-6A7F7D7768DA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557201.437 5936690.005 557218.624 5936690.183 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1E18EB62-06DA-4638-98AB-FD7952384EF1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557218.624 5936690.183 557230.716 5936690.308 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7BA32F35-8BE7-4247-BECB-E2D894CC4D30" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557230.716 5936690.308 557232.757 5936690.46 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_96648824-EC3B-40AB-90CF-808C6EA4074F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557232.757 5936690.46 557246.445 5936691.482 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_20E08B23-E8FB-4DEA-BAB6-2AB09991657E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557246.445 5936691.482 557260.981 5936692.567 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_635DA561-663A-403E-95A1-32523ED90DAE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557260.981 5936692.567 557276.423 5936693.718 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EBD705F7-4F24-473A-85E8-03359236A0DB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557276.423 5936693.718 557316.405 5936696.699 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4AFD0E40-C405-4B0C-B08F-2E7D58C9669B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557316.405 5936696.699 557319.183 5936697.419 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C256432A-0F7D-47B0-AB35-775CB2C89CFA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557319.183 5936697.419 557321.606 5936699.002 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_85E29D47-B3FA-42D0-9B33-35F43E09B34A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557321.606 5936699.002 557323.248 5936701.408 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C4233E26-EF89-44B3-A589-10A6C5BA63E8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557323.248 5936701.408 557323.977 5936704.207 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_391A2B2B-E41C-4BD5-9490-304B1C71F464" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557323.977 5936704.207 557324.872 5936715.209 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DB678976-5659-4B8F-BB8D-32B4B60FD2D6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557324.872 5936715.209 557323.103 5936715.025 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_96E256F6-2004-4CB6-8F9C-CA5795E04E3A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557323.103 5936715.025 557324.48 5936731.963 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_850E5E59-2551-4854-98B5-6568EDB9351C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557324.48 5936731.963 557326.254 5936732.199 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E55DFCF5-A6E8-4E37-9501-701B86BB6B2C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557326.254 5936732.199 557327.353 5936749.254 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_AAEE7A87-1C3D-461F-92DE-703A8A26BAC0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557327.353 5936749.254 557328.457 5936766.407 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D454BD37-2F53-4636-AA97-07E82BB05EFA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557328.457 5936766.407 557328.652 5936769.424 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B51B651E-35EA-4164-8D40-19A9DE459873" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557328.652 5936769.424 557329.558 5936783.484 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DEA8D0BC-3960-4E8D-B3A3-9AE7956D5212" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557329.558 5936783.484 557329.705 5936787.686 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_726E8BD7-A2C9-4EE8-883D-56D85207743E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557329.705 5936787.686 557330.163 5936800.699 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_54714B81-4A0D-406E-899B-1BB33D8AF828" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557330.163 5936800.699 557330.701 5936816.037 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4FBB1047-4DC7-4752-BFDA-8786660CAFDA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557330.701 5936816.037 557331.241 5936831.464 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_68F15CEF-71E5-4127-81A9-242F31837A9A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557331.241 5936831.464 557331.621 5936842.299 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_92D803E0-F101-49BF-9142-967BFACCC895" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557331.621 5936842.299 557331.858 5936849.068 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7DCDB041-7419-4B49-BF25-5BC8B9D43DB7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557331.858 5936849.068 557332.205 5936858.941 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EE7D895A-4896-445B-AF6A-7678CCECA479" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557332.205 5936858.941 557332.312 5936861.98 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A48765AE-EF6B-478E-A1B5-4F9604A5A5C9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557332.312 5936861.98 557332.925 5936879.46 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E3D68021-6777-4E94-B5A8-51E02BC8EFC3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557332.925 5936879.46 557332.236 5936886.429 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_622B6D87-259A-463A-AAE9-9D8919796B51" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557332.236 5936886.429 557329.738 5936892.991 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_AE6D80B1-8FFF-48BE-8A90-BCBA69BFF0FE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557329.738 5936892.991 557325.621 5936898.652 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_849CA54B-6710-44E9-8818-8CF2401B7696" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557325.621 5936898.652 557320.152 5936903.06 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_79AF3DDA-7839-4EAC-9B35-64894A0BED5C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557320.152 5936903.06 557312.969 5936907.425 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5A2CC436-A214-4A51-AB85-CB497C788572" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557312.969 5936907.425 557289.035 5936921.965 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8306AE62-B38B-4A4F-B540-C973CE913408" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557289.035 5936921.965 557285.079 5936924.369 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4554C32A-3E81-4146-810D-8CAA103975A4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557285.079 5936924.369 557266.601 5936932.317 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_58F58823-A3F3-4EA9-AB3F-4AD6AD7A4170" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557266.601 5936932.317 557248.134 5936940.262 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_28EA0DB0-049E-4FC0-BE2A-D8A737E3A459" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557248.134 5936940.262 557238.684 5936943.809 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CE3A663B-8F30-40A5-8E07-6C9FB8B9C214" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557238.684 5936943.809 557233.495 5936945.128 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_34BA2680-E5B2-4EA9-8BA7-E9BD42F625AF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557233.495 5936945.128 557228.908 5936946.294 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C928E0E7-A847-42EB-8999-7B524EA93A6D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557228.908 5936946.294 557218.884 5936947.544 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_30615172-9E88-4CBA-A204-E2E7C0F22AFB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557218.884 5936947.544 557208.848 5936947.632 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0B5375C2-B6CD-4F79-92B9-4EE17FE7BACD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557208.848 5936947.632 557181.21 5936936.938 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_48D6F0F6-4D57-49D7-A1F7-DF5193F37AB1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557181.21 5936936.938 557169.523 5936932.518 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F5CB5557-6FB5-4DC8-B005-620F69FA4574" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557169.523 5936932.518 557167.711 5936931.674 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BECF443C-647D-41F6-9C13-D8ABB0CBD94E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557167.711 5936931.674 557117.719 5936908.398 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DA7BD604-C8FF-4174-B64F-FA0E7C732678" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557117.719 5936908.398 557105.291 5936902.612 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_40E71A2A-0FF9-4757-A204-B4FF0C60D0A9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557105.291 5936902.612 557085.808 5936893.54 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C5A27D88-4429-4D9D-A62A-5209901A7ABA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557085.808 5936893.54 557082.896 5936887.295 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5A243DF1-6801-4BD2-A07D-1EA6FA577BD2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557082.896 5936887.295 557089.988 5936860.845 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BB99EF41-7F65-40F3-888B-8B6715AF314F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557089.988 5936860.845 557092.284 5936850.813 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BE302783-C832-4C55-BA18-8E328DEE7A3F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557092.284 5936850.813 557093.792 5936843.489 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4B9678CB-8D2B-4310-86C1-2B2D9D662F9C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557093.792 5936843.489 557095.881 5936832.656 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B39FA8A5-65AE-4DB7-8FBD-B2F5C3E5D787" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557095.881 5936832.656 557096.392 5936828.69 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_426A6F01-7DB0-4B36-82BA-775749B09BE6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557096.392 5936828.69 557097.492 5936820.181 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F5521837-9690-4C1B-9260-B557BD2F2CED" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557097.492 5936820.181 557098.17 5936811.708 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_64817697-9F81-49DC-B449-253F274C0BA2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557098.17 5936811.708 557098.318 5936809.862 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_22AD1611-E54C-4026-9E21-9DB30045314A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557098.318 5936809.862 557098.435 5936807.666 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_3D2A1326-950B-410C-B758-0D6AAF2D9487" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">557098.435 5936807.666 557098.924968697 5936798.92075357 557099.162 5936790.165 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_BD9C3329-4518-4009-8482-7905527DD361" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">557099.162 5936790.165 557099.101000379 5936779.42601613 557098.658 5936768.696 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1C25546A-3157-493C-ADA6-E9A32D584C7D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557098.658 5936768.696 557097.449 5936748.336 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ED542EA4-6D73-4E29-9207-B74BE110A1B1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557097.449 5936748.336 557096.532 5936732.911 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7DF3BB15-D3BA-4F0C-A6A2-CDA37127959D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557096.532 5936732.911 557096.286 5936728.762 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9E4BC538-C2E2-4BB7-A957-42C03E57231F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557096.286 5936728.762 557095.273 5936711.711 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1C9669EB-9740-4901-8EB0-47DA4959E315" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557095.273 5936711.711 557094.392 5936696.867 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#Gml_D3E78336-D47F-412E-A05D-F06AD94EA261" />
      <xplan:planinhalt xlink:href="#Gml_81890FA3-6BEC-43B8-A7DF-22B9A254D071" />
      <xplan:planinhalt xlink:href="#Gml_C01842C9-4E58-4E47-B46A-E07B43242BAA" />
      <xplan:planinhalt xlink:href="#Gml_AFD405CF-5867-4986-83AB-8CF87885B91C" />
      <xplan:planinhalt xlink:href="#Gml_6EAE5CEE-79A3-4CB2-8DF3-1D81DB9150B1" />
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:gehoertZuPlan xlink:href="#Gml_66C1275A-5865-4A51-B4F4-5FD57E52B5A6" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_D3E78336-D47F-412E-A05D-F06AD94EA261">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557094.392 5936688.969</gml:lowerCorner>
          <gml:upperCorner>557172.06 5936751.42</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_612F9C72-83DD-4220-98A7-7AB487797D85" />
      <xplan:endeBedingung>
        <xplan:XP_WirksamkeitBedingung>
          <xplan:datumAbsolut>2023-03-16</xplan:datumAbsolut>          
        </xplan:XP_WirksamkeitBedingung>
      </xplan:endeBedingung>
      <xplan:rechtscharakter>7000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_F6539E17-C205-42A1-9F5D-ED7863F4A041" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">557101.914 5936688.969 557129.793 5936689.258 557151.675 5936689.487 
557172.06 5936689.699 557171.27 5936731.109 557170.883 5936751.42 
557158.439 5936749.428 557149.835 5936747.132 557128.379 5936741.407 
557101.084 5936734.124 557096.286 5936728.762 557095.273 5936711.711 
557094.392 5936696.867 557101.914 5936688.969 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>
        <xplan:BP_KomplexeZweckbestGruen>
          <xplan:allgemein>14000</xplan:allgemein>
        </xplan:BP_KomplexeZweckbestGruen>
      </xplan:zweckbestimmung>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_81890FA3-6BEC-43B8-A7DF-22B9A254D071">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557170.883 5936689.699</gml:lowerCorner>
          <gml:upperCorner>557329.558 5936783.484</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_612F9C72-83DD-4220-98A7-7AB487797D85" />
      <xplan:endeBedingung>
        <xplan:XP_WirksamkeitBedingung>
          <xplan:datumAbsolut>2023-03-16</xplan:datumAbsolut>
        </xplan:XP_WirksamkeitBedingung>
      </xplan:endeBedingung>
      <xplan:rechtscharakter>7000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_52B65AB5-A56E-4043-8D20-FE4FA4ED83AD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">557172.06 5936689.699 557186.448 5936689.848 557201.437 5936690.005 
557214.0841 5936690.136 557218.624 5936690.183 557230.716 5936690.308 
557232.757 5936690.46 557246.445 5936691.482 557260.981 5936692.567 
557276.423 5936693.718 557303.6472 5936695.7478 557316.405 5936696.699 
557319.183 5936697.419 557321.606 5936699.002 557323.248 5936701.408 
557323.977 5936704.207 557324.872 5936715.209 557323.103 5936715.025 
557324.48 5936731.963 557326.254 5936732.199 557327.353 5936749.254 
557328.457 5936766.407 557328.652 5936769.424 557329.558 5936783.484 
557306.729 5936778.227 557303.6472 5936777.5174 557270.594 5936769.907 
557265.672 5936768.773 557255.24 5936766.708 557241.825 5936764.055 
557237.198 5936763.14 557230.531 5936761.821 557217.971 5936759.337 
557217.151 5936759.174 557207.962 5936757.358 557200.149 5936756.106 
557190.7757 5936754.605 557185.205 5936753.713 557170.883 5936751.42 
557171.27 5936731.109 557172.06 5936689.699 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>
        <xplan:BP_KomplexeZweckbestGruen>
          <xplan:allgemein>9999</xplan:allgemein>
        </xplan:BP_KomplexeZweckbestGruen>
      </xplan:zweckbestimmung>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_LandwirtschaftsFlaeche gml:id="Gml_C01842C9-4E58-4E47-B46A-E07B43242BAA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557094.392 5936688.969</gml:lowerCorner>
          <gml:upperCorner>557329.558 5936783.484</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_612F9C72-83DD-4220-98A7-7AB487797D85" />
      <xplan:startBedingung>
        <xplan:XP_WirksamkeitBedingung>
          <xplan:datumAbsolut>2023-03-17</xplan:datumAbsolut>
        </xplan:XP_WirksamkeitBedingung>
      </xplan:startBedingung>
      <xplan:rechtscharakter>7000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_BB85A0D4-7665-4608-9AC7-A9C83C3A00B7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">557101.084 5936734.124 557096.286 5936728.762 557095.273 5936711.711 
557094.392 5936696.867 557101.914 5936688.969 557129.793 5936689.258 
557151.675 5936689.487 557172.06 5936689.699 557186.448 5936689.848 
557201.437 5936690.005 557214.0841 5936690.136 557218.624 5936690.183 
557230.716 5936690.308 557232.757 5936690.46 557246.445 5936691.482 
557260.981 5936692.567 557276.423 5936693.718 557303.6472 5936695.7478 
557316.405 5936696.699 557319.183 5936697.419 557321.606 5936699.002 
557323.248 5936701.408 557323.977 5936704.207 557324.872 5936715.209 
557323.103 5936715.025 557324.48 5936731.963 557326.254 5936732.199 
557327.353 5936749.254 557328.457 5936766.407 557328.652 5936769.424 
557329.558 5936783.484 557306.729 5936778.227 557303.6472 5936777.5174 
557270.594 5936769.907 557265.672 5936768.773 557255.24 5936766.708 
557241.825 5936764.055 557237.198 5936763.14 557230.531 5936761.821 
557217.971 5936759.337 557217.151 5936759.174 557207.962 5936757.358 
557200.149 5936756.106 557190.7757 5936754.605 557185.205 5936753.713 
557170.883 5936751.42 557158.439 5936749.428 557149.835 5936747.132 
557128.379 5936741.407 557101.084 5936734.124 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>
        <xplan:BP_KomplexeZweckbestLandwirtschaft>
          <xplan:allgemein>1600</xplan:allgemein>
        </xplan:BP_KomplexeZweckbestLandwirtschaft>
      </xplan:zweckbestimmung>
    </xplan:BP_LandwirtschaftsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Strassenverkehr gml:id="Gml_AFD405CF-5867-4986-83AB-8CF87885B91C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557096.286 5936728.762</gml:lowerCorner>
          <gml:upperCorner>557329.705 5936787.686</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_612F9C72-83DD-4220-98A7-7AB487797D85" />
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_3483729C-6B85-430E-A1D4-B83B985E0894" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">557096.286 5936728.762 557101.084 5936734.124 557128.379 5936741.407 
557149.835 5936747.132 557158.439 5936749.428 557170.883 5936751.42 
557185.205 5936753.713 557190.7757 5936754.605 557200.149 5936756.106 
557207.962 5936757.358 557217.151 5936759.174 557217.971 5936759.337 
557230.531 5936761.821 557237.198 5936763.14 557241.825 5936764.055 
557255.24 5936766.708 557265.672 5936768.773 557270.594 5936769.907 
557303.6472 5936777.5174 557306.729 5936778.227 557329.558 5936783.484 
557329.705 5936787.686 557276.18 5936775.274 557264.817 5936772.681 
557256.623 5936771.059 557256.196 5936775.052 557236.296 5936771.114 
557235.115 5936781.557 557225.201 5936779.617 557221.758 5936778.943 
557222.851 5936773.407 557216.123 5936768.578 557212.864 5936766.239 
557196.265 5936763.582 557177.244 5936760.538 557161.389 5936757.999 
557156.386 5936757.197 557147.401 5936755.479 557128.638 5936751.891 
557127.502 5936750.485 557102.87 5936743.912 557097.449 5936748.336 
557096.532 5936732.911 557096.286 5936728.762 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>
        <xplan:SO_KomplexeZweckbestStrassenverkehr>
          <xplan:allgemein>140013</xplan:allgemein>
        </xplan:SO_KomplexeZweckbestStrassenverkehr>
      </xplan:artDerFestlegung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:hatDarstellungMitBesondZweckbest>false</xplan:hatDarstellungMitBesondZweckbest>
    </xplan:SO_Strassenverkehr>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinbedarfsFlaeche gml:id="Gml_6EAE5CEE-79A3-4CB2-8DF3-1D81DB9150B1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557082.896 5936743.912</gml:lowerCorner>
          <gml:upperCorner>557332.925 5936947.632</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_612F9C72-83DD-4220-98A7-7AB487797D85" />
      <xplan:rechtscharakter>7000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_56171ABA-F36D-4520-8F9D-A315B32CCA24" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0B0A9924-EDEB-4523-BB14-C6D87F1C4A45" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557097.449 5936748.336 557102.87 5936743.912 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1B452D76-28CA-4667-A70C-991B7BCF1E22" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557102.87 5936743.912 557127.502 5936750.485 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6D1F0A07-42CB-4299-9C2C-68B479142700" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557127.502 5936750.485 557128.638 5936751.891 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2614D7A5-DF5A-4BDF-8117-B23102025C8F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557128.638 5936751.891 557147.401 5936755.479 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0AB46BD2-AB13-440F-B723-778F850B8E4A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557147.401 5936755.479 557156.386 5936757.197 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_417E3438-5960-4650-AAEB-CCFEA587C0AE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557156.386 5936757.197 557161.389 5936757.999 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_42C50DDE-BFD9-47B9-827A-73E0734FCEDF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557161.389 5936757.999 557177.244 5936760.538 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1A82406E-4481-4038-9ABE-DBFF92833C99" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557177.244 5936760.538 557196.265 5936763.582 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E00D785D-3E5F-4DB0-9039-A23675D79573" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557196.265 5936763.582 557212.864 5936766.239 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9ED30C34-CB7E-4F91-80A4-AFBA88D063BA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557212.864 5936766.239 557216.123 5936768.578 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_31ECBBC8-AC25-4BA4-92BD-644DBEC0BC2A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557216.123 5936768.578 557222.851 5936773.407 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5598ED9B-360B-4A23-8A8C-CE2DD59BBF1F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557222.851 5936773.407 557221.758 5936778.943 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E9EFA5D6-40AD-48B1-8AF9-D6129868F673" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557221.758 5936778.943 557225.201 5936779.617 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4105B5F4-8772-4157-B3D8-A56E22F3CF0E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557225.201 5936779.617 557235.115 5936781.557 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EC151BD2-A0E3-4A53-973A-4A201A0A9F78" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557235.115 5936781.557 557236.296 5936771.114 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2662B288-96AB-4C2D-951F-2272E18E411D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557236.296 5936771.114 557256.196 5936775.052 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CE786404-4C50-4560-BC86-E0EC65863103" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557256.196 5936775.052 557256.623 5936771.059 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D192C620-3CD9-4077-BD38-5FB718E28120" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557256.623 5936771.059 557264.817 5936772.681 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8FB3E39F-967F-44DB-8F7E-AA7FF7958213" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557264.817 5936772.681 557276.18 5936775.274 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_677BEF5C-71A5-42B0-9BCD-4EB0751A12DD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557276.18 5936775.274 557329.705 5936787.686 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_32ADB4A9-4D44-4979-ABDE-4BB320AF18DC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557329.705 5936787.686 557330.163 5936800.699 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_298B3A62-CDF3-4E16-A473-9C2150D309B9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557330.163 5936800.699 557330.701 5936816.037 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4AB7F27E-1E5F-4483-B9CC-1E636B15BE2F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557330.701 5936816.037 557331.241 5936831.464 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_19BDBB1F-DFE1-4CDD-9E5D-80F54B92C389" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557331.241 5936831.464 557331.621 5936842.299 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B9A0A01A-9B7B-43DF-9C02-F97BDE533FD2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557331.621 5936842.299 557331.858 5936849.068 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1CEC7F19-6DC8-459E-9125-5F2FDEBF3378" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557331.858 5936849.068 557332.205 5936858.941 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_67E4B69A-1809-48F6-8C41-88A60948B852" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557332.205 5936858.941 557332.312 5936861.98 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F22EB2EB-B07A-4FDD-BE1C-3A238512BC46" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557332.312 5936861.98 557332.925 5936879.46 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9E11BDFE-9F17-4B3F-8982-5F5582EBF2B8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557332.925 5936879.46 557332.236 5936886.429 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BC01C4F7-8C01-4FD3-ABF3-311D2DAD3C74" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557332.236 5936886.429 557329.738 5936892.991 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_60033774-13CB-48BF-8590-C54982C92AD2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557329.738 5936892.991 557325.621 5936898.652 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EBE07529-CFD1-4580-BC1D-7ACF0F84C07C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557325.621 5936898.652 557320.152 5936903.06 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FA205E0F-0B34-4299-9EEE-65828CEE6BCE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557320.152 5936903.06 557312.969 5936907.425 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_983EDD4C-81D0-4184-8701-AD8152033D49" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557312.969 5936907.425 557289.035 5936921.965 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A10E2959-B238-40CC-8BD1-97AECF8ADCAC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557289.035 5936921.965 557285.079 5936924.369 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A541FF61-B4C0-463A-9BC8-3167D5731822" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557285.079 5936924.369 557266.601 5936932.317 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BBAD03A6-1C83-4A91-A739-6CD62E78EF24" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557266.601 5936932.317 557248.134 5936940.262 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A40AC900-5F70-4D39-A45C-7E5E7F4654BF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557248.134 5936940.262 557238.684 5936943.809 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0874F00F-9761-48EE-8F73-06D35D6191C8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557238.684 5936943.809 557233.495 5936945.128 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4634736D-4FD0-4EC8-9F13-2195599A6A94" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557233.495 5936945.128 557228.908 5936946.294 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2ED5DA05-43A8-4984-A2F2-CF0CB6CA781C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557228.908 5936946.294 557218.884 5936947.544 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7D1DD950-BC04-485E-883A-CE477B08BC79" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557218.884 5936947.544 557208.848 5936947.632 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_77BCEB60-444E-4ED6-901E-803AC67349AB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557208.848 5936947.632 557181.21 5936936.938 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C27EC648-21FF-4A48-8CA9-B7F734D5004F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557181.21 5936936.938 557169.523 5936932.518 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_263594E2-5343-4AFE-BFE3-173E3FC4295F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557169.523 5936932.518 557167.711 5936931.674 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_27FE190A-BF54-4EC0-92CA-D6EE845496F6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557167.711 5936931.674 557117.719 5936908.398 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_96684F1E-298E-4615-B683-88D632F4FA66" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557117.719 5936908.398 557105.291 5936902.612 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FF10F250-9A49-45F7-9724-2CEAE15DD3EF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557105.291 5936902.612 557085.808 5936893.54 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FFE3ED60-8B9E-4702-B0AA-B92C9F4C3D0B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557085.808 5936893.54 557082.896 5936887.295 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_46663884-8116-4FDD-8059-62465CB3353A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557082.896 5936887.295 557089.988 5936860.845 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_92111C76-9BB9-4965-9FB7-82E310F95D30" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557089.988 5936860.845 557092.284 5936850.813 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_856A678A-87B2-46CF-9638-BAA293BE5FF1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557092.284 5936850.813 557093.792 5936843.489 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8B798DC8-3C28-4FA1-921E-BC561016E1D0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557093.792 5936843.489 557095.881 5936832.656 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_55BFBC1F-9970-478C-ACB2-0BCD864FF99B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557095.881 5936832.656 557096.392 5936828.69 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_80856176-CE3C-45F2-8FB2-312EE1CD3CEB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557096.392 5936828.69 557097.492 5936820.181 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_29569DD8-189F-4A7D-8CB1-5D280D56A199" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557097.492 5936820.181 557098.17 5936811.708 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0B4E5BDE-09F4-4D65-BA22-8F1DA89C971E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557098.17 5936811.708 557098.318 5936809.862 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4D413EE1-E7B9-4ED1-B3AA-B400C1D2FEA7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557098.318 5936809.862 557098.435 5936807.666 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_BF838D4A-C440-43F6-B25D-C5B11D53889D" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">557098.435 5936807.666 557098.924968697 5936798.92075357 557099.162 5936790.165 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_91F22BAE-F682-4A5C-8C6D-514346BAAF5C" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">557099.162 5936790.165 557099.101000379 5936779.42601613 557098.658 5936768.696 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C250D55F-2646-498A-8974-FB9394AFBE23" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">557098.658 5936768.696 557097.449 5936748.336 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>
        <xplan:BP_KomplexeZweckbestGemeinbedarf>
          <xplan:allgemein>2600</xplan:allgemein>
        </xplan:BP_KomplexeZweckbestGemeinbedarf>
      </xplan:zweckbestimmung>
    </xplan:BP_GemeinbedarfsFlaeche>
  </gml:featureMember>
</xplan:XPlanAuszug>