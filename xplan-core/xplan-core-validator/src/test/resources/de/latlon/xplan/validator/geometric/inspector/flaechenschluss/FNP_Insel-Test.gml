﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_4651C8BC-23F8-43F0-B954-0D9DE0C2C461" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/2 http://www.xplanungwiki.de/upload/XPlanGML/5.2/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/5/2">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>577347.269 5907319.5402</gml:lowerCorner>
      <gml:upperCorner>578039.4204 5908246.6421</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:FP_Plan gml:id="Gml_286721FA-96D9-4C58-AD4C-CE5009460448">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>577347.269 5907319.5402</gml:lowerCorner>
          <gml:upperCorner>578039.4204 5908246.6421</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>FNP Insel-Test</xplan:name>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_6B23DDA0-D5C2-421C-B28F-D4B2C2536C46" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">577406.0066 5908233.9421 577347.269 5907621.1658 577604.4445 5907386.2154 
578039.4204 5907319.5402 578006.0828 5908246.6421 577406.0066 5908233.9421 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">577582.2194 5907941.8415 577761.6073 5907941.8415 577766.3698 5907729.116 
577582.2194 5907735.4661 577582.2194 5907941.8415 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>03353040</xplan:ags>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:bereich xlink:href="#Gml_F9D4A8C2-81B9-41C0-A85A-D202793127A8" />
    </xplan:FP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:FP_Bereich gml:id="Gml_F9D4A8C2-81B9-41C0-A85A-D202793127A8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>577347.269 5907319.5402</gml:lowerCorner>
          <gml:upperCorner>578039.4204 5908246.6421</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_5580AD14-FDF3-47BB-AC45-781175062926" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">577406.0066 5908233.9421 577347.269 5907621.1658 577604.4445 5907386.2154 
578039.4204 5907319.5402 578006.0828 5908246.6421 577406.0066 5908233.9421 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">577582.2194 5907941.8415 577761.6073 5907941.8415 577766.3698 5907729.116 
577582.2194 5907735.4661 577582.2194 5907941.8415 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#Gml_073E7112-B2BE-48FE-9CBF-6429E3EAEAF7" />
      <xplan:planinhalt xlink:href="#Gml_4D9DFCF5-98CD-44AB-9437-AB5E550DBD9F" />
      <xplan:gehoertZuPlan xlink:href="#Gml_286721FA-96D9-4C58-AD4C-CE5009460448" />
    </xplan:FP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:FP_Landwirtschaft gml:id="Gml_073E7112-B2BE-48FE-9CBF-6429E3EAEAF7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>577369.048 5907848.1788</gml:lowerCorner>
          <gml:upperCorner>578016.974 5908246.6421</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_F9D4A8C2-81B9-41C0-A85A-D202793127A8" />
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_55170CDA-212E-48D2-833C-99A62D1613B8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">577406.0066 5908233.9421 577369.048 5907848.3737 577582.2194 5907848.1788 
577582.2194 5907941.8415 577761.6073 5907941.8415 578016.974 5907943.7635 
578006.0828 5908246.6421 577406.0066 5908233.9421 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
    </xplan:FP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:FP_BebauungsFlaeche gml:id="Gml_4D9DFCF5-98CD-44AB-9437-AB5E550DBD9F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>577347.269 5907319.5402</gml:lowerCorner>
          <gml:upperCorner>578039.4204 5907943.7635</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_F9D4A8C2-81B9-41C0-A85A-D202793127A8" />
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_89300658-D72E-41EE-9662-0AAD093A9C0D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">577369.048 5907848.3737 577347.269 5907621.1658 577604.4445 5907386.2154 
578039.4204 5907319.5402 578016.974 5907943.7635 577761.6073 5907941.8415 
577766.3698 5907729.116 577582.2194 5907735.4661 577582.2194 5907848.1788 
577369.048 5907848.3737 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
    </xplan:FP_BebauungsFlaeche>
  </gml:featureMember>
</xplan:XPlanAuszug>
