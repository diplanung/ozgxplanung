<gml:MultiSurface xmlns:gml="http://www.opengis.net/gml/3.2" gml:id="GML_306679b7-39fa-4cf4-9907-c61c6102d458"
                  srsName="EPSG:25832" srsDimension="2">
  <gml:surfaceMember>
    <gml:Polygon gml:id="mpoly.geom.0.0">
      <gml:exterior>
        <gml:LinearRing>
          <gml:posList>563456.633379448 5934814.91196812 563615.527392626 5934559.84526276 563318.646473268 5934553.57313066 563456.633379448 5934814.91196812
          </gml:posList>
        </gml:LinearRing>
      </gml:exterior>
    </gml:Polygon>
  </gml:surfaceMember>
  <gml:surfaceMember>
    <gml:Polygon gml:id="mpoly.geom.1.0">
      <gml:exterior>
        <gml:LinearRing>
          <gml:posList>563615.527392626 5934559.84526276 563797.4192235 5934223.24084011 564021.125268368 5934628.83871585 563615.527392626 5934559.84526276
          </gml:posList>
        </gml:LinearRing>
      </gml:exterior>
    </gml:Polygon>
  </gml:surfaceMember>
</gml:MultiSurface>
