<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<gml:Ring xmlns:gml="http://www.opengis.net/gml/3.2" srsName="EPSG:25832"
          gml:id="GML_2736d7d0-4494-48c2-86ea-635130e68120">
  <gml:curveMember>
    <gml:Curve srsName="EPSG:25832"
               gml:id="GML_2736d7d0-4494-48c2-86ea-635130e68121">
      <gml:segments>
        <gml:LineStringSegment interpolation="linear">
          <gml:posList srsDimension="2" count="7">572209.380 5938582.980 572183.015 5938671.367
            572166.198 5938665.141 572135.549 5938653.793 572083.047 5938634.355 572078.872 5938625.272
            572116.844 5938522.692
          </gml:posList>
        </gml:LineStringSegment>
        <gml:ArcString interpolation="circularArc3Points">
          <gml:posList srsDimension="2" count="7">572116.844 5938522.692 572119.846 5938515.711
            572123.546 5938509.074 572130.282 5938501.276 572139.043 5938495.852 572146.175 5938493.244
            572153.536 5938491.380
          </gml:posList>
        </gml:ArcString>
        <gml:LineStringSegment interpolation="linear">
          <gml:posList srsDimension="2" count="3">572153.536 5938491.380 572168.119 5938488.476
            572194.546 5938483.212
          </gml:posList>
        </gml:LineStringSegment>
        <gml:ArcString interpolation="circularArc3Points">
          <gml:posList srsDimension="2" count="5">572194.546 5938483.212 572195.728 5938482.968
            572196.907 5938482.708 572203.063 5938481.062 572209.080 5938478.965
          </gml:posList>
        </gml:ArcString>
        <gml:LineStringSegment interpolation="linear">
          <gml:posList srsDimension="2" count="4">572209.080 5938478.965 572225.523 5938528.858
            572213.862 5938567.956 572209.380 5938582.980
          </gml:posList>
        </gml:LineStringSegment>
      </gml:segments>
    </gml:Curve>
  </gml:curveMember>
</gml:Ring>
