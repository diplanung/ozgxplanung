﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_BA7FEDB4-20D0-4380-852C-09F2A16F98CB" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/2 http://www.xplanungwiki.de/upload/XPlanGML/5.2/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/5/2">
<!-- Gml_187912A3-F601-4334-8868-64C917A46ED2: Schnittpunkte mit dem Umring des Geltungsbereich: (574391.1320890521,5947122.911581698) -->
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>574369.114 5947113.1631</gml:lowerCorner>
      <gml:upperCorner>574973.8737 5947514.7064</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="Gml_7F15F4F7-92AA-4604-A944-B5296D79706A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574369.114 5947113.1631</gml:lowerCorner>
          <gml:upperCorner>574973.8737 5947514.7064</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>6_Schnittpunkte_Geltungsbereich_Linie</xplan:name>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_A8142C48-98AB-4760-B6FE-C30FC2B0F048" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574948.6922 5947223.8427 574972.718 5947414.4767 574973.8737 5947423.6464 
574968.0287 5947423.9573 574611.3662 5947502.9334 574463.3403 5947514.7064 
574449.422 5947501.569 574447.491 5947494.794 574439.535 5947466.864 
574437.479 5947459.643 574436.408 5947455.882 574436.937 5947455.702 
574438.315 5947455.235 574436.025 5947445.66 574432.5307 5947431.0496 
574434.648 5947430.501 574436.211 5947430.096 574433.582 5947418.761 
574431.252 5947408.716 574445.288 5947406.526 574438.487 5947382.289 
574427.052 5947341.535 574420.762 5947319.113 574413.387 5947292.703 
574406.238 5947267.102 574392.093 5947216.471 574372.303 5947217.945 
574369.114 5947218.0566 574369.3581 5947196.2684 574370.2471 5947116.9213 
574370.2892 5947113.1631 574375.1651 5947113.2373 574375.008 5947122.642 
574428.001 5947123.528 574479.523 5947127.996 574493.66 5947128.054 
574498.8049 5947128.0757 574566.816 5947128.362 574660.776 5947128.758 
574702.71 5947128.931 574748.898 5947129.118 574748.851 5947140.899 
574748.66 5947189.2228 574748.634 5947195.807 574749.3333 5947198.2846 
574749.382 5947198.457 574749.678 5947198.453 574767.01 5947198.217 
574786.906 5947197.945 574823.227 5947205.023 574854.016 5947212.936 
574885.02 5947213.359 574947.4293 5947213.8217 574948.6922 5947223.8427 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:bereich xlink:href="#Gml_07DEBC3D-B397-49CD-B684-FAB7DBFDD2D5" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="Gml_07DEBC3D-B397-49CD-B684-FAB7DBFDD2D5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574369.114 5947113.1631</gml:lowerCorner>
          <gml:upperCorner>574973.8737 5947514.7064</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_F8E23DA7-FFD0-414F-993B-ABADD970EFE7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574948.6922 5947223.8427 574972.718 5947414.4767 574973.8737 5947423.6464 
574968.0287 5947423.9573 574611.3662 5947502.9334 574463.3403 5947514.7064 
574449.422 5947501.569 574447.491 5947494.794 574439.535 5947466.864 
574437.479 5947459.643 574436.408 5947455.882 574436.937 5947455.702 
574438.315 5947455.235 574436.025 5947445.66 574432.5307 5947431.0496 
574434.648 5947430.501 574436.211 5947430.096 574433.582 5947418.761 
574431.252 5947408.716 574445.288 5947406.526 574438.487 5947382.289 
574427.052 5947341.535 574420.762 5947319.113 574413.387 5947292.703 
574406.238 5947267.102 574392.093 5947216.471 574372.303 5947217.945 
574369.114 5947218.0566 574369.3581 5947196.2684 574370.2471 5947116.9213 
574370.2892 5947113.1631 574375.1651 5947113.2373 574375.008 5947122.642 
574428.001 5947123.528 574479.523 5947127.996 574493.66 5947128.054 
574498.8049 5947128.0757 574566.816 5947128.362 574660.776 5947128.758 
574702.71 5947128.931 574748.898 5947129.118 574748.851 5947140.899 
574748.66 5947189.2228 574748.634 5947195.807 574749.3333 5947198.2846 
574749.382 5947198.457 574749.678 5947198.453 574767.01 5947198.217 
574786.906 5947197.945 574823.227 5947205.023 574854.016 5947212.936 
574885.02 5947213.359 574947.4293 5947213.8217 574948.6922 5947223.8427 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#Gml_187912A3-F601-4334-8868-64C917A46ED2" />
      <xplan:gehoertZuPlan xlink:href="#Gml_7F15F4F7-92AA-4604-A944-B5296D79706A" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Grenze gml:id="Gml_187912A3-F601-4334-8868-64C917A46ED2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574390.3645 5947116.7695</gml:lowerCorner>
          <gml:upperCorner>574575.1608 5947142.57200939</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_07DEBC3D-B397-49CD-B684-FAB7DBFDD2D5" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="Gml_885805C3-C725-4546-BAAF-35B4E0742731" srsName="EPSG:25832">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">574390.3645 5947116.7695 574394.617409525 5947130.72491005 574405.922 5947139.947 
</gml:posList>
            </gml:ArcString>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">574405.922 5947139.947 574415.81231599 5947139.09875054 574424.1783 5947133.7557 
</gml:posList>
            </gml:ArcString>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">574424.1783 5947133.7557 574432.134479886 5947127.33560011 574442.024 5947124.7441 
</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">574442.024 5947124.7441 574479.523 5947127.996 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">574479.523 5947127.996 574493.66 5947128.054 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">574493.66 5947128.054 574498.8049 5947128.0757 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">574498.8049 5947128.0757 574504.9766 5947128.1017 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">574504.9766 5947128.1017 574525.4638 5947128.1879 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">574525.4638 5947128.1879 574533.437169385 5947130.41847274 574540.0131 5947135.4491 
</gml:posList>
            </gml:ArcString>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">574540.0131 5947135.4491 574554.864585573 5947142.57181951 574569.7788 5947135.5814 
</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">574569.7788 5947135.5814 574575.1608 5947128.3961 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:SO_Grenze>
  </gml:featureMember>
</xplan:XPlanAuszug>
