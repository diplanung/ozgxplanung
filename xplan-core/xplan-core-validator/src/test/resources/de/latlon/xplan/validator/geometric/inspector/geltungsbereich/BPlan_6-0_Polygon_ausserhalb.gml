﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_BB48D3AF-C7A1-4F1F-8583-EB9D6DEABA9B" xsi:schemaLocation="http://www.xplanung.de/xplangml/6/0 http://repository.gdi-de.org/schemas/de.xleitstelle.xplanung/6.0/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/6/0">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>557094.5259 5936699.1238</gml:lowerCorner>
      <gml:upperCorner>557245.9037 5936833.7776</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="Gml_CD6556C4-8BDB-43DA-821A-66FAE8B17BBA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557094.5259 5936699.1238</gml:lowerCorner>
          <gml:upperCorner>557245.9037 5936833.7776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan_6-0_Polygon_ausserhalb</xplan:name>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_34139393-7909-4B99-9E02-FEAE8423AC4E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">557096.392 5936828.69 557094.5259 5936699.1238 557245.9037 5936699.9855 
557243.4354 5936833.7776 557096.392 5936828.69 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:bereich xlink:href="#Gml_432F3BD2-639E-4A0F-826E-53D45A7209C6" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="Gml_432F3BD2-639E-4A0F-826E-53D45A7209C6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557094.5259 5936699.1238</gml:lowerCorner>
          <gml:upperCorner>557245.9037 5936833.7776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_6D854150-CF75-42F1-BA39-BB99E4F024FB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">557096.392 5936828.69 557094.5259 5936699.1238 557245.9037 5936699.9855 
557243.4354 5936833.7776 557096.392 5936828.69 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#Gml_7AC61FB9-43DD-45BE-B01F-8F443B5F303F" />
      <xplan:planinhalt xlink:href="#Gml_758D6453-D193-40D3-964F-30E05A6BDB5D" />
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:gehoertZuPlan xlink:href="#Gml_CD6556C4-8BDB-43DA-821A-66FAE8B17BBA" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_7AC61FB9-43DD-45BE-B01F-8F443B5F303F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557094.5259 5936699.1238</gml:lowerCorner>
          <gml:upperCorner>557245.9037 5936833.7776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_432F3BD2-639E-4A0F-826E-53D45A7209C6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_D5979F89-F198-478E-9326-AC2DABF9110C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">557096.392 5936828.69 557094.5259 5936699.1238 557245.9037 5936699.9855 
557243.4354 5936833.7776 557096.392 5936828.69 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_758D6453-D193-40D3-964F-30E05A6BDB5D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557476.6925 5936689.3625</gml:lowerCorner>
          <gml:upperCorner>557533.8426 5936736.1938</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_432F3BD2-639E-4A0F-826E-53D45A7209C6" />
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_30AB6596-9D0A-4B6E-81C5-4DA92682F914" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">557476.6925 5936735.4001 557477.4863 5936689.3625 557533.8426 5936689.3625 
557529.0801 5936736.1938 557476.6925 5936735.4001 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:ziel>1000</xplan:ziel>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
</xplan:XPlanAuszug>