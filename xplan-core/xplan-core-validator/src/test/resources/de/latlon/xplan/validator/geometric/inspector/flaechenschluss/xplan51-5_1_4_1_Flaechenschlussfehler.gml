﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_9341BAAA-FED0-441D-AD90-7DD17DC72C9A" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/1 http://www.xplanungwiki.de/upload/XPlanGML/5.1/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/5/1">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>575369.2493 5952969.0597</gml:lowerCorner>
      <gml:upperCorner>577020.2526 5954596.2504</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:FP_Plan gml:id="Gml_762FDDCF-26DF-4E9F-9FC1-A58ABE47B89A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>575369.2493 5952969.0597</gml:lowerCorner>
          <gml:upperCorner>577020.2526 5954596.2504</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>xplan51-5_1_4_1</xplan:name>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_2FDAE845-8EFF-4B39-811C-60F8D019B794" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576956.7524 5954069.7285 576676.2935 5954490.4169 576276.7719 5954596.2504 
575705.2708 5954350.1874 575448.6244 5953971.8325 575369.2493 5953384.4563 
575615.3122 5953014.0389 576467.2723 5953585.5401 576655.1268 5952969.0597 
577020.2526 5953342.1229 576956.7524 5954069.7285 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>020000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>4000</xplan:rechtsstand>
      <xplan:bereich xlink:href="#Gml_2F7C8AD1-2BC6-4C6F-83CB-488EAFDA0008" />
    </xplan:FP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:FP_Bereich gml:id="Gml_2F7C8AD1-2BC6-4C6F-83CB-488EAFDA0008">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>575369.2493 5952969.0597</gml:lowerCorner>
          <gml:upperCorner>577020.2526 5954596.2504</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_50C484B1-00F2-44B3-B9C3-16A770ED276F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576956.7524 5954069.7285 576676.2935 5954490.4169 576276.7719 5954596.2504 
575705.2708 5954350.1874 575448.6244 5953971.8325 575369.2493 5953384.4563 
575615.3122 5953014.0389 576467.2723 5953585.5401 576655.1268 5952969.0597 
577020.2526 5953342.1229 576956.7524 5954069.7285 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#Gml_3E40CD06-7F9E-4301-B244-9C93B5B9F81C" />
      <xplan:planinhalt xlink:href="#Gml_AF8315A1-FDB4-4152-B0DF-D276D8EE8DD5" />
      <xplan:planinhalt xlink:href="#Gml_9A630642-3D4A-4EC6-8353-587CD3B65ECA" />
      <xplan:planinhalt xlink:href="#Gml_4C6F208C-300A-4DC5-91AA-ABF379F51244" />
      <xplan:planinhalt xlink:href="#Gml_702A30AF-09E5-4FDD-A2D7-4924C20ED521" />
      <xplan:planinhalt xlink:href="#Gml_00A349DD-0327-4BC2-AB22-6057DBBE2083" />
      <xplan:planinhalt xlink:href="#Gml_7DAD41E4-48B1-4A7E-81E7-50E2AC790E9E" />
      <xplan:planinhalt xlink:href="#Gml_35BA56CB-5727-4568-AB9F-A8145A0211E5" />
      <xplan:planinhalt xlink:href="#Gml_E776F534-00C5-4A18-8B52-CE999F89DD2A" />
      <xplan:planinhalt xlink:href="#Gml_EA1811EF-8DAE-40A3-B172-7F2D54988781" />
      <xplan:gehoertZuPlan xlink:href="#Gml_762FDDCF-26DF-4E9F-9FC1-A58ABE47B89A" />
    </xplan:FP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:FP_Strassenverkehr gml:id="Gml_3E40CD06-7F9E-4301-B244-9C93B5B9F81C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>575582.822 5952969.0597</gml:lowerCorner>
          <gml:upperCorner>576689.1771 5954596.2504</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F7C8AD1-2BC6-4C6F-83CB-488EAFDA0008" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_C8C58EB1-73C0-4093-A282-9103D54A2A15" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576397.4187 5954130.4029 576276.7719 5954596.2504 576230.8286 5954576.4693 576388.8385 5953925.7968 576456.6889 5953646.3944 576008.118 5953346.9018 575582.822 5953062.9489 575615.3122 5953014.0389 576467.2723 5953585.5401 576655.1268 5952969.0597 576689.1771 5953003.8502 576569.9321 5953464.285 576441.0575 5953961.9024 576397.4187 5954130.4029 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1200</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:FP_Strassenverkehr>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:FP_WaldFlaeche gml:id="Gml_AF8315A1-FDB4-4152-B0DF-D276D8EE8DD5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>575369.2493 5953062.9489</gml:lowerCorner>
          <gml:upperCorner>576008.118 5953585.5401</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F7C8AD1-2BC6-4C6F-83CB-488EAFDA0008" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_CDF1AE01-1B8E-44FE-B9E0-78FFDA954F43" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576008.118 5953346.9018 575845.5002 5953564.3734 575670.8749 5953585.5401 
575369.2493 5953384.4563 575582.822 5953062.9489 576008.118 5953346.9018 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:FP_WaldFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:FP_Gruen gml:id="Gml_9A630642-3D4A-4EC6-8353-587CD3B65ECA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576569.9321 5953003.8502</gml:lowerCorner>
          <gml:upperCorner>577020.2526 5953628.0052</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F7C8AD1-2BC6-4C6F-83CB-488EAFDA0008" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_F0D3A0D4-8411-43F2-8DC0-84628590D652" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576689.1771 5953003.8502 577020.2526 5953342.1229 576995.3028 5953628.0052 576723.4115 5953523.3574 576569.9321 5953464.285 576689.1771 5953003.8502 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:FP_Gruen>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:FP_Gewaesser gml:id="Gml_4C6F208C-300A-4DC5-91AA-ABF379F51244">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576678.9394 5953523.3574</gml:lowerCorner>
          <gml:upperCorner>576995.3028 5954069.7285</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F7C8AD1-2BC6-4C6F-83CB-488EAFDA0008" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_3839A58B-96B9-42AD-81EC-EA3181FDAE2F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576995.3028 5953628.0052 576956.7524 5954069.7285 576835.0438 5954053.8535 
576880.0231 5953760.1654 576678.9394 5953694.0195 576723.4115 5953523.3574 
576995.3028 5953628.0052 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:FP_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:FP_Wasserwirtschaft gml:id="Gml_702A30AF-09E5-4FDD-A2D7-4924C20ED521">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576276.7719 5954130.4029</gml:lowerCorner>
          <gml:upperCorner>576676.2935 5954596.2504</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F7C8AD1-2BC6-4C6F-83CB-488EAFDA0008" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_2EBE40FA-B606-489E-8166-A4C7D9254202" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576676.2935 5954490.4169 576276.7719 5954596.2504 576397.4187 5954130.4029 
576676.2935 5954490.4169 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1200</xplan:zweckbestimmung>
    </xplan:FP_Wasserwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:FP_Gemeinbedarf gml:id="Gml_00A349DD-0327-4BC2-AB22-6057DBBE2083">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>575705.2708 5953876.5823</gml:lowerCorner>
          <gml:upperCorner>576388.8385 5954576.4693</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F7C8AD1-2BC6-4C6F-83CB-488EAFDA0008" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_3A0CB0AE-9E94-41E7-BBB6-EAFE9C3E53E6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576230.8286 5954576.4693 575705.2708 5954350.1874 575792.5834 5954085.6036 
576125.9591 5954223.1872 576250.3135 5953876.5823 576388.8385 5953925.7968 
576230.8286 5954576.4693 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1600</xplan:zweckbestimmung>
    </xplan:FP_Gemeinbedarf>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:FP_BebauungsFlaeche gml:id="Gml_7DAD41E4-48B1-4A7E-81E7-50E2AC790E9E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>575369.2493 5953384.4563</gml:lowerCorner>
          <gml:upperCorner>576250.3135 5954350.1874</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F7C8AD1-2BC6-4C6F-83CB-488EAFDA0008" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7ACA84E2-1515-4D29-A18F-47668A0F361E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">575705.2708 5954350.1874 575448.6244 5953971.8325 575369.2493 5953384.4563 
575670.8749 5953585.5401 575845.5002 5953564.3734 576001.6047 5953770.7488 
576250.3135 5953876.5823 576125.9591 5954223.1872 575792.5834 5954085.6036 
575705.2708 5954350.1874 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:FP_BebauungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:FP_VerEntsorgung gml:id="Gml_35BA56CB-5727-4568-AB9F-A8145A0211E5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>575845.5002 5953346.9018</gml:lowerCorner>
          <gml:upperCorner>576456.6889 5953925.7968</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F7C8AD1-2BC6-4C6F-83CB-488EAFDA0008" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_79DDC05D-6842-4148-BC87-BD05BD8189A9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576456.6889 5953646.3944 576388.8385 5953925.7968 576250.3135 5953876.5823 
576001.6047 5953770.7488 575845.5002 5953564.3734 576008.118 5953346.9018 
576456.6889 5953646.3944 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:FP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:FP_SpielSportanlage gml:id="Gml_E776F534-00C5-4A18-8B52-CE999F89DD2A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576441.0575 5953464.285</gml:lowerCorner>
          <gml:upperCorner>576880.0231 5954053.8535</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F7C8AD1-2BC6-4C6F-83CB-488EAFDA0008" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_492BE85D-4AAE-4350-990E-F0277E6129C4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576723.4115 5953523.3574 576678.9394 5953694.0195 576880.0231 5953760.1654 
576835.0438 5954053.8535 576441.0575 5953961.9024 576569.9321 5953464.285 
576723.4115 5953523.3574 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:FP_SpielSportanlage>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:FP_BebauungsFlaeche gml:id="Gml_EA1811EF-8DAE-40A3-B172-7F2D54988781">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576397.4187 5953961.9024</gml:lowerCorner>
          <gml:upperCorner>576956.7524 5954490.4169</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
	  <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F7C8AD1-2BC6-4C6F-83CB-488EAFDA0008" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_8FAF8C2C-36A8-4181-8B42-291762B66EB1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576835.0438 5954053.8535 576956.7524 5954069.7285 576676.2935 5954490.4169 
576397.4187 5954130.4029 576441.0575 5953961.9024 576835.0438 5954053.8535 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>3000</xplan:allgArtDerBaulNutzung>
    </xplan:FP_BebauungsFlaeche>
  </gml:featureMember>
</xplan:XPlanAuszug>
