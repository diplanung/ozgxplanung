<gml:Polygon xmlns:gml="http://www.opengis.net/gml/3.2" srsName="EPSG:25832" gml:id="GML_45ddaf05-73a8-42dd-b7ef-fe3549c449e1">
  <gml:exterior>
    <gml:Ring>
      <gml:curveMember>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_1ba62067-b84e-4568-a5f1-ab67b514f852">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="7">
                572078.872 5938625.272
                572083.047 5938634.355
                572135.549 5938653.793
                572166.198 5938665.141
                572194.430 5938578.006
                572210.848 5938527.335
                572196.907 5938482.708
              </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">
                572196.907 5938482.708
                572195.728 5938482.968
                572194.546 5938483.212
              </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="3">
                572194.546 5938483.212
                572168.119 5938488.476
                572153.536 5938491.380
              </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="7">
                572153.536 5938491.380
                572146.175 5938493.244
                572139.043 5938495.852
                572130.282 5938501.276
                572123.546 5938509.074
                572119.846 5938515.711
                572116.844 5938522.692
              </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">
                572116.844 5938522.692
                572078.872 5938625.272
              </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </gml:curveMember>
    </gml:Ring>
  </gml:exterior>
  <gml:interior>
    <gml:Ring>
      <gml:curveMember>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_e1176420-6223-4f27-ba63-acc8dfed2329">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">
                572117.933 5938592.407
                572111.552 5938575.664
                572129.681 5938559.849
                572175.612 5938578.049
              </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">
                572175.612 5938578.049
                572173.569 5938595.846
                572162.867 5938610.212
              </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">
                572162.867 5938610.212
                572117.933 5938592.407
              </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </gml:curveMember>
    </gml:Ring>
  </gml:interior>
</gml:Polygon>
