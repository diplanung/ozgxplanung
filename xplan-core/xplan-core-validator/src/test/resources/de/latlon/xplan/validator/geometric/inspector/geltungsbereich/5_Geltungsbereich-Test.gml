﻿<?xml version="1.0" encoding="utf-8"?>
<!-- XPLANBOX-1856: Fall 5: Objekt (Gml_775C7DC3-0EC1-4BF5-A2BF-E529847C264C) außerhalb GP, in GB -->
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_789C38E0-6696-42E1-8AC6-A4A4ED42B7D1" xsi:schemaLocation="http://www.xplanung.de/xplangml/6/0 http://repository.gdi-de.org/schemas/de.xleitstelle.xplanung/6.0/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/6/0">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>564984.593 5934515.633</gml:lowerCorner>
      <gml:upperCorner>565350.954 5934743.006</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_b7e8fcfa-cecb-418c-904a-bb5e135d8bcd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564984.593 5934515.633</gml:lowerCorner>
          <gml:upperCorner>565343.193 5934743.006</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>5_Geltungsbereich-Test</xplan:name>
      <xplan:beschreibung> Der Bebauungsplan BPlan002_5-2 für den Geltungsbereich zwischen der Laeiszhalle 
und den Straßen Dammtorwall, Caffamacherreihe und Valentinskamp (Bezirk 
Hamburg-Mitte, Ortsteil 108) wird festgestellt.
Das Gebiet wird wie folgt begrenzt:
Dammtorwall – Ca</xplan:beschreibung>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:MultiSurface gml:id="Gml_30638EA3-A3A1-4084-B428-31D2A77CC3E5" srsName="EPSG:25832">
          <gml:surfaceMember>
            <gml:Polygon gml:id="Gml_46D3FB57-86ED-417F-A095-45A7DAB0C09C" srsName="EPSG:25832">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsName="EPSG:25832">565201.138 5934689.173 565201.138 5934696.508 565181.673 5934687.992 
565162.935 5934678.746 565150.525 5934672.179 565134.949 5934663.417 
565122.782 5934656.845 565110.615 5934648.818 565100.396 5934642.246 
565088.592 5934632.149 565081.048 5934625.823 565066.448 5934613.168 
565052.337 5934599.057 565036.391 5934581.511 564984.593 5934524.512 
564985.062 5934515.633 565009.216 5934517.109 565042.886 5934519.162 
565042.773 5934527.073 565044.863 5934527.174 565198.796 5934534.239 
565201.955 5934534.333 565201.678 5934550.926 565200.2 5934638.503 
565201.137 5934638.501 565201.138 5934689.173 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
          <gml:surfaceMember>
            <gml:Polygon gml:id="Gml_E78AB92D-D7CC-49CE-A413-BE78070B4ACA" srsName="EPSG:25832">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsName="EPSG:25832">565301.028 5934736.742 565301.218 5934726.323 565301.315 5934718.388 
565301.376 5934713.42 565301.378 5934713.254 565301.441 5934708.047 
565301.835 5934675.91 565301.96 5934661.725 565301.976 5934659.928 
565302.104 5934645.606 565302.107 5934645.264 565339.551 5934644.566 
565339.575 5934645.166 565339.677 5934647.788 565340.001 5934656.04 
565340.162 5934658.807 565340.176 5934659.052 565340.714 5934674.103 
565340.825 5934677.199 565340.935 5934680.268 565342.205 5934713.07 
565343.193 5934713.218 565343.127 5934713.66 565338.735 5934743.006 
565301.016 5934737.373 565301.028 5934736.742 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#GML_90d196c6-7704-4ecc-a6d7-394f1c0478c4" />
      <xplan:texte xlink:href="#GML_c180d211-33e9-4580-8e8f-f58eadca8f8a" />
      <xplan:texte xlink:href="#GML_0ad7d318-e915-42be-8739-1ce485872219" />
      <xplan:texte xlink:href="#GML_203cbf95-87be-466f-b869-335011c68542" />
      <xplan:texte xlink:href="#GML_5fab2299-1c39-4f77-8ab8-c12805fd3b4e" />
      <xplan:texte xlink:href="#GML_b9b06b68-91d2-4b39-9bf8-bd42bf8490fb" />
      <xplan:texte xlink:href="#GML_94338043-1aad-4bae-8c47-5ae720ca393e" />
      <xplan:texte xlink:href="#GML_52dee64b-a907-4b1b-892d-61fff34962fc" />
      <xplan:texte xlink:href="#GML_073c07fd-6041-46ee-9271-69585acaf7a7" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>108</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2021-03-08</xplan:rechtsverordnungsDatum>
      <xplan:versionBauNVO>
        <xplan:XP_GesetzlicheGrundlage>
          <xplan:name>Version vom 21.11.2017</xplan:name>
        </xplan:XP_GesetzlicheGrundlage>
      </xplan:versionBauNVO>
      <xplan:bereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
	  <xplan:bereich xlink:href="#Gml_FBDF41B7-DCE0-4CF8-B2F3-E8C4105C197E" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_90d196c6-7704-4ecc-a6d7-394f1c0478c4">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Auf der mit „(C)“ bezeichneten Fläche des Kerngebiets darf
die festgesetzte Traufhöhe durch Aufbauten für Nebenanlagen
und Haustechnik um höchstens 8 m überschritten werden.
Die Aufbauten sind gruppiert anzuordnen und durch
Verkleidungen gestalterisch zusammenzufassen. Frei stehende
Antennenanlagen sind nicht zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_c180d211-33e9-4580-8e8f-f58eadca8f8a">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Im Kerngebiet sind Spielhallen und ähnliche Unternehmen
im Sinne von § 1 Absatz 2 des Hamburgischen Spielhallengesetzes
vom 4. Dezember 2012 (HmbGVBl. S. 505), zuletzt
geändert am 17. Februar 2021 (HmbGVBl. S. 75, 77), die der
Aufstellung von Spielgeräten mit oder ohne Gewinnmöglichkeiten
dienen, Vorführ- und Geschäftsräume, deren
Zweck auf Darstellungen oder auf Handlungen mit sexuellem
Charakter ausgerichtet ist, sowie Tankstellen im Zusammenhang
mit Parkhäusern und Großgaragen unzulässig.
Ausnahmen für Tankstellen nach § 7 Absatz 3 Nummer 1
der Baunutzungsverordnung in der Fassung vom 21. November
2017 (BGBl. I S. 3787) werden ausgeschlossen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_0ad7d318-e915-42be-8739-1ce485872219">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>Auf der mit „(B)“ bezeichneten Fläche des Kerngebiets darf
die Höhe der durch die festgesetzten Traufhöhen entstehenden
geneigten Dachfläche durch Aufbauten für Nebenanlagen
und Haustechnik um höchstens 1 m überschritten werden.
Die Aufbauten sind gruppiert anzuordnen und durch
Verkleidungen gestalterisch zusammenzufassen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_203cbf95-87be-466f-b869-335011c68542">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>Stellplätze sind nur in Tiefgaragen zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_5fab2299-1c39-4f77-8ab8-c12805fd3b4e">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Auf der mit „(A)“ bezeichneten Fläche des Kerngebiets sind
oberhalb des ersten Geschosses ausschließlich Wohnungen
zulässig. Mindestens 4 700 m² der Geschossfläche sind für
Wohnungen vorzusehen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_b9b06b68-91d2-4b39-9bf8-bd42bf8490fb">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Auf der mit „(D)“ bezeichneten Fläche des Kerngebiets
sind mindestens fünf großkronige einheimische, standortgerechte
Laubbäume mit einem Stammumfang von mindestens
25 cm zu pflanzen und dauerhaft zu erhalten. Für
anzupflanzende Bäume auf Tiefgaragen muss auf einer Fläche
von 12 m² je Baum die Schichtstärke des durchwurzelbaren
Substrataufbaus mindestens 1 m betragen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_94338043-1aad-4bae-8c47-5ae720ca393e">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Auf der mit „(E)“ bezeichneten Fläche des Kerngebiets sind
mindestens zehn großkronige einheimische, standortgerechte
Laubbäume mit einem Stammumfang von mindestens
25 cm zu pflanzen und dauerhaft zu erhalten. Für anzupflanzende
Bäume auf Tiefgaragen muss auf einer Fläche
von 12 m² je Baum die Schichtstärke des durchwurzelbaren
Substrataufbaus mindestens 1 m betragen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_52dee64b-a907-4b1b-892d-61fff34962fc">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>Auf den mit „(A)“ bezeichneten Flächen des Kerngebiets
sind durch Anordnung der Baukörper beziehungsweise
durch geeignete Grundrissgestaltung die Wohn- und Schlafräume
den lärmabgewandten Gebäudeseiten zuzuordnen.
Sofern eine Anordnung aller Wohn- und Schlafräume einer
Wohnung an den lärmabgewandten Gebäudeseiten nicht
möglich ist, sind vorrangig die Schlafräume den lärmabgewandten
Gebäudeseiten zuzuordnen. Für die Räume an den
lärmzugewandten Gebäudeseiten muss ein ausreichender
Lärmschutz durch bauliche Maßnahmen an Außentüren,
Fenstern, Außenwänden und Dächern der Gebäude geschaffen
werden. Wohn-/Schlafräume in Einzimmerwohnungen
und Kinderzimmer sind wie Schlafräume zu beurteilen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_073c07fd-6041-46ee-9271-69585acaf7a7">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Das festgesetzte Gehrecht umfasst die Befugnis der Freien
und Hansestadt Hamburg, einen allgemein zugänglichen
Weg anzulegen und zu unterhalten. Geringfügige Abweichungen von dem festgesetzten Gehrecht können zugelassen
werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_70a45912-2871-4085-acf9-d3351cb00b20">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564984.593 5934515.633</gml:lowerCorner>
          <gml:upperCorner>565343.193 5934743.006</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>1</xplan:nummer>
      <xplan:geltungsbereich>        
            <gml:Polygon gml:id="Gml_AB3F2D77-D440-4165-BF22-2162B6AF9B11" srsName="EPSG:25832">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsName="EPSG:25832">565201.138 5934689.173 565201.138 5934696.508 565181.673 5934687.992 
565162.935 5934678.746 565150.525 5934672.179 565134.949 5934663.417 
565122.782 5934656.845 565110.615 5934648.818 565100.396 5934642.246 
565088.592 5934632.149 565081.048 5934625.823 565066.448 5934613.168 
565052.337 5934599.057 565036.391 5934581.511 564984.593 5934524.512 
564985.062 5934515.633 565009.216 5934517.109 565042.886 5934519.162 
565042.773 5934527.073 565044.863 5934527.174 565198.796 5934534.239 
565201.955 5934534.333 565201.678 5934550.926 565200.2 5934638.503 
565201.137 5934638.501 565201.138 5934689.173 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>          
      </xplan:geltungsbereich>     
      <xplan:planinhalt xlink:href="#GML_d096515a-05d4-4279-bb15-feb7c9d2e4b9" />
      <xplan:planinhalt xlink:href="#GML_ca159f3b-db8a-4696-a302-7c0e2fc04cdd" />
      <xplan:planinhalt xlink:href="#GML_a1b82d18-a41b-4881-bd89-89c1d74ab19d" />
      <xplan:planinhalt xlink:href="#GML_f77fee26-ad00-4388-9992-44ec1b78ef98" />
      <xplan:planinhalt xlink:href="#GML_aabbde2d-6fc9-4a96-826d-94268e779c8c" />
      <xplan:planinhalt xlink:href="#GML_f30f55de-adc6-4ddf-b0bb-208af2faec51" />
      <xplan:planinhalt xlink:href="#GML_ebb838c5-f952-42c3-b7a6-f7bd7d7b8384" />
      <xplan:planinhalt xlink:href="#GML_97a0450d-c3e6-4c4c-a9b9-2c87db917f64" />
      <xplan:planinhalt xlink:href="#GML_12af2fdb-69e1-4511-ad33-e78ca923e7df" />
      <xplan:planinhalt xlink:href="#GML_74695132-425d-4934-baea-be62efed31dc" />
      <xplan:planinhalt xlink:href="#GML_6437f13a-9da9-488d-84b5-871c9afec4b8" />
      <xplan:planinhalt xlink:href="#GML_20aa47c4-f8ae-4ef8-8b5b-caefaaebac66" />
      <xplan:planinhalt xlink:href="#GML_d50519c3-ed1b-4ed1-945d-eeb95b49fa97" />
      <xplan:planinhalt xlink:href="#GML_10181d95-090e-44b8-891c-b107dfda3117" />
      <xplan:planinhalt xlink:href="#GML_80335b66-8fb7-46a3-947f-8bddad49ee79" />
      <xplan:planinhalt xlink:href="#GML_225d02a7-5968-4b9f-b767-828a7683e216" />
      <xplan:planinhalt xlink:href="#GML_23e509ba-2f62-4294-834a-3bf97cef25ed" />
      <xplan:planinhalt xlink:href="#GML_528e7bdc-54b9-4b92-afb4-c564854493a3" />
      <xplan:planinhalt xlink:href="#GML_d8192c7f-a252-4050-b474-47694b5008e6" />
      <xplan:planinhalt xlink:href="#GML_00641b3c-05b0-42b1-811b-d7e2368c2d75" />
      <xplan:planinhalt xlink:href="#GML_1a5d5f69-6030-418e-8c50-9beb655cd816" />
      <xplan:planinhalt xlink:href="#GML_c804c892-8e65-4be7-8416-f4e3db65a417" />
      <xplan:planinhalt xlink:href="#GML_f5255a2e-51f5-472b-82a7-c834187ddf9a" />
      <xplan:planinhalt xlink:href="#GML_56261e3a-d8f3-4fa1-940f-b7e614f35aef" />
      <xplan:planinhalt xlink:href="#GML_6684677c-066b-485a-8c89-7db721819c87" />
      <xplan:planinhalt xlink:href="#GML_bf1e5a22-839d-4ef5-aaf3-3b4d97d2e963" />
      <xplan:planinhalt xlink:href="#GML_c57c2d59-3e07-455f-af8a-70e50607f2f1" />
      <xplan:planinhalt xlink:href="#GML_65006077-7996-40d6-bb62-0bd5b81334de" />
      <xplan:planinhalt xlink:href="#GML_cc3f7613-8da2-45ab-8e7c-c6d74c15de60" />
      <xplan:planinhalt xlink:href="#GML_d40b6338-c05d-48bc-b074-4f865f28bf13" />
      <xplan:planinhalt xlink:href="#GML_2c65a70f-1649-4012-b70d-4b61ed3157be" />
      <xplan:planinhalt xlink:href="#GML_e3587eee-0394-4062-b606-3a197d395a39" />
      <xplan:planinhalt xlink:href="#GML_09ca07dc-e432-4d01-a9c1-16816f563962" />
      <xplan:planinhalt xlink:href="#GML_db2debbc-9beb-47a9-b35e-8c6ffb087a9a" />
      <xplan:planinhalt xlink:href="#GML_0ca33bb0-185b-4d14-9359-41d6e9838701" />
      <xplan:planinhalt xlink:href="#GML_5f5fc8e2-f0d6-4132-9774-5dd43f52014a" />
      <xplan:planinhalt xlink:href="#GML_218233aa-c960-4155-a0eb-ea5eee23b45f" />
      <xplan:planinhalt xlink:href="#GML_03835fde-b640-43d4-9ca1-661f64c8458f" />
      <xplan:planinhalt xlink:href="#GML_7fbf04dc-cda8-4acd-831a-31d6327e2e27" />
      <xplan:planinhalt xlink:href="#GML_ad476a3d-d0c4-464d-87b3-da973ee4b9f0" />
      <xplan:planinhalt xlink:href="#GML_d1a4521b-59c9-4321-acd4-3f227535c90d" />
      <xplan:planinhalt xlink:href="#GML_33328a77-91a6-4864-b8a2-c52eab49bcac" />
      <xplan:planinhalt xlink:href="#GML_1b73e286-945e-4f67-b58b-0763bc4c1053" />
      <xplan:planinhalt xlink:href="#GML_10cdd180-6bfc-4872-8199-5869396827ea" />
      <xplan:planinhalt xlink:href="#GML_25f5c07b-1409-439e-8cea-2b89439f782e" />
      <xplan:planinhalt xlink:href="#GML_523a00ad-6533-4e60-b38a-06be2c3623d8" />
      <xplan:praesentationsobjekt xlink:href="#GML_56418f5b-3769-4c8b-8951-9ed4eba0c60b" />
      <xplan:praesentationsobjekt xlink:href="#GML_b37a15fb-1e04-4e1a-b065-e8e59009cce9" />
      <xplan:praesentationsobjekt xlink:href="#GML_e56bff69-5ddb-489a-af95-5c5033272577" />
      <xplan:praesentationsobjekt xlink:href="#GML_dd06a4de-f527-4d88-a87d-bb807bc51497" />
      <xplan:praesentationsobjekt xlink:href="#GML_198bf623-4f2f-4aaf-a21c-33a1b089d4e3" />
      <xplan:praesentationsobjekt xlink:href="#GML_3d97cdff-abfb-4d58-ae33-3c337440e946" />
      <xplan:praesentationsobjekt xlink:href="#GML_5a7d638b-c969-48f6-b532-27283883b07b" />
      <xplan:praesentationsobjekt xlink:href="#GML_809762ae-acf3-4ba0-9760-b988ac532891" />
      <xplan:praesentationsobjekt xlink:href="#GML_1ca60951-ec86-4875-b260-fbbe77da4968" />
      <xplan:praesentationsobjekt xlink:href="#GML_25c63b93-2cfa-4fa3-906d-ca0cc4c5799a" />
      <xplan:praesentationsobjekt xlink:href="#GML_0d135aa7-ebd4-487a-bfed-6c3b94473835" />
      <xplan:praesentationsobjekt xlink:href="#GML_9db49e76-0ff9-4057-89e8-9e677146a085" />
      <xplan:praesentationsobjekt xlink:href="#GML_c245b09b-2893-4014-bf33-b8ce363b1479" />
      <xplan:praesentationsobjekt xlink:href="#GML_a97811ae-add4-443d-9730-22929ea94313" />
      <xplan:praesentationsobjekt xlink:href="#GML_05108ce5-8c8d-4453-a291-6cbe7c7387f4" />
      <xplan:praesentationsobjekt xlink:href="#GML_60127909-667e-411d-b2f5-7176dd1ba297" />
      <xplan:praesentationsobjekt xlink:href="#GML_0284b4a3-f97a-4479-a85a-b09c10bd1b73" />
      <xplan:praesentationsobjekt xlink:href="#GML_c48df557-2637-47be-87a1-57e83f845993" />
      <xplan:praesentationsobjekt xlink:href="#GML_9ad62bbb-e92c-40a0-b728-552b351cfc3e" />
      <xplan:praesentationsobjekt xlink:href="#GML_c63d5ebe-b8d1-47ef-95e3-2cdb99dafe62" />
      <xplan:praesentationsobjekt xlink:href="#GML_24875732-da3e-4e57-9f25-89d74cd41468" />
      <xplan:praesentationsobjekt xlink:href="#GML_3ba582d4-6de2-420b-bb0a-fd8ea26d551f" />
      <xplan:praesentationsobjekt xlink:href="#GML_b68d8a04-fcb4-4fe0-ba1d-7c622be61da9" />
      <xplan:praesentationsobjekt xlink:href="#GML_0227c06e-c9c3-4d61-88cd-f57845c78844" />
      <xplan:praesentationsobjekt xlink:href="#GML_7783fc04-bf01-4b3c-b4bb-3058ca6071e5" />
      <xplan:praesentationsobjekt xlink:href="#GML_bb877354-91fc-4ee7-8255-d91292e4e374" />
      <xplan:praesentationsobjekt xlink:href="#GML_87e60ce7-8f61-4bc7-ad2b-72062166e775" />
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:gehoertZuPlan xlink:href="#GML_b7e8fcfa-cecb-418c-904a-bb5e135d8bcd" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_d096515a-05d4-4279-bb15-feb7c9d2e4b9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565146.479 5934640.094</gml:lowerCorner>
          <gml:upperCorner>565182.632 5934666.699</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_A45B9DF0-C35A-408C-A385-7D21820C22D2" srsName="EPSG:25832">
          <gml:posList>565146.479 5934652.557 565169.702 5934666.699 565182.133 5934659.567 
565182.632 5934640.396 565150.142 5934640.094 565146.479 5934652.557 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ca159f3b-db8a-4696-a302-7c0e2fc04cdd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565122.961 5934559.556</gml:lowerCorner>
          <gml:upperCorner>565188.396 5934626.078</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_F4AF0C53-0674-4221-8C43-DE5AAA6B790C" srsName="EPSG:25832">
          <gml:posList>565136.37 5934626.078 565122.961 5934617.865 565145.834 5934580.501 
565138.524 5934567.051 565152.363 5934559.556 565173.263 5934598.038 
565188.396 5934598.428 565187.991 5934614.164 565144.374 5934613.026 
565136.37 5934626.078 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Denkmalschutzrecht gml:id="GML_a1b82d18-a41b-4881-bd89-89c1d74ab19d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565112.66 5934547.571</gml:lowerCorner>
          <gml:upperCorner>565189.454 5934673.06</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Dammtorwall 15, vor Nr. 15, Valentinskamp 70, Unilever-Haus, Ensemble Dammtorwall 15 (Unilever-Haus), Gebäude mit Außenanlagen und Flügelstern-Plastik</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_6239CCE4-6073-4D1D-AD4A-4F0B76077E5F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_90760FC9-0597-44A2-B867-BFEDD7FC9E84" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565182.655 5934550.058 565189.454 5934557.359 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_89643B4A-AE2F-4AB9-A6B7-9BEBF58F3424" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565189.454 5934557.359 565186.667 5934665.599 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B24349FE-D5C1-4432-9AE1-C04F00F31D2E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565186.667 5934665.599 565173.663 5934673.06 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_0769849E-C16E-486F-BF73-6EFC8661248A" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">565173.663 5934673.06 565160.10095163 5934665.25797155 565146.693 5934657.194 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F8A8DE0D-29E1-4B88-A680-AF931CAA7034" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565146.693 5934657.194 565150.506 5934646.972 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EFD21B2A-ABF3-4CA3-80BF-E72648D783A1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565150.506 5934646.972 565148.472 5934646.921 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1F388901-CAE9-48CA-8947-9DA65F58E120" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565148.472 5934646.921 565112.66 5934624.983 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_46C3C691-6954-4E4F-B1C0-B54866733E9F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565112.66 5934624.983 565139.991 5934580.364 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8D1F3865-3EC7-493B-B604-D3C456330E15" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565139.991 5934580.364 565127.69 5934557.732 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DEB7532B-95AC-4543-9287-775458668AB7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565127.69 5934557.732 565128.154 5934547.571 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_59C116E2-E157-4F0C-BDF4-D6505F4DEC01" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565128.154 5934547.571 565182.655 5934550.058 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
      <xplan:nummer>	29977</xplan:nummer>
    </xplan:SO_Denkmalschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_f77fee26-ad00-4388-9992-44ec1b78ef98">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565182.204 5934562.41</gml:lowerCorner>
          <gml:upperCorner>565188.889 5934579.296</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_1A276AC3-C8F8-4605-AEED-05AF6A0F4F08" srsName="EPSG:25832">
          <gml:posList>565182.337 5934562.41 565182.204 5934567.575 565188.889 5934579.296 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_aabbde2d-6fc9-4a96-826d-94268e779c8c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565042.904 5934548.647</gml:lowerCorner>
          <gml:upperCorner>565119.138 5934623.909</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_8F0D1319-84B7-4446-BF3B-FB990B8487A0" srsName="EPSG:25832">
          <gml:posList>565099.779 5934589.684 565096.454 5934587.161 565091.525 5934583.397 
565086.596 5934579.634 565090.934 5934572.758 565095.725 5934575.761 
565101.428 5934579.335 565110.283 5934584.888 565119.138 5934590.44 
565108.893 5934606.761 565098.128 5934623.909 565089.853 5934616.124 
565081.579 5934608.339 565073.304 5934600.553 565065.029 5934592.768 
565055.91 5934584.169 565046.792 5934575.569 565042.904 5934571.727 
565043.727 5934557.927 565044.153 5934548.647 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_f30f55de-adc6-4ddf-b0bb-208af2faec51">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565182.241 5934535.476</gml:lowerCorner>
          <gml:upperCorner>565189.98 5934688.241</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>7000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_450F887E-60B4-48D8-8F73-02145B802C04" srsName="EPSG:25832">
          <gml:posList>565188.985 5934535.476 565189.277 5934548.754 565189.98 5934559.934 
565189.896 5934564.909 565188.807 5934599.633 565187.029 5934665.011 
565184.798 5934676.723 565182.241 5934688.241 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:zweckbestimmung>
        <xplan:BP_KomplexeZweckbestVerEntsorgung>
          <xplan:allgemein>1000</xplan:allgemein>
        </xplan:BP_KomplexeZweckbestVerEntsorgung>
      </xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_ebb838c5-f952-42c3-b7a6-f7bd7d7b8384">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565095.54 5934626.495</gml:lowerCorner>
          <gml:upperCorner>565095.54 5934626.495</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">15.3</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_414C51A5-AE38-4433-B03E-7E03D26ECF5A" srsName="EPSG:25832">
          <gml:pos>565095.54 5934626.495</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_97a0450d-c3e6-4c4c-a9b9-2c87db917f64">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565048.033 5934563.32</gml:lowerCorner>
          <gml:upperCorner>565111.833 5934615.32</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_DAD35447-85AB-4730-BA64-3C29B97CE61C" srsName="EPSG:25832">
          <gml:posList>565104.433 5934603.52 565097.033 5934615.32 565072.533 5934592.27 
565048.033 5934569.22 565057.233 5934563.32 565074.833 5934583.62 
565092.433 5934603.92 565099.633 5934596.32 565106.833 5934588.72 
565111.833 5934591.72 565104.433 5934603.52 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnittFlaeche gml:id="GML_12af2fdb-69e1-4511-ad33-e78ca923e7df">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565146.479 5934640.094</gml:lowerCorner>
          <gml:upperCorner>565182.632 5934666.699</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(E)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_809762ae-acf3-4ba0-9760-b988ac532891" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_94338043-1aad-4bae-8c47-5ae720ca393e" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3EBD03A2-F6E5-4A43-AE00-C7649F8AD29B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565182.632 5934640.396 565182.133 5934659.567 565169.702 5934666.699 
565146.479 5934652.557 565150.142 5934640.094 565182.632 5934640.396 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextAbschnittFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_809762ae-acf3-4ba0-9760-b988ac532891">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565164.044 5934647.51</gml:lowerCorner>
          <gml:upperCorner>565164.044 5934647.51</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:gliederung2</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_12af2fdb-69e1-4511-ad33-e78ca923e7df" />
      <xplan:position>
        <gml:Point gml:id="Gml_86FEB618-1742-4EB4-AF7A-6C0B94447206" srsName="EPSG:25832">
          <gml:pos>565164.044 5934647.51</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnittFlaeche gml:id="GML_74695132-425d-4934-baea-be62efed31dc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565064.743 5934544.677</gml:lowerCorner>
          <gml:upperCorner>565128.134 5934562.172</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_60127909-667e-411d-b2f5-7176dd1ba297" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_5fab2299-1c39-4f77-8ab8-c12805fd3b4e" />
      <xplan:refTextInhalt xlink:href="#GML_52dee64b-a907-4b1b-892d-61fff34962fc" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2645139D-5A16-433E-8DD0-9188C173EDC2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565067.573 5934544.807 565070.403 5934544.937 565073.233 5934545.067 
565076.063 5934545.197 565084.15 5934545.565 565095.146 5934546.066 
565106.142 5934546.567 565117.138 5934547.068 565128.134 5934547.569 
565128 5934550.83 565127.684 5934557.72 565127.53 5934561.205 
565106.642 5934561.553 565085.754 5934561.901 565077.641 5934562.036 
565069.528 5934562.172 565069.224 5934561.059 565067.142 5934553.446 
565064.743 5934544.677 565067.573 5934544.807 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextAbschnittFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_60127909-667e-411d-b2f5-7176dd1ba297">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565119.995 5934558.512</gml:lowerCorner>
          <gml:upperCorner>565119.995 5934558.512</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:gliederung2</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_74695132-425d-4934-baea-be62efed31dc" />
      <xplan:position>
        <gml:Point gml:id="Gml_E6654A51-F2F5-4C83-BA58-ED6740AD0E98" srsName="EPSG:25832">
          <gml:pos>565119.995 5934558.512</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_6437f13a-9da9-488d-84b5-871c9afec4b8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565043.82 5934552.123</gml:lowerCorner>
          <gml:upperCorner>565062.089 5934563.706</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Durchgang</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">3</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5a7d638b-c969-48f6-b532-27283883b07b" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_073c07fd-6041-46ee-9271-69585acaf7a7" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_463EB81F-4996-4E5C-8031-48EBFD74E614" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565045.076 5934552.627 565062.089 5934560.532 565060.614 5934563.706 
565043.82 5934555.902 565043.993 5934552.123 565045.076 5934552.627 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5a7d638b-c969-48f6-b532-27283883b07b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565037.59 5934554.45</gml:lowerCorner>
          <gml:upperCorner>565037.59 5934554.45</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMin</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMax</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hZwingend</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderHoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderBezugspunkt</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6437f13a-9da9-488d-84b5-871c9afec4b8" />
      <xplan:position>
        <gml:Point gml:id="Gml_D9A2C72D-9CDB-4C1D-BAEC-B393CB360F38" srsName="EPSG:25832">
          <gml:pos>565037.59 5934554.45</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_20aa47c4-f8ae-4ef8-8b5b-caefaaebac66">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565046.792 5934575.569</gml:lowerCorner>
          <gml:upperCorner>565098.117 5934623.926</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="Gml_3C911F6A-104A-497E-AF55-18859F1580C5" srsName="EPSG:25832">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">565098.117 5934623.926 565084.192792743 5934612.14181472 565070.928 5934599.62 
</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">565070.928 5934599.62 565046.792 5934575.569 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BereichOhneEinAusfahrtLinie gml:id="GML_d50519c3-ed1b-4ed1-945d-eeb95b49fa97">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565044.326 5934542.548</gml:lowerCorner>
          <gml:upperCorner>565189.78 5934549.156</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Gehwegsüberfahrten nicht zugelassen</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_DA725765-4DA1-4B77-99CC-012F73C817DD" srsName="EPSG:25832">
          <gml:posList>565044.326 5934542.548 565189.78 5934549.156 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>3000</xplan:typ>
    </xplan:BP_BereichOhneEinAusfahrtLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Strassenverkehr gml:id="GML_10181d95-090e-44b8-891c-b107dfda3117">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564984.593 5934515.633</gml:lowerCorner>
          <gml:upperCorner>565201.955 5934696.508</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_BEE8AD52-50A3-4ECA-97A4-B7F941FEF01F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565201.138 5934689.173 565201.138 5934696.508 565181.673 5934687.992 
565162.935 5934678.746 565150.525 5934672.179 565134.949 5934663.417 
565122.782 5934656.845 565110.615 5934648.818 565100.396 5934642.246 
565088.592 5934632.149 565081.048 5934625.823 565066.448 5934613.168 
565052.337 5934599.057 565036.391 5934581.511 564984.593 5934524.512 
564985.062 5934515.633 565009.216 5934517.109 565042.886 5934519.162 
565042.773 5934527.073 565044.863 5934527.174 565198.796 5934534.239 
565201.955 5934534.333 565201.678 5934550.926 565200.2 5934638.503 
565201.137 5934638.501 565201.138 5934689.173 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_77F2C9F7-3545-4CF9-8783-0E646C8DF2D2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565044.128 5934548.669 565043.202 5934566.241 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D8ABDB5B-85F7-4539-88C0-C6C8E1B0C3BC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565043.202 5934566.241 565042.904 5934571.727 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6C298CC5-AFB1-4768-8187-BF0FA3C2CA37" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565042.904 5934571.727 565046.792 5934575.569 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5674A835-5057-407A-AD38-67CDBA4E21EC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565046.792 5934575.569 565070.928 5934599.62 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_F2056435-AB55-4D97-B8A2-229DDAC9ECEB" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">565070.928 5934599.62 565095.240462892 5934621.59637909 565121.578 5934641.1 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_380917FD-20A4-47FF-8BE7-07C7CCC6EC1B" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">565121.578 5934641.1 565134.061117519 5934649.26307531 565146.693 5934657.194 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_88F68ADE-D471-466B-9266-521884F8027D" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">565146.693 5934657.194 565160.10095163 5934665.25797155 565173.663 5934673.06 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3A495DE8-58A6-4205-ACF6-967CE234617B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565173.663 5934673.06 565186.667 5934665.599 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A0DB9680-DA02-4147-ABDC-9532DE48DBCB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565186.667 5934665.599 565188.889 5934579.296 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_69462168-99A2-4888-B57A-DF221F03588B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565188.889 5934579.296 565182.204 5934567.575 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EDD91B1A-A870-4AB0-A3F0-4DEEE7332E62" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565182.204 5934567.575 565182.655 5934550.058 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9FD46B4F-6D43-4EC5-8056-FF809F37514F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565182.655 5934550.058 565128.154 5934547.571 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6C35974B-2743-45A4-9ADD-815F80E5C8CC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565128.154 5934547.571 565064.743 5934544.677 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_372CB9FA-3A87-4163-8655-8911D0F0D023" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565064.743 5934544.677 565049.308 5934543.973 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2432434C-8CA1-4603-9B86-D5DC55E1989F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565049.308 5934543.973 565044.128 5934548.669 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:hatDarstellungMitBesondZweckbest>false</xplan:hatDarstellungMitBesondZweckbest>
    </xplan:SO_Strassenverkehr>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_80335b66-8fb7-46a3-947f-8bddad49ee79">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565044.128 5934543.752</gml:lowerCorner>
          <gml:upperCorner>565049.308 5934548.669</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">43</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9ad62bbb-e92c-40a0-b728-552b351cfc3e" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_9393EEA9-14CA-4268-96FA-183DEE57165A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565049.308 5934543.973 565044.128 5934548.669 565044.377 5934543.752 
565049.308 5934543.973 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.8</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9ad62bbb-e92c-40a0-b728-552b351cfc3e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565041.183 5934540.403</gml:lowerCorner>
          <gml:upperCorner>565041.183 5934540.403</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMin</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMax</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hZwingend</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderHoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderBezugspunkt</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_80335b66-8fb7-46a3-947f-8bddad49ee79" />
      <xplan:position>
        <gml:Point gml:id="Gml_405B9ED0-B495-4FE0-B9C7-3B7ACF77F41A" srsName="EPSG:25832">
          <gml:pos>565041.183 5934540.403</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_225d02a7-5968-4b9f-b767-828a7683e216">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565043.227 5934543.973</gml:lowerCorner>
          <gml:upperCorner>565127.964 5934621.977</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">45.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7783fc04-bf01-4b3c-b4bb-3058ca6071e5" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_20782148-72BE-4146-8072-23BC8E95E0BF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565096.074 5934621.977 565081.579 5934608.339 565065.029 5934592.768 
565046.792 5934575.569 565046.414 5934575.195 565043.227 5934566.311 
565043.727 5934557.927 565044.128 5934548.669 565049.308 5934543.973 
565057.025 5934544.325 565064.743 5934544.677 565070.403 5934544.937 
565076.063 5934545.197 565084.15 5934545.565 565106.142 5934546.567 
565123.905 5934547.377 565124.048 5934551.368 565127.964 5934551.561 
565127.857 5934554.086 565127.684 5934557.72 565127.657 5934558.322 
565125.308 5934558.29 565125.267 5934561.243 565106.642 5934561.553 
565085.754 5934561.901 565077.641 5934562.036 565069.528 5934562.172 
565068.07 5934562.196 565061.826 5934562.297 565063.218 5934563.904 
565073.347 5934575.596 565083.477 5934587.289 565088.647 5934593.248 
565093.816 5934599.207 565096.701 5934601.092 565102.846 5934591.688 
565099.779 5934589.684 565096.454 5934587.161 565091.525 5934583.397 
565089.207 5934581.627 565093.685 5934574.482 565095.725 5934575.761 
565101.428 5934579.335 565110.283 5934584.888 565116.773 5934588.957 
565096.074 5934621.977 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.8</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7783fc04-bf01-4b3c-b4bb-3058ca6071e5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565055.805 5934570.327</gml:lowerCorner>
          <gml:upperCorner>565055.805 5934570.327</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMin</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMax</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hZwingend</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderHoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderBezugspunkt</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_225d02a7-5968-4b9f-b767-828a7683e216" />
      <xplan:position>
        <gml:Point gml:id="Gml_09A2DFCC-546C-4D7E-AFA2-02D983BBA65C" srsName="EPSG:25832">
          <gml:pos>565055.805 5934570.327</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnittFlaeche gml:id="GML_23e509ba-2f62-4294-834a-3bf97cef25ed">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565053.066 5934549.442</gml:lowerCorner>
          <gml:upperCorner>565068.24 5934557.577</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(B)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_24875732-da3e-4e57-9f25-89d74cd41468" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_0ad7d318-e915-42be-8739-1ce485872219" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D4A48E89-9F8B-4257-A32C-FD9C68D63865" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565066.208 5934550.034 565067.142 5934553.446 565068.24 5934557.461 
565061.337 5934557.577 565053.066 5934549.442 565066.208 5934550.034 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextAbschnittFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_24875732-da3e-4e57-9f25-89d74cd41468">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565061.436 5934554.226</gml:lowerCorner>
          <gml:upperCorner>565061.436 5934554.226</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:gliederung2</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_23e509ba-2f62-4294-834a-3bf97cef25ed" />
      <xplan:position>
        <gml:Point gml:id="Gml_9E339D80-5265-4A26-A13D-38A06E1A46DE" srsName="EPSG:25832">
          <gml:pos>565061.436 5934554.226</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_528e7bdc-54b9-4b92-afb4-c564854493a3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565188.397 5934579.814</gml:lowerCorner>
          <gml:upperCorner>565188.876 5934598.427</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_F7AA43B8-83F7-496D-96F3-24A7CB0D1F33" srsName="EPSG:25832">
          <gml:posList>565188.876 5934579.814 565188.397 5934598.427 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d8192c7f-a252-4050-b474-47694b5008e6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565086.596 5934572.758</gml:lowerCorner>
          <gml:upperCorner>565093.685 5934581.627</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">48</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c48df557-2637-47be-87a1-57e83f845993" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_C49498BD-4416-46EA-B48F-8AD402D97BB6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565093.685 5934574.482 565089.207 5934581.627 565086.596 5934579.634 
565090.934 5934572.758 565093.685 5934574.482 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.8</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c48df557-2637-47be-87a1-57e83f845993">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565081.175 5934575.759</gml:lowerCorner>
          <gml:upperCorner>565081.175 5934575.759</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMin</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMax</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hZwingend</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderHoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderBezugspunkt</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d8192c7f-a252-4050-b474-47694b5008e6" />
      <xplan:position>
        <gml:Point gml:id="Gml_02F4C83D-8921-4250-98C9-DB568C916730" srsName="EPSG:25832">
          <gml:pos>565081.175 5934575.759</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_00641b3c-05b0-42b1-811b-d7e2368c2d75">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565098.117 5934614.164</gml:lowerCorner>
          <gml:upperCorner>565187.991 5934673.06</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="Gml_CAE8BA02-83D8-4CD7-B2FC-E65A957DAB07" srsName="EPSG:25832">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">565187.991 5934614.164 565186.667 5934665.599 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">565186.667 5934665.599 565173.663 5934673.06 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">565173.663 5934673.06 565160.10095163 5934665.25797155 565146.693 5934657.194 
</gml:posList>
            </gml:ArcString>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">565146.693 5934657.194 565134.061117519 5934649.26307531 565121.578 5934641.1 
</gml:posList>
            </gml:ArcString>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">565121.578 5934641.1 565109.661717465 5934632.76679318 565098.117 5934623.926 
</gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_1a5d5f69-6030-418e-8c50-9beb655cd816">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565141.18 5934582.565</gml:lowerCorner>
          <gml:upperCorner>565167.346 5934605.732</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_C268AADF-81DC-45C7-9D6F-EA6E1100A5B8" srsName="EPSG:25832">
          <gml:posList>565155.344 5934582.565 565167.346 5934605.732 565141.18 5934604.701 
565155.344 5934582.565 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_c804c892-8e65-4be7-8416-f4e3db65a417">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565133.659 5934544.572</gml:lowerCorner>
          <gml:upperCorner>565133.659 5934544.572</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">14.35</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_B79F769D-D03B-483B-B81B-3C4DF8105BF1" srsName="EPSG:25832">
          <gml:pos>565133.659 5934544.572</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_f5255a2e-51f5-472b-82a7-c834187ddf9a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565122.961 5934559.556</gml:lowerCorner>
          <gml:upperCorner>565188.396 5934626.078</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">105.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0d135aa7-ebd4-487a-bfed-6c3b94473835" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c245b09b-2893-4014-bf33-b8ce363b1479" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_DD4F7752-4FE6-4762-AD50-5F2AED660D37" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565136.37 5934626.078 565122.961 5934617.865 565145.834 5934580.501 
565138.524 5934567.051 565152.363 5934559.556 565173.263 5934598.038 
565188.396 5934598.427 565187.991 5934614.164 565144.374 5934613.026 
565136.37 5934626.078 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">45.5</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0d135aa7-ebd4-487a-bfed-6c3b94473835">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565149.237 5934609.391</gml:lowerCorner>
          <gml:upperCorner>565149.237 5934609.391</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:bebauungsArt</xplan:art>
      <xplan:art>xplan:bauweise</xplan:art>
      <xplan:art>xplan:bauweiseText</xplan:art>
      <xplan:art>xplan:GFZ</xplan:art>
      <xplan:art>xplan:GFZmin</xplan:art>
      <xplan:art>xplan:GFZmax</xplan:art>
      <xplan:art>xplan:GFZ_Ausn</xplan:art>
      <xplan:art>xplan:GF</xplan:art>
      <xplan:art>xplan:GFmin</xplan:art>
      <xplan:art>xplan:GFmax</xplan:art>
      <xplan:art>xplan:GF_Ausn</xplan:art>
      <xplan:art>xplan:GRZ</xplan:art>
      <xplan:art>xplan:GRZmin</xplan:art>
      <xplan:art>xplan:GRZmax</xplan:art>
      <xplan:art>xplan:GRZ_Ausn</xplan:art>
      <xplan:art>xplan:GR</xplan:art>
      <xplan:art>xplan:GRmin</xplan:art>
      <xplan:art>xplan:GRmax</xplan:art>
      <xplan:art>xplan:GR_Ausn</xplan:art>
      <xplan:art>xplan:BMZ</xplan:art>
      <xplan:art>xplan:BMZ_Ausn</xplan:art>
      <xplan:art>xplan:BM</xplan:art>
      <xplan:art>xplan:BM_Ausn</xplan:art>
      <xplan:art>xplan:Z</xplan:art>
      <xplan:art>xplan:Zmin</xplan:art>
      <xplan:art>xplan:Zmax</xplan:art>
      <xplan:art>xplan:Z_Ausn</xplan:art>
      <xplan:art>xplan:Z_Dach</xplan:art>
      <xplan:art>xplan:Z_Staffel</xplan:art>
      <xplan:art>xplan:Zzwingend</xplan:art>
      <xplan:art>xplan:ZU</xplan:art>
      <xplan:art>xplan:ZUmin</xplan:art>
      <xplan:art>xplan:ZUmax</xplan:art>
      <xplan:art>xplan:ZU_Ausn</xplan:art>
      <xplan:art>xplan:ZUzwingend</xplan:art>
      <xplan:art>xplan:MaxZahlWohnungen</xplan:art>
      <xplan:art>xplan:Fmin</xplan:art>
      <xplan:art>xplan:Fmax</xplan:art>
      <xplan:art>xplan:Bmin</xplan:art>
      <xplan:art>xplan:Bmax</xplan:art>
      <xplan:art>xplan:Tmin</xplan:art>
      <xplan:art>xplan:Tmax</xplan:art>
      <xplan:art>xplan:dachgestaltung[1]/xplan:BP_Dachgestaltung/xplan:dachform</xplan:art>
      <xplan:art>xplan:dachgestaltung[1]/xplan:BP_Dachgestaltung/xplan:DNmin</xplan:art>
      <xplan:art>xplan:dachgestaltung[1]/xplan:BP_Dachgestaltung/xplan:DNmax</xplan:art>
      <xplan:art>xplan:dachgestaltung[1]/xplan:BP_Dachgestaltung/xplan:DN</xplan:art>
      <xplan:art>xplan:dachgestaltung[1]/xplan:BP_Dachgestaltung/xplan:DNzwingend</xplan:art>
      <xplan:art>xplan:MZspezial[1]</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f5255a2e-51f5-472b-82a7-c834187ddf9a" />
      <xplan:position>
        <gml:Point gml:id="Gml_CED7CB53-25A5-498A-AD37-0D264E8FC749" srsName="EPSG:25832">
          <gml:pos>565149.237 5934609.391</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c245b09b-2893-4014-bf33-b8ce363b1479">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565170.972 5934602.56</gml:lowerCorner>
          <gml:upperCorner>565170.972 5934602.56</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMin</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMax</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hZwingend</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderHoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderBezugspunkt</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f5255a2e-51f5-472b-82a7-c834187ddf9a" />
      <xplan:position>
        <gml:Point gml:id="Gml_ADDD54A6-0F9C-49C9-98CC-F1BF87E3D59F" srsName="EPSG:25832">
          <gml:pos>565170.972 5934602.56</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_56261e3a-d8f3-4fa1-940f-b7e614f35aef">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565064.743 5934544.677</gml:lowerCorner>
          <gml:upperCorner>565069.528 5934562.172</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_700D8B28-1C27-4325-9D9A-1484CA202240" srsName="EPSG:25832">
          <gml:posList>565064.743 5934544.677 565069.528 5934562.172 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_6684677c-066b-485a-8c89-7db721819c87">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565064.743 5934544.677</gml:lowerCorner>
          <gml:upperCorner>565069.528 5934562.172</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_F18EB813-CE85-46E4-8CE3-98FDEB447C38" srsName="EPSG:25832">
          <gml:posList>565064.743 5934544.677 565069.528 5934562.172 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_bf1e5a22-839d-4ef5-aaf3-3b4d97d2e963">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565044.153 5934543.752</gml:lowerCorner>
          <gml:upperCorner>565049.308 5934548.647</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">3.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_05108ce5-8c8d-4453-a291-6cbe7c7387f4" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_5554C3B8-891A-49CF-94E2-20058BCFEF60" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565049.308 5934543.973 565044.153 5934548.647 565044.377 5934543.752 
565049.308 5934543.973 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_05108ce5-8c8d-4453-a291-6cbe7c7387f4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565034.975 5934545.528</gml:lowerCorner>
          <gml:upperCorner>565034.975 5934545.528</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMin</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMax</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hZwingend</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderHoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderBezugspunkt</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bf1e5a22-839d-4ef5-aaf3-3b4d97d2e963" />
      <xplan:position>
        <gml:Point gml:id="Gml_B6B43DF4-D0CD-4B86-AD87-F6B89950A971" srsName="EPSG:25832">
          <gml:pos>565034.975 5934545.528</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_c57c2d59-3e07-455f-af8a-70e50607f2f1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565060.614 5934547.688</gml:lowerCorner>
          <gml:upperCorner>565141.637 5934632.802</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_073c07fd-6041-46ee-9271-69585acaf7a7" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_35090E0F-0590-4F77-9DBF-DCEBB1F9DBA4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565141.637 5934580.679 565109.71 5934632.802 565106.885 5934630.712 
565137.617 5934580.541 565130.836 5934567.561 565070.193 5934568.157 
565060.614 5934563.706 565062.089 5934560.532 565070.95 5934564.649 
565129.967 5934564.069 565130.715 5934547.688 565134.212 5934547.847 
565133.43 5934564.967 565141.637 5934580.679 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_65006077-7996-40d6-bb62-0bd5b81334de">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565109.827 5934568.52</gml:lowerCorner>
          <gml:upperCorner>565136.422 5934592.837</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_68E04EB0-FFCA-464C-8101-41E32B33628E" srsName="EPSG:25832">
          <gml:posList>565109.827 5934580.118 565113.89 5934574.371 565117.953 5934568.625 
565128.633 5934568.52 565136.422 5934582.493 565130.085 5934592.837 
565109.827 5934580.118 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_cc3f7613-8da2-45ab-8e7c-c6d74c15de60">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565123.905 5934547.377</gml:lowerCorner>
          <gml:upperCorner>565128.134 5934551.561</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">36.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a97811ae-add4-443d-9730-22929ea94313" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_9BF04021-A984-4602-84F8-53C44E554477" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565128.134 5934547.569 565127.964 5934551.561 565124.048 5934551.368 
565123.905 5934547.377 565128.134 5934547.569 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.8</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a97811ae-add4-443d-9730-22929ea94313">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565138.333 5934552.923</gml:lowerCorner>
          <gml:upperCorner>565138.333 5934552.923</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMin</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMax</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hZwingend</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderHoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderBezugspunkt</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_cc3f7613-8da2-45ab-8e7c-c6d74c15de60" />
      <xplan:position>
        <gml:Point gml:id="Gml_77477F04-1738-4C06-839E-82D30C8ACE55" srsName="EPSG:25832">
          <gml:pos>565138.333 5934552.923</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnittFlaeche gml:id="GML_d40b6338-c05d-48bc-b074-4f865f28bf13">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565066.208 5934550.034</gml:lowerCorner>
          <gml:upperCorner>565122.243 5934557.461</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(B)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0227c06e-c9c3-4d61-88cd-f57845c78844" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_0ad7d318-e915-42be-8739-1ce485872219" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4C7B8B21-4776-4433-8778-3DE8002CB430" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565087.655 5934551 565122.243 5934552.558 565122.153 5934554.545 
565122.064 5934556.531 565122.065 5934556.553 565091.715 5934557.065 
565068.24 5934557.461 565068.183 5934557.253 565067.141 5934553.446 
565066.208 5934550.034 565087.655 5934551 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextAbschnittFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0227c06e-c9c3-4d61-88cd-f57845c78844">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565086.209 5934553.331</gml:lowerCorner>
          <gml:upperCorner>565086.209 5934553.331</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:gliederung2</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d40b6338-c05d-48bc-b074-4f865f28bf13" />
      <xplan:position>
        <gml:Point gml:id="Gml_E9EF8CBB-1979-4031-8606-446BADC78E2F" srsName="EPSG:25832">
          <gml:pos>565086.209 5934553.331</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnittFlaeche gml:id="GML_2c65a70f-1649-4012-b70d-4b61ed3157be">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565141.18 5934582.565</gml:lowerCorner>
          <gml:upperCorner>565167.346 5934605.732</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3ba582d4-6de2-420b-bb0a-fd8ea26d551f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_90d196c6-7704-4ecc-a6d7-394f1c0478c4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BDD4CAEF-DB21-4DF8-900B-316613091DF6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565167.346 5934605.732 565141.18 5934604.701 565155.344 5934582.565 
565167.346 5934605.732 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextAbschnittFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3ba582d4-6de2-420b-bb0a-fd8ea26d551f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565155.708 5934600.194</gml:lowerCorner>
          <gml:upperCorner>565155.708 5934600.194</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:gliederung2</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_2c65a70f-1649-4012-b70d-4b61ed3157be" />
      <xplan:position>
        <gml:Point gml:id="Gml_EA1581A6-42AE-470C-8D82-34D02D71A338" srsName="EPSG:25832">
          <gml:pos>565155.708 5934600.194</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e3587eee-0394-4062-b606-3a197d395a39">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565125.267 5934558.29</gml:lowerCorner>
          <gml:upperCorner>565127.657 5934561.243</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">39.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c63d5ebe-b8d1-47ef-95e3-2cdb99dafe62" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_87e60ce7-8f61-4bc7-ad2b-72062166e775" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_BE017230-8F99-44E6-9D4C-28778902D061" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565127.657 5934558.322 565127.53 5934561.205 565125.267 5934561.243 
565125.308 5934558.29 565127.657 5934558.322 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.8</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c63d5ebe-b8d1-47ef-95e3-2cdb99dafe62">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565136.583 5934559.834</gml:lowerCorner>
          <gml:upperCorner>565136.583 5934559.834</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMin</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMax</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hZwingend</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderHoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderBezugspunkt</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e3587eee-0394-4062-b606-3a197d395a39" />
      <xplan:position>
        <gml:Point gml:id="Gml_86FA6096-48D0-44D4-9D03-7DADF18D106D" srsName="EPSG:25832">
          <gml:pos>565136.583 5934559.834</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_87e60ce7-8f61-4bc7-ad2b-72062166e775">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565113.041 5934571.627</gml:lowerCorner>
          <gml:upperCorner>565113.041 5934571.627</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:bebauungsArt</xplan:art>
      <xplan:art>xplan:bauweise</xplan:art>
      <xplan:art>xplan:bauweiseText</xplan:art>
      <xplan:art>xplan:GFZ</xplan:art>
      <xplan:art>xplan:GFZmin</xplan:art>
      <xplan:art>xplan:GFZmax</xplan:art>
      <xplan:art>xplan:GFZ_Ausn</xplan:art>
      <xplan:art>xplan:GF</xplan:art>
      <xplan:art>xplan:GFmin</xplan:art>
      <xplan:art>xplan:GFmax</xplan:art>
      <xplan:art>xplan:GF_Ausn</xplan:art>
      <xplan:art>xplan:GRZ</xplan:art>
      <xplan:art>xplan:GRZmin</xplan:art>
      <xplan:art>xplan:GRZmax</xplan:art>
      <xplan:art>xplan:GRZ_Ausn</xplan:art>
      <xplan:art>xplan:GR</xplan:art>
      <xplan:art>xplan:GRmin</xplan:art>
      <xplan:art>xplan:GRmax</xplan:art>
      <xplan:art>xplan:GR_Ausn</xplan:art>
      <xplan:art>xplan:BMZ</xplan:art>
      <xplan:art>xplan:BMZ_Ausn</xplan:art>
      <xplan:art>xplan:BM</xplan:art>
      <xplan:art>xplan:BM_Ausn</xplan:art>
      <xplan:art>xplan:Z</xplan:art>
      <xplan:art>xplan:Zmin</xplan:art>
      <xplan:art>xplan:Zmax</xplan:art>
      <xplan:art>xplan:Z_Ausn</xplan:art>
      <xplan:art>xplan:Z_Dach</xplan:art>
      <xplan:art>xplan:Z_Staffel</xplan:art>
      <xplan:art>xplan:Zzwingend</xplan:art>
      <xplan:art>xplan:ZU</xplan:art>
      <xplan:art>xplan:ZUmin</xplan:art>
      <xplan:art>xplan:ZUmax</xplan:art>
      <xplan:art>xplan:ZU_Ausn</xplan:art>
      <xplan:art>xplan:ZUzwingend</xplan:art>
      <xplan:art>xplan:MaxZahlWohnungen</xplan:art>
      <xplan:art>xplan:Fmin</xplan:art>
      <xplan:art>xplan:Fmax</xplan:art>
      <xplan:art>xplan:Bmin</xplan:art>
      <xplan:art>xplan:Bmax</xplan:art>
      <xplan:art>xplan:Tmin</xplan:art>
      <xplan:art>xplan:Tmax</xplan:art>
      <xplan:art>xplan:dachgestaltung[1]/xplan:BP_Dachgestaltung/xplan:dachform</xplan:art>
      <xplan:art>xplan:dachgestaltung[1]/xplan:BP_Dachgestaltung/xplan:DNmin</xplan:art>
      <xplan:art>xplan:dachgestaltung[1]/xplan:BP_Dachgestaltung/xplan:DNmax</xplan:art>
      <xplan:art>xplan:dachgestaltung[1]/xplan:BP_Dachgestaltung/xplan:DN</xplan:art>
      <xplan:art>xplan:dachgestaltung[1]/xplan:BP_Dachgestaltung/xplan:DNzwingend</xplan:art>
      <xplan:art>xplan:MZspezial[1]</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e3587eee-0394-4062-b606-3a197d395a39" />
      <xplan:position>
        <gml:Point gml:id="Gml_9ADA4950-626B-48A8-8AD8-FA1D99BFE77B" srsName="EPSG:25832">
          <gml:pos>565113.041 5934571.627</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnittFlaeche gml:id="GML_09ca07dc-e432-4d01-a9c1-16816f563962">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565109.827 5934568.52</gml:lowerCorner>
          <gml:upperCorner>565136.422 5934592.837</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_25c63b93-2cfa-4fa3-906d-ca0cc4c5799a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_b9b06b68-91d2-4b39-9bf8-bd42bf8490fb" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E1B971E0-847F-447E-9176-367882256155" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565136.422 5934582.493 565130.085 5934592.837 565109.827 5934580.118 
565113.89 5934574.371 565117.953 5934568.625 565128.633 5934568.52 
565136.422 5934582.493 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextAbschnittFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_25c63b93-2cfa-4fa3-906d-ca0cc4c5799a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565123.322 5934581.185</gml:lowerCorner>
          <gml:upperCorner>565123.322 5934581.185</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:gliederung2</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_09ca07dc-e432-4d01-a9c1-16816f563962" />
      <xplan:position>
        <gml:Point gml:id="Gml_C23C98C4-4EEF-4D7C-A2D7-14E327855214" srsName="EPSG:25832">
          <gml:pos>565123.322 5934581.185</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauLinie gml:id="GML_db2debbc-9beb-47a9-b35e-8c6ffb087a9a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565044.153 5934543.752</gml:lowerCorner>
          <gml:upperCorner>565049.308 5934548.647</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_253B7C19-4318-4E37-8043-4E77664FEBF0" srsName="EPSG:25832">
          <gml:posList>565044.153 5934548.647 565044.377 5934543.752 565049.308 5934543.973 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_0ca33bb0-185b-4d14-9359-41d6e9838701">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565049.308 5934543.973</gml:lowerCorner>
          <gml:upperCorner>565128.134 5934599.207</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_E7633367-5AEF-4FDC-9FB2-034BFD9F4D2D" srsName="EPSG:25832">
          <gml:posList>565049.308 5934543.973 565057.025 5934544.325 565064.743 5934544.677 
565070.403 5934544.937 565076.063 5934545.197 565084.15 5934545.565 
565106.142 5934546.567 565128.134 5934547.569 565127.857 5934554.086 
565127.684 5934557.72 565127.53 5934561.205 565106.642 5934561.553 
565085.754 5934561.901 565077.641 5934562.036 565069.528 5934562.172 
565068.07 5934562.196 565064.948 5934562.247 565061.826 5934562.297 
565063.218 5934563.904 565068.283 5934569.75 565073.347 5934575.596 
565078.412 5934581.443 565083.477 5934587.289 565088.647 5934593.248 
565093.816 5934599.207 565099.779 5934589.684 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_5f5fc8e2-f0d6-4132-9774-5dd43f52014a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565128.134 5934547.57</gml:lowerCorner>
          <gml:upperCorner>565182.815 5934562.41</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_6D1719A9-8717-4D9E-AA6C-5C18F759EA1C" srsName="EPSG:25832">
          <gml:posList>565128.134 5934547.57 565128.154 5934547.571 565182.655 5934550.058 
565182.815 5934550.065 565182.337 5934562.41 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_218233aa-c960-4155-a0eb-ea5eee23b45f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565053.066 5934549.442</gml:lowerCorner>
          <gml:upperCorner>565122.243 5934557.577</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4BC44258-DEBF-432F-9C16-AB32406317FC" srsName="EPSG:25832">
          <gml:posList>565053.066 5934549.442 565061.337 5934557.577 565064.788 5934557.519 
565068.24 5934557.461 565079.977 5934557.263 565091.715 5934557.065 
565106.89 5934556.809 565122.065 5934556.553 565122.153 5934554.545 
565122.243 5934552.558 565104.949 5934551.779 565087.655 5934551 
565076.931 5934550.517 565059.637 5934549.738 565053.066 5934549.442 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_03835fde-b640-43d4-9ca1-661f64c8458f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565044.994 5934579.246</gml:lowerCorner>
          <gml:upperCorner>565044.994 5934579.246</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">16.3</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_08099379-FBB2-4DCD-9B44-3A302AAEFA64" srsName="EPSG:25832">
          <gml:pos>565044.994 5934579.246</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7fbf04dc-cda8-4acd-831a-31d6327e2e27">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565093.816 5934589.684</gml:lowerCorner>
          <gml:upperCorner>565102.846 5934601.092</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">49.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3d97cdff-abfb-4d58-ae33-3c337440e946" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_79616D09-56A3-4074-8D0F-63CAE22ED856" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565102.846 5934591.688 565096.701 5934601.092 565093.816 5934599.207 
565099.779 5934589.684 565102.846 5934591.688 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.8</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3d97cdff-abfb-4d58-ae33-3c337440e946">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565088.647 5934593.248</gml:lowerCorner>
          <gml:upperCorner>565088.647 5934593.248</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMin</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMax</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hZwingend</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderHoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderBezugspunkt</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7fbf04dc-cda8-4acd-831a-31d6327e2e27" />
      <xplan:position>
        <gml:Point gml:id="Gml_7DD7BF22-46A3-4C54-A547-38DCCDA1DBB4" srsName="EPSG:25832">
          <gml:pos>565088.647 5934593.248</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Denkmalschutzrecht gml:id="GML_ad476a3d-d0c4-464d-87b3-da973ee4b9f0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565171.294 5934621.476</gml:lowerCorner>
          <gml:upperCorner>565171.294 5934621.476</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Dammtorwall 15, vor Nr. 15, Valentinskamp 70, Unilever-Haus, Ensemble Dammtorwall 15 (Unilever-Haus), Gebäude mit Außenanlagen und Flügelstern-Plastik</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_537AE632-9411-4124-B37E-A044C5734621" srsName="EPSG:25832">
          <gml:pos>565171.294 5934621.476</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
      <xplan:nummer>29977</xplan:nummer>
    </xplan:SO_Denkmalschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_d1a4521b-59c9-4321-acd4-3f227535c90d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565042.904 5934543.973</gml:lowerCorner>
          <gml:upperCorner>565188.889 5934673.06</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9db49e76-0ff9-4057-89e8-9e677146a085" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0284b4a3-f97a-4479-a85a-b09c10bd1b73" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c180d211-33e9-4580-8e8f-f58eadca8f8a" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D6A25108-DE9F-4EE3-8C14-84E95DBB5C9E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7B940C56-CBFD-46A9-B35A-ECD5183E97D5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565064.743 5934544.677 565128.154 5934547.571 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_90752C51-7868-480A-978B-94E71CCF656E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565128.154 5934547.571 565182.655 5934550.058 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8DEFD36B-35C1-4392-9358-7B7510999F3F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565182.655 5934550.058 565182.204 5934567.575 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C12B8557-647F-451B-81E9-CF95871F57AF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565182.204 5934567.575 565188.889 5934579.296 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BC3E8445-EF7F-46B2-A421-2FB2C635212F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565188.889 5934579.296 565186.667 5934665.599 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4986AA7A-9E2B-4421-B3BB-51C70126A861" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565186.667 5934665.599 565173.663 5934673.06 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_16508E11-53FD-4AF4-8AE6-F8D9B8BFCFDE" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">565173.663 5934673.06 565160.10095163 5934665.25797155 565146.693 5934657.194 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_B46B8230-9E54-4CEE-90C3-98ED51EB7516" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">565146.693 5934657.194 565134.061117519 5934649.26307531 565121.578 5934641.1 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_60E88EAD-F35C-47DA-93B8-25903C3F4AF8" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">565121.578 5934641.1 565095.240462892 5934621.59637909 565070.928 5934599.62 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_42169AC3-D701-4CDF-8B3C-9DDA1A173042" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565070.928 5934599.62 565046.792 5934575.569 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EEC8320F-646B-4FEF-91BA-C03CF4D0412E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565046.792 5934575.569 565042.904 5934571.727 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4622BB4C-816F-4218-A04B-486A4CF8FFCA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565042.904 5934571.727 565043.202 5934566.241 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B2AC1C70-5025-4DEE-B551-EC20BEF43D38" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565043.202 5934566.241 565044.128 5934548.669 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9FB82244-6865-44CE-B32C-A3B4B382418D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565044.128 5934548.669 565049.308 5934543.973 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4D1EBEF6-D3AB-4253-A03D-1EAD73B455A7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">565049.308 5934543.973 565064.743 5934544.677 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1600</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9db49e76-0ff9-4057-89e8-9e677146a085">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565146.134 5934629.132</gml:lowerCorner>
          <gml:upperCorner>565146.134 5934629.132</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GRZ</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d1a4521b-59c9-4321-acd4-3f227535c90d" />
      <xplan:position>
        <gml:Point gml:id="Gml_CAEF0BF2-67B0-4381-BFE3-18887B0123D4" srsName="EPSG:25832">
          <gml:pos>565146.134 5934629.132</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0284b4a3-f97a-4479-a85a-b09c10bd1b73">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565146.585 5934635.47</gml:lowerCorner>
          <gml:upperCorner>565146.585 5934635.47</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d1a4521b-59c9-4321-acd4-3f227535c90d" />
      <xplan:position>
        <gml:Point gml:id="Gml_670081FD-CBF4-41C7-AE2C-51C0275A8CDC" srsName="EPSG:25832">
          <gml:pos>565146.585 5934635.47</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnittFlaeche gml:id="GML_33328a77-91a6-4864-b8a2-c52eab49bcac">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565048.033 5934563.32</gml:lowerCorner>
          <gml:upperCorner>565111.833 5934615.32</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(B)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_bb877354-91fc-4ee7-8255-d91292e4e374" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_0ad7d318-e915-42be-8739-1ce485872219" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6AE51472-711C-4991-9BD2-07AD07F4AA60" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565097.033 5934615.32 565072.533 5934592.27 565048.033 5934569.22 
565057.233 5934563.32 565074.833 5934583.62 565092.433 5934603.92 
565099.633 5934596.32 565106.833 5934588.72 565111.833 5934591.72 
565104.433 5934603.52 565097.033 5934615.32 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextAbschnittFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_bb877354-91fc-4ee7-8255-d91292e4e374">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565077.28 5934592.501</gml:lowerCorner>
          <gml:upperCorner>565077.28 5934592.501</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:gliederung2</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_33328a77-91a6-4864-b8a2-c52eab49bcac" />
      <xplan:position>
        <gml:Point gml:id="Gml_C0C5D19C-6872-4F9E-852F-51099C630146" srsName="EPSG:25832">
          <gml:pos>565077.28 5934592.501</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_1b73e286-945e-4f67-b58b-0763bc4c1053">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565056.978 5934530.365</gml:lowerCorner>
          <gml:upperCorner>565201.299 5934564.753</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_8C5F3C51-2247-48F2-9118-B48943E52576" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565187.273 5934564.753 565182.797 5934564.705 565182.819 5934546.68 
565169.258 5934546.02 565169.496 5934541.121 565167.011 5934540.908 
565166.691 5934541.469 565153.842 5934540.19 565129.213 5934537.692 
565104.916 5934535.228 565088.957 5934533.609 565056.978 5934530.365 
565080.13 5934530.915 565117.017 5934531.791 565141.33 5934533.036 
565201.299 5934536.107 565201.111 5934547.423 565197.292 5934547.242 
565187.92 5934546.799 565187.273 5934564.753 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_10cdd180-6bfc-4872-8199-5869396827ea">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565042.904 5934566.311</gml:lowerCorner>
          <gml:upperCorner>565046.414 5934575.195</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">44</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b68d8a04-fcb4-4fe0-ba1d-7c622be61da9" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_0339477B-A2E7-4D1D-A1EE-905926B6CD31" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565046.414 5934575.195 565042.904 5934571.727 565043.227 5934566.311 
565046.414 5934575.195 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.8</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b68d8a04-fcb4-4fe0-ba1d-7c622be61da9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565033.382 5934571.458</gml:lowerCorner>
          <gml:upperCorner>565033.382 5934571.458</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMin</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMax</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hZwingend</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderHoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderBezugspunkt</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_10cdd180-6bfc-4872-8199-5869396827ea" />
      <xplan:position>
        <gml:Point gml:id="Gml_70D29710-B31F-4DF5-9A2D-97C8960CF183" srsName="EPSG:25832">
          <gml:pos>565033.382 5934571.458</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_25f5c07b-1409-439e-8cea-2b89439f782e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564984.593 5934518.449</gml:lowerCorner>
          <gml:upperCorner>565042.876 5934528.943</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_F289C0EF-8977-4CB2-805A-F28E49F8595E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565013.367 5934527.703 565011.919 5934527.718 565009.463 5934527.761 
565006.917 5934527.826 565004.315 5934527.915 565000.321 5934528.097 
564996.013 5934528.353 564992.579 5934528.602 564989.968 5934528.819 
564988.62 5934528.943 564984.593 5934524.512 564984.913 5934518.449 
565013.555 5934519.141 565042.876 5934519.886 565042.773 5934527.073 
565042.659 5934528.913 565041.998 5934528.846 565040.528 5934528.72 
565036.622 5934528.42 565034.26 5934528.264 565031.694 5934528.116 
565029.575 5934528.01 565027.375 5934527.917 565024.628 5934527.823 
565022.096 5934527.759 565018.04 5934527.701 565015.289 5934527.693 
565013.367 5934527.703 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_523a00ad-6533-4e60-b38a-06be2c3623d8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565096.074 5934588.957</gml:lowerCorner>
          <gml:upperCorner>565119.138 5934623.909</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">51</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1ca60951-ec86-4875-b260-fbbe77da4968" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_03DF98B3-C4A5-47E3-9104-AC35EAFAD947" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565119.138 5934590.44 565108.893 5934606.761 565098.128 5934623.909 
565096.074 5934621.977 565116.773 5934588.957 565119.138 5934590.44 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.8</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1ca60951-ec86-4875-b260-fbbe77da4968">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565108.893 5934606.761</gml:lowerCorner>
          <gml:upperCorner>565108.893 5934606.761</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMin</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hMax</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hZwingend</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderHoehenbezug</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:abweichenderBezugspunkt</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_523a00ad-6533-4e60-b38a-06be2c3623d8" />
      <xplan:position>
        <gml:Point gml:id="Gml_B9015B79-C2AF-4CE4-95E0-2E05869F842D" srsName="EPSG:25832">
          <gml:pos>565108.893 5934606.761</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_56418f5b-3769-4c8b-8951-9ed4eba0c60b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565119.139 5934610.724</gml:lowerCorner>
          <gml:upperCorner>565119.139 5934610.724</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:schriftinhalt>3,5</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_5FC80D4F-78CF-4A13-B0FA-EE73D8400AEE" srsName="EPSG:25832">
          <gml:pos>565119.139 5934610.724</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">32.2</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_b37a15fb-1e04-4e1a-b065-e8e59009cce9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565130.305 5934553.439</gml:lowerCorner>
          <gml:upperCorner>565130.305 5934553.439</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:schriftinhalt>3,5</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_25ACA7E5-5453-4DBB-B8D4-A3D42DA5361D" srsName="EPSG:25832">
          <gml:pos>565130.305 5934553.439</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_e56bff69-5ddb-489a-af95-5c5033272577">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565191.879 5934572.227</gml:lowerCorner>
          <gml:upperCorner>565191.879 5934572.227</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:schriftinhalt>E</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_A7BD22C1-0BED-4F26-BFFC-152A7A4C999A" srsName="EPSG:25832">
          <gml:pos>565191.879 5934572.227</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_dd06a4de-f527-4d88-a87d-bb807bc51497">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565091.472 5934568.124</gml:lowerCorner>
          <gml:upperCorner>565091.472 5934568.124</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:schriftinhalt>3,5</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_9D742A9F-9A84-4915-89C8-8AE8A1E0808A" srsName="EPSG:25832">
          <gml:pos>565091.472 5934568.124</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">270</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_198bf623-4f2f-4aaf-a21c-33a1b089d4e3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565056.811 5934538.186</gml:lowerCorner>
          <gml:upperCorner>565056.811 5934538.186</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:schriftinhalt>Gehwegüberfahrten nicht zugelassen</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_A1BD273A-7165-4762-AC41-04694CD3E69F" srsName="EPSG:25832">
          <gml:pos>565056.811 5934538.186</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">2.89</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="Gml_FBDF41B7-DCE0-4CF8-B2F3-E8C4105C197E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565219.559 5934584.965</gml:lowerCorner>
          <gml:upperCorner>565350.954 5934631.11</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>2</xplan:nummer>
      <xplan:bedeutung>9999</xplan:bedeutung>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_6BB8DCD8-8BE2-4CED-BFD9-453580C4D23C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565219.559 5934631.11 565220.645 5934589.001 565229.16 5934588.891 
565230.79 5934588.87 565232.212 5934588.852 565232.261 5934587.773 
565234.658 5934587.883 565240.136 5934587.846 565292.886 5934586.151 
565331.716 5934584.965 565338.302 5934585.758 565343.515 5934586.385 
565350.556 5934586.08 565350.775 5934586.071 565350.954 5934615.352 
565350.889 5934620.29 565350.893 5934629.344 565324.776 5934629.839 
565301.893 5934630.273 565219.559 5934631.11 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
	  <xplan:planinhalt xlink:href="#Gml_775C7DC3-0EC1-4BF5-A2BF-E529847C264C" />
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:gehoertZuPlan xlink:href="#GML_b7e8fcfa-cecb-418c-904a-bb5e135d8bcd" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_775C7DC3-0EC1-4BF5-A2BF-E529847C264C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565250.6456 5934595.4435</gml:lowerCorner>
          <gml:upperCorner>565288.5347 5934616.9567</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_FBDF41B7-DCE0-4CF8-B2F3-E8C4105C197E" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_B307F2C0-3F04-40D2-8FB0-47C3D82EE027" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">565250.6456 5934616.626 565250.8315 5934595.4435 565288.5347 5934595.7742 
565288.3488 5934616.9567 565250.6456 5934616.626 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
</xplan:XPlanAuszug>