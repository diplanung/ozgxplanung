﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<gml:Polygon xmlns:gml="http://www.opengis.net/gml/3.2" srsName="EPSG:25832"
             gml:id="GML_21ed2a37-63d0-44ac-a488-837d8e124f97">
  <gml:exterior>
    <gml:Ring>
      <gml:curveMember>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_28e736a5-133d-4976-a065-f476fd25f2e4">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">567949.311 5932242.934 567986.595 5932231.130 568022.835
                5932216.434
              </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="3">568022.835 5932216.434 568024.111 5932213.030 568035.436
                5932210.759
              </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="7">568035.436 5932210.759 568055.464 5932199.976 568074.968
                5932188.272 568081.473 5932196.032 568089.377 5932202.362 568079.852 5932215.400 568070.696 5932228.700
              </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">568070.696 5932228.700 568076.175 5932240.598</gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">568076.175 5932240.598 568068.087 5932253.352 568060.751
                5932266.553
              </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="16">568060.751 5932266.553 568052.061 5932267.716 568051.093
                5932268.291 568049.705 5932269.621 568049.104 5932270.317 568048.406 5932271.191 568046.598 5932273.706
                568045.388 5932275.541 568043.853 5932278.001 568041.817 5932281.443 568038.967 5932286.526 568034.930
                5932294.128 568029.953 5932304.016 568024.328 5932315.881 568017.058 5932332.443 568009.189 5932352.276
              </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">568009.189 5932352.276 567991.377 5932340.969 567971.751
                5932333.227
              </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567971.751 5932333.227 567971.605 5932328.614</gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">567971.605 5932328.614 567962.303 5932285.294 567949.311
                5932242.934
              </gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </gml:curveMember>
    </gml:Ring>
  </gml:exterior>
  <gml:interior>
    <gml:Ring>
      <gml:curveMember>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_0ed5faed-fcf4-48a0-ab38-6248773696cc">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="11">568074.760 5932189.290 568015.548 5932221.136 567952.531
                5932244.567 567965.844 5932286.734 567975.251 5932329.940 567993.038 5932337.048 568009.293 5932347.181
                568043.592 5932272.022 568088.299 5932202.548 568080.921 5932196.541 568074.760 5932189.290
              </gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </gml:curveMember>
    </gml:Ring>
  </gml:interior>
</gml:Polygon>
