﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_5992CAF9-FC15-4810-B9B1-75A98196D6F7" xsi:schemaLocation="http://www.xplanung.de/xplangml/6/0 http://repository.gdi-de.org/schemas/de.xleitstelle.xplanung/6.0/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/6/0">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>557094.5259 5936699.1238</gml:lowerCorner>
      <gml:upperCorner>557245.9037 5936833.7776</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="Gml_CD6556C4-8BDB-43DA-821A-66FAE8B17BBA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557094.5259 5936699.1238</gml:lowerCorner>
          <gml:upperCorner>557245.9037 5936833.7776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan_6-0_Punkt_ausserhalb</xplan:name>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_01CA3F9F-11C8-422F-A353-22029AC36D73" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">557096.392 5936828.69 557094.5259 5936699.1238 557245.9037 5936699.9855 
557243.4354 5936833.7776 557096.392 5936828.69 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:bereich xlink:href="#Gml_432F3BD2-639E-4A0F-826E-53D45A7209C6" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="Gml_432F3BD2-639E-4A0F-826E-53D45A7209C6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557094.5259 5936699.1238</gml:lowerCorner>
          <gml:upperCorner>557245.9037 5936833.7776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_BF70BD41-4AD6-4146-BC50-CA4C2E927689" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">557096.392 5936828.69 557094.5259 5936699.1238 557245.9037 5936699.9855 
557243.4354 5936833.7776 557096.392 5936828.69 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#Gml_7AC61FB9-43DD-45BE-B01F-8F443B5F303F" />
      <xplan:planinhalt xlink:href="#Gml_86CEE2F5-F5F9-4F06-962F-33469C2ACB5D" />
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:gehoertZuPlan xlink:href="#Gml_CD6556C4-8BDB-43DA-821A-66FAE8B17BBA" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_7AC61FB9-43DD-45BE-B01F-8F443B5F303F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557094.5259 5936699.1238</gml:lowerCorner>
          <gml:upperCorner>557245.9037 5936833.7776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_432F3BD2-639E-4A0F-826E-53D45A7209C6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_A5CEF308-F8B1-45F1-9FA1-498CCCCB4CFA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">557096.392 5936828.69 557094.5259 5936699.1238 557245.9037 5936699.9855 
557243.4354 5936833.7776 557096.392 5936828.69 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_86CEE2F5-F5F9-4F06-962F-33469C2ACB5D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>557066.0319 5936740.9087</gml:lowerCorner>
          <gml:upperCorner>557066.0319 5936740.9087</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_432F3BD2-639E-4A0F-826E-53D45A7209C6" />
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_2F3641E1-1FFC-41A7-A79B-069F5BDC1D26" srsName="EPSG:25832">
          <gml:pos>557066.0319 5936740.9087</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
</xplan:XPlanAuszug>