﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_7B3783C1-0822-41E9-B1B6-C7AF6175FB4B" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/2 http://www.xplanungwiki.de/upload/XPlanGML/5.2/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/5/2">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>572646.058 5938105.275</gml:lowerCorner>
      <gml:upperCorner>572788.184 5938291.4</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_2fe3fb2c-ca10-4fc1-b905-dd176715b0d9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572646.058 5938105.275</gml:lowerCorner>
          <gml:upperCorner>572788.184 5938291.4</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>xplan51_Flaechenschlussfehler</xplan:name>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_FC9D5132-B307-4C5E-BD4C-42A8F492680C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572776.622 5938131.44 572787.357 5938141.203 572788.184 5938148.111 
572784.759 5938158.529 572781.444 5938166.705 572780.074 5938170.523 
572776.892 5938182.467 572741.086 5938291.4 572733.983 5938289.072 
572732.119 5938288.462 572722.954 5938285.458 572717.173 5938283.564 
572701.186 5938278.324 572694.002 5938275.97 572684.616 5938272.895 
572663.628 5938266.018 572647.371 5938260.689 572646.058 5938260.26 
572663.977 5938205.789 572672.515 5938179.784 572696.972 5938105.275 
572776.622 5938131.44 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:bereich xlink:href="#GML_8e737511-c592-4e59-8e5c-80cd081aba51" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_8e737511-c592-4e59-8e5c-80cd081aba51">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572646.058 5938105.275</gml:lowerCorner>
          <gml:upperCorner>572788.184 5938291.4</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>1</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_E18B1D19-17E0-4FD0-97FC-B2B31AF0E491" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572776.622 5938131.44 572787.357 5938141.203 572788.184 5938148.111 
572784.759 5938158.529 572781.444 5938166.705 572780.074 5938170.523 
572776.892 5938182.467 572760.761 5938231.533 572741.086 5938291.4 
572733.983 5938289.072 572732.119 5938288.462 572722.954 5938285.458 
572717.173 5938283.564 572701.186 5938278.324 572694.002 5938275.97 
572684.616 5938272.895 572663.628 5938266.018 572647.371 5938260.689 
572646.058 5938260.26 572663.977 5938205.789 572665.716 5938200.501 
572672.515 5938179.784 572696.972 5938105.275 572776.622 5938131.44 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#GML_4c5557e0-2015-4ccf-a87a-9d6ef125c8d2" />
      <xplan:planinhalt xlink:href="#GML_81f50166-1fa8-4186-9fae-e026fc8b3a22" />
      <xplan:praesentationsobjekt xlink:href="#GML_ba378cfa-3a91-472f-92e6-58d050c84464" />
      <xplan:gehoertZuPlan xlink:href="#GML_2fe3fb2c-ca10-4fc1-b905-dd176715b0d9" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_4c5557e0-2015-4ccf-a87a-9d6ef125c8d2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572646.058 5938200.501</gml:lowerCorner>
          <gml:upperCorner>572760.761 5938291.4</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_8e737511-c592-4e59-8e5c-80cd081aba51" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_ba378cfa-3a91-472f-92e6-58d050c84464" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_19698EC6-23B9-4D94-8ED0-A900DC2EF31F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572667.046 5938200.935 572683.287 5938206.239 572703.5953 5938215.9377
572713.882 5938216.227 572742.307 5938225.508 572751.826 5938228.615 
572753.251 5938229.081 572760.761 5938231.533 572755.21 5938248.4235 
572741.086 5938291.4 572733.983 5938289.072 572732.119 5938288.462 
572722.954 5938285.458 572717.173 5938283.564 572701.186 5938278.324 
572694.002 5938275.97 572684.616 5938272.895 572663.628 5938266.018 
572647.371 5938260.689 572646.058 5938260.26 572662.528 5938214.726 
572663.977 5938205.789 572665.716 5938200.501 572667.046 5938200.935 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ba378cfa-3a91-472f-92e6-58d050c84464">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572720.488 5938251.511</gml:lowerCorner>
          <gml:upperCorner>572720.488 5938251.511</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_8e737511-c592-4e59-8e5c-80cd081aba51" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_4c5557e0-2015-4ccf-a87a-9d6ef125c8d2" />
      <xplan:position>
        <gml:Point gml:id="Gml_08D11899-E976-45EF-93D9-C14DF3C95B28" srsName="EPSG:25832">
          <gml:pos>572720.488 5938251.511</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_81f50166-1fa8-4186-9fae-e026fc8b3a22">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>572665.716 5938105.275</gml:lowerCorner>
          <gml:upperCorner>572788.184 5938231.533</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_8e737511-c592-4e59-8e5c-80cd081aba51" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_3772E426-1A13-43D8-BE31-D6D6A17ACEF2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">572776.622 5938131.44 572787.357 5938141.203 572788.184 5938148.111 
572784.759 5938158.529 572781.444 5938166.705 572780.074 5938170.523 
572776.892 5938182.467 572760.761 5938231.533 572753.251 5938229.081 
572751.826 5938228.615 572742.307 5938225.508 572713.882 5938216.227 
572683.287 5938206.239 572667.046 5938200.935 572665.716 5938200.501 
572672.515 5938179.784 572696.972 5938105.275 572776.622 5938131.44 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
</xplan:XPlanAuszug>
