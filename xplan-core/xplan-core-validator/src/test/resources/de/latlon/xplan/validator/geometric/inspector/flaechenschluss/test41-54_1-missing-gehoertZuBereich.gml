<?xml version="1.0" ?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs/2.0" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xplan="http://www.xplanung.de/xplangml/5/4" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:fes="http://www.opengis.net/fes/2.0" xmlns:ows="http://www.opengis.net/ows/1.1" xmlns:hfp="http://www.w3.org/2001/XMLSchema-hasFacetAndProperty" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" gml:id="_3699fc59-30bd-4c3e-b6cd-e5e46699ab49" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/4 https://repository.gdi-de.org/schemas/de.xleitstelle.xplanung/5.4/XPlanung-Operationen.xsd">
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_ca345e0b-f841-411d-8983-4a72343e6cd1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566849.469 5932728.094</gml:lowerCorner>
          <gml:upperCorner>567498.31 5932859.222</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_acacb149-69a5-43c2-8acc-4c1e44c620fc" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>566849.469 5932859.222 566896.825 5932852.378 566894.962 5932839.469 566913.019 5932836.466 566918.283 5932835.601 566931.081 5932833.497 566932.117 5932839.756 566918.207 5932842.058 566904.297 5932844.361 566905.266 5932851.083 566916.768 5932849.21 566919.425 5932849.281 566968.153 5932840.52 566980.426 5932838.241 566987.76 5932836.783 567057.602 5932824.015 567127.727 5932812.902 567173.834 5932806.143 567206.34 5932801.096 567210.643 5932800.367 567232.728 5932796.1 567254.637 5932791.004 567270.059 5932786.989 567293.738 5932780.36 567328.582 5932770.627 567361.266 5932761.734 567386.531 5932755.262 567391.412 5932754.053 567403.33 5932751.144 567403.33 5932751.144 567437.692 5932742.804 567473.816 5932734.037 567498.31 5932728.094</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_303e9eb8-189e-485e-b658-913d8dd568f6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566942.034 5932743.169</gml:lowerCorner>
          <gml:upperCorner>567509.287 5932905.509</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="_97a54b0b-37f5-414a-8cc0-7fc8cc806aaa" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567071.225 5932875.978 566952.478 5932905.509 566942.034 5932887.077 567047.196 5932861.151 567122.404 5932841.663 567163.038 5932830.198 567363.553 5932775.056 567390.254 5932768.247 567485.287 5932746.96 567502.35 5932743.169 567509.287 5932769.105 567416.044 5932790.244 567354.506 5932803.777 567322.897 5932810.727 567243.981 5932831.427 567071.225 5932875.978</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_c0a8d0b0-6902-46d0-b3d5-c220e29edc97">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566930.194 5932737.377</gml:lowerCorner>
          <gml:upperCorner>567500.798 5932866.541</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_dd055a90-ed39-4d69-9633-f439702a53c2" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567500.798 5932737.377 567446.666 5932750.517 567434.247 5932753.531 567393.238 5932763.716 567365.696 5932770.815 567297.461 5932789.308 567270.403 5932796.898 567224.067 5932809.859 567224.066 5932809.858 567177.219 5932822.041 567141.654 5932830.401 567119.92 5932835.137 567056.168 5932847.424 567014.704 5932854.187 566973.112 5932860.188 566930.194 5932866.541</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_82d36cb7-9ef4-4f3a-b7f0-694ffaa9dcc4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567181.156 5932731.601</gml:lowerCorner>
          <gml:upperCorner>567245.957 5932781.173</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_dd0e91e3-d682-45d5-ba52-b356694e0210" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567202.283 5932764.51 567229.869 5932757.348 567224.591 5932737.022 567237.174 5932733.755 567245.957 5932767.583 567193.176 5932781.173 567181.156 5932734.868 567193.739 5932731.601 567202.283 5932764.51</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_625ce698-8502-4018-ace9-a65c622f4c02">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567100.317 5932741.64</gml:lowerCorner>
          <gml:upperCorner>567155.075 5932802.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_63214469-0c6b-4e4e-bf22-04164de5032d" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567149.128 5932791.971 567103.525 5932802.817 567100.317 5932789.338 567113.013 5932786.154 567137.502 5932780.326 567141.581 5932745.143 567155.075 5932741.64 567154.527 5932745.398 567149.128 5932791.971</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_55bfc4f0-7895-42aa-8497-417dd95d70d9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567108.409 5932752.067</gml:lowerCorner>
          <gml:upperCorner>567140.779 5932786.174</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_d3e2c632-f082-4c16-9fb6-6ec617ccb641" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567137.502 5932780.326 567112.927 5932786.174 567108.409 5932767.187 567120.277 5932764.106 567118.644 5932757.814 567140.779 5932752.067 567137.502 5932780.326</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_bc4f775e-aebc-4cc0-a5f0-84eef160b876">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566938.222 5932794.066</gml:lowerCorner>
          <gml:upperCorner>566998.078 5932837.394</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_7d0747a0-34d3-4d5d-a7d5-3352a4ab4bae" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>566998.078 5932827.167 566944.546 5932837.394 566938.222 5932804.293 566950.991 5932801.853 566954.688 5932821.203 566982.682 5932815.855 566978.985 5932796.505 566991.754 5932794.066 566995.451 5932813.416 566998.078 5932827.167</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ca655909-b5cd-4dc4-9fb5-0145ec5a3724">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567377.878 5932674.7</gml:lowerCorner>
          <gml:upperCorner>567405.389 5932693.724</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_b7ce7305-cbee-4b2c-9215-fadfb20bb00b" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567405.389 5932687.258 567393.798 5932690.361 567381.24 5932693.724 567377.878 5932681.166 567402.027 5932674.7 567405.389 5932687.258</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ee877546-6cf7-4e4d-bcd2-7a419263dc61">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566916.859 5932822.166</gml:lowerCorner>
          <gml:upperCorner>566944.546 5932841.614</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_1da107e9-7c17-4e85-a9f7-4b367e392eff" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>566944.546 5932837.394 566939.864 5932838.193 566919.2 5932841.614 566916.859 5932826.266 566941.637 5932822.166 566944.546 5932837.394</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_8341fc3c-a7c7-440d-9be7-99259d8c216e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566976.546 5932778.857</gml:lowerCorner>
          <gml:upperCorner>567004.523 5932796.505</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_3b6e8e8f-a6c6-4fb1-b298-2ce9ef992fca" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567004.523 5932791.626 566991.754 5932794.066 566978.985 5932796.505 566976.546 5932783.737 567002.084 5932778.857 567004.523 5932791.626</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_c45e5acb-2fab-43f3-880d-30526f4c94eb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567266.628 5932696.257</gml:lowerCorner>
          <gml:upperCorner>567325.501 5932761.347</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_364ce250-9379-407a-b3a0-cc61aee9fdfa" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567322.893 5932747.553 567270.174 5932761.347 567266.628 5932747.803 567279.2 5932744.514 567306.772 5932737.3 567297.868 5932703.269 567321.864 5932696.257 567325.501 5932708.738 567313.643 5932712.203 567319.349 5932734.009 567322.893 5932747.553</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_798d558b-73d7-416d-8862-237cb57c9540">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567242.452 5932747.803</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932767.583</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_573a0589-9f28-48a6-aea6-543689ec0d61" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567270.174 5932761.347 567245.957 5932767.583 567242.452 5932754.081 567266.628 5932747.803 567270.174 5932761.347</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6f1f3557-da54-47ca-89d0-248d8b9c76ae">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567417.947 5932680.533</gml:lowerCorner>
          <gml:upperCorner>567436.453 5932706.113</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_f7c8f463-d65f-470c-a1dc-e577d299a926" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567436.453 5932702.751 567423.896 5932706.113 567417.947 5932683.896 567430.505 5932680.533 567436.453 5932702.751</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f750899c-e632-46e7-9864-f2403dfdf334">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567261.31 5932723.846</gml:lowerCorner>
          <gml:upperCorner>567279.2 5932747.803</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_edd46efc-b064-47b6-accd-2eec5f8a6f27" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567279.2 5932744.514 567266.628 5932747.803 567261.31 5932727.488 567273.793 5932723.846 567279.2 5932744.514</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_32ac26a6-25b6-472a-9bd0-095fbb07bdf6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566935.783 5932786.645</gml:lowerCorner>
          <gml:upperCorner>566963.761 5932804.293</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_eb7e9a68-bacd-41cc-86a2-40c3eff96229" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>566963.761 5932799.414 566950.991 5932801.853 566938.222 5932804.293 566935.783 5932791.524 566961.321 5932786.645 566963.761 5932799.414</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f0acca72-d01e-4a95-ba82-db3675ed653b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567423.896 5932654.275</gml:lowerCorner>
          <gml:upperCorner>567481.725 5932719.637</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_646d155e-501e-4fab-8cec-a267e0885ffb" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567427.517 5932719.637 567423.896 5932706.113 567436.453 5932702.751 567463.984 5932695.38 567454.673 5932660.605 567478.32 5932654.275 567481.725 5932666.819 567470.593 5932669.8 567480.162 5932705.541 567427.517 5932719.637</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_288452c4-3611-4e09-84c0-2029abab6a39">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567092.822 5932751.558</gml:lowerCorner>
          <gml:upperCorner>567120.277 5932770.455</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_0f26750d-5980-4c1f-b09e-32c74c953cdc" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567120.277 5932764.106 567108.409 5932767.187 567095.823 5932770.455 567092.822 5932757.841 567117.019 5932751.558 567120.277 5932764.106</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_992fe65b-4d1b-41a4-834d-49236d0b9d11">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566876.538 5932813.399</gml:lowerCorner>
          <gml:upperCorner>566904.004 5932832.072</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_263749b0-4fda-4fab-8847-3a1e5d665d8d" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>566904.004 5932828.228 566878.799 5932832.072 566876.538 5932817.244 566901.745 5932813.399 566904.004 5932828.228</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b99f5f46-d32c-4796-9a95-efb93cdd3445">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567258.017 5932707.901</gml:lowerCorner>
          <gml:upperCorner>567285.65 5932727.488</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_7d90460b-f697-40a9-b319-ec774a5f5530" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567273.793 5932723.846 567261.31 5932727.488 567258.017 5932714.912 567282.014 5932707.901 567285.65 5932720.382 567273.793 5932723.846</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_95908282-8125-4a08-87c6-7c25ddefda97">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566875.105 5932804.008</gml:lowerCorner>
          <gml:upperCorner>566901.745 5932817.244</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_5e468be9-b0a4-470e-9b90-a8ecdfc0ce65" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>566901.745 5932813.399 566876.538 5932817.244 566875.105 5932807.852 566900.314 5932804.008 566901.249 5932810.137 566901.745 5932813.399</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_24cd4e41-95f6-4c31-8711-6b0673c48a0e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567075.983 5932789.185</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932808.602</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_24dd6d26-802e-4d2d-9912-22756ded148b" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567103.525 5932802.817 567079.203 5932808.602 567075.983 5932794.967 567100.281 5932789.185 567103.525 5932802.817</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_3ad88da2-f1a7-4d09-8023-6ee4d7b0fa6e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567056.034 5932761.11</gml:lowerCorner>
          <gml:upperCorner>567083.489 5932780.007</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_0b1a5c06-bfeb-4b96-a6c4-f6cd46c5d6a7" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567083.489 5932773.657 567070.652 5932776.99 567059.035 5932780.007 567056.034 5932767.393 567080.231 5932761.11 567083.489 5932773.657</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_a0b7e9f5-09b8-412c-9fa7-4da902b89f30">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567414.585 5932664.872</gml:lowerCorner>
          <gml:upperCorner>567442.096 5932683.896</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_b8a7af92-1d46-4b9d-ae6a-00b7cd7da5ca" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567430.505 5932680.533 567417.947 5932683.896 567414.585 5932671.338 567438.734 5932664.872 567442.096 5932677.43 567430.505 5932680.533</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_285ceaa0-1a01-4527-8a53-1f2f23dbc5cf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567381.24 5932690.361</gml:lowerCorner>
          <gml:upperCorner>567399.746 5932715.941</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_c10193c2-c039-4483-81ab-0acfd71c2003" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567399.746 5932712.579 567387.189 5932715.941 567381.24 5932693.724 567393.798 5932690.361 567399.746 5932712.579</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_a2559c2a-2d26-43b6-b1cf-f711e167627c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567399.746 5932706.113</gml:lowerCorner>
          <gml:upperCorner>567427.517 5932726.103</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_a272bae5-ffb6-4fcd-8236-5b5be2744c17" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567427.517 5932719.637 567403.367 5932726.103 567399.746 5932712.579 567423.896 5932706.113 567427.517 5932719.637</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_48e8084f-68bf-4a23-aac1-4df9d7eeff57">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567059.035 5932776.739</gml:lowerCorner>
          <gml:upperCorner>567075.96 5932797.983</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_0d15dcda-6412-4fbc-8249-be25927215b0" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567075.96 5932794.973 567063.313 5932797.983 567059.035 5932780.007 567070.652 5932776.99 567071.62 5932776.739 567075.96 5932794.973</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_784dbaff-c2f6-4519-a40b-7afdf39d1b8f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.503 5932773.896</gml:lowerCorner>
          <gml:upperCorner>567079.203 5932821.212</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_a90b8e8b-356b-4b6d-82a1-7994fa799b9b" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567079.203 5932808.602 567026.182 5932821.212 567022.941 5932807.591 567015.503 5932776.34 567028.284 5932773.896 567035.587 5932804.581 567063.313 5932797.983 567075.983 5932794.967 567079.203 5932808.602</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_2f828891-0f23-4cb8-a4d5-9c131b2e3089">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567337.79 5932688.537</gml:lowerCorner>
          <gml:upperCorner>567403.367 5932740.198</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_a573712f-eb0e-4b10-a79a-c0f6ca3b40e2" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567350.721 5932740.198 567347.101 5932726.674 567337.79 5932691.899 567350.348 5932688.537 567359.658 5932723.312 567387.189 5932715.941 567399.746 5932712.579 567403.367 5932726.103 567350.721 5932740.198</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_442af384-8831-4bd1-ae12-3e01802b513a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567195.374 5932730.73</gml:lowerCorner>
          <gml:upperCorner>567229.869 5932764.51</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_eae94aa6-07c3-4b2c-818c-5e97b93cef41" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567229.869 5932757.348 567202.283 5932764.51 567195.374 5932737.898 567222.958 5932730.73 567224.591 5932737.022 567229.869 5932757.348</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f9238a58-ea13-46bf-8414-f7baa3badd69">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567221.324 5932718.156</gml:lowerCorner>
          <gml:upperCorner>567248.789 5932737.022</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_1a99332e-97b4-4594-92e8-1541f209d21c" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567237.174 5932733.755 567224.591 5932737.022 567222.958 5932730.73 567221.324 5932724.439 567245.522 5932718.156 567248.789 5932730.739 567237.174 5932733.755</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_a69fedcd-054a-4c17-ba94-b4c5cde51146">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566899.289 5932793.365</gml:lowerCorner>
          <gml:upperCorner>566926.952 5932810.137</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_daeeb248-0348-4ad6-8e03-a56eb30f9357" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>566926.952 5932806.22 566914.1 5932808.177 566901.249 5932810.137 566900.314 5932804.008 566899.289 5932797.285 566924.003 5932793.516 566924.991 5932793.365 566926.952 5932806.22</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_01b3c2d3-922c-4b45-a801-2b9ac29ab4b5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566858.112 5932800.141</gml:lowerCorner>
          <gml:upperCorner>566919.2 5932849.831</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_f99d41ea-0905-4188-9a37-14b62e3386ff" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>566914.1 5932808.177 566919.2 5932841.614 566865.323 5932849.831 566858.112 5932802.554 566873.929 5932800.141 566878.799 5932832.072 566904.004 5932828.228 566901.249 5932810.137 566914.1 5932808.177</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6169224a-3c33-477c-97d8-07b9ca2d0a5a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567095.823 5932767.187</gml:lowerCorner>
          <gml:upperCorner>567112.927 5932789.338</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_18bf583e-bdff-4bc5-bf01-1abc298d9b83" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567112.927 5932786.175 567100.317 5932789.338 567100.281 5932789.185 567095.823 5932770.455 567108.409 5932767.187 567112.927 5932786.175</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_0772db52-ce5c-46fd-828d-edd7297e9816">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567212.621 5932749.916</gml:lowerCorner>
          <gml:upperCorner>567213.621 5932750.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_ff172cb9-5612-475f-ad24-913df1c76eb1"></xplan:dientZurDarstellungVon>
      <xplan:schriftinhalt>(B)</xplan:schriftinhalt>
      <xplan:skalierung>1.0</xplan:skalierung>
      <xplan:position>
        <gml:Point gml:id="_91c129dd-e61e-43d8-b829-5d3bfe2abd8e" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567212.621 5932749.916</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_8da28984-778e-4e95-b90d-64afa790714a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567124.376 5932775.503</gml:lowerCorner>
          <gml:upperCorner>567125.376 5932776.503</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_01e35ac7-7837-4b85-8d85-e79ee17191ac"></xplan:dientZurDarstellungVon>
      <xplan:schriftinhalt>(B)</xplan:schriftinhalt>
      <xplan:skalierung>1.0</xplan:skalierung>
      <xplan:position>
        <gml:Point gml:id="_515b82f3-f46d-4ff5-ba5a-b72c3d10b5bc" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567124.376 5932775.503</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Wasserrecht gml:id="GML_1957130e-eb44-4fe1-9900-057e7b1656b3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566839.305 5932624.6</gml:lowerCorner>
          <gml:upperCorner>567474.886 5932795.563</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="_f8845979-2657-42a7-8474-5c7e973ba220" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567099.41 5932743.34 567034.553 5932760.037 567024.668 5932762.288 567001.253 5932766.989 566957.155 5932775.947 566925.622 5932781.675 566925.816 5932782.812 566893.555 5932788.149 566876.457 5932790.784 566856.166 5932793.67 566841.322 5932795.563 566839.305 5932779.633 566885.545 5932770.145 566933.255 5932760.356 567084.333 5932725.96 567189.373 5932700.56 567374.73 5932651.63 567470.445 5932624.6 567474.886 5932638.488 567462.605 5932641.979 567379.905 5932665.733 567324.249 5932681.582 567309.188 5932685.742 567309.648 5932687.426 567251.407 5932703.902 567207.598 5932715.718 567165.898 5932726.231 567099.41 5932743.34</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
    </xplan:SO_Wasserrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Wasserrecht gml:id="GML_2ca68aee-6fe5-4f9b-adb8-50541a1ebad6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566839.305 5932624.6</gml:lowerCorner>
          <gml:upperCorner>567478.319 5932805.784</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Hochwassergefährdeter Bereich</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_2d7be849-3d58-4bec-a554-3d752d9b2ab9_SO"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_87ecc9f6-a887-4b04-9133-3714de4e52d6" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567478.319 5932654.273 567325.715 5932695.132 567291.866 5932705.022 567258.017 5932714.912 567181.156 5932734.868 567182.755 5932741.028 567172.637 5932743.881 567171.049 5932756.863 567159.379 5932755.521 567161.242 5932742.524 567155.075 5932741.64 567154.642 5932745.368 567117.89 5932754.911 567117.01 5932751.523 567038.607 5932771.88 567004.903 5932778.319 566935.784 5932791.524 566924.003 5932793.516 566858.263 5932803.542 566842.603 5932805.784 566841.516 5932797.098 566841.353 5932795.811 566841.322 5932795.563 566839.305 5932779.633 566885.545 5932770.145 566933.255 5932760.356 567084.333 5932725.96 567189.373 5932700.56 567374.73 5932651.63 567470.445 5932624.6 567474.886 5932638.488 567474.962 5932638.726 567477.061 5932645.289 567475.978 5932645.649 567478.319 5932654.273</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>20002</xplan:artDerFestlegung>
    </xplan:SO_Wasserrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_5b9f4938-413d-43fb-b861-720b1d5e0601">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566904.86 5932822.968</gml:lowerCorner>
          <gml:upperCorner>566939.864 5932843.606</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_15fef662-24ed-4ee4-864b-95c4720f68c8_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_498b0819-7b15-4de6-aec6-21e7dfd05263" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566906.14 5932843.606 566904.86 5932835.912 566918.024 5932833.904 566916.859 5932826.266 566936.789 5932822.968 566939.864 5932838.193 566919.2 5932841.614 566906.14 5932843.606</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_9fec54ea-0a52-48aa-9554-04ace7a1e2cf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567470.593 5932666.819</gml:lowerCorner>
          <gml:upperCorner>567491.419 5932705.541</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_15fef662-24ed-4ee4-864b-95c4720f68c8_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_ff87f3f7-c987-4269-be54-1a49058a82d4" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567491.419 5932702.527 567480.162 5932705.541 567470.593 5932669.8 567481.725 5932666.819 567491.419 5932702.527</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_11c0cdfb-2b02-4518-8b2e-1458a8f6c9bd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566842.603 5932803.585</gml:lowerCorner>
          <gml:upperCorner>566865.323 5932852.383</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_a5bbf517-2698-419f-a93a-1996ca8c450c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_ac45e758-407c-4d1c-a49a-96a7d0f0395f" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566865.319 5932849.807 566865.323 5932849.831 566848.591 5932852.383 566842.603 5932805.784 566858.269 5932803.585 566865.319 5932849.807</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>3000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_2934b32c-6d07-4d3d-9d73-00d133e28839">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566847.954 5932803.585</gml:lowerCorner>
          <gml:upperCorner>566862.976 5932836.346</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_15fef662-24ed-4ee4-864b-95c4720f68c8_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_b8602449-e12a-4835-ab83-b381a9daead6" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566862.976 5932834.447 566851.949 5932836.346 566847.954 5932805.033 566858.269 5932803.585 566862.976 5932834.447</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_16a5aba2-9c83-4175-a28b-c8d6e5a4a640">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566991.754 5932776.34</gml:lowerCorner>
          <gml:upperCorner>567026.182 5932827.172</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_15fef662-24ed-4ee4-864b-95c4720f68c8_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_18e0d91c-077d-4a9c-a495-8d067210575f" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567026.182 5932821.212 567013.667 5932824.189 566998.079 5932827.172 566995.451 5932813.416 566991.754 5932794.066 567004.523 5932791.626 567002.093 5932778.904 567004.845 5932778.378 567015.503 5932776.34 567026.182 5932821.212</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_d0188aa3-4f7b-4d35-bd8f-a39cc777b93c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566914.1 5932791.571</gml:lowerCorner>
          <gml:upperCorner>566941.637 5932826.266</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_15fef662-24ed-4ee4-864b-95c4720f68c8_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_3cd76803-80bb-4f55-9b55-263b7819a910" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566941.637 5932822.166 566916.859 5932826.266 566914.1 5932808.177 566926.952 5932806.22 566924.995 5932793.392 566925.489 5932793.308 566935.792 5932791.571 566938.222 5932804.293 566941.637 5932822.166</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_0066135d-0b09-423e-a7bc-12ea23ff7196">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567313.643 5932691.899</gml:lowerCorner>
          <gml:upperCorner>567350.721 5932747.553</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_15fef662-24ed-4ee4-864b-95c4720f68c8_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_337db7fe-d914-4893-b904-00ff7e44a9e5" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567350.721 5932740.198 567338.647 5932743.431 567322.893 5932747.553 567319.349 5932734.009 567313.643 5932712.203 567325.501 5932708.738 567321.864 5932696.257 567325.715 5932695.132 567337.79 5932691.899 567350.721 5932740.198</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c795e4d7-d3e7-4b85-a8d6-b6cc1e6feba2_BP">
      <xplan:schluessel>§2 Nr.27</xplan:schluessel>
      <xplan:text>In den Mischgebieten ist der Erschütterungsschutz der
Gebäude durch bauliche oder technische Maßnahmen
(zum Beispiel an Wänden, Decken und Fundamenten)
so sicherzustellen, dass die Anhaltswerte der DIN 4150
(Erschütterungen im Bauwesen), Teil 2 (Einwirkungen auf
Menschen in Gebäuden), Tabelle 1, Zeile 3 (Mischgebiete
nach BauNVO) eingehalten werden. Zusätzlich ist durch
die baulichen und technischen Maßnahmen zu gewährleisten,
dass der sekundäre Luftschall die Immissionsrichtwerte
der Technischen Anleitung zum Schutz gegen Lärm
vom 26. August 1998 (Gemeinsames Ministerialblatt
S. 503), Nummer 6.2, nicht überschreitet. Einsichtnahmestelle
der DIN 4150: Freie und Hansestadt Hamburg,
Behörde für Stadtentwicklung und Umwelt, Amt für
Immissionsschutz und Betriebe, Bezugsquelle der DIN
4150: Beuth Verlag GmbH, Berlin.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_6450304f-6c22-4cc9-8a19-3e98e46eed8a_BP">
      <xplan:schluessel>§2 Nr.25</xplan:schluessel>
      <xplan:text>In den Mischgebieten sind Dächer als Flachdächer oder
flachgeneigte Dächer mit einer Neigung bis zu 10 Grad
auszuführen.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_382e6441-314e-443a-a5a2-290c68c53794_BP">
      <xplan:schluessel>§2 Nr.13.3</xplan:schluessel>
      <xplan:text>Vom Anschluss- und Benutzungsgebot nach Nummer 13.1
kann auf Antrag befreit werden, soweit die Erfüllung der
Anforderungen im Einzelfall wegen besonderer Umstände
zu einer unbilligen Härte führen würde. Die Befreiung soll
zeitlich befristet werden.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_4f96ce33-c275-4311-9ea1-737d64b48b78_BP">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Die Aufenthaltsräume für gewerbliche Nutzungen – hier insbesondere
die Pausen- und Ruheräume – sind durch geeignete
Grundrissgestaltung den Verkehrslärm abgewandten
Gebäudeseiten zuzuordnen. Soweit die Anordnung an den
vom Verkehrslärm abgewandten Gebäudeseiten nicht möglich
ist, muss für diese Räume ein ausreichender Schallschutz
an Außentüren, Fenstern, Außenwänden und Dächern der
Gebäude durch bauliche Maßnahmen geschaffen werden.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_0a31d433-388a-43b7-8815-b0c3b0eea5cf_BP">
      <xplan:schluessel>§2 Nr.14</xplan:schluessel>
      <xplan:text>Das auf den Mischgebietsflächen und den Straßenverkehrsflächen
südlich der Versmannstraße anfallende
Nieder schlagswasser ist direkt in das nächst liegende
Gewässer (Baakenhafen) einzuleiten.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_6a75e8bb-f2d6-4867-8a26-f6c5df60cae1_BP">
      <xplan:schluessel>§2 Nr.13.2</xplan:schluessel>
      <xplan:text>Vom Anschluss- und Benutzungsgebot nach Nummer 13.1
kann ausnahmsweise abgesehen werden, wenn der berechnete
Heizwärmebedarf der Gebäude nach der Energieeinsparverordnung
vom 24. Juli 2007 (BGBl. I S. 1519),
zuletzt geändert am 18. November 2013 (BGBl. I S. 3951),
den Wert von 15 kWh/m² Nutzfläche nicht übersteigt.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_d7e44f97-b0f8-43cb-9ed6-b76d768f9071_BP">
      <xplan:schluessel>§2 Nr.13.1</xplan:schluessel>
      <xplan:text>Neu zu errichtende Gebäude sind an ein Wärmenetz anzuschließen,
das überwiegend mit erneuerbaren Energien
versorgt wird.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_e6ef7a8a-cdde-4f34-a748-a84cb7831fd4_BP">
      <xplan:schluessel>§2 Nr.18</xplan:schluessel>
      <xplan:text>Auf den gekennzeichneten Flächen nördlich des Baakenhafens
(im Bereich Versmannkai und Versmannstraße),
deren Böden erheblich mit umweltgefährdenden Stoffen
belastet sind, sind bauliche Gassicherungsmaßnahmen
vorzusehen, die sowohl Gasansammlungen unter den
baulichen Anlagen und den befestigten Flächen als auch
Gaseintritte in die baulichen Anlagen verhindern.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_0958e0ae-8310-4eb4-9586-7df30e9c78de_BP">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>Tiefgaragen sind außerhalb der überbaubaren Grundstücksflächen
zulässig. Stellplätze sind nur in Tiefgaragen
oder Garagengeschossen unterhalb der Höhe von 8,7 m
über Normalnull (NN) zulässig. Geringfügige Abweichungen
sind zulässig, wenn sie durch abweichende Straßenanschlusshöhen
von über 8,7 m über NN begründet
sind.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_b7112969-6eac-47c8-9443-cd9a0ac271ba_BP">
      <xplan:schluessel>§2 Nr.26</xplan:schluessel>
      <xplan:text>Für festgesetzte Anpflanzungen sind standortgerechte
Laubbäume oder belaubte Heckenpflanzen zu verwenden.
Großkronige Bäume müssen einen Stammumfang von
mindestens 18 cm, kleinkronige Bäume von mindestens
14 cm, in 1 m Höhe über dem Erdboden gemessen, aufweisen;
Heckenpflanzen eine Mindesthöhe von 80 cm.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP">
      <xplan:schluessel>§2 Nr.12</xplan:schluessel>
      <xplan:text>Werbeanlagen größer als 2 m² und Werbeanlagen oberhalb
der Gebäudetraufen sind unzulässig. Die Gestaltung der
Gesamtbaukörper und der privaten Freiflächen darf nicht
durch Werbeanlagen beeinträchtigt werden. Werbeanlagen
sind nur an der Stätte der Leistung zulässig. Oberhalb der
Brüstung des zweiten Vollgeschosses sind Werbeanlagen
nur ausnahmsweise zulässig, wenn zudem das Ortsbild
nicht beeinträchtigt wird.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Auf den mit „(A)“ bezeichneten Flächen der Mischgebiete
sind Wohnungen in den Erdgeschossen unzulässig. Auf den
mit „(F)“ bezeichneten Flächen der Mischgebiete sind
Wohnungen in den Erdgeschossen ausnahmsweise zulässig.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_7768f4e1-2504-4f08-9c4a-1b9e3ab5ed6e_BP">
      <xplan:schluessel>§2 Nr.10</xplan:schluessel>
      <xplan:text>Die Gebäudefassaden können in unterschiedlichen Materialien
ausschließlich in den Farben Weiß, Beige, Gelb und
Blaubunt ausgeführt werden.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Außer auf den mit „(C)“ bezeichneten Flächen muss die
Oberkante des Fußbodens des ersten Obergeschosses mindestens
5,5 m und höchstens 6,5 m über der angrenzenden
Geländeoberfläche liegen. Ausnahmsweise kann im Erdgeschoss
eine Galerie eingebaut werden, wenn das Galeriegeschoss
eine Grundfläche kleiner 50 vom Hundert (v.H.)
der Grundfläche des Erdgeschosses einnimmt. Die Galerieebene
muss einen Abstand von mindestens 4,5 m von der
Innenseite der zu den öffentlichen Straßenverkehrsflächen
und mit Gehrechten belegten Flächen gerichteten Außenfassade
einhalten. Das Erdgeschoss samt einem eventuell
eingezogenen Galeriegeschoss wird als ein Vollgeschoss
gewertet.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_b2d9cad2-18da-4d71-9e2b-e4e3dc0de2d1_BP">
      <xplan:schluessel>§2 Nr.20</xplan:schluessel>
      <xplan:text>In den Baugebieten sind für Einfriedigungen nur Hecken
oder durchbrochene Zäune in Verbindung mit Hecken bis
zu einer Höhe von 1,2 m zulässig.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_2c4e8d5b-d2f3-4adb-b554-efe208d803af_BP">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Oberhalb der festgesetzten Vollgeschosse (einschließlich
einem möglichen Galeriegeschoss im Erdgeschoss) sind
weitere Geschosse unzulässig. Technikgeschosse und technische
oder erforderliche Aufbauten, wie Treppenräume, sind ausnahmsweise, auch über der festgesetzten Gebäudehöhe,
zulässig, wenn die Gestaltung des Gesamtbaukörpers
und das Ortsbild nicht beeinträchtigt werden und
diese keine wesentliche Verschattung der Nachbargebäude
und der Umgebung bewirken. Aufbauten, deren Einhausung
und Technikgeschosse sind mindestens 2,5 m von
der Außenfassade zurückzusetzen.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_d03d15de-716d-4215-b4bb-f6866dc31699_BP">
      <xplan:schluessel>§2 Nr.2.2</xplan:schluessel>
      <xplan:text>Vergnügungsstätten in den Teilen des Mischgebiets, die
überwiegend durch gewerbliche Nutzungen geprägt sind
sowie Tankstellen sind unzulässig. Ausnahmen für Vergnügungsstätten
in den übrigen Teilen des Mischgebiets werden
ausgeschlossen.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_fd656471-9a50-4e8b-8da2-af82666cde09_BP">
      <xplan:schluessel>§2 Nr.23</xplan:schluessel>
      <xplan:text>Tiefgaragen und die Dachflächen der festgesetzten eingeschossigen
Gebäude auf den mit „(E)“ bezeichneten
Flächen sind in den zu begrünenden Bereichen mit einem
mindestens 50 cm starken durchwurzelbaren Substrataufbau
zu versehen. Für Baumpflanzungen muss auf einer
Fläche von 16 m² je Baum die Stärke des durchwurzelbaren
Substrataufbaus mindestens 80 cm betragen.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_4231d2e4-1587-42db-9621-050e3eba8e62_BP">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>In den Mischgebieten an der Versmannstraße sind die
Schlafräume zur lärmabgewandten Gebäudeseite zu orientieren.
Wohn- / Schlafräume in Einzimmerwohnungen und
Kinderzimmer sind wie Schlafräume zu beurteilen. Wird an
den Gebäudeseiten ein Pegel von 70 dB(A) am Tag (6.00 Uhr
bis 22.00 Uhr) überschritten, sind vor den Fenstern der
zu dieser Gebäudeseite orientierten Wohnräume bauliche
Schallschutzmaßnahmen in Form von verglasten Vorbauten
(zum Beispiel verglaste Loggien, Wintergärten) oder vergleichbare
Maßnahmen vorzusehen.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP">
      <xplan:schluessel>§2 Nr.11</xplan:schluessel>
      <xplan:text>Eine Überschreitung der Baugrenzen durch Balkone,
Erker, Loggien und Sichtschutzwände kann zu den öffentlichen
Straßenräumen oder den mit Gehrechten belasteten
Flächen ausnahmsweise bis zu einer Tiefe von 1,5 m zugelassen
werden, wenn die Gestaltung des Gesamtbaukörpers
nicht beeinträchtigt wird und diese keine wesentliche Verschattung
der benachbarten Nutzungen und der Umgebung
bewirken. Dabei ist eine Überbauung der Straßenverkehrsfläche
nur oberhalb einer lichten Höhe von 4,5 m
zulässig</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_4436c60e-ce8d-4fbf-88fa-a464667ec0e9_BP">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Für die Mischgebiete gilt:</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_9a1336c1-2d19-4f4a-8cdb-8ce7b44d7b5d_BP">
      <xplan:schluessel>§2 Nr.21</xplan:schluessel>
      <xplan:text>Die nicht überbauten Grundstücksflächen der Mischgebiete,
mit Ausnahme der Flächen mit festgesetzten
Gehrechten, sowie die Dachflächen der festgesetzten eingeschossigen
Gebäude auf den mit „(E)“ bezeichneten
Flächen sind mit einem Anteil von mindestens 50 v. H.
zu begrünen. Je 300 m² ist mindestens ein großkroniger
Baum oder je 150 m² ein kleinkroniger Baum zu pflanzen
und dauerhaft zu erhalten. Bei Abgang ist eine gleichwertige
Ersatzpflanzung vorzunehmen.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c2d35123-8af2-4cf8-8325-13c37fbd6830_BP">
      <xplan:schluessel>§2 Nr.24</xplan:schluessel>
      <xplan:text>Die übrigen Dachflächen in den Mischgebieten sind mit
Ausnahme der gemäß Nummer 9 zulässigen Anlagen und
technischen Aufbauten zu mindestens 30 v. H. mit einem
mindestens 15 cm starken durchwurzelbaren Substrataufbau
extensiv mit standortangepassten Stauden und Gräsern
zu begrünen. Darüber hinaus müssen mindestens
20 v. H. mit einem mindestens 50 cm starken Substrataufbau
intensiv mit Stauden und Sträuchern begrünt werden.
Die Dachbegrünung ist dauerhaft zu erhalten.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c0ad16b1-6dcc-423e-ba6c-c8e31f8f867e_BP">
      <xplan:schluessel>§2 Nr.19</xplan:schluessel>
      <xplan:text>Auf den nicht überbauten Grundstücksflächen sind
Nebenanlagen nur ausnahmsweise zulässig, wenn die
Gestaltung der Freiflächen nicht beeinträchtigt ist.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_40708916-979d-4b80-a8bf-274dac77799c_BP">
      <xplan:schluessel>§2 Nr.2.3</xplan:schluessel>
      <xplan:text>Die festgesetzten Grundflächenzahlen können für Nutzungen
nach § 19 Absatz 4 Satz 1 der Baunutzungsverordnung
(BauNVO) in der Fassung vom 23. Januar 1990
(BGBl. I S. 133), zuletzt geändert am 11. Juni 2013 (BGBl. I
S. 1548, 1551), bis 1,0 überschritten werden.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_15fef662-24ed-4ee4-864b-95c4720f68c8_BP">
      <xplan:schluessel>§2 Nr.16</xplan:schluessel>
      <xplan:text>Die festgesetzten Gehrechte umfassen die Befugnis der
Freien und Hansestadt Hamburg, allgemein zugängige
Gehwege anzulegen und zu unterhalten. Geringfügige
Abweichungen von den festgesetzten Gehrechten sind
zulässig.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_8ade82cf-d047-4e3c-a7b8-426429ae8f2a_BP">
      <xplan:schluessel>§2 Nr.13</xplan:schluessel>
      <xplan:text>Für die Beheizung und Bereitstellung des Warmwassers
gilt:</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_d854f146-551d-4d47-a76e-3125c7e50abd_BP">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>Durch geeignete bauliche Schallschutzmaßnahmen wie zum
Beispiel Doppelfassaden, verglaste Vorbauten (zum Beispiel
verglaste Loggien, Wintergärten), besondere Fensterkonstruktionen
oder in ihrer Wirkung vergleichbare Maßnahmen
ist sicherzustellen, dass durch diese baulichen Maßnahmen
insgesamt eine Schallpegeldifferenz erreicht wird,
die es ermöglicht, dass in Schlafräumen ein Innenraumpegel
bei teilgeöffneten Fenstern von 30 dB(A) während der
Nachtzeit (22.00 Uhr bis 6.00 Uhr) nicht überschritten
wird. Erfolgt die bauliche Schallschutzmaßnahme in Form
von verglasten Vorbauten, muss dieser Innenraumpegel bei
teilgeöffneten Bauteilen erreicht werden.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_afff76f6-49c8-449e-8b0a-fe6fdd7bfb1a_BP">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Für einen Außenbereich einer Wohnung ist entweder durch
Orientierung an lärmabgewandten Gebäudeseiten oder
durch bauliche Schallschutzmaßnahmen wie zum Beispiel
verglaste Vorbauten (zum Beispiel verglaste Loggien, Wintergärten)
mit teilgeöffneten Bauteilen sicherzustellen, dass
durch diese baulichen Maßnahmen insgesamt eine Schallpegelminderung
erreicht wird, die es ermöglicht, dass in
dem der Wohnung zugehörigen Außenbereich ein Tagpegel
(6.00 Uhr bis 22.00 Uhr) von kleiner 65 dB(A) erreicht
wird.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_a5bbf517-2698-419f-a93a-1996ca8c450c_BP">
      <xplan:schluessel>§2 Nr.17</xplan:schluessel>
      <xplan:text>Das festgesetzte Geh- und Fahrrecht umfasst die Befugnis
der für die Unterhaltung der Kaianlagen zuständigen
Stelle, diese Flächen zu begehen und zu befahren.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_592b628c-3832-49f2-ac44-4ce50d35556f_BP">
      <xplan:schluessel>§2 Nr.22</xplan:schluessel>
      <xplan:text>Die mit festgesetzten Gehrechten belegten Flächen der
Mischgebiete sind mit einem Anteil von mindestens 20 v. H.
zu begrünen. Je 500 m² ist mindestens ein großkroniger
Baum oder je 250 m² ein kleinkroniger Baum zu pflanzen
und dauerhaft zu erhalten. Bei Abgang ist eine gleichwertige
Ersatzpflanzung vorzunehmen.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_2d7be849-3d58-4bec-a554-3d752d9b2ab9_BP">
      <xplan:schluessel>§2 Nr.15</xplan:schluessel>
      <xplan:text>An den Rändern der hochwassergefährdeten Bereiche sind
zum Zwecke des Hochwasserschutzes, soweit erforderlich,
zusätzliche besondere bauliche Maßnahmen vorzusehen.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_a5d62934-0c67-42df-ab71-478ecb59f350_BP">
      <xplan:schluessel>§2 Nr.2.1</xplan:schluessel>
      <xplan:text>Großflächiger Einzelhandel kann ausnahmsweise auf den
mit „(B)“ bezeichneten Flächen zugelassen werden, wenn
er der Nahversorgung der angrenzenden Quartiere dient.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_0602cd45-6a1a-4b24-8786-30862f1fabca">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566839.305 5932624.6</gml:lowerCorner>
          <gml:upperCorner>567509.287 5932905.509</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan001_4-1</xplan:name>
      <xplan:beschreibung>Der Bebauungsplan BPlan001_4-1 für das Gebiet am
Nordufer des Baakenhafens (Bezirk Hamburg-Mitte, Ortsteil
104) wird festgestellt.
Das Gebiet wird wie folgt begrenzt:
Über die Flurstücke 2348 (alt: 2236 – Pfeilerbahn), 1635,
1637 (Versmannstraße) und 2367 (alt: 1639) der Gemarkung
Altstadt Süd – Baakenhafen – über das Flurstück 2367,
Westgrenze des Flurstücks 2361, über das Flurstück 2358
(alt: 1021 – Versmannstraße), Nordgrenzen der Flurstücke
2358 und 1636 (Versmannstraße), über das Flurstück 1634
(Versmannstraße) der Gemarkung Altstadt Süd.</xplan:beschreibung>
      <xplan:technHerstellDatum>2017-03-29</xplan:technHerstellDatum>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:MultiSurface gml:id="_19f2e9c4-4d1d-4cab-a477-38dc0e121f27" srsName="EPSG:25832" srsDimension="2">
          <gml:surfaceMember>
            <gml:Polygon gml:id="_3c7118b2-9815-43fa-bd4b-9cc1782d4ca2" srsName="EPSG:25832" srsDimension="2">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList>566952.478 5932905.509 566941.899 5932887.112 566926.109 5932859.655 566925.011 5932857.747 566850.623 5932868.84 566849.428 5932859.158 566848.719 5932853.417 566848.591 5932852.383 566842.603 5932805.784 566841.516 5932797.098 566839.305 5932779.633 566885.545 5932770.145 566933.255 5932760.356 567084.333 5932725.96 567189.373 5932700.56 567374.73 5932651.63 567470.445 5932624.6 567476.043 5932642.107 567477.061 5932645.289 567475.978 5932645.649 567489.76 5932696.417 567494.443 5932713.666 567502.35 5932743.169 567509.287 5932769.105 567416.044 5932790.244 567354.506 5932803.777 567322.897 5932810.727 567243.981 5932831.427 567071.225 5932875.978 566952.478 5932905.509</gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#GML_c795e4d7-d3e7-4b85-a8d6-b6cc1e6feba2_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_6450304f-6c22-4cc9-8a19-3e98e46eed8a_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_382e6441-314e-443a-a5a2-290c68c53794_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_4f96ce33-c275-4311-9ea1-737d64b48b78_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_0a31d433-388a-43b7-8815-b0c3b0eea5cf_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_6a75e8bb-f2d6-4867-8a26-f6c5df60cae1_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_d7e44f97-b0f8-43cb-9ed6-b76d768f9071_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_e6ef7a8a-cdde-4f34-a748-a84cb7831fd4_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_0958e0ae-8310-4eb4-9586-7df30e9c78de_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_b7112969-6eac-47c8-9443-cd9a0ac271ba_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_7768f4e1-2504-4f08-9c4a-1b9e3ab5ed6e_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_b2d9cad2-18da-4d71-9e2b-e4e3dc0de2d1_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_2c4e8d5b-d2f3-4adb-b554-efe208d803af_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_d03d15de-716d-4215-b4bb-f6866dc31699_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_fd656471-9a50-4e8b-8da2-af82666cde09_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_4231d2e4-1587-42db-9621-050e3eba8e62_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_4436c60e-ce8d-4fbf-88fa-a464667ec0e9_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_9a1336c1-2d19-4f4a-8cdb-8ce7b44d7b5d_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_c2d35123-8af2-4cf8-8325-13c37fbd6830_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_c0ad16b1-6dcc-423e-ba6c-c8e31f8f867e_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_40708916-979d-4b80-a8bf-274dac77799c_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_15fef662-24ed-4ee4-864b-95c4720f68c8_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_8ade82cf-d047-4e3c-a7b8-426429ae8f2a_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_d854f146-551d-4d47-a76e-3125c7e50abd_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_afff76f6-49c8-449e-8b0a-fe6fdd7bfb1a_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_a5bbf517-2698-419f-a93a-1996ca8c450c_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_2d7be849-3d58-4bec-a554-3d752d9b2ab9_BP"></xplan:texte>
      <xplan:texte xlink:href="#GML_a5d62934-0c67-42df-ab71-478ecb59f350_BP"></xplan:texte>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>104</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2014-12-23</xplan:rechtsverordnungsDatum>
      <xplan:veraenderungssperre>false</xplan:veraenderungssperre>
      <xplan:staedtebaulicherVertrag>false</xplan:staedtebaulicherVertrag>
      <xplan:erschliessungsVertrag>false</xplan:erschliessungsVertrag>
      <xplan:durchfuehrungsVertrag>false</xplan:durchfuehrungsVertrag>
      <xplan:gruenordnungsplan>false</xplan:gruenordnungsplan>
      <xplan:bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:bereich>
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_88948c97-8b3d-46fc-a104-60857bb84be4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566848.591 5932702.527</gml:lowerCorner>
          <gml:upperCorner>567502.35 5932887.077</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="_ecf71172-9b3b-4a67-a59b-6a361fb3f884" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566848.591 5932852.383 566919.171 5932841.619 566944.512 5932837.424 567013.667 5932824.189 567103.525 5932802.817 567149.128 5932791.971 567193.176 5932781.173 567245.957 5932767.583 567270.174 5932761.347 567338.647 5932743.431 567427.516 5932719.637 567491.419 5932702.527 567502.35 5932743.169 567485.287 5932746.96 567390.254 5932768.247 567363.553 5932775.056 567163.038 5932830.198 567122.404 5932841.663 567047.196 5932861.151 566942.034 5932887.077 566925.011 5932857.747 566850.623 5932868.84 566848.591 5932852.383</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="GML_36c586d6-47b1-447d-b11d-cdd263f9c472">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566841.322 5932638.488</gml:lowerCorner>
          <gml:upperCorner>567478.319 5932805.784</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="_5993447f-f287-4932-a4fc-440a611be5a3" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567193.176 5932781.173 567149.128 5932791.971 567154.527 5932745.398 567117.89 5932754.911 567117.019 5932751.558 567038.535 5932771.936 567004.845 5932778.378 566935.752 5932791.578 566925.489 5932793.308 566923.976 5932793.564 566899.326 5932797.323 566858.262 5932803.586 566842.603 5932805.784 566841.516 5932797.098 566841.322 5932795.563 566856.166 5932793.67 566876.457 5932790.784 566893.555 5932788.149 566925.816 5932782.812 566925.622 5932781.675 566957.155 5932775.947 567001.253 5932766.989 567024.668 5932762.288 567034.553 5932760.037 567099.41 5932743.34 567165.898 5932726.231 567207.598 5932715.718 567251.407 5932703.902 567309.648 5932687.426 567309.188 5932685.742 567324.249 5932681.582 567379.905 5932665.733 567462.605 5932641.979 567474.886 5932638.488 567476.043 5932642.107 567477.061 5932645.289 567475.978 5932645.649 567478.319 5932654.273 567414.585 5932671.338 567325.715 5932695.132 567258.017 5932714.912 567181.156 5932734.868 567193.176 5932781.173</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_4372a665-a76e-41eb-bbb3-e4899011d132">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567347.101 5932723.312</gml:lowerCorner>
          <gml:upperCorner>567359.658 5932726.674</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_35a25bf7-e834-45c1-abd4-152ba9682804" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567347.101 5932726.674 567359.658 5932723.312</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_8b10198c-064b-45c4-992a-4c6739d79849">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567306.772 5932734.136</gml:lowerCorner>
          <gml:upperCorner>567318.865 5932737.3</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_4d87c595-e6f1-4003-ad64-f35f558adeb5" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567306.772 5932737.3 567318.865 5932734.136</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_51cc1bc7-c630-4562-92e3-ac760070eda2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566982.682 5932813.416</gml:lowerCorner>
          <gml:upperCorner>566995.451 5932815.855</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_51f53f1a-fb98-4432-9414-5783ea516a32" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>566982.682 5932815.855 566995.451 5932813.416</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_ca387c49-a139-4e28-8c5c-e6c506d9ba9c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567092.822 5932757.841</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932802.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_b39f7571-d02a-4809-aca2-65ea36f344f9" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567103.525 5932802.817 567100.317 5932789.337 567100.281 5932789.185 567095.824 5932770.455 567092.822 5932757.841</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_63fb2da6-f94c-4b26-9823-38cfc2d3bfb6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567022.941 5932804.581</gml:lowerCorner>
          <gml:upperCorner>567035.587 5932807.591</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_dab9bdbe-59e3-44a6-bec3-3755b56b6827" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567022.941 5932807.591 567035.587 5932804.581</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_a2eeafdd-573a-4ad3-826b-7ec4e2452a0c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567258.017 5932714.912</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932761.347</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="_c4bbfc16-8d79-4d64-865c-1b55d4f4cb4f" srsName="EPSG:25832" srsDimension="2">
          <gml:segments>
            <gml:LineStringSegment>
              <gml:posList>567270.174 5932761.347 567258.017 5932714.912</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="GML_3fc11cdb-b7eb-4c39-a0b8-7f4cf383708d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566841.322 5932638.488</gml:lowerCorner>
          <gml:upperCorner>567509.287 5932905.509</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e6ef7a8a-cdde-4f34-a748-a84cb7831fd4_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_e0c4cccb-2981-4cd2-a468-d4283bab8408" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567509.287 5932769.105 567416.044 5932790.244 567354.506 5932803.777 567322.897 5932810.727 567243.981 5932831.427 567071.225 5932875.978 566952.478 5932905.509 566925.077 5932857.666 566850.623 5932868.84 566848.591 5932852.383 566842.603 5932805.784 566841.516 5932797.098 566841.322 5932795.563 566856.166 5932793.67 566876.457 5932790.784 566893.555 5932788.149 566925.816 5932782.812 566925.622 5932781.675 566957.155 5932775.947 567001.253 5932766.989 567024.668 5932762.288 567034.553 5932760.037 567099.41 5932743.34 567165.898 5932726.231 567207.598 5932715.718 567251.407 5932703.902 567309.648 5932687.426 567309.188 5932685.742 567324.249 5932681.582 567379.905 5932665.733 567462.605 5932641.979 567474.886 5932638.488 567477.061 5932645.289 567475.978 5932645.649 567478.319 5932654.273 567481.725 5932666.819 567491.419 5932702.527 567500.798 5932737.377 567502.35 5932743.169 567509.287 5932769.105</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>4000</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_8ac68803-4f31-48da-ab0e-3752c4d3ddad">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567143.706 5932787.913</gml:lowerCorner>
          <gml:upperCorner>567144.706 5932787.913</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.75</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="_c36f04cb-5cdf-48d3-8f31-7a3289a5803d" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567143.706 5932787.913</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_1fec35eb-3032-4ed6-a9cf-3c36ed5b4652">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567168.773 5932775.928</gml:lowerCorner>
          <gml:upperCorner>567169.773 5932775.928</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="_23afc874-383d-4940-81d1-1d3a5a2de5f5" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567168.773 5932775.928</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_8ebf4ef9-132a-43c2-ac70-4a5a95a49351">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567461.102 5932715.143</gml:lowerCorner>
          <gml:upperCorner>567462.102 5932715.143</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.75</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="_672e82f3-69d8-4bb3-97ef-1af6e91c9834" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567461.102 5932715.143</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_f303aa68-317d-4992-a7b2-ae920695dbfb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566869.36 5932853.767</gml:lowerCorner>
          <gml:upperCorner>566870.36 5932853.767</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.6</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="_a71bd121-7b62-450d-8aa8-7c7e131a307a" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>566869.36 5932853.767</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_bb101a4b-bfd9-4dee-a952-7edaf0eb42e1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567162.168 5932733.046</gml:lowerCorner>
          <gml:upperCorner>567163.168 5932733.046</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">5.4</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="_30f779f6-1b92-4f71-b969-e2b62abc2e18" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567162.168 5932733.046</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_287dc3f6-c180-4e5b-98bb-1deab55eb469">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567088.12 5932789.185</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932805.699</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">8.0</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="_a8e7dcac-64d6-4d12-89fc-88b7e81c0f15" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567088.12 5932792.079 567100.281 5932789.185 567103.525 5932802.817 567091.361 5932805.699 567088.12 5932792.079</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_2ade7eaf-560c-4a1a-a46b-5b3bf3e3b469">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566904.86 5932833.904</gml:lowerCorner>
          <gml:upperCorner>566919.2 5932843.606</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">5.0</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="_6f3e2462-5b30-4bc8-ad79-0600d671b236" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566904.86 5932835.912 566918.024 5932833.904 566919.2 5932841.614 566906.14 5932843.606 566904.86 5932835.912</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1400</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_5b15ec94-09cd-4e34-8d26-e06cf33a1191">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567254.372 5932747.803</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932764.526</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">8.0</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="_74a9416c-776c-4bfc-9f05-7189166f70c1" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567254.372 5932750.986 567266.628 5932747.803 567270.174 5932761.347 567257.829 5932764.526 567254.372 5932750.986</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_cc91cd1a-a10f-4344-ae85-2ad7682a6fcf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566858.112 5932800.141</gml:lowerCorner>
          <gml:upperCorner>566874.087 5932803.585</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">8.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="_71b278ca-9fb5-4072-bc08-ad85cd4f6afc" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566858.269 5932803.585 566858.112 5932802.554 566873.929 5932800.141 566874.087 5932801.172 566858.269 5932803.585</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_93892231-742a-46ad-bc0a-532018ab7236">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566916.859 5932822.968</gml:lowerCorner>
          <gml:upperCorner>566939.864 5932841.614</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">5.0</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="_9e104d80-e9d7-42bb-adee-978b43d2fc73" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566916.859 5932826.266 566936.789 5932822.968 566939.864 5932838.193 566919.2 5932841.614 566916.859 5932826.266</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_8b29af24-1126-4ac9-a7bd-24702842fe87">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567411.526 5932706.113</gml:lowerCorner>
          <gml:upperCorner>567427.516 5932722.929</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">8.0</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="_90eff2ae-6591-4266-a606-c5c2b0703965" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567415.221 5932722.929 567411.526 5932709.425 567423.896 5932706.113 567427.516 5932719.637 567415.221 5932722.929</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_fa60d8a2-ba3c-4c8c-ab75-f0d83a71c00a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567141.149 5932741.64</gml:lowerCorner>
          <gml:upperCorner>567155.075 5932748.872</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">4.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="_04bfedc5-baed-4928-9c9d-45fae17e3d08" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567155.075 5932741.64 567154.527 5932745.398 567141.149 5932748.872 567141.581 5932745.143 567155.075 5932741.64</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_TextAbschnitt gml:id="GML_2d7be849-3d58-4bec-a554-3d752d9b2ab9_SO">
      <xplan:schluessel>§2 Nr.15</xplan:schluessel>
      <xplan:text>An den Rändern der hochwassergefährdeten Bereiche sind
zum Zwecke des Hochwasserschutzes, soweit erforderlich,
zusätzliche besondere bauliche Maßnahmen vorzusehen.</xplan:text>
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
    </xplan:SO_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_01e35ac7-7837-4b85-8d85-e79ee17191ac">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567108.409 5932752.067</gml:lowerCorner>
          <gml:upperCorner>567140.779 5932786.174</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(B)</xplan:gliederung1>
      <xplan:gliederung2>(E)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_8da28984-778e-4e95-b90d-64afa790714a"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_f3822531-3f07-45c9-9251-1a538887b558"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_1149c0ca-dbaf-49f3-b297-d4313ddefa67"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_fd656471-9a50-4e8b-8da2-af82666cde09_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_a5d62934-0c67-42df-ab71-478ecb59f350_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_c6531bb3-56d7-4249-ad0f-bce1e4b91a2b" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567137.502 5932780.326 567112.927 5932786.174 567108.409 5932767.187 567120.277 5932764.106 567118.644 5932757.814 567140.779 5932752.067 567137.502 5932780.326</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_9d909301-e146-48ee-a590-c3982c2a5cdf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566978.985 5932794.066</gml:lowerCorner>
          <gml:upperCorner>566995.451 5932815.855</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(F)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_babf4409-ca64-454b-8629-74ddfc39a247"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_25b34d01-429b-4699-a792-c04ed44a3c8a" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566995.451 5932813.416 566982.682 5932815.855 566978.985 5932796.505 566991.754 5932794.066 566995.451 5932813.416</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7803ad38-1d7f-40eb-acd0-9f61d0149d94">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567381.24 5932690.361</gml:lowerCorner>
          <gml:upperCorner>567399.746 5932715.941</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_0a93742a-3a7a-4ebb-a203-26f465b0c0d8"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_6f2da464-0064-4c8f-8b74-1970b9476e33"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_5b223a14-b8c5-4bc6-8a10-0ec7aea23a50" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567399.746 5932712.579 567387.189 5932715.941 567381.24 5932693.724 567393.798 5932690.361 567399.746 5932712.579</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_087a1120-98af-4731-9053-a097ba5abec0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567261.31 5932723.846</gml:lowerCorner>
          <gml:upperCorner>567279.2 5932747.803</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_ba047098-ec96-43b6-aed7-9906e6a1bfff"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_3ea9a702-2f78-4b09-9500-ab448539d957"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_4129a1c7-1139-47c0-9dd0-d1cbf37a6071" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567279.2 5932744.514 567266.628 5932747.803 567261.31 5932727.488 567273.793 5932723.846 567279.2 5932744.514</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7413c4ed-9475-45fd-9492-7a29739d0ef8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566876.538 5932813.399</gml:lowerCorner>
          <gml:upperCorner>566904.004 5932832.072</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_b10a18ef-f593-4b72-a792-57081b41793a"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_80c332aa-d77b-4748-9652-b7f45c655819" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566904.004 5932828.228 566878.799 5932832.072 566876.538 5932817.244 566901.745 5932813.399 566904.004 5932828.228</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>2</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_6f779b16-20fe-4043-a328-c828dba674a7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567022.941 5932794.967</gml:lowerCorner>
          <gml:upperCorner>567079.203 5932821.212</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_6b8daf18-657d-40aa-89c0-029534abea4b"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_b0f15ed7-64ee-4979-b353-8e8c7a36c673"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_25df8a1a-bf9d-4990-8db2-288ddbd7e809" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567079.203 5932808.602 567026.182 5932821.212 567022.941 5932807.591 567035.587 5932804.581 567063.313 5932797.983 567075.983 5932794.967 567079.203 5932808.602</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ff172cb9-5612-475f-ad24-913df1c76eb1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567195.374 5932730.73</gml:lowerCorner>
          <gml:upperCorner>567229.869 5932764.51</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(B)</xplan:gliederung1>
      <xplan:gliederung2>(E)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_0772db52-ce5c-46fd-828d-edd7297e9816"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_9cb6a2f4-84b8-4323-94ec-9fcdc3bca5c4"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_7ae7199c-8628-485a-8a8d-bc43f81e4f66"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_fd656471-9a50-4e8b-8da2-af82666cde09_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_a5d62934-0c67-42df-ab71-478ecb59f350_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_e3fb49f2-0f23-47f3-bd61-ce7fdddb3c7c" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567229.869 5932757.348 567202.283 5932764.51 567195.374 5932737.898 567222.958 5932730.73 567224.591 5932737.022 567229.869 5932757.348</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_13fdf7ef-5200-4b91-995c-c5de0a4f0093">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567095.823 5932767.187</gml:lowerCorner>
          <gml:upperCorner>567112.927 5932789.338</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_c74c5ea3-a4cf-416f-a0d5-f6b39041a9c7"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_f6746f2f-5c38-4a9a-897b-bdf35dc2f0c5"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_bf393366-1de7-4e39-bb84-dc0ecc5fbc63" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567112.927 5932786.175 567100.317 5932789.338 567100.281 5932789.185 567095.823 5932770.455 567108.409 5932767.187 567112.927 5932786.175</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_0d0ea6ef-50a9-47c7-8e8a-4c16fd4fbaf4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567056.034 5932761.11</gml:lowerCorner>
          <gml:upperCorner>567083.489 5932780.007</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_d433c17c-7f10-4790-8edf-99e8de7dd6d9"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_c5cc02f9-a56f-430e-90a5-4e22c26b5593"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_b0ba6e02-4bda-4129-8008-d0286143b3c1" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567083.489 5932773.657 567070.652 5932776.99 567059.035 5932780.007 567056.034 5932767.393 567080.231 5932761.11 567083.489 5932773.657</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c5f6c94c-2390-4ee6-a167-73416b289a03">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567242.452 5932747.803</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932767.583</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_85603634-4129-4520-9d5d-0f36ba4ec383"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_eab8154e-4634-4d14-adb1-7b51e589f122"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_b09109c4-3932-4c65-8305-f0a477522ab2" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567270.174 5932761.347 567245.957 5932767.583 567242.452 5932754.081 567266.628 5932747.803 567270.174 5932761.347</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_8dbfebc6-0a1c-4ee6-be68-f52f642e5991">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567258.017 5932707.901</gml:lowerCorner>
          <gml:upperCorner>567285.65 5932727.488</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_436cc005-c54a-46db-87b4-0009f6121f90"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_69a35e30-7c8a-4da1-867b-beacd1d91a98"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_10e9020e-fc0b-4771-9f1b-4a7cd793749f" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567273.793 5932723.846 567261.31 5932727.488 567258.017 5932714.912 567282.014 5932707.901 567285.65 5932720.382 567273.793 5932723.846</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d6b5bede-11cd-4e5f-b074-23920a818290">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566899.289 5932793.365</gml:lowerCorner>
          <gml:upperCorner>566926.952 5932810.137</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_e54969cc-46f2-4361-9965-5040f418f2a5"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_1c72a098-bb0a-43a0-b815-ebc245fd4f48" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566926.952 5932806.22 566914.1 5932808.177 566901.249 5932810.137 566900.314 5932804.008 566899.289 5932797.285 566924.003 5932793.516 566924.991 5932793.365 566926.952 5932806.22</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7f8e5623-8052-47a2-8e6f-455baa906726">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566916.859 5932822.166</gml:lowerCorner>
          <gml:upperCorner>566944.546 5932841.614</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_38f73e57-9eef-4065-b564-07292912b163"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_58b2e421-2d1d-4450-8a6b-82befba6f11f" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566944.546 5932837.394 566939.864 5932838.193 566919.2 5932841.614 566916.859 5932826.266 566941.637 5932822.166 566944.546 5932837.394</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_cfbbc7f5-d158-4fb1-a87c-f795f0011c86">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567414.585 5932664.872</gml:lowerCorner>
          <gml:upperCorner>567442.096 5932683.896</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_d30778b9-209d-4939-a2cd-64f03313fc15"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_c69485a9-5ff7-4557-9648-fd65bd7e140e"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_07aa7d24-17f4-4991-a62f-a09e6f076c49" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567430.505 5932680.533 567417.947 5932683.896 567414.585 5932671.338 567438.734 5932664.872 567442.096 5932677.43 567430.505 5932680.533</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a059a827-7a77-4a4e-b930-f77ed029eee0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567423.896 5932654.275</gml:lowerCorner>
          <gml:upperCorner>567481.725 5932719.637</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_163f2452-281f-402a-b37b-94ceb90c2cbe"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_30e32b73-dff9-483e-9e12-8bcc9200146b"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_41d57d3a-91c7-4d3c-b60f-45bf0925a1d5" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567427.517 5932719.637 567423.896 5932706.113 567436.453 5932702.751 567463.984 5932695.38 567454.673 5932660.605 567478.32 5932654.275 567481.725 5932666.819 567470.593 5932669.8 567480.162 5932705.541 567427.517 5932719.637</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_45452be9-af94-408b-87c8-bfb37aeaae0d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567100.317 5932741.64</gml:lowerCorner>
          <gml:upperCorner>567155.075 5932802.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_e27efe51-548d-405a-b6f3-3d39fa616b34"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_a1ad661d-e2c5-48b3-b576-88ab29efcbaa"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_37e4bc04-beed-4a01-be9d-3888ca664c5f" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567149.128 5932791.971 567103.525 5932802.817 567100.317 5932789.338 567113.013 5932786.154 567137.502 5932780.326 567141.581 5932745.143 567155.075 5932741.64 567154.527 5932745.398 567149.128 5932791.971</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_366f3f01-dd6c-45ca-97ee-4d6dc1372c2c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566875.105 5932804.008</gml:lowerCorner>
          <gml:upperCorner>566901.745 5932817.244</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_dad5dbd9-c264-4272-a68c-86bcd2d75a10"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_182d78c7-ebc5-45f6-a3e6-c54b002d7c03" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566901.745 5932813.399 566876.538 5932817.244 566875.105 5932807.852 566900.314 5932804.008 566901.249 5932810.137 566901.745 5932813.399</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d137f77a-1da0-4217-ba41-6dae7c3ebf5e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567347.101 5932712.579</gml:lowerCorner>
          <gml:upperCorner>567403.367 5932740.198</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_f87f4c4f-5ff8-45b3-b9e5-2001ce450f37"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_ee00dc13-7e0e-44a6-b4c6-990c40489501"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_747de3f3-fbfc-425a-ae23-7992fa3f0ddd" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567350.721 5932740.198 567347.101 5932726.674 567359.658 5932723.312 567387.189 5932715.941 567399.746 5932712.579 567403.367 5932726.103 567350.721 5932740.198</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_1dfc27bb-4892-4c38-aed1-42eabc4a55dd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566938.222 5932801.853</gml:lowerCorner>
          <gml:upperCorner>566998.078 5932837.394</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_220f7fba-87d5-4b75-bbfa-5bb53f6f4f92"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_3e94ac43-7796-4f08-a888-b77863b192dc"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_0c5d9bd4-b84c-496c-9a4e-8e7baffb18fb" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566998.078 5932827.167 566944.546 5932837.394 566938.222 5932804.293 566950.991 5932801.853 566954.688 5932821.203 566982.682 5932815.855 566995.451 5932813.416 566998.078 5932827.167</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_deaaa4cb-fef0-40af-b249-a6743aa695dd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566976.546 5932778.857</gml:lowerCorner>
          <gml:upperCorner>567004.523 5932796.505</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_ec583607-38e1-4e4b-81e1-59510e28139d"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_98197c07-8b7f-4f60-b32f-7a207e87f85f" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567004.523 5932791.626 566991.754 5932794.066 566978.985 5932796.505 566976.546 5932783.737 567002.084 5932778.857 567004.523 5932791.626</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_6829362e-dfa9-445a-b18b-be68acd05371">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567266.628 5932734.009</gml:lowerCorner>
          <gml:upperCorner>567322.893 5932761.347</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_b5a075b7-a142-418c-8d84-312f843cb25b"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_dcec9cc3-ede0-40aa-8327-1983aab6647a"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_0ef9a8e7-3c41-4fc4-8139-a594e65a43a4" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567322.893 5932747.553 567270.174 5932761.347 567266.628 5932747.803 567279.2 5932744.514 567306.772 5932737.3 567319.349 5932734.009 567322.893 5932747.553</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_41292320-d863-4a1d-95d5-c8aa074b141f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567059.035 5932776.739</gml:lowerCorner>
          <gml:upperCorner>567075.96 5932797.983</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_ef5f32ed-c552-4708-b957-8f71d639d5a6"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_27f09a05-2f77-4f71-8daa-237957f2d1ef"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_b4f90aa7-ae5f-4cb0-badf-14f0028bd458" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567075.96 5932794.973 567063.313 5932797.983 567059.035 5932780.007 567070.652 5932776.99 567071.62 5932776.739 567075.96 5932794.973</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_771a4c9b-dbad-42b2-b923-e550ffd1f3b7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567075.983 5932789.185</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932808.602</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_08948a85-5183-4b8d-98c1-c493f155491d"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_a9970c7a-79d6-44ed-b6bf-5dbf42d1819a"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_399b4fef-da59-4772-8773-3e6565610e5f" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567103.525 5932802.817 567079.203 5932808.602 567075.983 5932794.967 567100.281 5932789.185 567103.525 5932802.817</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_6e0999cd-b43d-48bd-9039-4dd8373cabaa">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567417.947 5932680.533</gml:lowerCorner>
          <gml:upperCorner>567436.453 5932706.113</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_d9086011-c3c5-4582-8d8b-41df2492e8c5"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_81f9c336-b916-4cd2-81db-3bb9973748b9"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_d11dd2c9-a35a-4c12-9e44-67ec3ad0ffa2" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567436.453 5932702.751 567423.896 5932706.113 567417.947 5932683.896 567430.505 5932680.533 567436.453 5932702.751</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_8fcaa7c4-056a-4bb4-a8a8-d28fd3829d95">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567337.79 5932688.537</gml:lowerCorner>
          <gml:upperCorner>567359.658 5932726.674</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(F)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_dbf21e70-22fa-4917-8444-2918a72b9516"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_958f0336-36cc-4b3b-8e4f-31321a085b49" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567359.658 5932723.312 567347.101 5932726.674 567337.79 5932691.899 567350.348 5932688.537 567359.658 5932723.312</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_9d6fb6b8-2e1a-4eba-80b1-79d191906936">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567181.156 5932731.601</gml:lowerCorner>
          <gml:upperCorner>567245.957 5932781.173</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_71b00e63-fe33-4ba5-b6f1-0529b9525728"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_8ef2b2e5-3f88-4fb5-aee9-2741d7305301"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_7e67f5a0-6589-457e-9c4c-37aae0ee874d" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567202.283 5932764.51 567229.869 5932757.348 567224.591 5932737.022 567237.174 5932733.755 567245.957 5932767.583 567193.176 5932781.173 567181.156 5932734.868 567193.739 5932731.601 567202.283 5932764.51</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_9d384060-a110-44cd-b0eb-8daca2c88662">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566858.112 5932800.141</gml:lowerCorner>
          <gml:upperCorner>566919.2 5932849.831</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_23c8e2cb-eea0-4ede-8380-e4bd444a2c27"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_35cf4346-ed26-4a9f-b266-1a4ed64089a9"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_ae05c9fa-c278-4cf0-b7d3-79c3fe4ef417" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566914.1 5932808.177 566919.2 5932841.614 566865.323 5932849.831 566858.112 5932802.554 566873.929 5932800.141 566878.799 5932832.072 566904.004 5932828.228 566901.249 5932810.137 566914.1 5932808.177</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_3dc269a7-9889-48ae-969a-f6653030938c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567377.878 5932674.7</gml:lowerCorner>
          <gml:upperCorner>567405.389 5932693.724</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_cb7de8de-5680-4187-9c99-4f195bfeb807"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_7cb373be-48bc-4644-87be-eaf3969051a0"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_50990ad8-d5ea-499b-90ff-4b5fecc11fd1" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567405.389 5932687.258 567393.798 5932690.361 567381.24 5932693.724 567377.878 5932681.166 567402.027 5932674.7 567405.389 5932687.258</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c3036682-fcff-4418-b943-bcee53271588">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566935.783 5932786.645</gml:lowerCorner>
          <gml:upperCorner>566963.761 5932804.293</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_5e016c20-61a9-45a7-af55-41b9815bf89c"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_8e822ffe-e6ef-464e-9cdc-63b9fb255db6" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>566963.761 5932799.414 566950.991 5932801.853 566938.222 5932804.293 566935.783 5932791.524 566961.321 5932786.645 566963.761 5932799.414</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_5083db57-c8e8-457e-8dc7-60a7ccb34a0d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567297.868 5932696.257</gml:lowerCorner>
          <gml:upperCorner>567325.501 5932737.3</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(F)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_b50cd749-911b-4edc-a389-6e54140a69c2"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_10bd07bf-837f-41a5-80d7-a4d2b6e9298b" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567313.643 5932712.203 567319.349 5932734.009 567306.772 5932737.3 567297.868 5932703.269 567321.864 5932696.257 567325.501 5932708.738 567313.643 5932712.203</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_23ed66cd-0484-497a-a0ae-aaafa4a7febc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567092.822 5932751.558</gml:lowerCorner>
          <gml:upperCorner>567120.277 5932770.455</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_a82100c3-cc2a-47ed-82b6-a1ad280cbdc3"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_32c05c56-5bfe-4302-b070-239b322ba5a4"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_05bd364e-7567-41d6-99b7-63b9212206b7" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567120.277 5932764.106 567108.409 5932767.187 567095.823 5932770.455 567092.822 5932757.841 567117.019 5932751.558 567120.277 5932764.106</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_69086e3c-5a9e-4202-83b3-f88167bbd387">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.503 5932773.896</gml:lowerCorner>
          <gml:upperCorner>567035.587 5932807.591</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(F)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_387b0ade-f5d2-4c53-80cc-6ad7bcf40811"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_005a9261-488d-411b-8f9c-c01c5a7bb88e" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567035.587 5932804.581 567022.941 5932807.591 567015.503 5932776.34 567028.284 5932773.896 567035.587 5932804.581</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_77e5eecd-f6ca-4fb0-98cc-f1d1212b06a6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567221.324 5932718.156</gml:lowerCorner>
          <gml:upperCorner>567248.789 5932737.022</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_bacae243-8786-4292-94fc-651f36ff6494"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_1afbafa0-d4f6-4227-8797-4f2c7bfd93a2"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_5e231b1d-9059-4f54-9b8e-d570ddd2f6c4" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567237.174 5932733.755 567224.591 5932737.022 567222.958 5932730.73 567221.324 5932724.439 567245.522 5932718.156 567248.789 5932730.739 567237.174 5932733.755</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_4354adf9-4004-4283-98f9-aca57842955d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567399.746 5932706.113</gml:lowerCorner>
          <gml:upperCorner>567427.517 5932726.103</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_33e3de60-aecb-4593-aa85-bebac94e0bcb"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_7f19c2df-be1d-42b6-bae1-a6907a19a10b"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_dffa37f2-988f-44a8-9c6a-825136e45e86" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567427.517 5932719.637 567403.367 5932726.103 567399.746 5932712.579 567423.896 5932706.113 567427.517 5932719.637</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_23c8e2cb-eea0-4ede-8380-e4bd444a2c27">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566869.626 5932821.486</gml:lowerCorner>
          <gml:upperCorner>566870.626 5932822.486</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_9d384060-a110-44cd-b0eb-8daca2c88662"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_044172a7-4ffb-4e36-909a-a5411479f23c" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>566869.626 5932821.486</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a82100c3-cc2a-47ed-82b6-a1ad280cbdc3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567106.486 5932763.117</gml:lowerCorner>
          <gml:upperCorner>567107.486 5932764.117</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_23ed66cd-0484-497a-a0ae-aaafa4a7febc"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_124bfb8c-f178-4e7d-87a7-04a4afd32741" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567106.486 5932763.117</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ec583607-38e1-4e4b-81e1-59510e28139d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566990.534 5932787.681</gml:lowerCorner>
          <gml:upperCorner>566991.534 5932788.681</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_deaaa4cb-fef0-40af-b249-a6743aa695dd"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_e409c580-4876-4707-97d0-98906f02d5d5" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>566990.534 5932787.681</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ba047098-ec96-43b6-aed7-9906e6a1bfff">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567269.576 5932735.532</gml:lowerCorner>
          <gml:upperCorner>567270.576 5932736.532</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_087a1120-98af-4731-9053-a097ba5abec0"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_bb4241ad-b44d-4129-8118-f57ec738f2e0" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567269.576 5932735.532</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_6b8daf18-657d-40aa-89c0-029534abea4b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567051.077 5932810.386</gml:lowerCorner>
          <gml:upperCorner>567052.077 5932811.386</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_6f779b16-20fe-4043-a328-c828dba674a7"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_0f31e481-83a8-4c53-b90d-ed77cc405f04" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567051.077 5932810.386</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ef5f32ed-c552-4708-b957-8f71d639d5a6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567067.482 5932789.553</gml:lowerCorner>
          <gml:upperCorner>567068.482 5932790.553</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_41292320-d863-4a1d-95d5-c8aa074b141f"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_2fd3447a-0396-45c6-a41f-d09947846c1f" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567067.482 5932789.553</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_71b00e63-fe33-4ba5-b6f1-0529b9525728">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567217.826 5932764.134</gml:lowerCorner>
          <gml:upperCorner>567218.826 5932765.134</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_9d6fb6b8-2e1a-4eba-80b1-79d191906936"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_93da4ace-4218-4397-ae6a-fe849b49f4c6" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567217.826 5932764.134</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0a93742a-3a7a-4ebb-a203-26f465b0c0d8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567390.493 5932699.938</gml:lowerCorner>
          <gml:upperCorner>567391.493 5932700.938</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_7803ad38-1d7f-40eb-acd0-9f61d0149d94"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_aa03e087-273f-4734-8c6a-36055d87b21e" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567390.493 5932699.938</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e54969cc-46f2-4361-9965-5040f418f2a5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566918.914 5932800.868</gml:lowerCorner>
          <gml:upperCorner>566919.914 5932801.868</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_d6b5bede-11cd-4e5f-b074-23920a818290"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_6d66b72f-45ab-40f9-bf82-93821c16b774" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>566918.914 5932800.868</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_163f2452-281f-402a-b37b-94ceb90c2cbe">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567452.029 5932702.327</gml:lowerCorner>
          <gml:upperCorner>567453.029 5932703.327</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_a059a827-7a77-4a4e-b930-f77ed029eee0"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_921cb7d3-29a4-43aa-99dd-12f700ea66cb" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567452.029 5932702.327</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_08948a85-5183-4b8d-98c1-c493f155491d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567089.748 5932795.393</gml:lowerCorner>
          <gml:upperCorner>567090.748 5932796.393</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_771a4c9b-dbad-42b2-b923-e550ffd1f3b7"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_abf2af6e-9568-4e11-86bd-1590deaa9490" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567089.748 5932795.393</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_35cf4346-ed26-4a9f-b266-1a4ed64089a9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566869.626 5932827.281</gml:lowerCorner>
          <gml:upperCorner>566870.626 5932828.281</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_9d384060-a110-44cd-b0eb-8daca2c88662"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_31774a59-55ac-422e-8b0e-b3a8dc766c96" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>566869.626 5932827.281</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_32c05c56-5bfe-4302-b070-239b322ba5a4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567106.486 5932757.776</gml:lowerCorner>
          <gml:upperCorner>567107.486 5932758.776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_23ed66cd-0484-497a-a0ae-aaafa4a7febc"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_3d099984-1d92-45e7-a66e-4372acff8d16" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567106.486 5932757.776</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_387b0ade-f5d2-4c53-80cc-6ad7bcf40811">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567025.58 5932790.605</gml:lowerCorner>
          <gml:upperCorner>567026.58 5932791.605</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_69086e3c-5a9e-4202-83b3-f88167bbd387"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_a759a957-c9c2-4349-8779-5ba5162f0d59" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567025.58 5932790.605</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b50cd749-911b-4edc-a389-6e54140a69c2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567311.6 5932716.843</gml:lowerCorner>
          <gml:upperCorner>567312.6 5932717.843</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_5083db57-c8e8-457e-8dc7-60a7ccb34a0d"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_a1baf9fc-8786-4b0c-8618-6eee84e4ab93" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567311.6 5932716.843</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a44c6563-dd2f-4734-8c68-1353654452a1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566930.0 5932818.487</gml:lowerCorner>
          <gml:upperCorner>566931.0 5932819.487</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_2aa18469-6fc9-4128-86e5-0c27d251ca8e"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_23fd6dda-ba0a-494d-a5a7-9eb7ca0afed0" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>566930.0 5932818.487</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3ea9a702-2f78-4b09-9500-ab448539d957">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567269.576 5932730.191</gml:lowerCorner>
          <gml:upperCorner>567270.576 5932731.191</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_087a1120-98af-4731-9053-a097ba5abec0"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_3e182432-f95d-4229-8a9b-e4d2f33b1b14" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567269.576 5932730.191</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c6d432bc-5dcc-481d-b7c1-a806cd3d767f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567374.551 5932707.847</gml:lowerCorner>
          <gml:upperCorner>567375.551 5932707.847</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_fbcbb2d0-0fb6-4699-8f79-e1fb8d3be5e9"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_c1dd2130-cd93-428e-b574-ecdf442059b0" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567374.551 5932707.847</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_dbf21e70-22fa-4917-8444-2918a72b9516">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567348.724 5932707.606</gml:lowerCorner>
          <gml:upperCorner>567349.724 5932708.606</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_8fcaa7c4-056a-4bb4-a8a8-d28fd3829d95"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_4568ba50-ed64-4716-8b1a-eaa2b9953c18" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567348.724 5932707.606</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_436cc005-c54a-46db-87b4-0009f6121f90">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567273.005 5932714.09</gml:lowerCorner>
          <gml:upperCorner>567274.005 5932715.09</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_8dbfebc6-0a1c-4ee6-be68-f52f642e5991"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_7633add8-935f-4990-9355-971703d9b00d" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567273.005 5932714.09</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d9086011-c3c5-4582-8d8b-41df2492e8c5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567427.2 5932690.109</gml:lowerCorner>
          <gml:upperCorner>567428.2 5932691.109</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_6e0999cd-b43d-48bd-9039-4dd8373cabaa"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_190575e2-3c44-46c1-bbef-aa3a7851b175" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567427.2 5932690.109</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f3822531-3f07-45c9-9251-1a538887b558">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567124.376 5932769.996</gml:lowerCorner>
          <gml:upperCorner>567125.376 5932770.996</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_01e35ac7-7837-4b85-8d85-e79ee17191ac"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_bc5f6816-b126-48e3-bac1-48cb1ee66188" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567124.376 5932769.996</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d30778b9-209d-4939-a2cd-64f03313fc15">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.341 5932671.17</gml:lowerCorner>
          <gml:upperCorner>567429.341 5932672.17</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_cfbbc7f5-d158-4fb1-a87c-f795f0011c86"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_9f31d252-a1a3-4b9c-83a8-79d06ff39d51" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567428.341 5932671.17</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_38f73e57-9eef-4065-b564-07292912b163">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566929.173 5932832.096</gml:lowerCorner>
          <gml:upperCorner>566930.173 5932833.096</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_7f8e5623-8052-47a2-8e6f-455baa906726"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_765cf4f3-71bf-4d06-8e07-123532bf8ccc" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>566929.173 5932832.096</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b0f15ed7-64ee-4979-b353-8e8c7a36c673">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567051.077 5932804.592</gml:lowerCorner>
          <gml:upperCorner>567052.077 5932805.592</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_6f779b16-20fe-4043-a328-c828dba674a7"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_e211302d-939b-4468-9755-7980e2ea8f57" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567051.077 5932804.592</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d5d2be62-0d59-42f6-a64a-84924879b589">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567238.324 5932744.791</gml:lowerCorner>
          <gml:upperCorner>567239.324 5932745.791</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4e2251e-6329-4655-9aa4-f4bbaa18be04"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_0eed0164-0a83-49aa-8fe3-21e691786804" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567238.324 5932744.791</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_8ef2b2e5-3f88-4fb5-aee9-2741d7305301">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567217.826 5932769.928</gml:lowerCorner>
          <gml:upperCorner>567218.826 5932770.928</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_9d6fb6b8-2e1a-4eba-80b1-79d191906936"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_b5ae0409-d7c3-4026-a6a6-0af463ba4f83" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567217.826 5932769.928</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f87f4c4f-5ff8-45b3-b9e5-2001ce450f37">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567375.234 5932728.683</gml:lowerCorner>
          <gml:upperCorner>567376.234 5932729.683</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_d137f77a-1da0-4217-ba41-6dae7c3ebf5e"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_9a50e4c9-7896-4958-8d29-ac4c04cebd82" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567375.234 5932728.683</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1149c0ca-dbaf-49f3-b297-d4313ddefa67">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567124.376 5932764.202</gml:lowerCorner>
          <gml:upperCorner>567125.376 5932765.202</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_01e35ac7-7837-4b85-8d85-e79ee17191ac"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_6ec969c5-bbe3-41ec-a926-02393c4b26d2" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567124.376 5932764.202</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_babf4409-ca64-454b-8629-74ddfc39a247">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566987.218 5932804.96</gml:lowerCorner>
          <gml:upperCorner>566988.218 5932805.96</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_9d909301-e146-48ee-a590-c3982c2a5cdf"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_cab9cf9a-6ed5-4840-94c4-1c1c11b041c3" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>566987.218 5932804.96</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a9970c7a-79d6-44ed-b6bf-5dbf42d1819a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567089.748 5932801.187</gml:lowerCorner>
          <gml:upperCorner>567090.748 5932802.187</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_771a4c9b-dbad-42b2-b923-e550ffd1f3b7"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_eff90649-5de5-43b1-8b9d-edad29c85231" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567089.748 5932801.187</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_6f2da464-0064-4c8f-8b74-1970b9476e33">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567390.493 5932705.278</gml:lowerCorner>
          <gml:upperCorner>567391.493 5932706.278</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_7803ad38-1d7f-40eb-acd0-9f61d0149d94"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_635a56f5-8eac-41a2-ab77-1669b1acb8b4" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567390.493 5932705.278</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_dfa7c1d9-239c-44d0-9e82-827dc9b09876">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567133.905 5932761.277</gml:lowerCorner>
          <gml:upperCorner>567134.905 5932762.277</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_361d84aa-b947-414a-8d43-f26733aed4d5"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_91d68d37-8a51-4630-a8a0-816b14521107" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567133.905 5932761.277</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b5a075b7-a142-418c-8d84-312f843cb25b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567294.761 5932744.178</gml:lowerCorner>
          <gml:upperCorner>567295.761 5932745.178</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_6829362e-dfa9-445a-b18b-be68acd05371"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_591e61bb-7c75-45d0-979f-31297a4de118" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567294.761 5932744.178</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_33e3de60-aecb-4593-aa85-bebac94e0bcb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567413.631 5932718.402</gml:lowerCorner>
          <gml:upperCorner>567414.631 5932719.402</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_4354adf9-4004-4283-98f9-aca57842955d"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_2e062242-c2a4-4d93-ac8f-fbc482bec64c" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567413.631 5932718.402</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_220f7fba-87d5-4b75-bbfa-5bb53f6f4f92">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566969.999 5932827.699</gml:lowerCorner>
          <gml:upperCorner>566970.999 5932828.699</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_1dfc27bb-4892-4c38-aed1-42eabc4a55dd"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_11406bc3-2eef-4b14-91e7-afde20e2ef87" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>566969.999 5932827.699</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_27f09a05-2f77-4f71-8daa-237957f2d1ef">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567067.482 5932784.212</gml:lowerCorner>
          <gml:upperCorner>567068.482 5932785.212</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_41292320-d863-4a1d-95d5-c8aa074b141f"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_53d671f2-3b73-4af4-8d62-60554e78c8c8" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567067.482 5932784.212</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d433c17c-7f10-4790-8edf-99e8de7dd6d9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567069.698 5932772.669</gml:lowerCorner>
          <gml:upperCorner>567070.698 5932773.669</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_0d0ea6ef-50a9-47c7-8e8a-4c16fd4fbaf4"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_a5915750-52c4-4467-9189-c5840b0356d6" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567069.698 5932772.669</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_cb7de8de-5680-4187-9c99-4f195bfeb807">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567391.634 5932680.998</gml:lowerCorner>
          <gml:upperCorner>567392.634 5932681.998</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_3dc269a7-9889-48ae-969a-f6653030938c"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_4a14c7b0-c395-456b-9c91-f3160b2bae5d" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567391.634 5932680.998</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_69a35e30-7c8a-4da1-867b-beacd1d91a98">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567273.005 5932719.431</gml:lowerCorner>
          <gml:upperCorner>567274.005 5932720.431</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_8dbfebc6-0a1c-4ee6-be68-f52f642e5991"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_f331db9b-b5e7-499b-86cc-c9bbc3cff759" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567273.005 5932719.431</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5e016c20-61a9-45a7-af55-41b9815bf89c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566949.772 5932795.469</gml:lowerCorner>
          <gml:upperCorner>566950.772 5932796.469</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_c3036682-fcff-4418-b943-bcee53271588"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_276c94e4-c01b-45c1-8430-331364f8f67e" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>566949.772 5932795.469</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e27efe51-548d-405a-b6f3-3d39fa616b34">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567128.362 5932786.204</gml:lowerCorner>
          <gml:upperCorner>567129.362 5932787.204</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_45452be9-af94-408b-87c8-bfb37aeaae0d"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_c3de43a2-4e69-40ed-824e-ed935bb5f785" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567128.362 5932786.204</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c5cc02f9-a56f-430e-90a5-4e22c26b5593">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567069.698 5932767.328</gml:lowerCorner>
          <gml:upperCorner>567070.698 5932768.328</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_0d0ea6ef-50a9-47c7-8e8a-4c16fd4fbaf4"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_91bedd17-6236-4e6e-81a8-8d54f0771e8f" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567069.698 5932767.328</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a9ddcedf-e590-476d-bc67-720015f9a23e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567133.905 5932767.233</gml:lowerCorner>
          <gml:upperCorner>567134.905 5932768.233</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_361d84aa-b947-414a-8d43-f26733aed4d5"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_d7a5b5cb-c598-4c6d-a4af-7f49aa46b203" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567133.905 5932767.233</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_85603634-4129-4520-9d5d-0f36ba4ec383">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567256.304 5932754.198</gml:lowerCorner>
          <gml:upperCorner>567257.304 5932755.198</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_c5f6c94c-2390-4ee6-a167-73416b289a03"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_dd126c75-742e-4207-b83f-7beea0f18beb" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567256.304 5932754.198</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ee00dc13-7e0e-44a6-b4c6-990c40489501">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567375.234 5932722.888</gml:lowerCorner>
          <gml:upperCorner>567376.234 5932723.888</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_d137f77a-1da0-4217-ba41-6dae7c3ebf5e"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_1161c997-1ae1-42db-81ad-a4848765c66e" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567375.234 5932722.888</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c74c5ea3-a4cf-416f-a0d5-f6b39041a9c7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567104.369 5932780.416</gml:lowerCorner>
          <gml:upperCorner>567105.369 5932781.416</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_13fdf7ef-5200-4b91-995c-c5de0a4f0093"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_24bb105a-6303-44b7-9479-9184fc1025ac" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567104.369 5932780.416</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ce8ff976-ddbe-4b15-84d9-e7e0c3762308">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567374.551 5932707.847</gml:lowerCorner>
          <gml:upperCorner>567375.551 5932707.847</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_fbcbb2d0-0fb6-4699-8f79-e1fb8d3be5e9"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_0828801e-fd96-439d-b130-48bacf078afc" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567374.551 5932707.847</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9cb6a2f4-84b8-4323-94ec-9fcdc3bca5c4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567212.621 5932744.409</gml:lowerCorner>
          <gml:upperCorner>567213.621 5932745.409</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_ff172cb9-5612-475f-ad24-913df1c76eb1"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_d1717352-2cdd-4cf0-ade5-a56e7f3b6e7e" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567212.621 5932744.409</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_dad5dbd9-c264-4272-a68c-86bcd2d75a10">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566888.426 5932810.626</gml:lowerCorner>
          <gml:upperCorner>566889.426 5932811.626</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_366f3f01-dd6c-45ca-97ee-4d6dc1372c2c"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_cc9081d4-b574-4845-96b7-75c70fca196a" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>566888.426 5932810.626</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7f19c2df-be1d-42b6-bae1-a6907a19a10b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567413.631 5932712.608</gml:lowerCorner>
          <gml:upperCorner>567414.631 5932713.608</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_4354adf9-4004-4283-98f9-aca57842955d"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_f484a73b-3580-4781-8fb8-4d2da683876a" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567413.631 5932712.608</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7c2071c8-7621-4fb4-8943-57d771fa0d20">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566928.39 5932812.523</gml:lowerCorner>
          <gml:upperCorner>566929.39 5932813.523</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_2aa18469-6fc9-4128-86e5-0c27d251ca8e"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_dc1624b8-a61c-4832-8fb5-d3dca25eb686" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>566928.39 5932812.523</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7cb373be-48bc-4644-87be-eaf3969051a0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567391.634 5932686.339</gml:lowerCorner>
          <gml:upperCorner>567392.634 5932687.339</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_3dc269a7-9889-48ae-969a-f6653030938c"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_fe1def99-f4a0-4287-aac3-6b83d1201fd8" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567391.634 5932686.339</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_bacae243-8786-4292-94fc-651f36ff6494">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567235.057 5932724.375</gml:lowerCorner>
          <gml:upperCorner>567236.057 5932725.375</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_77e5eecd-f6ca-4fb0-98cc-f1d1212b06a6"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_3f348412-521c-42a7-afa1-0a7f47a1fba7" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567235.057 5932724.375</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f6746f2f-5c38-4a9a-897b-bdf35dc2f0c5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567104.369 5932775.075</gml:lowerCorner>
          <gml:upperCorner>567105.369 5932776.075</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_13fdf7ef-5200-4b91-995c-c5de0a4f0093"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_6e8ded7e-e24f-40ce-97e8-6acce6138e22" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567104.369 5932775.075</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7ae7199c-8628-485a-8a8d-bc43f81e4f66">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567212.621 5932738.615</gml:lowerCorner>
          <gml:upperCorner>567213.621 5932739.615</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_ff172cb9-5612-475f-ad24-913df1c76eb1"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_5fd117ff-2bfb-4daa-9746-f3e8667bc85f" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567212.621 5932738.615</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_30e32b73-dff9-483e-9e12-8bcc9200146b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567452.029 5932708.121</gml:lowerCorner>
          <gml:upperCorner>567453.029 5932709.121</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_a059a827-7a77-4a4e-b930-f77ed029eee0"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_f424a818-6b6d-4742-8cda-59b6bf590769" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567452.029 5932708.121</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3e94ac43-7796-4f08-a888-b77863b192dc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566969.999 5932821.905</gml:lowerCorner>
          <gml:upperCorner>566970.999 5932822.905</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_1dfc27bb-4892-4c38-aed1-42eabc4a55dd"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_a7384b2d-f2a5-4ada-a26e-b56a831fd7c8" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>566969.999 5932821.905</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_dcec9cc3-ede0-40aa-8327-1983aab6647a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567294.761 5932749.973</gml:lowerCorner>
          <gml:upperCorner>567295.761 5932750.973</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_6829362e-dfa9-445a-b18b-be68acd05371"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_db43e5f6-5cbe-4e0a-9d5f-802f5e4a1fc6" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567294.761 5932749.973</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1afbafa0-d4f6-4227-8797-4f2c7bfd93a2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567235.057 5932729.716</gml:lowerCorner>
          <gml:upperCorner>567236.057 5932730.716</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_77e5eecd-f6ca-4fb0-98cc-f1d1212b06a6"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_a5e84028-e2cb-42ad-bbc1-e302ac69b1f5" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567235.057 5932729.716</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_81f9c336-b916-4cd2-81db-3bb9973748b9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567427.2 5932695.45</gml:lowerCorner>
          <gml:upperCorner>567428.2 5932696.45</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_6e0999cd-b43d-48bd-9039-4dd8373cabaa"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_5933cc62-aaa3-453f-95ac-cb9be1af6f71" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567427.2 5932695.45</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a1ad661d-e2c5-48b3-b576-88ab29efcbaa">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567128.362 5932791.999</gml:lowerCorner>
          <gml:upperCorner>567129.362 5932792.999</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_45452be9-af94-408b-87c8-bfb37aeaae0d"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_d71e8dc8-dbfb-46a6-b3d9-d8478ffcec7c" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567128.362 5932791.999</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_eab8154e-4634-4d14-adb1-7b51e589f122">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567256.304 5932759.992</gml:lowerCorner>
          <gml:upperCorner>567257.304 5932760.992</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_c5f6c94c-2390-4ee6-a167-73416b289a03"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_a0613821-fbe8-4c6b-98af-e7eb5af8afb8" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567256.304 5932759.992</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e7af056f-472c-47be-848d-7b7c5f3c41a0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567238.325 5932744.79</gml:lowerCorner>
          <gml:upperCorner>567239.325 5932744.79</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4e2251e-6329-4655-9aa4-f4bbaa18be04"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_62c855a5-430f-4703-9094-b279e93791ea" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567238.325 5932744.79</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_44ee80ca-f0f0-4aee-acde-ef863d6fedc6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567368.388 5932699.843</gml:lowerCorner>
          <gml:upperCorner>567369.388 5932700.843</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_fbcbb2d0-0fb6-4699-8f79-e1fb8d3be5e9"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_e9705a09-ffec-4087-b820-af35f798aebf" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567368.388 5932699.843</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c69485a9-5ff7-4557-9648-fd65bd7e140e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.341 5932676.511</gml:lowerCorner>
          <gml:upperCorner>567429.341 5932677.511</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_cfbbc7f5-d158-4fb1-a87c-f795f0011c86"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_3c53ec93-b4da-40dc-a0da-cf08368c0da2" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>567428.341 5932676.511</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b10a18ef-f593-4b72-a792-57081b41793a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566890.271 5932822.736</gml:lowerCorner>
          <gml:upperCorner>566891.271 5932823.736</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:dientZurDarstellungVon xlink:href="#GML_7413c4ed-9475-45fd-9492-7a29739d0ef8"></xplan:dientZurDarstellungVon>
      <xplan:position>
        <gml:Point gml:id="_21403b88-4d99-47e8-a664-25afd6e50b57" srsName="EPSG:25832" srsDimension="2">
          <gml:pos>566890.271 5932822.736</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_361d84aa-b947-414a-8d43-f26733aed4d5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567092.822 5932745.398</gml:lowerCorner>
          <gml:upperCorner>567154.527 5932802.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_dfa7c1d9-239c-44d0-9e82-827dc9b09876"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_a9ddcedf-e590-476d-bc67-720015f9a23e"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c795e4d7-d3e7-4b85-a8d6-b6cc1e6feba2_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_6450304f-6c22-4cc9-8a19-3e98e46eed8a_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_4f96ce33-c275-4311-9ea1-737d64b48b78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_0a31d433-388a-43b7-8815-b0c3b0eea5cf_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_0958e0ae-8310-4eb4-9586-7df30e9c78de_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7768f4e1-2504-4f08-9c4a-1b9e3ab5ed6e_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_b2d9cad2-18da-4d71-9e2b-e4e3dc0de2d1_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_2c4e8d5b-d2f3-4adb-b554-efe208d803af_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d03d15de-716d-4215-b4bb-f6866dc31699_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_4231d2e4-1587-42db-9621-050e3eba8e62_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_4436c60e-ce8d-4fbf-88fa-a464667ec0e9_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_9a1336c1-2d19-4f4a-8cdb-8ce7b44d7b5d_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_c2d35123-8af2-4cf8-8325-13c37fbd6830_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_c0ad16b1-6dcc-423e-ba6c-c8e31f8f867e_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_40708916-979d-4b80-a8bf-274dac77799c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d854f146-551d-4d47-a76e-3125c7e50abd_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_afff76f6-49c8-449e-8b0a-fe6fdd7bfb1a_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_a5d62934-0c67-42df-ab71-478ecb59f350_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_58533c21-7769-4f1e-a44b-a5c5da83d571" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567149.128 5932791.971 567103.525 5932802.817 567092.822 5932757.841 567117.019 5932751.558 567117.89 5932754.911 567154.527 5932745.398 567149.128 5932791.971</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_2aa18469-6fc9-4128-86e5-0c27d251ca8e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566842.603 5932757.841</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932852.383</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_a44c6563-dd2f-4734-8c68-1353654452a1"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_7c2071c8-7621-4fb4-8943-57d771fa0d20"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c795e4d7-d3e7-4b85-a8d6-b6cc1e6feba2_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_6450304f-6c22-4cc9-8a19-3e98e46eed8a_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_4f96ce33-c275-4311-9ea1-737d64b48b78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_0a31d433-388a-43b7-8815-b0c3b0eea5cf_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_0958e0ae-8310-4eb4-9586-7df30e9c78de_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7768f4e1-2504-4f08-9c4a-1b9e3ab5ed6e_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_b2d9cad2-18da-4d71-9e2b-e4e3dc0de2d1_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_2c4e8d5b-d2f3-4adb-b554-efe208d803af_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d03d15de-716d-4215-b4bb-f6866dc31699_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_4231d2e4-1587-42db-9621-050e3eba8e62_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_4436c60e-ce8d-4fbf-88fa-a464667ec0e9_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_9a1336c1-2d19-4f4a-8cdb-8ce7b44d7b5d_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_c2d35123-8af2-4cf8-8325-13c37fbd6830_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_c0ad16b1-6dcc-423e-ba6c-c8e31f8f867e_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_40708916-979d-4b80-a8bf-274dac77799c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d854f146-551d-4d47-a76e-3125c7e50abd_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_afff76f6-49c8-449e-8b0a-fe6fdd7bfb1a_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_a5d62934-0c67-42df-ab71-478ecb59f350_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_bc03968f-88eb-4c87-a99a-aa5b641e509c" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567013.667 5932824.189 566944.512 5932837.424 566919.171 5932841.619 566848.591 5932852.383 566842.603 5932805.784 566858.262 5932803.586 566899.326 5932797.323 566923.976 5932793.564 566925.489 5932793.308 566935.752 5932791.578 567004.845 5932778.378 567038.535 5932771.936 567092.822 5932757.841 567103.525 5932802.817 567013.667 5932824.189</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.7</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_e4e2251e-6329-4655-9aa4-f4bbaa18be04">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567181.156 5932714.912</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932781.173</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_d5d2be62-0d59-42f6-a64a-84924879b589"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_e7af056f-472c-47be-848d-7b7c5f3c41a0"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c795e4d7-d3e7-4b85-a8d6-b6cc1e6feba2_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_6450304f-6c22-4cc9-8a19-3e98e46eed8a_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_0a31d433-388a-43b7-8815-b0c3b0eea5cf_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_0958e0ae-8310-4eb4-9586-7df30e9c78de_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7768f4e1-2504-4f08-9c4a-1b9e3ab5ed6e_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_b2d9cad2-18da-4d71-9e2b-e4e3dc0de2d1_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_2c4e8d5b-d2f3-4adb-b554-efe208d803af_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d03d15de-716d-4215-b4bb-f6866dc31699_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_4231d2e4-1587-42db-9621-050e3eba8e62_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_4436c60e-ce8d-4fbf-88fa-a464667ec0e9_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_9a1336c1-2d19-4f4a-8cdb-8ce7b44d7b5d_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_c2d35123-8af2-4cf8-8325-13c37fbd6830_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_c0ad16b1-6dcc-423e-ba6c-c8e31f8f867e_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_40708916-979d-4b80-a8bf-274dac77799c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d854f146-551d-4d47-a76e-3125c7e50abd_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_afff76f6-49c8-449e-8b0a-fe6fdd7bfb1a_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_a5d62934-0c67-42df-ab71-478ecb59f350_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_734f5f0f-8306-4423-a4d9-172708b595a1" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567270.174 5932761.347 567245.957 5932767.583 567193.176 5932781.173 567181.156 5932734.868 567258.017 5932714.912 567270.174 5932761.347</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.8</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_fbcbb2d0-0fb6-4699-8f79-e1fb8d3be5e9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567258.017 5932654.273</gml:lowerCorner>
          <gml:upperCorner>567491.419 5932761.347</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156"></xplan:gehoertZuBereich>
      <xplan:wirdDargestelltDurch xlink:href="#GML_c6d432bc-5dcc-481d-b7c1-a806cd3d767f"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_ce8ff976-ddbe-4b15-84d9-e7e0c3762308"></xplan:wirdDargestelltDurch>
      <xplan:wirdDargestelltDurch xlink:href="#GML_44ee80ca-f0f0-4aee-acde-ef863d6fedc6"></xplan:wirdDargestelltDurch>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c795e4d7-d3e7-4b85-a8d6-b6cc1e6feba2_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_6450304f-6c22-4cc9-8a19-3e98e46eed8a_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_4f96ce33-c275-4311-9ea1-737d64b48b78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_0a31d433-388a-43b7-8815-b0c3b0eea5cf_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_0958e0ae-8310-4eb4-9586-7df30e9c78de_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_7768f4e1-2504-4f08-9c4a-1b9e3ab5ed6e_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_b2d9cad2-18da-4d71-9e2b-e4e3dc0de2d1_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_2c4e8d5b-d2f3-4adb-b554-efe208d803af_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d03d15de-716d-4215-b4bb-f6866dc31699_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_4231d2e4-1587-42db-9621-050e3eba8e62_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_4436c60e-ce8d-4fbf-88fa-a464667ec0e9_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_9a1336c1-2d19-4f4a-8cdb-8ce7b44d7b5d_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_c2d35123-8af2-4cf8-8325-13c37fbd6830_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_c0ad16b1-6dcc-423e-ba6c-c8e31f8f867e_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_40708916-979d-4b80-a8bf-274dac77799c_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_d854f146-551d-4d47-a76e-3125c7e50abd_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_afff76f6-49c8-449e-8b0a-fe6fdd7bfb1a_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f_BP"></xplan:refTextInhalt>
      <xplan:refTextInhalt xlink:href="#GML_a5d62934-0c67-42df-ab71-478ecb59f350_BP"></xplan:refTextInhalt>
      <xplan:position>
        <gml:Polygon gml:id="_6fa39f1d-8054-430a-9c19-7ee2c630c68d" srsName="EPSG:25832" srsDimension="2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>567338.647 5932743.431 567270.174 5932761.347 567258.017 5932714.912 567325.715 5932695.132 567414.585 5932671.338 567478.319 5932654.273 567491.419 5932702.527 567427.516 5932719.637 567338.647 5932743.431</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.7</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567399.746 5932706.113</gml:lowerCorner>
          <gml:upperCorner>567245.957 5932781.173</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:rasterBasis xlink:href="#GML_f40519da-3ea5-4713-944e-c530690d3a0c"></xplan:rasterBasis>
      <xplan:praesentationsobjekt xlink:href="#GML_23c8e2cb-eea0-4ede-8380-e4bd444a2c27"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_a82100c3-cc2a-47ed-82b6-a1ad280cbdc3"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_ec583607-38e1-4e4b-81e1-59510e28139d"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_ba047098-ec96-43b6-aed7-9906e6a1bfff"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_6b8daf18-657d-40aa-89c0-029534abea4b"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_ef5f32ed-c552-4708-b957-8f71d639d5a6"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_71b00e63-fe33-4ba5-b6f1-0529b9525728"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_0a93742a-3a7a-4ebb-a203-26f465b0c0d8"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_e54969cc-46f2-4361-9965-5040f418f2a5"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_163f2452-281f-402a-b37b-94ceb90c2cbe"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_08948a85-5183-4b8d-98c1-c493f155491d"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_35cf4346-ed26-4a9f-b266-1a4ed64089a9"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_32c05c56-5bfe-4302-b070-239b322ba5a4"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_387b0ade-f5d2-4c53-80cc-6ad7bcf40811"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_b50cd749-911b-4edc-a389-6e54140a69c2"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_a44c6563-dd2f-4734-8c68-1353654452a1"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_0772db52-ce5c-46fd-828d-edd7297e9816"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_3ea9a702-2f78-4b09-9500-ab448539d957"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_c6d432bc-5dcc-481d-b7c1-a806cd3d767f"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_dbf21e70-22fa-4917-8444-2918a72b9516"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_436cc005-c54a-46db-87b4-0009f6121f90"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_d9086011-c3c5-4582-8d8b-41df2492e8c5"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_8da28984-778e-4e95-b90d-64afa790714a"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_f3822531-3f07-45c9-9251-1a538887b558"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_d30778b9-209d-4939-a2cd-64f03313fc15"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_38f73e57-9eef-4065-b564-07292912b163"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_b0f15ed7-64ee-4979-b353-8e8c7a36c673"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_d5d2be62-0d59-42f6-a64a-84924879b589"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_8ef2b2e5-3f88-4fb5-aee9-2741d7305301"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_f87f4c4f-5ff8-45b3-b9e5-2001ce450f37"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_1149c0ca-dbaf-49f3-b297-d4313ddefa67"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_babf4409-ca64-454b-8629-74ddfc39a247"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_a9970c7a-79d6-44ed-b6bf-5dbf42d1819a"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_6f2da464-0064-4c8f-8b74-1970b9476e33"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_dfa7c1d9-239c-44d0-9e82-827dc9b09876"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_b5a075b7-a142-418c-8d84-312f843cb25b"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_33e3de60-aecb-4593-aa85-bebac94e0bcb"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_220f7fba-87d5-4b75-bbfa-5bb53f6f4f92"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_27f09a05-2f77-4f71-8daa-237957f2d1ef"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_d433c17c-7f10-4790-8edf-99e8de7dd6d9"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_cb7de8de-5680-4187-9c99-4f195bfeb807"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_69a35e30-7c8a-4da1-867b-beacd1d91a98"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_5e016c20-61a9-45a7-af55-41b9815bf89c"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_e27efe51-548d-405a-b6f3-3d39fa616b34"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_c5cc02f9-a56f-430e-90a5-4e22c26b5593"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_a9ddcedf-e590-476d-bc67-720015f9a23e"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_85603634-4129-4520-9d5d-0f36ba4ec383"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_ee00dc13-7e0e-44a6-b4c6-990c40489501"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_c74c5ea3-a4cf-416f-a0d5-f6b39041a9c7"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_ce8ff976-ddbe-4b15-84d9-e7e0c3762308"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_9cb6a2f4-84b8-4323-94ec-9fcdc3bca5c4"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_dad5dbd9-c264-4272-a68c-86bcd2d75a10"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_7f19c2df-be1d-42b6-bae1-a6907a19a10b"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_7c2071c8-7621-4fb4-8943-57d771fa0d20"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_7cb373be-48bc-4644-87be-eaf3969051a0"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_bacae243-8786-4292-94fc-651f36ff6494"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_f6746f2f-5c38-4a9a-897b-bdf35dc2f0c5"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_7ae7199c-8628-485a-8a8d-bc43f81e4f66"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_30e32b73-dff9-483e-9e12-8bcc9200146b"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_3e94ac43-7796-4f08-a888-b77863b192dc"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_dcec9cc3-ede0-40aa-8327-1983aab6647a"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_1afbafa0-d4f6-4227-8797-4f2c7bfd93a2"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_81f9c336-b916-4cd2-81db-3bb9973748b9"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_a1ad661d-e2c5-48b3-b576-88ab29efcbaa"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_eab8154e-4634-4d14-adb1-7b51e589f122"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_e7af056f-472c-47be-848d-7b7c5f3c41a0"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_44ee80ca-f0f0-4aee-acde-ef863d6fedc6"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_c69485a9-5ff7-4557-9648-fd65bd7e140e"></xplan:praesentationsobjekt>
      <xplan:praesentationsobjekt xlink:href="#GML_b10a18ef-f593-4b72-a792-57081b41793a"></xplan:praesentationsobjekt>
      <xplan:versionBauNVOText>Version vom 11.06.2013</xplan:versionBauNVOText>
      <xplan:gehoertZuPlan xlink:href="#GML_0602cd45-6a1a-4b24-8786-30862f1fabca"></xplan:gehoertZuPlan>
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_Rasterdarstellung gml:id="GML_f40519da-3ea5-4713-944e-c530690d3a0c">
      <xplan:refScan></xplan:refScan>
    </xplan:XP_Rasterdarstellung>
  </gml:featureMember>
</xplan:XPlanAuszug>
