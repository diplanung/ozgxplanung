<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<xplan:XPlanAuszug xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:schemaLocation="http://www.xplanung.de/xplangml/5/0 ../../../../../../../../../../../xplan-schemas/src/main/resources/appschemas/XPlanGML_5_0/XPlanung-Operationen.xsd"
                   xmlns:xplan="http://www.xplanung.de/xplangml/5/0"
                   xmlns:xlink="http://www.w3.org/1999/xlink"
                   xmlns:gml="http://www.opengis.net/gml/3.2"
                   gml:id="GML_40adb0a5-8ba6-478e-8384-c1939d2711c7">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:31467">
      <gml:lowerCorner>3954633.369 5499860.173</gml:lowerCorner>
      <gml:upperCorner>3954791.49 5499972.787</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="GML_1265b858-4bfa-4694-94dd-f3b3cbd9ee03">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:31467">
          <gml:lowerCorner>3954683.088 5499947.56</gml:lowerCorner>
          <gml:upperCorner>3954716.126 5499966.522</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_18e6f5cd-9896-4e80-b4f3-ce0d8cc8a0c4"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="GML_e098a71d-b6d5-4adc-b96d-83a194dcacac">
          <gml:posList srsDimension="2" count="11">3954715.608 5499966.522 3954716.126 5499951.526
            3954710.115 5499951.17 3954701.227 5499950.272 3954692.491 5499948.856 3954686.404 5499947.56
            3954683.088 5499962.19 3954689.727 5499963.604 3954699.271 5499965.15 3954708.916 5499966.125
            3954715.608 5499966.522
          </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
</xplan:XPlanAuszug>
