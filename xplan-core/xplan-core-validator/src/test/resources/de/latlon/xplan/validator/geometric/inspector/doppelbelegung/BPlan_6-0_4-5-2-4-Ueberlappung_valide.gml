﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_1D2E333D-11AE-4292-94A9-BCE311CC4816" xsi:schemaLocation="http://www.xplanung.de/xplangml/6/0 http://www.xplanungwiki.de/upload/XPlanGML/6.0/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/6/0">
  <!-- Testplan 6.0 
  BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_93284040-8E0F-440C-A9CD-43B8CE37A8AE überlappt BP_BaugebietsTeilFlaeche gml:id="Gml_C0EE7EFD-A811-4E1D-98B4-782540EF8AB7, in der ebenfalls das Attribut Z belegt ist, um 0,03mm -> Kein Verstoß gegen 4.5.2.4
  -->
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>574369.114 5947113.1631</gml:lowerCorner>
      <gml:upperCorner>574973.8737 5947514.7064</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="Gml_D8287F23-3657-48D3-928D-91B547728D07">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574369.114 5947113.1631</gml:lowerCorner>
          <gml:upperCorner>574973.8737 5947514.7064</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan_6-0_4-5-2-4-Ueberlappung_valide</xplan:name>
      <xplan:technHerstellDatum>2016-02-26</xplan:technHerstellDatum>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_F4D592C2-125F-4702-B08F-E6CDFA4CAE9A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574948.6922 5947223.8427 574972.718 5947414.4767 574973.8737 5947423.6464 
574968.0287 5947423.9573 574611.3662 5947502.9334 574463.3403 5947514.7064 
574449.422 5947501.569 574447.491 5947494.794 574439.535 5947466.864 
574437.479 5947459.643 574436.408 5947455.882 574436.937 5947455.702 
574438.315 5947455.235 574436.025 5947445.66 574432.5307 5947431.0496 
574434.648 5947430.501 574436.211 5947430.096 574433.582 5947418.761 
574431.252 5947408.716 574445.288 5947406.526 574438.487 5947382.289 
574427.052 5947341.535 574420.762 5947319.113 574413.387 5947292.703 
574406.238 5947267.102 574392.093 5947216.471 574372.303 5947217.945 
574369.114 5947218.0566 574370.2892 5947113.1631 574375.1651 5947113.2373 
574375.008 5947122.642 574428.001 5947123.528 574479.523 5947127.996 
574493.66 5947128.054 574566.816 5947128.362 574660.776 5947128.758 
574702.71 5947128.931 574748.898 5947129.118 574748.851 5947140.899 
574748.634 5947195.807 574749.382 5947198.457 574749.678 5947198.453 
574767.01 5947198.217 574786.906 5947197.945 574823.227 5947205.023 
574854.016 5947212.936 574885.02 5947213.359 574947.4293 5947213.8217 
574948.6922 5947223.8427 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#Gml_9A307A2B-957A-42A4-8945-9A22C4A7FCFD" />
      <xplan:texte xlink:href="#Gml_EC77FB20-D20C-4B74-BE98-6DBB50D320B3" />
      <xplan:texte xlink:href="#Gml_CE3C7F3D-36A0-4C78-82A3-49678159524D" />
      <xplan:texte xlink:href="#Gml_4E00D5B2-45BD-460E-8798-B4A8F08F86F2" />
      <xplan:texte xlink:href="#Gml_C2904A32-7FF2-40AD-AD81-B2F971D2221A" />
      <xplan:texte xlink:href="#Gml_1359F5B4-2C2F-4F29-A5E5-1E584F05862A" />
      <xplan:texte xlink:href="#Gml_DEB17AE0-0B33-425F-896A-707883419923" />
      <xplan:texte xlink:href="#Gml_EC4313BD-7BBA-4061-A310-C59DC6519124" />
      <xplan:texte xlink:href="#Gml_73358AAF-3579-4A01-957C-846BBFAF3C2A" />
      <xplan:texte xlink:href="#Gml_C57F3AB5-03C5-4CCC-BA07-16AF977FC34E" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>524</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>      
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>1990-06-26</xplan:rechtsverordnungsDatum>    
      <xplan:versionBauNVO><xplan:XP_GesetzlicheGrundlage><xplan:datum>1977-01-01</xplan:datum></xplan:XP_GesetzlicheGrundlage></xplan:versionBauNVO>      
      <xplan:bereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_9A307A2B-957A-42A4-8945-9A22C4A7FCFD">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Entlang des Volksdorfer Damms sind durch geeignete Grundrißgestaltung die Wohn- und Schlafräume den lärmabgewandten Gebäudeseiten zuzuordnen. Soweit die Anordnung von Wohn- und Schlafräumen an den lärmabgewandten Gebäudeseiten nicht möglich ist, muß für diese Räume ein ausreichender Lärmschutz durch bauliche Maßnahmen an Türen, Fenstern, Außenwänden und Dächern der Gebäude geschaffen werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_EC77FB20-D20C-4B74-BE98-6DBB50D320B3">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>In den Wohngebieten darf die Dachneigung nicht weniger als 30 Grad betragen; Staffelgeschosse sind unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_CE3C7F3D-36A0-4C78-82A3-49678159524D">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>Bei giebelständigen Gebäuden sind unterschiedliche Drempelhöhen unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_4E00D5B2-45BD-460E-8798-B4A8F08F86F2">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>In den Wohngebieten darf die Oberkante des Erdgeschoßfußbodens nicht höher als 80 cm über Gehweg liegen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_C2904A32-7FF2-40AD-AD81-B2F971D2221A">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Am Osterkampstieg sind die von außen sichtbaren Teile der Außenwände von Wohngebäuden in rotem Ziegelmauerwerk auszufuhren.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_1359F5B4-2C2F-4F29-A5E5-1E584F05862A">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Auf den mit „(A)", „(B)" und „(C)" bezeichneten Friedhofsflächen sind nur Stellplätze sowie Gebäude für friedhofsbezogene Nutzungen (z.B. Kapelle, Friefhofsverwaltung, Betriebs- und Sozialräume) zulässig, und zwar auf der mit „(A)" bezeichneten Fläche bis zu einer Gebäudehöhe von 37,1 m über Normalnull, auf der mit „(B)" bezeichneten Fläche bis zu einer Gebäudehöhe von 42,4 m über Normalnull und auf der mit „(C)" bezeichneten Fläche bis zu einer Gebäudehöhe von 38,3 m über Normalnull.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_DEB17AE0-0B33-425F-896A-707883419923">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>Für die nach der Planzeichnung zu erhaltenden Bäume und Sträucher sind bei Abgang Ersatzpflanzungen mit standortgerechten einheimischen Arten vorzunehmen. Die Bäume müssen einen Stammumfang von mindestens 16 cm in 1 m Höhe über dem Erdboden aufweisen. Im Kronenbereich jedes Baumes ist eine offene Vegetationsfläche von mindestens 12 m² anzulegen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_EC4313BD-7BBA-4061-A310-C59DC6519124">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Auf den privaten Grundstücksflächen sind Fahr- und Gehwege sowie Stellplätze in wasser- und luftdurchlässigem Aufbau herzustellen. Die Wasser- und Luftdurchlässigkeit des Bodens wesentlich mindernde Befestigungen wie Betonunterbau, Fugenverguß, Asphaltierung oder Betonierung sind unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_73358AAF-3579-4A01-957C-846BBFAF3C2A">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Die Durchlässigkeit gewachsenen Bodens ist nach baubedingter Verdichtung wieder herzustellen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_C57F3AB5-03C5-4CCC-BA07-16AF977FC34E">
      <xplan:schluessel>§2 Nr.10</xplan:schluessel>
      <xplan:text>Außerhalb von öffentlichen Verkehrsflächen sind Geländeaufhöhungen und Abgrabungen im Kronenbereich der nach der Planzeichnung zu erhaltenden Bäume unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574369.114 5947113.1631</gml:lowerCorner>
          <gml:upperCorner>574973.8737 5947514.7064</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_FD35D26A-A3DA-4061-ABB0-8496A3495BC9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574948.6922 5947223.8427 574972.718 5947414.4767 574973.8737 5947423.6464 
574968.0287 5947423.9573 574611.3662 5947502.9334 574463.3403 5947514.7064 
574449.422 5947501.569 574447.491 5947494.794 574439.535 5947466.864 
574437.479 5947459.643 574436.408 5947455.882 574436.937 5947455.702 
574438.315 5947455.235 574436.025 5947445.66 574432.5307 5947431.0496 
574434.648 5947430.501 574436.211 5947430.096 574433.582 5947418.761 
574431.252 5947408.716 574445.288 5947406.526 574438.487 5947382.289 
574427.052 5947341.535 574420.762 5947319.113 574413.387 5947292.703 
574406.238 5947267.102 574392.093 5947216.471 574372.303 5947217.945 
574369.114 5947218.0566 574370.2892 5947113.1631 574375.1651 5947113.2373 
574375.008 5947122.642 574428.001 5947123.528 574479.523 5947127.996 
574493.66 5947128.054 574566.816 5947128.362 574660.776 5947128.758 
574702.71 5947128.931 574748.898 5947129.118 574748.851 5947140.899 
574748.634 5947195.807 574749.382 5947198.457 574749.678 5947198.453 
574767.01 5947198.217 574786.906 5947197.945 574823.227 5947205.023 
574854.016 5947212.936 574885.02 5947213.359 574947.4293 5947213.8217 
574948.6922 5947223.8427 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#Gml_AB37AF69-516B-46FF-A302-8C4664B6F3D2" />
      <xplan:planinhalt xlink:href="#Gml_E2FA9054-EDEE-40B2-B3EC-78C40BA82D74" />
      <xplan:planinhalt xlink:href="#Gml_EF08378B-4CE8-43AB-BF16-9B39EAF78DA4" />
      <xplan:planinhalt xlink:href="#Gml_605E50D6-E6C2-45A9-8F04-70E028E8A848" />
      <xplan:planinhalt xlink:href="#Gml_40C8A259-6527-4E93-AAB8-5AC7B6B672EF" />
      <xplan:planinhalt xlink:href="#Gml_0B9950C3-412C-49AE-9BF6-5244273F71DE" />
      <xplan:planinhalt xlink:href="#Gml_C0EE7EFD-A811-4E1D-98B4-782540EF8AB7" />
      <xplan:planinhalt xlink:href="#Gml_17F3F0AB-25D7-4AA0-9B01-B92F977EFAA8" />
      <xplan:planinhalt xlink:href="#Gml_4FA16482-785A-4A2D-B458-EB4872D43E4A" />
      <xplan:planinhalt xlink:href="#Gml_3B41665D-65F5-487D-87C8-9D10DB7648A3" />
      <xplan:planinhalt xlink:href="#Gml_F0BACAB3-1838-495A-A56D-3D5D2A35941C" />
      <xplan:planinhalt xlink:href="#Gml_70762126-27DB-4AAA-B92E-DD4959752EC1" />
      <xplan:planinhalt xlink:href="#Gml_9D7E3E5B-E5C0-4407-8027-C4FF180DFC83" />
      <xplan:planinhalt xlink:href="#Gml_68A7EF47-F1BD-446B-AAD5-1FAE523E8639" />
      <xplan:planinhalt xlink:href="#Gml_A8A6036E-CDF9-45EE-8B03-A824FB0D4A88" />
      <xplan:planinhalt xlink:href="#Gml_AD910008-03F7-44D7-A5BD-5862391C1DC1" />
      <xplan:planinhalt xlink:href="#Gml_8E2A2C79-C33D-4B43-BB56-C4542B85C54D" />
      <xplan:planinhalt xlink:href="#Gml_4E63A33A-A768-4C86-AE79-C4B16975CA3B" />
      <xplan:planinhalt xlink:href="#Gml_9E5811D1-ECAE-4E40-9E7E-8E93C40E51E1" />
      <xplan:planinhalt xlink:href="#Gml_32E783AD-7718-4E26-8DA7-648E07E2AB65" />
      <xplan:planinhalt xlink:href="#Gml_E4B4E687-C981-4A85-9CCD-FA56EE3F1E96" />
      <xplan:planinhalt xlink:href="#Gml_E0E037A7-5BEC-4AB6-A4C6-4385E8689852" />
      <xplan:planinhalt xlink:href="#Gml_01F29C20-B21E-4CA2-AB1E-D60CAA2F6435" />
      <xplan:planinhalt xlink:href="#Gml_F3AE7979-CAD1-414A-9171-57CCD19315A0" />
      <xplan:planinhalt xlink:href="#Gml_2CD06187-C80F-4A4B-B234-C34933F6BAF0" />
      <xplan:planinhalt xlink:href="#Gml_5233035D-BFC3-47E5-B67D-DD794F9CBF70" />
      <xplan:planinhalt xlink:href="#Gml_12C4F03D-657D-457A-9B2D-189399A2074C" />
      <xplan:planinhalt xlink:href="#Gml_8B3465E4-E6DC-43C3-BBE4-55076DF50741" />
      <xplan:planinhalt xlink:href="#Gml_6F9F150D-23F1-4306-AF2B-1A8C0F2E7AFA" />
      <xplan:planinhalt xlink:href="#Gml_25AF375B-7C1C-40ED-8F6B-8CD437F3142D" />
      <xplan:planinhalt xlink:href="#Gml_2C217D2B-49F9-440B-B4AD-8266D4C181BB" />
      <xplan:planinhalt xlink:href="#Gml_2F127535-0788-4130-A6D6-BA8999337F58" />
      <xplan:planinhalt xlink:href="#Gml_BA1A00F0-BA67-46C2-B2B4-7538A3F69CFC" />
      <xplan:planinhalt xlink:href="#Gml_20353AA5-6F87-4EA8-AC5D-22CF63EF2FB8" />
      <xplan:planinhalt xlink:href="#Gml_CADA9092-A424-4C68-A12A-1AD40D4D10EF" />
      <xplan:planinhalt xlink:href="#Gml_D111EF9A-A62E-41DC-B971-565A25E4BB34" />
      <xplan:planinhalt xlink:href="#Gml_C875030A-9332-4069-A898-E43C98152788" />
      <xplan:planinhalt xlink:href="#Gml_1DC005C3-8403-47B3-A3EE-89F328C335D2" />
      <xplan:planinhalt xlink:href="#Gml_4761B301-F292-47FE-8951-D449B7AEF856" />
      <xplan:planinhalt xlink:href="#Gml_45B67BFD-E736-4909-BB39-D071E904E9C8" />
      <xplan:planinhalt xlink:href="#Gml_09994523-31CD-4C35-9050-D3D8BC1E22C5" />
      <xplan:planinhalt xlink:href="#Gml_5851D686-0618-4B91-9572-2CEAA65F902A" />
      <xplan:planinhalt xlink:href="#Gml_52134E9F-8BE3-490B-A2D7-C4A733939A72" />
      <xplan:planinhalt xlink:href="#Gml_DBD55B85-95D8-4EB0-9B9C-26305CB1AB5C" />
      <xplan:planinhalt xlink:href="#Gml_131B39A6-861D-48E2-90A2-AACFBB9862E7" />
      <xplan:planinhalt xlink:href="#Gml_133D6B99-06B2-4D7B-90C5-02FBB70E52AD" />
      <xplan:planinhalt xlink:href="#Gml_AEFEA73C-B50D-400C-8AB3-41D512C0A3F2" />
      <xplan:planinhalt xlink:href="#Gml_A98C305C-D608-4BC7-9DCF-B6B369993841" />
      <xplan:planinhalt xlink:href="#Gml_4F55C9BF-70A6-4C26-8D7E-55E6B5F0D18B" />
      <xplan:planinhalt xlink:href="#Gml_066A9E89-B2ED-415A-8434-BEDF7E89F2F1" />
      <xplan:planinhalt xlink:href="#Gml_FE245DC4-84F0-4C8A-9F8E-0DBDBE1C6868" />
      <xplan:planinhalt xlink:href="#Gml_61B3046A-A685-43BB-B726-0BC6CF9C5887" />
      <xplan:planinhalt xlink:href="#Gml_25288D32-8780-4AC5-887B-E357442D50A4" />
      <xplan:planinhalt xlink:href="#Gml_375987FB-FCD2-4D3B-BB2A-4F1C53C0D9FC" />
      <xplan:planinhalt xlink:href="#Gml_4898DA1D-2D52-4633-BD02-362581AB1CDD" />
      <xplan:planinhalt xlink:href="#Gml_1610E07B-396B-4E84-93A2-5FF6B90278C8" />
      <xplan:planinhalt xlink:href="#Gml_495B1C2E-B3D2-4206-8E90-191959203566" />
      <xplan:planinhalt xlink:href="#Gml_3BD4A5D4-2830-41F7-A922-4D3C21933C6B" />
      <xplan:planinhalt xlink:href="#Gml_C281F72B-3423-45B1-B52F-599C4153C5AA" />
      <xplan:planinhalt xlink:href="#Gml_5C383746-97D5-477D-82D6-C4B868C4CFF3" />
      <xplan:planinhalt xlink:href="#Gml_FB55ABCF-E8A6-4BC8-B516-6C19DAB48F7A" />
      <xplan:planinhalt xlink:href="#Gml_93284040-8E0F-440C-A9CD-43B8CE37A8AE" />
      <xplan:planinhalt xlink:href="#Gml_8B473ED1-518C-4432-8C7E-692561FC97FD" />
      <xplan:planinhalt xlink:href="#Gml_26D4A77A-D4F3-416C-A4D9-145ED2997421" />
      <xplan:planinhalt xlink:href="#Gml_CB1C4B5E-C403-429F-A0D6-EC292A422BC8" />
      <xplan:planinhalt xlink:href="#Gml_94ECE09F-43F8-4E3A-89B4-641807379381" />
      <xplan:planinhalt xlink:href="#Gml_CE34F6AB-75D9-486D-AAE9-21664D030DB2" />
      <xplan:planinhalt xlink:href="#Gml_686A1A57-9994-4D66-916B-717F3B249231" />
      <xplan:planinhalt xlink:href="#Gml_8E5A7CA2-57E0-4B32-9103-83E85366B48E" />
      <xplan:planinhalt xlink:href="#Gml_95467D92-7AA9-4DC8-AE26-48A4BA76EEB6" />
      <xplan:planinhalt xlink:href="#Gml_7C7846E7-C451-4C24-B71D-CEB2958CD92D" />
      <xplan:planinhalt xlink:href="#Gml_13AA2C3E-8FBA-4BC4-A5DD-114276CAFE95" />
      <xplan:planinhalt xlink:href="#Gml_75083B3A-089E-4C86-8C14-1C8868716D6E" />
      <xplan:planinhalt xlink:href="#Gml_F5DC5D57-D392-4C3B-A36D-B48B25F191B9" />
      <xplan:planinhalt xlink:href="#Gml_ABE7288D-A15C-4952-AA73-0EB911527A6A" />
      <xplan:planinhalt xlink:href="#Gml_E6801B0D-3414-40EF-A671-EB57EC022C89" />
      <xplan:planinhalt xlink:href="#Gml_9DC9424D-F417-475B-A3BB-5A25A0A4C382" />
      <xplan:planinhalt xlink:href="#Gml_F755745E-44C1-4684-93C1-8552735936E8" />
      <xplan:planinhalt xlink:href="#Gml_11C06D8F-853C-4676-BA70-DD0A5352D463" />
      <xplan:planinhalt xlink:href="#Gml_66B3A988-076F-4BAF-A66C-13FD30587278" />
      <xplan:planinhalt xlink:href="#Gml_E33DC17A-118D-4CC2-BDFB-C30F5644CDEE" />
      <xplan:planinhalt xlink:href="#Gml_39F9F27A-A940-4D7A-981B-948CF93BA741" />
      <xplan:planinhalt xlink:href="#Gml_276F51A9-7BDF-4ACB-8469-615E8B2A5916" />
      <xplan:planinhalt xlink:href="#Gml_741E4DC3-1E5E-4631-B298-7A0030926073" />
      <xplan:planinhalt xlink:href="#Gml_3484B9F2-2FE0-428B-82D2-FDB6261042C3" />
      <xplan:planinhalt xlink:href="#Gml_C15C5CF9-6D2E-4823-8036-87B3C2E94BA8" />
      <xplan:planinhalt xlink:href="#Gml_CDB5BC28-C1D6-4175-8DE9-A26348C3F7DF" />
      <xplan:planinhalt xlink:href="#Gml_1C732454-A955-4E78-813E-2DCDDC83E66D" />
      <xplan:planinhalt xlink:href="#Gml_D9C0CCF2-B7C2-46FA-AD45-060113C347D6" />
      <xplan:planinhalt xlink:href="#Gml_16AF1467-6CA7-4E8A-A21B-F95A3083A2CA" />
      <xplan:planinhalt xlink:href="#Gml_8247CD61-BFA8-47DF-BA23-2F899276F2CC" />
      <xplan:planinhalt xlink:href="#Gml_076896AF-FC83-4E2C-A08A-31BDF824E6CD" />
      <xplan:planinhalt xlink:href="#Gml_20D16DE3-2C57-475A-86ED-E594E8838EFD" />
      <xplan:planinhalt xlink:href="#Gml_5F06A281-791F-4B8E-9CC6-101E5690A093" />
      <xplan:verfahren>1000</xplan:verfahren>             
      <xplan:gehoertZuPlan xlink:href="#Gml_D8287F23-3657-48D3-928D-91B547728D07" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_AB37AF69-516B-46FF-A302-8C4664B6F3D2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574374.031 5947122.642</gml:lowerCorner>
          <gml:upperCorner>574428.001 5947186.997</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#Gml_EC77FB20-D20C-4B74-BE98-6DBB50D320B3" />
      <xplan:refTextInhalt xlink:href="#Gml_4E00D5B2-45BD-460E-8798-B4A8F08F86F2" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_07B0C348-B311-40C8-92FD-8279FCBA07F9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574428.001 5947123.528 574427.68 5947142.413 574427.4063 5947158.4949 
574427.377 5947160.216 574427.029 5947183.999 574426.985 5947186.997 
574380.149 5947186.502 574374.156 5947186.439 574374.031 5947180.518 
574374.351 5947161.562 574374.689 5947141.507 574375.008 5947122.642 
574428.001 5947123.528 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung>
        <xplan:BP_Dachgestaltung>
          <xplan:dachform>3100</xplan:dachform>
        </xplan:BP_Dachgestaltung>
      </xplan:dachgestaltung>      
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GFZ>0.6</xplan:GFZ>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>2</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_E2FA9054-EDEE-40B2-B3EC-78C40BA82D74">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574770.537 5947207.285</gml:lowerCorner>
          <gml:upperCorner>574949.462 5947318.93</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#Gml_EC77FB20-D20C-4B74-BE98-6DBB50D320B3" />
      <xplan:refTextInhalt xlink:href="#Gml_4E00D5B2-45BD-460E-8798-B4A8F08F86F2" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A3234ED7-C6B5-4D32-A310-D8A8CC1130A3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574918.411 5947227.511 574937.083 5947227.669 574942.78 5947232.631 
574949.462 5947283.588 574933.321 5947287.573 574913.91 5947292.366 
574892.936 5947297.544 574878.957 5947300.996 574860.634 5947305.52 
574831.232 5947312.779 574806.32 5947318.93 574800.125 5947315.301 
574795.773 5947299.911 574790.327 5947280.658 574785.705 5947264.316 
574781.107 5947248.063 574776.509 5947231.806 574770.537 5947210.687 
574782.569 5947207.285 574807.845 5947212.209 574834.987 5947217.499 
574839.271 5947218.335 574841.308 5947227.422 574849.144 5947227.409 
574860.706 5947227.39 574880.682 5947227.361 574897.789 5947227.336 
574918.411 5947227.511 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung>
        <xplan:BP_Dachgestaltung>
          <xplan:dachform>3100</xplan:dachform>
        </xplan:BP_Dachgestaltung>
      </xplan:dachgestaltung>      
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GFZ>0.3</xplan:GFZ>
      <xplan:GRZ>0.2</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_EF08378B-4CE8-43AB-BF16-9B39EAF78DA4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574805.559 5947293.571</gml:lowerCorner>
          <gml:upperCorner>574965.284 5947444.122</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#Gml_9A307A2B-957A-42A4-8945-9A22C4A7FCFD" />
      <xplan:refTextInhalt xlink:href="#Gml_EC77FB20-D20C-4B74-BE98-6DBB50D320B3" />
      <xplan:refTextInhalt xlink:href="#Gml_4E00D5B2-45BD-460E-8798-B4A8F08F86F2" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6587AEDD-8FD3-4427-AA09-2C89F6E56B00" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574958.801 5947354.726 574961.762 5947380.415 574965.284 5947410.966 
574961.496 5947416.932 574946.863 5947420.2 574926.876 5947424.662 
574906.44 5947429.225 574886.014 5947433.786 574861.044 5947439.363 
574840.8896 5947443.8635 574839.732 5947444.122 574835.983 5947441.892 
574825.792 5947406.123 574820.45 5947387.368 574815.604 5947370.181 
574810.219 5947351.08 574805.559 5947334.548 574809.054 5947328.543 
574836.712 5947321.717 574864.987 5947314.741 574882.935 5947310.312 
574899.263 5947306.283 574915.554 5947302.263 574931.882 5947298.235 
574950.782 5947293.571 574958.801 5947354.726 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung>
        <xplan:BP_Dachgestaltung>
          <xplan:dachform>3100</xplan:dachform>
        </xplan:BP_Dachgestaltung>
      </xplan:dachgestaltung>      
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GFZ>0.3</xplan:GFZ>
      <xplan:GRZ>0.2</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_605E50D6-E6C2-45A9-8F04-70E028E8A848">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574670.413 5947412.488</gml:lowerCorner>
          <gml:upperCorner>574722.787 5947477.539</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#Gml_9A307A2B-957A-42A4-8945-9A22C4A7FCFD" />
      <xplan:refTextInhalt xlink:href="#Gml_EC77FB20-D20C-4B74-BE98-6DBB50D320B3" />
      <xplan:refTextInhalt xlink:href="#Gml_4E00D5B2-45BD-460E-8798-B4A8F08F86F2" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7E522CCA-C2C3-4446-90BC-58CA7819403C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574722.787 5947470.176 574702.905 5947474.586 574689.938 5947477.539 
574684.922 5947456.195 574681.03 5947457.109 574670.413 5947420.656 
574686.167 5947417.088 574706.472 5947412.488 574722.787 5947470.176 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung>
        <xplan:BP_Dachgestaltung>
          <xplan:dachform>3100</xplan:dachform>
        </xplan:BP_Dachgestaltung>
      </xplan:dachgestaltung>      
      <xplan:GFZ>0.5</xplan:GFZ>
      <xplan:GRZ>0.4</xplan:GRZ>      
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_40C8A259-6527-4E93-AAB8-5AC7B6B672EF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574581.199 5947420.656</gml:lowerCorner>
          <gml:upperCorner>574689.938 5947498.947</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#Gml_9A307A2B-957A-42A4-8945-9A22C4A7FCFD" />
      <xplan:refTextInhalt xlink:href="#Gml_EC77FB20-D20C-4B74-BE98-6DBB50D320B3" />
      <xplan:refTextInhalt xlink:href="#Gml_4E00D5B2-45BD-460E-8798-B4A8F08F86F2" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_EDCDC42B-455F-453B-925D-7010A2DF4A83" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574689.938 5947477.539 574658.449 5947484.709 574627.329 5947491.421 
574628.44 5947495.406 574592.838 5947498.947 574589.642 5947493.098 
574581.199 5947461.891 574612.83 5947454.316 574616.732 5947453.401 
574647.354 5947446.221 574643.2953 5947432.1408 574641.85 5947427.127 
574644.2378 5947426.586 574644.456 5947426.5366 574670.413 5947420.656 
574681.03 5947457.109 574684.922 5947456.195 574689.938 5947477.539 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung>
        <xplan:BP_Dachgestaltung>
          <xplan:dachform>3100</xplan:dachform>
        </xplan:BP_Dachgestaltung>
      </xplan:dachgestaltung>      
      <xplan:GFZ>0.8</xplan:GFZ>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>2</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_0B9950C3-412C-49AE-9BF6-5244273F71DE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574557.613 5947461.891</gml:lowerCorner>
          <gml:upperCorner>574592.838 5947499.961</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#Gml_9A307A2B-957A-42A4-8945-9A22C4A7FCFD" />
      <xplan:refTextInhalt xlink:href="#Gml_EC77FB20-D20C-4B74-BE98-6DBB50D320B3" />
      <xplan:refTextInhalt xlink:href="#Gml_4E00D5B2-45BD-460E-8798-B4A8F08F86F2" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_FDA4F095-B013-43CD-9F0A-CABCD469B810" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574589.642 5947493.098 574592.838 5947498.947 574564.018 5947499.961 
574557.613 5947467.608 574581.199 5947461.891 574589.642 5947493.098 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung>
        <xplan:BP_Dachgestaltung>
          <xplan:dachform>3100</xplan:dachform>
        </xplan:BP_Dachgestaltung>
      </xplan:dachgestaltung>      
      <xplan:GFZ>0.5</xplan:GFZ>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_C0EE7EFD-A811-4E1D-98B4-782540EF8AB7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574694.872 5947335.029</gml:lowerCorner>
          <gml:upperCorner>574819.338 5947470.176</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#Gml_9A307A2B-957A-42A4-8945-9A22C4A7FCFD" />
      <xplan:refTextInhalt xlink:href="#Gml_EC77FB20-D20C-4B74-BE98-6DBB50D320B3" />
      <xplan:refTextInhalt xlink:href="#Gml_4E00D5B2-45BD-460E-8798-B4A8F08F86F2" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BB553632-51E9-4358-A77C-36CA53600853" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574789.035 5947338.643 574793.088 5947352.946 574795.5906 5947361.7813 
574798.527 5947372.148 574803.947 5947391.286 574809.532 5947411.004 
574814.1192 5947427.1971 574819.338 5947445.62 574817.23 5947449.146 
574795.255 5947454.062 574770.393 5947459.622 574747.948 5947464.597 
574743.065 5947465.68 574727.674 5947469.092 574726.696 5947469.308 
574722.787 5947470.176 574706.472 5947412.488 574710.377 5947411.603 
574696.2865 5947361.7813 574694.872 5947356.78 574705.8342 5947354.068 
574738.167 5947346.069 574745.6403 5947344.2199 574752.0167 5947342.6421 
574782.785 5947335.029 574789.035 5947338.643 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung>
        <xplan:BP_Dachgestaltung>
          <xplan:dachform>3100</xplan:dachform>
        </xplan:BP_Dachgestaltung>
      </xplan:dachgestaltung>      
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GFZ>0.3</xplan:GFZ>
      <xplan:GRZ>0.2</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_17F3F0AB-25D7-4AA0-9B01-B92F977EFAA8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574706.472 5947412.488</gml:lowerCorner>
          <gml:upperCorner>574722.787 5947470.176</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_D2F3D77D-2ACC-4CF3-A9D5-DE0C78DCCC94" srsName="EPSG:25832">
          <gml:posList>574706.472 5947412.488 574722.787 5947470.176 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_4FA16482-785A-4A2D-B458-EB4872D43E4A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574670.413 5947420.656</gml:lowerCorner>
          <gml:upperCorner>574689.938 5947477.539</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4C2CF6A4-9DC4-4AB3-B704-28F983E66F3C" srsName="EPSG:25832">
          <gml:posList>574670.413 5947420.656 574681.03 5947457.109 574684.922 5947456.195 
574689.938 5947477.539 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_3B41665D-65F5-487D-87C8-9D10DB7648A3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574581.199 5947461.891</gml:lowerCorner>
          <gml:upperCorner>574592.838 5947498.947</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_0DFD8905-898D-42D0-8E97-5BE445A17FAE" srsName="EPSG:25832">
          <gml:posList>574581.199 5947461.891 574589.642 5947493.098 574592.838 5947498.947 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Strassenverkehr gml:id="Gml_F0BACAB3-1838-495A-A56D-3D5D2A35941C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574447.491 5947197.945</gml:lowerCorner>
          <gml:upperCorner>574973.8737 5947514.7064</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_9281DFD6-1D51-4B7B-8884-5191DBD4B0D0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574463.3403 5947514.7064 574449.422 5947501.569 574447.491 5947494.794 
574450.07 5947498.453 574454.263 5947501.828 574461.909 5947504.544 
574467.1402 5947505.5702 574470.05 5947506.141 574477.941 5947506.688 
574478.4712 5947506.6465 574561.437 5947501.2501 574561.465 5947500.291 
574561.9927 5947500.253 574564.046 5947500.105 574564.018 5947499.961 
574592.838 5947498.947 574628.44 5947495.406 574627.329 5947491.421 
574658.449 5947484.709 574689.938 5947477.539 574702.905 5947474.586 
574722.787 5947470.176 574726.696 5947469.308 574727.674 5947469.092 
574743.065 5947465.68 574747.948 5947464.597 574770.393 5947459.622 
574795.255 5947454.062 574817.23 5947449.146 574819.338 5947445.62 
574814.1192 5947427.1971 574809.532 5947411.004 574803.947 5947391.286 
574798.527 5947372.148 574795.5906 5947361.7813 574793.088 5947352.946 
574789.035 5947338.643 574765.4627 5947255.3274 574757.1362 5947225.8977 
574753.332 5947212.452 574750.2133 5947201.4025 574749.603 5947199.483 
574749.382 5947198.457 574749.678 5947198.453 574767.01 5947198.217 
574786.906 5947197.945 574823.227 5947205.023 574854.016 5947212.936 
574885.02 5947213.359 574947.4293 5947213.8217 574948.6922 5947223.8427 
574972.718 5947414.4767 574973.8737 5947423.6464 574968.0287 5947423.9573 
574611.3662 5947502.9334 574463.3403 5947514.7064 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574836.712 5947321.717 574809.054 5947328.543 574805.559 5947334.548 
574810.219 5947351.08 574815.604 5947370.181 574820.45 5947387.368 
574825.792 5947406.123 574835.983 5947441.892 574839.732 5947444.122 
574840.8896 5947443.8635 574861.044 5947439.363 574886.014 5947433.786 
574906.44 5947429.225 574926.876 5947424.662 574946.863 5947420.2 
574961.496 5947416.932 574965.284 5947410.966 574961.762 5947380.415 
574958.801 5947354.726 574950.782 5947293.571 574931.882 5947298.235 
574915.554 5947302.263 574899.263 5947306.283 574882.935 5947310.312 
574864.987 5947314.741 574836.712 5947321.717 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574831.232 5947312.779 574860.634 5947305.52 574878.957 5947300.996 
574892.936 5947297.544 574913.91 5947292.366 574933.321 5947287.573 
574949.462 5947283.588 574942.78 5947232.631 574937.083 5947227.669 
574918.411 5947227.511 574897.789 5947227.336 574880.682 5947227.361 
574860.706 5947227.39 574849.144 5947227.409 574841.308 5947227.422 
574839.271 5947218.335 574834.987 5947217.499 574807.845 5947212.209 
574782.569 5947207.285 574770.537 5947210.687 574776.509 5947231.806 
574781.107 5947248.063 574785.705 5947264.316 574790.327 5947280.658 
574795.773 5947299.911 574800.125 5947315.301 574806.32 5947318.93 
574831.232 5947312.779 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:hatDarstellungMitBesondZweckbest>false</xplan:hatDarstellungMitBesondZweckbest>
    </xplan:SO_Strassenverkehr>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Strassenverkehr gml:id="Gml_70762126-27DB-4AAA-B92E-DD4959752EC1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574369.114 5947113.1631</gml:lowerCorner>
          <gml:upperCorner>574380.149 5947218.0566</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_D6E0E702-B357-439D-92EC-D0508A790935" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574375.1651 5947113.2373 574375.008 5947122.642 574374.689 5947141.507 
574374.351 5947161.562 574374.031 5947180.518 574374.156 5947186.439 
574380.149 5947186.502 574379.4059 5947189.3114 574378.5506 5947192.5449 
574378.1399 5947194.0974 574378.103 5947194.237 574376.285 5947201.116 
574372.303 5947217.945 574369.114 5947218.0566 574370.2892 5947113.1631 
574375.1651 5947113.2373 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:hatDarstellungMitBesondZweckbest>false</xplan:hatDarstellungMitBesondZweckbest>
    </xplan:SO_Strassenverkehr>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="Gml_9D7E3E5B-E5C0-4407-8027-C4FF180DFC83">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574372.303 5947113.2373</gml:lowerCorner>
          <gml:upperCorner>574380.149 5947217.945</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_00BE3B24-0FA1-43D6-A753-F10B8F157324" srsName="EPSG:25832">
          <gml:posList>574372.303 5947217.945 574376.285 5947201.116 574378.103 5947194.237 
574378.1399 5947194.0974 574378.5506 5947192.5449 574379.4059 5947189.3114 
574380.149 5947186.502 574374.156 5947186.439 574374.031 5947180.518 
574374.351 5947161.562 574374.689 5947141.507 574375.008 5947122.642 
574375.1651 5947113.2373 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="Gml_68A7EF47-F1BD-446B-AAD5-1FAE523E8639">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574770.537 5947207.285</gml:lowerCorner>
          <gml:upperCorner>574949.462 5947318.93</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_F84F64BF-0198-4F6C-89DB-028CDC729ED9" srsName="EPSG:25832">
          <gml:posList>574918.411 5947227.511 574937.083 5947227.669 574942.78 5947232.631 
574949.462 5947283.588 574933.321 5947287.573 574913.91 5947292.366 
574892.936 5947297.544 574878.957 5947300.996 574860.634 5947305.52 
574831.232 5947312.779 574806.32 5947318.93 574800.125 5947315.301 
574795.773 5947299.911 574790.327 5947280.658 574785.705 5947264.316 
574781.107 5947248.063 574776.509 5947231.806 574770.537 5947210.687 
574782.569 5947207.285 574807.845 5947212.209 574834.987 5947217.499 
574839.271 5947218.335 574841.308 5947227.422 574849.144 5947227.409 
574860.706 5947227.39 574880.682 5947227.361 574897.789 5947227.336 
574918.411 5947227.511 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="Gml_A8A6036E-CDF9-45EE-8B03-A824FB0D4A88">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574805.559 5947293.571</gml:lowerCorner>
          <gml:upperCorner>574965.284 5947444.122</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_D719CAF4-4189-4C7C-A81C-813978DA6C33" srsName="EPSG:25832">
          <gml:posList>574958.801 5947354.726 574961.762 5947380.415 574965.284 5947410.966 
574961.496 5947416.932 574946.863 5947420.2 574926.876 5947424.662 
574906.44 5947429.225 574886.014 5947433.786 574861.044 5947439.363 
574840.8896 5947443.8635 574839.732 5947444.122 574835.983 5947441.892 
574825.792 5947406.123 574820.45 5947387.368 574815.604 5947370.181 
574810.219 5947351.08 574805.559 5947334.548 574809.054 5947328.543 
574836.712 5947321.717 574864.987 5947314.741 574882.935 5947310.312 
574899.263 5947306.283 574915.554 5947302.263 574931.882 5947298.235 
574950.782 5947293.571 574958.801 5947354.726 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="Gml_AD910008-03F7-44D7-A5BD-5862391C1DC1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574447.491 5947198.457</gml:lowerCorner>
          <gml:upperCorner>574819.338 5947506.688</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_255660DE-3FEC-410E-BC7B-8ADBF9FFB660" srsName="EPSG:25832">
          <gml:posList>574749.382 5947198.457 574749.603 5947199.483 574750.2133 5947201.4025 
574753.332 5947212.452 574757.1362 5947225.8977 574765.4627 5947255.3274 
574789.035 5947338.643 574793.088 5947352.946 574795.5906 5947361.7813 
574798.527 5947372.148 574803.947 5947391.286 574809.532 5947411.004 
574814.1192 5947427.1971 574819.338 5947445.62 574817.23 5947449.146 
574795.255 5947454.062 574770.393 5947459.622 574747.948 5947464.597 
574743.065 5947465.68 574727.674 5947469.092 574726.696 5947469.308 
574722.787 5947470.176 574702.905 5947474.586 574689.938 5947477.539 
574658.449 5947484.709 574627.329 5947491.421 574628.44 5947495.406 
574592.838 5947498.947 574564.018 5947499.961 574564.046 5947500.105 
574561.9927 5947500.253 574561.465 5947500.291 574561.437 5947501.2501 
574478.4712 5947506.6465 574477.941 5947506.688 574470.05 5947506.141 
574467.1402 5947505.5702 574461.909 5947504.544 574454.263 5947501.828 
574450.07 5947498.453 574447.491 5947494.794 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_8E2A2C79-C33D-4B43-BB56-C4542B85C54D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574372.303 5947123.528</gml:lowerCorner>
          <gml:upperCorner>574789.035 5947506.688</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_A5ADB983-63B6-410A-8581-3E681B74C470" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574752.0167 5947342.6421 574745.6403 5947344.2199 574738.167 5947346.069 
574705.8342 5947354.068 574694.872 5947356.78 574696.2865 5947361.7813 
574710.377 5947411.603 574706.472 5947412.488 574686.167 5947417.088 
574670.413 5947420.656 574644.456 5947426.5366 574644.2378 5947426.586 
574641.85 5947427.127 574643.2953 5947432.1408 574647.354 5947446.221 
574616.732 5947453.401 574612.83 5947454.316 574581.199 5947461.891 
574557.613 5947467.608 574564.018 5947499.961 574564.046 5947500.105 
574561.9927 5947500.253 574561.465 5947500.291 574561.437 5947501.2501 
574478.4712 5947506.6465 574477.941 5947506.688 574470.05 5947506.141 
574467.1402 5947505.5702 574461.909 5947504.544 574454.263 5947501.828 
574450.07 5947498.453 574447.491 5947494.794 574439.535 5947466.864 
574437.479 5947459.643 574436.408 5947455.882 574436.937 5947455.702 
574438.315 5947455.235 574436.025 5947445.66 574432.5307 5947431.0496 
574434.648 5947430.501 574436.211 5947430.096 574433.582 5947418.761 
574431.252 5947408.716 574445.288 5947406.526 574438.487 5947382.289 
574427.052 5947341.535 574420.762 5947319.113 574413.387 5947292.703 
574406.238 5947267.102 574392.093 5947216.471 574372.303 5947217.945 
574376.285 5947201.116 574378.103 5947194.237 574378.1399 5947194.0974 
574378.5506 5947192.5449 574379.4059 5947189.3114 574380.149 5947186.502 
574426.985 5947186.997 574427.029 5947183.999 574427.377 5947160.216 
574427.4063 5947158.4949 574427.68 5947142.413 574428.001 5947123.528 
574479.523 5947127.996 574493.66 5947128.054 574566.816 5947128.362 
574660.776 5947128.758 574702.71 5947128.931 574748.898 5947129.118 
574748.851 5947140.899 574748.634 5947195.807 574749.3333 5947198.2846 
574749.288 5947198.286 574748.8682 5947198.262 574746.314 5947198.116 
574742.937 5947195.261 574714.736 5947194.727 574687.7748 5947194.6093 
574687.2995 5947194.6072 574662.28 5947194.498 574656.5284 5947194.4654 
574616.9809 5947194.2415 574603.2677 5947194.1639 574552.9263 5947193.8788 
574538.8944 5947193.7994 574530.878 5947193.754 574493.9571 5947193.6182 
574478.968 5947193.563 574468.1712 5947193.4481 574429.8917 5947193.0407 
574405.7029 5947192.7832 574380.975 5947192.52 574379.7467 5947192.5088 
574379.4301 5947193.8154 574380.544 5947193.8181 574380.922 5947193.819 
574442.6917 5947194.2113 574468.0652 5947194.3724 574504.8425 5947194.606 
574537.603 5947194.814 574566.322 5947194.832 574587.2593 5947194.8843 
574606.372 5947194.932 574636.4465 5947195.1116 574706.516 5947195.53 
574743.164 5947196.251 574746.413 5947199.209 574749.603 5947199.483 
574750.2133 5947201.4025 574753.332 5947212.452 574757.1362 5947225.8977 
574765.4627 5947255.3274 574789.035 5947338.643 574782.785 5947335.029 
574752.0167 5947342.6421 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung><xplan:BP_KomplexeZweckbestGruen><xplan:allgemein>2600</xplan:allgemein></xplan:BP_KomplexeZweckbestGruen></xplan:zweckbestimmung>     
      <xplan:zugunstenVon>Kirchengemeindeverband Bergstedt</xplan:zugunstenVon>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnittFlaeche gml:id="Gml_4E63A33A-A768-4C86-AE79-C4B16975CA3B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574555.1311 5947383.4728</gml:lowerCorner>
          <gml:upperCorner>574640.8562 5947462.3717</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gliederung1>(B)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#Gml_1359F5B4-2C2F-4F29-A5E5-1E584F05862A" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_964FACFF-9B77-4661-9E6B-1508208B603F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574640.8562 5947443.7979 574559.5761 5947462.3717 574555.1311 5947440.6229 
574596.477 5947427.8434 574587.5161 5947393.4741 574624.0287 5947383.4728 
574640.8562 5947443.7979 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextAbschnittFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_9E5811D1-ECAE-4E40-9E7E-8E93C40E51E1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574555.1311 5947383.4728</gml:lowerCorner>
          <gml:upperCorner>574640.8562 5947462.3717</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_3C8CE750-017F-48DA-8B98-6B92BD766AED" srsName="EPSG:25832">
          <gml:posList>574559.5761 5947462.3717 574640.8562 5947443.7979 574624.0287 5947383.4728 
574587.5161 5947393.4741 574596.477 5947427.8434 574555.1311 5947440.6229 
574559.5761 5947462.3717 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnittFlaeche gml:id="Gml_32E783AD-7718-4E26-8DA7-648E07E2AB65">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574714.3488 5947155.331</gml:lowerCorner>
          <gml:upperCorner>574745.4374 5947182.0539</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gliederung1>(C)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#Gml_1359F5B4-2C2F-4F29-A5E5-1E584F05862A" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7DD1080A-C6DF-446E-A1E3-EC3AB39B72A0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574745.4374 5947155.9924 574745.0405 5947182.0539 574717.1269 5947181.7893 
574714.3488 5947155.331 574745.4374 5947155.9924 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextAbschnittFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_E4B4E687-C981-4A85-9CCD-FA56EE3F1E96">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574714.3488 5947155.331</gml:lowerCorner>
          <gml:upperCorner>574745.4374 5947182.0539</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_07134A69-52A3-4627-8947-CB53C9D79387" srsName="EPSG:25832">
          <gml:posList>574717.1269 5947181.7893 574745.0405 5947182.0539 574745.4374 5947155.9924 
574714.3488 5947155.331 574717.1269 5947181.7893 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnittFlaeche gml:id="Gml_E0E037A7-5BEC-4AB6-A4C6-4385E8689852">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574394.4934 5947201.5008</gml:lowerCorner>
          <gml:upperCorner>574472.8102 5947258.651</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gliederung1>(A)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#Gml_1359F5B4-2C2F-4F29-A5E5-1E584F05862A" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A20B0823-3CA3-4326-B98A-37BB0E798902" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574462.121 5947203.8292 574472.8102 5947240.9768 574410.0509 5947258.651 
574403.962 5947236.197 574394.4934 5947201.5008 574462.121 5947203.8292 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextAbschnittFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_01F29C20-B21E-4CA2-AB1E-D60CAA2F6435">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574394.4934 5947201.5008</gml:lowerCorner>
          <gml:upperCorner>574472.8102 5947258.651</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_452423BC-F9F6-4AC1-B89C-862CF06B305C" srsName="EPSG:25832">
          <gml:posList>574403.962 5947236.197 574410.0509 5947258.651 574472.8102 5947240.9768 
574462.121 5947203.8292 574394.4934 5947201.5008 574403.962 5947236.197 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_F3AE7979-CAD1-414A-9171-57CCD19315A0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574379.3393 5947128.7932</gml:lowerCorner>
          <gml:upperCorner>574379.3393 5947128.7932</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_D0998297-28E5-47B3-835D-5669B22FD110" srsName="EPSG:25832">
          <gml:pos>574379.3393 5947128.7932</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_2CD06187-C80F-4A4B-B234-C34933F6BAF0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574387.0652 5947129.7457</gml:lowerCorner>
          <gml:upperCorner>574387.0652 5947129.7457</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_2AE2923C-C84F-4E9F-9048-4CBA98F62AF1" srsName="EPSG:25832">
          <gml:pos>574387.0652 5947129.7457</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_5233035D-BFC3-47E5-B67D-DD794F9CBF70">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574386.1127 5947124.7715</gml:lowerCorner>
          <gml:upperCorner>574386.1127 5947124.7715</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_7F873530-12B4-446E-AA01-A5259AA36018" srsName="EPSG:25832">
          <gml:pos>574386.1127 5947124.7715</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_12C4F03D-657D-457A-9B2D-189399A2074C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574394.791 5947123.819</gml:lowerCorner>
          <gml:upperCorner>574394.791 5947123.819</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_B9165F9B-196C-4481-8B06-FE0BA3191F6A" srsName="EPSG:25832">
          <gml:pos>574394.791 5947123.819</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_8B3465E4-E6DC-43C3-BBE4-55076DF50741">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574913.3754 5947228.4884</gml:lowerCorner>
          <gml:upperCorner>574913.3754 5947228.4884</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_98FB612D-FBF7-4E76-A257-E511AEA3D000" srsName="EPSG:25832">
          <gml:pos>574913.3754 5947228.4884</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_6F9F150D-23F1-4306-AF2B-1A8C0F2E7AFA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574923.1121 5947260.2385</gml:lowerCorner>
          <gml:upperCorner>574923.1121 5947260.2385</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_F86BE117-48C6-4393-8B76-E70F26B5B775" srsName="EPSG:25832">
          <gml:pos>574923.1121 5947260.2385</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_25AF375B-7C1C-40ED-8F6B-8CD437F3142D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574925.5463 5947263.096</gml:lowerCorner>
          <gml:upperCorner>574925.5463 5947263.096</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_441E6E85-0261-4667-A107-7E3E676C7FF3" srsName="EPSG:25832">
          <gml:pos>574925.5463 5947263.096</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_2C217D2B-49F9-440B-B4AD-8266D4C181BB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574946.1838 5947269.0226</gml:lowerCorner>
          <gml:upperCorner>574946.1838 5947269.0226</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_6F56D311-7CAE-4187-9F9C-B152577EB7CE" srsName="EPSG:25832">
          <gml:pos>574946.1838 5947269.0226</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_2F127535-0788-4130-A6D6-BA8999337F58">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574924.0646 5947422.5871</gml:lowerCorner>
          <gml:upperCorner>574924.0646 5947422.5871</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_AF7DB971-BE5E-41D6-B7E8-C32D4F21917A" srsName="EPSG:25832">
          <gml:pos>574924.0646 5947422.5871</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_BA1A00F0-BA67-46C2-B2B4-7538A3F69CFC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574923.0062 5947414.8613</gml:lowerCorner>
          <gml:upperCorner>574923.0062 5947414.8613</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_B0C3A0B8-E4FC-4876-B317-358E2476CAEE" srsName="EPSG:25832">
          <gml:pos>574923.0062 5947414.8613</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_20353AA5-6F87-4EA8-AC5D-22CF63EF2FB8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574902.6862 5947406.3946</gml:lowerCorner>
          <gml:upperCorner>574902.6862 5947406.3946</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_8475D81E-6F0A-4A98-8293-9C2752B8B38D" srsName="EPSG:25832">
          <gml:pos>574902.6862 5947406.3946</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_CADA9092-A424-4C68-A12A-1AD40D4D10EF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574852.9444 5947397.6104</gml:lowerCorner>
          <gml:upperCorner>574852.9444 5947397.6104</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_24A5E9EE-F6FC-4017-9302-A6861C90B974" srsName="EPSG:25832">
          <gml:pos>574852.9444 5947397.6104</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_D111EF9A-A62E-41DC-B971-565A25E4BB34">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574852.9444 5947384.9104</gml:lowerCorner>
          <gml:upperCorner>574852.9444 5947384.9104</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_07A29D9D-8E03-40DB-A312-7A7947877B8A" srsName="EPSG:25832">
          <gml:pos>574852.9444 5947384.9104</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_C875030A-9332-4069-A898-E43C98152788">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574810.611 5947334.3749</gml:lowerCorner>
          <gml:upperCorner>574810.611 5947334.3749</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_51B9B9A8-2E28-4CD4-A121-5A4C6E29B633" srsName="EPSG:25832">
          <gml:pos>574810.611 5947334.3749</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_1DC005C3-8403-47B3-A3EE-89F328C335D2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574779.8135 5947337.2324</gml:lowerCorner>
          <gml:upperCorner>574779.8135 5947337.2324</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_B1B5EACE-E7EF-407D-A579-4717443B063F" srsName="EPSG:25832">
          <gml:pos>574779.8135 5947337.2324</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_4761B301-F292-47FE-8951-D449B7AEF856">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574744.5444 5947452.632</gml:lowerCorner>
          <gml:upperCorner>574744.5444 5947452.632</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_5EDE7503-3D22-4680-BD33-B5BFA9B37B17" srsName="EPSG:25832">
          <gml:pos>574744.5444 5947452.632</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_45B67BFD-E736-4909-BB39-D071E904E9C8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574702.5589 5947473.3971</gml:lowerCorner>
          <gml:upperCorner>574702.5589 5947473.3971</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_798B6A3A-4F9E-4436-B5D8-FCE39B058E9A" srsName="EPSG:25832">
          <gml:pos>574702.5589 5947473.3971</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_09994523-31CD-4C35-9050-D3D8BC1E22C5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574646.3309 5947447.0876</gml:lowerCorner>
          <gml:upperCorner>574646.3309 5947447.0876</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_71310902-E4A2-448F-BC27-98AFBEB872F3" srsName="EPSG:25832">
          <gml:pos>574646.3309 5947447.0876</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>2000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_5851D686-0618-4B91-9572-2CEAA65F902A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574647.839 5947451.5326</gml:lowerCorner>
          <gml:upperCorner>574647.839 5947451.5326</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_536B399D-4F46-4400-B4E9-40355C87B9BF" srsName="EPSG:25832">
          <gml:pos>574647.839 5947451.5326</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>2000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_52134E9F-8BE3-490B-A2D7-C4A733939A72">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574649.109 5947455.7395</gml:lowerCorner>
          <gml:upperCorner>574649.109 5947455.7395</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_ACEC29BD-EB9D-4120-B57A-0260DEAE8B9B" srsName="EPSG:25832">
          <gml:pos>574649.109 5947455.7395</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>2000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_DBD55B85-95D8-4EB0-9B9C-26305CB1AB5C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574650.6965 5947460.9782</gml:lowerCorner>
          <gml:upperCorner>574650.6965 5947460.9782</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_89B9BC2E-4129-426B-85F6-6FF68B036246" srsName="EPSG:25832">
          <gml:pos>574650.6965 5947460.9782</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>2000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_131B39A6-861D-48E2-90A2-AACFBB9862E7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574651.649 5947464.8676</gml:lowerCorner>
          <gml:upperCorner>574651.649 5947464.8676</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_3E2C3E9E-32C5-4D82-BB75-219926686DB8" srsName="EPSG:25832">
          <gml:pos>574651.649 5947464.8676</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>2000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_133D6B99-06B2-4D7B-90C5-02FBB70E52AD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574652.919 5947469.392</gml:lowerCorner>
          <gml:upperCorner>574652.919 5947469.392</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_9902C55B-1D35-4650-8A54-3B7BADD6FE5B" srsName="EPSG:25832">
          <gml:pos>574652.919 5947469.392</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>2000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="Gml_AEFEA73C-B50D-400C-8AB3-41D512C0A3F2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574759.9961 5947207.2952</gml:lowerCorner>
          <gml:upperCorner>574759.9961 5947207.2952</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">32.3</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_5752B9FC-F9AD-4140-9A4E-18971423AC9A" srsName="EPSG:25832">
          <gml:pos>574759.9961 5947207.2952</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="Gml_A98C305C-D608-4BC7-9DCF-B6B369993841">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574612.2525 5947392.2393</gml:lowerCorner>
          <gml:upperCorner>574612.2525 5947392.2393</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">34.9</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_EA1152E0-2C04-4034-BD10-8F5670C48E51" srsName="EPSG:25832">
          <gml:pos>574612.2525 5947392.2393</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="Gml_4F55C9BF-70A6-4C26-8D7E-55E6B5F0D18B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574440.445 5947253.4783</gml:lowerCorner>
          <gml:upperCorner>574440.445 5947253.4783</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">31.1</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_7253F759-A931-4B5F-AE3F-F50030470142" srsName="EPSG:25832">
          <gml:pos>574440.445 5947253.4783</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_066A9E89-B2ED-415A-8434-BEDF7E89F2F1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574386.4875 5947128.1451</gml:lowerCorner>
          <gml:upperCorner>574408.6697 5947183.3289</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_1536E711-9838-4812-B12F-E16D2C610E01" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574408.5358 5947183.2714 574396.536 5947183.3289 574396.4856 5947182.0771 
574386.4875 5947181.8797 574386.7257 5947141.7128 574389.5588 5947141.7612 
574389.4039 5947128.1451 574407.4029 5947128.3412 574407.559 5947142.069 
574404.7447 5947142.0209 574404.9081 5947160.7863 574408.6697 5947160.6909 
574408.5358 5947183.2714 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_FE245DC4-84F0-4C8A-9F8E-0DBDBE1C6868">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574821.3165 5947221.0812</gml:lowerCorner>
          <gml:upperCorner>574841.8816 5947244.4704</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_2D11A39F-8EB3-46E5-919E-55BE7BBF088B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574837.8638 5947225.1431 574841.8816 5947240.653 574827.3755 5947244.4704 
574821.3165 5947221.0812 574837.8638 5947225.1431 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_61B3046A-A685-43BB-B726-0BC6CF9C5887">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574782.5682 5947214.2544</gml:lowerCorner>
          <gml:upperCorner>574827.194 5947311.828</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_C92237A8-B45A-4C9C-AFE8-4C4BFB5BB3BF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574827.194 5947307.444 574809.736 5947311.828 574782.5682 5947216.3848 
574790.2101 5947214.2544 574798.0201 5947215.9112 574809.8551 5947257.4889 
574812.742 5947256.6728 574827.194 5947307.444 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_25288D32-8780-4AC5-887B-E357442D50A4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574845.9104 5947232.0798</gml:lowerCorner>
          <gml:upperCorner>574938.5063 5947254.4012</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_5279F9E8-A753-4AAF-B698-EB8B26A28077" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574938.5063 5947247.2587 574884.1706 5947247.3424 574885.403 5947254.4012 
574867.1694 5947254.117 574865.6093 5947247.1654 574849.3774 5947247.2027 
574845.9104 5947232.236 574846.2289 5947232.2329 574861.8688 5947232.0798 
574863.6047 5947239.0805 574882.7617 5947239.2729 574881.5322 5947232.2304 
574899.0044 5947232.2391 574919.5873 5947232.2493 574934.4922 5947232.2567 
574938.5063 5947247.2587 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_375987FB-FCD2-4D3B-BB2A-4F1C53C0D9FC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574834.5892 5947257.6335</gml:lowerCorner>
          <gml:upperCorner>574942.9236 5947307.2068</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_5D8471EB-29DA-425B-8A3E-B2EA2B535B65" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574942.9236 5947275.085 574930.9499 5947278.0214 574924.4503 5947279.6153 
574920.3339 5947280.6248 574911.5341 5947282.7828 574891.2281 5947287.7626 
574876.5672 5947291.358 574858.2415 5947295.8521 574859.7586 5947301.9828 
574838.4792 5947307.2068 574834.5892 5947291.6867 574855.944 5947286.4442 
574853.9543 5947278.3701 574920.9075 5947261.9508 574922.0436 5947266.3382 
574929.5049 5947264.539 574928.536 5947260.08 574938.5121 5947257.6335 
574942.9236 5947275.085 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_4898DA1D-2D52-4633-BD02-362581AB1CDD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574943.1997 5947358.8467</gml:lowerCorner>
          <gml:upperCorner>574956.352 5947374.0974</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_07AAB3E3-BCCE-4FB6-8999-18CA5E310478" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574956.352 5947371.573 574944.6205 5947374.0974 574943.1997 5947361.6318 
574954.8721 5947358.8467 574956.352 5947371.573 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_1610E07B-396B-4E84-93A2-5FF6B90278C8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574941.083 5947340.22</gml:lowerCorner>
          <gml:upperCorner>574954.2353 5947355.4707</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_493CB7FF-CA10-458C-96D7-2A541B74E7F0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574954.2353 5947352.9463 574942.5038 5947355.4707 574941.083 5947343.0051 
574952.7554 5947340.22 574954.2353 5947352.9463 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_495B1C2E-B3D2-4206-8E90-191959203566">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574814.2914 5947327.4571</gml:lowerCorner>
          <gml:upperCorner>574851.7379 5947401.44</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E9619163-4F8A-4514-8335-A1DCB5471AA5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574851.7379 5947397.475 574834.18 5947401.44 574814.2914 5947331.7679 
574831.7676 5947327.4571 574851.7379 5947397.475 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_3BD4A5D4-2830-41F7-A922-4D3C21933C6B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574847.9072 5947304.9421</gml:lowerCorner>
          <gml:upperCorner>574949.6702 5947341.0832</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_648616DA-3098-40EA-9CA5-CDF286B94B1D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574875.8208 5947341.0832 574872.5518 5947329.4776 574850.9941 5947335.1903 
574847.9072 5947323.5942 574866.1197 5947318.7679 574867.7026 5947324.3949 
574947.5492 5947304.9421 574949.6702 5947322.8167 574875.8208 5947341.0832 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_C281F72B-3423-45B1-B52F-599C4153C5AA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574706.1693 5947383.8876</gml:lowerCorner>
          <gml:upperCorner>574720.6424 5947404.1864</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7CF6613D-98A1-4633-8B14-099B18C5B9B3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574720.6424 5947401.2495 574710.9586 5947404.1864 574706.1693 5947386.8245 
574715.8531 5947383.8876 574720.6424 5947401.2495 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_5C383746-97D5-477D-82D6-C4B868C4CFF3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574728.1041 5947396.6446</gml:lowerCorner>
          <gml:upperCorner>574754.2582 5947417.2833</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_5B0FBCDB-AFB7-4A9E-BAE9-03DD6652EF18" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574754.2582 5947411.0882 574732.1518 5947417.2833 574728.1041 5947402.8398 
574750.2106 5947396.6446 574754.2582 5947411.0882 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_FB55ABCF-E8A6-4BC8-B516-6C19DAB48F7A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574719.1899 5947425.0584</gml:lowerCorner>
          <gml:upperCorner>574733.5805 5947445.0646</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_EC2175F4-BC23-4107-B6AB-E7100339F7FE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574733.5805 5947442.3659 574724.1349 5947445.0646 574719.1899 5947427.7572 
574728.6355 5947425.0584 574733.5805 5947442.3659 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_93284040-8E0F-440C-A9CD-43B8CE37A8AE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574699.3346 5947446.9898</gml:lowerCorner>
          <gml:upperCorner>574721.1667 5947468.4009</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_693F92B1-01D7-413D-BF51-B2EDA8E2F046" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574721.1667 5947464.4467 574704.1323 5947468.4009 574699.3346 5947450.9772 
574716.2296 5947446.9898 574721.1667 5947464.4467 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_8B473ED1-518C-4432-8C7E-692561FC97FD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574678.308 5947433.927</gml:lowerCorner>
          <gml:upperCorner>574692.7817 5947453.9546</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_FDBFA344-5A9D-4DC4-A87A-FBC400D93DB0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574692.7817 5947451.2559 574683.1773 5947453.9546 574678.308 5947436.6258 
574687.9124 5947433.927 574692.7817 5947451.2559 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
	  <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_26D4A77A-D4F3-416C-A4D9-145ED2997421">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574655.2448 5947454.4796</gml:lowerCorner>
          <gml:upperCorner>574684.7907 5947478.3757</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_813633B3-4F35-4734-BBCF-242AF3E5C859" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574684.7907 5947471.9077 574659.7457 5947478.3757 574655.2448 5947460.9475 
574680.2898 5947454.4796 574684.7907 5947471.9077 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_CB1C4B5E-C403-429F-A0D6-EC292A422BC8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574625.3823 5947462.0933</gml:lowerCorner>
          <gml:upperCorner>574652.6549 5947484.9374</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_F14926CB-B977-447F-AD26-14D87F039740" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574652.6549 5947479.6457 574629.3715 5947484.9374 574625.3823 5947467.385 
574648.6657 5947462.0933 574652.6549 5947479.6457 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_94ECE09F-43F8-4E3A-89B4-641807379381">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574589.114 5947460.6637</gml:lowerCorner>
          <gml:upperCorner>574611.8474 5947493.4513</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E2AE024D-74B6-4CF6-88D5-1972ACAEB29C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574611.8474 5947489.2429 574597.4498 5947493.4513 574589.114 5947464.872 
574603.5116 5947460.6637 574611.8474 5947489.2429 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_CE34F6AB-75D9-486D-AAE9-21664D030DB2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574561.7186 5947475.1264</gml:lowerCorner>
          <gml:upperCorner>574585.0273 5947492.9851</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_15F9768C-5B7E-489D-B41D-6C54BD345C7D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574585.0273 5947492.6633 574564.9087 5947492.9851 574561.7186 5947475.27 
574580.9703 5947475.1264 574585.0273 5947492.6633 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_686A1A57-9994-4D66-916B-717F3B249231">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574743.7687 5947425.5834</gml:lowerCorner>
          <gml:upperCorner>574810.2058 5947457.0197</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_06510DFF-317C-4B6F-88E8-C39831E88093" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574810.2058 5947442.9876 574749.0767 5947457.0197 574743.7687 5947439.8201 
574805.613 5947425.5834 574810.2058 5947442.9876 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_8E5A7CA2-57E0-4B32-9103-83E85366B48E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574747.4953 5947343.2395</gml:lowerCorner>
          <gml:upperCorner>574799.9041 5947414.2803</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_C6F26E2B-CA04-4B66-8D3C-48C30B54CB43" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574799.9041 5947410.0926 574782.398 5947414.2803 574772.7129 5947378.7208 
574757.454 5947382.6061 574747.4953 5947347.6939 574764.9355 5947343.2395 
574774.8978 5947378.1645 574790.1503 5947374.2809 574799.9041 5947410.0926 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_95467D92-7AA9-4DC8-AE26-48A4BA76EEB6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574839.654 5947391.8965</gml:lowerCorner>
          <gml:upperCorner>574960.8828 5947436.1056</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E75A8C38-82A4-485C-AC5C-937F6F5A58C6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574919.6138 5947411.8223 574921.0051 5947418.8203 574843.7014 5947436.1056 
574839.654 5947418.5665 574899.0221 5947405.2917 574900.3098 5947410.5893 
574907.4536 5947409.1341 574906.2409 5947403.6775 574958.9284 5947391.8965 
574960.8828 5947409.9035 574927.7547 5947417.311 574926.3025 5947410.4253 
574919.6138 5947411.8223 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_7C7846E7-C451-4C24-B71D-CEB2958CD92D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574386.4875 5947128.1451</gml:lowerCorner>
          <gml:upperCorner>574408.6697 5947183.3289</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_9EA5F400-B6DE-4583-9DE8-2E2F7045134E" srsName="EPSG:25832">
          <gml:posList>574386.7257 5947141.7128 574386.4875 5947181.8797 574396.4856 5947182.0771 
574396.536 5947183.3289 574408.5358 5947183.2714 574408.6697 5947160.6909 
574404.9081 5947160.7863 574404.7447 5947142.0209 574407.559 5947142.069 
574407.4029 5947128.3412 574389.4039 5947128.1451 574389.5588 5947141.7612 
574386.7257 5947141.7128 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_13AA2C3E-8FBA-4BC4-A5DD-114276CAFE95">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574821.3165 5947221.0812</gml:lowerCorner>
          <gml:upperCorner>574841.8816 5947244.4704</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_1862F719-314D-4BC7-A078-FE8F214C2EE2" srsName="EPSG:25832">
          <gml:posList>574821.3165 5947221.0812 574827.3755 5947244.4704 574841.8816 5947240.653 
574837.8638 5947225.1431 574821.3165 5947221.0812 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_75083B3A-089E-4C86-8C14-1C8868716D6E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574782.5682 5947214.2544</gml:lowerCorner>
          <gml:upperCorner>574827.194 5947311.828</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_CF428682-E2C6-41AB-8D1E-1B75B91ED253" srsName="EPSG:25832">
          <gml:posList>574782.5682 5947216.3848 574809.736 5947311.828 574827.194 5947307.444 
574812.742 5947256.6728 574809.8551 5947257.4889 574798.0201 5947215.9112 
574790.2101 5947214.2544 574782.5682 5947216.3848 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_F5DC5D57-D392-4C3B-A36D-B48B25F191B9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574845.9104 5947232.0798</gml:lowerCorner>
          <gml:upperCorner>574938.5063 5947254.4012</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_C2FAC2F9-84D6-4C2C-BDAC-07293C92B93B" srsName="EPSG:25832">
          <gml:posList>574845.9104 5947232.236 574849.3774 5947247.2027 574865.6093 5947247.1654 
574867.1694 5947254.117 574885.403 5947254.4012 574884.1706 5947247.3424 
574938.5063 5947247.2587 574934.4922 5947232.2567 574919.5873 5947232.2493 
574899.0044 5947232.2391 574881.5322 5947232.2304 574882.7617 5947239.2729 
574863.6047 5947239.0805 574861.8688 5947232.0798 574846.2289 5947232.2329 
574845.9104 5947232.236 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_ABE7288D-A15C-4952-AA73-0EB911527A6A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574834.5892 5947257.6335</gml:lowerCorner>
          <gml:upperCorner>574942.9236 5947307.2068</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_56DBA7A8-E5E7-46EB-A707-CC6BE4CED3E4" srsName="EPSG:25832">
          <gml:posList>574838.4792 5947307.2068 574859.7586 5947301.9828 574858.2415 5947295.8521 
574876.5672 5947291.358 574891.2281 5947287.7626 574911.5341 5947282.7828 
574920.3339 5947280.6248 574924.4503 5947279.6153 574930.9499 5947278.0214 
574942.9236 5947275.085 574938.5121 5947257.6335 574928.536 5947260.08 
574929.5049 5947264.539 574922.0436 5947266.3382 574920.9075 5947261.9508 
574853.9543 5947278.3701 574855.944 5947286.4442 574834.5892 5947291.6867 
574838.4792 5947307.2068 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_E6801B0D-3414-40EF-A671-EB57EC022C89">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574943.1997 5947358.8467</gml:lowerCorner>
          <gml:upperCorner>574956.352 5947374.0974</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_C1129E54-E316-4B2E-9BD4-AB9D19B5BB67" srsName="EPSG:25832">
          <gml:posList>574944.6205 5947374.0974 574956.352 5947371.573 574954.8721 5947358.8467 
574943.1997 5947361.6318 574944.6205 5947374.0974 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_9DC9424D-F417-475B-A3BB-5A25A0A4C382">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574941.083 5947340.22</gml:lowerCorner>
          <gml:upperCorner>574954.2353 5947355.4707</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_236B08AF-F64C-417E-B52B-CDC7A2EC1D07" srsName="EPSG:25832">
          <gml:posList>574942.5038 5947355.4707 574954.2353 5947352.9463 574952.7554 5947340.22 
574941.083 5947343.0051 574942.5038 5947355.4707 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_F755745E-44C1-4684-93C1-8552735936E8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574814.2914 5947327.4571</gml:lowerCorner>
          <gml:upperCorner>574851.7379 5947401.44</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_846F8319-6A2A-42F3-9E75-89F1E1F48DCA" srsName="EPSG:25832">
          <gml:posList>574834.18 5947401.44 574851.7379 5947397.475 574831.7676 5947327.4571 
574814.2914 5947331.7679 574834.18 5947401.44 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_11C06D8F-853C-4676-BA70-DD0A5352D463">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574847.9072 5947304.9421</gml:lowerCorner>
          <gml:upperCorner>574949.6702 5947341.0832</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_DECAECD5-8B52-4FEA-BB1C-807D05D61DF0" srsName="EPSG:25832">
          <gml:posList>574949.6702 5947322.8167 574947.5492 5947304.9421 574867.7026 5947324.3949 
574866.1197 5947318.7679 574847.9072 5947323.5942 574850.9941 5947335.1903 
574872.5518 5947329.4776 574875.8208 5947341.0832 574949.6702 5947322.8167 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_66B3A988-076F-4BAF-A66C-13FD30587278">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574706.1693 5947383.8876</gml:lowerCorner>
          <gml:upperCorner>574720.6424 5947404.1864</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_93B62EFC-DED2-478A-83FC-4DC77A75DF08" srsName="EPSG:25832">
          <gml:posList>574710.9586 5947404.1864 574720.6424 5947401.2495 574715.8531 5947383.8876 
574706.1693 5947386.8245 574710.9586 5947404.1864 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_E33DC17A-118D-4CC2-BDFB-C30F5644CDEE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574728.1041 5947396.6446</gml:lowerCorner>
          <gml:upperCorner>574754.2582 5947417.2833</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_E6F11CB7-FCD1-4E5E-B987-907F29A1ABE6" srsName="EPSG:25832">
          <gml:posList>574732.1518 5947417.2833 574754.2582 5947411.0882 574750.2106 5947396.6446 
574728.1041 5947402.8398 574732.1518 5947417.2833 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_39F9F27A-A940-4D7A-981B-948CF93BA741">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574719.1899 5947425.0584</gml:lowerCorner>
          <gml:upperCorner>574733.5805 5947445.0646</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_5488D262-804E-484C-BD41-9D0AA23C921B" srsName="EPSG:25832">
          <gml:posList>574724.1349 5947445.0646 574733.5805 5947442.3659 574728.6355 5947425.0584 
574719.1899 5947427.7572 574724.1349 5947445.0646 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_276F51A9-7BDF-4ACB-8469-615E8B2A5916">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574699.3346 5947447.6435</gml:lowerCorner>
          <gml:upperCorner>574718.3405 5947468.4009</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_19D6ECD2-E499-440A-9629-904BAD0087B9" srsName="EPSG:25832">
          <gml:posList>574704.1323 5947468.4009 574721.1667 5947464.4467 574716.2296 5947446.9898 
574699.3346 5947450.9772 574704.1323 5947468.4009 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_741E4DC3-1E5E-4631-B298-7A0030926073">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574678.308 5947433.927</gml:lowerCorner>
          <gml:upperCorner>574692.7817 5947453.9546</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4B078914-26B7-40CA-9A5D-A52547AE1217" srsName="EPSG:25832">
          <gml:posList>574683.1773 5947453.9546 574692.7817 5947451.2559 574687.9124 5947433.927 
574678.308 5947436.6258 574683.1773 5947453.9546 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_3484B9F2-2FE0-428B-82D2-FDB6261042C3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574655.2448 5947454.4796</gml:lowerCorner>
          <gml:upperCorner>574684.7907 5947478.3757</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_10FB6952-A0CF-4256-B02C-3B468C42507A" srsName="EPSG:25832">
          <gml:posList>574659.7457 5947478.3757 574684.7907 5947471.9077 574680.2898 5947454.4796 
574655.2448 5947460.9475 574659.7457 5947478.3757 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_C15C5CF9-6D2E-4823-8036-87B3C2E94BA8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574625.3823 5947462.0933</gml:lowerCorner>
          <gml:upperCorner>574652.6549 5947484.9374</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_9EDBEAB8-30A7-4D60-9259-8F1E7549CF8A" srsName="EPSG:25832">
          <gml:posList>574629.3715 5947484.9374 574652.6549 5947479.6457 574648.6657 5947462.0933 
574625.3823 5947467.385 574629.3715 5947484.9374 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_CDB5BC28-C1D6-4175-8DE9-A26348C3F7DF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574589.114 5947460.6637</gml:lowerCorner>
          <gml:upperCorner>574611.8474 5947493.4513</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_9474AAB4-EDDC-4C20-8391-F0887399B57E" srsName="EPSG:25832">
          <gml:posList>574589.114 5947464.872 574597.4498 5947493.4513 574611.8474 5947489.2429 
574603.5116 5947460.6637 574589.114 5947464.872 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_1C732454-A955-4E78-813E-2DCDDC83E66D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574561.7186 5947475.1264</gml:lowerCorner>
          <gml:upperCorner>574585.0273 5947492.9851</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_5E08129D-EA6B-494B-BBCB-076966565029" srsName="EPSG:25832">
          <gml:posList>574561.7186 5947475.27 574564.9087 5947492.9851 574585.0273 5947492.6633 
574580.9703 5947475.1264 574561.7186 5947475.27 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_D9C0CCF2-B7C2-46FA-AD45-060113C347D6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574743.7687 5947425.5834</gml:lowerCorner>
          <gml:upperCorner>574810.2058 5947457.0197</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_0504A804-0220-4835-9DC7-A2F1120D34FF" srsName="EPSG:25832">
          <gml:posList>574743.7687 5947439.8201 574749.0767 5947457.0197 574810.2058 5947442.9876 
574805.613 5947425.5834 574743.7687 5947439.8201 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_16AF1467-6CA7-4E8A-A21B-F95A3083A2CA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574747.4953 5947343.2395</gml:lowerCorner>
          <gml:upperCorner>574799.9041 5947414.2803</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_D6D3ADBE-7F84-4CA8-A4B6-912FEDCF7F40" srsName="EPSG:25832">
          <gml:posList>574772.7129 5947378.7208 574782.398 5947414.2803 574799.9041 5947410.0926 
574790.1503 5947374.2809 574774.8978 5947378.1645 574764.9355 5947343.2395 
574747.4953 5947347.6939 574757.454 5947382.6061 574772.7129 5947378.7208 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_8247CD61-BFA8-47DF-BA23-2F899276F2CC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574839.654 5947391.8965</gml:lowerCorner>
          <gml:upperCorner>574960.8828 5947436.1056</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_34AB4607-6B03-4E93-8EC5-1775997F68F2" srsName="EPSG:25832">
          <gml:posList>574958.9284 5947391.8965 574906.2409 5947403.6775 574907.4536 5947409.1341 
574900.3098 5947410.5893 574899.0221 5947405.2917 574839.654 5947418.5665 
574843.7014 5947436.1056 574921.0051 5947418.8203 574919.6138 5947411.8223 
574926.3025 5947410.4253 574927.7547 5947417.311 574960.8828 5947409.9035 
574958.9284 5947391.8965 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NebenanlagenFlaeche gml:id="Gml_076896AF-FC83-4E2C-A08A-31BDF824E6CD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574591.0514 5947457.835</gml:lowerCorner>
          <gml:upperCorner>574623.553 5947498.947</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_F51E0C9C-9D12-4DE4-B21D-5AF7973B64EA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574623.553 5947487.85 574618.735 5947489.213 574615.891 5947491.515 
574592.838 5947498.947 574591.0514 5947495.6773 574597.4498 5947493.4513 
574612.764 5947488.979 574608.582 5947474.117 574607.463 5947474.432 
574603.69 5947461.042 574615.053 5947457.835 574623.553 5947487.85 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung><xplan:BP_KomplexeZweckbestNebenanlagen><xplan:allgemein>3200</xplan:allgemein></xplan:BP_KomplexeZweckbestNebenanlagen></xplan:zweckbestimmung>     
    </xplan:BP_NebenanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="Gml_20D16DE3-2C57-475A-86ED-E594E8838EFD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574379.4301 5947192.5088</gml:lowerCorner>
          <gml:upperCorner>574749.603 5947199.483</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_6F0ED5B0-FA5F-4806-9532-C0887CBC1774" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574749.382 5947198.457 574749.603 5947199.483 574746.413 5947199.209 
574743.164 5947196.251 574706.516 5947195.53 574636.4465 5947195.1116 
574606.372 5947194.932 574587.2593 5947194.8843 574566.322 5947194.832 
574537.603 5947194.814 574504.8425 5947194.606 574468.0652 5947194.3724 
574442.6917 5947194.2113 574380.922 5947193.819 574380.544 5947193.8181 
574379.4301 5947193.8154 574379.7467 5947192.5088 574380.975 5947192.52 
574405.7029 5947192.7832 574429.8917 5947193.0407 574468.1712 5947193.4481 
574478.968 5947193.563 574493.9571 5947193.6182 574530.878 5947193.754 
574538.8944 5947193.7994 574552.9263 5947193.8788 574603.2677 5947194.1639 
574616.9809 5947194.2415 574656.5284 5947194.4654 574662.28 5947194.498 
574687.2995 5947194.6072 574687.7748 5947194.6093 574714.736 5947194.727 
574742.937 5947195.261 574746.314 5947198.116 574748.8682 5947198.262 
574749.288 5947198.286 574749.3333 5947198.2846 574749.382 5947198.457 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung><xplan:SO_KomplexeFestlegungGewaesser><xplan:allgemein>1000</xplan:allgemein></xplan:SO_KomplexeFestlegungGewaesser></xplan:artDerFestlegung>      
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UnverbindlicheVormerkung gml:id="Gml_5F06A281-791F-4B8E-9CC6-101E5690A093">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574377.5296 5947186.502</gml:lowerCorner>
          <gml:upperCorner>574749.603 5947199.483</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_2F28D465-3E8E-419F-B5A5-DA078CA9C0A6" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_0C5014F2-0B33-4C64-AA84-B895FA9E25A2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574426.985 5947186.997 574748.66 5947189.2228 574748.634 5947195.807 
574749.603 5947199.483 574377.5296 5947196.4067 574380.149 5947186.502 
574426.985 5947186.997 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:vormerkung>Vorgesehener Arbeits- und Schauweg</xplan:vormerkung>
    </xplan:BP_UnverbindlicheVormerkung>
  </gml:featureMember>
</xplan:XPlanAuszug>
