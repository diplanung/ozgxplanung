/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.report.geojson.jts;

import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.GEOMETRYCOLLECTION;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.LINESTRING;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.MULTILINESTRING;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.MULTIPOINT;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.MULTIPOLYGON;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.POINT;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.POLYGON;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import de.latlon.xplan.validator.report.geojson.model.Geometry;
import de.latlon.xplan.validator.report.geojson.model.GeometryCollection;
import de.latlon.xplan.validator.report.geojson.model.LineString;
import de.latlon.xplan.validator.report.geojson.model.MultiLineString;
import de.latlon.xplan.validator.report.geojson.model.MultiPoint;
import de.latlon.xplan.validator.report.geojson.model.MultiPolygon;
import de.latlon.xplan.validator.report.geojson.model.Point;
import de.latlon.xplan.validator.report.geojson.model.Polygon;
import org.junit.jupiter.api.Test;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class JtsToGeoJsonGeometryBuilderTest {

	private final GeometryFactory geometryFactory = new GeometryFactory();

	@Test
	public void test_Point() {
		org.locationtech.jts.geom.Point point = createPoint(5.4, 10.8);

		Geometry geometry = JtsToGeoJsonGeometryBuilder.createGeometry(point);
		assertThat(geometry, instanceOf(Point.class));
		assertThat(geometry.getType(), is(POINT));
		assertThat(((Point) geometry).getPosition().get(0).doubleValue(), is(point.getX()));
		assertThat(((Point) geometry).getPosition().get(1).doubleValue(), is(point.getY()));
	}

	@Test
	public void test_MultiPoint() {
		org.locationtech.jts.geom.Point point1 = createPoint(5.4, 10.8);
		org.locationtech.jts.geom.Point point2 = createPoint(6.4, 10.5);
		org.locationtech.jts.geom.Point[] points = { point1, point2 };
		org.locationtech.jts.geom.MultiPoint multiPoint = geometryFactory.createMultiPoint(points);

		Geometry geometry = JtsToGeoJsonGeometryBuilder.createGeometry(multiPoint);
		assertThat(geometry, instanceOf(MultiPoint.class));
		assertThat(geometry.getType(), is(MULTIPOINT));
		assertThat(((MultiPoint) geometry).getPositions().size(), is(2));
		assertThat(((MultiPoint) geometry).getPositions().get(0).get(0).doubleValue(), is(point1.getX()));
		assertThat(((MultiPoint) geometry).getPositions().get(0).get(1).doubleValue(), is(point1.getY()));
	}

	@Test
	public void test_LineString() {
		org.locationtech.jts.geom.LineString lineString = createLineString1();

		Geometry geometry = JtsToGeoJsonGeometryBuilder.createGeometry(lineString);
		assertThat(geometry, instanceOf(LineString.class));
		assertThat(geometry.getType(), is(LINESTRING));
		assertThat(((LineString) geometry).getCoordinates().size(), is(3));
		assertThat(((LineString) geometry).getCoordinates().get(0).get(0).doubleValue(),
				is(lineString.getCoordinate().getX()));
		assertThat(((LineString) geometry).getCoordinates().get(0).get(1).doubleValue(),
				is(lineString.getCoordinate().getY()));
	}

	@Test
	public void test_MultiLineString() {
		org.locationtech.jts.geom.LineString lineString1 = createLineString1();
		org.locationtech.jts.geom.LineString lineString2 = createLineString2();
		org.locationtech.jts.geom.LineString[] lineStrings = { lineString1, lineString2 };
		org.locationtech.jts.geom.MultiLineString multiLineString = geometryFactory.createMultiLineString(lineStrings);

		Geometry geometry = JtsToGeoJsonGeometryBuilder.createGeometry(multiLineString);
		assertThat(geometry, instanceOf(MultiLineString.class));
		assertThat(geometry.getType(), is(MULTILINESTRING));
		assertThat(((MultiLineString) geometry).getLineStringCoordinates().size(), is(2));
		assertThat(((MultiLineString) geometry).getLineStringCoordinates().get(0).get(0).get(0).doubleValue(),
				is(lineString1.getCoordinates()[0].getX()));
		assertThat(((MultiLineString) geometry).getLineStringCoordinates().get(0).get(0).get(1).doubleValue(),
				is(lineString1.getCoordinates()[0].getY()));
	}

	@Test
	public void test_Polygon() {
		org.locationtech.jts.geom.Polygon polygon = createPolygon1();

		Geometry geometry = JtsToGeoJsonGeometryBuilder.createGeometry(polygon);
		assertThat(geometry, instanceOf(Polygon.class));
		assertThat(geometry.getType(), is(POLYGON));
		assertThat(((Polygon) geometry).getLinearRings().size(), is(1));
		assertThat(((Polygon) geometry).getLinearRings().get(0).size(), is(5));

		assertThat(((Polygon) geometry).getLinearRings().get(0).get(0).get(0).doubleValue(),
				is(polygon.getExteriorRing().getCoordinates()[0].getX()));
		assertThat(((Polygon) geometry).getLinearRings().get(0).get(0).get(1).doubleValue(),
				is(polygon.getExteriorRing().getCoordinates()[0].getY()));
	}

	@Test
	public void test_MultiPolygon() {
		org.locationtech.jts.geom.Polygon polygon1 = createPolygon1();
		org.locationtech.jts.geom.Polygon polygon2 = createPolygon1();
		org.locationtech.jts.geom.Polygon[] polygons = { polygon1, polygon2 };
		org.locationtech.jts.geom.MultiPolygon multiPolygon = geometryFactory.createMultiPolygon(polygons);

		Geometry geometry = JtsToGeoJsonGeometryBuilder.createGeometry(multiPolygon);
		assertThat(geometry, instanceOf(MultiPolygon.class));
		assertThat(geometry.getType(), is(MULTIPOLYGON));
		assertThat(((MultiPolygon) geometry).getLinearRings().size(), is(2));
		assertThat(((MultiPolygon) geometry).getLinearRings().get(0).size(), is(1));
		assertThat(((MultiPolygon) geometry).getLinearRings().get(0).get(0).size(), is(5));

		assertThat(((MultiPolygon) geometry).getLinearRings().get(0).get(0).get(0).get(0).doubleValue(), is(
				((org.locationtech.jts.geom.Polygon) multiPolygon.getGeometryN(0)).getExteriorRing().getCoordinates()[1]
					.getX()));
		assertThat(((MultiPolygon) geometry).getLinearRings().get(0).get(0).get(0).get(1).doubleValue(), is(
				((org.locationtech.jts.geom.Polygon) multiPolygon.getGeometryN(0)).getExteriorRing().getCoordinates()[0]
					.getY()));
	}

	@Test
	public void test_GeometryCollection() {
		org.locationtech.jts.geom.Point point1 = createPoint(5.4, 10.8);
		org.locationtech.jts.geom.LineString lineString1 = createLineString1();
		org.locationtech.jts.geom.Polygon polygon1 = createPolygon1();
		org.locationtech.jts.geom.Geometry[] geoms = { point1, lineString1, polygon1 };
		org.locationtech.jts.geom.GeometryCollection multiGeometry = geometryFactory.createGeometryCollection(geoms);

		Geometry geometry = JtsToGeoJsonGeometryBuilder.createGeometry(multiGeometry);
		assertThat(geometry, instanceOf(GeometryCollection.class));
		assertThat(geometry.getType(), is(GEOMETRYCOLLECTION));
		assertThat(((GeometryCollection) geometry).getGeometries().size(), is(3));
		assertThat(((GeometryCollection) geometry).getGeometries().get(0), instanceOf(Point.class));
		assertThat(((GeometryCollection) geometry).getGeometries().get(0).getType(), is(POINT));
		assertThat(((GeometryCollection) geometry).getGeometries().get(1), instanceOf(LineString.class));
		assertThat(((GeometryCollection) geometry).getGeometries().get(1).getType(), is(LINESTRING));
		assertThat(((GeometryCollection) geometry).getGeometries().get(2), instanceOf(Polygon.class));
		assertThat(((GeometryCollection) geometry).getGeometries().get(2).getType(), is(POLYGON));

		assertThat(((Point) ((GeometryCollection) geometry).getGeometries().get(0)).getPosition().get(0).doubleValue(),
				is(point1.getX()));
		assertThat(((Point) ((GeometryCollection) geometry).getGeometries().get(0)).getPosition().get(1).doubleValue(),
				is(point1.getY()));
	}

	private org.locationtech.jts.geom.LineString createLineString1() {
		org.locationtech.jts.geom.Coordinate[] coordinates = { createCoordinate(5.7, 10.9), createCoordinate(5.7, 11.9),
				createCoordinate(5.8, 12.9) };
		return geometryFactory.createLineString(coordinates);
	}

	private org.locationtech.jts.geom.LineString createLineString2() {
		org.locationtech.jts.geom.Coordinate[] coordinates = { createCoordinate(4.7, 12.9), createCoordinate(4.7, 13.9),
				createCoordinate(4.8, 11.9) };
		return geometryFactory.createLineString(coordinates);
	}

	private org.locationtech.jts.geom.Polygon createPolygon1() {
		org.locationtech.jts.geom.Coordinate[] coordinates = { createCoordinate(5.7, 10.9), createCoordinate(5.7, 11.9),
				createCoordinate(5.8, 12.9), createCoordinate(5.7, 11.9), createCoordinate(5.7, 10.9) };
		LinearRing interior = geometryFactory.createLinearRing(coordinates);
		return geometryFactory.createPolygon(interior);
	}

	private org.locationtech.jts.geom.Point createPoint(double x, double y) {
		Coordinate coordinate = new Coordinate(x, y);
		return geometryFactory.createPoint(coordinate);
	}

	private org.locationtech.jts.geom.Coordinate createCoordinate(double x, double y) {
		return new Coordinate(x, y);
	}

}
