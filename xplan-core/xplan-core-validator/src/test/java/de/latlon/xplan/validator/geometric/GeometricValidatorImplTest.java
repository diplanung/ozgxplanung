/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.geometric;

import static de.latlon.xplan.validator.geometric.GeometricValidatorImpl.SKIP_FLAECHENSCHLUSS_OPTION;
import static de.latlon.xplan.validator.geometric.GeometricValidatorImpl.SKIP_OPTIONS;
import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.ERROR;
import static de.latlon.xplan.validator.web.shared.ValidationType.GEOMETRIC;
import static de.latlon.xplan.validator.web.shared.ValidationType.SEMANTIC;
import static de.latlon.xplan.validator.web.shared.ValidationType.SYNTACTIC;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.validator.ValidatorException;
import de.latlon.xplan.validator.XPlanGmlValidation;
import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFinding;
import de.latlon.xplan.validator.geometric.result.GeometricValidationRule;
import de.latlon.xplan.validator.listener.InactiveValidationProcessListener;
import de.latlon.xplan.validator.planinfo.PlanInfoParser;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.report.ValidatorResult;
import de.latlon.xplan.validator.web.shared.ValidationOption;
import de.latlon.xplan.validator.web.shared.ValidationSettings;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:schneider@occamlabs.de">Markus Schneider</a>
 */
public class GeometricValidatorImplTest {

	@Disabled("TODO: test plan is required (with only few features)")
	@Test
	public void testValidateGeometryWithNullVoOptions() throws Exception {
		XPlanArchive archive = getTestArchive("xplan51/BP2070.zip");
		ValidatorResult report = validateGeometryAndReturnReport(archive, null);
		assertNotEquals(null, report);
	}

	@Test
	public void testValidateGeometryWithBrokenGeometry() throws Exception {
		XPlanArchive archive = getTestArchive("xplan41/Eidelstedt_4_V4-broken-geometry.zip");
		ValidatorResult report = validateGeometryAndReturnReport(archive, SKIP_OPTIONS);
		GeometricValidatorResult geometricReport = (GeometricValidatorResult) report;
		int numberOfErrors = geometricReport.getErrors().size();
		int numberOfWarnings = geometricReport.getWarnings().size();

		assertThat(report.isValid(), is(false));
		assertThat(numberOfErrors, is(1));
		assertThat(numberOfWarnings, is(0));

		List<GeometricValidationRule> geometricValidationRules = geometricReport.getRules();
		assertThat(geometricValidationRules.size(), is(2));

		GeometricValidationFinding finding = geometricValidationRules.get(0).getFindings().get(0);
		assertThat(finding.getLevel(), is(ERROR));
		assertThat(finding.getGmlIds().size(), is(0));
	}

	@Test
	public void testValidateGeometryWithInvalidFlaechenschluss_skipped() throws Exception {
		XPlanArchive archive = getTestArchive("xplan51/BP2070.zip");
		ValidatorResult report = validateGeometryAndReturnReport(archive, SKIP_OPTIONS);
		GeometricValidatorResult geometricReport = (GeometricValidatorResult) report;
		int numberOfErrors = geometricReport.getErrors().size();

		assertThat(report.isValid(), is(false));
		assertThat(numberOfErrors, is(6));
	}

	@Disabled("TODO: test plan is required (with only few features)")
	@Test
	public void testValidateGeometryWithInvalidFlaechenschluss_notskipped() throws Exception {
		XPlanArchive archive = getTestArchive("xplan51/BP2070.zip");
		List<ValidationOption> voOptions = Collections
			.singletonList(new ValidationOption(SKIP_FLAECHENSCHLUSS_OPTION, Boolean.toString(false)));
		ValidatorResult report = validateGeometryAndReturnReport(archive, voOptions);
		GeometricValidatorResult geometricReport = (GeometricValidatorResult) report;
		int numberOfErrors = geometricReport.getErrors().size();

		assertThat(report.isValid(), is(false));
		assertThat(numberOfErrors, is(12));
	}

	@Test
	public void testValidateGeometryWithInteriorRing_ValidOrientation() throws Exception {
		XPlanArchive archive = getTestArchive(getClass().getResourceAsStream("geometryOrientationValid.gml"));
		GeometricValidatorResult report = (GeometricValidatorResult) validateGeometryAndReturnReport(archive,
				SKIP_OPTIONS);

		assertThat(report.isValid(), is(true));
		assertThat(report.getErrors().size(), is(0));
		assertThat(report.getWarnings().size(), is(0));
	}

	@Test
	public void testValidateGeometryWithInteriorRing_InvalidOrientation() throws Exception {
		XPlanArchive archive = getTestArchive(getClass().getResourceAsStream("geometryOrientationInvalid.gml"));
		GeometricValidatorResult report = (GeometricValidatorResult) validateGeometryAndReturnReport(archive,
				SKIP_OPTIONS);

		assertThat(report.isValid(), is(false));
		assertThat(report.getErrors().size(), is(2));
		assertThat(report.getWarnings().size(), is(0));

		assertThat(report.getRules().size(), is(1));

		GeometricValidationFinding finding1 = report.getRules().get(0).getFindings().get(0);
		assertThat(finding1.getLevel(), is(ERROR));
		assertThat(finding1.getGmlIds(), hasItem("GML_88258139-e3ff-4388-9838-a30775e1f8bf"));
		assertNotNull(finding1.getMarkerGeom());

		GeometricValidationFinding finding2 = report.getRules().get(0).getFindings().get(1);
		assertThat(finding2.getLevel(), is(ERROR));
		assertThat(finding2.getGmlIds(), hasItem("GML_88258139-e3ff-4388-9838-a30775e1f8bf"));
		assertNotNull(finding2.getMarkerGeom());
	}

	@Test
	public void testValidateGeometry_InvalidSelfintersectionDetected() throws Exception {
		XPlanArchive archive = getTestArchive(getClass().getResourceAsStream("invalidSelfintersectionDeteced.gml"));
		GeometricValidatorResult report = (GeometricValidatorResult) validateGeometryAndReturnReport(archive,
				SKIP_OPTIONS);

		assertThat(report.isValid(), is(true));
		assertThat(report.getErrors().size(), is(0));
		assertThat(report.getWarnings().size(), is(0));
	}

	@Test
	public void testValidateGeometry_PolygonPatch_doppelt_nicht_zusammenhaengend() throws Exception {
		XPlanArchive archive = getTestArchive(getClass()
			.getResourceAsStream("patches/BP_6-0_false_2.2.2.1_PolygonPatch_doppelt_nicht_zusammenhaengend.gml"));
		GeometricValidatorResult report = (GeometricValidatorResult) validateGeometryAndReturnReport(archive,
				SKIP_OPTIONS);

		assertThat(report.isValid(), is(false));
		assertThat(report.getErrors().size(), is(1));
		assertThat(report.getWarnings().size(), is(0));

		List<GeometricValidationRule> planRules = report.getRules();
		assertThat(planRules.size(), is(2));

		GeometricValidationFinding finding = planRules.get(1).getFindings().get(0);
		assertThat(finding.getLevel(), is(ERROR));
		assertThat(finding.getGmlIds(), hasItem("GML_032"));
	}

	@Test
	public void testValidateGeometry_PolygonPatch_nicht_zusammenhaengend_Flaechenschlussobjekt() throws Exception {
		XPlanArchive archive = getTestArchive(getClass().getResourceAsStream(
				"patches/BP_6-0_false_2.2.2.1_PolygonPatch_nicht_zusammenhaengend_Flaechenschlussobjekt.gml"));
		GeometricValidatorResult report = (GeometricValidatorResult) validateGeometryAndReturnReport(archive,
				SKIP_OPTIONS);

		assertThat(report.isValid(), is(false));
		assertThat(report.getErrors().size(), is(1));
		assertThat(report.getWarnings().size(), is(0));

		List<GeometricValidationRule> planRules = report.getRules();
		assertThat(planRules.size(), is(2));

		GeometricValidationFinding finding = planRules.get(1).getFindings().get(0);

		assertThat(finding.getLevel(), is(ERROR));
		assertThat(finding.getGmlIds(), hasItem("GML_022"));
	}

	@Test
	public void testValidateGeometry_PolygonPatch_Ueberlappung() throws Exception {
		XPlanArchive archive = getTestArchive(
				getClass().getResourceAsStream("patches/BP_6-0_false_2.2.2.1_PolygonPatch_Ueberlappung.gml"));
		GeometricValidatorResult report = (GeometricValidatorResult) validateGeometryAndReturnReport(archive,
				SKIP_OPTIONS);

		assertThat(report.isValid(), is(false));
		assertThat(report.getErrors().size(), is(1));
		assertThat(report.getWarnings().size(), is(0));

		List<GeometricValidationRule> planRules = report.getRules();
		assertThat(planRules.size(), is(2));

		GeometricValidationFinding finding = planRules.get(1).getFindings().get(0);
		assertThat(finding.getLevel(), is(ERROR));
		assertThat(finding.getGmlIds(), hasItem("GML_022"));

	}

	@Test
	public void testValidateGeometry_PolygonPatch_Flaechenschlussobjekt() throws Exception {
		XPlanArchive archive = getTestArchive(
				getClass().getResourceAsStream("patches/BP_6-0_true_PolygonPatch_Flaechenschlussobjekt.gml"));
		GeometricValidatorResult report = (GeometricValidatorResult) validateGeometryAndReturnReport(archive,
				SKIP_OPTIONS);

		assertThat(report.isValid(), is(true));
		assertThat(report.getErrors().size(), is(0));
		assertThat(report.getWarnings().size(), is(0));
	}

	private XPlanArchive getTestArchive(String name) throws IOException {
		XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();
		return archiveCreator.createXPlanArchiveFromZip(name, getClass().getResourceAsStream("/testdata/" + name));
	}

	private XPlanArchive getTestArchive(InputStream inputStream) throws IOException {
		XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();
		return archiveCreator.createXPlanArchiveFromGml("test", inputStream);
	}

	private ValidatorResult validateGeometryAndReturnReport(XPlanArchive archive, List<ValidationOption> voOptions)
			throws ValidatorException {
		ValidationSettings validationSettings = new ValidationSettings("test", List.of(SYNTACTIC, SEMANTIC, GEOMETRIC),
				voOptions);
		GeometricValidatorImpl geometricValidator = new GeometricValidatorImpl();
		XPlanGmlValidation xPlanGmlValidation = new XPlanGmlValidation(geometricValidator,
				new PlanInfoParser(Optional.empty()), archive, validationSettings,
				new InactiveValidationProcessListener());
		ValidatorReport report = new ValidatorReport();
		xPlanGmlValidation.executeGmlValidation("uuid", report);
		return report.getPlanInfoReport()
			.getPlanInfos()
			.values()
			.stream()
			.findFirst()
			.get()
			.getGeometricValidatorResult();
	}

}
