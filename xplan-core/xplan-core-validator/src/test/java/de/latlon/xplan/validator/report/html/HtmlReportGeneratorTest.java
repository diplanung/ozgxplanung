/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.report.html;

import static de.latlon.xplan.commons.XPlanType.BP_Plan;
import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.ERROR;
import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.WARNING;
import static de.latlon.xplan.validator.report.SkipCode.SYNTAX_ERRORS;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.xmlunit.matchers.EvaluateXPathMatcher.hasXPath;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import de.latlon.xplan.commons.XPlanVersion;
import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFinding;
import de.latlon.xplan.validator.geometric.result.GeometricValidationRule;
import de.latlon.xplan.validator.planinfo.PlanInfo;
import de.latlon.xplan.validator.planinfo.PlanInfoReport;
import de.latlon.xplan.validator.report.ValidatorDetail;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.semantic.report.InvalidFeaturesResult;
import de.latlon.xplan.validator.semantic.report.SemanticValidatorResult;
import de.latlon.xplan.validator.syntactic.report.SyntacticValidatorResult;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class HtmlReportGeneratorTest {

	@Test
	public void testGenerateHtmlReport() throws Exception {
		ByteArrayOutputStream html = new ByteArrayOutputStream();
		HtmlReportGenerator htmlReportGenerator = new HtmlReportGenerator();

		htmlReportGenerator.generateHtmlReport(createValidationReport(), html);

		assertThat(html.toString(), hasXPath("/html/body/h1", containsString("Validierungsbericht")));
	}

	@Test
	public void testGenerateHtmlReport_WithSemanticFailures() throws Exception {
		ByteArrayOutputStream html = new ByteArrayOutputStream();
		HtmlReportGenerator htmlReportGenerator = new HtmlReportGenerator();

		htmlReportGenerator.generateHtmlReport(createValidatorReportWithSemanticFailures(), html);

		assertThat(html.toString(), hasXPath("/html/body/p[4]/p[4]", containsString("semantischen")));
		assertThat(html.toString(),
				hasXPath("/html/body/p[4]/p[4]/p/ul/li[1]", containsString("2 Validierungsregeln")));
		assertThat(html.toString(),
				hasXPath("/html/body/p[4]/p[4]/p/ul/li[2]", containsString("1 Validierungsregeln nicht")));
		assertThat(html.toString(),
				hasXPath("/html/body/p[4]/p[4]/p/ul/li[3]", containsString("1 Validierungsregeln")));
	}

	@Test
	public void testGenerateHtmlReport_WithSyntacticDetailHint() throws Exception {
		ByteArrayOutputStream html = new ByteArrayOutputStream();
		HtmlReportGenerator htmlReportGenerator = new HtmlReportGenerator();

		htmlReportGenerator.generateHtmlReport(createValidatorReportWithSyntacticDetailHint(), html);

		assertThat(html.toString(), hasXPath("/html/body/p/div", containsString("detailsHint")));
	}

	@Test
	public void testGenerateHtmlReport_WithGeometricWarnings() throws Exception {
		ByteArrayOutputStream html = new ByteArrayOutputStream();
		HtmlReportGenerator htmlReportGenerator = new HtmlReportGenerator();

		htmlReportGenerator.generateHtmlReport(createValidatorReportWithGeometricWarnings(), html);

		assertThat(html.toString(), hasXPath("/html/body/p[4]/p[4]", containsString("geometrischen")));
		assertThat(html.toString(), hasXPath("/html/body/p[4]/p[4]/p[2]", containsString("1 Warnungen")));
	}

	@Test
	public void testGenerateHtmlReport_OrderOfValidations() throws Exception {
		ByteArrayOutputStream html = new ByteArrayOutputStream();
		HtmlReportGenerator htmlReportGenerator = new HtmlReportGenerator();

		htmlReportGenerator.generateHtmlReport(createValidatorReportWithAllTypes(), html);

		assertThat(html.toString(), hasXPath("/html/body/p[4]", containsString("syntaktischen")));
		assertThat(html.toString(), hasXPath("/html/body/p[5]/p[4]", containsString("semantischen")));
		assertThat(html.toString(), hasXPath("/html/body/p[5]/p[5]", containsString("geometrischen")));
	}

	@Test
	public void testGenerateHtmlReport_Profil() throws Exception {
		ByteArrayOutputStream html = new ByteArrayOutputStream();
		HtmlReportGenerator htmlReportGenerator = new HtmlReportGenerator();

		htmlReportGenerator.generateHtmlReport(createValidatorReportWithAllTypesAndProfile(), html);

		assertThat(html.toString(), hasXPath("/html/body/p[4]", containsString("syntaktischen")));
		assertThat(html.toString(), hasXPath("/html/body/p[5]/p[4]", containsString("semantischen")));
		assertThat(html.toString(), hasXPath("/html/body/p[5]/p[5]", containsString("geometrischen")));
		assertThat(html.toString(), hasXPath("/html/body/p[5]/p[6]", containsString("gegen das Profil")));
	}

	@Test
	public void testGenerateXmlReportWithNullReport() {
		assertThrows(IllegalArgumentException.class, () -> {
			HtmlReportGenerator htmlReportGenerator = new HtmlReportGenerator();
			htmlReportGenerator.generateHtmlReport(null, new ByteArrayOutputStream());
		});
	}

	@Test
	public void testGenerateXmlReportWithNullOutputStream() {
		assertThrows(IllegalArgumentException.class, () -> {
			HtmlReportGenerator htmlReportGenerator = new HtmlReportGenerator();
			htmlReportGenerator.generateHtmlReport(createValidationReport(), null);
		});
	}

	private ValidatorReport createValidatorReportWithSemanticFailures() {
		ValidatorReport validatorReport = createValidationReport();
		SemanticValidatorResult semanticValidatorResult = new SemanticValidatorResult();
		semanticValidatorResult.addRule("1.1", "Test valid", Collections.emptyList());
		InvalidFeaturesResult id_12 = new InvalidFeaturesResult("id_12");
		semanticValidatorResult.addRule("1.2", "Test in valid", Collections.singletonList(id_12));
		validatorReport.getPlanInfoReport()
			.getPlanInfos()
			.values()
			.stream()
			.findFirst()
			.get()
			.semanticValidatorResult(semanticValidatorResult);
		return validatorReport;
	}

	private ValidatorReport createValidatorReportWithSyntacticDetailHint() {
		ValidatorReport validatorReport = createValidationReport();
		List<String> messages = Collections.singletonList("Error in xml...");
		ValidatorDetail detail = new ValidatorDetail("detailsHint");
		SyntacticValidatorResult syntacticValidatorResult = new SyntacticValidatorResult(messages, detail);
		validatorReport.setSyntacticValidatorResult(syntacticValidatorResult);
		return validatorReport;
	}

	private ValidatorReport createValidatorReportWithGeometricWarnings() {
		ValidatorReport validatorReport = createValidationReport();
		GeometricValidatorResult geometricValidatorResult = new GeometricValidatorResult(
				singletonList(new GeometricValidationRule("1.9", "test")
					.findings(Arrays.asList(new GeometricValidationFinding().level(WARNING).message("Warning..."),
							new GeometricValidationFinding().level(ERROR).message("Error...")))));
		validatorReport.getPlanInfoReport()
			.getPlanInfos()
			.values()
			.stream()
			.findFirst()
			.get()
			.geometricValidatorResult(geometricValidatorResult);
		return validatorReport;
	}

	private ValidatorReport createValidatorReportWithAllTypes() {
		ValidatorReport validatorReport = createValidationReport();
		SyntacticValidatorResult syntacticValidatorResult = new SyntacticValidatorResult(Collections.emptyList(), null);
		validatorReport.setSyntacticValidatorResult(syntacticValidatorResult);

		SemanticValidatorResult semanticValidatorResult = new SemanticValidatorResult();
		semanticValidatorResult.addRule("1.1", "Test valid", Collections.emptyList());
		InvalidFeaturesResult id_12 = new InvalidFeaturesResult("id_12");
		semanticValidatorResult.addRule("1.2", "Test in valid", Collections.singletonList(id_12));
		GeometricValidatorResult geometricValidatorResult = new GeometricValidatorResult(SYNTAX_ERRORS);
		validatorReport.getPlanInfoReport()
			.getPlanInfos()
			.values()
			.stream()
			.findFirst()
			.get()
			.semanticValidatorResult(semanticValidatorResult)
			.geometricValidatorResult(geometricValidatorResult);
		return validatorReport;
	}

	private ValidatorReport createValidatorReportWithAllTypesAndProfile() {
		ValidatorReport validatorReport = createValidatorReportWithAllTypes();

		SemanticValidatorResult profileValidatorResult = new SemanticValidatorResult();
		profileValidatorResult.addRule("a", "Test valid", Collections.emptyList());
		InvalidFeaturesResult invalidFeaturesResult = new InvalidFeaturesResult("id_profile10");
		profileValidatorResult.addRule("b", "Test invalid", Collections.singletonList(invalidFeaturesResult));
		validatorReport.getPlanInfoReport()
			.getPlanInfos()
			.values()
			.stream()
			.findFirst()
			.get()
			.addSemanticProfileValidatorResults(profileValidatorResult);
		return validatorReport;
	}

	private ValidatorReport createValidationReport() {
		ValidatorReport validatorReport = new ValidatorReport();
		PlanInfo planInfo = new PlanInfo("GML_plan1").name("PLAN_NAME").version(XPlanVersion.XPLAN_60).type(BP_Plan);
		validatorReport.setPlanInfoReport(new PlanInfoReport().planInfos(singletonMap("GML_plan1", planInfo)));
		validatorReport.setValidationName("VALIDATION_NAME");
		return validatorReport;
	}

}
