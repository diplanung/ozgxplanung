/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.report.pdf;

import static de.latlon.xplan.commons.XPlanType.BP_Plan;
import static java.util.Collections.singletonMap;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.latlon.xplan.commons.XPlanVersion;
import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.planinfo.PlanInfo;
import de.latlon.xplan.validator.planinfo.PlanInfoReport;
import de.latlon.xplan.validator.report.ValidatorDetail;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.semantic.report.SemanticValidatorResult;
import de.latlon.xplan.validator.syntactic.report.SyntacticValidatorResult;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version $Revision: $, $Date: $
 */
public class ReportBuilderTest {

	@TempDir
	public File tmpFolder;

	// @Ignore("Make a visibility check if the report looks like expected!")
	@Test
	public void testCreateReportAsPdf() throws Exception {
		PdfReportGenerator reportBuilder = new PdfReportGenerator();

		File file = File.createTempFile("junit", null, tmpFolder);
		OutputStream os = new FileOutputStream(file);
		reportBuilder.createPdfReport(createReport(), os);
		os.close();
		assertThat(Files.size(file.toPath()), is(not(0)));
	}

	@Test
	public void testCreateReportAsPdfWithNullReport() {
		assertThrows(IllegalArgumentException.class, () -> {
			PdfReportGenerator reportBuilder = new PdfReportGenerator();
			reportBuilder.createPdfReport(null, createSimpleOutputStream());
		});
	}

	@Test
	public void testCreateReportAsPdfWithNullStream() {
		assertThrows(IllegalArgumentException.class, () -> {
			PdfReportGenerator reportBuilder = new PdfReportGenerator();
			reportBuilder.createPdfReport(createReport(), null);
		});
	}

	private ValidatorReport createReport() {
		ValidatorReport report = new ValidatorReport();
		PlanInfo planInfo = new PlanInfo("GML_plan1").name("PLAN_NAME")
			.version(XPlanVersion.XPLAN_60)
			.type(BP_Plan)
			.semanticValidatorResult(createSemanticResult())
			.geometricValidatorResult(createGeometricResult());
		report.setPlanInfoReport(new PlanInfoReport().planInfos(singletonMap("GML_plan1", planInfo)));
		report.setValidationName("VALIDATION_NAME");

		report.setSyntacticValidatorResult(createSyntacticResult());
		return report;
	}

	private SemanticValidatorResult createSemanticResult() {
		SemanticValidatorResult semanticResult = new SemanticValidatorResult();
		for (int i = 0; i < 20; i++) {
			if (i == 8 || i == 2)
				semanticResult.addRule("Name" + i,
						"HinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweis"
								+ i,
						Collections.emptyList());
			else
				semanticResult.addRule("Name" + i, "Hinweis" + i, Collections.emptyList());
		}
		return semanticResult;
	}

	private SyntacticValidatorResult createSyntacticResult() {
		List<String> messages = new ArrayList<String>();
		addMessages("Syntactic ", messages);
		ValidatorDetail detail = new ValidatorDetail("detailsHint");
		return new SyntacticValidatorResult(messages, detail);
	}

	private GeometricValidatorResult createGeometricResult() {
		return new GeometricValidatorResult(Collections.emptyList());
	}

	private void addMessages(String prefix, List<String> allMessages) {
		for (int i = 0; i < 20; i++) {
			allMessages.add(prefix + i);
		}
	}

	private OutputStream createSimpleOutputStream() {
		return new ByteArrayOutputStream();
	}

}
