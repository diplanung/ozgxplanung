/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.syntactic;

import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Arrays;
import java.util.List;

/**
 * Tests for <link>SyntacticValidatorImpl</link>
 *
 * @author <a href="mailto:erben@lat-lon.de">Alexander Erben</a>
 * @version $Revision: $, $Date: $
 */
public class ParameterizedSyntacticValidatorTest {

	private String testResource;

	public static List<Object[]> data() {
		return Arrays.asList(new Object[][] { { "xplan41/BP2070.zip" }, { "xplan41/BP2135.zip" },
				{ "xplan41/Demo.zip" }, { "xplan41/Eidelstedt_4_V4.zip" }, { "xplan41/FPlan.zip" },
				{ "xplan41/LA22.zip" }, { "xplan41/LA67.zip" }, { "xplan41/BPlan001_4-1.zip" },
				{ "xplan40/BPlan004_4-0.zip" }, { "xplan50/BP2070.zip" }, { "xplan50/BP2135.zip" },
				{ "xplan50/FPlan.zip" }, { "xplan50/LA22.zip" }, { "xplan50/LA67.zip" }, { "xplan51/BP2070.zip" },
				{ "xplan51/BP2135.zip" }, { "xplan51/FPlan.zip" }, { "xplan51/LA22.zip" }, { "xplan51/LA67.zip" } });
	}

	public void initParameterizedSyntacticValidatorTest(String testResource) {
		this.testResource = testResource;
	}

	@MethodSource("data")
	@ParameterizedTest
	public void testValidateSyntax(String testResource) throws IOException {
		initParameterizedSyntacticValidatorTest(testResource);
		XPlanArchive archive = getTestArchive(testResource);
		SyntacticValidator validator = new SyntacticValidatorImpl();
		validator.validateSyntax(archive);
		assertTrue(validator.validateSyntax(archive).isValid());
	}

	private XPlanArchive getTestArchive(String name) throws IOException {
		XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();
		return archiveCreator.createXPlanArchiveFromZip(name, getClass().getResourceAsStream("/testdata/" + name));
	}

}
