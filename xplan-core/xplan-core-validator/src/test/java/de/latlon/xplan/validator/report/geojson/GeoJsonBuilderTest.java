/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.report.geojson;

import static de.latlon.xplan.commons.XPlanType.BP_Plan;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_52;
import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.ERROR;
import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.WARNING;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.FEATURE;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.FEATURECOLLECTION;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.POINT;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFinding;
import de.latlon.xplan.validator.geometric.result.GeometricValidationRule;
import de.latlon.xplan.validator.planinfo.PlanInfo;
import de.latlon.xplan.validator.planinfo.PlanInfoReport;
import de.latlon.xplan.validator.report.ReportGenerationException;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.report.geojson.model.Feature;
import de.latlon.xplan.validator.report.geojson.model.FeatureCollection;
import de.latlon.xplan.validator.report.geojson.model.Point;
import org.deegree.cs.exceptions.UnknownCRSException;
import org.deegree.cs.persistence.CRSManager;
import org.deegree.geometry.Geometry;
import org.deegree.geometry.GeometryFactory;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class GeoJsonBuilderTest {

	@Test
	public void test_createGeoJsonFailures_NoFindings() throws ReportGenerationException {
		ValidatorReport reportWithoutFindings = createReportWithoutFindings();
		FeatureCollection geoJsonFailures = GeoJsonBuilder.createGeoJsonFailures(reportWithoutFindings);

		assertThat(geoJsonFailures, nullValue());
	}

	@Test
	public void test_createGeoJsonFailures_WithFinding() throws UnknownCRSException, ReportGenerationException {
		ValidatorReport reportWithoutFindings = createReportWithFinding();
		FeatureCollection geoJsonFailures = GeoJsonBuilder.createGeoJsonFailures(reportWithoutFindings);

		assertThat(geoJsonFailures, notNullValue());
		assertThat(geoJsonFailures.getType(), equalTo(FEATURECOLLECTION));
		assertThat(geoJsonFailures.getFeatures().size(), equalTo(1));

		Feature feature = geoJsonFailures.getFeatures().get(0);
		assertThat(feature.getGeometry(), notNullValue());
		assertThat(feature.getType(), equalTo(FEATURE));
		assertThat(feature.getGeometry().getType(), equalTo(POINT));
		assertThat(((Point) feature.getGeometry()).getPosition().get(0).doubleValue(), equalTo(9.991965747700812));
		assertThat(((Point) feature.getGeometry()).getPosition().get(1).doubleValue(), equalTo(53.665150839846035));
		Map<String, Object> properties = feature.getProperties();
		assertThat(properties.size(), equalTo(6));

		PlanInfo planInfo = reportWithoutFindings.getPlanInfoReport()
			.getPlanInfos()
			.values()
			.stream()
			.findFirst()
			.get();
		GeometricValidationRule rule = planInfo.getGeometricValidatorResult().getRules().get(0);
		GeometricValidationFinding finding = rule.getFindings().get(0);

		assertThat(properties.get("id"), equalTo(rule.getId()));
		assertThat(properties.get("title"), equalTo(rule.getTitle()));
		assertThat(properties.get("planName"), equalTo(planInfo.getName()));
		assertThat(properties.get("level"), equalTo(finding.getLevel().name()));
		assertThat(properties.get("message"), equalTo(finding.getMessage()));
		assertThat(properties.get("gmlIds"), equalTo("Gml_001, Gml_002"));
	}

	@Test
	public void test_createGeoJsonFailures_WithMultiplePlansAndFindings()
			throws UnknownCRSException, ReportGenerationException {
		ValidatorReport reportWithoutFindings = createReportWithMultiplePlansAndFindings();
		FeatureCollection geoJsonFailures = GeoJsonBuilder.createGeoJsonFailures(reportWithoutFindings);

		assertThat(geoJsonFailures, notNullValue());
		assertThat(geoJsonFailures.getFeatures().size(), equalTo(3));
	}

	private static ValidatorReport createReportWithoutFindings() {
		PlanInfo planInfo = new PlanInfo("GML_plan1").name("planName").version(XPLAN_52).type(BP_Plan);
		PlanInfoReport planInfoReport = new PlanInfoReport().planInfos(Collections.singletonMap("GML_plan1", planInfo));
		ValidatorReport sourceReport = new ValidatorReport();
		sourceReport.setPlanInfoReport(planInfoReport);
		return sourceReport;
	}

	private static ValidatorReport createReportWithFinding() throws UnknownCRSException {
		GeometricValidatorResult geometricValidatorResult = createGeometricValidatorResult();
		PlanInfo planInfo = new PlanInfo("GML_plan1").name("planName")
			.version(XPLAN_52)
			.type(BP_Plan)
			.geometricValidatorResult(geometricValidatorResult);
		PlanInfoReport planInfoReport = new PlanInfoReport().planInfos(Collections.singletonMap("GML_plan1", planInfo));
		ValidatorReport sourceReport = new ValidatorReport();
		sourceReport.setPlanInfoReport(planInfoReport);
		return sourceReport;
	}

	private static ValidatorReport createReportWithMultiplePlansAndFindings() throws UnknownCRSException {
		PlanInfo planInfo1 = new PlanInfo("GML_plan1").name("planName2")
			.version(XPLAN_52)
			.type(BP_Plan)
			.geometricValidatorResult(createGeometricValidatorResult());
		PlanInfo planInfo2 = new PlanInfo("GML_plan2").name("planName2")
			.version(XPLAN_52)
			.type(BP_Plan)
			.geometricValidatorResult(createGeometricValidatorResultWithErrorAndWarning());
		HashMap<String, PlanInfo> planInfos = new HashMap<>();
		planInfos.put(planInfo1.getPlanGmlId(), planInfo1);
		planInfos.put(planInfo2.getPlanGmlId(), planInfo2);
		PlanInfoReport planInfoReport = new PlanInfoReport().planInfos(planInfos);
		ValidatorReport sourceReport = new ValidatorReport();
		sourceReport.setPlanInfoReport(planInfoReport);
		return sourceReport;
	}

	private static GeometricValidatorResult createGeometricValidatorResult() throws UnknownCRSException {
		GeometricValidationRule rule = new GeometricValidationRule("1.1", "test1")
			.addFinding(new GeometricValidationFinding().message("fehler")
				.level(ERROR)
				.gmlIds("Gml_001", "Gml_002")
				.markerGeom(createPoint()));
		return new GeometricValidatorResult(Collections.singletonList(rule));
	}

	private static GeometricValidatorResult createGeometricValidatorResultWithErrorAndWarning()
			throws UnknownCRSException {
		GeometricValidationRule ruleError = new GeometricValidationRule("1.1", "test1")
			.addFinding(new GeometricValidationFinding().message("fehler")
				.level(ERROR)
				.gmlIds("Gml_001")
				.markerGeom(createPoint()));
		GeometricValidationRule ruleWarning = new GeometricValidationRule("1.1", "test1")
			.addFinding(new GeometricValidationFinding().message("warnung")
				.level(WARNING)
				.gmlIds("Gml_004")
				.markerGeom(createPoint()));
		return new GeometricValidatorResult(List.of(ruleError, ruleWarning));
	}

	private static Geometry createPoint() throws UnknownCRSException {
		return new GeometryFactory().createPoint("id", 565542.625, 5946724.305, CRSManager.lookup("EPSG:25832"));
	}

}
