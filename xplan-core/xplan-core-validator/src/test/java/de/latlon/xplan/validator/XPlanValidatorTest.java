/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator;

import static de.latlon.xplan.validator.web.shared.ValidationType.GEOMETRIC;
import static de.latlon.xplan.validator.web.shared.ValidationType.SEMANTIC;
import static de.latlon.xplan.validator.web.shared.ValidationType.SYNTACTIC;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import de.latlon.xplan.commons.archive.SemanticValidableXPlanArchive;
import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.feature.XPlanGmlParser;
import de.latlon.xplan.commons.feature.XPlanGmlParserBuilder;
import de.latlon.xplan.validator.geometric.GeometricValidator;
import de.latlon.xplan.validator.geometric.GeometricValidatorImpl;
import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.listener.ValidationProcessListener;
import de.latlon.xplan.validator.planinfo.PlanInfo;
import de.latlon.xplan.validator.planinfo.PlanInfoParser;
import de.latlon.xplan.validator.report.ReportArchiveGenerator;
import de.latlon.xplan.validator.report.ReportGenerationException;
import de.latlon.xplan.validator.report.SkipCode;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.semantic.SemanticValidator;
import de.latlon.xplan.validator.semantic.profile.SemanticProfileValidator;
import de.latlon.xplan.validator.semantic.report.InvalidFeaturesResult;
import de.latlon.xplan.validator.semantic.report.RuleResult;
import de.latlon.xplan.validator.semantic.report.SemanticValidatorResult;
import de.latlon.xplan.validator.syntactic.SyntacticValidator;
import de.latlon.xplan.validator.syntactic.report.SyntacticValidatorResult;
import de.latlon.xplan.validator.web.shared.ValidationSettings;
import de.latlon.xplan.validator.web.shared.ValidationType;
import org.apache.commons.io.IOUtils;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests for <link>XPlanValidator</link>
 *
 * @author <a href="mailto:erben@lat-lon.de">Alexander Erben</a>
 * @version $Revision: $, $Date: $
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class XPlanValidatorTest {

	private GeometricValidator geoVal;

	private SemanticValidator semVal;

	private SemanticValidator semValInvalid;

	private SyntacticValidator synVal;

	private SyntacticValidator synValInvalid;

	private XPlanGmlParser xPlanGmlParser;

	private ValidationProcessListener validationProcessListener;

	private static File planToValidate;

	@BeforeEach
	public void resetMocks() {
		geoVal = mockGeometricValidator();
		semVal = mockSemanticValidator();
		semValInvalid = mockSemanticValidatorWithError();
		synVal = mockSyntacticValidator();
		synValInvalid = mockSyntacticValidatorWithError();
		xPlanGmlParser = mockXPlanGmlParser();
		validationProcessListener = mock(ValidationProcessListener.class);
	}

	@BeforeAll
	public static void initFileToValidate() throws IOException {
		InputStream input = XPlanValidatorTest.class.getResourceAsStream("/testdata/xplan60/Blankenese29.gml");
		Path xPlanGml = Files.createTempFile("XPlanValidatorTest", ".gml");
		FileOutputStream output = new FileOutputStream(xPlanGml.toFile());
		IOUtils.copy(input, output);
		input.close();
		output.close();
		planToValidate = xPlanGml.toFile();
	}

	@Test
	public void testValidateNotWriteReportNoSettings() throws Exception {
		ValidationSettings settings = new ValidationSettings();
		executeValidator(geoVal, semVal, synVal, settings);

		verify(synVal, times(1)).validateSyntax(archive());
		verify(geoVal, times(0)).validateGeometry(gmlValidation());
		verifyNoInteractions(semVal);
		verifyNoInteractions(xPlanGmlParser);
	}

	@Test
	public void testValidateNotWriteReportTypeSyntax() throws Exception {
		ValidationSettings settings = new ValidationSettings("", singletonList(SYNTACTIC), emptyList());
		executeValidator(geoVal, semVal, synVal, settings);

		verify(synVal, times(1)).validateSyntax(archive());
		verify(geoVal, times(0)).validateGeometry(gmlValidation());
		verifyNoInteractions(semVal);
		verifyNoInteractions(xPlanGmlParser);
	}

	@Test
	public void testValidateNotWriteReportTypeGeometry() throws Exception {
		ValidationSettings settings = new ValidationSettings("", singletonList(GEOMETRIC), emptyList());
		executeValidator(geoVal, semVal, synVal, settings);

		verify(synVal, times(1)).validateSyntax(archive());
		verify(geoVal, times(1)).validateGeometry(gmlValidation());
		verifyNoInteractions(xPlanGmlParser);
		verifyNoInteractions(semVal);
	}

	@Test
	public void testValidateNotWriteReportTypeSemantic() throws Exception {
		ValidationSettings settings = new ValidationSettings("", singletonList(SEMANTIC), emptyList());
		executeValidator(geoVal, semVal, synVal, settings);

		verify(synVal, times(1)).validateSyntax(archive());
		verify(geoVal, times(0)).validateGeometry(gmlValidation());
		verify(semVal, times(1)).validateSemantic(semanticValidableXPlanArchive(), list());
		verifyNoInteractions(xPlanGmlParser);
	}

	@Test
	public void testValidateNotWriteReportAllTypes() throws Exception {
		List<ValidationType> validationTypes = Arrays.asList(SYNTACTIC, SEMANTIC, GEOMETRIC);
		ValidationSettings settings = new ValidationSettings("", validationTypes, emptyList());
		executeValidator(geoVal, semVal, synVal, settings);

		verify(synVal, times(1)).validateSyntax(archive());
		verify(geoVal, times(1)).validateGeometry(gmlValidation());
		verify(semVal, times(1)).validateSemantic(semanticValidableXPlanArchive(), list());
		verifyNoInteractions(xPlanGmlParser);

		verify(validationProcessListener, times(1)).validationStarted(anyString());
		verify(validationProcessListener, times(1)).validationPartStarted(anyString(), eq(SYNTACTIC));
		verify(validationProcessListener, times(1)).validationPartFinished(anyString(), eq(SYNTACTIC), anyBoolean());
		verify(validationProcessListener, times(1)).validationPartStarted(anyString(), eq(SEMANTIC));
		verify(validationProcessListener, times(1)).validationPartFinished(anyString(), eq(SEMANTIC), anyBoolean());
		verify(validationProcessListener, times(1)).validationPartStarted(anyString(), eq(GEOMETRIC));
		verify(validationProcessListener, times(1)).validationPartFinished(anyString(), eq(GEOMETRIC), anyBoolean());
		verify(validationProcessListener, times(1)).validationFinished(anyString(), anyBoolean());
	}

	@Test
	public void testValidateNotWriteReportAllTypesAndProfile() throws Exception {
		List<ValidationType> validationTypes = Arrays.asList(SYNTACTIC, SEMANTIC, GEOMETRIC);
		String profileId = "testprofile";
		ValidationSettings settings = new ValidationSettings("", validationTypes, singletonList(profileId),
				emptyList());
		SemanticProfileValidator profileValidator = mockSemanticProfileValidator(profileId);
		List<SemanticProfileValidator> profileValidators = Collections.singletonList(profileValidator);
		executeValidator(geoVal, semVal, synVal, profileValidators, settings);

		verify(synVal, times(1)).validateSyntax(archive());
		verify(geoVal, times(1)).validateGeometry(gmlValidation());
		verify(semVal, times(1)).validateSemantic(semanticValidableXPlanArchive(), list());
		verify(profileValidator, times(1)).validateSemantic(semanticValidableXPlanArchive(), list());
		verifyNoInteractions(xPlanGmlParser);
	}

	@Test
	public void testValidateNotWriteReportAllTypesAndProfile_skipSemantic() throws Exception {
		List<ValidationType> validationTypes = Arrays.asList(SYNTACTIC, GEOMETRIC);
		String profileId = "testprofile";
		ValidationSettings settings = new ValidationSettings("", validationTypes, singletonList(profileId),
				emptyList());
		SemanticProfileValidator profileValidator = mockSemanticProfileValidator(profileId);
		List<SemanticProfileValidator> profileValidators = Collections.singletonList(profileValidator);
		executeValidator(geoVal, semVal, synVal, profileValidators, settings);

		verify(synVal, times(1)).validateSyntax(archive());
		verify(geoVal, times(1)).validateGeometry(gmlValidation());
		verify(semVal, times(0)).validateSemantic(semanticValidableXPlanArchive(), list());
		verify(profileValidator, times(1)).validateSemantic(semanticValidableXPlanArchive(), list());
		verifyNoInteractions(xPlanGmlParser);
	}

	@Test
	public void testValidateNotWriteReportTypeEmpty() throws Exception {
		ValidationSettings settings = new ValidationSettings("", emptyList(), emptyList());
		executeValidator(geoVal, semVal, synVal, settings);

		verify(synVal, times(1)).validateSyntax(archive());
		verify(geoVal, times(0)).validateGeometry(gmlValidation());
		verify(semVal, times(0)).validateSemantic(semanticValidableXPlanArchive(), list());
		verifyNoInteractions(xPlanGmlParser);
	}

	@Test
	public void testWriteReport_Valid() throws Exception {
		ValidationSettings semanticSettings = new ValidationSettings("", singletonList(SEMANTIC), emptyList());
		ValidatorReport semanticReportNotValid = executeValidator(geoVal, semVal, synVal, semanticSettings);

		assertThat(semanticReportNotValid.isValid(), is(true));
		assertThat(semanticReportNotValid.getPlanInfoReport().getPlanInfos().size(), is(1));
		SemanticValidatorResult semanticValidatorResult = semanticReportNotValid.getPlanInfoReport()
			.getPlanInfos()
			.values()
			.stream()
			.findFirst()
			.get()
			.getSemanticValidatorResult();
		assertThat(semanticValidatorResult, containsSemanticResult("message", "name"));
		assertTrue(semanticReportNotValid.getSyntacticValidatorResult().getMessages().isEmpty());
	}

	@Test
	public void testValidateWithProfile() throws Exception {
		ValidationSettings semanticSettings = new ValidationSettings("", singletonList(SEMANTIC), emptyList());
		String profileId = "10";
		semanticSettings.setProfiles(Collections.singletonList(profileId));
		SemanticProfileValidator profileValidator = mockSemanticProfileValidator(profileId);
		List<SemanticProfileValidator> profileValidators = Collections.singletonList(profileValidator);

		executeValidator(geoVal, semVal, synVal, profileValidators, semanticSettings);

		verify(profileValidator, times(1)).validateSemantic(semanticValidableXPlanArchive(), list());
	}

	@Test
	public void testValidateWithoutProfile() throws Exception {
		ValidationSettings semanticSettings = new ValidationSettings("", singletonList(SEMANTIC), emptyList());
		SemanticProfileValidator profileValidator = mockSemanticProfileValidator("42");
		List<SemanticProfileValidator> profileValidators = Collections.singletonList(profileValidator);

		executeValidator(geoVal, semVal, synVal, profileValidators, semanticSettings);

		verify(profileValidator, times(0)).validateSemantic(semanticValidableXPlanArchive(), list());
	}

	@Test
	public void testWriteReport_InvalidSyntax() throws Exception {
		List<ValidationType> validationTypes = Arrays.asList(SYNTACTIC, SEMANTIC, GEOMETRIC);
		ValidationSettings settings = new ValidationSettings("", validationTypes, emptyList());
		ValidatorReport report = executeValidator(geoVal, semVal, synValInvalid, settings);

		assertThat(report.isValid(), is(false));

		assertThat(report.getSyntacticValidatorResult(), containsSyntaticResult("message"));
	}

	@Test
	public void testWriteReport_InvalidSemantic() throws Exception {
		List<ValidationType> validationTypes = Arrays.asList(SYNTACTIC, SEMANTIC, GEOMETRIC);
		ValidationSettings settings = new ValidationSettings("", validationTypes, emptyList());
		ValidatorReport report = executeValidator(geoVal, semValInvalid, synVal, settings);

		assertThat(report.isValid(), is(false));
		assertThat(report.getPlanInfoReport().getPlanInfos().size(), is(1));

		PlanInfo planInfo = report.getPlanInfoReport().getPlanInfos().values().stream().findFirst().get();
		assertThat(planInfo.getSemanticValidatorResult(), containsSemanticResult("message", "name"));
		assertTrue(report.getSyntacticValidatorResult().getMessages().isEmpty());
	}

	@Test
	public void testWriteReport_skipSemantic() throws Exception {
		List<ValidationType> validationTypes = Arrays.asList(SYNTACTIC, GEOMETRIC);
		ValidationSettings settings = new ValidationSettings("", validationTypes, emptyList());
		ValidatorReport report = executeValidator(geoVal, semVal, synVal, settings);

		assertThat(report.isValid(), is(true));
		assertThat(report.getPlanInfoReport().getPlanInfos().size(), is(1));

		PlanInfo planInfo = report.getPlanInfoReport().getPlanInfos().values().stream().findFirst().get();
		assertThat(planInfo.getSemanticValidatorResult().getSkipCode(), is(SkipCode.SKIPPED));
		assertTrue(report.getSyntacticValidatorResult().getMessages().isEmpty());
	}

	@Test
	public void testValidateWithInvalidProfile() {
		assertThrows(ValidatorException.class, () -> {
			ValidationSettings semanticSettings = new ValidationSettings("", singletonList(SEMANTIC), emptyList());
			semanticSettings.setProfiles(Collections.singletonList("99"));
			SemanticProfileValidator profileValidator = mockSemanticProfileValidator("42");
			List<SemanticProfileValidator> profileValidators = Collections.singletonList(profileValidator);

			executeValidator(geoVal, semVal, synVal, profileValidators, semanticSettings);
		});
	}

	@Test
	public void testValidateWithInvalidProfile_NoProfileConfigured() {
		assertThrows(ValidatorException.class, () -> {
			ValidationSettings semanticSettings = new ValidationSettings("", singletonList(SEMANTIC), emptyList());
			semanticSettings.setProfiles(Collections.singletonList("99"));

			executeValidator(geoVal, semVal, synVal, semanticSettings);
		});
	}

	private Matcher<SyntacticValidatorResult> containsSyntaticResult(final String messageToCheck) {
		return new TypeSafeMatcher<>() {
			@Override
			public boolean matchesSafely(SyntacticValidatorResult result) {
				if (messageToCheck != null) {
					String firstMessage = result.getMessages().get(0);
					boolean doesTypeMatch = "Syntaktische Validierung".equals(result.getType());
					boolean doesMessageMatch = firstMessage.equals("message");
					return doesMessageMatch && doesTypeMatch;
				}
				else
					return true;
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("Report should contain a SyntacticValidatorResult");
				description.appendText(" With message " + messageToCheck);
			}
		};
	}

	private Matcher<SemanticValidatorResult> containsSemanticResult(final String messageToCheck,
			final String nameToCheck) {
		return new TypeSafeMatcher<>() {
			@Override
			public boolean matchesSafely(SemanticValidatorResult result) {
				RuleResult firstRule = result.getRules().get(0);
				String firstMessage = firstRule.getMessage();
				if (!firstMessage.equals(messageToCheck))
					return false;

				String firstName = firstRule.getName();
				if (!firstName.equals(nameToCheck))
					return false;
				return "Semantische Validierung".equals(result.getType());
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("Report should contain a SemanticRuleResult");
				description.appendText(" With message ").appendValue(messageToCheck);
				description.appendText(" With name ").appendValue(nameToCheck);
			}
		};
	}

	private Matcher<GeometricValidatorResult> containsGeometricResult() {
		return new TypeSafeMatcher<>() {
			@Override
			public boolean matchesSafely(GeometricValidatorResult result) {
				boolean doesTypeMatch = "Geometrische Validierung".equals(result.getType());
				boolean areErrorsAndWarningsEmpty = result.getWarnings().isEmpty() && result.getErrors().isEmpty();
				return doesTypeMatch && areErrorsAndWarningsEmpty;
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("Report should contain a GeometricValidatorResult");
			}
		};
	}

	private ValidatorReport executeValidator(GeometricValidator geomVal, SemanticValidator semVal,
			SyntacticValidator synVal, List<SemanticProfileValidator> profileValidators, ValidationSettings settings)
			throws IOException, ValidatorException, ReportGenerationException {
		XPlanValidator validator = new XPlanValidator(geomVal, synVal, semVal, profileValidators,
				mock(ReportArchiveGenerator.class), new PlanInfoParser(Optional.empty()), validationProcessListener);
		return validator.validate(settings, planToValidate, "archiveName");
	}

	private ValidatorReport executeValidator(GeometricValidator geomVal, SemanticValidator semVal,
			SyntacticValidator synVal, ValidationSettings settings)
			throws IOException, ValidatorException, ReportGenerationException {
		return executeValidator(geomVal, semVal, synVal, Collections.emptyList(), settings);
	}

	private SemanticProfileValidator mockSemanticProfileValidator(String profileId) {
		SemanticProfileValidator mock = mock(SemanticProfileValidator.class);
		SemanticValidatorResult toBeReturned = new SemanticValidatorResult();
		toBeReturned.addRule("name", "message", Collections.emptyList());
		doReturn(toBeReturned).when(mock).validateSemantic(semanticValidableXPlanArchive(), list());
		doReturn(profileId).when(mock).getId();
		return mock;
	}

	private SemanticValidator mockSemanticValidator() {
		SemanticValidator mock = mock(SemanticValidator.class);
		SemanticValidatorResult toBeReturned = new SemanticValidatorResult();
		toBeReturned.addRule("name", "message", Collections.emptyList());
		doReturn(toBeReturned).when(mock).validateSemantic(semanticValidableXPlanArchive(), list());
		return mock;
	}

	private SemanticValidator mockSemanticValidatorWithError() {
		SemanticValidator mock = mock(SemanticValidator.class);
		SemanticValidatorResult toBeReturned = new SemanticValidatorResult();
		toBeReturned.addRule("name", "message", Collections.singletonList(new InvalidFeaturesResult("error")));
		doReturn(toBeReturned).when(mock).validateSemantic(semanticValidableXPlanArchive(), list());
		return mock;
	}

	private SyntacticValidator mockSyntacticValidator() {
		SyntacticValidator mock = mock(SyntacticValidator.class);
		SyntacticValidatorResult toBeReturned = new SyntacticValidatorResult(Collections.emptyList(), null);
		doReturn(toBeReturned).when(mock).validateSyntax(archive());
		return mock;
	}

	private SyntacticValidator mockSyntacticValidatorWithError() {
		SyntacticValidator mock = mock(SyntacticValidator.class);
		SyntacticValidatorResult toBeReturned = new SyntacticValidatorResult(Collections.singletonList("message"),
				null);
		doReturn(toBeReturned).when(mock).validateSyntax(archive());
		return mock;
	}

	private GeometricValidator mockGeometricValidator() {
		GeometricValidator geomVal = spy(new GeometricValidatorImpl());
		Map<String, GeometricValidatorResult> result = new HashMap<>();
		result.put("Blankenese29", new GeometricValidatorResult());
		doReturn(result).when(geomVal).validateGeometry(gmlValidation());
		return geomVal;
	}

	private XPlanGmlParser mockXPlanGmlParser() {
		return spy(XPlanGmlParserBuilder.newBuilder().build());
	}

	private List emptyList() {
		return Collections.emptyList();
	}

	private List list() {
		return any(List.class);
	}

	private SemanticValidableXPlanArchive semanticValidableXPlanArchive() {
		return any(SemanticValidableXPlanArchive.class);
	}

	private XPlanArchive archive() {
		return any(XPlanArchive.class);
	}

	private XPlanGmlValidation gmlValidation() {
		return any(XPlanGmlValidation.class);
	}

}
