/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.planinfo;

import static de.latlon.xplan.commons.XPlanType.BP_Plan;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_52;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_60;
import static de.latlon.xplan.validator.report.reference.ExternalReferenceStatus.AVAILABLE;
import static de.latlon.xplan.validator.report.reference.ExternalReferenceStatus.MISSING;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.manager.web.shared.RasterEvaluationResult;
import de.latlon.xplan.validator.ValidatorException;
import de.latlon.xplan.validator.XPlanGmlValidation;
import de.latlon.xplan.validator.geometric.GeometricValidatorImpl;
import de.latlon.xplan.validator.listener.InactiveValidationProcessListener;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.web.shared.ValidationSettings;
import de.latlon.xplanbox.core.raster.evaluation.XPlanRasterEvaluation;
import de.latlon.xplanbox.core.raster.evaluation.XPlanRasterEvaluator;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class PlanInfoParserTest {

	@Test
	public void test_parseReferencesAndPlanNames() throws IOException, ValidatorException {
		PlanInfoParser planInfoParser = new PlanInfoParser(
				Optional.of(new XPlanRasterEvaluator(new XPlanRasterEvaluation("EPSG:25832"))));
		InputStream planToTest = getClass().getResourceAsStream("/testdata/xplan60/Blankenese29_Test_60.zip");
		XPlanArchive archive = new XPlanArchiveCreator().createXPlanArchiveFromZip("test", planToTest);

		XPlanGmlValidation xPlanGmlValidation = new XPlanGmlValidation(new GeometricValidatorImpl(), planInfoParser,
				archive, new ValidationSettings(), new InactiveValidationProcessListener());
		xPlanGmlValidation.executeGmlValidation("uuid", new ValidatorReport());
		PlanInfoReport planInfoReport = planInfoParser.parsePlanInfo(xPlanGmlValidation);

		assertEquals(1, planInfoReport.getPlanInfos().size());
		PlanInfo planInfo = planInfoReport.getPlanInfos().values().stream().findFirst().get();
		assertEquals("Blankenese29_Testplan_60", planInfo.getName());
		assertEquals(XPLAN_60, planInfo.getVersion());
		assertEquals(BP_Plan, planInfo.getType());

		assertEquals(2, planInfo.getExternalReferenceReport().getReferences().size());
		assertEquals(AVAILABLE, planInfo.getExternalReferenceReport().getReferencesAndStatus().get("Blankenese29.png"));
		assertEquals(AVAILABLE, planInfo.getExternalReferenceReport().getReferencesAndStatus().get("Blankenese29.pgw"));

		assertEquals(1, planInfo.getRasterEvaluationResults().size());
		assertEquals("Blankenese29.png", planInfo.getRasterEvaluationResults().get(0).getRasterName());
		assertFalse(planInfo.getRasterEvaluationResults().get(0).isConfiguredCrs());
		assertTrue(planInfo.getRasterEvaluationResults().get(0).isSupportedImageFormat());
		assertTrue(planInfo.getRasterEvaluationResults().get(0).isRasterFileAvailable());
	}

	@Test
	public void test_parseReferencesAndPlanNames_multiplePlans() throws IOException, ValidatorException {
		PlanInfoParser planInfoParser = new PlanInfoParser(
				Optional.of(new XPlanRasterEvaluator(new XPlanRasterEvaluation("EPSG:25832"))));
		InputStream planToTest = getClass().getResourceAsStream("xplan-multipleInstances.gml");
		XPlanArchive archive = new XPlanArchiveCreator().createXPlanArchiveFromGml("test", planToTest);

		XPlanGmlValidation xPlanGmlValidation = new XPlanGmlValidation(new GeometricValidatorImpl(), planInfoParser,
				archive, new ValidationSettings(), new InactiveValidationProcessListener());
		xPlanGmlValidation.executeGmlValidation("uuid", new ValidatorReport());
		PlanInfoReport planInfoReport = planInfoParser.parsePlanInfo(xPlanGmlValidation);

		assertEquals(planInfoReport.getPlanInfos().size(), 3);

		Optional<PlanInfo> planInfo1 = planInfoByName(planInfoReport, "Langenhorn51(1Aend)");
		assertTrue(planInfo1.isPresent());
		assertEquals(XPLAN_52, planInfo1.get().getVersion());
		assertEquals(BP_Plan, planInfo1.get().getType());
		assertEquals(planInfo1.get().getExternalReferenceReport().getReferences().size(), 1);
		assertEquals(
				planInfo1.get().getExternalReferenceReport().getReferencesAndStatus().get("Langenhorn51(1Aend).pdf"),
				MISSING);
		assertEquals(0, planInfo1.get().getRasterEvaluationResults().size());

		Optional<PlanInfo> planInfo2 = planInfoByName(planInfoReport, "Langenhorn51(2Aend)");
		assertTrue(planInfo2.isPresent());
		assertEquals(XPLAN_52, planInfo2.get().getVersion());
		assertEquals(BP_Plan, planInfo2.get().getType());
		assertEquals(1, planInfo2.get().getExternalReferenceReport().getReferences().size());
		assertEquals(
				planInfo2.get().getExternalReferenceReport().getReferencesAndStatus().get("Langenhorn51(2Aend).pdf"),
				MISSING);
		assertEquals(0, planInfo2.get().getRasterEvaluationResults().size());

		Optional<PlanInfo> planInfo3 = planInfoByName(planInfoReport, "Langenhorn51");
		assertTrue(planInfo3.isPresent());
		assertEquals(XPLAN_52, planInfo3.get().getVersion());
		assertEquals(BP_Plan, planInfo3.get().getType());
		assertEquals(4, planInfo3.get().getExternalReferenceReport().getReferences().size());
		assertEquals(
				planInfo3.get().getExternalReferenceReport().getReferencesAndStatus().get("Langenhorn51Blatt1.png"),
				MISSING);
		assertEquals(
				planInfo3.get().getExternalReferenceReport().getReferencesAndStatus().get("Langenhorn51Blatt1.pgw"),
				MISSING);
		assertEquals(
				planInfo3.get().getExternalReferenceReport().getReferencesAndStatus().get("Langenhorn51Blatt2.png"),
				MISSING);
		assertEquals(
				planInfo3.get().getExternalReferenceReport().getReferencesAndStatus().get("Langenhorn51Blatt2.pgw"),
				MISSING);
		assertEquals(planInfo3.get().getRasterEvaluationResults().size(), 2);

		Optional<RasterEvaluationResult> rasterEvaluationResult1 = rasterEvaluationResultByName(planInfo3.get(),
				"Langenhorn51Blatt1.png");
		assertTrue(rasterEvaluationResult1.isPresent());
		assertFalse(rasterEvaluationResult1.get().isRasterFileAvailable());

		Optional<RasterEvaluationResult> rasterEvaluationResult2 = rasterEvaluationResultByName(planInfo3.get(),
				"Langenhorn51Blatt2.png");
		assertTrue(rasterEvaluationResult2.isPresent());
		assertFalse(rasterEvaluationResult2.get().isRasterFileAvailable());
	}

	private static Optional<PlanInfo> planInfoByName(PlanInfoReport planInfoReport, String planInfoName) {
		return planInfoReport.getPlanInfos()
			.values()
			.stream()
			.filter(pi -> planInfoName.equals(pi.getName()))
			.findFirst();
	}

	private static Optional<RasterEvaluationResult> rasterEvaluationResultByName(PlanInfo planInfo3,
			String rasterName) {
		return planInfo3.getRasterEvaluationResults()
			.stream()
			.filter(rasterEvaluationResult -> rasterName.equals(rasterEvaluationResult.getRasterName()))
			.findFirst();
	}

}
