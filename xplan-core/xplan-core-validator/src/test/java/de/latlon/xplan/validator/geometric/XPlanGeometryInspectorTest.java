/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.geometric;

import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.ERROR;
import static org.deegree.gml.GMLVersion.GML_32;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.net.URL;
import java.util.Collections;
import java.util.List;

import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.commons.feature.XPlanGmlParser;
import de.latlon.xplan.commons.feature.XPlanGmlParserBuilder;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFinding;
import org.deegree.geometry.Geometry;
import org.deegree.geometry.primitive.Point;
import org.deegree.geometry.primitive.Ring;
import org.deegree.geometry.primitive.patches.PolygonPatch;
import org.deegree.gml.GMLInputFactory;
import org.junit.jupiter.api.Test;

/**
 * Tests for <link>XPlanGeometryInspector</link>.
 *
 * @author <a href="mailto:erben@lat-lon.de">Alexander Erben</a>
 * @version $Revision: $, $Date: $
 */
public class XPlanGeometryInspectorTest {

	@Test
	public void testInspect_PolygonWithInteriorRing() throws Exception {
		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(readGeometry("polygonWithInteriorRing.gml"));

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(0));
	}

	@Test
	public void testInspect_PolygonWithInteriorRing_touching() throws Exception {
		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(readGeometry("polygonWithInteriorRing-touching.gml"));

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(0));
	}

	@Test
	public void testInspect_PolygonWithInteriorRings_touching() throws Exception {
		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(readGeometry("polygonWithInteriorRings-touching.gml"));

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(0));
	}

	@Test
	public void testInspect_Ring_ShouldTestSelfIntersection() throws Exception {
		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(readGeometry("curve.gml"));
		verify(inspector, times(1)).checkSelfIntersection(any(Ring.class));
	}

	@Test
	public void testInspect_PolygonPatch_ShouldTestSelfIntersectionAndOrientation() throws Exception {
		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(readGeometry("surface.gml"));
		verify(inspector, times(1)).checkSelfIntersection(any(PolygonPatch.class));
		verify(inspector, times(1)).checkRingOrientations(any(PolygonPatch.class));
	}

	@Test
	public void testInspect_RingWithSelfIntersection() throws Exception {
		Geometry geometryToInspect = readGeometry("selfIntersectingRing.gml");

		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(geometryToInspect);

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(1));

		assertThat(findings.get(0).getMarkerGeom().getId(), is("GML_ID_67697_selbstueberschneidung"));
		assertThat(findings.get(0).getMessage(),
				is("Selbstüberschneidung an folgenden Punkten: (3566129.076,5934646.032),(3566116.413,5934626.717)."));
	}

	@Test
	public void testInspect_RingWithSelfIntersectionAtTheSamePoint() throws Exception {
		Geometry geometryToInspect = readGeometry("selfIntersectingRing-samePoint.gml");

		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(geometryToInspect);

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(findings.get(0).getMarkerGeom().getId(), is("GML_ID_67697_identischerStuetzpunkt_1"));
		assertThat(findings.get(0).getMessage(), is("Identische Stützpunkte: (3566082.863,5934589.344)."));
	}

	@Test
	public void testInspect_RingWithDuplicateControlPoint() throws Exception {
		Geometry geometryToInspect = readGeometry("duplicateControlPointRing.gml");

		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(geometryToInspect);

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(1));

		assertThat(findings.get(0).getMarkerGeom().getId(), is("GML_doppelterStuetzpunkt_identischerStuetzpunkt_1"));
		assertThat(findings.get(0).getMessage(), is("Identische Stützpunkte: (557804.291,5945798.048)."));
	}

	@Test
	public void testInspect_RingWithTwoSelfIntersections() throws Exception {
		Geometry geometryToInspect = readGeometry("selfIntersectingRIng-2intersections.gml");
		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(geometryToInspect);

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(1));

		assertThat(findings.get(0).getMarkerGeom().getId(),
				is("Gml_8AB9C0E6-69DB-4855-A32C-CD9BBC95ABED_selbstueberschneidung"));
		assertThat(findings.get(0).getMessage(), is(
				"Selbstüberschneidung an folgenden Punkten: (583192.1906790873,5920635.179921611),(583028.4653110565,3581555.962447329)."));
	}

	@Test
	public void testInspect_MultiSurface_shouldBeValid() throws Exception {
		Geometry geometryToInspect = readGeometry("multiSurface.gml");
		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(geometryToInspect);

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(0));
	}

	@Test
	public void testInspect_MultiSurfaceTouches_shouldBeValid() throws Exception {
		Geometry geometryToInspect = readGeometry("multiSurface-touches.gml");
		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(geometryToInspect);

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(0));
	}

	@Test
	public void testInspect_MultiSurfaceIntersection_shouldBeInvalid() throws Exception {
		Geometry geometryToInspect = readGeometry("multiSurface-intersection.gml");
		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(geometryToInspect);

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(1));

		assertThat(findings.get(0).getMarkerGeom().getId(),
				is("GML_48d90d78-aa4a-44cc-939b-3562757993c6_intersection_1"));
		assertThat(findings.get(0).getMessage(), is(
				"Selbstüberschneidung zwischen Polygonen des MultiPolygonen GML_48d90d78-aa4a-44cc-939b-3562757993c6 an folgenden Punkten: (73417.68361133069,69717.162), (73424.792,69725.5733103175)"));
	}

	@Test
	public void testInspect_MultiSurfaceIntersectionInSelfIntersection() throws Exception {
		Geometry geometryToInspect = readGeometry("multiSurface-intersectionInIntersection.gml");
		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(geometryToInspect);

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(2));
	}

	@Test
	public void testInspect_MultiSurfaceCoveringGeometries() throws Exception {
		Geometry geometryToInspect = readGeometry("multiSurface-covering.gml");
		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(geometryToInspect);

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(1));
	}

	@Test
	public void testInspect_InvalidOrientation() throws Exception {
		Geometry geometryToInspect = readGeometry("polygon-orientation-invalid.gml");
		XPlanGeometryInspector inspector = createInspectorWithMockedStream();
		inspector.inspect(geometryToInspect);

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(2));
	}

	@Test
	public void testInspect_InvalidOrientation_skipOrientation() throws Exception {
		Geometry geometryToInspect = readGeometry("polygon-orientation-invalid.gml");
		XPlanGeometryInspector inspector = createInspectorWithMockedStream(true);
		inspector.inspect(geometryToInspect);

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(0));
	}

	@Test
	public void testInspect_CompositeCurve() throws Exception {
		Geometry geometryToInspect = readGeometry("compositecurve.gml");
		XPlanGeometryInspector inspector = createInspectorWithMockedStream(true);
		inspector.inspect(geometryToInspect);

		List<GeometricValidationFinding> findings = inspector.getFindings();
		assertThat(findings.size(), is(0));
	}

	@Test
	public void testInspect_GmlId() throws Exception {
		XPlanArchiveCreator xPlanArchiveCreator = new XPlanArchiveCreator();
		XPlanArchive archive = xPlanArchiveCreator.createXPlanArchiveFromGml("test",
				getClass().getResourceAsStream("xplan51-identische_Stuetzpunkte.gml"));
		XPlanGeometryInspector geometryInspector = new XPlanGeometryInspector(true);
		XPlanGmlParser parser = XPlanGmlParserBuilder.newBuilder()
			.withSkipResolveReferences(true)
			.withSkipBrokenGeometries(true)
			.withGeometryInspectors(geometryInspector)
			.withFeatureInspectors(Collections.singletonList(geometryInspector))
			.build();
		parser.parseXPlanFeatureCollectionAllowMultipleInstances(archive);

		List<GeometricValidationFinding> findings = geometryInspector.getFindings();
		assertThat(findings.size(), is(2));

		GeometricValidationFinding finding0 = findings.get(1);
		assertThat(finding0.getLevel(), is(ERROR));
		assertThat(finding0.getGmlIds(), hasItem("Gml_95467D92-7AA9-4DC8-AE26-48A4BA76EEB6"));
		assertThat(finding0.getMarkerGeom(), instanceOf(Point.class));
		assertThat(((Point) finding0.getMarkerGeom()).get0(), is(574919.6138));
		assertThat(((Point) finding0.getMarkerGeom()).get1(), is(5947411.8223));

		GeometricValidationFinding finding1 = findings.get(0);
		assertThat(finding1.getLevel(), is(ERROR));
		assertThat(finding1.getGmlIds(), hasItem("Gml_D8287F23-3657-48D3-928D-91B547728D07"));
		assertThat(finding1.getMarkerGeom(), instanceOf(Point.class));
		assertThat(((Point) finding1.getMarkerGeom()).get0(), is(574370.2892));
		assertThat(((Point) finding1.getMarkerGeom()).get1(), is(5947113.1631));

	}

	private Geometry readGeometry(String geometryFile) throws Exception {
		URL url = XPlanGeometryInspectorTest.class.getResource(geometryFile);
		return GMLInputFactory.createGMLStreamReader(GML_32, url).readGeometry();
	}

	private XPlanGeometryInspector createInspectorWithMockedStream() {
		return createInspectorWithMockedStream(false);
	}

	private XPlanGeometryInspector createInspectorWithMockedStream(boolean skipOrientation) {
		return spy(new XPlanGeometryInspector(skipOrientation));
	}

}
