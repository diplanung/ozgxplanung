/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator;

import static de.latlon.xplan.validator.geometric.GeometricValidatorImpl.SKIP_OPTIONS;
import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.ERROR;
import static de.latlon.xplan.validator.web.shared.ValidationType.GEOMETRIC;
import static de.latlon.xplan.validator.web.shared.ValidationType.SEMANTIC;
import static de.latlon.xplan.validator.web.shared.ValidationType.SYNTACTIC;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.validator.geometric.GeometricValidatorImpl;
import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFinding;
import de.latlon.xplan.validator.geometric.result.GeometricValidationRule;
import de.latlon.xplan.validator.listener.InactiveValidationProcessListener;
import de.latlon.xplan.validator.planinfo.PlanInfoParser;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.web.shared.ValidationSettings;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class XPlanGmlValidationTest {

	@Test
	public void testExecuteValidationWithBrokenGeometry() throws Exception {
		XPlanArchive archive = getTestArchive("xplan41/Eidelstedt_4_V4-broken-geometry.zip");

		ValidationSettings validationSettings = new ValidationSettings("test", List.of(SYNTACTIC, SEMANTIC, GEOMETRIC),
				SKIP_OPTIONS);
		XPlanGmlValidation xPlanGmlValidation = new XPlanGmlValidation(new GeometricValidatorImpl(),
				new PlanInfoParser(Optional.empty()), archive, validationSettings,
				new InactiveValidationProcessListener());
		ValidatorReport report = new ValidatorReport();
		xPlanGmlValidation.executeGmlValidation("uuid", report);

		GeometricValidatorResult geometricReport = report.getPlanInfoReport()
			.getPlanInfos()
			.values()
			.stream()
			.findFirst()
			.get()
			.getGeometricValidatorResult();
		int numberOfErrors = geometricReport.getErrors().size();
		int numberOfWarnings = geometricReport.getWarnings().size();

		assertThat(geometricReport.isValid(), is(false));
		assertThat(numberOfErrors, is(1));
		assertThat(numberOfWarnings, is(0));

		List<GeometricValidationRule> geometricValidationRules = geometricReport.getRules();
		assertThat(geometricValidationRules.size(), is(2));

		GeometricValidationFinding finding = geometricValidationRules.get(0).getFindings().get(0);
		assertThat(finding.getLevel(), is(ERROR));
		assertThat(finding.getGmlIds().size(), is(0));
	}

	private XPlanArchive getTestArchive(String name) throws IOException {
		XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();
		return archiveCreator.createXPlanArchiveFromZip(name, getClass().getResourceAsStream("/testdata/" + name));
	}

}
