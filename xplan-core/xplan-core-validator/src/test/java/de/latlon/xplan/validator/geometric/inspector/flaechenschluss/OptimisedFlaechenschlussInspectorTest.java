/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.geometric.inspector.flaechenschluss;

import static de.latlon.xplan.commons.XPlanType.BP_Plan;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_51;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_52;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_53;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_54;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_60;
import static de.latlon.xplan.validator.FeatureParserUtils.readFeaturesFromGml;
import static de.latlon.xplan.validator.FeatureParserUtils.readFeaturesFromZip;
import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.ERROR;
import static de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel.WARNING;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.List;

import de.latlon.xplan.validator.geometric.result.GeometricValidationFinding;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class OptimisedFlaechenschlussInspectorTest {

	@Disabled
	@Test
	public void testCheckFlaechenschluss_executionTime() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_54,
				BP_Plan);
		long start = System.currentTimeMillis();
		readFeaturesFromZip("Testplan.zip", OptimisedFlaechenschlussInspectorTest.class, flaechenschlussInspector);
		flaechenschlussInspector.checkGeometricRule();
		long end = System.currentTimeMillis();
		long timeNeeded = end - start;
		System.out.println("Flaechenschluss with optimized implementation: " + timeNeeded + " [ms]");
	}

	@Test
	public void testCheckFlaechenschluss() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_51,
				BP_Plan);
		readFeaturesFromZip("xplan51/BPlan001_5-1.zip", flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(findings.get(0).getLevel(), is(WARNING));
	}

	@Test
	public void testCheckFlaechenschluss_wirksamkeit() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_51,
				BP_Plan);
		readFeaturesFromGml("V4_1_ID_103_wirksamkeit.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));
	}

	@Test
	public void testCheckFlaechenschluss_invalid() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_51,
				BP_Plan);
		readFeaturesFromGml("V4_1_ID_103_kein-flaechenschluss.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(false));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		GeometricValidationFinding finding = findings.get(0);
		assertThat(finding.getLevel(), is(ERROR));
		assertThat(finding.getGmlIds(), hasItem("FEATURE_ff2cc402-89f7-4a77-86c3-d9f824d9c0a8"));
		assertNotNull(finding.getMarkerGeom());
	}

	@Test
	public void testCheckFlaechenschluss_equalFlaechenschlussGeometries() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_51,
				BP_Plan);
		readFeaturesFromGml("equalFlaechenschlussGeometries.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(false));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(19));
		assertThat(noOfErrors(findings), is(1));
		assertThat(noOfWarnings(findings), is(18));

		GeometricValidationFinding finding = findings.get(0);
		assertThat(finding.getLevel(), is(ERROR));
		assertThat(finding.getGmlIds(), hasItem("FEATURE_059c7335-42f9-4675-9328-93408ee0e134"));
		assertNotNull(finding.getMarkerGeom());
	}

	@Test
	public void testCheckFlaechenschluss_vollstaendigeUeberlappungFlaechenschlussGeometries() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_51,
				BP_Plan);
		readFeaturesFromGml("xplan52_Flaechenschlussfehler_vollstaendige_Ueberlappung.gml",
				OptimisedFlaechenschlussInspectorTest.class, flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(false));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(noOfErrors(findings), is(1));

		GeometricValidationFinding finding = findings.get(0);
		assertThat(finding.getLevel(), is(ERROR));
		assertThat(finding.getGmlIds(),
				hasItems("GML_349d357a-cb9a-4a26-a946-908647eb1bad", "GML_a7a48503-d117-42b7-914f-0a20c862a54e"));
		assertNotNull(finding.getMarkerGeom());
	}

	@Test
	public void testCheckFlaechenschluss_LueckeGeltungsbereich() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_52,
				BP_Plan);
		readFeaturesFromGml("xplan52_Flaechenschlussfehler_Luecke_Geltungsbereich.gml",
				OptimisedFlaechenschlussInspectorTest.class, flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(4));
		assertThat(noOfWarnings(findings), is(4));
	}

	@Test
	public void testCheckFlaechenschluss_Luecken() throws Exception {
		// same plan as xplan52_Flaechenschlussfehler_Luecke_Geltungsbereich.gml but
		// XPlanGML 5.4
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_54,
				BP_Plan);
		readFeaturesFromGml("xplan54_Flaechenschlussfehler_Luecken.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(4));
		assertThat(noOfWarnings(findings), is(4));
	}

	@Test
	public void testCheckFlaechenschluss_TestLuecke() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_52,
				BP_Plan);
		readFeaturesFromGml("Test_Luecke.gml", OptimisedFlaechenschlussInspectorTest.class, flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(noOfWarnings(findings), is(1));
	}

	@Test
	public void testCheckFlaechenschluss_TestInsel() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_52,
				BP_Plan);
		readFeaturesFromGml("FNP_Insel-Test.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(0));
	}

	@Test
	public void testCheckFlaechenschluss_TestAenderungsplanWithLuecke() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_52,
				BP_Plan);
		readFeaturesFromGml("xplan60_Aenderungsplan_Luecke.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(noOfWarnings(findings), is(1));

		GeometricValidationFinding finding = findings.get(0);
		assertThat(finding.getLevel(), is(WARNING));
		assertThat(finding.getGmlIds(), hasItem("GML_1dd3969f-1703-4c63-ab27-88b2bb10e090"));
		assertNotNull(finding.getMarkerGeom());
	}

	@Test
	public void testCheckFlaechenschluss_TestAenderungsplanValid() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_52,
				BP_Plan);
		readFeaturesFromGml("xplan60_Aenderungsplan_valide.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(0));
	}

	@Test
	public void testCheckFlaechenschluss_Toleranz() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_52,
				BP_Plan);
		readFeaturesFromGml("Flaechenschluss-Test_1mm.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(0));
	}

	@Test
	public void testCheckFlaechenschluss_Toleranz_nichtUeberlappend() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_52,
				BP_Plan);
		readFeaturesFromGml("Flaechenschluss-Test_1mm-nichtUeberlappend.gml",
				OptimisedFlaechenschlussInspectorTest.class, flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(0));
	}

	@Test
	public void testCheckFlaechenschluss_Luecke_Ebene1() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_51,
				BP_Plan);
		readFeaturesFromGml("xplan51-5_1_4_1_Flaechenschlussfehler.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(noOfWarnings(findings), is(1));
		List<String> warnings = findings.stream()
			.filter(f -> WARNING.equals(f.getLevel()))
			.map(GeometricValidationFinding::getMessage)
			.toList();
		assertThat(warnings, hasItem(
				"Das Flächenschlussobjekt mit der gml id Gml_3E40CD06-7F9E-4301-B244-9C93B5B9F81C, Gml_4C6F208C-300A-4DC5-91AA-ABF379F51244, Gml_702A30AF-09E5-4FDD-A2D7-4924C20ED521, Gml_E776F534-00C5-4A18-8B52-CE999F89DD2A erfüllt die Flächenschlussbedingung hinsichtlich folgender Bedingungen nicht: Überdeckung des Geltungsbereichs des Bereichs, Überdeckung des Geltungsbereichs des Plans. Bitte prüfen Sie, ob es sich um eine gewollte Lücke handelt: POLYGON ((576676.293500 5954490.416900,576956.752400 5954069.728500,576835.043800 5954053.853500,576441.057500 5953961.902400,576397.418700 5954130.402900,576676.293500 5954490.416900))"));
		GeometricValidationFinding finding = findings.get(0);
		assertThat(finding.getGmlIds(),
				hasItems("Gml_3E40CD06-7F9E-4301-B244-9C93B5B9F81C", "Gml_4C6F208C-300A-4DC5-91AA-ABF379F51244",
						"Gml_702A30AF-09E5-4FDD-A2D7-4924C20ED521", "Gml_E776F534-00C5-4A18-8B52-CE999F89DD2A"));
		assertNotNull(finding.getMarkerGeom());
	}

	@Test
	public void testCheckFlaechenschluss_Luecke_Missing() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_51,
				BP_Plan);
		readFeaturesFromGml("xplan51-5_1_4_2_Flaechenschlussfehler.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(noOfWarnings(findings), is(1));

		List<String> warnings = findings.stream()
			.filter(f -> WARNING.equals(f.getLevel()))
			.map(GeometricValidationFinding::getMessage)
			.toList();
		assertThat(warnings, hasItem(
				"Das Flächenschlussobjekt mit der gml id Gml_3E40CD06-7F9E-4301-B244-9C93B5B9F81C, Gml_4C6F208C-300A-4DC5-91AA-ABF379F51244, Gml_702A30AF-09E5-4FDD-A2D7-4924C20ED521, Gml_E776F534-00C5-4A18-8B52-CE999F89DD2A erfüllt die Flächenschlussbedingung hinsichtlich folgender Bedingungen nicht: Überdeckung des Geltungsbereichs des Bereichs, Überdeckung des Geltungsbereichs des Plans. Bitte prüfen Sie, ob es sich um eine gewollte Lücke handelt: POLYGON ((576676.293500 5954490.416900,576956.752400 5954069.728500,576835.043800 5954053.853500,576441.057500 5953961.902400,576397.418700 5954130.402900,576676.293500 5954490.416900))"));
		GeometricValidationFinding finding = findings.get(0);
		assertThat(finding.getGmlIds(),
				hasItems("Gml_3E40CD06-7F9E-4301-B244-9C93B5B9F81C", "Gml_4C6F208C-300A-4DC5-91AA-ABF379F51244",
						"Gml_702A30AF-09E5-4FDD-A2D7-4924C20ED521", "Gml_E776F534-00C5-4A18-8B52-CE999F89DD2A"));
		assertNotNull(finding.getMarkerGeom());
	}

	@Test
	public void testCheckFlaechenschluss_Luecke_FlaechenschlussFalse() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_51,
				BP_Plan);
		readFeaturesFromGml("xplan51-5_5_4_2_Flaechenschlussfehler.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(noOfWarnings(findings), is(1));

		List<String> warnings = findings.stream()
			.filter(f -> WARNING.equals(f.getLevel()))
			.map(GeometricValidationFinding::getMessage)
			.toList();
		assertThat(warnings, hasItem(
				"Das Flächenschlussobjekt mit der gml id Gml_3E40CD06-7F9E-4301-B244-9C93B5B9F81C, Gml_4C6F208C-300A-4DC5-91AA-ABF379F51244, Gml_702A30AF-09E5-4FDD-A2D7-4924C20ED521, Gml_E776F534-00C5-4A18-8B52-CE999F89DD2A erfüllt die Flächenschlussbedingung hinsichtlich folgender Bedingungen nicht: Überdeckung des Geltungsbereichs des Bereichs, Überdeckung des Geltungsbereichs des Plans. Bitte prüfen Sie, ob es sich um eine gewollte Lücke handelt: POLYGON ((576676.293500 5954490.416900,576956.752400 5954069.728500,576835.043800 5954053.853500,576441.057500 5953961.902400,576397.418700 5954130.402900,576676.293500 5954490.416900))"));
		GeometricValidationFinding finding = findings.get(0);
		assertThat(finding.getGmlIds(),
				hasItems("Gml_3E40CD06-7F9E-4301-B244-9C93B5B9F81C", "Gml_4C6F208C-300A-4DC5-91AA-ABF379F51244",
						"Gml_702A30AF-09E5-4FDD-A2D7-4924C20ED521", "Gml_E776F534-00C5-4A18-8B52-CE999F89DD2A"));
		assertNotNull(finding.getMarkerGeom());
	}

	@Test
	public void testCheckFlaechenschluss_Luecke_RandGeltungsbereich() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_53,
				BP_Plan);
		readFeaturesFromGml("xplan53-4_1_7_1_Flaechenschlussfehler.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(noOfWarnings(findings), is(1));

		GeometricValidationFinding finding = findings.get(0);
		assertThat(finding.getLevel(), is(WARNING));
		assertThat(finding.getGmlIds().size(), is(0));
		assertNotNull(finding.getMarkerGeom());
	}

	@Test
	public void testCheckFlaechenschluss_Luecke_LochGeltungsbereichImToleranzbereich() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_54,
				BP_Plan);
		readFeaturesFromGml("BPlan001_5-4_Toleranz_Geltungsbereich_0008.gml",
				OptimisedFlaechenschlussInspectorTest.class, flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(0));
	}

	@Test
	public void testCheckFlaechenschluss_missing_gehoertZuBereich() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_54,
				BP_Plan);
		readFeaturesFromGml("test41-54_1-missing-gehoertZuBereich.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(2));
		assertThat(noOfErrors(findings), is(0));
		assertThat(noOfWarnings(findings), is(2));
	}

	@Test
	public void testCheckFlaechenschluss_missing_gehoertZuBereich_multipleBereiche() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_54,
				BP_Plan);
		readFeaturesFromGml("test41-54_1-missing-gehoertZuBereich-multipleBereiche.gml",
				OptimisedFlaechenschlussInspectorTest.class, flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(false));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(noOfErrors(findings), is(1));

		GeometricValidationFinding finding = findings.get(0);
		assertThat(finding.getLevel(), is(ERROR));
		assertThat(finding.getGmlIds(), hasItem("GML_303e9eb8-189e-485e-b658-913d8dd568f6"));
		assertNull(finding.getMarkerGeom());
	}

	@Test
	public void testCheckFlaechenschluss_OneFlaechenschlussFeature_Luecke() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_54,
				BP_Plan);
		readFeaturesFromGml("Test_Insel_Luecke_5mm.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(noOfWarnings(findings), is(1));

		GeometricValidationFinding finding = findings.get(0);
		assertThat(finding.getLevel(), is(WARNING));
		assertThat(finding.getGmlIds(), hasItem("GML_48d4501f-e4f7-479c-a1d9-b8ed2471335b"));
		assertNotNull(finding.getMarkerGeom());
	}

	@Test
	public void testCheckFlaechenschluss_TeilbereichAusserhalbPlanbereich() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_60,
				BP_Plan);
		readFeaturesFromGml("BPlan_TeilbereichAusserhalbPlanbereich_6-0-2.gml",
				OptimisedFlaechenschlussInspectorTest.class, flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(false));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(noOfErrors(findings), is(1));

		GeometricValidationFinding finding = findings.get(0);
		assertThat(finding.getLevel(), is(ERROR));
		assertThat(finding.getGmlIds().size(), is(5));
		assertNotNull(finding.getMarkerGeom());
	}

	@Test
	public void testCheckFlaechenschluss_Kompensationsbereich() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_60,
				BP_Plan);
		readFeaturesFromGml("BPlan_Kompensationsbereich_6-0-2.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));
	}

	@Test
	public void testCheckFlaechenschluss_Kompensationsbereich_Luecke() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_60,
				BP_Plan);
		readFeaturesFromGml("BPlan_Kompensationsbereich_6-0-2_Luecke.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(false));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(noOfErrors(findings), is(1));

		GeometricValidationFinding finding = findings.get(0);
		assertThat(finding.getLevel(), is(ERROR));
		assertThat(finding.getGmlIds(), hasItem("Gml_303B15E7-1CC4-4829-A81F-5CAA4E7D1E74"));
		assertNotNull(finding.getMarkerGeom());
	}

	@Test
	public void testCheckFlaechenschluss_Kompensationsbereich_Ueberlappung() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_60,
				BP_Plan);
		readFeaturesFromGml("BPlan_Kompensationsbereich_6-0-2_Ueberlappung.gml",
				OptimisedFlaechenschlussInspectorTest.class, flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(false));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(noOfErrors(findings), is(1));

		GeometricValidationFinding finding = findings.get(0);
		assertThat(finding.getLevel(), is(ERROR));
		assertThat(finding.getGmlIds(),
				hasItems("Gml_303B15E7-1CC4-4829-A81F-5CAA4E7D1E74", "Gml_E64D3EEE-ACBC-466E-BDCB-088B7E34A934"));
		assertNotNull(finding.getMarkerGeom());
	}

	@Test
	public void testCheckFlaechenschluss_Wirksamkeit_valide() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_60,
				BP_Plan);
		readFeaturesFromGml("BPlan_Wirksamkeit_6-0-2_valide.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));
	}

	@Test
	public void testCheckFlaechenschluss_Wirksamkeit_invalide() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_60,
				BP_Plan);
		readFeaturesFromGml("BPlan_Wirksamkeit_6-0-2_invalide.gml", OptimisedFlaechenschlussInspectorTest.class,
				flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(false));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(1));
		assertThat(noOfErrors(findings), is(1));

		GeometricValidationFinding finding = findings.get(0);
		assertThat(finding.getLevel(), is(ERROR));
		assertThat(finding.getGmlIds(),
				hasItems("Gml_C01842C9-4E58-4E47-B46A-E07B43242BAA", "Gml_D3E78336-D47F-412E-A05D-F06AD94EA261"));
		assertNotNull(finding.getMarkerGeom());
	}

	@Test
	public void testCheckFlaechenschluss_MultipleBereicheAndPlan_OneBereichWithoutGeom() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_54,
				BP_Plan);
		readFeaturesFromGml("BPlan_5_4-MultipleBereicheAndPlan_OneBereichWithoutGeom.gml",
				OptimisedFlaechenschlussInspectorTest.class, flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));

		List<GeometricValidationFinding> findings = flaechenschlussInspector.getFindings();
		assertThat(findings.size(), is(0));
	}

	@Test
	public void testCheckFlaechenschluss_PolygonPatch_doppelt_nicht_zusammenhaengend() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_60,
				BP_Plan);
		readFeaturesFromGml("../../patches/BP_6-0_false_2.2.2.1_PolygonPatch_doppelt_nicht_zusammenhaengend.gml",
				OptimisedFlaechenschlussInspectorTest.class, flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));
	}

	@Disabled
	@Test
	public void testCheckFlaechenschluss_PolygonPatch_nicht_zusammenhaengend_Flaechenschlussobjekt() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_60,
				BP_Plan);
		readFeaturesFromGml(
				"../../patches/BP_6-0_false_2.2.2.1_PolygonPatch_nicht_zusammenhaengend_Flaechenschlussobjekt.gml",
				OptimisedFlaechenschlussInspectorTest.class, flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));
	}

	@Test
	public void testCheckFlaechenschluss_PolygonPatch_Ueberlappung() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_60,
				BP_Plan);
		readFeaturesFromGml("../../patches/BP_6-0_false_2.2.2.1_PolygonPatch_Ueberlappung.gml",
				OptimisedFlaechenschlussInspectorTest.class, flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));
	}

	@Disabled
	@Test
	public void testCheckFlaechenschluss_PolygonPatch_Flaechenschlussobjekt() throws Exception {
		OptimisedFlaechenschlussInspector flaechenschlussInspector = new OptimisedFlaechenschlussInspector(XPLAN_60,
				BP_Plan);
		readFeaturesFromGml("../../patches/BP_6-0_true_PolygonPatch_Flaechenschlussobjekt.gml",
				OptimisedFlaechenschlussInspectorTest.class, flaechenschlussInspector);

		boolean isValid = flaechenschlussInspector.checkGeometricRule();
		assertThat(isValid, is(true));
	}

	private int noOfErrors(List<GeometricValidationFinding> findings) {
		return (int) findings.stream().filter(f -> ERROR.equals(f.getLevel())).count();
	}

	private int noOfWarnings(List<GeometricValidationFinding> findings) {
		return (int) findings.stream().filter(f -> WARNING.equals(f.getLevel())).count();
	}

}
