/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.report.geojson;

import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.GEOMETRYCOLLECTION;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.LINESTRING;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.MULTILINESTRING;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.MULTIPOINT;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.MULTIPOLYGON;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.POINT;
import static de.latlon.xplan.validator.report.geojson.model.GeoJsonObject.TypeEnum.POLYGON;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Collections;
import java.util.List;

import de.latlon.xplan.validator.report.geojson.model.Geometry;
import de.latlon.xplan.validator.report.geojson.model.GeometryCollection;
import de.latlon.xplan.validator.report.geojson.model.LineString;
import de.latlon.xplan.validator.report.geojson.model.MultiLineString;
import de.latlon.xplan.validator.report.geojson.model.MultiPoint;
import de.latlon.xplan.validator.report.geojson.model.MultiPolygon;
import de.latlon.xplan.validator.report.geojson.model.Point;
import de.latlon.xplan.validator.report.geojson.model.Polygon;
import org.deegree.cs.coordinatesystems.ICRS;
import org.deegree.cs.exceptions.UnknownCRSException;
import org.deegree.cs.persistence.CRSManager;
import org.deegree.geometry.GeometryFactory;
import org.deegree.geometry.composite.CompositeGeometry;
import org.deegree.geometry.multi.MultiGeometry;
import org.deegree.geometry.points.Points;
import org.deegree.geometry.primitive.GeometricPrimitive;
import org.deegree.geometry.primitive.Ring;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class GeoJsonGeometryBuilderTest {

	private final GeometryFactory geometryFactory = new GeometryFactory();

	@Test
	public void test_Point() throws UnknownCRSException {
		org.deegree.geometry.primitive.Point point = createPoint(5.4, 10.8);

		Geometry geometry = GeoJsonGeometryBuilder.createGeometry(point);
		assertThat(geometry, instanceOf(Point.class));
		assertThat(geometry.getType(), is(POINT));
		assertThat(((Point) geometry).getPosition().get(0).doubleValue(), is(point.get0()));
		assertThat(((Point) geometry).getPosition().get(1).doubleValue(), is(point.get1()));
	}

	@Test
	public void test_MultiPoint() throws UnknownCRSException {
		org.deegree.geometry.primitive.Point point1 = createPoint(5.4, 10.8);
		org.deegree.geometry.primitive.Point point2 = createPoint(6.4, 10.5);
		org.deegree.geometry.multi.MultiPoint multiPoint = geometryFactory.createMultiPoint("id", crs(),
				List.of(point1, point2));

		Geometry geometry = GeoJsonGeometryBuilder.createGeometry(multiPoint);
		assertThat(geometry, instanceOf(MultiPoint.class));
		assertThat(geometry.getType(), is(MULTIPOINT));
		assertThat(((MultiPoint) geometry).getPositions().size(), is(2));
		assertThat(((MultiPoint) geometry).getPositions().get(0).get(0).doubleValue(), is(point1.get0()));
		assertThat(((MultiPoint) geometry).getPositions().get(0).get(1).doubleValue(), is(point1.get1()));
	}

	@Test
	public void test_LineString() throws UnknownCRSException {
		org.deegree.geometry.primitive.LineString lineString = createLineString1();

		Geometry geometry = GeoJsonGeometryBuilder.createGeometry(lineString);
		assertThat(geometry, instanceOf(LineString.class));
		assertThat(geometry.getType(), is(LINESTRING));
		assertThat(((LineString) geometry).getCoordinates().size(), is(3));
		assertThat(((LineString) geometry).getCoordinates().get(0).get(0).doubleValue(),
				is(lineString.getControlPoints().get(0).get0()));
		assertThat(((LineString) geometry).getCoordinates().get(0).get(1).doubleValue(),
				is(lineString.getControlPoints().get(0).get1()));
	}

	@Test
	public void test_MultiLineString() throws UnknownCRSException {
		org.deegree.geometry.primitive.LineString lineString1 = createLineString1();
		org.deegree.geometry.primitive.LineString lineString2 = createLineString2();
		org.deegree.geometry.multi.MultiLineString multiLineString = geometryFactory.createMultiLineString("id", crs(),
				List.of(lineString1, lineString2));

		Geometry geometry = GeoJsonGeometryBuilder.createGeometry(multiLineString);
		assertThat(geometry, instanceOf(MultiLineString.class));
		assertThat(geometry.getType(), is(MULTILINESTRING));
		assertThat(((MultiLineString) geometry).getLineStringCoordinates().size(), is(2));
		assertThat(((MultiLineString) geometry).getLineStringCoordinates().get(0).get(0).get(0).doubleValue(),
				is(multiLineString.get(0).getControlPoints().get(0).get0()));
		assertThat(((MultiLineString) geometry).getLineStringCoordinates().get(0).get(0).get(1).doubleValue(),
				is(multiLineString.get(0).getControlPoints().get(0).get1()));
	}

	@Test
	public void test_MultiCurve() throws UnknownCRSException {
		org.deegree.geometry.primitive.LineString lineString1 = createLineString1();
		org.deegree.geometry.primitive.LineString lineString2 = createLineString2();
		org.deegree.geometry.multi.MultiCurve<?> multiCurve = geometryFactory.createMultiCurve("id", crs(),
				List.of(lineString1, lineString2));

		Geometry geometry = GeoJsonGeometryBuilder.createGeometry(multiCurve);
		assertThat(geometry, instanceOf(MultiLineString.class));
		assertThat(geometry.getType(), is(MULTILINESTRING));
		assertThat(((MultiLineString) geometry).getLineStringCoordinates().size(), is(2));
		assertThat(((MultiLineString) geometry).getLineStringCoordinates().get(0).get(0).get(0).doubleValue(),
				is(multiCurve.get(0).getControlPoints().get(0).get0()));
		assertThat(((MultiLineString) geometry).getLineStringCoordinates().get(0).get(0).get(1).doubleValue(),
				is(multiCurve.get(0).getControlPoints().get(0).get1()));
	}

	@Test
	public void test_Polygon() throws UnknownCRSException {
		org.deegree.geometry.primitive.Polygon polygon = createPolygon1();

		Geometry geometry = GeoJsonGeometryBuilder.createGeometry(polygon);
		assertThat(geometry, instanceOf(Polygon.class));
		assertThat(geometry.getType(), is(POLYGON));
		assertThat(((Polygon) geometry).getLinearRings().size(), is(1));
		assertThat(((Polygon) geometry).getLinearRings().get(0).size(), is(4));

		assertThat(((Polygon) geometry).getLinearRings().get(0).get(0).get(0).doubleValue(),
				is(polygon.getExteriorRing().getControlPoints().get(0).get0()));
		assertThat(((Polygon) geometry).getLinearRings().get(0).get(0).get(1).doubleValue(),
				is(polygon.getExteriorRing().getControlPoints().get(0).get1()));
	}

	@Test
	public void test_MultiPolygon() throws UnknownCRSException {
		org.deegree.geometry.primitive.Polygon polygon1 = createPolygon1();
		org.deegree.geometry.primitive.Polygon polygon2 = createPolygon1();
		org.deegree.geometry.multi.MultiPolygon multiPolygon = geometryFactory.createMultiPolygon("id", crs(),
				List.of(polygon1, polygon2));

		Geometry geometry = GeoJsonGeometryBuilder.createGeometry(multiPolygon);
		assertThat(geometry, instanceOf(MultiPolygon.class));
		assertThat(geometry.getType(), is(MULTIPOLYGON));
		assertThat(((MultiPolygon) geometry).getLinearRings().size(), is(2));
		assertThat(((MultiPolygon) geometry).getLinearRings().get(0).size(), is(1));
		assertThat(((MultiPolygon) geometry).getLinearRings().get(0).get(0).size(), is(4));

		assertThat(((MultiPolygon) geometry).getLinearRings().get(0).get(0).get(0).get(0).doubleValue(),
				is(multiPolygon.get(0).getExteriorRing().getControlPoints().get(0).get0()));
		assertThat(((MultiPolygon) geometry).getLinearRings().get(0).get(0).get(0).get(1).doubleValue(),
				is(multiPolygon.get(0).getExteriorRing().getControlPoints().get(0).get1()));
	}

	@Test
	public void test_MultiSurface() throws UnknownCRSException {
		org.deegree.geometry.primitive.Polygon polygon1 = createPolygon1();
		org.deegree.geometry.primitive.Polygon polygon2 = createPolygon1();
		org.deegree.geometry.multi.MultiSurface<?> multiSurface = geometryFactory.createMultiSurface("id", crs(),
				List.of(polygon1, polygon2));

		Geometry geometry = GeoJsonGeometryBuilder.createGeometry(multiSurface);
		assertThat(geometry, instanceOf(MultiPolygon.class));
		assertThat(geometry.getType(), is(MULTIPOLYGON));
		assertThat(((MultiPolygon) geometry).getLinearRings().size(), is(2));
		assertThat(((MultiPolygon) geometry).getLinearRings().get(0).size(), is(1));
		assertThat(((MultiPolygon) geometry).getLinearRings().get(0).get(0).size(), is(4));

		assertThat(((MultiPolygon) geometry).getLinearRings().get(0).get(0).get(0).get(0).doubleValue(),
				is(multiSurface.get(0).getExteriorRingCoordinates().get(0).get0()));
		assertThat(((MultiPolygon) geometry).getLinearRings().get(0).get(0).get(0).get(1).doubleValue(),
				is(multiSurface.get(0).getExteriorRingCoordinates().get(0).get1()));
	}

	@Test
	public void test_MultiGeometry() throws UnknownCRSException {
		org.deegree.geometry.primitive.Point point1 = createPoint(5.4, 10.8);
		org.deegree.geometry.primitive.LineString lineString1 = createLineString1();
		org.deegree.geometry.primitive.Polygon polygon1 = createPolygon1();
		MultiGeometry<org.deegree.geometry.Geometry> multiGeometry = geometryFactory.createMultiGeometry("id", crs(),
				List.of(point1, lineString1, polygon1));

		Geometry geometry = GeoJsonGeometryBuilder.createGeometry(multiGeometry);
		assertThat(geometry, instanceOf(GeometryCollection.class));
		assertThat(geometry.getType(), is(GEOMETRYCOLLECTION));
		assertThat(((GeometryCollection) geometry).getGeometries().size(), is(3));
		assertThat(((GeometryCollection) geometry).getGeometries().get(0), instanceOf(Point.class));
		assertThat(((GeometryCollection) geometry).getGeometries().get(0).getType(), is(POINT));
		assertThat(((GeometryCollection) geometry).getGeometries().get(1), instanceOf(LineString.class));
		assertThat(((GeometryCollection) geometry).getGeometries().get(1).getType(), is(LINESTRING));
		assertThat(((GeometryCollection) geometry).getGeometries().get(2), instanceOf(Polygon.class));
		assertThat(((GeometryCollection) geometry).getGeometries().get(2).getType(), is(POLYGON));

		assertThat(((Point) ((GeometryCollection) geometry).getGeometries().get(0)).getPosition().get(0).doubleValue(),
				is(point1.get0()));
		assertThat(((Point) ((GeometryCollection) geometry).getGeometries().get(0)).getPosition().get(1).doubleValue(),
				is(point1.get1()));
	}

	@Test
	public void test_GeometryCollection() throws UnknownCRSException {
		org.deegree.geometry.primitive.Point point1 = createPoint(5.4, 10.8);
		org.deegree.geometry.primitive.LineString lineString1 = createLineString1();
		org.deegree.geometry.primitive.Polygon polygon1 = createPolygon1();
		CompositeGeometry<GeometricPrimitive> compositeGeometry = geometryFactory.createCompositeGeometry("id", crs(),
				List.of(point1, lineString1, polygon1));

		Geometry geometry = GeoJsonGeometryBuilder.createGeometry(compositeGeometry);
		assertThat(geometry, instanceOf(GeometryCollection.class));
		assertThat(geometry.getType(), is(GEOMETRYCOLLECTION));
		assertThat(((GeometryCollection) geometry).getGeometries().size(), is(3));
		assertThat(((GeometryCollection) geometry).getGeometries().get(0), instanceOf(Point.class));
		assertThat(((GeometryCollection) geometry).getGeometries().get(0).getType(), is(POINT));
		assertThat(((GeometryCollection) geometry).getGeometries().get(1), instanceOf(LineString.class));
		assertThat(((GeometryCollection) geometry).getGeometries().get(1).getType(), is(LINESTRING));
		assertThat(((GeometryCollection) geometry).getGeometries().get(2), instanceOf(Polygon.class));
		assertThat(((GeometryCollection) geometry).getGeometries().get(2).getType(), is(POLYGON));

		assertThat(((Point) ((GeometryCollection) geometry).getGeometries().get(0)).getPosition().get(0).doubleValue(),
				is(point1.get0()));
		assertThat(((Point) ((GeometryCollection) geometry).getGeometries().get(0)).getPosition().get(1).doubleValue(),
				is(point1.get1()));
	}

	private org.deegree.geometry.primitive.LineString createLineString1() throws UnknownCRSException {
		Points points = geometryFactory
			.createPoints(List.of(createPoint(5.7, 10.9), createPoint(5.7, 11.9), createPoint(5.8, 12.9)));
		return geometryFactory.createLineString("id", crs(), points);
	}

	private org.deegree.geometry.primitive.LineString createLineString2() throws UnknownCRSException {
		Points points = geometryFactory
			.createPoints(List.of(createPoint(4.7, 12.9), createPoint(4.7, 13.9), createPoint(4.8, 11.9)));
		return geometryFactory.createLineString("id", crs(), points);
	}

	private org.deegree.geometry.primitive.Polygon createPolygon1() throws UnknownCRSException {
		Points points = geometryFactory.createPoints(List.of(createPoint(5.7, 10.9), createPoint(5.7, 11.9),
				createPoint(5.8, 12.9), createPoint(5.7, 11.9)));
		Ring interior = geometryFactory.createLinearRing("id", crs(), points);
		return geometryFactory.createPolygon("id", crs(), interior, Collections.emptyList());
	}

	private org.deegree.geometry.primitive.Polygon createPolygon2() throws UnknownCRSException {
		Points points = geometryFactory.createPoints(
				List.of(createPoint(3.7, 9.9), createPoint(3.7, 10.9), createPoint(3.8, 11.9), createPoint(3.7, 10.9)));
		Ring interior = geometryFactory.createLinearRing("id", crs(), points);
		return geometryFactory.createPolygon("id", crs(), interior, Collections.emptyList());
	}

	private static ICRS crs() throws UnknownCRSException {
		return CRSManager.lookup("EPSG:4326");
	}

	private org.deegree.geometry.primitive.Point createPoint(double x, double y) throws UnknownCRSException {
		return geometryFactory.createPoint("id", x, y, crs());
	}

}
