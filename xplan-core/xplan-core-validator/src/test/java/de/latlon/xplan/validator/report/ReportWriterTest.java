/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.report;

import static de.latlon.xplan.commons.XPlanType.BP_Plan;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_60;
import static de.latlon.xplan.validator.web.shared.ReportFormatType.HTML;
import static java.util.Collections.singletonMap;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.planinfo.PlanInfo;
import de.latlon.xplan.validator.planinfo.PlanInfoReport;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version $Revision: $, $Date: $
 */
public class ReportWriterTest {

	private static final String PLAN_NAME = "planName";

	private static final String VALIDATION_NAME = "validationName";

	private static final String FAILURE = "Failure";

	private ReportWriter reportWriter = new ReportWriter();

	@TempDir
	public File temporaryFolder;

	private Path targetDirectory;

	@BeforeEach
	public void createTargetDirectory() throws Exception {
		targetDirectory = Paths.get(newFolder(temporaryFolder, "ReportWriterTest").toURI());
	}

	@Test
	public void testWriteArtefacts_ShouldHaveSubdirectoryWithArtifacts() throws Exception {
		reportWriter.writeArtefacts(createReport(), targetDirectory);

		assertThat(targetDirectory, containsFile(VALIDATION_NAME + ".html"));
		assertThat(targetDirectory, containsFile(VALIDATION_NAME + ".pdf"));
	}

	@Test
	public void testWriteArtefacts_WithFailure() {
		reportWriter.writeArtefacts(createReportThrowingFailure(), targetDirectory);

		assertThat(targetDirectory, containsFile("error.log"));
	}

	@Test
	public void testRetrieveHtmlReport_ShouldExistWithCorrectName() throws Exception {
		reportWriter.writeArtefacts(createReport(), targetDirectory);

		Path htmlReport = reportWriter.retrieveHtmlReport(VALIDATION_NAME, targetDirectory);

		assertThat(Files.exists(htmlReport), is(true));
		assertThat(htmlReport.getFileName().toString(), is(VALIDATION_NAME + ".html"));
	}

	@Test
	public void testWriteZipWithArtifacts_ShouldContainHtml() throws Exception {
		reportWriter.writeArtefacts(createReport(), targetDirectory);

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		reportWriter.writeZipWithArtifacts(outputStream, VALIDATION_NAME, Collections.singletonList(HTML),
				targetDirectory);
		ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(outputStream.toByteArray()));
		assertThat(zipInputStream, hasEntryWithNameAndSize(VALIDATION_NAME + ".html", 1));
	}

	@Test
	public void testWriteZipWithArtifacts_WithFailure() throws Exception {
		reportWriter.writeArtefacts(createReportThrowingFailure(), targetDirectory);

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		reportWriter.writeZipWithArtifacts(outputStream, VALIDATION_NAME, Collections.singletonList(HTML),
				targetDirectory);
		ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(outputStream.toByteArray()));
		assertThat(zipInputStream, hasEntryWithNameAndSize("error.log", 2));
	}

	private static ValidatorReport createReport() {
		GeometricValidatorResult geometricValidatorResult = new GeometricValidatorResult(Collections.emptyList());
		ValidatorReport report = new ValidatorReport();
		PlanInfo planInfo = new PlanInfo("GML_plan1").name(PLAN_NAME)
			.version(XPLAN_60)
			.type(BP_Plan)
			.geometricValidatorResult(geometricValidatorResult);
		report.setPlanInfoReport(new PlanInfoReport().planInfos(singletonMap("GML_plan1", planInfo)));
		report.setValidationName(VALIDATION_NAME);
		return report;
	}

	private ValidatorReport createReportThrowingFailure() {
		PlanInfoReport planInfoReport = mock(PlanInfoReport.class);
		PlanInfo planInfo = mock(PlanInfo.class);
		when(planInfo.getName()).thenReturn(PLAN_NAME);
		when(planInfoReport.getPlanInfos()).thenReturn(singletonMap("GML_plan1", planInfo));
		ValidatorReport report = mock(ValidatorReport.class);
		when(report.getPlanInfoReport()).thenReturn(planInfoReport);
		when(report.getValidationName()).thenReturn(VALIDATION_NAME);
		when(planInfo.getGeometricValidatorResult()).thenThrow(new IllegalArgumentException(FAILURE));
		return report;
	}

	private Matcher<? super Path> containsFile(final String fileName) {
		return new TypeSafeMatcher<>() {

			@Override
			public boolean matchesSafely(Path directory) {
				return Files.isRegularFile(directory.resolve(fileName));
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("Directory must contain a file " + fileName);
			}
		};
	}

	private Matcher<? super Path> containsDirectory(final String directoryName) {
		return new TypeSafeMatcher<>() {

			@Override
			public boolean matchesSafely(Path directory) {
				return Files.isDirectory(directory.resolve(directoryName));
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("Directory must contain a directory " + directoryName);
			}
		};
	}

	private Matcher<ZipInputStream> hasEntryWithNameAndSize(final String expectedName, final int expectedSize) {
		return new TypeSafeMatcher<>() {
			@Override
			protected boolean matchesSafely(ZipInputStream zip) {
				try {
					boolean hasExpectedName = false;
					int numberOfEntries = 0;
					ZipEntry nextEntry = zip.getNextEntry();
					while (nextEntry != null) {
						if (expectedName.equals(nextEntry.getName())) {
							hasExpectedName = true;
						}
						numberOfEntries++;
						nextEntry = zip.getNextEntry();
					}
					return expectedSize == numberOfEntries && hasExpectedName;
				}
				catch (IOException e) {
					throw new IllegalArgumentException("zip cannot be read");
				}
			}

			@Override
			public void describeTo(Description description) {
				description
					.appendText("Zip must contain " + expectedSize + "entries and an entry with name " + expectedName);
			}
		};
	}

	private static File newFolder(File root, String... subDirs) throws IOException {
		String subFolder = String.join("/", subDirs);
		File result = new File(root, subFolder);
		if (!result.mkdirs()) {
			throw new IOException("Couldn't create folders " + root);
		}
		return result;
	}

}
