/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.report.pdf;

import static java.util.Collections.singletonMap;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import de.latlon.xplan.commons.XPlanType;
import de.latlon.xplan.commons.XPlanVersion;
import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.planinfo.PlanInfo;
import de.latlon.xplan.validator.planinfo.PlanInfoReport;
import de.latlon.xplan.validator.report.ValidatorDetail;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.semantic.report.SemanticValidatorResult;
import de.latlon.xplan.validator.syntactic.report.SyntacticValidatorResult;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version $Revision: $, $Date: $
 */
public class PdfReportGeneratorTest {

	@Test
	public void testCreateReportAsPdfWithNullReport() {
		assertThrows(IllegalArgumentException.class, () -> {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			PdfReportGenerator pdfReportGenerator = new PdfReportGenerator();
			pdfReportGenerator.createPdfReport(null, bos);
		});
	}

	@Test
	public void testCreateReportAsPdfWithNullValidationName() throws Exception {
		ValidatorReport report = createReport(null, "PlanName");
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		PdfReportGenerator pdfReportGenerator = new PdfReportGenerator();
		pdfReportGenerator.createPdfReport(report, bos);
	}

	@Test
	public void testCreateReportAsPdfWithEmptyValidationName() throws Exception {
		ValidatorReport report = createReport("", "PlanName");
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		PdfReportGenerator pdfReportGenerator = new PdfReportGenerator();
		pdfReportGenerator.createPdfReport(report, bos);
	}

	@Test
	public void testCreateReportAsPdfWithNullPlanName() throws Exception {
		ValidatorReport report = createReport("ValName", null);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		PdfReportGenerator pdfReportGenerator = new PdfReportGenerator();
		pdfReportGenerator.createPdfReport(report, bos);
	}

	@Test
	public void testCreateReportAsPdfWithEmptyPlanName() throws Exception {
		ValidatorReport report = createReport("ValName", "");
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		PdfReportGenerator pdfReportGenerator = new PdfReportGenerator();
		pdfReportGenerator.createPdfReport(report, bos);
	}

	@Test
	public void testCreateReportAsPdfWithResults() throws Exception {
		ValidatorReport report = createReportWithResults("Validerungtest", "test.gml");
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		PdfReportGenerator pdfReportGenerator = new PdfReportGenerator();
		pdfReportGenerator.createPdfReport(report, bos);
	}

	private ValidatorReport createReport(String validationName, String planName) {
		ValidatorReport validatorReport = new ValidatorReport();
		PlanInfo planInfo = new PlanInfo("GML_plan1").name(planName)
			.version(XPlanVersion.XPLAN_60)
			.type(XPlanType.BP_Plan);
		validatorReport.setPlanInfoReport(new PlanInfoReport().planInfos(singletonMap("GML_plan1", planInfo)));
		validatorReport.setValidationName(validationName);
		validatorReport.setDate(new Date());
		validatorReport.setArchiveName(planName);
		return validatorReport;
	}

	private ValidatorReport createReportWithResults(String validationName, String planName) {
		ValidatorReport validatorReport = createReport(validationName, planName);
		PlanInfo planInfo = new PlanInfo("GML_plan1").name(planName)
			.version(XPlanVersion.XPLAN_60)
			.type(XPlanType.BP_Plan)
			.semanticValidatorResult(createSemanticResult())
			.geometricValidatorResult(createGeometricResult());
		validatorReport.setPlanInfoReport(new PlanInfoReport().planInfos(singletonMap("GML_plan1", planInfo)));
		validatorReport.setValidationName(validationName);

		validatorReport.setSyntacticValidatorResult(createSyntacticResult());

		return validatorReport;
	}

	private SemanticValidatorResult createSemanticResult() {
		SemanticValidatorResult semanticResult = new SemanticValidatorResult();
		for (int i = 0; i < 20; i++) {
			if (i == 8 || i == 2)
				semanticResult.addRule("Name" + i,
						"HinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweisHinweis"
								+ i,
						Collections.emptyList());
			else
				semanticResult.addRule("Name" + i, "Hinweis" + i, Collections.emptyList());
		}
		return semanticResult;
	}

	private SyntacticValidatorResult createSyntacticResult() {
		List<String> messages = new ArrayList<String>();
		addMessages("Syntactic ", messages);
		ValidatorDetail detail = new ValidatorDetail("detailsHint");
		return new SyntacticValidatorResult(messages, detail);
	}

	private GeometricValidatorResult createGeometricResult() {
		return new GeometricValidatorResult(Collections.emptyList());
	}

	private void addMessages(String prefix, List<String> allMessages) {
		for (int i = 0; i < 20; i++) {
			allMessages.add(prefix + i);
		}
	}

}
