/*-
 * #%L
 * xplan-core-validator - XPlan Validator Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.validator.geometric;

import static de.latlon.xplan.validator.geometric.GeometricValidatorImpl.SKIP_OPTIONS;
import static de.latlon.xplan.validator.web.shared.ValidationType.GEOMETRIC;
import static de.latlon.xplan.validator.web.shared.ValidationType.SEMANTIC;
import static de.latlon.xplan.validator.web.shared.ValidationType.SYNTACTIC;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.validator.ValidatorException;
import de.latlon.xplan.validator.XPlanGmlValidation;
import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.listener.InactiveValidationProcessListener;
import de.latlon.xplan.validator.planinfo.PlanInfoParser;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.report.ValidatorResult;
import de.latlon.xplan.validator.web.shared.ValidationSettings;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class ParameterizedGeometricValidatorImplTest {

	private String testResource;

	private boolean expectedValidationResult;

	private int expectedNumberOfErrors;

	private int expectedNumberOfWarnings;

	public static List<Object[]> data() {
		return Arrays
			.asList(new Object[][] { { "xplan41/BP2070.zip", false, 6, 0 }, { "xplan41/BP2135.zip", true, 0, 0 },
					{ "xplan41/Demo.zip", false, 1, 0 }, { "xplan41/Eidelstedt_4_V4.zip", true, 0, 0 },
					{ "xplan41/FPlan.zip", false, 141, 0 }, { "xplan41/LA22.zip", false, 24, 0 },
					{ "xplan41/LA67.zip", false, 7, 0 }, { "xplan41/BPlan001_4-1.zip", true, 0, 0 },
					{ "xplan40/BPlan004_4-0.zip", true, 0, 0 }, { "xplan41/PlanWithComplexCurve.zip", true, 0, 0 },
					{ "xplan50/BP2070.zip", false, 6, 0 }, { "xplan50/BP2135.zip", true, 0, 0 },
					{ "xplan50/FPlan.zip", false, 141, 0 }, { "xplan50/LA22.zip", false, 24, 0 },
					{ "xplan50/LA67.zip", false, 7, 0 }, { "xplan51/BP2070.zip", false, 6, 0 },
					{ "xplan51/BP2135.zip", true, 0, 0 }, { "xplan51/FPlan.zip", false, 141, 0 },
					{ "xplan51/LA22.zip", false, 24, 0 }, { "xplan51/LA67.zip", false, 7, 0 } });
	}

	public void initParameterizedGeometricValidatorImplTest(String testResource, boolean expectedValidationResult,
			int expectedNumberOfErrors, int expectedNumberOfWarnings) {
		this.testResource = testResource;
		this.expectedValidationResult = expectedValidationResult;
		this.expectedNumberOfErrors = expectedNumberOfErrors;
		this.expectedNumberOfWarnings = expectedNumberOfWarnings;
	}

	@MethodSource("data")
	@ParameterizedTest
	public void testValidateGeometry(String testResource, boolean expectedValidationResult, int expectedNumberOfErrors,
			int expectedNumberOfWarnings) throws Exception {
		initParameterizedGeometricValidatorImplTest(testResource, expectedValidationResult, expectedNumberOfErrors,
				expectedNumberOfWarnings);
		XPlanArchive archive = getTestArchive(testResource);
		ValidatorResult report = validateGeometryAndReturnReport(archive);
		GeometricValidatorResult geometricReport = (GeometricValidatorResult) report;
		int numberOfErrors = geometricReport.getErrors().size();
		int numberOfWarnings = geometricReport.getWarnings().size();

		assertThat(report.isValid(), is(expectedValidationResult));
		assertThat(numberOfErrors, is(expectedNumberOfErrors));
		assertThat(numberOfWarnings, is(expectedNumberOfWarnings));
	}

	private ValidatorResult validateGeometryAndReturnReport(XPlanArchive archive) throws ValidatorException {
		GeometricValidatorImpl geometricValidator = new GeometricValidatorImpl();
		ValidationSettings validationSettings = new ValidationSettings("test", List.of(SYNTACTIC, SEMANTIC, GEOMETRIC),
				SKIP_OPTIONS);

		XPlanGmlValidation xPlanGmlValidation = new XPlanGmlValidation(geometricValidator,
				new PlanInfoParser(Optional.empty()), archive, validationSettings,
				new InactiveValidationProcessListener());
		ValidatorReport report = new ValidatorReport();
		xPlanGmlValidation.executeGmlValidation("uuid", report);
		return report.getPlanInfoReport()
			.getPlanInfos()
			.values()
			.stream()
			.findFirst()
			.get()
			.getGeometricValidatorResult();
	}

	private XPlanArchive getTestArchive(String name) throws IOException {
		XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();
		return archiveCreator.createXPlanArchiveFromZip(name, getClass().getResourceAsStream("/testdata/" + name));
	}

}
