/*-
 * #%L
 * xplan-core-validator-events - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.core.validator.events.v1;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.latlon.core.validator.events.v1.XPlanPublicV1Event.EventType;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
class XPlanPublicV1EventTest {

	@Test
	void jsonSerializeValidationStarted() throws Exception {
		XPlanPublicV1Event event = new XPlanPublicV1Event(EventType.VALIDATION_STARTED, "uuid1");

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(event);

		String expected = "{\"apiVersion\":\"1.0\",\"eventType\":\"VALIDATION_STARTED\",\"uuid\":\"uuid1\",\"valid\":null,\"error\":null}";

		assertThat(json).isEqualTo(expected);
	}

	@Test
	void jsonSerializeValidationFinished() throws Exception {
		XPlanPublicV1Event event = new XPlanPublicV1Event(EventType.VALIDATION_FINISHED, "uuid1", true);

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(event);

		String expected = "{\"apiVersion\":\"1.0\",\"eventType\":\"VALIDATION_FINISHED\",\"uuid\":\"uuid1\",\"valid\":true,\"error\":null}";

		assertThat(json).isEqualTo(expected);
	}

	@Test
	void jsonSerializeImportStart() throws Exception {
		XPlanPublicV1Event event = new XPlanPublicV1Event(EventType.IMPORT_STARTED, "uuid1");

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(event);

		String expected = "{\"apiVersion\":\"1.0\",\"eventType\":\"IMPORT_STARTED\",\"uuid\":\"uuid1\",\"valid\":null,\"error\":null}";

		assertThat(json).isEqualTo(expected);
	}

	@Test
	void jsonSerializeImportFinished() throws Exception {
		XPlanPublicV1Event event = new XPlanPublicV1Event(EventType.IMPORT_FINISHED, "uuid1");

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(event);

		String expected = "{\"apiVersion\":\"1.0\",\"eventType\":\"IMPORT_FINISHED\",\"uuid\":\"uuid1\",\"valid\":null,\"error\":null}";

		assertThat(json).isEqualTo(expected);
	}

	@Test
	void jsonSerializeReferencedStarted() throws Exception {
		XPlanPublicV1Event event = new XPlanPublicV1Event(EventType.REFERENCES_STARTED, "uuid1");

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(event);

		String expected = "{\"apiVersion\":\"1.0\",\"eventType\":\"REFERENCES_STARTED\",\"uuid\":\"uuid1\",\"valid\":null,\"error\":null}";

		assertThat(json).isEqualTo(expected);
	}

	@Test
	void jsonSerializeReferencesFinished() throws Exception {
		XPlanPublicV1Event event = new XPlanPublicV1Event(EventType.REFERENCES_FINISHED, "uuid1", true);

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(event);

		String expected = "{\"apiVersion\":\"1.0\",\"eventType\":\"REFERENCES_FINISHED\",\"uuid\":\"uuid1\",\"valid\":true,\"error\":null}";

		assertThat(json).isEqualTo(expected);
	}

}
