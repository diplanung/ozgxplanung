/*-
 * #%L
 * xplan-core-validator-events - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.core.validator.events;

import static de.latlon.core.validator.events.ValidationFinishedEvent.ValidationFinishedStatus.SUCCEEDED;
import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
class ValidationFinishedEventTest {

	@Test
	void jsonSerializeAndDeserialize() throws Exception {
		ValidationFinishedEvent event = new ValidationFinishedEvent("my-uuid", SUCCEEDED);

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(event);

		ValidationFinishedEvent valueFromJson = mapper.readValue(json, ValidationFinishedEvent.class);

		assertThat(valueFromJson).usingRecursiveComparison().isEqualTo(event);

	}

}
