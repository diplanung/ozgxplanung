/*-
 * #%L
 * xplan-core-validator-events - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.core.validator.events.planimport;

import static de.latlon.core.validator.events.ValidationRequestedEvent.OriginFile.GML;
import static de.latlon.core.validator.events.planimport.ImportRequestedEvent.EXTERNAL_REFERENCES_RESULT.AVAILABLE_AND_VALID;
import static de.latlon.core.validator.events.planimport.ImportRequestedEvent.VALIDATION_RESULT.VALID;
import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
class ImportRequestedEventTest {

	@Test
	void jsonSerializeAndDeserialize() throws Exception {
		ImportRequestedEvent event = new ImportRequestedEvent("my-uuid", "validationName", "internalId", "planStatus",
				GML, VALID, AVAILABLE_AND_VALID, true);

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(event);

		ImportRequestedEvent valueFromJson = mapper.readValue(json, ImportRequestedEvent.class);

		assertThat(valueFromJson).usingRecursiveComparison().isEqualTo(event);

	}

}
