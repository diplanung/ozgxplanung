/*-
 * #%L
 * xplan-core-validator-events - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.core.validator.events;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import brave.Tracing;
import brave.spring.rabbit.SpringRabbitTracing;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 7.2
 */
@Configuration
@Import({ ValidationTaskQueueConfig.class, ImportTaskQueueConfig.class })
@Profile("!test")
public class RabbitConfig {

	@Target({ ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD })
	@Retention(RetentionPolicy.RUNTIME)
	@Qualifier
	@interface PrivateTopicExchange {

	}

	@Target({ ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD })
	@Retention(RetentionPolicy.RUNTIME)
	@Qualifier
	@interface PublicTopicExchange {

	}

	@Bean
	@ConditionalOnMissingBean(RabbitSettings.class)
	RabbitSettings rabbitSettings() {
		return new RabbitSettings();
	}

	@Bean
	@ConditionalOnMissingBean(Jackson2JsonMessageConverter.class)
	public Jackson2JsonMessageConverter jsonMessageConverter() {
		Jackson2JsonMessageConverter jsonConverter = new Jackson2JsonMessageConverter();
		return jsonConverter;
	}

	@Bean
	@PrivateTopicExchange
	public TopicExchange privateTopicExchange(RabbitSettings rabbitSettings) {
		return new TopicExchange(rabbitSettings.getPrivateTopicExchangeName());
	}

	@Bean
	@PublicTopicExchange
	public TopicExchange publicTopicExchange(RabbitSettings rabbitSettings) {
		return new TopicExchange(rabbitSettings.getPublicTopicExchangeName());
	}

	@Bean
	public SpringRabbitTracing springRabbitTracing(Tracing tracing) {
		return SpringRabbitTracing.create(tracing);
	}

}
