/*-
 * #%L
 * xplan-core-validator-events - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.core.validator.events.v1;

import java.util.Objects;

import de.latlon.core.validator.events.XPlanPublicEvent;

/**
 * V1 public event.
 *
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 8.0
 */
public class XPlanPublicV1Event implements XPlanPublicEvent {

	public enum EventType {

		VALIDATION_STARTED("validation"),

		SYNTACTIC_VALIDATION_STARTED("validation.syntaktisch"),

		SYNTACTIC_VALIDATION_FINISHED("validation.syntaktisch"),

		REFERENCES_STARTED("validation.referenzen"),

		REFERENCES_FINISHED("validation.referenzen"),

		GEOMETRIC_VALIDATION_STARTED("validation.geometrisch"),

		GEOMETRIC_VALIDATION_FINISHED("validation.geometrisch"),

		SEMANTIC_VALIDATION_STARTED("validation.semantisch"),

		SEMANTIC_VALIDATION_FINISHED("validation.semantisch"),

		PROFILE_VALIDATION_STARTED("validation.profile."),

		PROFILE_VALIDATION_FINISHED("validation.profile."),

		VALIDATION_FINISHED("validation"),

		VALIDATION_FAILED("validation"),

		IMPORT_STARTED("import"),

		IMPORT_FINISHED("import"),

		IMPORT_FAILED("import");

		private final String routingKeySuffix;

		EventType(String routingKeySuffix) {
			this.routingKeySuffix = routingKeySuffix;
		}

		public String routingKeySuffix() {
			return routingKeySuffix;
		}

	}

	private final String apiVersion = "1.0";

	private EventType eventType;

	private String uuid;

	private Boolean valid;

	private String error;

	public XPlanPublicV1Event() {

	}

	public XPlanPublicV1Event(EventType eventType, String uuid) {
		this.eventType = eventType;
		this.uuid = uuid;
	}

	public XPlanPublicV1Event(EventType eventType, String uuid, Boolean valid) {
		this.eventType = eventType;
		this.uuid = uuid;
		this.valid = valid;
	}

	public XPlanPublicV1Event(EventType eventType, String uuid, String error) {
		this.eventType = eventType;
		this.uuid = uuid;
		this.error = error;
	}

	public String getApiVersion() {
		return apiVersion;
	}

	public EventType getEventType() {
		return eventType;
	}

	public String getUuid() {
		return uuid;
	}

	public Boolean getValid() {
		return valid;
	}

	public String getError() {
		return error;
	}

	@Override
	public int hashCode() {
		return Objects.hash(apiVersion, eventType, uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		XPlanPublicV1Event other = (XPlanPublicV1Event) obj;
		return Objects.equals(apiVersion, other.apiVersion) && eventType == other.eventType
				&& Objects.equals(uuid, other.uuid);
	}

	@Override
	public String toString() {
		return "XPlanboxPublicV1Event(" + eventType + ", " + uuid + ")";
	}

}
