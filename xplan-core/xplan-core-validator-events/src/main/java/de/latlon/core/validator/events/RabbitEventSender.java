/*-
 * #%L
 * xplan-core-validator-events - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.core.validator.events;

import static org.slf4j.LoggerFactory.getLogger;

import java.util.HashMap;
import java.util.Map;

import de.latlon.core.validator.events.planimport.ImportRequestedEvent;
import de.latlon.core.validator.events.planimport.ImportValidationRequestedEvent;
import org.slf4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 7.2
 */
class RabbitEventSender implements EventSender {

	private static final Logger LOG = getLogger(RabbitEventSender.class);

	private final RabbitTemplate rabbitTemplate;

	private final String privateTopicExchangeName;

	private final String privateTopicRoutingPrefix;

	private final String publicTopicExchangeName;

	private final String publicTopicRoutingPrefix;

	private final Map<Class<?>, String> taskClass2QueueName = new HashMap<>();

	public RabbitEventSender(RabbitTemplate rabbitTemplate, RabbitSettings rabbitSettings) {
		this.rabbitTemplate = rabbitTemplate;

		this.privateTopicExchangeName = rabbitSettings.getPrivateTopicExchangeName();
		this.privateTopicRoutingPrefix = rabbitSettings.getPrivateTopicRoutingPrefix();
		this.publicTopicExchangeName = rabbitSettings.getPublicTopicExchangeName();
		this.publicTopicRoutingPrefix = rabbitSettings.getPublicTopicRoutingPrefix();

		taskClass2QueueName.put(ValidationRequestedEvent.class, rabbitSettings.getPrivateValidationWorkQueueName());
		taskClass2QueueName.put(ImportValidationRequestedEvent.class,
				rabbitSettings.getPrivateValidationWorkQueueName());
		taskClass2QueueName.put(ImportRequestedEvent.class, rabbitSettings.getPrivateImportWorkQueueName());
	}

	@Override
	public void sendPrivateEvent(XPlanPrivateEvent event) {
		if (event instanceof XPlanPrivateTaskEvent) {
			sendPrivateTaskEvent((XPlanPrivateTaskEvent) event);
		}
		else
			sendPrivateTopicEvent((XPlanPrivateTopicEvent) event);
	}

	@Override
	public void sendPublicEvent(XPlanPublicEvent event, String routingKeySuffix) {
		String routingKey = publicTopicRoutingPrefix + routingKeySuffix;

		LOG.info("Sending public event with key >{}<: {}", routingKey, event);
		rabbitTemplate.convertAndSend(publicTopicExchangeName, routingKey, event);
	}

	private void sendPrivateTaskEvent(XPlanPrivateTaskEvent event) {
		String workQueueName = taskClass2QueueName.get(event.getClass());
		if (workQueueName == null) {
			LOG.error("Ignoring task: no queue configured to send event to ({})", event);
		}
		else {
			LOG.info("Sending private task event: {}", event);
			rabbitTemplate.convertAndSend(workQueueName, event);
		}
	}

	private void sendPrivateTopicEvent(XPlanPrivateTopicEvent event) {
		String routingKey = privateTopicRoutingPrefix + event.getClass().getSimpleName();
		LOG.info("Sending private topic event with key >{}<: {}", routingKey, event);

		rabbitTemplate.convertAndSend(privateTopicExchangeName, routingKey, event);
	}

}
