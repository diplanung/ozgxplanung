/*-
 * #%L
 * xplan-core-validator-events - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.core.validator.events;

import org.springframework.beans.factory.annotation.Value;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 7.2
 */
class RabbitSettings {

	@Value("${xplanbox.rabbitmq.private.anonymousqueues.prefix}")
	private String privateAnonymousQueuesPrefix;

	@Value("${xplanbox.rabbitmq.private.topic.bindingkey:}")
	private String privateTopicBindingKey;

	@Value("${xplanbox.rabbitmq.private.topic.name}")
	private String privateTopicExchangeName;

	@Value("${xplanbox.rabbitmq.private.topic.routingprefix:}")
	private String privateTopicRoutingPrefix;

	@Value("${xplanbox.rabbitmq.private.workqueue.import}")
	private String privateImportWorkQueueName;

	@Value("${xplanbox.rabbitmq.private.workqueue.validation}")
	private String privateValidationWorkQueueName;

	public String getPrivateAnonymousQueuesPrefix() {
		return valueOrDefault(privateAnonymousQueuesPrefix, "xplanbox-private-");
	}

	@Value("${xplanbox.rabbitmq.public.topic.name}")
	private String publicTopicExchangeName;

	@Value("${xplanbox.rabbitmq.public.topic.routingprefix:}")
	private String publicTopicRoutingPrefix;

	public String getPrivateValidationWorkQueueName() {
		return valueOrDefault(privateValidationWorkQueueName, "xplanbox-validation");
	}

	public String getPrivateImportWorkQueueName() {
		return valueOrDefault(privateImportWorkQueueName, "xplanbox-import");
	}

	public String getPrivateTopicExchangeName() {
		return privateTopicExchangeName;
	}

	public String getPrivateTopicBindingKey() {
		return valueOrDefault(privateTopicBindingKey, "#");
	}

	public String getPrivateTopicRoutingPrefix() {
		return privateTopicRoutingPrefix;
	}

	public String getPublicTopicExchangeName() {
		return valueOrDefault(publicTopicExchangeName, "xplanbox-public");
	}

	private String valueOrDefault(String value, String defaultValue) {
		return value.isBlank() ? defaultValue : value;
	}

	public String getPublicTopicRoutingPrefix() {
		return publicTopicRoutingPrefix;
	}

}
