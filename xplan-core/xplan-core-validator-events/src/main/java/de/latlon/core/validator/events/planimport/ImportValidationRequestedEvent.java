/*-
 * #%L
 * xplan-core-validator-events - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.core.validator.events.planimport;

import de.latlon.core.validator.events.ValidationRequestedEvent;
import de.latlon.xplan.validator.web.shared.ValidationSettings;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 8.0
 */
public class ImportValidationRequestedEvent extends ValidationRequestedEvent {

	private String internalId;

	private String planStatus;

	private boolean supportsGetPlanByIdAsZip;

	public ImportValidationRequestedEvent() {
	}

	public ImportValidationRequestedEvent(String uuid, ValidationSettings settings, String xFileName, String internalId,
			String planStatus, MediaType requestedMediaType, OriginFile originFile, boolean supportsGetPlanByIdAsZip) {
		super(uuid, settings, xFileName, requestedMediaType, originFile);
		this.internalId = internalId;
		this.planStatus = planStatus;
		this.supportsGetPlanByIdAsZip = supportsGetPlanByIdAsZip;
	}

	public String getPlanStatus() {
		return planStatus;
	}

	public String getInternalId() {
		return internalId;
	}

	public boolean isSupportsGetPlanByIdAsZip() {
		return supportsGetPlanByIdAsZip;
	}

}
