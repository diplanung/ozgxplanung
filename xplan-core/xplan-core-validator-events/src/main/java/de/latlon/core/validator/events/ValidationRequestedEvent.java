/*-
 * #%L
 * xplan-core-validator-events - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.core.validator.events;

import de.latlon.xplan.validator.web.shared.ValidationSettings;

/**
 * @author <a href="mailto:guillemot@lat-lon.de">Marc Guillemot</a>
 * @since 7.2
 */
public class ValidationRequestedEvent extends XPlanPrivateTaskEvent {

	public enum MediaType {

		ZIP("application/zip", ".zip"), PDF("application/pdf", ".pdf"), JSON("application/json", ".json"),
		XML("application/xml", ".xml");

		private final String mimeType;

		private final String fileExtension;

		MediaType(String mimeType, String fileExtension) {
			this.mimeType = mimeType;
			this.fileExtension = fileExtension;
		}

		public String getFileExtension() {
			return fileExtension;
		}

		public String getMimeType() {
			return mimeType;
		}

	}

	public enum OriginFile {

		ZIP, GML

	}

	private ValidationSettings settings;

	private String uuid;

	private ValidationRequestedEvent.MediaType requestedMediaType;

	private String xFileName;

	private ValidationRequestedEvent.OriginFile originFile;

	public ValidationRequestedEvent() {
	}

	public ValidationRequestedEvent(String uuid, ValidationSettings settings, String xFileName,
			ValidationRequestedEvent.MediaType requestedMediaType, ValidationRequestedEvent.OriginFile originFile) {
		this.uuid = uuid;
		this.settings = settings;
		this.xFileName = xFileName;
		this.requestedMediaType = requestedMediaType;
		this.originFile = originFile;
	}

	public ValidationRequestedEvent.OriginFile getOriginFile() {
		return originFile;
	}

	public ValidationRequestedEvent.MediaType getRequestedMediaType() {
		return requestedMediaType;
	}

	public ValidationSettings getSettings() {
		return settings;
	}

	public String getUuid() {
		return uuid;
	}

	public String getxFileName() {
		return xFileName;
	}

	@Override
	public String toString() {
		return "settings: " + settings + ", uuid: " + uuid;
	}

}
