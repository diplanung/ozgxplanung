/*-
 * #%L
 * xplan-core-validator-events - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.core.validator.events.planimport;

import de.latlon.core.validator.events.ValidationRequestedEvent;
import de.latlon.core.validator.events.XPlanPrivateTaskEvent;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 8.0
 */
public class ImportRequestedEvent extends XPlanPrivateTaskEvent {

	public enum VALIDATION_RESULT {

		VALID, INVALID

	}

	public enum EXTERNAL_REFERENCES_RESULT {

		AVAILABLE_AND_VALID, MISSING_OR_INVALID

	}

	private String uuid;

	private String validationName;

	private String internalId;

	private String planStatus;

	private ValidationRequestedEvent.OriginFile originFile;

	private VALIDATION_RESULT validationResult;

	private EXTERNAL_REFERENCES_RESULT externalReferencesResult;

	private boolean supportsGetPlanByIdAsZip;

	public ImportRequestedEvent() {
	}

	public ImportRequestedEvent(String uuid, String validationName, String internalId, String planStatus,
			ValidationRequestedEvent.OriginFile originFile, VALIDATION_RESULT isValid,
			EXTERNAL_REFERENCES_RESULT externalReferencesResult, boolean supportsGetPlanByIdAsZip) {
		this.uuid = uuid;
		this.validationName = validationName;
		this.internalId = internalId;
		this.planStatus = planStatus;
		this.originFile = originFile;
		this.validationResult = isValid;
		this.externalReferencesResult = externalReferencesResult;
		this.supportsGetPlanByIdAsZip = supportsGetPlanByIdAsZip;
	}

	public String getUuid() {
		return uuid;
	}

	public String getValidationName() {
		return validationName;
	}

	public String getPlanStatus() {
		return planStatus;
	}

	public String getInternalId() {
		return internalId;
	}

	public ValidationRequestedEvent.OriginFile getOriginFile() {
		return originFile;
	}

	public VALIDATION_RESULT getValidationResult() {
		return validationResult;
	}

	public EXTERNAL_REFERENCES_RESULT getExternalReferencesResult() {
		return externalReferencesResult;
	}

	public boolean isSupportsGetPlanByIdAsZip() {
		return supportsGetPlanByIdAsZip;
	}

}
