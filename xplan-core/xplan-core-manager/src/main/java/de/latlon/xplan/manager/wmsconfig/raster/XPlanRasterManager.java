/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.wmsconfig.raster;

import static de.latlon.xplanbox.core.raster.RasterUtils.findRasterplanZipEntry;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.latlon.xplan.commons.archive.XPlanArchiveContentAccess;
import de.latlon.xplan.commons.s3.StorageException;
import de.latlon.xplan.manager.edit.RasterReference;
import de.latlon.xplan.manager.storage.StorageEvent;
import de.latlon.xplan.manager.wmsconfig.raster.storage.RasterStorage;
import de.latlon.xplan.manager.workspace.WorkspaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;

/**
 * An instance of XPlanRasterManager provides the service methods to manage raster files
 * part of XPlan.
 *
 * @author <a href="mailto:schneider@occamlabs.de">Markus Schneider</a>
 * @since 1.0
 */
public class XPlanRasterManager {

	private static final Logger LOG = LoggerFactory.getLogger(XPlanRasterManager.class);

	private final RasterStorage rasterStorage;

	private final ApplicationEventPublisher applicationEventPublisher;

	/**
	 * Instantiates a {@link XPlanRasterManager} with workspace and manager configuration.
	 * @param rasterStorage the RasterStorage used to write raster files, never
	 * <code>null</code>
	 * @param applicationEventPublisher never <code>null</code>
	 * @throws WorkspaceException
	 */
	public XPlanRasterManager(RasterStorage rasterStorage, ApplicationEventPublisher applicationEventPublisher)
			throws WorkspaceException {
		this.rasterStorage = rasterStorage;
		this.applicationEventPublisher = applicationEventPublisher;
	}

	/**
	 * Removes the configuration of the plan with the given id.
	 * @param planId the id of the plan to remove, should not be <code>null</code>
	 * @param referenzUrlToRemove list of referenceUrls (relative) to remove
	 */
	public void removeRasterLayers(int planId, List<String> referenzUrlToRemove) {
		StorageEvent storageEvent = new StorageEvent();
		try {
			for (String fileName : referenzUrlToRemove) {
				if (fileName != null) {
					rasterStorage.deleteRasterFile(planId, fileName, storageEvent);
				}
			}
		}
		catch (Exception e) {
			LOG.error("Rasterlayers of plan with id " + planId + " could not be removed: {}", e.getMessage());
			LOG.trace("Rasterlayers of plan with id " + planId + " could not be removed!", e);
		}
		finally {
			applicationEventPublisher.publishEvent(storageEvent);
		}
	}

	/**
	 * Creates one raster layer for each referenced raster. Sorts the raster layer after
	 * the plan with the moreRecentPlanId or at the end.
	 * @param archive containing the rasterdata to evaluate, never <code>null</code>
	 * @param rasterRefsToAdd list of refrences to add, never <code>null</code>
	 * @param planId the id of the plan, never <code>null</code>
	 * @param sortDate
	 */
	public void updateWmsWorkspaceWithRasterLayers(XPlanArchiveContentAccess archive,
			List<RasterReference> rasterRefsToAdd, int planId, Date sortDate) {
		long begin = System.currentTimeMillis();

		String sortDateAsString = "unbekannt";
		if (sortDate != null) {
			SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy");
			sortDateAsString = sd.format(sortDate);
		}
		LOG.info("- Erzeugen/Einsortieren der Rasterkonfigurationen (nach Datum: {} )...", sortDateAsString);
		StorageEvent storageEvent = new StorageEvent();
		try {
			logScanFiles(begin, rasterRefsToAdd);

			storeRasterfiles(archive, rasterRefsToAdd, planId, storageEvent);
		}
		catch (Exception e) {
			LOG.error("Rasterconfiguration could not be created: {} ", e.getMessage());
			LOG.trace("Rasterconfiguration could not be created!", e);
			throw new RuntimeException("Fehler beim Erzeugen der Rasterkonfigurationen: " + e.getLocalizedMessage());
		}
		finally {
			applicationEventPublisher.publishEvent(storageEvent);
		}
	}

	private void storeRasterfiles(XPlanArchiveContentAccess archive, List<RasterReference> rasterRefsToAdd, int planId,
			StorageEvent storageEvent) throws IOException, StorageException {
		for (RasterReference rasterRef : rasterRefsToAdd) {
			String entryName = findRasterplanZipEntry(archive, rasterRef.getReferenzUrl()).getName();
			String entryNameGeoref = rasterRef.getGeoRefUrl() != null
					? findRasterplanZipEntry(archive, rasterRef.getGeoRefUrl()).getName() : null;
			LOG.debug("Raster data entry {} ", entryName);
			rasterStorage.addRasterFile(planId, entryName, entryNameGeoref, archive, storageEvent);
		}
	}

	private String createRasterId(String dataFileName) {
		return dataFileName.replaceAll(".tiff?", "");
	}

	private void logScanFiles(long begin, List<RasterReference> rasterRefs) {
		long elapsed = System.currentTimeMillis() - begin;
		LOG.info("OK [{} ms]", elapsed);
		if (!rasterRefs.isEmpty()) {
			LOG.info("Rasterscans:");
			for (RasterReference rasterRef : rasterRefs) {
				LOG.info(" - {}/{}", rasterRef.getReferenzUrl(), rasterRef.getGeoRefUrl());
			}
		}
	}

}
