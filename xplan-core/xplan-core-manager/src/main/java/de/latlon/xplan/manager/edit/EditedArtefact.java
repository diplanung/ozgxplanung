/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.edit;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 7.0
 */
public class EditedArtefact {

	private String fileName;

	private String georefFileName;

	private ArtefactType artefactType;

	private EditType editType;

	public EditedArtefact(String fileName, String georefFileName, ArtefactType artefactType, EditType editType) {
		this.fileName = fileName;
		this.georefFileName = georefFileName;
		this.artefactType = artefactType;
		this.editType = editType;
	}

	/**
	 * @return the fileName, never <code>null</code>
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @return the fileName of the georef, may be <code>null</code>
	 */
	public String getGeorefFileName() {
		return georefFileName;
	}

	/**
	 * @return the artefactType, never <code>null</code>
	 */
	public ArtefactType getArtefactType() {
		return artefactType;
	}

	/**
	 * @return the editType, never <code>null</code>
	 */
	public EditType getEditType() {
		return editType;
	}

}
