/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.export;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import de.latlon.xplan.core.manager.db.model.Artefact;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Exports the content of a plan as zip archive.
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version $Revision: $, $Date: $
 */
public final class XPlanArtefactExporter {

	private final static Logger LOG = LoggerFactory.getLogger(XPlanArtefactExporter.class);

	private XPlanArtefactExporter() {
	}

	/**
	 * Exports the content of a plan as zip archive.
	 * @param outputStream to write the artefacts into, never <code>null</code>
	 * @param artefacts the content to write, the underlying artefacts stream is closed
	 * after the export, never <code>null</code>
	 * @throws NullPointerException if outputStream or contents is <code>null</code>
	 * @throws XPlanExportException if an error occurred during export
	 */
	public static void export(OutputStream outputStream, List<Artefact> artefacts) {
		long begin = System.currentTimeMillis();
		writeContentToStream(outputStream, artefacts);
		long elapsed = System.currentTimeMillis() - begin;
		LOG.info("OK [" + elapsed + " ms]");
	}

	private static void writeContentToStream(OutputStream outputStream, List<Artefact> artefacts) {
		try {
			ZipOutputStream zipOS = new ZipOutputStream(outputStream);
			artefacts.forEach(artefact -> writeArtefactToStream(zipOS, artefact));
			zipOS.close();
		}
		catch (XPlanExportException e) {
			throw e;
		}
		catch (Exception e) {
			LOG.error("Plan could not be exported!", e);
			throw new XPlanExportException("Fehler beim Exportieren des Plans: " + e.getMessage() + ".", e);
		}
	}

	private static void writeArtefactToStream(ZipOutputStream zos, Artefact artefact) {
		String fileName = artefact.getId().getFilename();
		LOG.info("- Schreibe Artefakt '" + fileName + "'...");
		byte[] artefactContent = artefact.getData();
		try (ByteArrayInputStream bis = new ByteArrayInputStream(artefactContent);
				GZIPInputStream is = new GZIPInputStream(bis)) {
			ZipEntry entry = new ZipEntry(fileName);
			zos.putNextEntry(entry);
			byte[] buffer = new byte[10240];
			int read;
			while ((read = is.read(buffer)) != -1) {
				zos.write(buffer, 0, read);
			}
		}
		catch (Exception e) {
			throw new XPlanExportException("Fehler beim Rekonstruieren des XPlan-Artefakts mit Namen " + fileName + ": "
					+ e.getLocalizedMessage(), e);
		}
		finally {
			try {
				zos.closeEntry();
			}
			catch (IOException e) {
			}
		}
	}

}
