<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<!--Erzeugt mit FZK (www.iai.fzk.de) XPlanGML-Toolbox, Erstellungsdatum: 12/07/10 -->
<XPlanAuszug xmlns="http://www.xplanung.de/xplangml/4/1" xmlns:xplan="http://www.xplanung.de/xplangml/4/1"
  xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:wfs="http://www.adv-online.de/namespaces/adv/gid/wfs" xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.xplanung.de/xplangml/4/1 ../../../../../../../../../xplan-schemas/src/main/resources/appschemas/XPlanGML_4_1/XPlanung-Operationen.xsd"
  gml:id="GML_cbfbff49-c213-4613-9150-eb55cb057a27">
  <gml:boundedBy>
    <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
      <gml:lowerCorner>559573.142 5938465.032</gml:lowerCorner>
      <gml:upperCorner>560174.871 5939188.129</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_671C685B-CE75-4B05-8236-622B0B8A7A5B">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559573.142 5938465.032</gml:lowerCorner>
          <gml:upperCorner>560174.871 5939188.129</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>Eidelstedt 4</xplan:name>
      <xplan:beschreibung>Beschreibung von Eidelstedt 4</xplan:beschreibung>
      <xplan:technHerstellDatum>2010-10-07</xplan:technHerstellDatum>
      <xplan:untergangsDatum>2020-10-07</xplan:untergangsDatum>
      <xplan:aendert>
        <xplan:XP_VerbundenerPlan>
          <xplan:planName>Eidelstedt 3 (alt)</xplan:planName>
          <xplan:rechtscharakter>1100</xplan:rechtscharakter>
          <xplan:nummer>3</xplan:nummer>
        </xplan:XP_VerbundenerPlan>
      </xplan:aendert>
      <xplan:wurdeGeaendertVon>
        <xplan:XP_VerbundenerPlan>
          <xplan:planName>Eidelstedt 4 (textliche Änderung)</xplan:planName>
          <xplan:rechtscharakter>1000</xplan:rechtscharakter>
        </xplan:XP_VerbundenerPlan>
      </xplan:wurdeGeaendertVon>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:xPlanGMLVersion>4.0</xplan:xPlanGMLVersion>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_2e24017b-a16a-4e32-9753-5efaf718c54f">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="90">559588.000 5938591.055 559587.584 5938589.472 559583.278
                5938573.082 559583.149 5938571.755 559706.528 5938556.561 559735.128 5938550.929 559775.580 5938543.091
                559909.797 5938517.162 559918.660 5938515.314 559925.431 5938513.743 559939.317 5938510.055 559949.989
                5938507.022 559970.985 5938500.137 559980.737 5938496.974 559988.476 5938494.141 559997.949 5938490.930
                560011.170 5938487.024 560024.244 5938483.599 560048.366 5938478.727 560066.776 5938475.199 560120.345
                5938465.032 560132.443 5938513.348 560136.443 5938529.882 560143.057 5938557.079 560155.365 5938606.254
                560160.428 5938627.258 560166.474 5938682.082 560167.786 5938696.283 560174.485 5938757.619 560174.871
                5938763.042 560174.231 5938763.599 560165.753 5938770.895 560099.434 5938826.609 560097.913 5938827.897
                560096.424 5938829.158 560094.891 5938830.455 560077.715 5938844.888 560064.799 5938856.511 560059.460
                5938861.538 560032.472 5938889.745 560031.485 5938890.791 560018.978 5938902.489 559982.030 5938935.989
                559961.754 5938953.049 559949.718 5938962.911 559944.324 5938967.387 559943.828 5938966.784 559943.185
                5938967.311 559943.325 5938967.481 559942.071 5938968.516 559931.803 5938977.053 559929.832 5938978.808
                559927.087 5938981.252 559935.814 5939000.612 559931.162 5939004.098 559767.728 5939132.488 559696.038
                5939188.129 559616.839 5939112.806 559617.074 5939107.548 559617.518 5939107.548 559619.053 5939096.806
                559613.768 5939094.412 559575.182 5939084.385 559574.187 5939075.403 559573.182 5939067.904 559573.142
                5939060.323 559574.167 5939052.818 559608.521 5938863.627 559622.049 5938787.301 559622.383 5938779.819
                559622.133 5938772.306 559617.656 5938730.219 559616.762 5938718.653 559616.039 5938699.527 559616.094
                5938693.509 559616.032 5938693.456 559616.102 5938692.604 559616.321 5938668.884 559615.939 5938658.123
                559614.416 5938643.266 559614.175 5938642.445 559610.659 5938641.773 559609.721 5938638.393 559607.971
                5938633.446 559601.244 5938619.493 559596.936 5938607.468 559591.337 5938598.352 559588.574 5938592.311
                559588.103 5938591.279 559588.000 5938591.055
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>Blankenese</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>2000</xplan:verfahren>
      <xplan:rechtsstand>4000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>1970-01-01</xplan:rechtsverordnungsDatum>
      <xplan:inkrafttretensDatum>1973-10-16</xplan:inkrafttretensDatum>
      <xplan:veraenderungssperre>true</xplan:veraenderungssperre>
      <xplan:staedtebaulicherVertrag>false</xplan:staedtebaulicherVertrag>
      <xplan:bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_1C210013-14A8-491F-AD56-024BDFE3F111">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559573.142 5938465.032</gml:lowerCorner>
          <gml:upperCorner>560174.871 5939188.129</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:nachrichtlich xlink:href="#GML_AE8AC554-FCCB-4328-8A54-51B5BE7F5B7F" />
      <xplan:nachrichtlich xlink:href="#GML_533C1C8D-4094-4276-8824-3B4CDA6D1807" />
      <xplan:praesentationsobjekt xlink:href="#GML_1F108B1E-A2B9-4EEE-ADA3-0CAAD9E85AF6" />
      <xplan:praesentationsobjekt xlink:href="#GML_51C1E272-B821-4DEC-9BD0-7A33F2DCCD6A" />
      <xplan:praesentationsobjekt xlink:href="#GML_ADEAAF45-41BD-4C3A-A3C4-CAB89226FD5D" />
      <xplan:praesentationsobjekt xlink:href="#GML_F2896D61-55BD-41E6-8035-3300E01AE589" />
      <xplan:praesentationsobjekt xlink:href="#GML_90AAA765-4F10-40DE-9DF3-C0F4C45EB547" />
      <xplan:praesentationsobjekt xlink:href="#GML_5C057E8A-7B3C-46AB-98C9-C9D57328920B" />
      <xplan:praesentationsobjekt xlink:href="#GML_CFBCCB90-6E86-4807-9CB7-7F471C395D81" />
      <xplan:praesentationsobjekt xlink:href="#GML_011220F6-0B44-4EF9-B1BB-34146DA653BF" />
      <xplan:praesentationsobjekt xlink:href="#GML_E2BD2C83-4C4C-4D2F-95AA-99923FBAB1A3" />
      <xplan:praesentationsobjekt xlink:href="#GML_512E4B69-58E9-4A5D-961F-1F5BBFF027DB" />
      <xplan:praesentationsobjekt xlink:href="#GML_8821C455-CBA5-4713-882F-434CB9BF96DF" />
      <xplan:praesentationsobjekt xlink:href="#GML_FC8D3F76-78C5-4BCD-A946-99D876B894EF" />
      <xplan:praesentationsobjekt xlink:href="#GML_12E3A430-EAC1-407F-898F-3116CE3F94EE" />
      <xplan:praesentationsobjekt xlink:href="#GML_94CF7CBD-9042-4AA0-8EA0-736BC1BC0757" />
      <xplan:praesentationsobjekt xlink:href="#GML_59E7C507-4BE5-4C12-A947-9A892123637B" />
      <xplan:praesentationsobjekt xlink:href="#GML_7D98E041-66FC-48DD-B6E6-A544F9216D2F" />
      <xplan:praesentationsobjekt xlink:href="#GML_7175E100-8706-4690-A79C-6B20D21703DE" />
      <xplan:praesentationsobjekt xlink:href="#GML_2DD7C743-B538-4E78-9143-2179ACF0C737" />
      <xplan:praesentationsobjekt xlink:href="#GML_194448C9-2BD7-4742-98E1-F75F523F4FA0" />
      <xplan:praesentationsobjekt xlink:href="#GML_BCA8C7F9-3B26-421A-8DD0-CF1646871CAE" />
      <xplan:praesentationsobjekt xlink:href="#GML_CC23CA29-6CB0-4374-B48E-F74E84B0CC4C" />
      <xplan:praesentationsobjekt xlink:href="#GML_CC244AE8-013E-45A7-AA8F-2B893592C5EF" />
      <xplan:praesentationsobjekt xlink:href="#GML_BA39F1EC-909E-469F-9732-0CF711EC7204" />
      <xplan:versionBauNVO>4000</xplan:versionBauNVO>
      <xplan:gehoertZuPlan xlink:href="#GML_671C685B-CE75-4B05-8236-622B0B8A7A5B" />
      <xplan:inhaltBPlan xlink:href="#GML_55CA442A-B21F-45E4-A318-EBDE2176DA22" />
      <xplan:inhaltBPlan xlink:href="#GML_D09CEDBB-8925-45AB-8B51-27A9DDDE0729" />
      <xplan:inhaltBPlan xlink:href="#GML_F7A143F6-D488-47A4-B59D-5A54105DCD25" />
      <xplan:inhaltBPlan xlink:href="#GML_129A74FB-06C2-4D19-B989-0520DFA44499" />
      <xplan:inhaltBPlan xlink:href="#GML_B709120A-0474-4C43-B87B-0BAD5D4ED643" />
      <xplan:inhaltBPlan xlink:href="#GML_062F366D-A131-4411-9628-F3E41BECBF8E" />
      <xplan:inhaltBPlan xlink:href="#GML_5DD0B5C6-430A-4A28-9E73-E4D510EB0A9B" />
      <xplan:inhaltBPlan xlink:href="#GML_0B911BB1-8BB9-4D0D-ABD2-10D6C3BDC455" />
      <xplan:inhaltBPlan xlink:href="#GML_4FFC6EF4-9D70-44C2-8E82-6396AF0B43D2" />
      <xplan:inhaltBPlan xlink:href="#GML_A2806AA2-CE11-4DDA-AB1B-4D2383C5D63A" />
      <xplan:inhaltBPlan xlink:href="#GML_76DB7973-30CE-4669-B1FD-2DAC4750AAB1" />
      <xplan:inhaltBPlan xlink:href="#GML_EBB01753-E69E-4C28-87E8-67984CDBAF26" />
      <xplan:inhaltBPlan xlink:href="#GML_37C4F6F5-92D6-4282-82DB-C05148A9A952" />
      <xplan:inhaltBPlan xlink:href="#GML_AA3B905F-9877-4A47-A9E5-F26EC96F6477" />
      <xplan:inhaltBPlan xlink:href="#GML_2E9152E3-2213-4D0B-94A5-797234DAA69D" />
      <xplan:inhaltBPlan xlink:href="#GML_270DAD4F-493B-4DA4-B4DE-63531664DB7E" />
      <xplan:inhaltBPlan xlink:href="#GML_0633B265-59B6-4442-9376-70DB9F098C40" />
      <xplan:inhaltBPlan xlink:href="#GML_2D74790B-B74F-45F1-BD3E-7A6C52401131" />
      <xplan:inhaltBPlan xlink:href="#GML_221C4C13-65F2-4184-883A-551442542452" />
      <xplan:inhaltBPlan xlink:href="#GML_3CE5E8DD-8CD9-447D-96F6-E637D5B4E182" />
      <xplan:inhaltBPlan xlink:href="#GML_9E3659B1-CBC6-4F3D-BC45-B5A7E0D3E134" />
      <xplan:inhaltBPlan xlink:href="#GML_439A7922-255C-48B9-8BF8-F37843A359FA" />
      <xplan:inhaltBPlan xlink:href="#GML_1A0B420B-C96D-445B-880F-80CA3E609096" />
      <xplan:inhaltBPlan xlink:href="#GML_02D24813-9C98-4279-B4DD-AD7FE05B40BD" />
      <xplan:inhaltBPlan xlink:href="#GML_95FE9C78-BCF7-4504-83B8-F46C6C0A6FDA" />
      <xplan:inhaltBPlan xlink:href="#GML_5A0F778F-8E15-4616-9FDD-EBB068AFD704" />
      <xplan:inhaltBPlan xlink:href="#GML_AE52C847-92B6-40EE-9C3E-2FF8A8E93396" />
      <xplan:inhaltBPlan xlink:href="#GML_D014345B-9A71-4F2F-9135-70350A03A501" />
      <xplan:inhaltBPlan xlink:href="#GML_E145EBFE-8CDE-4A10-AB78-8379E7EB1797" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="GML_AE8AC554-FCCB-4328-8A54-51B5BE7F5B7F">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559573.142 5938587.485</gml:lowerCorner>
          <gml:upperCorner>559670.663 5939096.806</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_4261A820_7876_4F5C_AC63_41D949BC8BB2</xplan:uuid>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_c4f0b21e-407a-4e21-9e72-9827f8456713">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="86">559620.127 5939096.065 559619.053 5939096.806 559613.768
                5939094.412 559575.182 5939084.385 559574.187 5939075.403 559573.182 5939067.904 559573.142 5939060.323
                559574.167 5939052.818 559608.521 5938863.627 559622.049 5938787.301 559622.383 5938779.819 559622.133
                5938772.306 559617.656 5938730.219 559616.762 5938718.653 559616.039 5938699.527 559616.094 5938693.509
                559616.032 5938693.456 559616.102 5938692.604 559616.321 5938668.884 559615.939 5938658.123 559614.416
                5938643.266 559614.175 5938642.445 559610.659 5938641.773 559609.721 5938638.393 559607.971 5938633.446
                559601.244 5938619.493 559596.936 5938607.468 559591.337 5938598.352 559588.574 5938592.311 559588.103
                5938591.279 559588.000 5938591.055 559587.584 5938589.472 559594.815 5938587.485 559594.888 5938587.723
                559594.980 5938587.955 559595.091 5938588.178 559595.221 5938588.391 559595.367 5938588.593 559595.530
                5938588.782 559595.708 5938588.956 559595.900 5938589.116 559596.104 5938589.258 559596.320 5938589.384
                559596.545 5938589.491 559596.778 5938589.579 559597.018 5938589.647 559597.263 5938589.695 559597.511
                5938589.723 559597.760 5938589.730 559598.009 5938589.716 559598.256 5938589.682 559598.499 5938589.627
                559602.995 5938588.411 559608.617 5938594.678 559606.617 5938610.300 559612.483 5938618.331 559616.981
                5938627.160 559618.172 5938630.164 559619.124 5938632.860 559619.898 5938635.871 559621.443 5938642.192
                559623.605 5938657.439 559624.517 5938669.196 559623.631 5938699.203 559623.694 5938708.256 559621.853
                5938708.952 559625.474 5938742.112 559628.156 5938771.674 559628.192 5938780.041 559627.694 5938786.341
                559627.539 5938788.299 559614.239 5938861.401 559590.659 5938990.377 559585.803 5939016.947 559580.407
                5939046.466 559579.097 5939053.636 559578.455 5939060.023 559578.684 5939066.369 559579.772 5939072.640
                559581.700 5939078.694 559611.974 5939086.101 559619.459 5939087.933 559670.663 5939052.611 559667.960
                5939059.273 559666.599 5939063.988 559620.127 5939096.065
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1200</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_533C1C8D-4094-4276-8824-3B4CDA6D1807">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559659.259 5938954.440</gml:lowerCorner>
          <gml:upperCorner>559931.162 5939188.129</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_0D93F75E_63C3_4B34_B336_DFED36289915</xplan:uuid>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_c4f6356f-a0b2-4d4f-8afc-7cb088265bab">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="28">559696.038 5939188.129 559659.259 5939153.149 559680.323
                5939138.430 559758.267 5939090.693 559757.879 5939088.114 559775.939 5939075.892 559784.539 5939070.852
                559794.129 5939065.231 559816.294 5939053.885 559838.416 5939041.810 559838.690 5939041.593 559838.901
                5939041.860 559839.254 5939041.581 559839.044 5939041.314 559865.437 5939020.453 559890.840 5939000.374
                559894.798 5938994.317 559901.626 5938980.665 559905.985 5938971.265 559912.018 5938954.440 559913.205
                5938959.795 559914.449 5938964.365 559917.839 5938974.169 559919.351 5938977.839 559923.920 5938988.526
                559931.162 5939004.098 559767.728 5939132.488 559696.038 5939188.129
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1F108B1E-A2B9-4EEE-ADA3-0CAAD9E85AF6">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>560012.895 5938715.818</gml:lowerCorner>
          <gml:upperCorner>560012.895 5938715.818</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:art>xplan:besondereArtDerBaulNutzung</xplan:art>
      <xplan:dientZurDarstellungVon xlink:href="#GML_76DB7973-30CE-4669-B1FD-2DAC4750AAB1" />
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_5faa301e-dd09-4e8b-a38f-ec3ed2b8be41">
          <gml:pos>560012.895 5938715.818</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_51C1E272-B821-4DEC-9BD0-7A33F2DCCD6A">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559724.647 5938737.598</gml:lowerCorner>
          <gml:upperCorner>559724.647 5938737.598</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GBH 23.0 m</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_3e524d0e-26c0-4730-b29b-3ced17d75897">
          <gml:pos>559724.647 5938737.598</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_ADEAAF45-41BD-4C3A-A3C4-CAB89226FD5D">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559724.938 5938744.508</gml:lowerCorner>
          <gml:upperCorner>559724.938 5938744.508</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>BMZ 9.0</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_a4c0d121-2bd0-48c2-af79-a8b17487d6d4">
          <gml:pos>559724.938 5938744.508</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_F2896D61-55BD-41E6-8035-3300E01AE589">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559724.579 5938750.485</gml:lowerCorner>
          <gml:upperCorner>559724.579 5938750.485</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GRZ 0.8</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_c4de35ea-90f2-41dd-b851-41925e26bdc9">
          <gml:pos>559724.579 5938750.485</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_90AAA765-4F10-40DE-9DF3-C0F4C45EB547">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559724.619 5938756.842</gml:lowerCorner>
          <gml:upperCorner>559724.619 5938756.842</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GI</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_884d4086-c252-4c68-bdd1-b6256fe1ce89">
          <gml:pos>559724.619 5938756.842</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_5C057E8A-7B3C-46AB-98C9-C9D57328920B">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559903.837 5938552.879</gml:lowerCorner>
          <gml:upperCorner>559903.837 5938552.879</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GBH 18.0 m</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_402f51bb-060b-48b8-a19f-7c1178ce23b1">
          <gml:pos>559903.837 5938552.879</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_CFBCCB90-6E86-4807-9CB7-7F471C395D81">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559914.656 5938576.223</gml:lowerCorner>
          <gml:upperCorner>559914.656 5938576.223</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GBH 23.0 m</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_e6f3a4e2-3460-47e8-b3ad-0b312b819ccd">
          <gml:pos>559914.656 5938576.223</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_011220F6-0B44-4EF9-B1BB-34146DA653BF">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>560000.337 5938524.251</gml:lowerCorner>
          <gml:upperCorner>560000.337 5938524.251</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GBH 23.0 m</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_b0e6ed50-b1cf-4dcd-8ed4-25d967279b1a">
          <gml:pos>560000.337 5938524.251</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_E2BD2C83-4C4C-4D2F-95AA-99923FBAB1A3">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>560044.764 5938558.755</gml:lowerCorner>
          <gml:upperCorner>560044.764 5938558.755</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GBH 17.0 m</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_81adcf3b-458b-451e-90c8-4abee64901f3">
          <gml:pos>560044.764 5938558.755</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_512E4B69-58E9-4A5D-961F-1F5BBFF027DB">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559978.155 5938635.710</gml:lowerCorner>
          <gml:upperCorner>559978.155 5938635.710</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GBH 23.0</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_5721283f-9f09-4b44-a9d7-6666965da5a5">
          <gml:pos>559978.155 5938635.710</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_8821C455-CBA5-4713-882F-434CB9BF96DF">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559978.380 5938641.648</gml:lowerCorner>
          <gml:upperCorner>559978.380 5938641.648</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>BMZ 9.0</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_295cc420-4b86-4dab-956a-51f613a04731">
          <gml:pos>559978.380 5938641.648</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_FC8D3F76-78C5-4BCD-A946-99D876B894EF">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559978.335 5938647.402</gml:lowerCorner>
          <gml:upperCorner>559978.335 5938647.402</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GRZ 0.8</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_53106b99-c556-4915-bf9a-1a39de004618">
          <gml:pos>559978.335 5938647.402</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_12E3A430-EAC1-407F-898F-3116CE3F94EE">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559978.580 5938653.392</gml:lowerCorner>
          <gml:upperCorner>559978.580 5938653.392</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GI</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_f6d1be49-28dd-40dc-ba43-416a43a37cd4">
          <gml:pos>559978.580 5938653.392</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_94CF7CBD-9042-4AA0-8EA0-736BC1BC0757">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559676.358 5939023.214</gml:lowerCorner>
          <gml:upperCorner>559676.358 5939023.214</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>g</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_94aaa03f-b1b5-4f52-a9b4-52a34121550d">
          <gml:pos>559676.358 5939023.214</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_59E7C507-4BE5-4C12-A947-9A892123637B">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559667.711 5939023.055</gml:lowerCorner>
          <gml:upperCorner>559667.711 5939023.055</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>VI</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_59f5ec37-5ae2-4257-8d1d-d1f284ae9b46">
          <gml:pos>559667.711 5939023.055</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_7D98E041-66FC-48DD-B6E6-A544F9216D2F">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559658.593 5939010.398</gml:lowerCorner>
          <gml:upperCorner>559658.593 5939010.398</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GFZ 2.4</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_64e55148-296b-465a-a777-becc9bb224c5">
          <gml:pos>559658.593 5939010.398</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_7175E100-8706-4690-A79C-6B20D21703DE">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559658.607 5939016.762</gml:lowerCorner>
          <gml:upperCorner>559658.607 5939016.762</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GRZ 0.8</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_3f9e2378-8978-42ca-a8ca-08f9cfe46eb2">
          <gml:pos>559658.607 5939016.762</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_2DD7C743-B538-4E78-9143-2179ACF0C737">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559658.623 5939022.999</gml:lowerCorner>
          <gml:upperCorner>559658.623 5939022.999</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GE</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_b327e8ab-96ce-4cda-8b41-e4665e6081c8">
          <gml:pos>559658.623 5939022.999</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_194448C9-2BD7-4742-98E1-F75F523F4FA0">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559732.888 5939042.375</gml:lowerCorner>
          <gml:upperCorner>559732.888 5939042.375</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GBH 23.0 m</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_6323b69e-6085-4855-9148-de1b6868abc0">
          <gml:pos>559732.888 5939042.375</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_BCA8C7F9-3B26-421A-8DD0-CF1646871CAE">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559732.722 5939048.635</gml:lowerCorner>
          <gml:upperCorner>559732.722 5939048.635</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>BMZ 9.0</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_0fbcaee1-0886-4cb7-93f5-dfe5d97c5943">
          <gml:pos>559732.722 5939048.635</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_CC23CA29-6CB0-4374-B48E-F74E84B0CC4C">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559732.929 5939055.065</gml:lowerCorner>
          <gml:upperCorner>559732.929 5939055.065</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GRZ 0.8</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_66378b26-baae-423f-9264-d6b6d37d97fd">
          <gml:pos>559732.929 5939055.065</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_CC244AE8-013E-45A7-AA8F-2B893592C5EF">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559732.946 5939061.497</gml:lowerCorner>
          <gml:upperCorner>559732.946 5939061.497</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>GI</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_c299feba-8be1-4dd8-ac6b-dd0361573d82">
          <gml:pos>559732.946 5939061.497</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_BA39F1EC-909E-469F-9732-0CF711EC7204">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559913.215 5938847.830</gml:lowerCorner>
          <gml:upperCorner>559913.215 5938847.830</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>1000</xplan:stylesheetId>
      <xplan:gehoertZuBereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:schriftinhalt>LB</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>2</xplan:skalierung>
      <xplan:horizontaleAusrichtung>linksbündig</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Basis</xplan:vertikaleAusrichtung>
      <xplan:position>
        <gml:Point srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_1b24f3a7-bb98-4b54-9af3-9a1a58ca59c5">
          <gml:pos>559913.215 5938847.830</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_55CA442A-B21F-45E4-A318-EBDE2176DA22">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559590.659 5938543.016</gml:lowerCorner>
          <gml:upperCorner>559880.545 5939006.240</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_727233E8_E492_44B1_A269_D8D9941452C2</xplan:uuid>
      <xplan:gliederung1>GI</xplan:gliederung1>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>5000</xplan:bezugspunkt>
          <xplan:h uom="urn:adv:uom:m">23</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_d789bb23-aac6-43ae-b6c5-8d8b2024a6fd">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="73">559872.252 5938836.512 559872.758 5938836.326 559880.545
                5938859.354 559875.226 5938870.585 559774.313 5938909.307 559764.183 5938913.662 559757.947 5938917.611
                559750.659 5938923.299 559744.095 5938929.827 559738.225 5938936.857 559733.316 5938944.720 559700.433
                5939006.240 559590.659 5938990.377 559613.743 5938864.114 559614.239 5938861.401 559627.539 5938788.299
                559627.694 5938786.341 559628.192 5938780.041 559628.156 5938771.674 559621.853 5938708.952 559623.694
                5938708.256 559624.264 5938677.757 559624.308 5938666.502 559623.605 5938657.439 559621.443 5938642.192
                559619.124 5938632.860 559614.947 5938623.169 559612.483 5938618.331 559606.617 5938610.300 559602.801
                5938606.327 559597.797 5938596.684 559594.910 5938587.778 559595.014 5938588.019 559595.138 5938588.250
                559595.281 5938588.470 559595.442 5938588.676 559595.621 5938588.868 559595.815 5938589.044 559596.024
                5938589.203 559596.246 5938589.344 559596.478 5938589.465 559596.721 5938589.565 559596.971 5938589.645
                559597.226 5938589.702 559597.486 5938589.738 559597.748 5938589.752 559598.010 5938589.742 559598.271
                5938589.711 559598.528 5938589.657 559598.779 5938589.582 559599.023 5938589.485 559603.161 5938588.366
                559659.917 5938574.508 559682.980 5938569.470 559719.153 5938561.566 559746.355 5938555.618 559798.985
                5938545.966 559805.204 5938545.951 559820.202 5938543.016 559824.773 5938545.908 559827.208 5938563.396
                559828.310 5938571.327 559827.399 5938571.453 559832.702 5938608.967 559833.520 5938608.778 559838.561
                5938644.674 559840.070 5938655.394 559856.699 5938773.503 559857.617 5938780.038 559857.061 5938780.128
                559857.267 5938781.613 559859.806 5938799.972 559872.252 5938836.512
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:BMZ>9</xplan:BMZ>
      <xplan:GRZ>0.8</xplan:GRZ>
      <xplan:allgArtDerBaulNutzung>3000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1800</xplan:besondereArtDerBaulNutzung>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_D09CEDBB-8925-45AB-8B51-27A9DDDE0729">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559925.303 5938494.689</gml:lowerCorner>
          <gml:upperCorner>560127.250 5938543.505</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_C22D2391_2027_40BF_AC2B_59F6E4F5FC86</xplan:uuid>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>5000</xplan:bezugspunkt>
          <xplan:h uom="urn:adv:uom:m">23</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_7d11b86e-5b05-49ab-a865-1c7fbb67893d">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="17">560123.233 5938525.611 560127.250 5938542.193 560116.219
                5938541.346 560009.330 5938543.505 559941.456 5938533.614 559925.303 5938531.260 559934.126 5938529.761
                559976.295 5938521.021 559985.579 5938519.931 559991.324 5938519.448 560107.938 5938494.689 560113.447
                5938497.969 560115.280 5938505.532 560118.133 5938516.054 560120.178 5938520.596 560121.351 5938525.649
                560123.233 5938525.611
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_F7A143F6-D488-47A4-B59D-5A54105DCD25">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559938.760 5938533.614</gml:lowerCorner>
          <gml:upperCorner>560009.330 5938564.697</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_1A000301_7908_47ED_B8EE_9AD101B6C779</xplan:uuid>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>5000</xplan:bezugspunkt>
          <xplan:h uom="urn:adv:uom:m">15.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_fa6e01d2-0e39-4d94-b1b0-c0ef8afe8697">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">560009.330 5938543.505 560008.179 5938564.697 559993.979
                5938563.932 559996.745 5938553.191 559977.734 5938553.370 559977.609 5938556.159 559969.839 5938563.030
                559938.760 5938561.325 559940.146 5938547.995 559941.456 5938533.614 560009.330 5938543.505
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_129A74FB-06C2-4D19-B989-0520DFA44499">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559868.013 5938531.260</gml:lowerCorner>
          <gml:upperCorner>559941.456 5938561.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_97B601C7_358E_475F_9CD7_6F86D4C880CB</xplan:uuid>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>5000</xplan:bezugspunkt>
          <xplan:h uom="urn:adv:uom:m">18</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_f116f273-68dc-4aaf-aa54-9541829f4446">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">559941.456 5938533.614 559940.146 5938547.995 559938.760
                5938561.325 559878.049 5938557.996 559872.928 5938557.625 559868.019 5938556.919 559868.013 5938552.436
                559880.427 5938538.503 559906.215 5938534.502 559925.303 5938531.260 559941.456 5938533.614
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_B709120A-0474-4C43-B87B-0BAD5D4ED643">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559868.019 5938556.919</gml:lowerCorner>
          <gml:upperCorner>559969.839 5938606.119</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_96A0D19F_FDA7_47F0_9CBC_F718B61CE88A</xplan:uuid>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>5000</xplan:bezugspunkt>
          <xplan:h uom="urn:adv:uom:m">23</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_98185f8f-e08a-4280-86e6-c592f942953f">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="12">559969.839 5938563.030 559947.871 5938582.455 559939.403
                5938585.737 559937.262 5938586.728 559909.021 5938597.049 559884.363 5938606.119 559873.725 5938601.105
                559868.034 5938568.371 559868.019 5938556.919 559872.928 5938557.625 559878.049 5938557.996 559969.839
                5938563.030
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_062F366D-A131-4411-9628-F3E41BECBF8E">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>560008.179 5938541.346</gml:lowerCorner>
          <gml:upperCorner>560134.083 5938570.548</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_D7AA1E9B_12FC_4C86_90AE_DBF613553660</xplan:uuid>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>5000</xplan:bezugspunkt>
          <xplan:h uom="urn:adv:uom:m">17</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_5be8c24f-be77-47d6-8bda-eb11a2289c3e">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">560127.250 5938542.193 560134.083 5938570.449 560116.809
                5938570.548 560008.179 5938564.697 560009.330 5938543.505 560116.219 5938541.346 560127.250 5938542.193
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_5DD0B5C6-430A-4A28-9E73-E4D510EB0A9B">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559879.048 5938563.932</gml:lowerCorner>
          <gml:upperCorner>560164.068 5938932.689</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_C8C735B6_A6E6_4AB2_8A76_AD790C381DBF</xplan:uuid>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>5000</xplan:bezugspunkt>
          <xplan:h uom="urn:adv:uom:m">23</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_620d813f-3333-4af1-a074-87b65e574189">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="39">560156.737 5938686.594 560156.727 5938686.595 560161.128
                5938726.738 560161.139 5938726.737 560164.068 5938753.446 560159.199 5938753.967 559956.604 5938931.639
                559942.928 5938932.689 559933.670 5938911.549 559929.244 5938897.215 559928.489 5938897.288 559926.122
                5938872.771 559924.901 5938860.123 559923.688 5938847.548 559923.432 5938838.187 559923.301 5938833.396
                559910.402 5938833.180 559905.017 5938811.196 559886.678 5938736.360 559883.971 5938719.831 559882.982
                5938675.707 559882.710 5938669.732 559881.713 5938661.162 559879.048 5938639.319 559884.235 5938627.562
                559903.866 5938620.480 559946.273 5938605.178 559947.610 5938605.076 559948.951 5938605.062 559950.290
                5938605.135 559980.771 5938607.824 559987.829 5938587.809 559993.979 5938563.932 560008.179 5938564.697
                560116.809 5938570.548 560134.083 5938570.449 560152.257 5938645.761 560152.322 5938646.333 560156.737
                5938686.594
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_0B911BB1-8BB9-4D0D-ABD2-10D6C3BDC455">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559600.563 5938548.533</gml:lowerCorner>
          <gml:upperCorner>559876.850 5939005.317</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_818861D8_393E_40C9_A957_6A68CBA33DA2</xplan:uuid>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_4b38e9c0-c718-417e-bfb2-0056b2f58a7b">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="46">559853.941 5938801.363 559868.554 5938844.264 559869.047
                5938844.083 559876.850 5938867.157 559876.135 5938868.667 559792.327 5938900.685 559747.846 5938917.884
                559746.683 5938918.791 559739.667 5938925.768 559733.354 5938933.329 559728.120 5938941.714 559695.185
                5939003.329 559694.042 5939005.317 559600.563 5938991.808 559623.580 5938865.912 559624.076 5938863.199
                559637.468 5938789.592 559637.663 5938787.130 559638.194 5938780.414 559638.154 5938771.151 559632.571
                5938715.591 559629.350 5938689.081 559627.599 5938667.245 559626.746 5938658.714 559626.332 5938656.805
                559614.771 5938592.818 559640.166 5938588.151 559639.612 5938584.935 559644.871 5938584.031 559644.003
                5938578.987 559680.401 5938572.730 559684.850 5938572.063 559715.724 5938566.686 559719.923 5938565.863
                559723.085 5938565.296 559732.177 5938563.593 559769.988 5938555.424 559810.772 5938548.533 559819.072
                5938549.044 559832.620 5938645.510 559834.129 5938656.231 559850.757 5938774.338 559850.858 5938775.054
                559850.312 5938775.142 559851.324 5938782.435 559853.941 5938801.363
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_4FFC6EF4-9D70-44C2-8E82-6396AF0B43D2">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559580.588 5938990.605</gml:lowerCorner>
          <gml:upperCorner>559694.042 5939085.640</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_18FDDE2D_EF72_4033_9158_F5F2ACADFD58</xplan:uuid>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_eaf2e51d-9331-4769-8369-1fb524eaf897">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="26">559694.042 5939005.317 559680.908 5939028.151 559670.911
                5939035.818 559651.949 5939033.076 559640.599 5939031.433 559631.421 5939025.054 559622.984 5939023.834
                559610.814 5939022.078 559606.957 5939048.790 559617.147 5939050.264 559627.561 5939051.770 559638.166
                5939048.257 559658.795 5939051.242 559657.567 5939058.988 559618.628 5939085.640 559582.716 5939076.961
                559581.712 5939072.790 559581.024 5939068.556 559580.657 5939064.282 559580.588 5939059.953 559580.765
                5939055.628 559582.456 5939046.762 559587.922 5939017.253 559592.238 5938990.605 559600.563 5938991.808
                559694.042 5939005.317
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_A2806AA2-CE11-4DDA-AB1B-4D2383C5D63A">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559629.235 5938893.425</gml:lowerCorner>
          <gml:upperCorner>559910.075 5939144.158</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_0AA2A85F_C556_48BA_93FE_E78549C88396</xplan:uuid>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_82700ccf-6f45-42e3-b2e1-b491cc1bc227">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="37">559901.626 5938980.665 559894.798 5938994.317 559890.840
                5939000.374 559865.437 5939020.453 559839.044 5939041.314 559838.690 5939041.593 559838.416 5939041.810
                559818.826 5939052.503 559816.196 5939047.603 559768.524 5939073.987 559714.241 5939109.711 559680.532
                5939129.740 559659.267 5939144.158 559629.235 5939116.062 559667.622 5939090.381 559673.399 5939086.399
                559678.832 5939081.956 559683.882 5939077.084 559688.516 5939071.815 559692.703 5939066.184 559696.415
                5939060.229 559746.087 5938968.165 559746.607 5938968.433 559753.149 5938956.194 559756.814 5938950.323
                559760.994 5938945.317 559765.832 5938940.506 559771.149 5938936.356 559774.898 5938933.982 559782.941
                5938930.524 559879.626 5938893.425 559889.978 5938898.920 559892.697 5938907.200 559894.167 5938911.539
                559910.075 5938959.857 559905.985 5938971.265 559901.626 5938980.665
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_76DB7973-30CE-4669-B1FD-2DAC4750AAB1">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559861.642 5938489.429</gml:lowerCorner>
          <gml:upperCorner>560164.148 5938942.207</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_015DF7FA_3F97_4870_8DC5_FAE801428908</xplan:uuid>
      <xplan:gliederung1>GI</xplan:gliederung1>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_fe351672-8687-478e-9327-83b7cedd9c51">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="91">560164.148 5938754.176 560161.745 5938758.449 560126.435
                5938788.911 560077.618 5938831.028 560071.969 5938835.927 560065.909 5938841.792 560057.985 5938849.582
                560047.989 5938858.886 560041.614 5938864.092 560036.609 5938868.050 560032.009 5938872.013 560009.542
                5938893.089 560004.099 5938898.195 559999.545 5938901.595 559976.207 5938922.317 559972.907 5938925.967
                559972.137 5938926.820 559959.631 5938936.759 559951.683 5938941.424 559940.422 5938942.207 559931.054
                5938919.829 559917.186 5938879.015 559912.771 5938863.580 559924.901 5938860.123 559923.688 5938847.548
                559923.432 5938838.187 559917.072 5938838.378 559906.457 5938841.511 559905.129 5938836.872 559899.189
                5938812.624 559880.795 5938737.561 559877.982 5938720.386 559876.985 5938675.910 559876.726 5938670.216
                559875.755 5938661.871 559872.819 5938637.809 559876.638 5938628.934 559886.184 5938621.543 559902.169
                5938615.777 559946.702 5938599.708 559946.667 5938600.194 559948.062 5938600.077 559949.462 5938600.068
                559950.858 5938600.167 559965.637 5938601.804 559966.778 5938601.883 559967.921 5938601.869 559969.059
                5938601.762 559970.185 5938601.562 559971.290 5938601.271 559972.369 5938600.891 559973.412 5938600.424
                559974.414 5938599.874 559975.368 5938599.243 559976.245 5938598.554 559977.065 5938597.798 559977.823
                5938596.979 559978.512 5938596.103 559979.130 5938595.174 559979.672 5938594.199 559980.135 5938593.185
                559980.516 5938592.136 559982.473 5938586.138 559990.142 5938558.327 559981.147 5938558.511 559944.344
                5938588.416 559941.881 5938590.099 559939.174 5938591.353 559910.737 5938601.745 559882.564 5938612.039
                559871.951 5938610.079 559867.077 5938603.605 559861.783 5938568.928 559861.642 5938547.382 559877.220
                5938534.747 559906.266 5938528.264 559933.564 5938522.911 559980.728 5938513.760 559986.050 5938515.347
                559992.014 5938515.384 560108.639 5938489.429 560117.745 5938494.475 560120.178 5938504.512 560122.666
                5938513.667 560126.606 5938518.304 560128.352 5938525.506 560135.584 5938555.356 560135.966 5938556.939
                560147.993 5938606.778 560151.043 5938634.666 560164.148 5938754.176
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:BMZ>9</xplan:BMZ>
      <xplan:GRZ>0.8</xplan:GRZ>
      <xplan:allgArtDerBaulNutzung>3000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1800</xplan:besondereArtDerBaulNutzung>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_EBB01753-E69E-4C28-87E8-67984CDBAF26">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559578.455 5938990.377</gml:lowerCorner>
          <gml:upperCorner>559700.433 5939087.933</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_AB2296D4_501E_4447_B47E_3617A5EBE80C</xplan:uuid>
      <xplan:gliederung1>GE</xplan:gliederung1>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_bc7f4534-dcad-4d31-8848-97f988f169b4">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="25">559619.459 5939087.933 559611.974 5939086.101 559581.700
                5939078.694 559579.772 5939072.640 559578.684 5939066.369 559578.455 5939060.023 559579.097 5939053.636
                559580.407 5939046.466 559585.803 5939016.947 559590.659 5938990.377 559700.433 5939006.240 559685.511
                5939032.182 559675.327 5939039.993 559651.448 5939036.540 559639.280 5939034.779 559630.102 5939028.400
                559622.484 5939027.298 559616.252 5939026.399 559613.395 5939046.185 559617.648 5939046.800 559627.245
                5939048.188 559637.850 5939044.675 559667.296 5939048.936 559670.663 5939052.611 559619.459 5939087.933
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GFZ>2.4</xplan:GFZ>
      <xplan:GRZ>0.8</xplan:GRZ>
      <xplan:Z>6</xplan:Z>
      <xplan:allgArtDerBaulNutzung>3000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1700</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_37C4F6F5-92D6-4282-82DB-C05148A9A952">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559619.512 5938886.887</gml:lowerCorner>
          <gml:upperCorner>559912.018 5939153.149</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_D9AFFD5D_0A21_40B9_8F1C_46521DADD7B9</xplan:uuid>
      <xplan:gliederung1>GI</xplan:gliederung1>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>5000</xplan:bezugspunkt>
          <xplan:h uom="urn:adv:uom:m">23</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_93c083c0-76d8-45c4-b970-4bb785956c49">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="40">559839.044 5939041.314 559839.254 5939041.581 559838.901
                5939041.860 559838.690 5939041.593 559838.416 5939041.810 559816.294 5939053.885 559794.129 5939065.231
                559784.539 5939070.852 559775.939 5939075.892 559757.879 5939088.114 559758.267 5939090.693 559680.323
                5939138.430 559659.259 5939153.149 559619.512 5939115.348 559664.354 5939085.348 559669.733 5939081.649
                559674.790 5939077.521 559679.493 5939072.993 559683.808 5939068.094 559687.708 5939062.859 559691.166
                5939057.322 559743.601 5938960.137 559744.101 5938960.394 559747.953 5938953.188 559751.944 5938946.795
                559756.566 5938941.259 559761.857 5938935.997 559767.690 5938931.445 559772.091 5938928.658 559780.680
                5938924.965 559879.915 5938886.887 559891.768 5938892.933 559911.433 5938951.803 559912.018 5938954.440
                559905.985 5938971.265 559901.626 5938980.665 559894.798 5938994.317 559890.840 5939000.374 559865.437
                5939020.453 559839.044 5939041.314
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:BMZ>9</xplan:BMZ>
      <xplan:GRZ>0.8</xplan:GRZ>
      <xplan:allgArtDerBaulNutzung>3000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1800</xplan:besondereArtDerBaulNutzung>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinbedarfsFlaeche gml:id="GML_AA3B905F-9877-4A47-A9E5-F26EC96F6477">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559906.457 5938838.187</gml:lowerCorner>
          <gml:upperCorner>559924.901 5938863.580</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_5445D932_C3B4_42D6_88AA_02225DCC52BD</xplan:uuid>
      <xplan:text>Löschwasserbecken</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_88b5033b-f1fc-430f-a8b0-ebb620412d4f">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">559924.901 5938860.123 559912.771 5938863.580 559906.457
                5938841.511 559917.072 5938838.378 559923.432 5938838.187 559923.688 5938847.548 559924.901 5938860.123
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
    </xplan:BP_GemeinbedarfsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_2E9152E3-2213-4D0B-94A5-797234DAA69D">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559583.149 5938527.218</gml:lowerCorner>
          <gml:upperCorner>559990.142 5939115.348</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_00517807_F435_4B1C_8184_FAA2F63E8844</xplan:uuid>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_a544687b-9a02-4f67-bb9e-33f14e6ffa12">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="168">559906.457 5938841.511 559912.771 5938863.580 559917.186
                5938879.015 559931.054 5938919.829 559940.422 5938942.207 559931.803 5938977.053 559929.832 5938978.808
                559927.087 5938981.252 559935.814 5939000.612 559931.162 5939004.098 559923.920 5938988.526 559919.351
                5938977.839 559917.839 5938974.169 559914.449 5938964.365 559913.205 5938959.795 559912.018 5938954.440
                559911.433 5938951.803 559891.768 5938892.933 559879.915 5938886.887 559780.680 5938924.965 559772.091
                5938928.658 559767.690 5938931.445 559761.857 5938935.997 559756.566 5938941.259 559751.944 5938946.795
                559747.953 5938953.188 559744.101 5938960.394 559743.601 5938960.137 559691.166 5939057.322 559687.708
                5939062.859 559683.808 5939068.094 559679.493 5939072.993 559674.790 5939077.521 559669.733 5939081.649
                559664.354 5939085.348 559619.512 5939115.348 559616.839 5939112.806 559617.074 5939107.548 559617.518
                5939107.548 559619.053 5939096.806 559620.127 5939096.065 559623.553 5939093.795 559624.284 5939093.269
                559625.871 5939092.143 559666.599 5939063.988 559667.289 5939061.596 559667.904 5939059.410 559670.663
                5939052.611 559667.296 5939048.936 559637.850 5939044.675 559627.245 5939048.188 559617.648 5939046.800
                559613.395 5939046.185 559616.252 5939026.399 559622.484 5939027.298 559630.102 5939028.400 559639.280
                5939034.779 559651.448 5939036.540 559675.327 5939039.993 559685.511 5939032.182 559700.433 5939006.240
                559733.316 5938944.720 559738.225 5938936.857 559744.095 5938929.827 559750.659 5938923.299 559757.947
                5938917.611 559764.183 5938913.662 559774.313 5938909.307 559875.226 5938870.585 559880.545 5938859.354
                559872.758 5938836.326 559872.252 5938836.512 559859.806 5938799.972 559857.267 5938781.613 559857.061
                5938780.128 559857.617 5938780.038 559856.699 5938773.503 559840.070 5938655.394 559838.561 5938644.674
                559833.520 5938608.778 559832.702 5938608.967 559827.399 5938571.453 559828.310 5938571.327 559827.208
                5938563.396 559824.773 5938545.908 559820.202 5938543.016 559805.204 5938545.951 559798.985 5938545.966
                559746.355 5938555.618 559719.153 5938561.566 559682.980 5938569.470 559660.274 5938574.430 559603.161
                5938588.366 559599.023 5938589.485 559598.779 5938589.582 559598.528 5938589.657 559598.271 5938589.711
                559598.010 5938589.742 559597.748 5938589.752 559597.486 5938589.738 559597.226 5938589.702 559596.971
                5938589.645 559596.721 5938589.565 559596.478 5938589.465 559596.246 5938589.344 559596.024 5938589.203
                559595.815 5938589.044 559595.621 5938588.868 559595.442 5938588.676 559595.281 5938588.470 559595.138
                5938588.250 559595.014 5938588.019 559594.910 5938587.778 559594.815 5938587.485 559587.584 5938589.472
                559583.278 5938573.082 559583.149 5938571.755 559706.528 5938556.561 559735.128 5938550.929 559775.580
                5938543.091 559857.747 5938527.218 559861.642 5938547.382 559861.783 5938568.928 559867.077 5938603.605
                559871.951 5938610.079 559882.564 5938612.039 559910.737 5938601.745 559939.174 5938591.353 559941.881
                5938590.099 559944.344 5938588.416 559981.147 5938558.511 559990.142 5938558.327 559982.473 5938586.138
                559980.516 5938592.136 559980.135 5938593.185 559979.672 5938594.199 559979.130 5938595.174 559978.512
                5938596.103 559977.823 5938596.979 559977.065 5938597.798 559976.245 5938598.554 559975.368 5938599.243
                559974.414 5938599.874 559973.412 5938600.424 559972.369 5938600.891 559971.290 5938601.271 559970.185
                5938601.562 559969.059 5938601.762 559967.921 5938601.869 559966.778 5938601.883 559965.637 5938601.804
                559950.858 5938600.167 559949.462 5938600.068 559948.062 5938600.077 559946.667 5938600.194 559946.702
                5938599.708 559902.169 5938615.777 559886.184 5938621.543 559876.638 5938628.934 559872.819 5938637.809
                559875.755 5938661.871 559876.726 5938670.216 559876.985 5938675.910 559877.982 5938720.386 559880.795
                5938737.561 559899.189 5938812.624 559905.129 5938836.872 559906.457 5938841.511
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_270DAD4F-493B-4DA4-B4DE-63531664DB7E">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559857.747 5938465.032</gml:lowerCorner>
          <gml:upperCorner>560174.871 5938977.053</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_AD0A6608_9CBC_4490_9687_A5830DB07259</xplan:uuid>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:Polygon srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_9f4ba909-56c3-463c-9422-d69c17aa5ebf">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="83">559857.747 5938527.218 559909.797 5938517.162 559918.660
                5938515.314 559925.431 5938513.743 559939.317 5938510.055 559949.989 5938507.022 559970.985 5938500.137
                559980.737 5938496.974 559988.476 5938494.141 559997.949 5938490.930 560011.170 5938487.024 560024.244
                5938483.599 560048.366 5938478.727 560066.776 5938475.199 560120.345 5938465.032 560132.443 5938513.348
                560136.443 5938529.882 560143.057 5938557.079 560155.365 5938606.254 560160.428 5938627.258 560166.474
                5938682.082 560167.786 5938696.283 560174.485 5938757.619 560174.871 5938763.042 560174.231 5938763.599
                560165.753 5938770.895 560099.434 5938826.609 560097.913 5938827.897 560096.424 5938829.158 560094.891
                5938830.455 560077.715 5938844.888 560064.799 5938856.511 560059.460 5938861.538 560032.472 5938889.745
                560031.485 5938890.791 560018.978 5938902.489 559982.030 5938935.989 559961.754 5938953.049 559949.718
                5938962.911 559944.324 5938967.387 559943.828 5938966.784 559943.185 5938967.311 559943.325 5938967.481
                559942.071 5938968.516 559931.803 5938977.053 559940.422 5938942.207 559951.683 5938941.424 559959.631
                5938936.759 559972.137 5938926.820 559972.907 5938925.967 559976.207 5938922.317 559999.545 5938901.595
                560004.099 5938898.195 560009.542 5938893.089 560032.009 5938872.013 560036.609 5938868.050 560041.614
                5938864.092 560047.989 5938858.886 560057.985 5938849.582 560065.909 5938841.792 560071.969 5938835.927
                560077.618 5938831.028 560126.435 5938788.911 560161.745 5938758.449 560164.148 5938754.176 560151.043
                5938634.666 560147.993 5938606.778 560135.966 5938556.939 560135.584 5938555.356 560128.352 5938525.506
                560126.606 5938518.304 560122.666 5938513.667 560120.178 5938504.512 560117.745 5938494.475 560108.639
                5938489.429 559992.014 5938515.384 559986.050 5938515.347 559980.728 5938513.760 559933.564 5938522.911
                559906.266 5938528.264 559877.220 5938534.747 559861.642 5938547.382 559857.747 5938527.218
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_0633B265-59B6-4442-9376-70DB9F098C40">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559590.659 5938990.377</gml:lowerCorner>
          <gml:upperCorner>559700.433 5939006.240</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_EC9B1870_A04D_4B02_B1D0_314F2563A393</xplan:uuid>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:LineString srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_0d1f87d2-8856-447f-a272-1b618875d2e2">
          <gml:posList srsDimension="2" count="2">559590.659 5938990.377 559700.433 5939006.240 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_2D74790B-B74F-45F1-BD3E-7A6C52401131">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559580.588 5938548.533</gml:lowerCorner>
          <gml:upperCorner>559876.850 5939085.640</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_0463BF81_B105_4D38_BB09_7ABD46942750</xplan:uuid>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:LineString srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_a71023e0-ba0d-4029-8dfb-6b0681123011">
          <gml:posList srsDimension="2" count="68">559600.563 5938991.809 559592.238 5938990.605 559587.922
            5939017.253 559582.456 5939046.762 559580.765 5939055.628 559580.588 5939059.953 559580.657 5939064.282
            559581.024 5939068.556 559581.712 5939072.790 559582.716 5939076.961 559618.628 5939085.640 559657.567
            5939058.988 559658.795 5939051.242 559638.166 5939048.257 559627.561 5939051.770 559617.147 5939050.264
            559606.957 5939048.790 559610.814 5939022.078 559622.984 5939023.834 559631.421 5939025.054 559640.599
            5939031.433 559651.949 5939033.076 559670.911 5939035.818 559680.908 5939028.151 559695.185 5939003.329
            559728.120 5938941.714 559733.354 5938933.329 559739.667 5938925.768 559746.683 5938918.791 559747.846
            5938917.884 559792.327 5938900.685 559876.135 5938868.667 559876.850 5938867.157 559869.047 5938844.083
            559868.554 5938844.264 559853.941 5938801.363 559851.324 5938782.435 559850.312 5938775.142 559850.858
            5938775.054 559850.757 5938774.338 559834.129 5938656.231 559832.620 5938645.510 559819.072 5938549.044
            559810.772 5938548.533 559769.988 5938555.424 559732.177 5938563.593 559723.085 5938565.296 559719.923
            5938565.863 559715.724 5938566.686 559684.850 5938572.063 559680.401 5938572.730 559644.003 5938578.987
            559644.871 5938584.031 559639.612 5938584.935 559640.166 5938588.151 559614.771 5938592.818 559626.332
            5938656.805 559626.746 5938658.714 559627.599 5938667.245 559629.350 5938689.081 559632.571 5938715.591
            559638.154 5938771.151 559638.194 5938780.414 559637.663 5938787.130 559637.468 5938789.592 559624.076
            5938863.199 559623.580 5938865.912 559600.563 5938991.808
          </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="urn:adv:uom:m">0</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_221C4C13-65F2-4184-883A-551442542452">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559812.133 5938536.030</gml:lowerCorner>
          <gml:upperCorner>560143.057 5938557.079</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_178C7EED_50B5_4510_8A42_4A2C01433723</xplan:uuid>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:LineString srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_d2ebf46b-5219-4bf8-b9e2-94493cf72849">
          <gml:posList srsDimension="2" count="3">560143.057 5938557.079 560009.075 5938554.438 559812.133
            5938536.030
          </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:besondereZweckbestimmung>10000</xplan:besondereZweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_3CE5E8DD-8CD9-447D-96F6-E637D5B4E182">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559861.642 5938489.429</gml:lowerCorner>
          <gml:upperCorner>560164.148 5938942.207</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_7879B11D_F5B7_406D_90BF_D517B53DCAA8</xplan:uuid>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:LineString srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_94c45d2a-2618-43ee-b906-6c408b6e05a1">
          <gml:posList srsDimension="2" count="87">559912.771 5938863.580 559917.186 5938879.015 559931.054
            5938919.829 559940.422 5938942.207 559951.683 5938941.424 559959.631 5938936.759 559972.137 5938926.820
            559972.907 5938925.967 559976.207 5938922.317 559999.545 5938901.595 560004.099 5938898.195 560009.542
            5938893.089 560032.009 5938872.013 560036.609 5938868.050 560041.614 5938864.092 560047.989 5938858.886
            560057.985 5938849.582 560065.909 5938841.792 560071.969 5938835.927 560077.618 5938831.028 560126.435
            5938788.911 560161.745 5938758.449 560164.148 5938754.176 560151.043 5938634.666 560147.993 5938606.778
            560135.966 5938556.939 560135.584 5938555.356 560128.352 5938525.506 560126.606 5938518.304 560122.666
            5938513.667 560120.178 5938504.512 560117.745 5938494.475 560108.639 5938489.429 559992.014 5938515.384
            559986.050 5938515.347 559980.728 5938513.760 559933.564 5938522.911 559906.266 5938528.264 559877.220
            5938534.747 559861.642 5938547.382 559861.783 5938568.928 559867.077 5938603.605 559871.951 5938610.079
            559882.564 5938612.039 559910.737 5938601.745 559939.174 5938591.353 559941.881 5938590.099 559944.344
            5938588.416 559981.147 5938558.511 559990.142 5938558.327 559982.473 5938586.138 559980.457 5938592.317
            559980.071 5938593.338 559979.607 5938594.326 559979.068 5938595.275 559978.456 5938596.180 559977.776
            5938597.033 559977.032 5938597.832 559976.227 5938598.570 559975.368 5938599.243 559974.414 5938599.874
            559973.412 5938600.424 559972.369 5938600.891 559971.290 5938601.271 559970.185 5938601.562 559969.059
            5938601.762 559967.921 5938601.869 559966.778 5938601.883 559965.637 5938601.804 559950.858 5938600.167
            559949.462 5938600.068 559948.062 5938600.077 559946.667 5938600.194 559946.702 5938599.708 559902.169
            5938615.777 559886.184 5938621.543 559876.638 5938628.934 559872.819 5938637.809 559875.755 5938661.871
            559876.726 5938670.216 559876.985 5938675.910 559877.982 5938720.386 559880.795 5938737.561 559899.189
            5938812.624 559905.129 5938836.872 559906.457 5938841.511 559912.771 5938863.580
          </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_9E3659B1-CBC6-4F3D-BC45-B5A7E0D3E134">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559587.584 5938543.016</gml:lowerCorner>
          <gml:upperCorner>559880.545 5939096.806</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_FD98E825_8937_4ABF_860C_A750A669EC11</xplan:uuid>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:LineString srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_28c1ac77-9c02-4555-aec4-e5cec60c71d6">
          <gml:posList srsDimension="2" count="76">559587.584 5938589.472 559594.815 5938587.485 559594.895
            5938587.737 559594.996 5938587.982 559595.118 5938588.216 559595.260 5938588.440 559595.420 5938588.650
            559595.598 5938588.846 559595.793 5938589.025 559596.002 5938589.188 559596.224 5938589.331 559596.458
            5938589.455 559596.701 5938589.558 559596.953 5938589.640 559597.211 5938589.700 559597.473 5938589.737
            559597.737 5938589.751 559598.001 5938589.743 559598.264 5938589.712 559598.523 5938589.658 559598.777
            5938589.583 559599.023 5938589.485 559602.995 5938588.411 559659.917 5938574.508 559682.980 5938569.470
            559719.153 5938561.566 559746.355 5938555.618 559798.985 5938545.966 559805.204 5938545.951 559820.202
            5938543.016 559824.773 5938545.908 559827.208 5938563.396 559828.310 5938571.327 559827.399 5938571.453
            559832.702 5938608.967 559833.520 5938608.778 559838.561 5938644.674 559840.070 5938655.394 559856.699
            5938773.503 559857.617 5938780.038 559857.061 5938780.128 559857.267 5938781.613 559859.806 5938799.972
            559872.252 5938836.512 559872.758 5938836.326 559880.545 5938859.354 559875.226 5938870.585 559774.313
            5938909.307 559764.183 5938913.662 559757.947 5938917.611 559750.659 5938923.299 559744.095 5938929.827
            559738.225 5938936.857 559733.316 5938944.720 559700.433 5939006.240 559685.511 5939032.182 559675.327
            5939039.993 559651.448 5939036.540 559639.280 5939034.779 559630.102 5939028.400 559622.484 5939027.298
            559616.252 5939026.399 559613.395 5939046.185 559617.648 5939046.800 559627.245 5939048.188 559637.850
            5939044.675 559667.296 5939048.936 559670.663 5939052.611 559667.904 5939059.410 559667.289 5939061.596
            559666.599 5939063.988 559625.871 5939092.143 559624.284 5939093.269 559623.553 5939093.795 559620.127
            5939096.065 559619.053 5939096.806
          </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_439A7922-255C-48B9-8BF8-F37843A359FA">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559619.512 5938886.887</gml:lowerCorner>
          <gml:upperCorner>559931.162 5939115.348</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_512B83FF_6C12_422D_875B_95972470B390</xplan:uuid>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:LineString srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_3a588cec-017c-4306-a5ad-a1eebab20fac">
          <gml:posList srsDimension="2" count="26">559931.162 5939004.098 559923.920 5938988.526 559919.351
            5938977.839 559917.839 5938974.169 559914.449 5938964.365 559913.205 5938959.795 559911.433 5938951.803
            559891.768 5938892.933 559879.915 5938886.887 559780.680 5938924.965 559772.091 5938928.658 559767.690
            5938931.445 559761.857 5938935.997 559756.566 5938941.259 559751.944 5938946.795 559747.953 5938953.188
            559744.101 5938960.394 559743.601 5938960.137 559691.166 5939057.322 559687.708 5939062.859 559683.808
            5939068.094 559679.493 5939072.993 559674.790 5939077.521 559669.733 5939081.649 559664.354 5939085.348
            559619.512 5939115.348
          </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_1A0B420B-C96D-445B-880F-80CA3E609096">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559925.303 5938494.689</gml:lowerCorner>
          <gml:upperCorner>560127.250 5938543.505</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_637616CB_B8B0_4401_946F_587A10CEB31C</xplan:uuid>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:LineString srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_878d23ed-bb82-4621-b883-ad50eb188f79">
          <gml:posList srsDimension="2" count="17">560120.178 5938520.596 560118.133 5938516.054 560115.280
            5938505.532 560113.447 5938497.969 560107.938 5938494.689 559991.324 5938519.448 559985.579 5938519.931
            559976.295 5938521.021 559934.126 5938529.761 559925.303 5938531.260 559941.456 5938533.614 560009.330
            5938543.505 560116.219 5938541.346 560127.250 5938542.193 560123.233 5938525.611 560121.351 5938525.649
            560120.178 5938520.596
          </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="urn:adv:uom:m">0</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_02D24813-9C98-4279-B4DD-AD7FE05B40BD">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559868.013 5938531.260</gml:lowerCorner>
          <gml:upperCorner>559941.456 5938561.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_20CFC33D_43F2_4426_BC23_93B25413728C</xplan:uuid>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:LineString srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_bfe9aa11-ad82-40cf-a90a-65a290a50233">
          <gml:posList srsDimension="2" count="11">559941.456 5938533.614 559925.303 5938531.260 559906.215
            5938534.502 559880.427 5938538.503 559868.013 5938552.436 559868.019 5938556.919 559872.928 5938557.625
            559878.049 5938557.996 559938.760 5938561.325 559940.146 5938547.995 559941.456 5938533.614
          </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="urn:adv:uom:m">0</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_95FE9C78-BCF7-4504-83B8-F46C6C0A6FDA">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>560008.179 5938541.346</gml:lowerCorner>
          <gml:upperCorner>560134.083 5938570.548</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_F0403D6E_4390_46C9_A096_A7F171231F3C</xplan:uuid>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:LineString srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_e974c2b9-29ea-4522-bd74-6f6c84bbe625">
          <gml:posList srsDimension="2" count="7">560127.250 5938542.193 560116.219 5938541.346 560009.330
            5938543.505 560008.179 5938564.697 560116.809 5938570.548 560134.083 5938570.449 560127.250 5938542.193
          </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="urn:adv:uom:m">0</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_5A0F778F-8E15-4616-9FDD-EBB068AFD704">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559938.760 5938533.614</gml:lowerCorner>
          <gml:upperCorner>560009.330 5938564.697</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_0956CE2B_8949_42A9_9A8E_E1CFF87018B8</xplan:uuid>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:LineString srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_248650f7-2713-4e54-b16a-9d48beab25b6">
          <gml:posList srsDimension="2" count="11">560009.330 5938543.505 559941.456 5938533.614 559940.146
            5938547.995 559938.760 5938561.325 559969.839 5938563.030 559977.609 5938556.159 559977.734 5938553.370
            559996.745 5938553.191 559993.979 5938563.932 560008.179 5938564.697 560009.330 5938543.505
          </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="urn:adv:uom:m">0</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_AE52C847-92B6-40EE-9C3E-2FF8A8E93396">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559868.019 5938556.919</gml:lowerCorner>
          <gml:upperCorner>559969.839 5938606.119</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_6D3F0D8F_992C_4FB0_A5A7_C5E7E791032D</xplan:uuid>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:LineString srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_8c49ec36-409a-40b5-92bf-dc17e5b2b87c">
          <gml:posList srsDimension="2" count="12">559969.839 5938563.030 559878.049 5938557.996 559872.928
            5938557.625 559868.019 5938556.919 559868.034 5938568.371 559873.725 5938601.105 559884.363 5938606.119
            559909.021 5938597.049 559937.262 5938586.728 559939.403 5938585.737 559947.871 5938582.455 559969.839
            5938563.030
          </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="urn:adv:uom:m">0</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_D014345B-9A71-4F2F-9135-70350A03A501">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559879.048 5938563.932</gml:lowerCorner>
          <gml:upperCorner>560164.068 5938932.689</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_6E9303E5_DC75_438F_8AD3_BFA26980048D</xplan:uuid>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:LineString srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_407bb54f-3094-4890-9f10-3c65a2bff811">
          <gml:posList srsDimension="2" count="37">560134.083 5938570.449 560116.809 5938570.548 560008.179
            5938564.697 559993.979 5938563.932 559987.829 5938587.809 559980.771 5938607.824 559950.290 5938605.135
            559948.951 5938605.062 559947.610 5938605.076 559946.273 5938605.178 559903.866 5938620.480 559884.235
            5938627.562 559879.048 5938639.319 559881.713 5938661.162 559882.710 5938669.732 559882.982 5938675.707
            559883.971 5938719.831 559886.678 5938736.360 559905.017 5938811.196 559910.402 5938833.180 559923.301
            5938833.396 559923.688 5938847.548 559924.901 5938860.123 559926.122 5938872.771 559928.489 5938897.288
            559929.244 5938897.215 559933.670 5938911.549 559942.928 5938932.689 559956.604 5938931.639 560159.199
            5938753.967 560164.068 5938753.446 560161.128 5938726.738 560156.727 5938686.595 560153.475 5938656.845
            560152.399 5938646.997 560152.257 5938645.761 560134.083 5938570.449
          </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="urn:adv:uom:m">0</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_E145EBFE-8CDE-4A10-AB78-8379E7EB1797">
      <gml:boundedBy>
        <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32">
          <gml:lowerCorner>559629.235 5938893.425</gml:lowerCorner>
          <gml:upperCorner>559910.075 5939144.158</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>EAWS_PZIID_EC0A67E6_A7FC_4F32_849B_7A02223E95D8</xplan:uuid>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_1C210013-14A8-491F-AD56-024BDFE3F111" />
      <xplan:position>
        <gml:LineString srsName="urn:adv:crs:ETRS89_UTM32" gml:id="GML_1680e6b1-bc1e-4500-a0a0-b5a50f4e7f6a">
          <gml:posList srsDimension="2" count="37">559629.235 5939116.062 559659.267 5939144.158 559680.532
            5939129.740 559714.241 5939109.711 559768.524 5939073.987 559816.196 5939047.603 559818.826 5939052.503
            559838.416 5939041.810 559838.690 5939041.593 559839.044 5939041.314 559865.437 5939020.453 559890.840
            5939000.374 559894.798 5938994.317 559901.626 5938980.665 559905.985 5938971.265 559910.075 5938959.857
            559894.167 5938911.539 559892.697 5938907.200 559889.978 5938898.920 559879.626 5938893.425 559782.941
            5938930.524 559774.898 5938933.982 559771.149 5938936.356 559765.832 5938940.506 559760.994 5938945.317
            559756.814 5938950.323 559753.149 5938956.194 559746.607 5938968.433 559746.087 5938968.165 559696.415
            5939060.229 559692.703 5939066.184 559688.516 5939071.815 559683.882 5939077.084 559678.832 5939081.956
            559673.399 5939086.399 559667.622 5939090.381 559629.235 5939116.062
          </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="urn:adv:uom:m">0</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
</XPlanAuszug>
