/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.storage.s3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import de.latlon.xplan.commons.s3.StorageException;
import de.latlon.xplan.manager.storage.StorageEvent;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectResponse;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.ListObjectsRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsResponse;
import software.amazon.awssdk.services.s3.model.S3Object;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
class S3StorageCleanUpManagerTest {

	private static final String BUCKET_NAME = "xplanbox";

	@Test
	void deleteRasterFiles() throws StorageException {
		S3Client client = spy(S3Client.class);
		ListObjectsResponse objectListing = mock(ListObjectsResponse.class);
		doReturn(objectListing).when(client)
			.listObjects(eq(ListObjectsRequest.builder().bucket(BUCKET_NAME).prefix("1_").build()));
		ResponseInputStream<GetObjectResponse> objectStream = mock(ResponseInputStream.class);
		GetObjectResponse getObjectResponse = mock(GetObjectResponse.class);
		when(getObjectResponse.contentType()).thenReturn("image/png");
		when(getObjectResponse.contentLength()).thenReturn(90l);
		when(objectStream.response()).thenReturn(getObjectResponse);
		doReturn(objectStream).when(client).getObject(any(GetObjectRequest.class));
		doReturn(DeleteObjectResponse.builder().build()).when(client).deleteObject(any(DeleteObjectRequest.class));

		S3Object objectToDelete = mock(S3Object.class);
		when(objectToDelete.key()).thenReturn("1_test.png");
		List<S3Object> objectSummaries = Collections.singletonList(objectToDelete);
		when(objectListing.contents()).thenReturn(objectSummaries);
		S3StorageCleanUpManager s3RasterStorage = new S3StorageCleanUpManager(client, BUCKET_NAME);

		StorageEvent storageEvent = mock(StorageEvent.class);
		s3RasterStorage.deleteAll("1", storageEvent);

		verify(client).listObjects(ListObjectsRequest.builder().bucket(BUCKET_NAME).prefix("1_").build());
		verify(client).deleteObject(DeleteObjectRequest.builder().bucket(BUCKET_NAME).key("1_test.png").build());
		ArgumentCaptor<de.latlon.xplan.commons.s3.S3Object> argument = ArgumentCaptor
			.forClass(de.latlon.xplan.commons.s3.S3Object.class);
		verify(storageEvent).addDeletedKey(argument.capture());
		assertEquals("1_test.png", argument.getValue().getS3Metadata().getKey());
	}

}
