/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.workspace;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Tests for {@link WorkspaceReloader}.
 *
 * @author <a href="mailto:stenger@lat-lon.de">Dirk Stenger</a>
 * @version $Revision: $, $Date: $
 */
class WorkspaceReloaderTest {

	@Test
	void reloadWorkspaceWithInvalidConfigurationShouldFail() {
		WorkspaceReloaderConfiguration configuration = new WorkspaceReloaderConfiguration();
		WorkspaceReloader workspaceReloader = new WorkspaceReloader(configuration);
		boolean isReloadSuccessful = workspaceReloader.reloadWorkspace(1);

		assertFalse(isReloadSuccessful);
	}

	@Test
	void reloadWorkspaceWithInvalidUrlShouldFail() {
		List<String> urlList = singletonList("http://invalid-url");
		WorkspaceReloaderConfiguration configuration = new WorkspaceReloaderConfiguration(urlList, null, "user",
				"password");
		WorkspaceReloader workspaceReloader = new WorkspaceReloader(configuration);
		boolean isReloadSuccessful = workspaceReloader.reloadWorkspace(1);

		assertFalse(isReloadSuccessful);
	}

	@Test
	void reloadWorkspaceWithTwoInvalidUrlsShouldFail() {
		List<String> urlList = asList("http://invalid-url1", "http://invalid-url2");
		WorkspaceReloaderConfiguration configuration = new WorkspaceReloaderConfiguration(urlList, null, "user",
				"password");
		WorkspaceReloader workspaceReloader = new WorkspaceReloader(configuration);
		boolean isReloadSuccessful = workspaceReloader.reloadWorkspace(1);

		assertFalse(isReloadSuccessful);
	}

	@Test
	void reloadWorkspaceWithThreeInvalidUrlsShouldFail() {
		List<String> urlList = asList("http://invalid-url1", "http://invalid-url2", "http://invalid-url3");
		WorkspaceReloaderConfiguration configuration = new WorkspaceReloaderConfiguration(urlList, "apiKey", "user",
				"password");
		WorkspaceReloader workspaceReloader = new WorkspaceReloader(configuration);
		boolean isReloadSuccessful = workspaceReloader.reloadWorkspace(1);

		assertFalse(isReloadSuccessful);
	}

}
