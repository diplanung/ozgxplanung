/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager;

import static org.apache.commons.io.IOUtils.close;
import static org.apache.commons.io.IOUtils.copy;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;

import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.commons.configuration.SortConfiguration;
import de.latlon.xplan.manager.configuration.ManagerConfiguration;
import de.latlon.xplan.manager.database.XPlanDao;
import de.latlon.xplan.manager.web.shared.PlanStatus;
import de.latlon.xplan.manager.web.shared.RasterEvaluationResult;
import de.latlon.xplan.manager.web.shared.Rechtsstand;
import de.latlon.xplanbox.core.raster.evaluation.RasterEvaluation;
import de.latlon.xplanbox.core.raster.evaluation.XPlanRasterEvaluation;
import de.latlon.xplanbox.core.raster.evaluation.XPlanRasterEvaluator;
import org.deegree.commons.utils.Pair;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version 1.0
 */
// TODO enable Sprint TestContext and turn all mock objects created here into Spring
// Beans.
// @RunWith(SpringRunner.class)
// @ContextConfiguration(classes = {CoreTestContext.class, RasterStorageContext.class})
public class XPlanManagerTest {

	@TempDir
	public File tempFolder;

	private File managerWorkspaceDirectory;

	@BeforeEach
	void createWorkspaceFiles() throws IOException {
		managerWorkspaceDirectory = newFolder(tempFolder, "manager");
		File themesDir = new File(managerWorkspaceDirectory, "themes");
		Files.createDirectory(themesDir.toPath());
		File file = new File(themesDir, "bplanraster.xml");
		file.createNewFile();
		// TODO build minimal temp workspace
	}

	@AfterEach
	void deleteWorkspace() {
		managerWorkspaceDirectory.delete();
	}

	@Test
	void testEvaluateRasterdata() throws Exception {
		XPlanManager xPlanManager = createXPlanManager();
		String pathToArchive = copyPlan();

		List<RasterEvaluationResult> results = xPlanManager.evaluateRasterdata(pathToArchive);

		assertThat(results).hasSize(1);
		RasterEvaluationResult result = results.get(0);

		assertFalse(result.isCrsSet());
		assertFalse(result.isConfiguredCrs());
	}

	@Test
	void determineLegislationStatus() throws Exception {
		XPlanManager xPlanManager = createXPlanManager();
		String pathToArchive = copyPlan();

		Pair<Rechtsstand, PlanStatus> legislationStatus = xPlanManager.determineRechtsstand(pathToArchive);

		assertEquals(3000, legislationStatus.first.getCodeNumber());
		assertEquals(PlanStatus.FESTGESTELLT, legislationStatus.second);
	}

	private XPlanManager createXPlanManager() throws Exception {
		XPlanDao xPlanDao = mock(XPlanDao.class);
		XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();
		ManagerConfiguration managerConfiguration = mockManagerConfig();
		RasterEvaluation rasterEvaluation = new XPlanRasterEvaluation("EPSG:25832");
		XPlanRasterEvaluator xPlanRasterEvaluator = new XPlanRasterEvaluator(rasterEvaluation);
		return new XPlanManager(xPlanDao, archiveCreator, managerConfiguration, xPlanRasterEvaluator, null, null, null,
				null);
	}

	private ManagerConfiguration mockManagerConfig() {
		ManagerConfiguration mockedConfiguration = mock(ManagerConfiguration.class);
		when(mockedConfiguration.getSortConfiguration()).thenReturn(new SortConfiguration());
		return mockedConfiguration;
	}

	private String copyPlan() throws IOException {
		InputStream resource = getClass().getResourceAsStream("/testdata/xplan41/BPlan001_4-1.zip");
		FileOutputStream output = null;
		try {
			File resourceFile = Files.createTempFile("XPlanManagerTest_", ".zip").toFile();
			output = new FileOutputStream(resourceFile);
			return resourceFile.getAbsolutePath();
		}
		finally {
			copy(resource, output);
			close(resource);
		}
	}

	private static File newFolder(File root, String... subDirs) throws IOException {
		String subFolder = String.join("/", subDirs);
		File result = new File(root, subFolder);
		if (!result.mkdirs()) {
			throw new IOException("Couldn't create folders " + root);
		}
		return result;
	}

}
