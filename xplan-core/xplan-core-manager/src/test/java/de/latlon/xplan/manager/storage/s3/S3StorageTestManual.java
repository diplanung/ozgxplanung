/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.storage.s3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import de.latlon.xplan.commons.archive.XPlanArchiveContentAccess;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.commons.s3.S3Metadata;
import de.latlon.xplan.commons.s3.S3Object;
import de.latlon.xplan.commons.s3.S3Storage;
import de.latlon.xplan.commons.s3.StorageException;
import de.latlon.xplan.manager.storage.s3.config.TestS3Context;
import de.latlon.xplan.manager.storage.s3.config.S3StorageTestContext;

/**
 * ATTENTION: Executing this test class can run up the bill for the AWS account
 * configured! Ensure that the S3 client is using a free account or is substituted by a
 * mock.
 *
 * To run the IT against a AWS S3 account disable the profile "mock" and set accessKeyId
 * and secretKey in the s3Mock.properties file.
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
@ContextConfiguration(classes = { TestS3Context.class, S3StorageTestContext.class })
@ActiveProfiles({ "mock" })
@TestPropertySource("classpath:s3Mock.properties")
@ExtendWith(SpringExtension.class)
class S3StorageTestManual {

	@Autowired
	private S3Storage s3Storage;

	@Test
	void insertObjectAndGetObject() throws IOException, StorageException {
		InputStream inputStream = getClass().getResourceAsStream("/testdata/xplan60/Blankenese29_Test_60.zip");
		XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();
		XPlanArchiveContentAccess archive = archiveCreator.createXPlanArchiveFromZip("Blankenese29_Test_60.zip",
				inputStream);

		String key = s3Storage.insertObject(1, "Blankenese29.png", archive);
		assertEquals("1_Blankenese29.png", key);

		S3Metadata metadata = s3Storage.getObjectMetadata(key);
		assertEquals(2180090l, metadata.getContentLength());
		assertEquals("image/png", metadata.getContentType());

		S3Object object = s3Storage.getObject(key);
		assertEquals(2180090l, object.getS3Metadata().getContentLength());
		assertEquals("image/png", object.getS3Metadata().getContentType());
	}

	@Test
	void getObjectInvalidKey() throws StorageException {
		assertThrows(StorageException.class, () -> {
			s3Storage.getObject("invalid");
		});
	}

	@Test
	void getObjectMetadataInvalidKey() throws StorageException {
		assertThrows(StorageException.class, () -> {
			s3Storage.getObjectMetadata("invalid");
		});
	}

}
