/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.wmsconfig.raster;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.deegree.theme.persistence.standard.jaxb.ThemeType.Layer;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 */
class RasterConfigurationSorterTest {

	private final DateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

	private final RasterConfigurationSorter rasterConfigurationSorter = new RasterConfigurationSorter();

	@Test
	void testSortByDateInDeegreeOrder() throws Exception {
		Map<String, Date> unsortedMap = new HashMap<String, Date>();
		unsortedMap.put("4", date("2015-03-01"));
		unsortedMap.put("1", null);
		unsortedMap.put("6", date("2015-04-01"));
		unsortedMap.put("2", null);
		unsortedMap.put("5", date("2015-03-01"));
		unsortedMap.put("3", date("1999-01-01"));
		List<String> sortedStrings = rasterConfigurationSorter.sortByDateInDeegreeOrder(unsortedMap);

		assertThat(sortedStrings).isSorted();
	}

	@Test
	void testSortLayers() {
		List<Layer> layers = new ArrayList<>();
		layers.add(mockLayer("5"));
		layers.add(mockLayer("7"));
		layers.add(mockLayer("2"));
		layers.add(mockLayer("3"));
		List<String> sortedPlanIds = asList(new String[] { "1", "2", "3", "4", "5", "6", "7" });
		rasterConfigurationSorter.sortLayers(layers, sortedPlanIds);

		assertThat(layers.stream().map(Layer::getValue)).isSorted();
	}

	@Test
	void sortLayersMissingEntryInSortedList() {
		List<Layer> layers = new ArrayList<Layer>();
		layers.add(mockLayer("7"));
		layers.add(mockLayer("6"));
		layers.add(mockLayer("1"));
		layers.add(mockLayer("3"));
		List<String> sortedPlanIds = asList(new String[] { "2", "3", "4", "5", "6", "7" });
		rasterConfigurationSorter.sortLayers(layers, sortedPlanIds);

		assertThat(layers.stream().map(Layer::getValue)).isSorted();
	}

	private Layer mockLayer(String id) {
		Layer layer = mock(Layer.class);
		when(layer.getValue()).thenReturn(id + "_testLayer");
		when(layer.toString()).thenReturn(id);
		return layer;
	}

	private Date date(String date) throws ParseException {
		return FORMATTER.parse(date);
	}

}
