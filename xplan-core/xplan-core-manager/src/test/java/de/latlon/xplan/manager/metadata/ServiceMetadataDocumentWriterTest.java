/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.metadata;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.xmlunit.assertj3.XmlAssert;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
class ServiceMetadataDocumentWriterTest {

	private static final String DATE = "2017-01-03";

	private static final String TYPE = "service";

	private static final String TITLE = "Alsterdorf20";

	@Test
	void testWriteServiceMetadataDocument() throws Exception {
		byte[] template = IOUtils.toByteArray(ServiceMetadataDocumentWriterTest.class
			.getResourceAsStream("iso-service-metadata-example-template.xml"));

		ByteArrayOutputStream serviceMetadataInstance = new ByteArrayOutputStream();

		ServiceMetadataDocumentWriter serviceMetadataDocumentWriter = new ServiceMetadataDocumentWriter(template);
		serviceMetadataDocumentWriter.writeServiceMetadataDocument(properties(), serviceMetadataInstance);

		XmlAssert.assertThat(serviceMetadataInstance.toString())
			.withNamespaceContext(nsContext())
			.valueByXPath("//gmd:MD_Metadata/gmd:dateStamp/gco:Date")
			.isEqualTo(DATE);

		XmlAssert.assertThat(serviceMetadataInstance.toString())
			.withNamespaceContext(nsContext())
			.valueByXPath("//gmd:MD_Metadata/gmd:hierarchyLevel/gmd:MD_ScopeCode")
			.isEqualTo(TYPE);

		XmlAssert.assertThat(serviceMetadataInstance.toString())
			.withNamespaceContext(nsContext())
			.valueByXPath("//gmd:MD_Metadata/gmd:hierarchyLevel/gmd:MD_ScopeCode/@codeListValue")
			.isEqualTo(TYPE);

		XmlAssert.assertThat(serviceMetadataInstance.toString())
			.withNamespaceContext(nsContext())
			.valueByXPath("//gmd:MD_Metadata/gmd:hierarchyLevelName/gco:CharacterString")
			.isEqualTo(TYPE);

		XmlAssert.assertThat(serviceMetadataInstance.toString())
			.withNamespaceContext(nsContext())
			.valueByXPath("//gmd:MD_Metadata/gmd:metadataStandardName/gco:CharacterString")
			.isEqualTo("NOVALUE");

		XmlAssert.assertThat(serviceMetadataInstance.toString())
			.withNamespaceContext(nsContext())
			.valueByXPath(
					"//gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString")
			.isEqualTo("WMS Bebauungsplan " + TITLE);
	}

	private Properties properties() {
		Properties properties = new Properties();
		properties.setProperty("CURRENT_DATE", DATE);
		properties.setProperty("MD_SCOPE_CODE", TYPE);
		properties.setProperty("TITLE", TITLE);
		return properties;
	}

	private Map<String, String> nsContext() {
		Map<String, String> nsContext = new HashMap<>();
		nsContext.put("gmd", "http://www.isotc211.org/2005/gmd");
		nsContext.put("gco", "http://www.isotc211.org/2005/gco");
		nsContext.put("srv", "http://www.isotc211.org/2005/srv");
		return nsContext;
	}

}
