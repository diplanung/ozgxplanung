/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.edit;

import static de.latlon.xplan.commons.XPlanType.BP_Plan;
import static de.latlon.xplan.commons.XPlanType.FP_Plan;
import static de.latlon.xplan.commons.XPlanType.LP_Plan;
import static de.latlon.xplan.commons.XPlanType.RP_Plan;
import static de.latlon.xplan.commons.XPlanType.SO_Plan;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_41;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_50;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_51;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_52;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_53;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_54;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_60;
import static de.latlon.xplan.manager.web.shared.edit.ChangeType.CHANGED_BY;
import static de.latlon.xplan.manager.web.shared.edit.ChangeType.CHANGES;
import static de.latlon.xplan.manager.web.shared.edit.ExterneReferenzArt.DOKUMENT;
import static de.latlon.xplan.manager.web.shared.edit.ExterneReferenzArt.PLANMITGEOREFERENZ;
import static de.latlon.xplan.manager.web.shared.edit.MimeTypes.IMAGE_PNG;
import static de.latlon.xplan.manager.web.shared.edit.RasterReferenceType.LEGEND;
import static de.latlon.xplan.manager.web.shared.edit.RasterReferenceType.SCAN;
import static de.latlon.xplan.manager.web.shared.edit.ReferenceType.BEGRUENDUNG;
import static de.latlon.xplan.manager.web.shared.edit.ReferenceType.GRUENORDNUNGSPLAN;
import static de.latlon.xplan.manager.web.shared.edit.ReferenceType.INFORMELL;
import static de.latlon.xplan.manager.web.shared.edit.ReferenceType.RECHTSPLAN;
import static de.latlon.xplan.manager.web.shared.edit.ReferenceType.VERORDNUNG;
import static de.latlon.xplan.manager.web.shared.edit.TextRechtscharacterType.SO_SONSTIGES;
import static de.latlon.xplan.manager.web.shared.edit.TextRechtscharacterType.XP_FESTSETZUNGBPLAN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import de.latlon.xplan.commons.XPlanType;
import de.latlon.xplan.commons.XPlanVersion;
import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.commons.feature.XPlanGmlParserBuilder;
import de.latlon.xplan.manager.web.shared.XPlan;
import de.latlon.xplan.manager.web.shared.edit.BaseData;
import de.latlon.xplan.manager.web.shared.edit.Change;
import de.latlon.xplan.manager.web.shared.edit.RasterBasis;
import de.latlon.xplan.manager.web.shared.edit.RasterReference;
import de.latlon.xplan.manager.web.shared.edit.RasterReferenceType;
import de.latlon.xplan.manager.web.shared.edit.Reference;
import de.latlon.xplan.manager.web.shared.edit.Text;
import de.latlon.xplan.manager.web.shared.edit.XPlanToEdit;
import org.deegree.feature.FeatureCollection;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 */
class XPlanToEditFactoryTest {

	private XPlanToEditFactory factory = new XPlanToEditFactory();

	@Test
	void createXPlanToEditXPlan52MultipleBereiche() throws Exception {
		FeatureCollection featureCollection = readXPlanArchive(XPLAN_52, "xplan52/BPlan001_5-2_Bereiche.zip");

		XPlanToEdit xPlanToEdit = factory.createXPlanToEdit(mockXPlan(XPLAN_52, BP_Plan), featureCollection);

		assertTrue(xPlanToEdit.isHasBereich());

		List<RasterBasis> rasterBasis = xPlanToEdit.getRasterBasis();
		assertThat(rasterBasis).hasSize(2);

		// Bereich 0
		String bereichNummer0 = "0";
		RasterBasis rasterBasis0 = getByBereichNummer(rasterBasis, bereichNummer0);
		List<RasterReference> rasterReferences0 = rasterBasis0.getRasterReferences();

		assertNull(rasterBasis0.getFeatureId());
		assertThat(rasterReferences0).hasSize(1);
		assertEquals(bereichNummer0, rasterReferences0.get(0).getBereichNummer());
		assertEquals("BPlan001_5-2_Bereich0.png", rasterReferences0.get(0).getReference());

		// Bereich 1
		String bereichNummer1 = "1";
		RasterBasis rasterBasis1 = getByBereichNummer(rasterBasis, bereichNummer1);
		List<RasterReference> rasterReferences1 = rasterBasis1.getRasterReferences();

		assertNull(rasterBasis1.getFeatureId());
		assertThat(rasterReferences1).hasSize(1);
		assertEquals(bereichNummer1, rasterReferences1.get(0).getBereichNummer());
		assertEquals("BPlan001_5-2_Bereich1.png", rasterReferences1.get(0).getReference());
	}

	@Test
	void createXPlanToEditXPlan51RefScan() throws Exception {
		FeatureCollection featureCollection = readXPlanGml(XPLAN_51, "xplan51/V4_1_ID_103_refScan.gml");

		XPlanToEdit xPlanToEdit = factory.createXPlanToEdit(mockXPlan(XPLAN_51, BP_Plan), featureCollection);

		assertTrue(xPlanToEdit.isHasBereich());

		String bereichNummer = "0";
		List<RasterBasis> allRasterBasis = xPlanToEdit.getRasterBasis();
		assertThat(allRasterBasis).hasSize(1);

		RasterBasis rasterBasis = allRasterBasis.get(0);
		assertNull(rasterBasis.getFeatureId());
		assertEquals(bereichNummer, rasterBasis.getBereichNummer());

		List<RasterReference> rasterReferences = rasterBasis.getRasterReferences();
		assertThat(rasterReferences).hasSize(1);
		assertEquals(bereichNummer, rasterReferences.get(0).getBereichNummer());
		assertEquals("B-Plan_Klingmuehl_Heideweg_Karte.tif", rasterReferences.get(0).getReference());
		assertEquals("B-Plan_Klingmuehl_Heideweg_Karte.tfw", rasterReferences.get(0).getGeoReference());

	}

	@Test
	void createXPlanToEditXPlan51Rasterdarstellung() throws Exception {
		FeatureCollection featureCollection = readXPlanGml(XPLAN_51, "xplan51/V4_1_ID_103.gml");

		XPlanToEdit xPlanToEdit = factory.createXPlanToEdit(mockXPlan(XPLAN_51, BP_Plan), featureCollection);

		assertTrue(xPlanToEdit.isHasBereich());

		String bereichNummer = "0";
		List<RasterBasis> allRasterBasis = xPlanToEdit.getRasterBasis();
		assertThat(allRasterBasis).hasSize(1);

		RasterBasis rasterBasis = allRasterBasis.get(0);
		assertEquals("FEATURE_c2a83b1c-05f4-4dc0-a1b6-feb1a43328d6", rasterBasis.getFeatureId());
		assertEquals(bereichNummer, rasterBasis.getBereichNummer());

		List<RasterReference> rasterReferences = rasterBasis.getRasterReferences();
		assertThat(rasterReferences).hasSize(1);
		assertEquals(bereichNummer, rasterReferences.get(0).getBereichNummer());
		assertEquals("B-Plan_Klingmuehl_Heideweg_Karte.tif", rasterReferences.get(0).getReference());
		assertEquals("B-Plan_Klingmuehl_Heideweg_Karte.tfw", rasterReferences.get(0).getGeoReference());

		assertThat(xPlanToEdit.getTexts()).hasSize(2);
		assertThat(xPlanToEdit.getReferences()).hasSize(4);
	}

	@Test
	void createXPlanToEditXPlan50BaseDataChanges() throws Exception {
		FeatureCollection featureCollection = readXPlanGml(XPLAN_50, "xplan50/BP2070.gml");

		XPlanToEdit xPlanToEdit = factory.createXPlanToEdit(mockXPlan(XPLAN_50, BP_Plan), featureCollection);

		BaseData baseData = xPlanToEdit.getBaseData();
		assertEquals("BP2070", baseData.getPlanName());
		assertEquals("Testdatensatz XPlabGML", baseData.getDescription());

		assertEquals(-1, baseData.getLegislationStatusCode());
		assertEquals(1000, baseData.getPlanTypeCode());
		assertEquals(-1, baseData.getMethodCode());
		assertEquals(-1, baseData.getOtherPlanTypeCode());

		assertEquals(asDate("2001-08-06"), baseData.getCreationDate());
		assertNull(baseData.getLossDate());
		assertNull(baseData.getRegulationDate());

		List<Change> changes = xPlanToEdit.getChanges();
		assertThat(changes).hasSize(0);
	}

	@Test
	void createXPlanToEditXPlan41BaseDataChanges() throws Exception {
		FeatureCollection featureCollection = readXPlanGml(XPLAN_41, "xplan41/Eidelstedt_4_V4-Blankenese.gml");

		XPlanToEdit xPlanToEdit = factory.createXPlanToEdit(mockXPlan(XPLAN_41, BP_Plan), featureCollection);

		BaseData baseData = xPlanToEdit.getBaseData();
		assertEquals("Eidelstedt 4", baseData.getPlanName());
		assertEquals("Beschreibung von Eidelstedt 4", baseData.getDescription());

		assertEquals(4000, baseData.getLegislationStatusCode());
		assertEquals(1000, baseData.getPlanTypeCode());
		assertEquals(2000, baseData.getMethodCode());
		assertEquals(-1, baseData.getOtherPlanTypeCode());

		assertEquals(asDate("2010-10-07"), baseData.getCreationDate());
		assertEquals(asDate("2020-10-07"), baseData.getLossDate());
		assertEquals(asDate("1970-01-01"), baseData.getRegulationDate());

		List<Change> changes = xPlanToEdit.getChanges();
		assertThat(changes).hasSize(2);

		Change firstChange = changes.get(0);
		assertEquals("Eidelstedt 3 (alt)", firstChange.getPlanName());
		assertEquals(1100, firstChange.getLegalNatureCode());
		assertEquals("3", firstChange.getNumber());
		assertEquals(CHANGES, firstChange.getType());

		Change secondChange = changes.get(1);
		assertEquals("Eidelstedt 4 (textliche Änderung)", secondChange.getPlanName());
		assertEquals(1000, secondChange.getLegalNatureCode());
		assertNull(secondChange.getNumber());
		assertEquals(CHANGED_BY, secondChange.getType());
	}

	@Test
	void createXPlanToEditReferencesTextsV41ID10341() throws Exception {
		XPlanVersion version = XPLAN_41;
		FeatureCollection featureCollection = readXPlanGml(version, "xplan41/V4_1_ID_103.gml");

		XPlanToEdit xPlanToEdit = factory.createXPlanToEdit(mockXPlan(version, BP_Plan), featureCollection);

		BaseData baseData = xPlanToEdit.getBaseData();
		assertEquals("\"Heideweg\"", baseData.getPlanName());
		assertEquals(4000, baseData.getLegislationStatusCode());
		assertEquals(1000, baseData.getPlanTypeCode());
		assertEquals(asDate("2001-08-27"), baseData.getCreationDate());

		List<Change> changes = xPlanToEdit.getChanges();
		assertThat(changes).hasSize(0);

		List<Reference> references = xPlanToEdit.getReferences();
		assertThat(references).hasSize(3);

		Reference firstReference = references.get(0);
		assertNull(firstReference.getGeoReference());
		assertEquals("B-Plan_Klingmuehl_Heideweg_Leg.pdf", firstReference.getReference());
		assertEquals("B-Plan_Klingmuehl_Heideweg_Leg", firstReference.getReferenzName());
		assertEquals(BEGRUENDUNG, firstReference.getType());

		Reference secondReference = references.get(1);
		assertNull(secondReference.getGeoReference());
		assertEquals("B-Plan_Klingmuehl_Heideweg.tif", secondReference.getReference());
		assertEquals("B-Plan_Klingmuehl_Heideweg", secondReference.getReferenzName());
		assertEquals(RECHTSPLAN, secondReference.getType());

		Reference thirdReference = references.get(2);
		assertEquals("B-Plan_Klingmuehl_Heideweg_Gruen.pgw", thirdReference.getGeoReference());
		assertEquals("B-Plan_Klingmuehl_Heideweg_Gruen.png", thirdReference.getReference());
		assertEquals("B-Plan_Klingmuehl_Heideweg_Gruen", thirdReference.getReferenzName());
		assertEquals(GRUENORDNUNGSPLAN, thirdReference.getType());

		List<Text> texts = xPlanToEdit.getTexts();
		assertThat(texts).hasSize(1);

		Text text = texts.get(0);
		assertEquals("FEATURE_0f870967-bd6f-4367-9150-8a255f0290ad", text.getFeatureId());
		assertEquals("key", text.getKey());
		assertEquals("base", text.getBasis());
		assertEquals("Beschreibungstext", text.getText());
		assertNull(text.getGeoReference());
		assertEquals("B-Plan_Klingmuehl_Heideweg_Text.pdf", text.getReference());

		String bereichNummer = "0";
		List<RasterBasis> allRasterBasis = xPlanToEdit.getRasterBasis();
		assertThat(allRasterBasis).hasSize(1);

		RasterBasis rasterBasis = allRasterBasis.get(0);
		assertEquals(bereichNummer, rasterBasis.getBereichNummer());
		assertEquals("FEATURE_c2a83b1c-05f4-4dc0-a1b6-feb1a43328d6", rasterBasis.getFeatureId());

		List<RasterReference> rasterBasisReferences = rasterBasis.getRasterReferences();
		assertThat(rasterBasisReferences).hasSize(2);

		RasterReference scan = getByType(rasterBasisReferences, SCAN);
		assertNotNull(scan);
		assertNull(scan.getFeatureId());
		assertEquals(bereichNummer, scan.getBereichNummer());
		assertEquals("B-Plan_Klingmuehl_Heideweg_Karte.tfw", scan.getGeoReference());
		assertEquals("B-Plan_Klingmuehl_Heideweg_Karte.tif", scan.getReference());
		assertEquals("B-Plan_Klingmuehl_Heideweg_Karte", scan.getReferenzName());
		assertEquals(PLANMITGEOREFERENZ, scan.getArt());

		RasterReference legend = getByType(rasterBasisReferences, LEGEND);
		assertNotNull(legend);
		assertNull(legend.getFeatureId());
		assertEquals(bereichNummer, legend.getBereichNummer());
		assertEquals("B-Plan_Klingmuehl_Heideweg_Legende.png", legend.getReference());
		assertEquals(IMAGE_PNG, legend.getReferenzMimeType());
		assertNull(legend.getGeoReference());
		assertNull(legend.getGeorefMimeType());
		assertEquals("informationssystemURL", legend.getInformationssystemURL());
		assertEquals("B-Plan_Klingmuehl_Heideweg_Legende", legend.getReferenzName());
		assertEquals("beschreibung", legend.getBeschreibung());
		assertEquals(asDate("2018-03-01"), legend.getDatum());
		assertEquals(DOKUMENT, legend.getArt());
	}

	@Test
	void createXPlanToEditIncompleteRefText() throws Exception {
		XPlanVersion version = XPLAN_41;
		FeatureCollection featureCollection = readXPlanGml(version, "xplan41/V4_1_ID_103_incompleteRefText.gml");

		XPlanToEdit xPlanToEdit = factory.createXPlanToEdit(mockXPlan(version, BP_Plan), featureCollection);

		List<Text> texts = xPlanToEdit.getTexts();
		assertThat(texts).hasSize(1);

		Text text = texts.get(0);
		assertEquals("FEATURE_0f870967-bd6f-4367-9150-8a255f0290ad", text.getFeatureId());
		assertEquals("key", text.getKey());
		assertEquals("base", text.getBasis());
		assertEquals("Beschreibungstext", text.getText());
		assertNull(text.getGeoReference());
		assertNull(text.getReference());
	}

	@Test
	void createXPlanToEditReferencesTextsBPlan00450() throws Exception {
		XPlanVersion version = XPLAN_50;
		FeatureCollection featureCollection = readXPlanArchive(version, "xplan50/BPlan004_5-0.zip");

		XPlanToEdit xPlanToEdit = factory.createXPlanToEdit(mockXPlan(version, BP_Plan), featureCollection);

		BaseData baseData = xPlanToEdit.getBaseData();
		assertEquals("BPlan004_5-0", baseData.getPlanName());
		assertEquals(3000, baseData.getLegislationStatusCode());
		assertEquals(3000, baseData.getPlanTypeCode());
		assertEquals(asDate("2017-03-20"), baseData.getCreationDate());

		List<Change> changes = xPlanToEdit.getChanges();
		assertThat(changes).hasSize(0);

		List<Reference> references = xPlanToEdit.getReferences();
		assertThat(references).hasSize(1);

		Reference firstReference = references.get(0);
		assertNull(firstReference.getGeoReference());
		assertEquals("BPlan004_5-0.pdf", firstReference.getReference());
		assertNull(firstReference.getReferenzName());
		assertEquals(INFORMELL, firstReference.getType());

		List<Text> texts = xPlanToEdit.getTexts();
		assertThat(texts).hasSize(9);

		String featureIdOfText = "GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90";
		Text text = texts.stream().filter(t -> featureIdOfText.equals(t.getFeatureId())).findFirst().get();
		assertEquals(featureIdOfText, text.getFeatureId());
		assertEquals("§2 Nr.4", text.getKey());
		assertNull(text.getBasis());
		assertEquals(
				"Im allgemeinen Wohngebiet darf die festgesetzte Grundflächenzahl\nfür Tiefgaragen bis zu einer Grundflächenzahl\nvon 1,0 überschritten werden.",
				text.getText());

		String bereichNummer = "0";
		List<RasterBasis> allRasterBasis = xPlanToEdit.getRasterBasis();
		assertThat(allRasterBasis).hasSize(1);

		RasterBasis rasterBasis = allRasterBasis.get(0);
		assertEquals(bereichNummer, rasterBasis.getBereichNummer());
		assertEquals("Gml_FEC4F42F-5D66-4A59-9A47-6E03D1A3139A", rasterBasis.getFeatureId());

		List<RasterReference> rasterBasisReferences = rasterBasis.getRasterReferences();
		assertThat(rasterBasisReferences).hasSize(1);

		RasterReference scan = getByType(rasterBasisReferences, SCAN);
		assertNotNull(scan);
		assertNull(scan.getFeatureId());
		assertEquals(bereichNummer, scan.getBereichNummer());
		assertEquals("BPlan004_5-0.pgw", scan.getGeoReference());
		assertEquals("BPlan004_5-0.png", scan.getReference());
	}

	@Test
	void createXPlanToEditXPlan53WithoutBereich() throws Exception {
		FeatureCollection featureCollection = readXPlanGml(XPLAN_53, "xplan53/BPlan_ohneBereich.gml");

		XPlanToEdit xPlanToEdit = factory.createXPlanToEdit(mockXPlan(XPLAN_53, BP_Plan), featureCollection);

		assertFalse(xPlanToEdit.isHasBereich());
	}

	@Test
	void createXPlanToEditXPlan51FPlan() throws Exception {
		FeatureCollection featureCollection = readXPlanArchive(XPLAN_51, "xplan51/FPlan.zip");

		XPlanToEdit xPlanToEdit = factory.createXPlanToEdit(mockXPlan(XPLAN_51, FP_Plan), featureCollection);
		assertTrue(xPlanToEdit.isHasBereich());

		BaseData baseData = xPlanToEdit.getBaseData();
		assertEquals("FPlan Bad Liebenwerda", baseData.getPlanName());
		assertEquals(9999, baseData.getPlanTypeCode());
		assertEquals(asDate("2004-12-01"), baseData.getCreationDate());

		assertThat(xPlanToEdit.getRasterBasis()).hasSize(1);
		assertThat(xPlanToEdit.getRasterBasis().get(0).getRasterReferences()).hasSize(0);
		assertThat(xPlanToEdit.getTexts()).hasSize(0);
		assertThat(xPlanToEdit.getReferences()).hasSize(0);
		assertThat(xPlanToEdit.getChanges()).hasSize(0);
	}

	@Test
	void createXPlanToEditXPlan60LPlan() throws Exception {
		FeatureCollection featureCollection = readXPlanArchive(XPLAN_60, "xplan60/LP-Test_60.zip");

		XPlanToEdit xPlanToEdit = factory.createXPlanToEdit(mockXPlan(XPLAN_60, LP_Plan), featureCollection);
		assertTrue(xPlanToEdit.isHasBereich());

		BaseData baseData = xPlanToEdit.getBaseData();
		assertEquals("LP-Test 60", baseData.getPlanName());
		assertEquals(3000, baseData.getPlanTypeCode());

		assertThat(xPlanToEdit.getRasterBasis()).hasSize(1);
		assertThat(xPlanToEdit.getRasterBasis().get(0).getRasterReferences()).hasSize(0);

		List<Text> texts = xPlanToEdit.getTexts();
		assertThat(texts).hasSize(1);

		Text text = texts.get(0);
		assertEquals("Gml_1234", text.getFeatureId());
		assertEquals("Test", text.getText());
		assertEquals(XP_FESTSETZUNGBPLAN, text.getRechtscharakter());

		assertThat(xPlanToEdit.getReferences()).hasSize(0);
		assertThat(xPlanToEdit.getChanges()).hasSize(0);
	}

	@Test
	void createXPlanToEditXPlan51SOPlan() throws Exception {
		FeatureCollection featureCollection = readXPlanArchive(XPLAN_51, "xplan51/StErhVO_Heidberg_51.zip");

		XPlanToEdit xPlanToEdit = factory.createXPlanToEdit(XPLAN_51, SO_Plan, featureCollection);
		assertTrue(xPlanToEdit.isHasBereich());

		BaseData baseData = xPlanToEdit.getBaseData();
		assertEquals("StErhVO_Heidberg", baseData.getPlanName());
		assertEquals("siehe Lageplan", baseData.getDescription());
		assertEquals(17200, baseData.getPlanTypeCode());
		assertEquals(asDate("2019-06-07"), baseData.getCreationDate());

		assertThat(xPlanToEdit.getRasterBasis()).hasSize(1);
		assertThat(xPlanToEdit.getRasterBasis().get(0).getRasterReferences()).hasSize(0);

		List<Text> texts = xPlanToEdit.getTexts();
		assertThat(texts).hasSize(3);

		Text firstText = texts.get(0);
		assertEquals("Gml_2413F6CD-7CCF-4F69-B2A9-9C34D6FB5EFA", firstText.getFeatureId());
		assertEquals("1", firstText.getKey());
		assertThat(firstText.getText())
			.contains("Diese Verordnung gilt für die in der anliegenden Karte durch eine schwarze Linie abgegrenzte");
		assertNull(firstText.getGeoReference());
		assertEquals(SO_SONSTIGES, firstText.getRechtscharakter());

		Text secondText = texts.get(1);
		assertEquals("Gml_A903B546-00F6-47CD-8627-E50D9B0DD250", secondText.getFeatureId());
		assertEquals("2", secondText.getKey());
		assertThat(secondText.getText()).contains(
				"die Errichtung baulicher Anlagen der Genehmigung, und zwar auch dann, wenn nach den bauordnungsrechtlichen Vorschriften");
		assertNull(secondText.getGeoReference());
		assertEquals(SO_SONSTIGES, secondText.getRechtscharakter());

		Text thirdText = texts.get(2);
		assertEquals("Gml_C5992598-EF43-4EA2-AA00-2D431081A4F7", thirdText.getFeatureId());
		assertEquals("3", thirdText.getKey());
		assertThat(thirdText.getText()).contains("Es wird auf Folgendes hingewiesen:");
		assertNull(thirdText.getGeoReference());
		assertEquals(SO_SONSTIGES, thirdText.getRechtscharakter());

		List<Reference> references = xPlanToEdit.getReferences();
		assertThat(references).hasSize(1);

		Reference reference = references.get(0);
		assertNull(reference.getGeoReference());
		assertEquals("StErhVO_Heidberg.pdf", reference.getReference());
		assertNull(reference.getReferenzName());
		assertEquals(VERORDNUNG, reference.getType());

		assertThat(xPlanToEdit.getChanges()).hasSize(0);
	}

	@Test
	void createXPlanToEditXPlan51RPlan() throws Exception {
		FeatureCollection featureCollection = readXPlanArchive(XPLAN_51, "xplan51/RROP_Landkreis_Test_51.zip");

		XPlanToEdit xPlanToEdit = factory.createXPlanToEdit(mockXPlan(XPLAN_51, RP_Plan), featureCollection);
		assertTrue(xPlanToEdit.isHasBereich());

		BaseData baseData = xPlanToEdit.getBaseData();
		assertEquals("Regionales Raumordnungsprogramm Landkreis Test 2019", baseData.getPlanName());
		assertNull(baseData.getDescription());
		assertEquals(1000, baseData.getPlanTypeCode());
		assertNull(baseData.getCreationDate());
		assertEquals(3000, baseData.getMethodCode());

		assertThat(xPlanToEdit.getRasterBasis()).hasSize(1);
		assertThat(xPlanToEdit.getRasterBasis().get(0).getRasterReferences()).hasSize(0);
		assertThat(xPlanToEdit.getTexts()).hasSize(0);
		assertThat(xPlanToEdit.getReferences()).hasSize(0);
		assertThat(xPlanToEdit.getChanges()).hasSize(0);
	}

	@Test
	void createXPlanToEditXPlan54IncompleteRefScan() throws Exception {
		FeatureCollection featureCollection = readXPlanGml(XPLAN_54, "xplan54/BPlan002_5-4.gml");

		XPlanToEdit xPlanToEdit = factory.createXPlanToEdit(mockXPlan(XPLAN_54, BP_Plan), featureCollection);
		assertTrue(xPlanToEdit.isHasBereich());

		List<RasterBasis> rasterBasis = xPlanToEdit.getRasterBasis();
		assertThat(rasterBasis).hasSize(1);
		assertThat(rasterBasis.get(0).getRasterReferences()).hasSize(0);
	}

	private RasterReference getByType(List<RasterReference> rasterBasisReferences, RasterReferenceType type) {
		for (RasterReference rasterReference : rasterBasisReferences) {
			if (type.equals(rasterReference.getType()))
				return rasterReference;
		}
		return null;
	}

	private RasterBasis getByBereichNummer(List<RasterBasis> rasterBasis, String bereichNummer) {
		Optional<RasterBasis> rasterBasisWithNumber = rasterBasis.stream()
			.filter(rb -> rb.getBereichNummer().equals(bereichNummer))
			.findFirst();
		return rasterBasisWithNumber.isPresent() ? rasterBasisWithNumber.get() : null;
	}

	private FeatureCollection readXPlanGml(XPlanVersion xplanVersion, String plan) throws Exception {
		InputStream xplanGml = this.getClass().getResourceAsStream(plan);
		return XPlanGmlParserBuilder.newBuilder().build().parseFeatureCollection(xplanGml, xplanVersion);
	}

	private FeatureCollection readXPlanArchive(XPlanVersion xplanVersion, String resource) throws Exception {
		XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();
		InputStream inputStream = getClass().getResourceAsStream("/testdata/" + resource);
		XPlanArchive xPlanArchiveFromZip = archiveCreator.createXPlanArchiveFromZip(resource, inputStream);
		InputStream mainFileInputStream = xPlanArchiveFromZip.getMainFileInputStream();
		return XPlanGmlParserBuilder.newBuilder().build().parseFeatureCollection(mainFileInputStream, xplanVersion);
	}

	private Date asDate(String string) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return simpleDateFormat.parse(string);
	}

	private XPlan mockXPlan(XPlanVersion version, XPlanType xPlanType) {
		XPlan mock = mock(XPlan.class);
		when(mock.getVersion()).thenReturn(version.toString());
		when(mock.getType()).thenReturn(xPlanType.name());
		return mock;
	}

}
