/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.configuration;

import static de.latlon.xplan.commons.XPlanType.BP_Plan;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_50;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_51;
import static de.latlon.xplan.manager.configuration.ManagerConfiguration.RASTER_CONFIG_CRS;
import static de.latlon.xplan.manager.configuration.ManagerConfiguration.RASTER_LAYER_SCALE_DENOMINATOR_MAX;
import static de.latlon.xplan.manager.configuration.ManagerConfiguration.RASTER_LAYER_SCALE_DENOMINATOR_MIN;
import static de.latlon.xplan.manager.configuration.ManagerConfiguration.WORKSPACE_RELOAD_PASSWORD;
import static de.latlon.xplan.manager.configuration.ManagerConfiguration.WORKSPACE_RELOAD_URLS;
import static de.latlon.xplan.manager.configuration.ManagerConfiguration.WORKSPACE_RELOAD_USER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Properties;

import de.latlon.xplan.commons.configuration.PropertiesLoader;
import de.latlon.xplan.commons.configuration.SemanticConformityLinkConfiguration;
import de.latlon.xplan.commons.configuration.SortConfiguration;
import de.latlon.xplan.manager.web.shared.ConfigurationException;
import de.latlon.xplan.manager.workspace.WorkspaceReloaderConfiguration;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version $Revision: $, $Date: $
 */
class ManagerConfigurationTest {

	@Test
	void testGetSortConfiguration() throws Exception {
		ManagerConfiguration managerConfiguration = new ManagerConfiguration(mockPropertiesLoader());

		SortConfiguration sortConfiguration = managerConfiguration.getSortConfiguration();

		assertEquals("BP_Plan", sortConfiguration.retrieveFeatureType(BP_Plan, XPLAN_50));
		assertEquals("rechtsverordnungsDatum", sortConfiguration.retrievePropertyName(BP_Plan, XPLAN_50));
	}

	@Test
	void testGetSemanticConformityLinkConfiguration() throws Exception {
		ManagerConfiguration managerConfiguration = new ManagerConfiguration(mockPropertiesLoader());

		SemanticConformityLinkConfiguration semanticConformityLinkConfiguration = managerConfiguration
			.getSemanticConformityLinkConfiguration();
		assertEquals("http://link.de/to.pdf", semanticConformityLinkConfiguration.retrieveLink(XPLAN_50));
		assertNull(semanticConformityLinkConfiguration.retrieveLink(XPLAN_51));
	}

	@Test
	void testGetWorkspaceReloaderConfiguration() throws Exception {
		ManagerConfiguration managerConfiguration = new ManagerConfiguration(mockPropertiesLoader());

		WorkspaceReloaderConfiguration workspaceReloaderConfiguration = managerConfiguration
			.getWorkspaceReloaderConfiguration();

		assertThat(workspaceReloaderConfiguration.getUrls()).containsExactly("url1", "url2");
		assertEquals("user", workspaceReloaderConfiguration.getUser());
		assertEquals("password", workspaceReloaderConfiguration.getPassword());
	}

	@Test
	void defaultScaleDenominators() throws Exception {
		ManagerConfiguration managerConfiguration = new ManagerConfiguration(mockPropertiesLoader());

		assertEquals(Double.NaN, managerConfiguration.getRasterLayerMinScaleDenominator());
		assertEquals(Double.NaN, managerConfiguration.getRasterLayerMaxScaleDenominator());
	}

	@Test
	void scaleDenominators() throws Exception {
		double min = 5;
		double max = 1000;
		ManagerConfiguration managerConfiguration = new ManagerConfiguration(mockPropertiesLoader(min, max));

		assertEquals(min, managerConfiguration.getRasterLayerMinScaleDenominator());
		assertEquals(max, managerConfiguration.getRasterLayerMaxScaleDenominator());
	}

	@Test
	void scaleDenominatorsMaxLessMin() throws Exception {
		assertThrows(IllegalArgumentException.class, () -> {
			double min = 5;
			double max = 1;
			new ManagerConfiguration(mockPropertiesLoader(min, max));
		});
	}

	@Test
	void scaleDenominatorsMaxNegative() throws Exception {
		assertThrows(IllegalArgumentException.class, () -> {
			double min = 5;
			double max = -1;
			new ManagerConfiguration(mockPropertiesLoader(min, max));
		});
	}

	@Test
	void scaleDenominatorsMinNegative() throws Exception {
		assertThrows(IllegalArgumentException.class, () -> {
			double min = -5;
			double max = 1;
			new ManagerConfiguration(mockPropertiesLoader(min, max));
		});
	}

	private PropertiesLoader mockPropertiesLoader() throws ConfigurationException {
		return mockPropertiesLoader(Double.NaN, Double.NaN);
	}

	private PropertiesLoader mockPropertiesLoader(double minScaleDenominator, double maxScaleDenominator)
			throws ConfigurationException {
		PropertiesLoader propertiesLoader = mock(PropertiesLoader.class);
		Properties properties = new Properties();
		properties.put(RASTER_CONFIG_CRS, "epsg:4326");
		properties.put("wmsSortDate_BP_Plan_XPLAN_50", "BP_Plan,rechtsverordnungsDatum");
		properties.put("linkSemanticConformity_XPLAN_50", "http://link.de/to.pdf");
		properties.put(WORKSPACE_RELOAD_URLS, "url1,url2");
		properties.put(WORKSPACE_RELOAD_USER, "user");
		properties.put(WORKSPACE_RELOAD_PASSWORD, "password");
		if (!Double.isNaN(minScaleDenominator))
			properties.put(RASTER_LAYER_SCALE_DENOMINATOR_MIN, Double.toString(minScaleDenominator));
		if (!Double.isNaN(maxScaleDenominator))
			properties.put(RASTER_LAYER_SCALE_DENOMINATOR_MAX, Double.toString(maxScaleDenominator));
		when(propertiesLoader.loadProperties(anyString())).thenReturn(properties);
		return propertiesLoader;
	}

}
