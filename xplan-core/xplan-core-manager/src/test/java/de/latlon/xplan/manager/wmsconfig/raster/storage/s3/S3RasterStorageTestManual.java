/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.wmsconfig.raster.storage.s3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import de.latlon.xplan.commons.archive.XPlanArchiveContentAccess;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.commons.s3.S3Object;
import de.latlon.xplan.commons.s3.StorageException;
import de.latlon.xplan.manager.storage.StorageEvent;
import de.latlon.xplan.manager.storage.s3.config.TestS3Context;
import de.latlon.xplan.manager.wmsconfig.raster.storage.s3.config.AmazonS3RasterStorageContext;

/**
 * ATTENTION: Executing this test class can run up the bill for the AWS account
 * configured! Ensure that the S3 client is using a free account or is substituted by a
 * mock.
 *
 * To run the IT against a AWS S3 account disable the profile "mock" and set accessKeyId
 * and secretKey in the s3Mock.properties file.
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @author <a href="mailto:friebe@lat-lon.de">Torsten Friebe</a>
 */
@ContextConfiguration(classes = { AmazonS3RasterStorageContext.class, TestS3Context.class })
@ActiveProfiles({ "mock" })
@TestPropertySource("classpath:s3Mock.properties")
@ExtendWith(SpringExtension.class)
class S3RasterStorageTestManual {

	@Autowired
	private S3RasterStorage s3RasterStorage;

	@Test
	void testAddRasterFile() throws IOException, StorageException {
		InputStream inputStream = getClass().getResourceAsStream("/testdata/xplan60/Blankenese29_Test_60.zip");
		XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();
		XPlanArchiveContentAccess archive = archiveCreator.createXPlanArchiveFromZip("Blankenese29_Test_60.zip",
				inputStream);

		StorageEvent storageEvent = mock(StorageEvent.class);
		String key = s3RasterStorage.addRasterFile(1, "Blankenese29.png", null, archive, storageEvent);

		assertEquals("1_Blankenese29.png", key);
		verify(storageEvent).addInsertedKey(eq("1_Blankenese29.png"));
	}

	@Test
	void testDeleteRasterFile() throws StorageException, IOException {
		importRasterFile();

		StorageEvent storageEvent = mock(StorageEvent.class);
		s3RasterStorage.deleteRasterFile(1, "Blankenese29.png", storageEvent);
		verify(storageEvent).addDeletedKey(any(S3Object.class));
	}

	private void importRasterFile() throws IOException, StorageException {
		InputStream inputStream = getClass().getResourceAsStream("/testdata/xplan60/Blankenese29_Test_60.zip");
		XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();
		XPlanArchiveContentAccess archive = archiveCreator.createXPlanArchiveFromZip("Blankenese29_Test_60.zip",
				inputStream);

		StorageEvent storageEvent = mock(StorageEvent.class);
		s3RasterStorage.addRasterFile(1, "Blankenese29.png", null, archive, storageEvent);
	}

}
