/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.edit;

import static de.latlon.xplan.commons.XPlanType.BP_Plan;
import static de.latlon.xplan.commons.XPlanType.FP_Plan;
import static de.latlon.xplan.commons.XPlanType.LP_Plan;
import static de.latlon.xplan.commons.XPlanType.RP_Plan;
import static de.latlon.xplan.commons.XPlanType.SO_Plan;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_41;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_50;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_51;
import static de.latlon.xplan.manager.web.shared.edit.ChangeType.CHANGED_BY;
import static de.latlon.xplan.manager.web.shared.edit.ChangeType.CHANGES;
import static de.latlon.xplan.manager.web.shared.edit.ExterneReferenzArt.PLANMITGEOREFERENZ;
import static de.latlon.xplan.manager.web.shared.edit.MimeTypes.IMAGE_PNG;
import static de.latlon.xplan.manager.web.shared.edit.MimeTypes.TEXT_HTML;
import static de.latlon.xplan.manager.web.shared.edit.RasterReferenceType.LEGEND;
import static de.latlon.xplan.manager.web.shared.edit.RasterReferenceType.SCAN;
import static de.latlon.xplan.manager.web.shared.edit.ReferenceType.BEGRUENDUNG;
import static de.latlon.xplan.manager.web.shared.edit.ReferenceType.GRUENORDNUNGSPLAN;
import static de.latlon.xplan.manager.web.shared.edit.ReferenceType.RECHTSPLAN;
import static de.latlon.xplan.manager.web.shared.edit.TextRechtscharacterType.BP_HINWEIS;
import static de.latlon.xplan.manager.web.shared.edit.TextRechtscharacterType.BP_VERMERK;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamReader;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.Assertions;
import org.deegree.commons.tom.gml.property.Property;
import org.deegree.commons.tom.primitive.PrimitiveValue;
import org.deegree.feature.Feature;
import org.deegree.feature.FeatureCollection;
import org.deegree.feature.types.AppSchema;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.xmlunit.assertj3.XmlAssert;
import org.xmlunit.builder.Input;

import de.latlon.xplan.commons.XPlanSchemas;
import de.latlon.xplan.commons.XPlanVersion;
import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.commons.feature.XPlanGmlExporter;
import de.latlon.xplan.commons.feature.XPlanGmlParserBuilder;
import de.latlon.xplan.commons.util.XmlUtils;
import de.latlon.xplan.manager.web.shared.XPlan;
import de.latlon.xplan.manager.web.shared.edit.Change;
import de.latlon.xplan.manager.web.shared.edit.RasterBasis;
import de.latlon.xplan.manager.web.shared.edit.RasterReference;
import de.latlon.xplan.manager.web.shared.edit.Reference;
import de.latlon.xplan.manager.web.shared.edit.Text;
import de.latlon.xplan.manager.web.shared.edit.XPlanToEdit;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version $Revision: $, $Date: $
 */
class XPlanManipulatorTest {

	private final XPlanToEditFactory factory = new XPlanToEditFactory();

	private final XPlanManipulator planManipulator = new XPlanManipulator();

	@ParameterizedTest
	@CsvSource({ "xplan51/BP2070.gml,XPLAN_51", "xplan50/BP2070.gml,XPLAN_50",
			"xplan41/Eidelstedt_4_V4-Blankenese.gml,XPLAN_41" })
	void testModifyXPlan(String planResource, String xplanVersion) throws Exception {
		XPlanVersion version = XPlanVersion.valueOf(xplanVersion);
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGml(version, planResource);

		String planName = "newPlanName";
		String description = "newDescription";
		Date creationDate = asDate("2010-01-01");
		Date lossDate = asDate("2020-01-01");
		Date regulationDate = asDate("2006-01-01");
		int legislationStatusCode = 3000;
		int methodCode = 1000;
		int planTypeCode = 10000;
		XPlanToEdit editedXplan = createEditedXplan(planName, description, creationDate, lossDate, regulationDate,
				legislationStatusCode, methodCode, -1, planTypeCode);

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		FeatureCollectionAssert.assertThat(featureCollection).hasProperty(version, "BP_Plan", "name", planName);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(version, "BP_Plan", "beschreibung", description);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(version, "BP_Plan", "technHerstellDatum", creationDate);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(version, "BP_Plan", "untergangsDatum", lossDate);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(version, "BP_Plan", "rechtsverordnungsDatum", regulationDate);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(version, "BP_Plan", "rechtsverordnungsDatum", regulationDate);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(version, "BP_Plan", "rechtsstand", legislationStatusCode);
		FeatureCollectionAssert.assertThat(featureCollection).hasProperty(version, "BP_Plan", "verfahren", methodCode);
		FeatureCollectionAssert.assertThat(featureCollection).hasNoProperty(version, "BP_Plan", "sonstPlanArt");
		FeatureCollectionAssert.assertThat(featureCollection).hasProperty(version, "BP_Plan", "planArt", planTypeCode);

		assertThatPlanIsSchemaValid(featureCollection, version);
	}

	@ParameterizedTest
	@CsvSource({ "xplan51/BP2070.gml,XPLAN_51", "xplan50/BP2070.gml,XPLAN_50",
			"xplan41/Eidelstedt_4_V4-Blankenese.gml,XPLAN_41" })
	void modifyXPlanAenderungen(String planResource, String xplanVersion) throws Exception {
		XPlanVersion version = XPlanVersion.valueOf(xplanVersion);
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGml(version, planResource);

		XPlanToEdit editedXplan = createSimpleXPlan();
		editedXplan.getChanges().add(new Change("planName1", 1000, "eins", CHANGED_BY));
		editedXplan.getChanges().add(new Change("planName2", 1100, "zwei", CHANGES));
		editedXplan.getChanges().add(new Change("planName3", 2000, "drei", CHANGES));

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		FeatureCollectionAssert.assertThat(featureCollection)
			.hasPropertyCount(version, "BP_Plan", "wurdeGeaendertVon", 1);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasPropertyCount(version, "BP_Plan", "wurdeGeaendertVon", 1);
		FeatureCollectionAssert.assertThat(featureCollection).hasPropertyCount(version, "BP_Plan", "aendert", 2);

		assertThatPlanIsSchemaValid(featureCollection, version);
	}

	@ParameterizedTest
	@CsvSource({ "xplan51/BP2070.gml,XPLAN_51", "xplan50/BP2070.gml,XPLAN_50" })
	void modifyXPlanXPlan5XTexte(String planResource, String xplanVersion) throws Exception {
		XPlanVersion version = XPlanVersion.valueOf(xplanVersion);
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGml(version, planResource);

		XPlanToEdit editedXplan = createSimpleXPlan();
		editedXplan.getTexts()
			.add(new Text("id1", "key1", "basis1", "text1", BP_HINWEIS, "reference1", "geoReference1"));
		editedXplan.getTexts()
			.add(new Text("id2", "key2", "basis2", "text2", BP_VERMERK, "reference2", "geoReference2"));

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		FeatureCollectionAssert.assertThat(featureCollection).hasPropertyCount(version, "BP_Plan", "texte", 2);
		FeatureCollectionAssert.assertThat(featureCollection).hasFeatureCount(version, "BP_TextAbschnitt", 2);

		assertThatPlanIsSchemaValid(featureCollection, version);
	}

	@Test
	void modifyXPlanXPlan51ModifyTextKeepFeatureId() throws Exception {
		XPlanVersion version = XPLAN_51;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGml(version, "xplan51/V4_1_ID_103.gml");

		XPlanToEdit editedXplan = createSimpleXPlan();
		String featureIdUnderTest = "FEATURE_0f870967-bd6f-4367-9150-8a255f0290ad";
		editedXplan.getTexts()
			.add(new Text(featureIdUnderTest, "key", "base", "BeschreibungstextNeu", BP_HINWEIS,
					"B-Plan_Klingmuehl_Heideweg_Text", "B-Plan_Klingmuehl_Heideweg_Text.pdf"));

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		FeatureCollectionAssert.assertThat(featureCollection).hasPropertyCount(version, "BP_Plan", "texte", 1);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasFeatureWithId(version, "BP_TextAbschnitt", featureIdUnderTest);

		assertThatPlanIsSchemaValid(featureCollection, version);
	}

	@Test
	void modifyXPlanXPlan41Texte() throws Exception {
		XPlanVersion version = XPLAN_41;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGml(version, "xplan41/V4_1_ID_103.gml");

		XPlanToEdit editedXplan = createSimpleXPlan();
		editedXplan.getTexts().add(new Text("id1", "key1", "basis1", "text1", "reference1", "geoReference1"));
		editedXplan.getTexts().add(new Text("id2", "key2", "basis2", "text2", "reference2", "geoReference2"));

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		FeatureCollectionAssert.assertThat(featureCollection).hasPropertyCount(version, "BP_Plan", "texte", 2);
		FeatureCollectionAssert.assertThat(featureCollection).hasFeatureCount(version, "XP_TextAbschnitt", 2);

		assertThatPlanIsSchemaValid(featureCollection, version);
	}

	@ParameterizedTest
	@CsvSource({ "xplan51/BP2070.gml,XPLAN_51", "xplan50/BP2070.gml,XPLAN_50" })
	void modifyXPlanXPlan50TextWerte(String planResource, String xplanVersion) throws Exception {
		XPlanVersion version = XPlanVersion.valueOf(xplanVersion);
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGml(version, planResource);

		XPlanToEdit editedXplan = createSimpleXPlan();
		Text text = new Text("id1", "key1", "basis1", "text1", BP_VERMERK, "reference1", "geoReference1");
		editedXplan.getTexts().add(text);

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		FeatureCollectionAssert.assertThat(featureCollection).hasPropertyCount(version, "BP_Plan", "texte", 1);
		FeatureCollectionAssert.assertThat(featureCollection).hasFeatureCount(version, "BP_TextAbschnitt", 1);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(version, "BP_TextAbschnitt", "schluessel", text.getKey());
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(version, "BP_TextAbschnitt", "gesetzlicheGrundlage", text.getBasis());
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(version, "BP_TextAbschnitt", "text", text.getText());
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(version, "BP_TextAbschnitt", "rechtscharakter", text.getRechtscharakter().getCode());
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasPropertyCount(version, "BP_TextAbschnitt", "refText", 1);

		assertThatPlanIsSchemaValid(featureCollection, version);
	}

	@Test
	void modifyXPlanXPlan41TextWerte() throws Exception {
		XPlanVersion version = XPLAN_41;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGml(version, "xplan41/V4_1_ID_103.gml");

		XPlanToEdit editedXplan = createSimpleXPlan();
		Text text = new Text("id1", "key1", "basis1", "text1", "reference1", "geoReference1");
		editedXplan.getTexts().add(text);

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		FeatureCollectionAssert.assertThat(featureCollection).hasPropertyCount(version, "BP_Plan", "texte", 1);
		FeatureCollectionAssert.assertThat(featureCollection).hasFeatureCount(version, "XP_TextAbschnitt", 1);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(version, "XP_TextAbschnitt", "schluessel", text.getKey());
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(version, "XP_TextAbschnitt", "gesetzlicheGrundlage", text.getBasis());
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(version, "XP_TextAbschnitt", "text", text.getText());
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasPropertyCount(version, "XP_TextAbschnitt", "refText", 1);

		assertThatPlanIsSchemaValid(featureCollection, version);
	}

	@ParameterizedTest
	@CsvSource({ "xplan51/BP2070.gml,XPLAN_51", "xplan50/BP2070.gml,XPLAN_50" })
	void modifyXPlanXPlan50Referenzen(String planResource, String xplanVersion) throws Exception {
		XPlanVersion version = XPlanVersion.valueOf(xplanVersion);
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGml(version, planResource);

		XPlanToEdit editedXplan = createSimpleXPlan();
		editedXplan.getReferences().add(new Reference("ref1", "georef1", GRUENORDNUNGSPLAN));
		editedXplan.getReferences().add(new Reference("ref2", "georef2", RECHTSPLAN));
		editedXplan.getReferences().add(new Reference("ref3", "georef3", BEGRUENDUNG));

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		FeatureCollectionAssert.assertThat(featureCollection)
			.hasPropertyCount(version, "BP_Plan", "externeReferenz", 3);

		assertThatPlanIsSchemaValid(featureCollection, version);
	}

	@Test
	void modifyXPlanXPlan41Referenzen() throws Exception {
		XPlanVersion version = XPLAN_41;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGml(version, "xplan41/Eidelstedt_4_V4-Blankenese.gml");

		XPlanToEdit editedXplan = createSimpleXPlan();
		Reference reference1 = new Reference("ref1", "georef1", GRUENORDNUNGSPLAN);
		editedXplan.getReferences().add(reference1);
		editedXplan.getReferences().add(new Reference("ref2", "georef2", RECHTSPLAN));
		editedXplan.getReferences().add(new Reference("ref3", "georef3", BEGRUENDUNG));

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		FeatureCollectionAssert.assertThat(featureCollection).hasPropertyCount(version, "BP_Plan", "refBegruendung", 1);
		FeatureCollectionAssert.assertThat(featureCollection).hasPropertyCount(version, "BP_Plan", "refRechtsplan", 1);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasPropertyCount(version, "BP_Plan", "refGruenordnungsplan", 1);

		assertThatPlanIsSchemaValid(featureCollection, version);
	}

	@Test
	void modifyXPlanRasterReferences() throws Exception {
		XPlanVersion version = XPLAN_50;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGmlFromZip("xplan50/BPlan004_5-0.zip");

		XPlanToEdit editedXplan = createSimpleXPlan();
		RasterReference rasterBasisReference = new RasterReference("0", "ref1", "georef1", SCAN, IMAGE_PNG,
				PLANMITGEOREFERENZ, "informationssystemeURL", "refName", TEXT_HTML, "beschreibung",
				asDate("2018-03-01"));

		RasterBasis rasterBasis = new RasterBasis("Gml_FEC4F42F-5D66-4A59-9A47-6E03D1A3139A");
		rasterBasis.setBereichNummer("0");
		rasterBasis.addRasterReference(rasterBasisReference);
		editedXplan.addRasterBasis(rasterBasis);

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		String exportedPlan = exportPlan(featureCollection, version);

		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich/xp:rasterBasis)")
			.isEqualTo("1");
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:XP_Rasterdarstellung)")
			.isEqualTo("1");
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refScan/xp:XP_ExterneReferenz/xp:georefURL")
			.isEqualTo(rasterBasisReference.getGeoReference());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refScan/xp:XP_ExterneReferenz/xp:georefMimeType")
			.isEqualTo(rasterBasisReference.getGeorefMimeType().getCode());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refScan/xp:XP_ExterneReferenz/xp:art")
			.isEqualTo(rasterBasisReference.getArt().getCode());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refScan/xp:XP_ExterneReferenz/xp:informationssystemURL")
			.isEqualTo(rasterBasisReference.getInformationssystemURL());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refScan/xp:XP_ExterneReferenz/xp:referenzName")
			.isEqualTo(rasterBasisReference.getReferenzName());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refScan/xp:XP_ExterneReferenz/xp:referenzURL")
			.isEqualTo(rasterBasisReference.getReference());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refScan/xp:XP_ExterneReferenz/xp:referenzMimeType")
			.isEqualTo(rasterBasisReference.getReferenzMimeType().getCode());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refScan/xp:XP_ExterneReferenz/xp:beschreibung")
			.isEqualTo(rasterBasisReference.getBeschreibung());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refScan/xp:XP_ExterneReferenz/xp:datum")
			.isEqualTo("2018-03-01");

		assertThatPlanIsSchemaValid(exportedPlan, version);
	}

	@Test
	void modifyXPlanXPlan50DeleteRasterReferences() throws Exception {
		XPlanVersion xPlanVersion = XPLAN_50;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(xPlanVersion);
		FeatureCollection featureCollection = readXPlanGmlFromZip("xplan50/BPlan002_5-0.zip");

		XPlanToEdit editedXplan = createSimpleXPlan();
		editedXplan.addRasterBasis(null);

		planManipulator.modifyXPlan(featureCollection, editedXplan, xPlanVersion, BP_Plan, schema);

		String exportedPlan = exportPlan(featureCollection, xPlanVersion);

		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("count(//xp:BP_Bereich/xp:rasterBasis)")
			.isEqualTo("0");
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("count(//xp:XP_Rasterdarstellung)")
			.isEqualTo("0");
		assertThatPlanIsSchemaValid(exportedPlan, xPlanVersion);
	}

	@Test
	void modifyXPlanXPlan50NewRasterReferences() throws Exception {
		XPlanVersion xPlanVersion = XPLAN_50;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(xPlanVersion);
		FeatureCollection featureCollection = readXPlanGml(xPlanVersion, "xplan50/BP2070.gml");

		XPlanToEdit editedXplan = createSimpleXPlan();
		RasterReference scan = new RasterReference("0", "scanRef", "scanGeoRef", SCAN, null, null, null, null, null,
				null, null);
		RasterReference legend = new RasterReference("0", "legendRef", null, LEGEND, null, PLANMITGEOREFERENZ,
				"informationssystemeURL", "refName", IMAGE_PNG, "beschreibung", asDate("2018-03-01"));

		RasterBasis rasterBasis = new RasterBasis();
		rasterBasis.setBereichNummer("0");
		rasterBasis.addRasterReference(scan);
		rasterBasis.addRasterReference(legend);
		editedXplan.addRasterBasis(rasterBasis);

		planManipulator.modifyXPlan(featureCollection, editedXplan, xPlanVersion, BP_Plan, schema);

		String exportedPlan = exportPlan(featureCollection, xPlanVersion);
		assertThatPlanIsSchemaValid(exportedPlan, xPlanVersion);

		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("count(//xp:BP_Bereich/xp:rasterBasis)")
			.isEqualTo("1");
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("count(//xp:XP_Rasterdarstellung)")
			.isEqualTo("1");
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refScan/xp:XP_ExterneReferenz/xp:referenzURL")
			.isEqualTo(scan.getReference());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refScan/xp:XP_ExterneReferenz/xp:georefURL")
			.isEqualTo(scan.getGeoReference());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refLegende/xp:XP_ExterneReferenz/xp:georefMimeType")
			.isEmpty();
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refLegende/xp:XP_ExterneReferenz/xp:art")
			.isEqualTo(legend.getArt().getCode());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refLegende/xp:XP_ExterneReferenz/xp:informationssystemURL")
			.isEqualTo(legend.getInformationssystemURL());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refLegende/xp:XP_ExterneReferenz/xp:referenzName")
			.isEqualTo(legend.getReferenzName());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refLegende/xp:XP_ExterneReferenz/xp:referenzURL")
			.isEqualTo(legend.getReference());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refLegende/xp:XP_ExterneReferenz/xp:referenzMimeType")
			.isEqualTo(legend.getReferenzMimeType().getCode());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refLegende/xp:XP_ExterneReferenz/xp:beschreibung")
			.isEqualTo(legend.getBeschreibung());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("//xp:XP_Rasterdarstellung/xp:refLegende/xp:XP_ExterneReferenz/xp:datum")
			.isEqualTo("2018-03-01");
	}

	@Test
	void modifyXPlanXPlan41RasterReferences() throws Exception {
		XPlanVersion xPlanVersion = XPLAN_41;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(xPlanVersion);
		FeatureCollection featureCollection = readXPlanGml(xPlanVersion, "xplan41/V4_1_ID_103.gml");

		XPlanToEdit editedXplan = createSimpleXPlan();
		RasterReference rasterBasisReference = new RasterReference("0", "ref1", "georef1", SCAN, null, null, null, null,
				null, null, null);

		RasterBasis rasterBasis = new RasterBasis("FEATURE_c2a83b1c-05f4-4dc0-a1b6-feb1a43328d6");
		rasterBasis.setBereichNummer("0");
		rasterBasis.addRasterReference(rasterBasisReference);
		editedXplan.addRasterBasis(rasterBasis);

		planManipulator.modifyXPlan(featureCollection, editedXplan, xPlanVersion, BP_Plan, schema);

		String exportedPlan = exportPlan(featureCollection, xPlanVersion);

		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("count(//xp:BP_Bereich/xp:rasterBasis)")
			.isEqualTo("1");
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("count(//xp:XP_RasterplanBasis)")
			.isEqualTo("1");
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("//xp:XP_RasterplanBasis/xp:refScan/xp:XP_ExterneReferenz/xp:georefURL")
			.isEqualTo(rasterBasisReference.getGeoReference());
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(xPlanVersion))
			.valueByXPath("//xp:XP_RasterplanBasis/xp:refScan/xp:XP_ExterneReferenz/xp:referenzURL")
			.isEqualTo(rasterBasisReference.getReference());
		assertThatPlanIsSchemaValid(exportedPlan, xPlanVersion);
	}

	@ParameterizedTest
	@CsvSource({ "xplan50/BP2070.gml,XPLAN_50", "xplan41/Eidelstedt_4_V4-Blankenese.gml,XPLAN_41" })
	void modifyXPlanXPlan41NullAndEmptyValue(String planResource, String xplanVersion) throws Exception {
		XPlanVersion version = XPlanVersion.valueOf(xplanVersion);
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGml(version, planResource);

		XPlanToEdit editedXplan = createEditedXplan("newPlanName", null, asDate("2010-01-01"), null,
				asDate("2006-01-01"), 3000, -1, -1, 40001);

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		FeatureCollectionAssert.assertThat(featureCollection).hasNoProperty(version, "BP_Plan", "beschreibung");
		FeatureCollectionAssert.assertThat(featureCollection).hasNoProperty(version, "BP_Plan", "untergangsDatum");
		FeatureCollectionAssert.assertThat(featureCollection).hasNoProperty(version, "BP_Plan", "verfahren");

		assertThatPlanIsSchemaValid(featureCollection, version);
	}

	// #3288
	@Test
	void modifyXPlanXPlan41ValidReferences() throws Exception {
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(XPLAN_41);
		FeatureCollection featureCollection = readXPlanGml(XPLAN_41, "xplan41/V4_1_ID_103_references.gml");

		XPlanToEdit editedXplan = factory.createXPlanToEdit(mockXPlan(XPLAN_41), featureCollection);
		editedXplan.getBaseData().setDescription("newDescription");

		planManipulator.modifyXPlan(featureCollection, editedXplan, XPLAN_41, BP_Plan, schema);

		assertThatPlanIsSchemaValid(featureCollection, XPLAN_41);
	}

	@Test
	void modifyXPlanXPlan41TextValuesMultipleReferencesModify() throws Exception {
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(XPLAN_41);
		FeatureCollection featureCollection = readXPlanGml(XPLAN_41, "xplan41/V4_1_ID_103_texts.gml");

		XPlanToEdit editedXplan = factory.createXPlanToEdit(mockXPlan(XPLAN_41), featureCollection);
		retrieveText(editedXplan, "FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6b").setText("newText1");
		retrieveText(editedXplan, "FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6c").setText("newText2");

		planManipulator.modifyXPlan(featureCollection, editedXplan, XPLAN_41, BP_Plan, schema);

		FeatureCollectionAssert.assertThat(featureCollection).hasPropertyCount(XPLAN_41, "BP_Plan", "texte", 2);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasHrefAttribue(XPLAN_41, "BP_Plan", "texte", "#FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6b");
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasHrefAttribue(XPLAN_41, "BP_Plan", "texte", "#FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6c");
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasPropertyCount(XPLAN_41, "BP_BaugebietsTeilFlaeche", "refTextInhalt", 1);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasHrefAttribue(XPLAN_41, "BP_BaugebietsTeilFlaeche", "refTextInhalt",
					"#FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6b");
		FeatureCollectionAssert.assertThat(featureCollection).hasFeatureCount(XPLAN_41, "XP_TextAbschnitt", 2);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(XPLAN_41, "XP_TextAbschnitt", "text", "newText1");
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(XPLAN_41, "XP_TextAbschnitt", "text", "newText2");
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasFeatureWithId(XPLAN_41, "XP_TextAbschnitt", "FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6b");
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasFeatureWithId(XPLAN_41, "XP_TextAbschnitt", "FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6c");

		assertThatPlanIsSchemaValid(featureCollection, XPLAN_41);
	}

	@Test
	void modifyXPlanXPlan41TextValuesMultipleReferencesRemove() throws Exception {
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(XPLAN_41);
		FeatureCollection featureCollection = readXPlanGml(XPLAN_41, "xplan41/V4_1_ID_103_texts.gml");

		XPlanToEdit editedXplan = factory.createXPlanToEdit(mockXPlan(XPLAN_41), featureCollection);
		editedXplan.getTexts().remove(retrieveText(editedXplan, "FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6b"));

		planManipulator.modifyXPlan(featureCollection, editedXplan, XPLAN_41, BP_Plan, schema);

		FeatureCollectionAssert.assertThat(featureCollection).hasPropertyCount(XPLAN_41, "BP_Plan", "texte", 1);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasHrefAttribue(XPLAN_41, "BP_Plan", "texte", "#FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6c");
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasNoProperty(XPLAN_41, "BP_BaugebietsTeilFlaeche", "refTextInhalt");

		FeatureCollectionAssert.assertThat(featureCollection).hasFeatureCount(XPLAN_41, "XP_TextAbschnitt", 1);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasFeatureWithId(XPLAN_41, "XP_TextAbschnitt", "FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6c");

		assertThatPlanIsSchemaValid(featureCollection, XPLAN_41);
	}

	@Test
	void modifyXPlanXPlan41TextValuesMultipleReferencesNew() throws Exception {
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(XPLAN_41);
		FeatureCollection featureCollection = readXPlanGml(XPLAN_41, "xplan41/V4_1_ID_103_texts.gml");

		XPlanToEdit editedXplan = factory.createXPlanToEdit(mockXPlan(XPLAN_41), featureCollection);
		Text newText = new Text(null, "key", "basis", "text", "reference", "geoReference");
		editedXplan.getTexts().add(newText);

		planManipulator.modifyXPlan(featureCollection, editedXplan, XPLAN_41, BP_Plan, schema);

		FeatureCollectionAssert.assertThat(featureCollection).hasPropertyCount(XPLAN_41, "BP_Plan", "texte", 3);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasHrefAttribue(XPLAN_41, "BP_Plan", "texte", "#FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6b");
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasHrefAttribue(XPLAN_41, "BP_Plan", "texte", "#FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6c");
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasPropertyCount(XPLAN_41, "BP_BaugebietsTeilFlaeche", "refTextInhalt", 1);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasHrefAttribue(XPLAN_41, "BP_BaugebietsTeilFlaeche", "refTextInhalt",
					"#FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6b");
		FeatureCollectionAssert.assertThat(featureCollection).hasFeatureCount(XPLAN_41, "XP_TextAbschnitt", 3);
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(XPLAN_41, "XP_TextAbschnitt", "text", "Wiese");
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasProperty(XPLAN_41, "XP_TextAbschnitt", "text", "Weide");
		FeatureCollectionAssert.assertThat(featureCollection).hasProperty(XPLAN_41, "XP_TextAbschnitt", "text", "text");
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasFeatureWithId(XPLAN_41, "XP_TextAbschnitt", "FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6b");
		FeatureCollectionAssert.assertThat(featureCollection)
			.hasFeatureWithId(XPLAN_41, "XP_TextAbschnitt", "FEATURE_0453f54f-620f-40d7-8c1b-d842c6291a6c");

		assertThatPlanIsSchemaValid(featureCollection, XPLAN_41);
	}

	@Test
	void modifyXPlanRasterBasisRefScan() throws Exception {
		XPlanVersion version = XPlanVersion.XPLAN_51;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGml(version, "xplan51/V4_1_ID_103.gml");

		XPlanToEdit editedXplan = createSimpleXPlan();
		RasterReference rasterBasisReference = new RasterReference("0", "FEATURE_c2a83b1c-05f4-4dc0-a1b6-feb1a43328d6",
				"B-Plan_Klingmuehl_Heideweg_Karte.png", "B-Plan_Klingmuehl_Heideweg_Karte.tfw", SCAN, null,
				PLANMITGEOREFERENZ, null, "B-Plan_Klingmuehl_Heideweg_Karte", null, null, null);

		RasterBasis rasterBasis = new RasterBasis("FEATURE_c2a83b1c-05f4-4dc0-a1b6-feb1a43328d6");
		rasterBasis.setBereichNummer("0");
		rasterBasis.addRasterReference(rasterBasisReference);
		editedXplan.addRasterBasis(rasterBasis);

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		String exportedPlan = exportPlan(featureCollection, version);
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich/xp:rasterBasis)")
			.isEqualTo("0");
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:XP_Rasterdarstellung)")
			.isEqualTo("0");
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich/xp:refScan)")
			.isEqualTo("1");
	}

	@Test
	void modifyXPlanRasterBasisRefScanRemove() throws Exception {
		XPlanVersion version = XPlanVersion.XPLAN_51;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGml(version, "xplan51/V4_1_ID_103.gml");

		XPlanToEdit editedXplan = createSimpleXPlan();

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		String exportedPlan = exportPlan(featureCollection, version);

		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich/xp:rasterBasis)")
			.isEqualTo("0");
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich/xp:refScan)")
			.isEqualTo("0");
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:XP_Rasterdarstellung)")
			.isEqualTo("0");
	}

	/**
	 * https://www.jira.geoportal-hamburg.de/browse/XPLANBOX-961 (Testfall 1)
	 */
	@Test
	void modifyXPlanRasterBasisBereiche() throws Exception {
		XPlanVersion version = XPlanVersion.XPLAN_41;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGmlFromZip("xplan41/BPlan001_4-1_Bereiche.zip");

		XPlanToEditFactory xPlanToEditFactory = new XPlanToEditFactory();
		XPlan xPlan = mockXPlan(version);
		XPlanToEdit editedXplan = xPlanToEditFactory.createXPlanToEdit(xPlan, featureCollection);
		RasterBasis rasterBasisBereich0 = editedXplan.getRasterBasis()
			.stream()
			.filter(rasterBasis -> "0".equals(rasterBasis.getBereichNummer()))
			.findFirst()
			.get();
		rasterBasisBereich0.getRasterReferences().clear();
		// editedXplan.getRasterBasis().remove(rasterBasisBereich0);

		// remove rasterbasis from Bereich 0
		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		String exportedPlanUpdate1 = exportPlan(featureCollection, version);

		XmlAssert.assertThat(exportedPlanUpdate1)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich[xp:nummer = '0']/xp:rasterBasis)")
			.isEqualTo("0");
		XmlAssert.assertThat(exportedPlanUpdate1)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich[xp:nummer = '0']/xp:refScan)")
			.isEqualTo("0");
		XmlAssert.assertThat(exportedPlanUpdate1)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich[xp:nummer = '1']/xp:rasterBasis)")
			.isEqualTo("1");
		XmlAssert.assertThat(exportedPlanUpdate1)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich[xp:nummer = '1']/xp:refScan)")
			.isEqualTo("0");
		XmlAssert.assertThat(exportedPlanUpdate1)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:XP_RasterplanBasis)")
			.isEqualTo("1");

		assertThatPlanIsSchemaValid(featureCollection, version);

		RasterReference scan = new RasterReference("0", "scanRef", "scanGeoRef", SCAN, null, null, null, null, null,
				null, null);

		RasterBasis rasterBasisBereich1 = editedXplan.getRasterBasis()
			.stream()
			.filter(rasterBasis -> "1".equals(rasterBasis.getBereichNummer()))
			.collect(Collectors.toList())
			.stream()
			.findFirst()
			.get();
		rasterBasisBereich1.addRasterReference(scan);

		// add rasterbasis to Bereich 1
		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		String exportedPlanUpdate2 = exportPlan(featureCollection, version);

		XmlAssert.assertThat(exportedPlanUpdate2)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich[xp:nummer = '0']/xp:rasterBasis)")
			.isEqualTo("0");
		XmlAssert.assertThat(exportedPlanUpdate2)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich[xp:nummer = '0']/xp:refScan)")
			.isEqualTo("0");
		XmlAssert.assertThat(exportedPlanUpdate2)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich[xp:nummer = '1']/xp:rasterBasis)")
			.isEqualTo("1");
		XmlAssert.assertThat(exportedPlanUpdate2)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich[xp:nummer = '1']/xp:refScan)")
			.isEqualTo("0");
		XmlAssert.assertThat(exportedPlanUpdate2)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:XP_RasterplanBasis)")
			.isEqualTo("1");
		XmlAssert.assertThat(exportedPlanUpdate2)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:XP_RasterplanBasis/xp:refScan)")
			.isEqualTo("2");

		assertThatPlanIsSchemaValid(featureCollection, version);
	}

	@SuppressWarnings("unchecked")
	@Test
	void modifyXPlanFP() throws Exception {
		XPlanVersion version = XPlanVersion.XPLAN_60;
		FeatureCollection featureCollection = readXPlanGmlFromZip("xplan60/FNP_Test_60.zip");
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);

		String planName = "newPlanName";
		String description = "newDescription";
		Date creationDate = asDate("2010-01-01");
		Date lossDate = asDate("2020-01-01");
		int legislationStatusCode = 3000;
		int methodCode = 1000;
		int planTypeCode = 9999;
		XPlanToEdit editedXplan = createEditedXplan(planName, description, creationDate, lossDate, null,
				legislationStatusCode, methodCode, -1, planTypeCode);

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, FP_Plan, schema);

		String exportedPlan = exportPlan(featureCollection, version);

		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:FP_Plan/xp:name")
			.isEqualTo(planName);
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:FP_Plan/xp:beschreibung")
			.isEqualTo(description);
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:FP_Plan/xp:technHerstellDatum")
			.isEqualTo(asString(creationDate));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:FP_Plan/xp:untergangsDatum")
			.isEqualTo(asString(lossDate));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:FP_Plan/xp:rechtsstand")
			.isEqualTo(Integer.toString(legislationStatusCode));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:FP_Plan/xp:verfahren")
			.isEmpty();
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:FP_Plan/xp:planArt")
			.isEqualTo(Integer.toString(planTypeCode));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:FP_Plan/xp:sonstPlanArt")
			.isEmpty();

		XmlAssert.assertThat(exportedPlan).isValidAgainst(Input.fromURI(version.getSchemaUrl().toURI()));
	}

	@SuppressWarnings("unchecked")
	@Test
	void modifyXPlanLP() throws Exception {
		XPlanVersion version = XPlanVersion.XPLAN_60;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGmlFromZip("xplan60/LP-Test_60.zip");

		String planName = "newPlanName";
		String description = "newDescription";
		Date creationDate = asDate("2010-01-01");
		Date lossDate = asDate("2020-01-01");
		int legislationStatusCode = 3000;
		int methodCode = 1000;
		int planTypeCode = 9999;
		XPlanToEdit editedXplan = createEditedXplan(planName, description, creationDate, lossDate, null,
				legislationStatusCode, methodCode, -1, planTypeCode);

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, LP_Plan, schema);

		String exportedPlan = exportPlan(featureCollection, version);

		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:LP_Plan/xp:name")
			.isEqualTo(planName);
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:LP_Plan/xp:beschreibung")
			.isEqualTo(description);
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:LP_Plan/xp:technHerstellDatum")
			.isEqualTo(asString(creationDate));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:LP_Plan/xp:untergangsDatum")
			.isEqualTo(asString(lossDate));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:LP_Plan/xp:rechtsstand")
			.isEqualTo(Integer.toString(legislationStatusCode));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:LP_Plan/xp:verfahren")
			.isEmpty();
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:LP_Plan/xp:planArt")
			.isEqualTo(Integer.toString(planTypeCode));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:LP_Plan/xp:sonstPlanArt")
			.isEmpty();

		XmlAssert.assertThat(exportedPlan).isValidAgainst(Input.fromURI(version.getSchemaUrl().toURI()));
	}

	@SuppressWarnings("unchecked")
	@Test
	void modifyXPlanRP() throws Exception {
		XPlanVersion version = XPLAN_51;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGmlFromZip("xplan51/RROP_Landkreis_Test_51.zip");

		String planName = "newPlanName";
		String description = "newDescription";
		Date creationDate = asDate("2010-01-01");
		Date lossDate = asDate("2020-01-01");
		int legislationStatusCode = 3000;
		int methodCode = 1000;
		int planTypeCode = 9999;
		XPlanToEdit editedXplan = createEditedXplan(planName, description, creationDate, lossDate, null,
				legislationStatusCode, methodCode, -1, planTypeCode);

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, RP_Plan, schema);

		String exportedPlan = exportPlan(featureCollection, version);

		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:RP_Plan/xp:name")
			.isEqualTo(planName);
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:RP_Plan/xp:beschreibung")
			.isEqualTo(description);
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:RP_Plan/xp:technHerstellDatum")
			.isEqualTo(asString(creationDate));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:RP_Plan/xp:untergangsDatum")
			.isEqualTo(asString(lossDate));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:RP_Plan/xp:rechtsstand")
			.isEqualTo(Integer.toString(legislationStatusCode));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:RP_Plan/xp:verfahren")
			.isEqualTo(Integer.toString(methodCode));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:RP_Plan/xp:planArt")
			.isEqualTo(Integer.toString(planTypeCode));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:RP_Plan/xp:sonstPlanArt")
			.isEmpty();

		XmlAssert.assertThat(exportedPlan).isValidAgainst(Input.fromURI(version.getSchemaUrl().toURI()));
	}

	@SuppressWarnings("unchecked")
	@Test
	void modifyXPlanSO() throws Exception {
		XPlanVersion version = XPlanVersion.XPLAN_60;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGmlFromZip("xplan60/StErhVO_Hamm_60.zip");

		String planName = "newPlanName";
		String description = "newDescription";
		Date creationDate = asDate("2010-01-01");
		Date lossDate = asDate("2020-01-01");
		int planTypeCode = 9999;
		XPlanToEdit editedXplan = createEditedXplan(planName, description, creationDate, lossDate, null, -1, -1, -1,
				planTypeCode);

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, SO_Plan, schema);

		String exportedPlan = exportPlan(featureCollection, version);

		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:SO_Plan/xp:name")
			.isEqualTo(planName);
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:SO_Plan/xp:beschreibung")
			.isEqualTo(description);
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:SO_Plan/xp:technHerstellDatum")
			.isEqualTo(asString(creationDate));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:SO_Plan/xp:untergangsDatum")
			.isEqualTo(asString(lossDate));
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:SO_Plan/xp:rechtsstand")
			.isEmpty();
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:SO_Plan/xp:verfahren")
			.isEmpty();
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:SO_Plan/xp:planArt")
			.isEqualTo("17200");
		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("//xp:SO_Plan/xp:sonstPlanArt")
			.isEmpty();

		XmlAssert.assertThat(exportedPlan).isValidAgainst(Input.fromURI(version.getSchemaUrl().toURI()));
	}

	/**
	 * https://www.jira.geoportal-hamburg.de/browse/XPLANBOX-961 (Testfall 1)
	 */
	@Test
	void modifyXPlanRasterBasisBP60() throws Exception {
		XPlanVersion version = XPlanVersion.XPLAN_60;
		AppSchema schema = XPlanSchemas.getInstance().getAppSchema(version);
		FeatureCollection featureCollection = readXPlanGmlFromZip("xplan60/BPlan001_6-0.zip");

		XPlanToEditFactory xPlanToEditFactory = new XPlanToEditFactory();
		XPlan xPlan = mockXPlan(version);

		XPlanToEdit editedXplan = xPlanToEditFactory.createXPlanToEdit(xPlan, featureCollection);

		// add rasterbasis
		RasterReference scan = new RasterReference("0", "scanRef", "scanGeoRef", SCAN, null, null, null, "test", null,
				null, null);
		RasterBasis rasterBasis = editedXplan.getRasterBasis().get(0);
		rasterBasis.addRasterReference(scan);

		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		String exportedPlanAddRasterBasis = exportPlan(featureCollection, version);
		XmlAssert.assertThat(exportedPlanAddRasterBasis)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich[xp:nummer = '1']/xp:refScan)")
			.isEqualTo("1");

		assertThatPlanIsSchemaValid(featureCollection, version);

		// remove rasterbasis
		editedXplan.getRasterBasis().clear();
		planManipulator.modifyXPlan(featureCollection, editedXplan, version, BP_Plan, schema);

		String exportedPlanRemoveRasterBasis = exportPlan(featureCollection, version);

		XmlAssert.assertThat(exportedPlanRemoveRasterBasis)
			.withNamespaceContext(nsContext(version))
			.valueByXPath("count(//xp:BP_Bereich[xp:nummer = '1']/xp:refScan)")
			.isEqualTo("0");

		assertThatPlanIsSchemaValid(featureCollection, version);
	}

	private XPlanToEdit createSimpleXPlan() {
		XPlanToEdit editedXplan = new XPlanToEdit();
		editedXplan.getBaseData().setPlanName("planName");
		editedXplan.getBaseData().setPlanTypeCode(40000);
		return editedXplan;
	}

	private XPlanToEdit createEditedXplan(String planName, String description, Date creationDate, Date lossDate,
			Date regulationDate, int legislationStatusCode, int methodCode, int otherPlanTypeCode, int planTypeCode) {
		XPlanToEdit editedXplan = new XPlanToEdit();
		editedXplan.getBaseData().setPlanName(planName);
		editedXplan.getBaseData().setDescription(description);
		editedXplan.getBaseData().setCreationDate(creationDate);
		editedXplan.getBaseData().setLossDate(lossDate);
		editedXplan.getBaseData().setRegulationDate(regulationDate);
		editedXplan.getBaseData().setLegislationStatusCode(legislationStatusCode);
		editedXplan.getBaseData().setMethodCode(methodCode);
		editedXplan.getBaseData().setOtherPlanTypeCode(otherPlanTypeCode);
		editedXplan.getBaseData().setPlanTypeCode(planTypeCode);
		return editedXplan;
	}

	private void assertThatPlanIsSchemaValid(FeatureCollection featureCollection, XPlanVersion version)
			throws Exception {
		String exportedPlan = exportPlan(featureCollection, version);
		assertThatPlanIsSchemaValid(exportedPlan, version);
	}

	@SuppressWarnings("unchecked")
	private void assertThatPlanIsSchemaValid(String exportedPlan, XPlanVersion version) throws Exception {
		XmlAssert.assertThat(exportedPlan).isValidAgainst(Input.fromURI(version.getSchemaUrl().toURI()));
	}

	private String exportPlan(FeatureCollection featureCollection, XPlanVersion version) throws Exception {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		XPlanGmlExporter.export(outputStream, version, featureCollection, null);
		return new String(outputStream.toByteArray());
	}

	private Map<String, String> nsContext(XPlanVersion version) {
		Map<String, String> nsContext = new HashMap<>();
		nsContext.put("xplan", XPLAN_41.getNamespace());
		nsContext.put("xp", version.getNamespace());
		nsContext.put("gml", version.getGmlVersion().getNamespace());
		nsContext.put("xlink", "http://www.w3.org/1999/xlink");
		return nsContext;
	}

	private FeatureCollection readXPlanGml(XPlanVersion xplanVersion, String plan) throws Exception {
		InputStream xplanGml = this.getClass().getResourceAsStream(plan);
		XMLStreamReader reader = XmlUtils.createXMLInputFactory().createXMLStreamReader(xplanGml);
		return XPlanGmlParserBuilder.newBuilder().build().parseFeatureCollection(reader, xplanVersion);
	}

	private FeatureCollection readXPlanGmlFromZip(String resource) throws Exception {
		InputStream resourceAsStream = getClass().getResourceAsStream("/testdata/" + resource);
		XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();
		XPlanArchive xPlanArchive = archiveCreator.createXPlanArchiveFromZip(resource, resourceAsStream);
		return XPlanGmlParserBuilder.newBuilder().build().parseFeatureCollection(xPlanArchive);
	}

	private Text retrieveText(XPlanToEdit xplanToEdit, String featureId) {
		for (Text text : xplanToEdit.getTexts()) {
			if (featureId.equals(text.getFeatureId()))
				return text;
		}
		return null;
	}

	private XPlan mockXPlan(XPlanVersion version) {
		XPlan mock = mock(XPlan.class);
		when(mock.getVersion()).thenReturn(version.toString());
		return mock;
	}

	private Date asDate(String string) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return simpleDateFormat.parse(string);
	}

	private String asString(Date date) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return simpleDateFormat.format(date);
	}

	static class FeatureCollectionAssert extends AbstractAssert<FeatureCollectionAssert, FeatureCollection> {

		private interface PropertyMatcher {

			boolean matches(List<Property> properties);

		}

		protected FeatureCollectionAssert(FeatureCollection actual) {
			super(actual, FeatureCollectionAssert.class);
		}

		public void hasHrefAttribue(final XPlanVersion version, final String expectedFeature,
				final String expectedProperty, final String expectedHrefValue) {

			hasProperty(version, expectedFeature, expectedProperty, new PropertyMatcher() {

				private final QName xlinkHrefAttribute = new QName("http://www.w3.org/1999/xlink", "href");

				@Override
				public boolean matches(List<Property> properties) {
					for (Property property : properties) {
						PrimitiveValue hrefValue = property.getAttributes().get(xlinkHrefAttribute);
						if (hrefValue != null && hrefValue.toString().equals(expectedHrefValue))
							return true;
					}
					return false;
				}
			});
		}

		public void hasFeatureWithId(final XPlanVersion version, final String expectedFeature,
				final String expectedFeatureId) {

			QName qname = new QName(version.getNamespace(), expectedFeature);
			for (Feature feature : actual) {
				if (feature.getName().equals(qname)) {
					String id = feature.getId();
					if (expectedFeatureId.equals(id))
						return;
				}
			}
			failure("Expected feature with name %s and id %s not found", expectedFeature, expectedFeatureId);
		}

		public void hasFeatureCount(final XPlanVersion version, final String expectedFeature,
				final int numberOfOccurences) {

			QName qname = new QName(version.getNamespace(), expectedFeature);
			Iterator<Feature> iterator = actual.iterator();
			int numberOfExpectedFeatures = 0;
			while (iterator.hasNext()) {
				Feature feature = iterator.next();
				if (feature.getName().equals(qname))
					numberOfExpectedFeatures++;
			}

			if (numberOfExpectedFeatures != numberOfOccurences) {
				failureWithActualExpected(numberOfOccurences, numberOfExpectedFeatures,
						"Unexpected number of feature with name %s", expectedFeature);
			}
		}

		private boolean hasProperty(final XPlanVersion version, final String expectedFeature,
				final String expectedProperty, final PropertyMatcher propertyMatcher) {

			for (Feature feature : actual) {
				if (feature.getName().equals(new QName(version.getNamespace(), expectedFeature))) {
					List<Property> properties = feature
						.getProperties(new QName(version.getNamespace(), expectedProperty));
					if (propertyMatcher.matches(properties))
						return true;
				}
			}
			return false;
		}

		public void hasProperty(final XPlanVersion version, final String expectedFeature, final String expectedProperty,
				final Date expectedValue) {

			PropertyMatcher matcher = new PropertyMatcher() {
				@Override
				public boolean matches(List<Property> properties) {
					if (properties.size() != 1)
						return false;
					PrimitiveValue value = (PrimitiveValue) properties.get(0).getValue();
					org.deegree.commons.tom.datetime.Date propertyValue = (org.deegree.commons.tom.datetime.Date) value
						.getValue();
					return expectedValue.equals(new Date(propertyValue.getTimeInMilliseconds()));
				}

			};
			if (!hasProperty(version, expectedFeature, expectedProperty, matcher)) {
				throw failure("No feature %s found with property %s having value %s", expectedFeature, expectedProperty,
						expectedValue);
			}

		}

		public void hasProperty(final XPlanVersion version, final String expectedFeature, final String expectedProperty,
				final int expectedValue) {
			PropertyMatcher matcher = new PropertyMatcher() {

				@Override
				public boolean matches(List<Property> properties) {
					if (properties.size() != 1)
						return false;
					PrimitiveValue value = (PrimitiveValue) properties.get(0).getValue();
					return expectedValue == Integer.parseInt(value.getAsText());
				}

			};
			if (!hasProperty(version, expectedFeature, expectedProperty, matcher)) {
				throw failure("No feature %s found with property %s having value %s", expectedFeature, expectedProperty,
						expectedValue);
			}
		}

		public void hasNoProperty(final XPlanVersion version, final String expectedFeature,
				final String expectedProperty) {
			Iterator<Feature> iterator = actual.iterator();
			while (iterator.hasNext()) {
				Feature feature = iterator.next();
				if (feature.getName().equals(new QName(version.getNamespace(), expectedFeature))) {
					List<Property> properties = feature
						.getProperties(new QName(version.getNamespace(), expectedProperty));
					if (!properties.isEmpty()) {
						throw failureWithActualExpected(Collections.emptyList(), properties,
								"Found properties %s for feature %s", expectedProperty, expectedFeature);
					}
					else {
						return;
					}
				}
			}
			;
		}

		public void hasProperty(final XPlanVersion version, final String expectedFeature, final String expectedProperty,
				final String expectedValue) {

			PropertyMatcher matcher = new PropertyMatcher() {
				@Override
				public boolean matches(List<Property> properties) {
					if (properties.size() != 1)
						return false;
					String propertyValue = properties.get(0).getValue().toString();
					return propertyValue.equals(expectedValue);
				}

			};

			if (!hasProperty(version, expectedFeature, expectedProperty, matcher)) {
				throw failure("No feature %s found with property %s having value %s", expectedFeature, expectedProperty,
						expectedValue);
			}
		}

		public void hasPropertyCount(XPlanVersion version, final String expectedFeature, final String expectedProperty,
				final int numberOfOccurences) {

			Iterator<Feature> iterator = actual.iterator();
			while (iterator.hasNext()) {
				Feature feature = iterator.next();
				if (feature.getName().equals(new QName(version.getNamespace(), expectedFeature))) {
					List<Property> properties = feature
						.getProperties(new QName(version.getNamespace(), expectedProperty));
					Assertions.assertThat(properties)
						.as("Property %s of feature %s", expectedProperty, expectedFeature)
						.hasSize(numberOfOccurences);
					return;
				}
			}
			throw failure("No feature >%s< found with property >%s<", expectedFeature, expectedProperty);
		}

		public static FeatureCollectionAssert assertThat(FeatureCollection featureCollection) {
			return new FeatureCollectionAssert(featureCollection);
		}

	}

}
