/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.wmsconfig.raster.storage.s3;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.util.Collections;
import java.util.List;

import de.latlon.xplan.commons.archive.ArchiveEntry;
import de.latlon.xplan.commons.archive.XPlanArchiveContentAccess;
import de.latlon.xplan.commons.s3.StorageException;
import de.latlon.xplan.manager.storage.StorageEvent;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectResponse;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.HeadBucketRequest;
import software.amazon.awssdk.services.s3.model.HeadBucketResponse;
import software.amazon.awssdk.services.s3.model.ListObjectsRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsResponse;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;
import software.amazon.awssdk.services.s3.model.S3Object;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
class S3RasterStorageTest {

	private static final String BUCKET_NAME = "xplanbox";

	@Test
	void testAddRasterFile() throws StorageException {
		S3Client client = createS3ClientSpy();
		S3RasterStorage s3RasterStorage = new S3RasterStorage(client, BUCKET_NAME);
		XPlanArchiveContentAccess archive = mockArchive();

		StorageEvent storageEvent = mock(StorageEvent.class);
		String key = s3RasterStorage.addRasterFile(1, "test.png", "test.png.aux.xml", archive, storageEvent);

		ArgumentCaptor<HeadBucketRequest> headBucketRequestCaptor = ArgumentCaptor.forClass(HeadBucketRequest.class);
		ArgumentCaptor<PutObjectRequest> putObjectRequestCaptor = ArgumentCaptor.forClass(PutObjectRequest.class);

		assertEquals("1_test.png", key);

		verify(client, times(2)).headBucket(headBucketRequestCaptor.capture());
		verify(client, times(2)).putObject(putObjectRequestCaptor.capture(), any(RequestBody.class));
		assertThat(headBucketRequestCaptor.getAllValues()).map(HeadBucketRequest::bucket)
			.containsExactlyInAnyOrder(BUCKET_NAME, BUCKET_NAME);
		assertThat(putObjectRequestCaptor.getAllValues()).map(PutObjectRequest::bucket)
			.contains(BUCKET_NAME, BUCKET_NAME);
		assertThat(putObjectRequestCaptor.getAllValues()).map(PutObjectRequest::key)
			.contains("1_test.png", "1_test.png.aux.xml");

		verify(storageEvent).addInsertedKey(eq("1_test.png"));
	}

	@Test
	void testDeleteRasterFile() throws StorageException {
		S3Client client = spy(S3Client.class);
		ListObjectsResponse objectListing = mock(ListObjectsResponse.class);
		doReturn(objectListing).when(client)
			.listObjects(eq(ListObjectsRequest.builder().bucket(BUCKET_NAME).prefix("1_test.png").build()));
		ResponseInputStream<GetObjectResponse> objectStream = mock(ResponseInputStream.class);
		GetObjectResponse getObjectResponse = mock(GetObjectResponse.class);
		when(getObjectResponse.contentType()).thenReturn("image/png");
		when(getObjectResponse.contentLength()).thenReturn(90l);
		when(objectStream.response()).thenReturn(getObjectResponse);
		doReturn(objectStream).when(client).getObject(any(GetObjectRequest.class));
		doReturn(DeleteObjectResponse.builder().build()).when(client).deleteObject(any(DeleteObjectRequest.class));

		S3Object objectToDelete = mock(S3Object.class);
		when(objectToDelete.key()).thenReturn("1_test.png");
		List<S3Object> objectSummaries = Collections.singletonList(objectToDelete);
		when(objectListing.contents()).thenReturn(objectSummaries);

		S3RasterStorage s3RasterStorage = new S3RasterStorage(client, BUCKET_NAME);

		StorageEvent storageEvent = mock(StorageEvent.class);
		s3RasterStorage.deleteRasterFile(1, "test.png", storageEvent);

		verify(client).deleteObject(DeleteObjectRequest.builder().bucket(BUCKET_NAME).key("1_test.png").build());
		ArgumentCaptor<de.latlon.xplan.commons.s3.S3Object> argument = ArgumentCaptor
			.forClass(de.latlon.xplan.commons.s3.S3Object.class);
		verify(storageEvent).addDeletedKey(argument.capture());
		assertEquals("1_test.png", argument.getValue().getS3Metadata().getKey());
	}

	private XPlanArchiveContentAccess mockArchive() {
		XPlanArchiveContentAccess mock = mock(XPlanArchiveContentAccess.class);
		ArchiveEntry referenceEntry = mock(ArchiveEntry.class);
		when(referenceEntry.getContentLength()).thenReturn(90l);
		when(referenceEntry.getContentType()).thenReturn("image/png");
		when(mock.getEntry("test.png")).thenReturn(referenceEntry);
		when(mock.retrieveInputStreamFor("test.png")).thenReturn(new ByteArrayInputStream("test.png".getBytes()));

		ArchiveEntry georefEntry = mock(ArchiveEntry.class);
		when(georefEntry.getContentLength()).thenReturn(10l);
		when(georefEntry.getContentType()).thenReturn("application/xml");
		when(mock.getEntry("test.png.aux.xml")).thenReturn(georefEntry);
		when(mock.retrieveInputStreamFor("test.png.aux.xml"))
			.thenReturn(new ByteArrayInputStream("test.png.aux.xml".getBytes()));
		return mock;
	}

	private static S3Client createS3ClientSpy() {
		S3Client client = spy(S3Client.class);
		doReturn(HeadBucketResponse.builder().build()).when(client).headBucket(any(HeadBucketRequest.class));
		doReturn(PutObjectResponse.builder().build()).when(client)
			.putObject(any(PutObjectRequest.class), any(RequestBody.class));
		return client;
	}

}
