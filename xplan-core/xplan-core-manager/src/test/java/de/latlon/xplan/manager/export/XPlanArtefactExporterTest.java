/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.export;

import static org.apache.commons.io.IOUtils.copyLarge;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.deegree.cs.coordinatesystems.ICRS;
import org.deegree.cs.persistence.CRSManager;
import org.deegree.feature.FeatureCollection;
import org.junit.jupiter.api.Test;

import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import de.latlon.xplan.commons.feature.XPlanFeatureCollection;
import de.latlon.xplan.commons.feature.XPlanGmlParserBuilder;
import de.latlon.xplan.core.manager.db.model.Artefact;
import de.latlon.xplan.core.manager.db.model.ArtefactId;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version $Revision: $, $Date: $
 */
class XPlanArtefactExporterTest {

	@Test
	void testExport() throws Exception {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		List<Artefact> artefacts = createArtefacts();
		XPlanArtefactExporter.export(outputStream, artefacts);

		List<String> exportedFiles = readExportedContent(outputStream);

		assertThat(exportedFiles).containsExactlyInAnyOrder("1.xml", "2.xml");
	}

	@Test
	void exportWithNullManagerConfiguration() throws Exception {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		List<Artefact> artefacts = createArtefacts();
		XPlanArtefactExporter.export(outputStream, artefacts);

		List<String> exportedFiles = readExportedContent(outputStream);

		assertThat(exportedFiles).containsExactlyInAnyOrder("1.xml", "2.xml");
	}

	private List<Artefact> createArtefacts() throws Exception {
		Artefact artefact1 = artefact("1.xml");
		Artefact artefact2 = artefact("2.xml");
		return List.of(artefact1, artefact2);
	}

	private Artefact artefact(String name) throws IOException {
		Artefact xPlanArtefact = mock(Artefact.class);
		ArtefactId xPlanArtefactId = mock(ArtefactId.class);
		when(xPlanArtefactId.getFilename()).thenReturn(name);
		when(xPlanArtefact.getId()).thenReturn(xPlanArtefactId);
		byte[] content = createZippedContent(name);
		when(xPlanArtefact.getData()).thenReturn(content);
		return xPlanArtefact;
	}

	private List<String> readExportedContent(ByteArrayOutputStream outputStream) throws Exception {
		List<String> exportedFiles = new ArrayList<String>();
		ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(outputStream.toByteArray()));
		ZipEntry entry;
		while ((entry = zipInputStream.getNextEntry()) != null) {
			exportedFiles.add(entry.getName());
		}
		return exportedFiles;
	}

	private XPlanArchive createArchive(String testArchiveName) throws IOException {
		XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();
		InputStream archiveResource = getClass().getResourceAsStream("/testdata/" + testArchiveName);
		if (testArchiveName.endsWith(".gml"))
			return archiveCreator.createXPlanArchiveFromGml(testArchiveName, archiveResource);
		return archiveCreator.createXPlanArchiveFromZip(testArchiveName, archiveResource);
	}

	private XPlanFeatureCollection readFeatures(XPlanArchive archive) throws Exception {
		ICRS defaultCrs = CRSManager.lookup("EPSG:31467");
		if (archive.getCrs() != null)
			defaultCrs = archive.getCrs();
		return XPlanGmlParserBuilder.newBuilder()
			.withDefaultCrs(defaultCrs)
			.build()
			.parseXPlanFeatureCollection(archive);
	}

	private byte[] createZippedContent(String name) throws IOException {
		InputStream is = new ByteArrayInputStream(name.getBytes());
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		GZIPOutputStream gos = new GZIPOutputStream(bos);
		copyLarge(is, gos);
		gos.close();
		return bos.toByteArray();
	}

}
