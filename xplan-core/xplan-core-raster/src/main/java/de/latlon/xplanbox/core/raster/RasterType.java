/*-
 * #%L
 * xplan-core-raster - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.core.raster;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public enum RasterType {

	TIFF("image/tiff"), PNG("image/png"), UNKNOWN;

	private final String mediaType;

	RasterType(String mediaType) {
		this.mediaType = mediaType;
	}

	RasterType() {
		this(null);
	}

	public static RasterType fromMediaType(String mediaType) {
		for (RasterType rasterType : values()) {
			if (rasterType.mediaType != null && rasterType.mediaType.equals(mediaType)) {
				return rasterType;
			}
		}
		return UNKNOWN;
	}

}
