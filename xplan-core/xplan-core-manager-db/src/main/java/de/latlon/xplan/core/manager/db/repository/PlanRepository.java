/*-
 * #%L
 * xplan-core-manager-db - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.core.manager.db.repository;

import java.util.List;

import de.latlon.xplan.core.manager.db.model.Plan;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 7.0
 */
@Repository
@Transactional
public interface PlanRepository extends CrudRepository<Plan, Integer>, JpaSpecificationExecutor<Plan> {

	List<Plan> findByName(String name);

	@Query(value = "from Plan as p where (lower(p.name) like lower(concat('%', :name, '%')) or :name is null) "
			+ "and (p.internalid = :internalid or :internalid is null) " + "and p.planstatus in :planStatuses")
	List<Plan> findBySearchParams(@Param("name") String name, @Param("internalid") String internalid,
			@Param("planStatuses") List<String> planStatuses);

	@Query(value = "from Plan as p where (lower(p.name) like lower(concat('%', :name, '%')) or :name is null) "
			+ "and (p.internalid = :internalid or :internalid is null)")
	List<Plan> findBySearchParams(@Param("name") String name, @Param("internalid") String internalid);

	boolean existsPlanById(Integer id);

	boolean existsPlanByNameAndPlanstatus(String name, String planstatus);

}
