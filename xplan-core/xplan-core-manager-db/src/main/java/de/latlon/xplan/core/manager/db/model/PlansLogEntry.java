package de.latlon.xplan.core.manager.db.model;

import java.time.LocalDateTime;

import de.latlon.xplan.commons.XPlanType;
import de.latlon.xplan.commons.XPlanVersion;
import de.latlon.xplan.core.manager.db.converter.PlanStatusConverter;
import de.latlon.xplan.manager.web.shared.PlanStatus;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.locationtech.jts.geom.Geometry;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.1
 */
@Entity
@Table(schema = "xplanmgr", name = "planslog")
public class PlansLogEntry {

	@Id
	private @Valid Integer id;

	@Column(name = "plan_id")
	private @Valid Integer planId;

	@NotNull
	@Column(name = "xp_version")
	@Enumerated(EnumType.STRING)
	private @Valid XPlanVersion version;

	@NotNull
	@Column(name = "xp_type")
	@Enumerated(EnumType.STRING)
	private @Valid XPlanType type;

	private @Valid Geometry bbox;

	@Column(name = "planstatus_new")
	@Convert(converter = PlanStatusConverter.class)
	private @Valid PlanStatus planstatusNew;

	@Column(name = "planstatus_old")
	@Convert(converter = PlanStatusConverter.class)
	private @Valid PlanStatus planstatusOld;

	@Enumerated(EnumType.STRING)
	private @Valid OperationType operation;

	@Column(name = "last_update_date")
	private @Valid LocalDateTime lastUpdateDate;

	public PlansLogEntry id(Integer id) {
		this.id = id;
		return this;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PlansLogEntry planId(Integer planId) {
		this.planId = planId;
		return this;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public PlansLogEntry version(XPlanVersion version) {
		this.version = version;
		return this;
	}

	public XPlanVersion getVersion() {
		return version;
	}

	public void setVersion(XPlanVersion version) {
		this.version = version;
	}

	public PlansLogEntry type(XPlanType type) {
		this.type = type;
		return this;
	}

	public XPlanType getType() {
		return type;
	}

	public void setType(XPlanType type) {
		this.type = type;
	}

	public PlansLogEntry bbox(Geometry bbox) {
		this.bbox = bbox;
		return this;
	}

	public Geometry getBbox() {
		return bbox;
	}

	public void setBbox(Geometry bbox) {
		this.bbox = bbox;
	}

	public PlansLogEntry planstatusNew(PlanStatus planstatusNew) {
		this.planstatusNew = planstatusNew;
		return this;
	}

	public PlanStatus getPlanstatusNew() {
		return planstatusNew;
	}

	public void setPlanstatusNew(PlanStatus planstatusNew) {
		this.planstatusNew = planstatusNew;
	}

	public PlansLogEntry planstatusOld(PlanStatus planstatusOld) {
		this.planstatusOld = planstatusOld;
		return this;
	}

	public PlanStatus getPlanstatusOld() {
		return planstatusOld;
	}

	public void setPlanstatusOld(PlanStatus planstatusOld) {
		this.planstatusOld = planstatusOld;
	}

	public PlansLogEntry operation(OperationType operation) {
		this.operation = operation;
		return this;
	}

	public OperationType getOperation() {
		return operation;
	}

	public void setOperation(OperationType operation) {
		this.operation = operation;
	}

	public PlansLogEntry lastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
		return this;
	}

	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

}
