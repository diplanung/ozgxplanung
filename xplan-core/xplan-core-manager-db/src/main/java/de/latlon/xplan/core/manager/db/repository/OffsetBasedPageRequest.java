package de.latlon.xplan.core.manager.db.repository;

import java.util.Objects;
import java.util.Optional;

import org.springframework.data.domain.Limit;
import org.springframework.data.domain.OffsetScrollPosition;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.1
 */
public class OffsetBasedPageRequest implements Pageable {

	private int limit;

	private int offset;

	private final Sort sort;

	private OffsetBasedPageRequest(int offset, int limit, Sort sort) {
		if (offset < 0) {
			throw new IllegalArgumentException("Offset must be greater or equal than 0!");
		}

		if (limit < 1) {
			throw new IllegalArgumentException("Limit must be greater than 0!");
		}
		this.limit = limit;
		this.offset = offset;
		this.sort = sort;
	}

	/**
	 * @param offset zero-based offset, must be >=0
	 * @param limit number of elements to be returned, must be >=1
	 * @param sort may be {@literal null}.
	 */
	public static OffsetBasedPageRequest withOffsetAndLimit(int offset, int limit, Sort sort) {
		return new OffsetBasedPageRequest(offset, limit, sort);
	}

	@Override
	public boolean isPaged() {
		return Pageable.super.isPaged();
	}

	@Override
	public boolean isUnpaged() {
		return Pageable.super.isUnpaged();
	}

	@Override
	public int getPageNumber() {
		return offset / limit;
	}

	@Override
	public int getPageSize() {
		return limit;
	}

	@Override
	public long getOffset() {
		return offset;
	}

	@Override
	public Sort getSort() {
		return sort;
	}

	@Override
	public Sort getSortOr(Sort sort) {
		return Pageable.super.getSortOr(sort);
	}

	@Override
	public Pageable next() {
		return new OffsetBasedPageRequest(offset + getPageSize(), getPageSize(), getSort());
	}

	@Override
	public Pageable previousOrFirst() {
		return hasPrevious() ? previous() : first();
	}

	@Override
	public Pageable first() {
		return new OffsetBasedPageRequest(0, getPageSize(), getSort());
	}

	@Override
	public Pageable withPage(int pageNumber) {
		return null;
	}

	@Override
	public boolean hasPrevious() {
		return offset > limit;
	}

	@Override
	public Optional<Pageable> toOptional() {
		return Pageable.super.toOptional();
	}

	@Override
	public Limit toLimit() {
		return Pageable.super.toLimit();
	}

	@Override
	public OffsetScrollPosition toScrollPosition() {
		return Pageable.super.toScrollPosition();
	}

	public OffsetBasedPageRequest previous() {
		return hasPrevious() ? new OffsetBasedPageRequest(offset - getPageSize(), getPageSize(), getSort()) : this;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || getClass() != o.getClass())
			return false;
		OffsetBasedPageRequest that = (OffsetBasedPageRequest) o;
		return limit == that.limit && offset == that.offset && Objects.equals(sort, that.sort);
	}

	@Override
	public int hashCode() {
		return Objects.hash(limit, offset, sort);
	}

}
