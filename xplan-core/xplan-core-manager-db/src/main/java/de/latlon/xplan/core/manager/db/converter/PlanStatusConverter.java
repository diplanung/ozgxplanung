package de.latlon.xplan.core.manager.db.converter;

import de.latlon.xplan.manager.web.shared.PlanStatus;
import jakarta.persistence.AttributeConverter;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class PlanStatusConverter implements AttributeConverter<PlanStatus, String> {

	@Override
	public String convertToDatabaseColumn(PlanStatus planStatus) {
		if (planStatus == null)
			return null;
		return planStatus.getMessage();
	}

	@Override
	public PlanStatus convertToEntityAttribute(String planStatus) {
		if (planStatus == null || planStatus.isEmpty())
			return null;
		return PlanStatus.findByMessage(planStatus);
	}

}
