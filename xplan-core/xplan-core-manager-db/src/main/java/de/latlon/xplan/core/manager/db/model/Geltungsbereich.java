/*-
 * #%L
 * xplan-core-manager-db - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.core.manager.db.model;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import org.locationtech.jts.geom.Geometry;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@Entity
@Table(schema = "xplanmgr", name = "geltungsbereich")
public class Geltungsbereich {

	@Id
	@Column(name = "plan")
	private Integer id;

	@OneToOne
	@JoinColumn(name = "id")
	@MapsId("id")
	private Plan plan;

	@Column(name = "geltungsbereichwgs84")
	private Geometry geltungsbereichWGS84;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Geltungsbereich id(Integer id) {
		this.id = id;
		return this;
	}

	public Geometry getGeltungsbereichWGS84() {
		return geltungsbereichWGS84;
	}

	public void setGeltungsbereichWGS84(Geometry geltungsbereichWGS84) {
		this.geltungsbereichWGS84 = geltungsbereichWGS84;
	}

	public Geltungsbereich geltungsbereich(Geometry geltungsbereich) {
		this.geltungsbereichWGS84 = geltungsbereich;
		return this;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public Geltungsbereich plan(Plan plan) {
		this.plan = plan;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Geltungsbereich that = (Geltungsbereich) o;
		return Objects.equals(id, that.id) && Objects.equals(geltungsbereichWGS84, that.geltungsbereichWGS84);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, geltungsbereichWGS84);
	}

}
