/*-
 * #%L
 * xplan-core-manager-db - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.core.manager.db.repository;

import static de.latlon.xplan.commons.XPlanType.BP_Plan;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_51;
import static de.latlon.xplan.manager.web.shared.PlanStatus.ARCHIVIERT;
import static de.latlon.xplan.manager.web.shared.PlanStatus.FESTGESTELLT;
import static de.latlon.xplan.manager.web.shared.PlanStatus.IN_AUFSTELLUNG;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import de.latlon.xplan.core.manager.db.config.HsqlJpaContext;
import de.latlon.xplan.core.manager.db.config.JpaContext;
import de.latlon.xplan.core.manager.db.config.PostgisJpaContext;
import de.latlon.xplan.core.manager.db.model.Artefact;
import de.latlon.xplan.core.manager.db.model.ArtefactId;
import de.latlon.xplan.core.manager.db.model.ArtefactType;
import de.latlon.xplan.core.manager.db.model.Bereich;
import de.latlon.xplan.core.manager.db.model.Feature;
import de.latlon.xplan.core.manager.db.model.Gemeinde;
import de.latlon.xplan.core.manager.db.model.Plan;
import de.latlon.xplan.manager.web.shared.PlanStatus;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.locationtech.jts.io.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test-hsql")
@ContextConfiguration(classes = { JpaContext.class, HsqlJpaContext.class, PostgisJpaContext.class })
@Transactional
class PlanRepositoryTest {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private DataSource dataSource;

	@Autowired
	private PlanRepository planRepository;

	@Test
	void verify_injectedComponentsAreNotNull() {
		assertNotNull(dataSource);
		assertNotNull(entityManager);
		assertNotNull(planRepository);
	}

	@Test
	@Commit
	void verify_saveAndFindById() throws ParseException {
		assertFalse(TestTransaction.isFlaggedForRollback());
		Bereich bereich = new Bereich().nummer("0").name("test");
		Gemeinde gemeinde = new Gemeinde().ags("05465465").rs("856131321").gemeindeName("Test").ortsteilName("teil1");
		Feature feature = new Feature().num(1).fid("123");
		Plan plan = new Plan();
		ArtefactId artefactId = new ArtefactId().plan(plan).filename("test.xml");
		byte[] bytes = "test".getBytes(UTF_8);
		Artefact artefact = new Artefact().id(artefactId)
			.num(1)
			.artefacttype(ArtefactType.XPLANGML)
			.mimetype("text/xml")
			.length(Long.valueOf(bytes.length))
			.data(bytes);
		plan.importDate(new Date())
			.version(XPLAN_51)
			.type(BP_Plan)
			.hasRaster(false)
			.bereiche(Collections.singleton(bereich))
			.gemeinden(Collections.singleton(gemeinde))
			.features(Collections.singleton(feature))
			.artefacts(Collections.singleton(artefact));
		// Not running with HSQL DB (@ActiveProfiles("test-hsql"))
		// setBbox(plan);
		assertNull(plan.getId());
		Plan savedPlan = planRepository.save(plan);
		assertNotNull(savedPlan.getId());
		Optional<Plan> optionalFoundPlan = planRepository.findById(savedPlan.getId());
		assertTrue(optionalFoundPlan.isPresent());
		Plan foundPlan = optionalFoundPlan.get();
		assertNotNull(foundPlan);
		assertNotNull(foundPlan.getId());
	}

	@Test
	@Commit
	void verify_findByName() {
		assertFalse(TestTransaction.isFlaggedForRollback());
		String name = "saveAndFindByName";

		Bereich bereich = new Bereich().nummer("0").name("test");
		Gemeinde gemeinde = new Gemeinde().ags("05465465").rs("856131321").gemeindeName("Test").ortsteilName("teil1");
		Feature feature = new Feature().num(1).fid("123");
		Plan plan = new Plan().name(name)
			.importDate(new Date())
			.version(XPLAN_51)
			.type(BP_Plan)
			.hasRaster(false)
			.bereiche(Collections.singleton(bereich))
			.gemeinden(Collections.singleton(gemeinde))
			.features(Collections.singleton(feature));
		planRepository.save(plan);

		List<Plan> existingPlan = planRepository.findByName(name);
		assertFalse(existingPlan.isEmpty());
		assertEquals(existingPlan.get(0).getGemeinden().size(), 1);

		List<Plan> nonExistingPlan = planRepository.findByName("unknown");
		assertTrue(nonExistingPlan.isEmpty());
	}

	@Test
	@Commit
	void verify_findBySearchParams() {
		assertFalse(TestTransaction.isFlaggedForRollback());
		createAndSavePlan("saveAndFindByLikeName1", null, null);
		createAndSavePlan("saveAndFindByLikeName2", "internalId1", null);
		createAndSavePlan("saveAndFindByLikeName3", "internalId2", FESTGESTELLT);
		createAndSavePlan("saveAndFindByLikeName4", null, IN_AUFSTELLUNG);
		createAndSavePlan(null, null, ARCHIVIERT);
		createAndSavePlan(null, "internalId3", null);
		createAndSavePlan(null, "internalId4", FESTGESTELLT);
		List<Plan> existingPlanByName = planRepository.findBySearchParams("iKEnAme", null);
		assertFalse(existingPlanByName.isEmpty());

		List<Plan> existingPlanByInternalId = planRepository.findBySearchParams(null, "internalId2");
		assertFalse(existingPlanByInternalId.isEmpty());

		List<Plan> existingPlanByPlanstatus = planRepository.findBySearchParams(null, null,
				Collections.singletonList(FESTGESTELLT.getMessage()));
		assertFalse(existingPlanByPlanstatus.isEmpty());

		List<Plan> existingPlanByNameAndPlanstatus = planRepository.findBySearchParams("veand", null,
				Collections.singletonList(FESTGESTELLT.getMessage()));
		assertFalse(existingPlanByNameAndPlanstatus.isEmpty());

		List<Plan> existingPlanByNameAndInternalId = planRepository.findBySearchParams("veand", "internalId1");
		assertFalse(existingPlanByNameAndInternalId.isEmpty());

		List<Plan> existingPlanByNameAndInternalIdAndPlanStatus = planRepository.findBySearchParams("veand",
				"internalId2", Collections.singletonList(FESTGESTELLT.getMessage()));
		assertFalse(existingPlanByNameAndInternalIdAndPlanStatus.isEmpty());

		List<Plan> nonExistingPlanByNameAndInternalId = planRepository.findBySearchParams("unknown", "unknown");
		assertTrue(nonExistingPlanByNameAndInternalId.isEmpty());

		List<Plan> nonExistingPlanByNameAndInternalIdAndPlanstatus = planRepository.findBySearchParams(
				"saveAndFindByLikeName2", "internalId1",
				List.of(FESTGESTELLT.getMessage(), IN_AUFSTELLUNG.getMessage()));
		assertTrue(nonExistingPlanByNameAndInternalIdAndPlanstatus.isEmpty());
	}

	@Test
	@Commit
	void verify_existsPlanByNameAndPlanstatus() {
		assertFalse(TestTransaction.isFlaggedForRollback());
		String name = "existsPlanByNameAndPlanstatus";
		String planstatus = PlanStatus.FESTGESTELLT.name();
		Bereich bereich = new Bereich().nummer("0").name("test");
		Feature feature = new Feature().num(1).fid("123");
		Date wmsSortDate = new Date();
		Plan plan = new Plan().name(name)
			.importDate(new Date())
			.version(XPLAN_51)
			.type(BP_Plan)
			.planstatus(planstatus)
			.hasRaster(true)
			.wmssortdate(wmsSortDate)
			.bereiche(Collections.singleton(bereich))
			.features(Collections.singleton(feature));
		planRepository.save(plan);

		boolean planExists = planRepository.existsPlanByNameAndPlanstatus(name, planstatus);
		assertTrue(planExists);

		boolean notPlanExistsName = planRepository.existsPlanByNameAndPlanstatus("unknown", planstatus);
		assertFalse(notPlanExistsName);

		boolean notPlanExistsPlanstatus = planRepository.existsPlanByNameAndPlanstatus(name, IN_AUFSTELLUNG.name());
		assertFalse(notPlanExistsPlanstatus);
	}

	@Test
	@Commit
	void verify_existsPlanById() {
		assertFalse(TestTransaction.isFlaggedForRollback());
		String name = "existsPlanByNameAndPlanstatus";
		String planstatus = PlanStatus.FESTGESTELLT.name();
		Date wmsSortDate = new Date();
		Plan plan = new Plan().name(name)
			.importDate(new Date())
			.version(XPLAN_51)
			.type(BP_Plan)
			.planstatus(planstatus)
			.hasRaster(true)
			.wmssortdate(wmsSortDate);
		planRepository.save(plan);

		boolean planExists = planRepository.existsPlanById(plan.getId());
		assertTrue(planExists);

		boolean notPlanExists = planRepository.existsPlanById(99999);
		assertFalse(notPlanExists);
	}

	private void createAndSavePlan(String name, String internalId, PlanStatus planStatus) {
		Bereich bereich = new Bereich().nummer("0").name("test");
		Feature feature = new Feature().num(1).fid("123");
		Plan plan = new Plan().name(name)
			.internalid(internalId)
			.planstatus(planStatus != null ? planStatus.getMessage() : null)
			.importDate(new Date())
			.version(XPLAN_51)
			.type(BP_Plan)
			.hasRaster(false)
			.bereiche(Collections.singleton(bereich))
			.features(Collections.singleton(feature));
		planRepository.save(plan);
	}

}
