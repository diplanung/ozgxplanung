/*-
 * #%L
 * xplan-core-manager-db - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.core.manager.db.repository;

import static de.latlon.xplan.commons.XPlanType.BP_Plan;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_51;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import de.latlon.xplan.core.manager.db.config.HsqlJpaContext;
import de.latlon.xplan.core.manager.db.config.JpaContext;
import de.latlon.xplan.core.manager.db.config.PostgisJpaContext;
import de.latlon.xplan.core.manager.db.model.Artefact;
import de.latlon.xplan.core.manager.db.model.ArtefactId;
import de.latlon.xplan.core.manager.db.model.ArtefactType;
import de.latlon.xplan.core.manager.db.model.Bereich;
import de.latlon.xplan.core.manager.db.model.Feature;
import de.latlon.xplan.core.manager.db.model.Geltungsbereich;
import de.latlon.xplan.core.manager.db.model.Gemeinde;
import de.latlon.xplan.core.manager.db.model.Plan;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test-hsql")
@ContextConfiguration(classes = { JpaContext.class, HsqlJpaContext.class, PostgisJpaContext.class })
@Transactional
class GeltungsbereichRepositoryTest {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private DataSource dataSource;

	@Autowired
	private PlanRepository planRepository;

	@Autowired
	private GeltungsbereichRepository geltungsbereichRepository;

	@Test
	void verify_injectedComponentsAreNotNull() {
		assertNotNull(dataSource);
		assertNotNull(entityManager);
		assertNotNull(planRepository);
		assertNotNull(geltungsbereichRepository);
	}

	@Test
	@Commit
	void verify_saveAndFindById() throws ParseException {
		assertFalse(TestTransaction.isFlaggedForRollback());
		Bereich bereich = new Bereich().nummer("0").name("test");
		Gemeinde gemeinde = new Gemeinde().ags("05465465").rs("856131321").gemeindeName("Test").ortsteilName("teil1");
		Feature feature = new Feature().num(1).fid("123");
		Plan plan = new Plan();
		ArtefactId artefactId = new ArtefactId().plan(plan).filename("test.xml");
		byte[] bytes = "test".getBytes(UTF_8);
		Artefact artefact = new Artefact().id(artefactId)
			.num(1)
			.artefacttype(ArtefactType.XPLANGML)
			.mimetype("text/xml")
			.length(Long.valueOf(bytes.length))
			.data(bytes);
		plan.importDate(new Date())
			.version(XPLAN_51)
			.type(BP_Plan)
			.hasRaster(false)
			.bereiche(Collections.singleton(bereich))
			.gemeinden(Collections.singleton(gemeinde))
			.features(Collections.singleton(feature))
			.artefacts(Collections.singleton(artefact));
		assertNull(plan.getId());
		Plan savedPlan = planRepository.save(plan);

		Geltungsbereich geltungsbereich = geltungsbereich(savedPlan);
		geltungsbereichRepository.save(geltungsbereich);

		assertNotNull(savedPlan.getId());
		Optional<Plan> optionalFoundPlan = planRepository.findById(savedPlan.getId());
		assertTrue(optionalFoundPlan.isPresent());
		Plan foundPlan = optionalFoundPlan.get();
		assertNotNull(foundPlan);
		assertNotNull(foundPlan.getId());

		Optional<Geltungsbereich> foundGeltungsbereich = geltungsbereichRepository.findById(savedPlan.getId());
		assertTrue(foundGeltungsbereich.isPresent());
		assertNotNull(foundGeltungsbereich.get().getGeltungsbereichWGS84());
		assertEquals(foundGeltungsbereich.get().getId(), foundPlan.getId());
	}

	private Geltungsbereich geltungsbereich(Plan savedPlan) throws ParseException {
		Geometry jtsGeom = new WKTReader().read("POLYGON((0 0,1 0,1 1,0 1,0 0))");
		return new Geltungsbereich().id(savedPlan.getId()).plan(savedPlan).geltungsbereich(jtsGeom);
	}

}
