/*-
 * #%L
 * xplan-core-manager-db - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.core.manager.db.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.data.domain.Sort.Direction.DESC;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_CLASS;

import java.time.LocalDateTime;

import de.latlon.xplan.core.manager.db.config.HsqlJpaContext;
import de.latlon.xplan.core.manager.db.config.JpaContext;
import de.latlon.xplan.core.manager.db.config.PostgisJpaContext;
import de.latlon.xplan.core.manager.db.model.PlansLogEntry;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test-hsql")
@ContextConfiguration(classes = { JpaContext.class, HsqlJpaContext.class, PostgisJpaContext.class })
@Sql(value = "/import-planslog.sql", executionPhase = BEFORE_TEST_CLASS)
class PlansLogEntryRepositoryTest {

	@Autowired
	private PlansLogEntryRepository plansLogEntryRepository;

	@Test
	void verify_injectedComponentsAreNotNull() {
		assertNotNull(plansLogEntryRepository);
	}

	@Test
	void verify_findAll_paging() {
		Page<PlansLogEntry> firstFive = plansLogEntryRepository
			.findAll(OffsetBasedPageRequest.withOffsetAndLimit(0, 5, Sort.by(DESC, "lastUpdateDate")));
		assertEquals(5, firstFive.getNumberOfElements());
		assertEquals(103, firstFive.getTotalElements());
		assertThat(firstFive.stream().map(PlansLogEntry::getPlanId).toList()).containsExactly(54, 54, 53, 53, 49);

		Page<PlansLogEntry> fiveToTen = plansLogEntryRepository
			.findAll(OffsetBasedPageRequest.withOffsetAndLimit(5, 5, Sort.by(DESC, "lastUpdateDate")));
		assertEquals(5, fiveToTen.getNumberOfElements());
		assertEquals(103, fiveToTen.getTotalElements());
		assertThat(fiveToTen.stream().map(PlansLogEntry::getPlanId).toList()).containsExactly(43, 43, 42, 42, 41);
	}

	@Test
	void verify_findAll_filterLastUpdateDate() {
		LocalDateTime updatedSince = LocalDateTime.of(2025, 2, 20, 11, 25);
		Page<PlansLogEntry> plansLogEntries = plansLogEntryRepository.findAllByLastUpdateDateAfter(updatedSince,
				OffsetBasedPageRequest.withOffsetAndLimit(0, 5, Sort.by(DESC, "lastUpdateDate")));
		assertEquals(2, plansLogEntries.getNumberOfElements());
		assertEquals(2, plansLogEntries.getTotalElements());
		assertThat(plansLogEntries.stream().map(PlansLogEntry::getId).toList()).containsExactly(103, 102);
		assertThat(plansLogEntries.stream().map(PlansLogEntry::getPlanId).toList()).containsExactly(54, 54);

	}

}
