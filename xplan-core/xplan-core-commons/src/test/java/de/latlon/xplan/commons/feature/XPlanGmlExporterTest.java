/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.commons.feature;

import static de.latlon.xplan.commons.XPlanVersion.XPLAN_60;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import de.latlon.xplan.commons.archive.XPlanArchive;
import de.latlon.xplan.commons.archive.XPlanArchiveCreator;
import org.deegree.cs.coordinatesystems.ICRS;
import org.deegree.cs.persistence.CRSManager;
import org.deegree.feature.FeatureCollection;
import org.junit.jupiter.api.Test;
import org.xmlunit.assertj3.XmlAssert;
import org.xmlunit.builder.Input;
import org.xmlunit.matchers.ValidationMatcher;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @version $Revision: $, $Date: $
 */
public class XPlanGmlExporterTest {

	@Test
	public void testExport_SchemaConform() throws Exception {
		FeatureCollection featureCollection = readFeatureCollection("xplan60/BPlan001_6-0.zip");

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		XPlanGmlExporter.export(outputStream, XPLAN_60, featureCollection, null);

		String exportedPlan = outputStream.toString();

		assertThat(exportedPlan, ValidationMatcher.valid(Input.fromURI(XPLAN_60.getSchemaUrl().toURI())));
	}

	@Test
	public void testExport_DecimalNumbers() throws Exception {
		FeatureCollection featureCollection = readFeatureCollection("xplan60/BP_6.0_DecimalNumbers.gml");

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		XPlanGmlExporter.export(outputStream, XPLAN_60, featureCollection, null);

		String exportedPlan = outputStream.toString();

		assertThat(exportedPlan, containsString("564984.1281112354 "));
	}

	@Test
	public void testExport_Envelope() throws Exception {
		FeatureCollection featureCollection = readFeatureCollection("xplan60/BPlan001_6-0.zip");

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		XPlanGmlExporter.export(outputStream, XPLAN_60, featureCollection, false);

		String exportedPlan = outputStream.toString();

		XmlAssert.assertThat(exportedPlan)
			.withNamespaceContext(nsContext())
			.nodesByXPath("//xplan60:XPlanAuszug/gml:boundedBy")
			.doNotExist();
	}

	private FeatureCollection readFeatureCollection(String archiveName) throws Exception {
		XPlanArchive archive = createArchive(archiveName);
		XPlanFeatureCollection xplanFc = readFeatures(archive);
		return xplanFc.getFeatures();
	}

	private XPlanArchive createArchive(String testArchiveName) throws IOException {
		XPlanArchiveCreator archiveCreator = new XPlanArchiveCreator();
		InputStream archiveResource = getClass().getResourceAsStream("/testdata/" + testArchiveName);
		if (testArchiveName.endsWith(".gml"))
			return archiveCreator.createXPlanArchiveFromGml(testArchiveName, archiveResource);
		return archiveCreator.createXPlanArchiveFromZip(testArchiveName, archiveResource);
	}

	private XPlanFeatureCollection readFeatures(XPlanArchive archive) throws Exception {
		ICRS defaultCrs = CRSManager.lookup("EPSG:31467");
		if (archive.getCrs() != null)
			defaultCrs = archive.getCrs();
		return XPlanGmlParserBuilder.newBuilder()
			.withDefaultCrs(defaultCrs)
			.build()
			.parseXPlanFeatureCollection(archive);
	}

	private Map<String, String> nsContext() {
		Map<String, String> nsContext = new HashMap<>();
		nsContext.put("xplan60", XPLAN_60.getNamespace());
		nsContext.put("gml", "http://www.opengis.net/gml/3.2");
		return nsContext;
	}

}
