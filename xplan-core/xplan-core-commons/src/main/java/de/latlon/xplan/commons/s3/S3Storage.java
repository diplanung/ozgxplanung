/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.commons.s3;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;

import de.latlon.xplan.commons.archive.ArchiveEntry;
import de.latlon.xplan.commons.archive.XPlanArchiveContentAccess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.Bucket;
import software.amazon.awssdk.services.s3.model.BucketLifecycleConfiguration;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.ExpirationStatus;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.HeadBucketRequest;
import software.amazon.awssdk.services.s3.model.LifecycleExpiration;
import software.amazon.awssdk.services.s3.model.LifecycleRule;
import software.amazon.awssdk.services.s3.model.LifecycleRuleFilter;
import software.amazon.awssdk.services.s3.model.ListObjectsRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsResponse;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;
import software.amazon.awssdk.services.s3.model.PutBucketLifecycleConfigurationRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;
import software.amazon.awssdk.services.s3.model.S3Exception;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 7.0
 */
public class S3Storage {

	private static final Logger LOG = LoggerFactory.getLogger(S3Storage.class);

	private final S3Client client;

	private final String bucketName;

	public S3Storage(S3Client client, String bucketName) {
		this.client = client;
		this.bucketName = bucketName;
	}

	public void setBucketExpirationDate(String id, int expirationInDays) throws StorageException {
		createBucketIfNotExists();
		try {
			LifecycleRule rule = LifecycleRule.builder()
				.id(id)
				.expiration(LifecycleExpiration.builder().days(expirationInDays).build())
				.status(ExpirationStatus.ENABLED)
				.filter(LifecycleRuleFilter.builder().build())
				.build();
			BucketLifecycleConfiguration bucketLifecycleConfig = BucketLifecycleConfiguration.builder()
				.rules(rule)
				.build();
			PutBucketLifecycleConfigurationRequest lifecycleRequest = PutBucketLifecycleConfigurationRequest.builder()
				.bucket(bucketName)
				.lifecycleConfiguration(bucketLifecycleConfig)
				.build();
			client.putBucketLifecycleConfiguration(lifecycleRequest);
		}
		catch (Exception e) {
			LOG.warn("Could not set expiration date for bucket {} to {} days: {}", bucketName, expirationInDays,
					e.getMessage());
		}
	}

	/**
	 * @param key of the object to return
	 * @return the S3Object with the passed key, never <code>null</code>
	 * @throws StorageException if an error occurred requesting the object or an object
	 * with the passed key was not found
	 */
	public de.latlon.xplan.commons.s3.S3Object getObject(String key) throws StorageException {
		ResponseInputStream<GetObjectResponse> object = null;
		try {
			LOG.info("Get object with key {} from bucket {}.", key, bucketName);
			object = client.getObject(GetObjectRequest.builder().bucket(bucketName).key(key).build());
			GetObjectResponse response = object.response();
			S3Metadata s3Metadata = new S3Metadata(key, response.contentType(), response.contentLength());
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			object.transferTo(bos);
			return new de.latlon.xplan.commons.s3.S3Object(s3Metadata, bos.toByteArray());
		}
		catch (AwsServiceException | IOException e) {
			throw new StorageException("Could not get object with key " + key + " from bucket " + bucketName + ".", e);
		}
		finally {
			closeQuietly(object);
		}
	}

	/**
	 * @param key of the object metadata to return
	 * @return the S3Metadata of the object with the passed key, never <code>null</code>
	 * @throws StorageException if an error occurred requesting the object or an object
	 * with the passed key was not found
	 */
	public S3Metadata getObjectMetadata(String key) throws StorageException {
		try {
			LOG.info("Get object metadata with key {} from bucket {}.", key, bucketName);
			ResponseInputStream<GetObjectResponse> object = client
				.getObject(GetObjectRequest.builder().bucket(bucketName).key(key).build());
			GetObjectResponse response = object.response();
			return new S3Metadata(key, response.contentType(), response.contentLength());
		}
		catch (AwsServiceException e) {
			throw new StorageException("Could not get object with key " + key + " from bucket " + bucketName + ".", e);
		}
	}

	public List<software.amazon.awssdk.services.s3.model.S3Object> listObjects(String prefix) {
		ListObjectsResponse objectsToDelete = client
			.listObjects(ListObjectsRequest.builder().bucket(bucketName).prefix(prefix).build());
		return objectsToDelete.contents();
	}

	public String insertObject(int planId, String entryName, XPlanArchiveContentAccess archive)
			throws StorageException {
		createBucketIfNotExists();
		String key = createKey(planId, entryName);
		try {
			LOG.info("Insert object with key {} in bucket {}.", key, bucketName);
			ArchiveEntry entry = archive.getEntry(entryName);
			String contentType = entry.getContentType();
			long contentLength = entry.getContentLength();
			InputStream content = archive.retrieveInputStreamFor(entryName);
			PutObjectRequest putObjectRequest = PutObjectRequest.builder()
				.bucket(bucketName)
				.key(key)
				.contentLength(contentLength)
				.contentType(contentType)
				.build();
			client.putObject(putObjectRequest, RequestBody.fromInputStream(content, contentLength));
			return key;
		}
		catch (AwsServiceException e) {
			throw new StorageException("Could not insert object with key " + key + " in bucket " + bucketName + ".", e);
		}
	}

	public PutObjectResponse insertObject(String key, Path file) throws StorageException {
		createBucketIfNotExists();
		try {
			LOG.info("Insert object with key {} in bucket {}.", key, bucketName);
			return client.putObject(PutObjectRequest.builder().bucket(bucketName).key(key).build(),
					RequestBody.fromFile(file.toFile()));
		}
		catch (AwsServiceException e) {
			throw new StorageException("Could not insert object with key " + key + " in bucket " + bucketName + ".", e);
		}
	}

	public void insertObject(de.latlon.xplan.commons.s3.S3Object object) throws StorageException {
		createBucketIfNotExists();
		String key = object.getS3Metadata().getKey();
		try {
			LOG.info("Insert object with key {} in bucket {}.", key, bucketName);
			long contentLength = object.getS3Metadata().getContentLength();
			String contentType = object.getS3Metadata().getContentType();
			PutObjectRequest putObjectRequest = PutObjectRequest.builder()
				.bucket(bucketName)
				.key(key)
				.contentLength(contentLength)
				.contentType(contentType)
				.build();
			client.putObject(putObjectRequest, RequestBody.fromBytes(object.getContent()));
		}
		catch (AwsServiceException e) {
			throw new StorageException("Could not insert object with key " + key + " in bucket " + bucketName + ".", e);
		}
	}

	public void deleteObjects(String prefix) {
		List<software.amazon.awssdk.services.s3.model.S3Object> objects = listObjects(prefix);
		for (software.amazon.awssdk.services.s3.model.S3Object object : objects) {
			deleteObject(object);
		}
	}

	public void deleteObject(software.amazon.awssdk.services.s3.model.S3Object object) {
		String key = object.key();
		LOG.info("Delete object with key {} from bucket {}.", key, bucketName);
		client.deleteObject(DeleteObjectRequest.builder().bucket(bucketName).key(key).build());
	}

	protected String createKey(int planId, String entry) {
		return planId + "_" + entry;
	}

	protected void createBucketIfNotExists() throws StorageException {
		try {
			client.headBucket(HeadBucketRequest.builder().bucket(bucketName).build());
			LOG.info("Bucket {} already exists.", bucketName);
		}
		catch (NoSuchBucketException n) {
			try {
				LOG.info("Create bucket with name {}.", bucketName);
				client.createBucket(CreateBucketRequest.builder().bucket(bucketName).build());
			}
			catch (S3Exception e) {
				throw new StorageException("Could not create bucket", e);
			}
		}
	}

	public Bucket getBucket() {
		List<Bucket> buckets = client.listBuckets().buckets();
		for (Bucket bucket : buckets) {
			if (bucket.name().equals(bucketName)) {
				return bucket;
			}
		}
		return null;
	}

	private void closeQuietly(ResponseInputStream<GetObjectResponse> object) {
		if (object != null) {
			try {
				object.close();
			}
			catch (IOException e) {
				LOG.warn("Connection could not be closed: {}", e.getMessage());
				LOG.trace(e.getMessage(), e);
			}
		}
	}

}
