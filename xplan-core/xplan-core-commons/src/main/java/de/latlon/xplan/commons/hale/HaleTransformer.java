/*-
 * #%L
 * xplan-core-commons - Commons Paket fuer XPlan Manager und XPlan Validator
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.commons.hale;

import static org.apache.commons.io.IOUtils.closeQuietly;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class HaleTransformer {

	private static final Logger LOG = LoggerFactory.getLogger(HaleTransformer.class);

	private final String haleCli;

	private final Optional<String> javaOpts;

	/**
	 * @param haleCli the path to the hale CLI, never <code>null</code>
	 */
	public HaleTransformer(String haleCli) {
		this(haleCli, Optional.empty());
	}

	public HaleTransformer(String haleCli, Optional<String> javaOpts) {
		this.haleCli = haleCli;
		this.javaOpts = javaOpts;
	}

	/**
	 * Transform the passed file with the passed project, the result will be stored in the
	 * target file.
	 * @param haleProject to hale project to use for the transformation, never
	 * <code>null</code>
	 * @param sourceFile the source file to transform, never <code>null</code>
	 * @param targetFile the file to store the transformation result, never
	 * <code>null</code>
	 * @throws TransformationException if the transformation failed
	 */
	public void transform(String haleProject, String sourceFile, String targetFile) throws TransformationException {
		HaleIOProvider gmlWriter = new HaleIOProvider("eu.esdihumboldt.hale.io.gml.writer");
		transform(haleProject, sourceFile, targetFile, gmlWriter);
	}

	/**
	 * Transform the passed file with the passed project, the result will be stored in the
	 * target file.
	 * @param haleProject to hale project to use for the transformation, never
	 * <code>null</code>
	 * @param sourceFile the source file to transform, never <code>null</code>
	 * @param targetFile the file to store the transformation result, never
	 * <code>null</code>
	 * @param writer writer, may be <code>null</code> (default is the GML Writer)
	 * @throws TransformationException if the transformation failed
	 */
	public void transform(String haleProject, String sourceFile, String targetFile, HaleIOProvider writer)
			throws TransformationException {
		InputStream inputStream = null;
		try {
			List<String> command = buildCommand(haleProject, sourceFile, targetFile, writer);
			LOG.info("Execute the following command to transform the plan: {}", command);
			ProcessBuilder processBuilder = new ProcessBuilder(command);
			overwriteJavaOpts(processBuilder);
			processBuilder.redirectErrorStream(true);
			Process process = processBuilder.start();

			inputStream = process.getInputStream();
			LOG.info(IOUtils.toString(inputStream));

			int exitCode = process.waitFor();
			LOG.info("Transformation command finished with exit code {}. ", exitCode);
		}
		catch (IOException | InterruptedException e) {
			LOG.error("Could not transform", e);
			throw new TransformationException("Could not transform", e);
		}
		finally {
			closeQuietly(inputStream);
		}
	}

	private void overwriteJavaOpts(ProcessBuilder processBuilder) {
		Map<String, String> environment = processBuilder.environment();
		if (javaOpts.isEmpty()) {
			LOG.info("Remove JAVA_OPTS");
			environment.remove("JAVA_OPTS");
		}
		else {
			LOG.info("Set JAVA_OPTS: {}", javaOpts.get());
			environment.put("JAVA_OPTS", javaOpts.get());
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("Command starts with the following environment:");
			environment.forEach((k, v) -> LOG.debug(" - {}: {}", k, v));
		}
	}

	private List<String> buildCommand(String haleProject, String source, String target, HaleIOProvider writer) {
		List<String> command = new ArrayList<>();
		command.add(haleCli);
		command.add("transform");
		command.add("-project");
		command.add(haleProject);
		command.add("-source");
		command.add(source);
		command.add("-target");
		command.add(target);
		command.add("-providerId");
		command.add(writer.getName());
		for (Map.Entry<String, String> setting : writer.getSettings().entrySet()) {
			command.add("-S");
			command.add(setting.getKey());
			command.add(setting.getValue());
		}
		return command;
	}

}
