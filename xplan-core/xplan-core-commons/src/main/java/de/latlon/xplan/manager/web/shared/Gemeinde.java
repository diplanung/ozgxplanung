/*-
 * #%L
 * xplan-core-commons - Commons Paket fuer XPlan Manager und XPlan Validator
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.manager.web.shared;

import java.util.Objects;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class Gemeinde {

	private String ags;

	private String rs;

	private String gemeindeName;

	private String ortsteilName;

	public String getAgs() {
		return ags;
	}

	public void setAgs(String ags) {
		this.ags = ags;
	}

	public Gemeinde ags(String ags) {
		this.ags = ags;
		return this;
	}

	public String getRs() {
		return rs;
	}

	public void setRs(String rs) {
		this.rs = rs;
	}

	public Gemeinde rs(String rs) {
		this.rs = rs;
		return this;
	}

	public String getGemeindeName() {
		return gemeindeName;
	}

	public void setGemeindeName(String gemeindeName) {
		this.gemeindeName = gemeindeName;
	}

	public Gemeinde gemeindeName(String gemeindeName) {
		this.gemeindeName = gemeindeName;
		return this;
	}

	public String getOrtsteilName() {
		return ortsteilName;
	}

	public void setOrtsteilName(String ortsteilName) {
		this.ortsteilName = ortsteilName;
	}

	public Gemeinde ortsteilName(String ortsteilName) {
		this.ortsteilName = ortsteilName;
		return this;
	}

	@Override
	public int hashCode() {
		return Objects.hash(ags, rs, gemeindeName, ortsteilName);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Gemeinde gemeinde = (Gemeinde) o;
		return Objects.equals(ags, gemeinde.ags) && Objects.equals(rs, gemeinde.rs)
				&& Objects.equals(gemeindeName, gemeinde.gemeindeName)
				&& Objects.equals(ortsteilName, gemeinde.ortsteilName);
	}

	@Override
	public String toString() {
		return "Gemeinde{" + "ags='" + ags + '\'' + ", rs='" + rs + '\'' + ", gemeindeName='" + gemeindeName + '\''
				+ ", ortsteilName='" + ortsteilName + '\'' + '}';
	}

}
