/*-
 * #%L
 * xplan-core-manager - XPlan Manager Core Komponente
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.commons.s3.config;

import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3Configuration;

/**
 * Spring configuration for using AWS S3 as a storage.
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @author <a href="mailto:friebe@lat-lon.de">Torsten Friebe</a>
 * @since 8.0
 */
@Configuration
@ComponentScan("de.latlon.xplan.manager.storage.s3.listener")
@Profile({ "!test" })
public class AmazonS3ReadOnlyContext {

	private static final Logger LOG = LoggerFactory.getLogger(AmazonS3ReadOnlyContext.class);

	@Bean(destroyMethod = "close")
	public S3Client s3Client(AwsCredentialsProvider credentialsProvider,
			@Value("${xplanbox.s3.region:#{environment.XPLAN_S3_REGION}}") String region,
			@Value("${xplanbox.s3.endpoint.url:#{environment.XPLAN_S3_ENDPOINT}}") String endpointUrl,
			@Value("${xplanbox.s3.pathStyleAccessEnabled:#{environment.XPLAN_S3_PATHSTYLEACCESS_ENABLED?:false}}") boolean pathStyleAccessEnabled,
			@Value("${xplanbox.s3.chunkedEncodingEnabled:#{environment.XPLAN_S3_CHUNKENCODING_ENABLED?:true}}") boolean chunkedEncodingEnabled)
			throws URISyntaxException {
		LOG.info("Using S3 url {} (region {}) chunk encoding is {}, path style access is {}", endpointUrl, region,
				chunkedEncodingEnabled ? "enabled" : "disabled", pathStyleAccessEnabled ? "enabled" : "disabled");
		if (endpointUrl == null || endpointUrl.isEmpty()) {
			return S3Client.builder()
				.serviceConfiguration(S3Configuration.builder()
					.pathStyleAccessEnabled(pathStyleAccessEnabled)
					.chunkedEncodingEnabled(chunkedEncodingEnabled)
					.build())
				.credentialsProvider(credentialsProvider)
				.region(Region.of(region))
				.build();
		}
		return S3Client.builder()
			.serviceConfiguration(S3Configuration.builder()
				.pathStyleAccessEnabled(pathStyleAccessEnabled)
				.chunkedEncodingEnabled(chunkedEncodingEnabled)
				.build())
			.credentialsProvider(credentialsProvider)
			.region(Region.of(region))
			.endpointOverride(new URI(endpointUrl))
			.build();
	}

	@Bean
	public AwsCredentialsProvider credentialsProvider(
			@Value("${xplanbox.s3.accessKeyId:#{environment.XPLAN_S3_ACCESS_KEY}}") String accessKeyId,
			@Value("${xplanbox.s3.secretKey:#{environment.XPLAN_S3_SECRET_ACCESS_KEY}}") String secretKey) {
		return StaticCredentialsProvider.create(AwsBasicCredentials.create(accessKeyId, secretKey));
	}

}
