/*-
 * #%L
 * xplan-core-commons - Commons Paket fuer XPlan Manager und XPlan Validator
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.commons.util;

import static de.latlon.xplan.commons.synthesizer.Features.getPropertyStringValue;

import javax.xml.namespace.QName;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.latlon.xplan.commons.XPlanType;
import de.latlon.xplan.manager.web.shared.Bereich;
import de.latlon.xplan.manager.web.shared.Gemeinde;
import org.deegree.commons.tom.TypedObjectNode;
import org.deegree.commons.tom.genericxml.GenericXMLElement;
import org.deegree.commons.tom.gml.property.Property;
import org.deegree.feature.Feature;
import org.deegree.feature.FeatureCollection;
import org.deegree.feature.property.GenericProperty;

/**
 * Contains utilities for deegree {@link org.deegree.feature.FeatureCollection}s.
 *
 * @author <a href="mailto:stenger@lat-lon.de">Dirk Stenger</a>
 * @version $Revision: $, $Date: $
 */
public class FeatureCollectionUtils {

	private FeatureCollectionUtils() {
	}

	/**
	 * Finds the XP_Plan feature of a XPlan featureCollection.
	 * @param fc XPlan featureCollection, never <code>null</code>
	 * @param type the type of the expected plan feature, never <code>null</code>
	 * @return XP_Plan , never <code>null</code>
	 * @throws IllegalArgumentException if the feature collection does not contain at
	 * least one XP_Plan feature
	 */
	public static Feature findPlanFeature(FeatureCollection fc, XPlanType type) {
		for (Feature feature : fc) {
			QName featureName = feature.getName();
			if (featureName.getLocalPart().equals(type.name())) {
				return feature;
			}
		}
		throw new IllegalArgumentException("Keine XPlan-FeatureCollection. Kein XP_Plan-Feature enthalten.");
	}

	/**
	 * Retrieves the rechtsstand of a XPlan-FeatureCollection.
	 * @param fc XPlan-FeatureCollection, never <code>null</code>
	 * @param type XPlan-Type, never <code>null</code>
	 * @return rechtsstand of the plan or <code>null</code> if no value was found
	 */
	public static String retrieveRechtsstand(FeatureCollection fc, XPlanType type) {
		return retrievePlanProperty(fc, type, "rechtsstand");
	}

	/**
	 * Retrieves the translated rechtsstand ("rechtsstandWert") of a synthesized
	 * XPlan-FeatureCollection.
	 * @param fc synthesized XPlan-FeatureCollection, never <code>null</code>
	 * @param type XPlan-Type, never <code>null</code>
	 * @return translated rechtsstand ("rechtsstandWert") of the plan or <code>null</code>
	 * if no value was found
	 */
	public static String retrieveRechtsstandWert(FeatureCollection fc, XPlanType type) {
		return retrievePlanProperty(fc, type, "rechtsstandWert");
	}

	/**
	 * Retrieves the translated additional type ("sonstPlanArtWert") of a synthesized
	 * XPlan-FeatureCollection.
	 * @param fc synthesized XPlan-FeatureCollection, never <code>null</code>
	 * @param type XPlan-Type, never <code>null</code>
	 * @return translated sonstPlanArtWert value or <code>null</code> if no value was
	 * found
	 */
	public static String retrieveAdditionalTypeWert(FeatureCollection fc, XPlanType type) {
		return retrievePlanProperty(fc, type, "sonstPlanArtWert");
	}

	/**
	 * Retrieves the value of XX_Plan/beschreibung of the {@link FeatureCollection}.
	 * @param fc XPlan-FeatureCollection, never <code>null</code>
	 * @param type XPlan-Type, never <code>null</code>
	 * @return additional type value or <code>null</code> if no value was found
	 */
	public static String retrieveDescription(FeatureCollection fc, XPlanType type) {
		return retrievePlanProperty(fc, type, "beschreibung");
	}

	/**
	 * Retrieves the value of XX_Plan/name of the {@link FeatureCollection}.
	 * @param fc XPlan-FeatureCollection, never <code>null</code>
	 * @param type XPlan-Type, never <code>null</code>
	 * @return name of the plan, never <code>null</code> (a new name is created)
	 */
	public static String retrievePlanName(FeatureCollection fc, XPlanType type) {
		Feature planFeature = findPlanFeature(fc, type);
		return retrievePlanName(planFeature);
	}

	/**
	 * Retrieves the value of the attribute name of the passed {@link Feature}.
	 * @param planFeature never <code>null</code>
	 * @return name of the plan, never <code>null</code> (a new name is created)
	 */
	public static String retrievePlanName(Feature planFeature) {
		String ns = planFeature.getName().getNamespaceURI();
		String name = getPropertyStringValue(planFeature, new QName(ns, "name"));
		if (name == null || name.isEmpty()) {
			name = "Unbenannter XPlan (" + UUID.randomUUID().toString() + ")";
		}
		return name;
	}

	/**
	 * Retrieves the Bereiche of the passed {@link FeatureCollection}.
	 * @param fc XPlan-FeatureCollection, never <code>null</code>
	 * @return list of the bereiche of the plan, may be empty but never <code>null</code>
	 */
	public static List<Bereich> retrieveBereiche(FeatureCollection fc) {
		List<Bereich> bereiche = new ArrayList<>();
		for (Feature feature : fc) {
			QName featureName = feature.getName();
			if (featureName.getLocalPart().matches("(BP|FP|LP|RP|SO)_Bereich")) {
				String ns = feature.getName().getNamespaceURI();
				Bereich bereich = new Bereich();
				bereich.setNummer(getPropertyStringValue(feature, new QName(ns, "nummer")));
				bereich.setName(getPropertyStringValue(feature, new QName(ns, "name")));
				bereiche.add(bereich);
			}
		}
		return bereiche;
	}

	/**
	 * Retrieves the Gemeinden of the passed {@link FeatureCollection}.
	 * @param fc XPlan-FeatureCollection, never <code>null</code>
	 * @return list of the bereiche of the plan, may be empty but never <code>null</code>
	 */
	public static List<Gemeinde> retrieveGemeinden(FeatureCollection fc) {
		List<Gemeinde> gemeinden = new ArrayList<>();
		for (Feature feature : fc) {
			QName featureName = feature.getName();
			if (featureName.getLocalPart().matches("(BP|FP|LP|SO)_Plan")) {
				String ns = feature.getName().getNamespaceURI();
				List<Property> gemeindeProperties = feature.getProperties(new QName(ns, "gemeinde"));
				for (Property gemeindeProperty : gemeindeProperties) {
					GenericXMLElement xpGemeinde = getXpGemeinde(gemeindeProperty);
					if (xpGemeinde != null) {
						Gemeinde gemeinde = new Gemeinde();
						gemeinde.setAgs(getProperty(xpGemeinde, new QName(ns, "ags")));
						gemeinde.setRs(getProperty(xpGemeinde, new QName(ns, "rs")));
						gemeinde.setGemeindeName(getProperty(xpGemeinde, new QName(ns, "gemeindeName")));
						gemeinde.setOrtsteilName(getProperty(xpGemeinde, new QName(ns, "ortsteilName")));
						gemeinden.add(gemeinde);
					}
				}
			}
		}
		return gemeinden;
	}

	/**
	 * Retrieves the internalId of the passed {@link FeatureCollection}.
	 * @param fc XPlan-FeatureCollection, never <code>null</code>
	 * @return the internalId or <code>null</code>
	 */
	public static String retrieveInternalId(FeatureCollection fc, XPlanType type) {
		Feature planFeature = findPlanFeature(fc, type);
		String ns = planFeature.getName().getNamespaceURI();
		return getPropertyStringValue(planFeature, new QName(ns, "internalId"));
	}

	public static GenericXMLElement getXpGemeinde(Property f) {
		if (f instanceof GenericProperty) {
			List<TypedObjectNode> children = f.getChildren();
			for (TypedObjectNode typedObjectNode : children) {
				if (typedObjectNode instanceof GenericXMLElement
						&& "XP_Gemeinde".equals(((GenericXMLElement) typedObjectNode).getName().getLocalPart()))
					return (GenericXMLElement) typedObjectNode;
			}
		}
		return null;
	}

	private static String getProperty(GenericXMLElement xpGemeinde, QName propName) {
		List<TypedObjectNode> children = xpGemeinde.getChildren();
		for (TypedObjectNode child : children) {
			if (child instanceof GenericXMLElement && propName.equals(((GenericXMLElement) child).getName()))
				return ((GenericXMLElement) child).getValue().getAsText();
		}
		return null;
	}

	private static String retrievePlanProperty(FeatureCollection fc, XPlanType type, String propertyName) {
		Feature planFeature = findPlanFeature(fc, type);
		String ns = planFeature.getName().getNamespaceURI();
		return getPropertyStringValue(planFeature, new QName(ns, propertyName));
	}

}
