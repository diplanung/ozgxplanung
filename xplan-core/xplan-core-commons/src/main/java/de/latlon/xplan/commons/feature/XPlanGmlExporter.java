/*-
 * #%L
 * xplan-core-commons - Commons Paket fuer XPlan Manager und XPlan Validator
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplan.commons.feature;

import static javax.xml.stream.XMLOutputFactory.IS_REPAIRING_NAMESPACES;
import static org.deegree.commons.xml.CommonNamespaces.XLNNS;
import static org.deegree.gml.GMLOutputFactory.createGMLStreamWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.OutputStream;
import java.util.UUID;

import de.latlon.xplan.commons.XPlanVersion;
import org.deegree.commons.xml.stax.IndentingXMLStreamWriter;
import org.deegree.cs.exceptions.TransformationException;
import org.deegree.cs.exceptions.UnknownCRSException;
import org.deegree.feature.Feature;
import org.deegree.feature.FeatureCollection;
import org.deegree.geometry.Envelope;
import org.deegree.gml.GMLStreamWriter;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public final class XPlanGmlExporter {

	private XPlanGmlExporter() {
	}

	/**
	 * Exports the xplan as gml.
	 * @param outputStream the stream to write the exported xplan.gml into, never
	 * <code>null</code>
	 * @param version the version of the xplan, never <code>null</code>
	 * @param fc to export, never <code>null</code>
	 * @param comment written in the exported xml, may be <code>null</code>
	 * @throws Exception
	 */
	public static void export(OutputStream outputStream, XPlanVersion version, FeatureCollection fc, String comment)
			throws Exception {
		export(outputStream, version, fc, comment, true);
	}

	/**
	 * Exports the xplan as gml.
	 * @param outputStream the stream to write the exported xplan.gml into, never
	 * <code>null</code>
	 * @param version the version of the xplan, never <code>null</code>
	 * @param fc to export, never <code>null</code>
	 * @param exportEnvelope <code>true</code> if the envelope should be exported,
	 * <code>false</code> otherwise
	 * @throws Exception
	 */
	public static void export(OutputStream outputStream, XPlanVersion version, FeatureCollection fc,
			boolean exportEnvelope) throws Exception {
		export(outputStream, version, fc, null, exportEnvelope);
	}

	private static void export(OutputStream outputStream, XPlanVersion version, FeatureCollection fc, String comment,
			boolean exportEnvelope) throws Exception {
		XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
		xmlOutputFactory.setProperty(IS_REPAIRING_NAMESPACES, true);
		XMLStreamWriter writer = new IndentingXMLStreamWriter(
				xmlOutputFactory.createXMLStreamWriter(outputStream, "UTF-8"));
		writer.writeStartDocument("UTF-8", "1.0");
		if (comment != null)
			writer.writeComment(comment);
		GMLStreamWriter encoder = createGMLStreamWriter(version.getGmlVersion(), writer);
		encoder.setGenerateBoundedByForFeatures(true);
		encoder.setCoordinateFormatter(v -> Double.toString(v));
		String xplanNs = version.getNamespace();
		writer.setPrefix("xplan", xplanNs);
		writer.setPrefix("xlink", XLNNS);
		String gmlNs = version.getGmlVersion().getNamespace();
		writer.setPrefix("gml", gmlNs);
		writer.writeStartElement(xplanNs, "XPlanAuszug");
		String id = fc.getId();
		if (id == null)
			id = "AUSZUG_" + UUID.randomUUID();
		writer.writeAttribute("gml", gmlNs, "id", id);
		if (exportEnvelope)
			exportEnvelope(encoder, fc, writer, gmlNs);
		for (Feature feature : fc) {
			writer.writeStartElement(gmlNs, "featureMember");
			encoder.write(feature);
			writer.writeEndElement();
		}
		writer.writeEndElement();
		writer.close();

	}

	private static void exportEnvelope(GMLStreamWriter encoder, FeatureCollection fc, XMLStreamWriter writer,
			String gmlNs) throws XMLStreamException, UnknownCRSException, TransformationException {
		Envelope envelope = fc.getEnvelope();
		writer.writeStartElement(gmlNs, "boundedBy");
		if (envelope != null) {
			encoder.getGeometryWriter().exportEnvelope(envelope);
		}
		else {
			writer.writeStartElement(gmlNs, "null");
			writer.writeCharacters("missing");
			writer.writeEndElement();
		}
		writer.writeEndElement();
	}

}
