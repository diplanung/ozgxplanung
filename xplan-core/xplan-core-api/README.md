# Beschreibung der Schnittstellen von XPlanManagerAPI und XPlanValidatorAPI

Änderungen an den REST-Schnittstellen der Komponenten XPlanManagerAPI und XPlanValidatorAPI werden über Versionsnummern abgebildet.  

## Aufbau der Versionsnummer

Die Versionierung der API basiert auf dem Konzept von [Semantic Versioning](https://semver.org/).

Die Versionsnummer für die XPlanManagerAPI und XPlanValidatorAPI setzt sich aus folgenden Teilen zusammen:

    MAJOR_VERSION.MINOR_VERSION.BUGFIX_VERSION

Beispiel:

    2.1.0
    | | +-- Bugfix-Version (Revisionsnummer)
    | +---- Minor-Version (Nebenversionsnummer)
    +------ Major-Version (Hauptversionsnummer)

In den Ressourcenpfaden der beiden Komponenten wird nur die Major-Version angegeben:

    /api/v2
    /api/v2

In den OpenAPI-Dokumenten wird die vollständige Versionsnummer im Element `version` ausgegeben:

```json
"info": {
...
"version": "2.0.0"
}
```

### Änderungen in einer Major-Version

Mit Einführung einer neuen Major-Version wird ein neuer Ressourcenpfad mit z. B. `/api/v3` eingeführt.
Änderungen an der API, die nicht abwärts-/rückwärtskompatibel sind, können sein:

- Entfernen von bestehenden Ressourcen
- Entfernen eines HTTP-Verbs (z.B. POST, PUT, DELETE) zu einer bestehenden Ressource
- Entfernen von Werten von bestehenden Enumerationen
- Entfernen von zuvor verfügbaren Typen
- Entfernen von Datentypen, die in einer vorherigen Version als deprecated markiert worden sind
- Entfernen von Content-Types
- Entfernen eines Status-Codes zu einer Antwort
- Ändern des Status-Codes oder des Rückgabetyps
- Hinzufügen von neuen verpflichtenden Attributen
- Ändern eines Datentyps
- Ändern einer "operationId"


### Änderungen in einer Minor-Version

Hierunter fallen abwärts-/rückwärtskompatible Änderungen und Ergänzungen an der API:

- Hinzufügen neuer Ressourcen
- Hinzufügen eines HTTP-Verbs (z.B. POST, PUT, DELETE) für eine bestehende Ressource
- Hinzufügen neuer Werte für Enumerationen
- Hinzufügen von neuen optionalen Parametern
- Hinzufügen von neuen optionalen Attributen
- Hinzufügen von neuen Content-Types
- Hinzufügen eines neuen Datentyps (Schema)
- Hinzufügen eines neuen Properties zu einem Datentyp (Schema)
- Hinzufügen weiterer Status-Codes zu einer Antwort
- Hinzufügen von weiteren Informationen

### Änderungen in einer Bugfix-Version

Hierunter fallen abwärts-/rückwärtskompatible Änderungen an der API:

- Hinzufügen, Entfernen und Ändern von "Summary", "Description" oder "Example" Elementen
- Hinzufügen, Entfernen und Ändern von Tags
- Korrektur von Tipp- und Rechtschreibfehlern im OpenAPI-Dokument
