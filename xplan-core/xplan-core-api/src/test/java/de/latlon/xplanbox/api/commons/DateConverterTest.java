/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons;

import static de.latlon.xplanbox.api.commons.DateConverter.convertToLocalDate;
import static de.latlon.xplanbox.api.commons.DateConverter.convertToLocalDateTime;
import static de.latlon.xplanbox.api.commons.DateConverter.convertToZonedDateTime;
import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>#
 * @since 8.0
 */
public class DateConverterTest {

	@Test
	public void verify_convertToLocalDate() throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN);
		String dateInString = "13.08.2024";
		Date date = formatter.parse(dateInString);
		LocalDate localDate = convertToLocalDate(date);

		assertThat(localDate.getDayOfMonth()).isEqualTo(13);
		assertThat(localDate.getMonth()).isEqualTo(Month.AUGUST);
		assertThat(localDate.getYear()).isEqualTo(2024);
	}

	@Test
	public void verify_convertToLocalDateTime() throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy'T'hh:mm", Locale.GERMAN);
		String dateInString = "13.08.2024T06:23";
		Date date = formatter.parse(dateInString);
		LocalDateTime localDateTime = convertToLocalDateTime(date);

		assertThat(localDateTime.getDayOfMonth()).isEqualTo(13);
		assertThat(localDateTime.getMonth()).isEqualTo(Month.AUGUST);
		assertThat(localDateTime.getYear()).isEqualTo(2024);
		assertThat(localDateTime.getHour()).isEqualTo(6);
		assertThat(localDateTime.getMinute()).isEqualTo(23);
	}

	@Test
	public void verify_convertToZonedDateTime() throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy'T'hh:mm", Locale.GERMAN);
		String dateInString = "13.08.2024T06:23";
		Date date = formatter.parse(dateInString);
		ZonedDateTime zonedDateTime = DateConverter.convertToZonedDateTime(date);

		assertThat(zonedDateTime.getDayOfMonth()).isEqualTo(13);
		assertThat(zonedDateTime.getMonth()).isEqualTo(Month.AUGUST);
		assertThat(zonedDateTime.getYear()).isEqualTo(2024);
		assertThat(zonedDateTime.getHour()).isEqualTo(6);
		assertThat(zonedDateTime.getMinute()).isEqualTo(23);
	}

	@Test
	public void verify_convertToDate_fromLocalDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		LocalDate localDate = LocalDate.parse("13.08.2024", formatter);
		Date date = DateConverter.convertToDate(localDate);

		assertThat(date.getDay()).isEqualTo(2);// day of the week
		assertThat(date.getMonth()).isEqualTo(Calendar.AUGUST);
		assertThat(date.getYear()).isEqualTo(124);
	}

	@Test
	public void verify_convertBetweenZonedDateTimeAndDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss z");
		ZonedDateTime parsedZonedDateTime = ZonedDateTime.parse("13.08.2024 06:23:01 Europe/Paris", formatter);

		Date date = DateConverter.convertToDate(parsedZonedDateTime);
		ZonedDateTime convertedZonedDateTime = convertToZonedDateTime(date);

		assertThat(convertedZonedDateTime.isEqual(parsedZonedDateTime)).isTrue();
	}

	@Test
	public void verify_convertBetweenLocalDateAndDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		LocalDate parsedLocalDate = LocalDate.parse("13.08.2024", formatter);

		Date date = DateConverter.convertToDate(parsedLocalDate);
		LocalDate convertedLocalDate = convertToLocalDate(date);

		assertThat(convertedLocalDate.isEqual(parsedLocalDate)).isTrue();
	}

	@Test
	public void verify_convertBetweenLocalDateTimeAndDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
		LocalDateTime parsedLocalDateTime = LocalDateTime.parse("13.08.2024 06:23:01", formatter);

		Date date = DateConverter.convertToDate(parsedLocalDateTime);
		LocalDateTime convertedLocalDate = convertToLocalDateTime(date);

		assertThat(convertedLocalDate.isEqual(parsedLocalDateTime)).isTrue();
	}

}
