/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons.v2.model;

import static de.latlon.xplanbox.api.commons.v1.model.ExternalReferenceStatusEnum.AVAILABLE;
import static de.latlon.xplanbox.api.commons.v1.model.VersionEnum.XPLAN_60;
import static de.latlon.xplanbox.api.commons.v2.model.CrsStatusEnum.MISSING;
import static de.latlon.xplanbox.api.commons.v2.model.ImageFormatStatusEnum.UNSUPPORTED;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationRuleLevel.ERROR;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationRuleLevel.WARNING;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.COMPLETED;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.TERMINATED_WITH_ERRORS;
import static java.time.Month.JULY;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.Collections;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.latlon.xplanbox.api.commons.ObjectMapperContextResolver;
import de.latlon.xplanbox.api.commons.ValidationReportConverter;
import de.latlon.xplanbox.api.commons.v1.model.ExternalReferenceResult;
import de.latlon.xplanbox.api.commons.v1.model.RasterEvaluationResult;
import de.latlon.xplanbox.api.commons.v1.model.RulesMetadata;
import de.latlon.xplan.validator.report.geojson.model.Geometry;
import de.latlon.xplan.validator.report.geojson.model.Point;
import de.latlon.xplan.validator.report.geojson.model.Position;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class ValidationReportTest {

	@Test
	void verifyThat_ReportCanBeSerialized() throws URISyntaxException, IOException {
		ValidationReport validationReport = buildValidationReport();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectMapper objectMapper = new ObjectMapperContextResolver().getContext(ValidationReport.class);
		objectMapper.writerWithDefaultPrettyPrinter().writeValue(bos, validationReport);
		String serializedReport = bos.toString();

		assertThat(serializedReport).contains("externalReferencesResult");
		assertThat(serializedReport).contains("crsStatus");
		assertThat(serializedReport).contains("MISSING");
		assertThat(serializedReport).contains("imageFormatStatus");
		assertThat(serializedReport).contains("UNSUPPORTED");
		assertThat(serializedReport).contains("2024-07-31");
		assertThat(StringUtils.countMatches(serializedReport, "\"type\" : \"Point\"")).isEqualTo(2);
	}

	@Test
	void verifyThat_ReportCanBeDeserialized() throws IOException {
		InputStream resourceAsStream = getClass().getResourceAsStream("validation-report.json");

		ObjectMapper objectMapper = new ObjectMapperContextResolver().getContext(ValidationReport.class);
		ValidationReport validationReport = objectMapper.readValue(resourceAsStream, ValidationReport.class);

		assertThat(validationReport.getStatus()).isEqualTo(COMPLETED);
		assertThat(validationReport.getPlans().get(0).getValid()).isTrue();
	}

	@Test
	void verifyThat_ReportCanBeDeserializedAndConverted_syntaxError() throws IOException {
		InputStream resourceAsStream = getClass().getResourceAsStream("validation-report-syntaxError.json");

		ObjectMapper objectMapper = new ObjectMapperContextResolver().getContext(ValidationReport.class);
		ValidationReport validationReport = objectMapper.readValue(resourceAsStream, ValidationReport.class);

		assertThat(validationReport.getStatus()).isEqualTo(TERMINATED_WITH_ERRORS);

		de.latlon.xplanbox.api.commons.v1.model.ValidationReport validationReportV1 = ValidationReportConverter
			.convertToV1(validationReport);
		assertThat(validationReportV1.getValid()).isFalse();
		assertThat(validationReportV1.getStatus()).isEqualTo(
				"Aufgrund von syntaktischen Fehlern konnten nicht alle Validierungsschritte ausgeführt werden. Zudem konnte die BBox und externalReferences nicht ermittelt werden.");
		assertThat(validationReportV1.getValidationResult().getSyntaktisch().getValid()).isFalse();
		assertThat(validationReportV1.getValidationResult().getSemantisch().getValid()).isFalse();
		assertThat(validationReportV1.getValidationResult().getGeometrisch().getValid()).isFalse();
	}

	private ValidationReport buildValidationReport() throws URISyntaxException {
		ExternalReferenceResult externalReferenceResult = new ExternalReferenceResult().name("test.png")
			.status(AVAILABLE);
		RasterEvaluationResult rasterEvaluationResult = new RasterEvaluationResult().name("test.png")
			.crsStatus(MISSING)
			.imageFormatStatus(UNSUPPORTED);
		ValidationReportValidationResult validationResult = new ValidationReportValidationResult();
		ValidationReportValidationResultSyntaktisch syntaktischResult = new ValidationReportValidationResultSyntaktisch()
			.valid(true)
			.status(COMPLETED);
		ValidationReportValidationResultSemantischWithRulesMetadata semantischResult = (ValidationReportValidationResultSemantischWithRulesMetadata) new ValidationReportValidationResultSemantischWithRulesMetadata()
			.valid(true)
			.status(COMPLETED)
			.rules(asList(
					new ValidationReportValidationResultSemantischRule().id("3.1.1.2")
						.title("XP_Plan: Relationen auf Begruendungs-Abschnitte"),
					new ValidationReportValidationResultSemantischRule().id("3.2.3.1")
						.title("XP_Hoehenangabe: Konsistenz der verschiedenen Hoehenangaben")
						.findings(singletonList(new ValidationReportValidationResultSemantischRuleFinding()
							.message("Sollte nicht in m sein.")
							.level(WARNING)
							.gmlIds(asList("GML_1223", "GML_7587")))),
					new ValidationReportValidationResultSemantischRule().id("3.2.4.1")
						.title("XP_XXX: Konsistenz der XXXX")
						.findings(asList(
								new ValidationReportValidationResultSemantischRuleFinding()
									.message("Sollte nicht XXX sein.")
									.level(WARNING)
									.gmlIds(singletonList("GML_2589")),
								new ValidationReportValidationResultSemantischRuleFinding()
									.message("Darf nicht xy sein.")
									.level(ERROR)
									.gmlIds(singletonList("GML_1223"))))));
		ValidationReportValidationResultGeometrisch geometrischResult = new ValidationReportValidationResultGeometrisch()
			.valid(true)
			.status(COMPLETED)
			.rules(asList(new ValidationReportValidationResultGeometrischRule().id("2.2.3.1")
				.title("Geltungsbereich")
				.findings(singletonList(new ValidationReportValidationResultGeometrischRuleFinding().message(
						"Objekt ausserhalb des Geltungsbereich des Plans und des Bereichs: Es wird ein Loch geschnitten.")
					.level(ERROR)
					.gmlIds(asList("GML_1234", "GML_56789"))
					.markerGeom(createPoint(6.5, 10.8)))),
					new ValidationReportValidationResultGeometrischRule().id("2.2.1")
						.title("Flaechenschlussbedingung")
						.findings(singletonList(new ValidationReportValidationResultGeometrischRuleFinding().message(
								"Objekt ausserhalb des Geltungsbereich des Plans und des Bereichs: Es wird ein Loch geschnitten.")
							.level(ERROR)
							.gmlIds(asList("GML_1234", "GML_56789"))
							.markerGeom(createPoint(8.9, 11.5))))));

		ValidationReportValidationResultProfil profileResult = new ValidationReportValidationResultProfil()
			.rulesMetadata(new RulesMetadata().id("test").name("Test").version("0.0.1"))
			.result(new ValidationReportValidationResultSemantischWithRulesMetadata().valid(true)
				.status(COMPLETED)
				.rules(singletonList(
						new ValidationReportValidationResultSemantischRule().id("7.8.7").title("Test Porfil Rule"))));

		ValidationReportPlan plan = new ValidationReportPlan().name("test")
			.version(XPLAN_60)
			.valid(true)
			.status(COMPLETED)
			.externalReferencesResult(singletonList(externalReferenceResult))
			.rasterEvaluationResult(Collections.singletonList(rasterEvaluationResult))
			.validationResult(validationResult.semantisch(semantischResult)
				.geometrisch(geometrischResult)
				.profile(singletonList(profileResult)));
		return new ValidationReport().date(LocalDateTime.of(2024, JULY, 31, 9, 55))
			.filename("text.xml")
			.validationName("test")
			.status(COMPLETED)
			.wmsUrl(new URI("file:///no/real/file/name"))
			.syntaktisch(syntaktischResult)
			.addPlanItem(plan);
	}

	private Geometry createPoint(double x, double y) {
		Position position = new Position();
		position.addAll(asList(BigDecimal.valueOf(x), BigDecimal.valueOf(y)));
		return new Point().position(position);
	}

}
