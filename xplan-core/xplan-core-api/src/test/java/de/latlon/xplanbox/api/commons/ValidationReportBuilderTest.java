/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons;

import static de.latlon.xplan.commons.XPlanType.BP_Plan;
import static de.latlon.xplan.commons.XPlanVersion.XPLAN_52;
import static de.latlon.xplan.validator.report.SkipCode.SYNTAX_ERRORS;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.COMPLETED;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.SKIPPED;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.TERMINATED_REASON_UNKNOWN;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.TERMINATED_WITH_ERRORS;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;

import de.latlon.xplan.manager.web.shared.RasterEvaluationResult;
import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFinding;
import de.latlon.xplan.validator.geometric.result.GeometricValidationFindingLevel;
import de.latlon.xplan.validator.geometric.result.GeometricValidationRule;
import de.latlon.xplan.validator.planinfo.PlanInfo;
import de.latlon.xplan.validator.planinfo.PlanInfoReport;
import de.latlon.xplan.validator.report.ReportGenerationException;
import de.latlon.xplan.validator.report.SkipCode;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.report.reference.ExternalReferenceReport;
import de.latlon.xplan.validator.report.reference.ExternalReferenceStatus;
import de.latlon.xplan.validator.semantic.report.InvalidFeaturesResult;
import de.latlon.xplan.validator.semantic.report.RuleResult;
import de.latlon.xplan.validator.semantic.report.SemanticValidatorResult;
import de.latlon.xplan.validator.semantic.report.ValidationResultType;
import de.latlon.xplan.validator.syntactic.report.SyntacticValidatorResult;
import de.latlon.xplanbox.api.commons.v1.model.VersionEnum;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReport;
import de.latlon.xplanbox.api.commons.v2.model.ValidationRuleLevel;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:friebe@lat-lon.de">Torsten Friebe</a>
 */
class ValidationReportBuilderTest {

	@Test
	void verifyThat_Builder_noReport_throwsNPE() {
		assertThrows(NullPointerException.class, () -> new ValidationReportBuilder().build());
	}

	@Test
	void verifyThat_Builder_ReturnsInstance() throws ReportGenerationException {
		ValidatorReport sourceReport = mock(ValidatorReport.class);
		ValidationReport report = new ValidationReportBuilder().validatorReport(sourceReport).build();
		assertNotNull(report);
	}

	@Test
	void verifyThat_Builder_AddsFilename() throws ReportGenerationException {
		ValidatorReport sourceReport = mock(ValidatorReport.class);
		ValidationReport report = new ValidationReportBuilder().validatorReport(sourceReport)
			.filename("test.xml")
			.build();
		assertThat(report.getFilename()).contains("test.xml");
	}

	@Test
	void verifyThat_Builder_AddsVersion() throws ReportGenerationException {
		ValidatorReport sourceReport = createSourceReport();
		ValidationReport report = new ValidationReportBuilder().validatorReport(sourceReport).build();
		assertThat(report.getPlans().size()).isEqualTo(1);
		assertThat(report.getPlans().get(0).getVersion()).isEqualTo(VersionEnum.XPLAN_52);
	}

	@Test
	void verifyThat_Builder_AddsWmsUrl() throws URISyntaxException, ReportGenerationException {
		ValidatorReport sourceReport = mock(ValidatorReport.class);
		ValidationReport report = new ValidationReportBuilder().validatorReport(sourceReport)
			.wmsUrl(new URI("file://here"))
			.build();
		assertThat(report.getWmsUrl()).isNotNull();
	}

	@Test
	void verifyThat_Builder_AddRasterEvaluationResult_v1() throws URISyntaxException, ReportGenerationException {
		ValidatorReport sourceReport = mockSourceReportWithRasterEvaluation();

		ValidationReport report = new ValidationReportBuilder().validatorReport(sourceReport)
			.filename("test.xml")
			.wmsUrl(new URI("file:///no/real/file/name"))
			.build();
		assertThat(report.getPlans().size()).isEqualTo(1);
		assertThat(report.getPlans().get(0).getExternalReferencesResult().size()).isEqualTo(1);
		assertThat(report.getPlans().get(0).getRasterEvaluationResult().size()).isEqualTo(1);
	}

	@Test
	void verifyThat_Builder_AddRasterEvaluationResult_v2() throws URISyntaxException, ReportGenerationException {
		ValidatorReport sourceReport = mockSourceReportWithRasterEvaluation();

		de.latlon.xplanbox.api.commons.v2.model.ValidationReport report = new ValidationReportBuilder()
			.validatorReport(sourceReport)
			.filename("test.xml")
			.wmsUrl(new URI("file:///no/real/file/name"))
			.build();
		assertThat(report.getPlans().size()).isEqualTo(1);
		assertThat(report.getPlans().get(0).getExternalReferencesResult().size()).isEqualTo(1);
		assertThat(report.getPlans().get(0).getRasterEvaluationResult().size()).isEqualTo(1);
	}

	@Test
	void verifyThat_Builder_ReturnsCompleteInstance() throws URISyntaxException, ReportGenerationException {
		ValidatorReport sourceReport = mock(ValidatorReport.class);
		ValidationReport report = new ValidationReportBuilder().validatorReport(sourceReport)
			.filename("test.xml")
			.wmsUrl(new URI("file:///no/real/file/name"))
			.build();
		assertThat(report.getWmsUrl()).isNotNull();
		assertThat(report.getFilename()).contains("test");
	}

	@Test
	void verifyThat_Builder_statusSemanticError() throws URISyntaxException, ReportGenerationException {
		ValidatorReport sourceReport = createSourceReport();
		SyntacticValidatorResult syntacticValidatorResult = new SyntacticValidatorResult(emptyList(), null);

		InvalidFeaturesResult invalidFeatureResult = new InvalidFeaturesResult("gml_id", ValidationResultType.ERROR,
				"msg");
		RuleResult rule = new RuleResult("test semantic", "test semantic", singletonList(invalidFeatureResult));
		SemanticValidatorResult semanticValidatorResult = new SemanticValidatorResult();
		semanticValidatorResult.setRules(Collections.singletonList(rule));

		GeometricValidationRule geometricRule = new GeometricValidationRule("0.test", "test");
		GeometricValidatorResult geometricValidatorResult = new GeometricValidatorResult(
				Collections.singletonList(geometricRule));

		sourceReport.setSyntacticValidatorResult(syntacticValidatorResult);
		sourceReport.getPlanInfoReport()
			.getPlanInfos()
			.values()
			.stream()
			.findFirst()
			.get()
			.semanticValidatorResult(semanticValidatorResult)
			.geometricValidatorResult(geometricValidatorResult);

		ValidationReport report = new ValidationReportBuilder().validatorReport(sourceReport)
			.filename("test.xml")
			.wmsUrl(new URI("file:///no/real/file/name"))
			.build();

		assertThat(report.getStatus()).isEqualTo(COMPLETED);
		assertThat(report.getPlans().size()).isEqualTo(1);
		assertThat(report.getGeomfindings()).isNull();

		assertThat(report.getPlans().get(0).getValid()).isFalse();
		assertThat(report.getPlans().get(0).getValidationResult().getSemantisch().getStatus()).isEqualTo(COMPLETED);
		assertThat(report.getPlans().get(0).getValidationResult().getSemantisch().getValid()).isFalse();
		assertThat(report.getPlans().get(0).getValidationResult().getSemantisch().getRules().size()).isEqualTo(1);
		assertThat(
				report.getPlans().get(0).getValidationResult().getSemantisch().getRules().get(0).getFindings().size())
			.isEqualTo(1);
		assertThat(report.getPlans()
			.get(0)
			.getValidationResult()
			.getSemantisch()
			.getRules()
			.get(0)
			.getFindings()
			.get(0)
			.getLevel()).isEqualTo(ValidationRuleLevel.ERROR);
		assertThat(report.getPlans().get(0).getValidationResult().getGeometrisch().getStatus()).isEqualTo(COMPLETED);
		assertThat(report.getPlans().get(0).getValidationResult().getGeometrisch().getValid()).isTrue();

		de.latlon.xplanbox.api.commons.v1.model.ValidationReport validationReportV1 = ValidationReportConverter
			.convertToV1(report);
		assertThat(validationReportV1.getStatus()).isEqualTo("Die Validierung wurde ausgeführt.");
	}

	@Test
	void verifyThat_Builder_statusGeometricError() throws URISyntaxException, ReportGenerationException {
		ValidatorReport sourceReport = createSourceReport();
		SyntacticValidatorResult syntacticValidatorResult = new SyntacticValidatorResult(emptyList(), null);

		RuleResult rule = new RuleResult("test semantic", "test semantic", Collections.emptyList());
		SemanticValidatorResult semanticValidatorResult = new SemanticValidatorResult();
		semanticValidatorResult.setRules(Collections.singletonList(rule));

		GeometricValidationFinding geomError = new GeometricValidationFinding().message("invalid")
			.level(GeometricValidationFindingLevel.ERROR);
		GeometricValidationRule geometricRule = new GeometricValidationRule("0.test", "test").addFinding(geomError);
		GeometricValidatorResult geometricValidatorResult = new GeometricValidatorResult(
				Collections.singletonList(geometricRule));

		sourceReport.setSyntacticValidatorResult(syntacticValidatorResult);
		sourceReport.getPlanInfoReport()
			.getPlanInfos()
			.values()
			.stream()
			.findFirst()
			.get()
			.semanticValidatorResult(semanticValidatorResult)
			.geometricValidatorResult(geometricValidatorResult);

		ValidationReport report = new ValidationReportBuilder().validatorReport(sourceReport)
			.filename("test.xml")
			.wmsUrl(new URI("file:///no/real/file/name"))
			.build();

		assertThat(report.getStatus()).isEqualTo(COMPLETED);
		assertThat(report.getPlans().size()).isEqualTo(1);
		assertThat(report.getGeomfindings()).isNotNull();

		assertThat(report.getPlans().get(0).getValidationResult().getSemantisch().getStatus()).isEqualTo(COMPLETED);
		assertThat(report.getPlans().get(0).getValidationResult().getSemantisch().getValid()).isTrue();
		assertThat(report.getPlans().get(0).getValidationResult().getSemantisch().getRules().size()).isEqualTo(1);
		assertThat(
				report.getPlans().get(0).getValidationResult().getSemantisch().getRules().get(0).getFindings().size())
			.isEqualTo(0);

		assertThat(report.getPlans().get(0).getValidationResult().getGeometrisch().getStatus()).isEqualTo(COMPLETED);
		assertThat(report.getPlans().get(0).getValidationResult().getGeometrisch().getValid()).isFalse();
		assertThat(report.getPlans().get(0).getValidationResult().getGeometrisch().getRules().size()).isEqualTo(1);
		assertThat(
				report.getPlans().get(0).getValidationResult().getGeometrisch().getRules().get(0).getFindings().size())
			.isEqualTo(1);

		assertThat(report.getPlans()
			.get(0)
			.getValidationResult()
			.getGeometrisch()
			.getRules()
			.get(0)
			.getFindings()
			.get(0)
			.getLevel()).isEqualTo(ValidationRuleLevel.ERROR);

		de.latlon.xplanbox.api.commons.v1.model.ValidationReport validationReportV1 = ValidationReportConverter
			.convertToV1(report);
		assertThat(validationReportV1.getStatus()).isEqualTo("Die Validierung wurde ausgeführt.");
	}

	@Test
	void verifyThat_Builder_statusSkip() throws ReportGenerationException {
		ValidatorReport sourceReport = createSourceReport();
		SyntacticValidatorResult syntacticValidatorResult = new SyntacticValidatorResult(emptyList(), null);
		SemanticValidatorResult semanticValidatorResult = new SemanticValidatorResult(SkipCode.SKIPPED);
		GeometricValidatorResult geometricValidatorResult = new GeometricValidatorResult(SkipCode.SKIPPED);

		sourceReport.setSyntacticValidatorResult(syntacticValidatorResult);
		sourceReport.getPlanInfoReport()
			.getPlanInfos()
			.values()
			.stream()
			.findFirst()
			.get()
			.semanticValidatorResult(semanticValidatorResult)
			.geometricValidatorResult(geometricValidatorResult);

		ValidationReport report = new ValidationReportBuilder().validatorReport(sourceReport).build();

		assertThat(report.getStatus()).isEqualTo(COMPLETED);
		assertThat(report.getPlans().size()).isEqualTo(1);
		assertThat(report.getGeomfindings()).isNull();
		assertThat(report.getPlans().get(0).getValidationResult().getSemantisch().getStatus()).isEqualTo(SKIPPED);
		assertThat(report.getPlans().get(0).getValidationResult().getGeometrisch().getStatus()).isEqualTo(SKIPPED);
	}

	@Test
	void verifyThat_Builder_statusSyntaxError() throws URISyntaxException, ReportGenerationException {
		ValidatorReport sourceReport = createSourceReportWithSyntaxError();

		ValidationReport report = new ValidationReportBuilder().validatorReport(sourceReport)
			.filename("test.xml")
			.wmsUrl(new URI("file:///no/real/file/name"))
			.build();

		assertThat(report.getStatus()).isEqualTo(TERMINATED_WITH_ERRORS);
		assertThat(report.getPlans().size()).isEqualTo(0);
		assertThat(report.getGeomfindings()).isNull();

		de.latlon.xplanbox.api.commons.v1.model.ValidationReport reportV1 = ValidationReportConverter
			.convertToV1(report);
		assertThat(reportV1.getStatus()).isEqualTo(
				"Aufgrund von syntaktischen Fehlern konnten nicht alle Validierungsschritte ausgeführt werden. Zudem konnte die BBox und externalReferences nicht ermittelt werden.");
	}

	@Disabled("Improve test: sourceReport does not contain an error!")
	@Test
	void verifyThat_Builder_statusError() throws URISyntaxException, ReportGenerationException {
		ValidatorReport sourceReport = createSourceReport();

		ValidationReport report = new ValidationReportBuilder().validatorReport(sourceReport)
			.filename("test.xml")
			.wmsUrl(new URI("file:///no/real/file/name"))
			.build();

		assertThat(report.getStatus()).isEqualTo(TERMINATED_REASON_UNKNOWN);
		assertThat(report.getGeomfindings()).isNull();
		assertThat(report.getSyntaktisch()).isEqualTo(TERMINATED_REASON_UNKNOWN);
		assertThat(report.getPlans().size()).isEqualTo(1);
		assertThat(report.getPlans().get(0).getValidationResult().getSemantisch().getStatus())
			.isEqualTo(TERMINATED_REASON_UNKNOWN);
		assertThat(report.getPlans().get(0).getValidationResult().getGeometrisch().getStatus())
			.isEqualTo(TERMINATED_REASON_UNKNOWN);

		de.latlon.xplanbox.api.commons.v1.model.ValidationReport reportV1 = ValidationReportConverter
			.convertToV1(report);
		assertThat(reportV1.getStatus()).isEqualTo("Die Validierung wurde nicht oder nur unvollständig ausgeführt.");
	}

	private static ValidatorReport createSourceReportWithSyntaxError() {
		PlanInfoReport planInfoReport = new PlanInfoReport().skipCode(SYNTAX_ERRORS);
		ValidatorReport sourceReport = new ValidatorReport();
		SyntacticValidatorResult syntacticValidatorResult = new SyntacticValidatorResult(singletonList("error"), null);
		sourceReport.setSyntacticValidatorResult(syntacticValidatorResult);
		sourceReport.setPlanInfoReport(planInfoReport);
		return sourceReport;
	}

	private static ValidatorReport createSourceReport() {
		PlanInfo planInfo = new PlanInfo("GML_plan1").name("planName").version(XPLAN_52).type(BP_Plan);
		PlanInfoReport planInfoReport = new PlanInfoReport().planInfos(Collections.singletonMap("GML_plan1", planInfo));
		ValidatorReport sourceReport = new ValidatorReport();
		sourceReport.setPlanInfoReport(planInfoReport);
		return sourceReport;
	}

	private static ValidatorReport mockSourceReportWithRasterEvaluation() {
		PlanInfo planInfo = new PlanInfo("GML_plan1").name("planName")
			.version(XPLAN_52)
			.type(BP_Plan)
			.externalReferenceReport(new ExternalReferenceReport(
					Collections.singletonMap("test.png", ExternalReferenceStatus.AVAILABLE)))
			.rasterEvaluationResults(Collections
				.singletonList(new RasterEvaluationResult("test.png", "EPSG:25832", "EPSG:25832", true, true, true)));
		PlanInfoReport planInfoReport = new PlanInfoReport().planInfos(Collections.singletonMap("GML_plan1", planInfo));
		ValidatorReport sourceReport = mock(ValidatorReport.class);
		when(sourceReport.getPlanInfoReport()).thenReturn(planInfoReport);
		return sourceReport;
	}

}
