/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons;

import static de.latlon.xplanbox.api.commons.v1.model.ExternalReferenceStatusEnum.AVAILABLE;
import static de.latlon.xplanbox.api.commons.v1.model.VersionEnum.XPLAN_60;
import static de.latlon.xplanbox.api.commons.v2.model.CrsStatusEnum.MISSING;
import static de.latlon.xplanbox.api.commons.v2.model.ImageFormatStatusEnum.UNSUPPORTED;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationRuleLevel.ERROR;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationRuleLevel.WARNING;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.COMPLETED;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.SKIPPED;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.TERMINATED_WITH_ERRORS;
import static java.time.Month.JULY;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import de.latlon.xplanbox.api.commons.v1.model.ExternalReferenceResult;
import de.latlon.xplanbox.api.commons.v1.model.RulesMetadata;
import de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantischProfil;
import de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantischRules;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReport;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportPlan;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResult;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultGeometrisch;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultGeometrischRule;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultGeometrischRuleFinding;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultProfil;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultSemantischRule;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultSemantischRuleFinding;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultSemantischWithRulesMetadata;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultSyntaktisch;
import de.latlon.xplan.validator.report.geojson.model.Geometry;
import de.latlon.xplan.validator.report.geojson.model.Point;
import de.latlon.xplan.validator.report.geojson.model.Position;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 8.0
 */
class ValidationReportConverterTest {

	@Test
	void verifyThat_Builder_convertToV1_syntaxErrors() {
		ValidationReport sourceReport = buildValidationReportSyntaxErrors();
		ValidationReportValidationResultSyntaktisch sourceSyntaktisch = sourceReport.getSyntaktisch();

		de.latlon.xplanbox.api.commons.v1.model.ValidationReport validationReport = ValidationReportConverter
			.convertToV1(sourceReport);

		de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantisch validationResultSemantisch = validationReport
			.getValidationResult()
			.getSemantisch();
		de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSyntaktisch validationResultSyntaktisch = validationReport
			.getValidationResult()
			.getSyntaktisch();
		de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultGeometrisch validationResultGeometrisch = validationReport
			.getValidationResult()
			.getGeometrisch();
		List<ValidationReportValidationResultSemantischProfil> validationResultProfile = validationReport
			.getValidationResult()
			.getProfile();

		assertThat(validationReport.getValid()).isFalse();

		assertThat(validationResultSyntaktisch.getValid()).isEqualTo(sourceSyntaktisch.getValid());
		assertThat(validationResultSyntaktisch.getMessages().size()).isEqualTo(sourceSyntaktisch.getErrors().size());

		assertThat(validationResultSemantisch.getValid()).isFalse();
		assertThat(validationResultSemantisch.getRules().size()).isEqualTo(0);

		assertThat(validationResultGeometrisch.getValid()).isFalse();
		assertThat(validationResultGeometrisch.getErrors().size()).isEqualTo(0);
		assertThat(validationResultGeometrisch.getWarnings().size()).isEqualTo(0);

		assertThat(validationResultProfile.size()).isEqualTo(0);
	}

	@Test
	void verifyThat_Builder_convertToV1_skipSemanticAndGeometrisch() {
		ValidationReport sourceReport = buildValidationReportSkipSemantischAndGeometrisch();
		ValidationReportValidationResultSyntaktisch sourceSyntaktisch = sourceReport.getSyntaktisch();

		de.latlon.xplanbox.api.commons.v1.model.ValidationReport validationReport = ValidationReportConverter
			.convertToV1(sourceReport);
		de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantisch validationResultSemantisch = validationReport
			.getValidationResult()
			.getSemantisch();
		de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSyntaktisch validationResultSyntaktisch = validationReport
			.getValidationResult()
			.getSyntaktisch();
		de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultGeometrisch validationResultGeometrisch = validationReport
			.getValidationResult()
			.getGeometrisch();

		assertThat(validationResultSyntaktisch.getValid()).isEqualTo(sourceSyntaktisch.getValid());
		assertThat(validationResultSyntaktisch.getMessages().size()).isEqualTo(sourceSyntaktisch.getErrors().size());

		assertThat(validationResultSemantisch).isNull();
		assertThat(validationResultGeometrisch).isNull();
	}

	@Test
	void verifyThat_Builder_convertToV1() throws URISyntaxException {
		ValidationReport sourceReport = buildValidationReport();
		ValidationReportValidationResultSemantischWithRulesMetadata sourceSemantisch = sourceReport.getPlans()
			.get(0)
			.getValidationResult()
			.getSemantisch();
		ValidationReportValidationResultSyntaktisch sourceSyntaktisch = sourceReport.getSyntaktisch();
		ValidationReportValidationResultGeometrisch sourceGeometrisch = sourceReport.getPlans()
			.get(0)
			.getValidationResult()
			.getGeometrisch();
		List<ValidationReportValidationResultProfil> sourceProfiles = sourceReport.getPlans()
			.get(0)
			.getValidationResult()
			.getProfile();

		de.latlon.xplanbox.api.commons.v1.model.ValidationReport validationReport = ValidationReportConverter
			.convertToV1(sourceReport);
		de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantisch validationResultSemantisch = validationReport
			.getValidationResult()
			.getSemantisch();
		de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSyntaktisch validationResultSyntaktisch = validationReport
			.getValidationResult()
			.getSyntaktisch();
		de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultGeometrisch validationResultGeometrisch = validationReport
			.getValidationResult()
			.getGeometrisch();
		List<ValidationReportValidationResultSemantischProfil> validationResultProfile = validationReport
			.getValidationResult()
			.getProfile();

		assertThat(validationResultSyntaktisch.getValid()).isEqualTo(sourceSyntaktisch.getValid());
		assertThat(validationResultSyntaktisch.getMessages().size()).isEqualTo(sourceSyntaktisch.getErrors().size());

		assertThat(validationResultSemantisch.getValid()).isEqualTo(sourceSemantisch.getValid());
		assertThat(validationResultSemantisch.getRules().size()).isEqualTo(sourceSemantisch.getRules().size());

		ValidationReportValidationResultSemantischRules rule1 = getRule(validationResultSemantisch, "3.1.1.2");
		ValidationReportValidationResultSemantischRule sourceRule1 = getSourceRule(sourceSemantisch, "3.1.1.2");
		assertThat(rule1.getName()).isEqualTo(sourceRule1.getId());
		assertThat(rule1.getMessage()).isEqualTo(sourceRule1.getTitle());
		assertThat(rule1.getErroredFeatures().size() + rule1.getWarnedFeatures().size())
			.isEqualTo(sourceRule1.getFindings().size());

		ValidationReportValidationResultSemantischRules rule2 = getRule(validationResultSemantisch, "3.2.3.1");
		ValidationReportValidationResultSemantischRule sourceRule2 = getSourceRule(sourceSemantisch, "3.2.3.1");
		assertThat(rule2.getName()).isEqualTo(sourceRule2.getId());
		assertThat(rule2.getMessage()).isEqualTo(sourceRule2.getTitle());
		assertThat(rule2.getErroredFeatures().size()).isEqualTo(0);
		assertThat(rule2.getWarnedFeatures().size()).isEqualTo(1);

		ValidationReportValidationResultSemantischRules rule3 = getRule(validationResultSemantisch, "3.2.4.1");
		ValidationReportValidationResultSemantischRule sourceRule3 = getSourceRule(sourceSemantisch, "3.2.4.1");
		assertThat(rule3.getName()).isEqualTo(sourceRule3.getId());
		assertThat(rule3.getMessage()).isEqualTo(sourceRule3.getTitle());
		assertThat(rule3.getErroredFeatures().size()).isEqualTo(1);
		assertThat(rule3.getWarnedFeatures().size()).isEqualTo(1);

		assertThat(validationResultGeometrisch.getValid()).isEqualTo(sourceGeometrisch.getValid());
		ValidationReportValidationResultGeometrischRule sourceGeomRule1 = getSourceRule(sourceGeometrisch, "2.2.3.1");
		ValidationReportValidationResultGeometrischRule sourceGeomRule2 = getSourceRule(sourceGeometrisch, "2.2.1");
		List<String> errors = validationResultGeometrisch.getErrors();
		List<String> warnings = validationResultGeometrisch.getWarnings();
		assertThat(errors.size()).isEqualTo(2);
		assertThat(warnings.size()).isEqualTo(1);
		assertThat(errors).anyMatch(error -> error.startsWith(sourceGeomRule1.getId()));
		assertThat(errors).anyMatch(error -> error.startsWith(sourceGeomRule2.getId()));
		assertThat(warnings).anyMatch(warning -> warning.startsWith(sourceGeomRule2.getId()));

		assertThat(validationResultProfile.size()).isEqualTo(sourceProfiles.size());
		assertThat(validationResultProfile.get(0).getName())
			.isEqualTo(sourceProfiles.get(0).getRulesMetadata().getName());
		assertThat(validationResultProfile.get(0).getDescription())
			.isEqualTo(sourceProfiles.get(0).getRulesMetadata().getDescription());
		assertThat(validationResultProfile.get(0).getResult().getValid())
			.isEqualTo(sourceProfiles.get(0).getResult().getValid());
		assertThat(validationResultProfile.get(0).getResult().getRules().size())
			.isEqualTo(sourceProfiles.get(0).getResult().getRules().size());
	}

	private static ValidationReportValidationResultSemantischRules getRule(
			de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantisch validationResultSemantisch,
			String id) {
		return validationResultSemantisch.getRules().stream().filter(r -> id.equals(r.getName())).findFirst().get();
	}

	private static ValidationReportValidationResultSemantischRule getSourceRule(
			ValidationReportValidationResultSemantischWithRulesMetadata sourceSemantisch, String id) {
		return sourceSemantisch.getRules().stream().filter(r -> id.equals(r.getId())).findFirst().get();
	}

	private static ValidationReportValidationResultGeometrischRule getSourceRule(
			ValidationReportValidationResultGeometrisch sourceGeometrisch, String id) {
		return sourceGeometrisch.getRules().stream().filter(r -> id.equals(r.getId())).findFirst().get();
	}

	private ValidationReport buildValidationReport() throws URISyntaxException {
		ExternalReferenceResult externalReferenceResult = new ExternalReferenceResult().name("test.png")
			.status(AVAILABLE);
		de.latlon.xplanbox.api.commons.v1.model.RasterEvaluationResult rasterEvaluationResult = new de.latlon.xplanbox.api.commons.v1.model.RasterEvaluationResult()
			.name("test.png")
			.crsStatus(MISSING)
			.imageFormatStatus(UNSUPPORTED);
		ValidationReportValidationResult validationResult = new ValidationReportValidationResult();
		ValidationReportValidationResultSyntaktisch syntaktischResult = new ValidationReportValidationResultSyntaktisch()
			.valid(true)
			.status(COMPLETED);
		ValidationReportValidationResultSemantischWithRulesMetadata semantischResult = (ValidationReportValidationResultSemantischWithRulesMetadata) new ValidationReportValidationResultSemantischWithRulesMetadata()
			.valid(false)
			.status(COMPLETED)
			.rules(asList(
					new ValidationReportValidationResultSemantischRule().id("3.1.1.2")
						.title("XP_Plan: Relationen auf Begruendungs-Abschnitte"),
					new ValidationReportValidationResultSemantischRule().id("3.2.3.1")
						.title("XP_Hoehenangabe: Konsistenz der verschiedenen Hoehenangaben")
						.findings(singletonList(new ValidationReportValidationResultSemantischRuleFinding()
							.message("Sollte nicht in m sein.")
							.level(WARNING)
							.gmlIds(asList("GML_1223", "GML_7587")))),
					new ValidationReportValidationResultSemantischRule().id("3.2.4.1")
						.title("XP_XXX: Konsistenz der XXXX")
						.findings(asList(
								new ValidationReportValidationResultSemantischRuleFinding()
									.message("Sollte nicht XXX sein.")
									.level(WARNING)
									.gmlIds(singletonList("GML_2589")),
								new ValidationReportValidationResultSemantischRuleFinding()
									.message("Darf nicht xy sein.")
									.level(ERROR)
									.gmlIds(singletonList("GML_1223"))))));
		ValidationReportValidationResultGeometrisch geometrischResult = new ValidationReportValidationResultGeometrisch()
			.valid(true)
			.status(COMPLETED)
			.rules(asList(new ValidationReportValidationResultGeometrischRule().id("2.2.3.1")
				.title("Geltungsbereich")
				.findings(singletonList(new ValidationReportValidationResultGeometrischRuleFinding().message(
						"Objekt ausserhalb des Geltungsbereich des Plans und des Bereichs: Es wird ein Loch geschnitten.")
					.level(ERROR)
					.gmlIds(asList("GML_1234", "GML_56789"))
					.markerGeom(createPoint(6.5, 10.8)))),
					new ValidationReportValidationResultGeometrischRule().id("2.2.1")
						.title("Flaechenschlussbedingung")
						.findings(asList(new ValidationReportValidationResultGeometrischRuleFinding().message(
								"Objekt ausserhalb des Geltungsbereich des Plans und des Bereichs: Es wird ein Loch geschnitten.")
							.level(ERROR)
							.gmlIds(asList("GML_1234", "GML_56789"))
							.markerGeom(createPoint(8.9, 11.5)),
								new ValidationReportValidationResultGeometrischRuleFinding().message(
										"Objekt ausserhalb des Geltungsbereich des Plans und des Bereichs: Es wird ein Loch geschnitten.")
									.level(WARNING)
									.gmlIds(singletonList("GML_999"))
									.markerGeom(createPoint(7.1, 10.1))))));

		ValidationReportValidationResultProfil profileResult = new ValidationReportValidationResultProfil()
			.rulesMetadata(new RulesMetadata().id("test").name("Test").version("0.0.1"))
			.result(new ValidationReportValidationResultSemantischWithRulesMetadata().valid(true)
				.status(COMPLETED)
				.rules(singletonList(
						new ValidationReportValidationResultSemantischRule().id("7.8.7").title("Test Porfil Rule"))));

		ValidationReportPlan plan = new ValidationReportPlan().name("test")
			.version(XPLAN_60)
			.valid(true)
			.status(COMPLETED)
			.externalReferencesResult(singletonList(externalReferenceResult))
			.rasterEvaluationResult(Collections.singletonList(rasterEvaluationResult))
			.validationResult(validationResult.semantisch(semantischResult)
				.geometrisch(geometrischResult)
				.profile(singletonList(profileResult)));
		return new ValidationReport().date(LocalDateTime.of(2024, JULY, 31, 9, 55))
			.filename("text.xml")
			.validationName("test")
			.status(COMPLETED)
			.wmsUrl(new URI("file:///no/real/file/name"))
			.syntaktisch(syntaktischResult)
			.addPlanItem(plan);
	}

	private ValidationReport buildValidationReportSyntaxErrors() {
		ValidationReportValidationResultSyntaktisch syntaktischResult = new ValidationReportValidationResultSyntaktisch()
			.valid(false)
			.errors(asList("Fehler 1", "Fehler 2"))
			.status(COMPLETED);

		return new ValidationReport().date(LocalDateTime.of(2024, JULY, 31, 9, 55))
			.status(TERMINATED_WITH_ERRORS)
			.filename("text.xml")
			.syntaktisch(syntaktischResult);
	}

	private ValidationReport buildValidationReportSkipSemantischAndGeometrisch() {
		ValidationReportValidationResult validationResult = new ValidationReportValidationResult();
		ValidationReportValidationResultSyntaktisch syntaktischResult = new ValidationReportValidationResultSyntaktisch()
			.valid(false)
			.errors(asList("Fehler 1", "Fehler 2"))
			.status(COMPLETED);
		ValidationReportValidationResultSemantischWithRulesMetadata semantischResult = (ValidationReportValidationResultSemantischWithRulesMetadata) new ValidationReportValidationResultSemantischWithRulesMetadata()
			.status(SKIPPED);
		ValidationReportValidationResultGeometrisch geometrischResult = new ValidationReportValidationResultGeometrisch()
			.status(SKIPPED);

		ValidationReportPlan plan = new ValidationReportPlan().name("test")
			.version(XPLAN_60)
			.valid(true)
			.status(COMPLETED)
			.validationResult(validationResult.semantisch(semantischResult).geometrisch(geometrischResult));

		return new ValidationReport().date(LocalDateTime.of(2024, JULY, 31, 9, 55))
			.status(COMPLETED)
			.filename("text.xml")
			.syntaktisch(syntaktischResult)
			.addPlanItem(plan);
	}

	private Geometry createPoint(double x, double y) {
		Position position = new Position();
		position.addAll(asList(BigDecimal.valueOf(x), BigDecimal.valueOf(y)));
		return new Point().position(position);
	}

}
