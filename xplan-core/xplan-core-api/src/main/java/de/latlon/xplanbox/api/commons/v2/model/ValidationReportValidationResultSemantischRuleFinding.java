/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons.v2.model;

import java.util.List;
import java.util.Objects;

import jakarta.validation.Valid;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class ValidationReportValidationResultSemantischRuleFinding {

	private @Valid String message;

	private @Valid ValidationRuleLevel level;

	private @Valid List<String> gmlIds;

	public ValidationReportValidationResultSemantischRuleFinding message(@Valid String message) {
		this.message = message;
		return this;
	}

	public @Valid String getMessage() {
		return message;
	}

	public void setMessage(@Valid String message) {
		this.message = message;
	}

	public ValidationReportValidationResultSemantischRuleFinding level(@Valid ValidationRuleLevel level) {
		this.level = level;
		return this;
	}

	public @Valid ValidationRuleLevel getLevel() {
		return level;
	}

	public void setLevel(@Valid ValidationRuleLevel level) {
		this.level = level;
	}

	public ValidationReportValidationResultSemantischRuleFinding gmlIds(@Valid List<String> gmlIds) {
		this.gmlIds = gmlIds;
		return this;
	}

	public @Valid List<String> getGmlIds() {
		return gmlIds;
	}

	public void setGmlIds(@Valid List<String> gmlIds) {
		this.gmlIds = gmlIds;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ValidationReportValidationResultSemantischRuleFinding that = (ValidationReportValidationResultSemantischRuleFinding) o;
		return Objects.equals(message, that.message) && level == that.level && Objects.equals(gmlIds, that.gmlIds);
	}

	@Override
	public int hashCode() {
		return Objects.hash(message, level, gmlIds);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ValidationReportValidationResultGeometrisch {\n");

		sb.append("    message: ").append(toIndentedString(message)).append("\n");
		sb.append("    level: ").append(toIndentedString(level)).append("\n");
		sb.append("    gmlIds: ").append(toIndentedString(gmlIds)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces (except the
	 * first line).
	 */
	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
