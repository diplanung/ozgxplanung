/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons.v2.model;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.latlon.xplanbox.api.commons.v1.model.ExternalReferenceResult;
import de.latlon.xplanbox.api.commons.v1.model.PlanInfoBbox;
import de.latlon.xplanbox.api.commons.v1.model.RasterEvaluationResult;
import de.latlon.xplanbox.api.commons.v1.model.VersionEnum;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidationReportPlan {

	private @Valid String name;

	private @Valid String type;

	private @Valid VersionEnum version;

	private @Valid PlanInfoBbox bbox;

	// kann null sein (wenn status = running)
	private @Valid Boolean valid;

	@JsonInclude(NON_NULL)
	private @Valid ValidationStatus status;

	private @Valid List<ExternalReferenceResult> externalReferencesResult = new ArrayList<>();

	private @Valid List<RasterEvaluationResult> rasterEvaluationResult = new ArrayList<>();

	private @Valid ValidationReportValidationResult validationResult;

	public ValidationReportPlan name(String name) {
		this.name = name;
		return this;
	}

	@Schema(example = "Othmarschen3")
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ValidationReportPlan type(String type) {
		this.type = type;
		return this;
	}

	@Schema(example = "BP_Plan")
	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ValidationReportPlan version(VersionEnum version) {
		this.version = version;
		return this;
	}

	@Schema(example = "XPLAN_51")
	public VersionEnum getVersion() {
		return version;
	}

	public void setVersion(VersionEnum version) {
		this.version = version;
	}

	public ValidationReportPlan bbox(PlanInfoBbox bbox) {
		this.bbox = bbox;
		return this;
	}

	@Schema
	public PlanInfoBbox getBbox() {
		return bbox;
	}

	public void setBbox(PlanInfoBbox bbox) {
		this.bbox = bbox;
	}

	public ValidationReportPlan valid(Boolean valid) {
		this.valid = valid;
		return this;
	}

	@Schema(example = "false")
	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	public ValidationReportPlan status(ValidationStatus status) {
		this.status = status;
		return this;
	}

	@Schema(example = "COMPLETED",
			description = "The status of the validation. If missing the validation is not started yet.")
	public ValidationStatus getStatus() {
		return status;
	}

	public void setStatus(ValidationStatus status) {
		this.status = status;
	}

	public ValidationReportPlan externalReferencesResult(
			@Valid List<ExternalReferenceResult> externalReferencesResult) {
		if (externalReferencesResult != null)
			this.externalReferencesResult = externalReferencesResult;
		return this;
	}

	@ArraySchema
	public List<ExternalReferenceResult> getExternalReferencesResult() {
		return externalReferencesResult;
	}

	public void setExternalReferencesResult(List<ExternalReferenceResult> externalReferencesResult) {
		if (externalReferencesResult != null)
			this.externalReferencesResult = externalReferencesResult;
	}

	public ValidationReportPlan rasterEvaluationResult(@Valid List<RasterEvaluationResult> rasterEvaluationResults) {
		if (rasterEvaluationResults != null)
			this.rasterEvaluationResult = rasterEvaluationResults;
		return this;
	}

	public @Valid List<RasterEvaluationResult> getRasterEvaluationResult() {
		return rasterEvaluationResult;
	}

	public void setRasterEvaluationResult(@Valid List<RasterEvaluationResult> rasterEvaluationResult) {
		if (rasterEvaluationResult != null)
			this.rasterEvaluationResult = rasterEvaluationResult;
	}

	public ValidationReportPlan validationResult(ValidationReportValidationResult validationResult) {
		this.validationResult = validationResult;
		return this;
	}

	@Schema
	public ValidationReportValidationResult getValidationResult() {
		return validationResult;
	}

	public void setValidationResult(ValidationReportValidationResult validationResult) {
		this.validationResult = validationResult;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ValidationReport {\n");

		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    type: ").append(toIndentedString(type)).append("\n");
		sb.append("    version: ").append(toIndentedString(version)).append("\n");
		sb.append("    bbox: ").append(toIndentedString(bbox)).append("\n");
		sb.append("    valid: ").append(toIndentedString(valid)).append("\n");
		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("    externalReferencesResult: ").append(toIndentedString(externalReferencesResult)).append("\n");
		sb.append("    rasterEvaluationResults: ").append(toIndentedString(rasterEvaluationResult)).append("\n");
		sb.append("    validationResult: ").append(toIndentedString(validationResult)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces (except the
	 * first line).
	 */
	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
