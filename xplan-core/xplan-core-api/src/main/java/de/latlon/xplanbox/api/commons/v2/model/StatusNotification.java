/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons.v2.model;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public abstract class StatusNotification {

	@JsonInclude(NON_NULL)
	private String errorMsg;

	@JsonInclude(NON_EMPTY)
	private List<ResponseLink> links = new ArrayList<>();

	public StatusNotification() {
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public StatusNotification errorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
		return this;
	}

	public List<ResponseLink> getLinks() {
		return links;
	}

	public void setLinks(List<ResponseLink> links) {
		this.links = links;
	}

	public StatusNotification links(List<ResponseLink> links) {
		this.links = links;
		return this;
	}

	public StatusNotification links(ResponseLink... links) {
		this.links = List.of(links);
		return this;
	}

	public void addLink(ResponseLink link) {
		if (this.links == null)
			this.links = new ArrayList<>();
		this.links.add(link);
	}

}
