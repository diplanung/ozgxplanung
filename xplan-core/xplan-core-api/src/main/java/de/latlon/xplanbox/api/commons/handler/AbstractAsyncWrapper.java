/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons.handler;

import static org.slf4j.LoggerFactory.getLogger;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import de.latlon.core.validator.events.EventSender;
import de.latlon.core.validator.events.ValidationRequestedEvent;
import de.latlon.xplanbox.api.commons.exception.AsyncTimeout;
import org.slf4j.Logger;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public abstract class AbstractAsyncWrapper {

	private static final Logger LOG = getLogger(AbstractAsyncWrapper.class);

	private final EventSender eventSender;

	private final Map<String, Boolean> waitingUuids = Collections.synchronizedMap(new HashMap<>());

	protected AbstractAsyncWrapper(EventSender eventSender) {
		this.eventSender = eventSender;
	}

	public boolean sendEvent(ValidationRequestedEvent event) throws AsyncTimeout {
		eventSender.sendPrivateEvent(event);

		String uuid = event.getUuid();
		LOG.info("Event sent. Waiting for processing of {}", uuid);
		waitingUuids.put(uuid, null);

		// TODO: which value would be correct?
		LocalDateTime maxWaitDateTime = Instant.ofEpochMilli(Long.MAX_VALUE)
			.atZone(ZoneId.systemDefault())
			.toLocalDateTime();
		boolean waitFinished = false;
		while (!waitFinished) {
			synchronized (waitingUuids) {
				try {
					waitingUuids.wait();
				}
				catch (InterruptedException e) {
					// ignoring
				}
			}

			if (waitingUuids.get(uuid) != null) {
				LOG.info("Finished waiting for processing of {}", uuid);
				waitFinished = true;
			}
			else if (LocalDateTime.now().isAfter(maxWaitDateTime)) {
				throw new AsyncTimeout(uuid);
			}
			else {
				LOG.info("Still waiting for processing of {}", uuid);
			}
		}

		return waitingUuids.remove(uuid);
	}

	protected void someEventFinished(String uuid) {
		if (waitingUuids.containsKey(uuid)) {
			LOG.info("Notifying waiting threads for finished processing of {}", uuid);
			waitingUuids.put(uuid, Boolean.TRUE);
			synchronized (waitingUuids) {
				waitingUuids.notifyAll();
			}
		}
		else {
			LOG.info("No waiting threads for {}. Ignoring", uuid);
		}
	}

}
