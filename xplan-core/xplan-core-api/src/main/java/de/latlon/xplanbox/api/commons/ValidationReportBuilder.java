/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons;

import static de.latlon.xplan.validator.report.geojson.GeoJsonBuilder.createGeoJsonFailures;
import static de.latlon.xplanbox.api.commons.DateConverter.convertToLocalDateTime;
import static de.latlon.xplanbox.api.commons.v1.model.VersionEnum.fromXPlanVersion;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationRuleLevel.ERROR;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationRuleLevel.WARNING;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.COMPLETED;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.SKIPPED;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.TERMINATED_REASON_UNKNOWN;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.TERMINATED_WITH_ERRORS;
import static java.util.Objects.requireNonNull;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import de.latlon.xplan.validator.geometric.report.GeometricValidatorResult;
import de.latlon.xplan.validator.geometric.result.GeometricValidationRule;
import de.latlon.xplan.validator.planinfo.PlanInfo;
import de.latlon.xplan.validator.report.ReportGenerationException;
import de.latlon.xplan.validator.report.SkipCode;
import de.latlon.xplan.validator.report.ValidatorReport;
import de.latlon.xplan.validator.report.ValidatorResult;
import de.latlon.xplan.validator.report.geojson.GeoJsonGeometryBuilder;
import de.latlon.xplan.validator.report.reference.ExternalReferenceReport;
import de.latlon.xplan.validator.semantic.report.InvalidFeaturesResult;
import de.latlon.xplan.validator.semantic.report.SemanticValidatorResult;
import de.latlon.xplan.validator.semantic.report.ValidationResultType;
import de.latlon.xplan.validator.syntactic.report.SyntacticValidatorResult;
import de.latlon.xplanbox.api.commons.v1.model.ExternalReferenceResult;
import de.latlon.xplanbox.api.commons.v1.model.ExternalReferenceStatusEnum;
import de.latlon.xplanbox.api.commons.v1.model.PlanInfoBbox;
import de.latlon.xplanbox.api.commons.v1.model.RasterEvaluationResult;
import de.latlon.xplanbox.api.commons.v1.model.RulesMetadata;
import de.latlon.xplanbox.api.commons.v2.model.CrsStatusEnum;
import de.latlon.xplanbox.api.commons.v2.model.ImageFormatStatusEnum;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReport;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportPlan;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResult;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultGeometrisch;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultGeometrischRule;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultGeometrischRuleFinding;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultProfil;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultSemantisch;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultSemantischRule;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultSemantischRuleFinding;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultSemantischWithRulesMetadata;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultSyntaktisch;
import de.latlon.xplanbox.api.commons.v2.model.ValidationRuleLevel;
import de.latlon.xplanbox.api.commons.v2.model.ValidationStatus;
import org.deegree.geometry.Envelope;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class ValidationReportBuilder {

	private ValidatorReport validatorReport;

	private String filename;

	private URI wmsUrl;

	public ValidationReportBuilder validatorReport(ValidatorReport validatorReport) {
		this.validatorReport = validatorReport;
		return this;
	}

	public ValidationReportBuilder filename(String filename) {
		this.filename = filename;
		return this;
	}

	/**
	 * @deprecated XPlanValidatorWMS is deprecated and will be removed in a future version
	 */
	@Deprecated
	public ValidationReportBuilder wmsUrl(URI wmsUrl) {
		this.wmsUrl = wmsUrl;
		return this;
	}

	public ValidationReport build() throws ReportGenerationException {
		requireNonNull(validatorReport);
		return new de.latlon.xplanbox.api.commons.v2.model.ValidationReport().filename(filename)
			.validationName(validatorReport.getValidationName())
			.date(convertToLocalDateTime(validatorReport.getDate()))
			.status(status())
			.wmsUrl(wmsUrl)
			.syntaktisch(syntaktischResult())
			.plans(validationReportPlans())
			.geomfindings(createGeoJsonFailures(validatorReport));
	}

	private List<ValidationReportPlan> validationReportPlans() {
		return validatorReport.getPlanInfoReport() != null
				? validatorReport.getPlanInfoReport().getPlanInfos().values().stream().map(planInfo -> {
					List<ExternalReferenceResult> externalReferencesResult = externalReferencesResult(
							planInfo.getExternalReferenceReport());
					return new ValidationReportPlan().name(planInfo.getName())
						.type(planInfo.getType().name())
						.version(fromXPlanVersion(planInfo.getVersion()))
						.valid(planInfo.isValid())
						.status(status())
						.bbox(asBBox(planInfo.getBboxIn4326()))
						.externalReferencesResult(externalReferencesResult)
						.rasterEvaluationResult(rasterEvaluationResults(planInfo, externalReferencesResult))
						.validationResult(validationResult(planInfo));
				}).toList() : Collections.emptyList();
	}

	private ValidationStatus statusSyntax() {
		SyntacticValidatorResult result = validatorReport.getSyntacticValidatorResult();
		if (result == null)
			return TERMINATED_REASON_UNKNOWN;
		return COMPLETED;
	}

	private ValidationStatus status() {
		return status(null);
	}

	private ValidationStatus status(ValidatorResult validatorResult) {
		SyntacticValidatorResult result = validatorReport.getSyntacticValidatorResult();
		if (result == null)
			return TERMINATED_REASON_UNKNOWN;
		if (!result.isValid())
			return TERMINATED_WITH_ERRORS;
		if (validatorResult != null && SkipCode.SKIPPED.equals(validatorResult.getSkipCode()))
			return SKIPPED;
		return COMPLETED;
	}

	private PlanInfoBbox asBBox(Envelope bbox) {
		if (bbox != null) {
			return new PlanInfoBbox().maxX(bbox.getMax().get0())
				.maxY(bbox.getMax().get1())
				.minX(bbox.getMin().get0())
				.minY(bbox.getMin().get1())
				.crs(bbox.getCoordinateSystem().getName());
		}
		return null;
	}

	private List<ExternalReferenceResult> externalReferencesResult(ExternalReferenceReport externalReferenceReport) {
		if (externalReferenceReport != null) {
			List<ExternalReferenceResult> externalReferenceAndStatus = new ArrayList<>();
			externalReferenceReport.getReferencesAndStatus()
				.forEach((name, status) -> externalReferenceAndStatus.add(new ExternalReferenceResult().name(name)
					.status(ExternalReferenceStatusEnum.fromExternalReferenceStatus(status))));
			return externalReferenceAndStatus;
		}
		return Collections.emptyList();
	}

	private List<RasterEvaluationResult> rasterEvaluationResults(PlanInfo planInfo,
			List<ExternalReferenceResult> externalReferenceAndStatus) {
		List<RasterEvaluationResult> rasterEvaluationResults = new ArrayList<>();
		planInfo.getRasterEvaluationResults().forEach(rasterEvaluationResult -> {
			Optional<ExternalReferenceResult> matchingExternalReference = externalReferenceAndStatus.stream()
				.filter(externalReferenceResult -> externalReferenceResult.getName()
					.equals(rasterEvaluationResult.getRasterName()))
				.findFirst();
			if (matchingExternalReference.isPresent() && rasterEvaluationResult.isRasterFileAvailable()) {
				CrsStatusEnum crsStatus = CrsStatusEnum.fromResult(rasterEvaluationResult.isConfiguredCrs(),
						rasterEvaluationResult.isCrsSet());
				ImageFormatStatusEnum imageFormatStatus = ImageFormatStatusEnum
					.fromResult(rasterEvaluationResult.isSupportedImageFormat());
				RasterEvaluationResult rasterEvaluationResult1 = new RasterEvaluationResult()
					.name(matchingExternalReference.get().getName())
					.crsStatus(crsStatus)
					.imageFormatStatus(imageFormatStatus);
				rasterEvaluationResults.add(rasterEvaluationResult1);
			}
		});
		return rasterEvaluationResults;
	}

	private RulesMetadata rulesMetadata(SemanticValidatorResult result) {
		if (result != null) {
			de.latlon.xplan.validator.semantic.configuration.metadata.RulesMetadata rulesMetadata = result
				.getRulesMetadata();
			if (rulesMetadata != null)
				return new RulesMetadata().version(rulesMetadata.getVersion()).source(rulesMetadata.getSource());
		}
		return null;
	}

	private ValidationReportValidationResult validationResult(PlanInfo planInfo) {
		return new ValidationReportValidationResult().semantisch(semantischResult(planInfo))
			.geometrisch(geometrischResult(planInfo))
			.profile(profileResult(planInfo));
	}

	private ValidationReportValidationResultSyntaktisch syntaktischResult() {
		ValidationReportValidationResultSyntaktisch validationReportValidationResultSyntaktisch = new ValidationReportValidationResultSyntaktisch()
			.status(statusSyntax());
		if (validatorReport.getSyntacticValidatorResult() != null) {
			SyntacticValidatorResult result = validatorReport.getSyntacticValidatorResult();
			return validationReportValidationResultSyntaktisch.valid(result.isValid()).errors(result.getMessages());
		}
		return validationReportValidationResultSyntaktisch;
	}

	private ValidationReportValidationResultGeometrisch geometrischResult(PlanInfo planInfo) {
		if (planInfo.getGeometricValidatorResult() != null) {
			GeometricValidatorResult geometricValidatorResult = planInfo.getGeometricValidatorResult();
			ValidationStatus status = status(geometricValidatorResult);
			if (COMPLETED.equals(status)) {
				List<ValidationReportValidationResultGeometrischRule> planRules = geometricValidatorResult.getRules()
					.stream()
					.map(rule -> new ValidationReportValidationResultGeometrischRule().id(rule.getId())
						.title(rule.getTitle())
						.findings(findings(rule)))
					.toList();
				boolean isValid = geometricValidatorResult.isValid();
				return new ValidationReportValidationResultGeometrisch().status(status).rules(planRules).valid(isValid);
			}
			return new ValidationReportValidationResultGeometrisch().status(status);
		}
		return null;
	}

	private ValidationReportValidationResultSemantischWithRulesMetadata semantischResult(PlanInfo planInfo) {
		SemanticValidatorResult semanticValidatorResult = planInfo.getSemanticValidatorResult();
		if (semanticValidatorResult != null) {
			ValidationStatus status = status(semanticValidatorResult);
			if (COMPLETED.equals(status)) {
				return validatorResultSemantisch(semanticValidatorResult)
					.rulesMetadata(rulesMetadata(semanticValidatorResult));
			}
			return (ValidationReportValidationResultSemantischWithRulesMetadata) new ValidationReportValidationResultSemantischWithRulesMetadata()
				.status(status);
		}
		return null;
	}

	private static List<ValidationReportValidationResultGeometrischRuleFinding> findings(GeometricValidationRule rule) {
		return rule.getFindings()
			.stream()
			.map(finding -> new ValidationReportValidationResultGeometrischRuleFinding().message(finding.getMessage())
				.level(ValidationRuleLevel.valueOf(finding.getLevel().name()))
				.gmlIds(finding.getGmlIds())
				.markerGeom(GeoJsonGeometryBuilder.createGeometry(finding.getMarkerGeom())))
			.toList();
	}

	private List<ValidationReportValidationResultProfil> profileResult(PlanInfo planInfo) {
		if (planInfo.getSemanticProfileValidatorResults() != null) {
			List<SemanticValidatorResult> profileResults = planInfo.getSemanticProfileValidatorResults();
			return profileResults.stream().map(profileResult -> {
				de.latlon.xplan.validator.semantic.configuration.metadata.RulesMetadata rulesMetadata = profileResult
					.getRulesMetadata();
				ValidationReportValidationResultSemantisch result = validatorResultSemantischProfil(profileResult);
				return new ValidationReportValidationResultProfil().rulesMetadata(rulesMetadataProfiles(rulesMetadata))
					.result(result);
			}).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	private static RulesMetadata rulesMetadataProfiles(
			de.latlon.xplan.validator.semantic.configuration.metadata.RulesMetadata rulesMetadata) {
		if (rulesMetadata != null) {
			return new RulesMetadata().id(rulesMetadata.getId())
				.name(rulesMetadata.getName())
				.version(rulesMetadata.getVersion())
				.description(rulesMetadata.getDescription())
				.source(rulesMetadata.getSource());
		}
		return null;
	}

	private ValidationReportValidationResultSemantischWithRulesMetadata validatorResultSemantisch(
			SemanticValidatorResult result) {
		ValidationReportValidationResultSemantischWithRulesMetadata resultSemantisch = (ValidationReportValidationResultSemantischWithRulesMetadata) new ValidationReportValidationResultSemantischWithRulesMetadata()
			.status(status(result));
		if (result != null) {
			resultSemantisch.valid(result.isValid()).rules(result.getRules().stream().map(ruleResult -> {
				List<ValidationReportValidationResultSemantischRuleFinding> errors = ruleResult
					.getInvalidFeaturesResults()
					.stream()
					.map(invalidFeaturesResult -> new ValidationReportValidationResultSemantischRuleFinding()
						.message(invalidFeaturesResult.getMessage())
						.level(level(invalidFeaturesResult))
						.gmlIds(invalidFeaturesResult.getGmlIds().stream().toList()))
					.toList();
				return new ValidationReportValidationResultSemantischRule().id(ruleResult.getName())
					.title(ruleResult.getMessage())
					.findings(errors);
			}).toList());
		}
		return resultSemantisch;
	}

	private ValidationReportValidationResultSemantisch validatorResultSemantischProfil(SemanticValidatorResult result) {
		ValidationReportValidationResultSemantisch resultSemantisch = new ValidationReportValidationResultSemantisch()
			.status(status(result));
		if (result != null) {
			resultSemantisch.valid(result.isValid()).rules(result.getRules().stream().map(ruleResult -> {
				List<ValidationReportValidationResultSemantischRuleFinding> errors = ruleResult
					.getInvalidFeaturesResults()
					.stream()
					.map(invalidFeaturesResult -> new ValidationReportValidationResultSemantischRuleFinding()
						.message(invalidFeaturesResult.getMessage())
						.level(level(invalidFeaturesResult))
						.gmlIds(invalidFeaturesResult.getGmlIds().stream().toList()))
					.toList();
				return new ValidationReportValidationResultSemantischRule().id(ruleResult.getName())
					.title(ruleResult.getMessage())
					.findings(errors);
			}).toList());
		}
		return resultSemantisch;
	}

	private ValidationRuleLevel level(InvalidFeaturesResult invalidFeaturesResult) {
		if (invalidFeaturesResult.getResultType() != null
				&& invalidFeaturesResult.getResultType() == ValidationResultType.WARNING)
			return WARNING;
		return ERROR;
	}

}
