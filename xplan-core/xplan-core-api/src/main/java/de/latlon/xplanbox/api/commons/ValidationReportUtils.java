/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons;

import java.util.List;

import de.latlon.xplanbox.api.commons.v1.model.ExternalReferenceResult;
import de.latlon.xplanbox.api.commons.v1.model.ExternalReferenceStatusEnum;
import de.latlon.xplanbox.api.commons.v1.model.RasterEvaluationResult;
import de.latlon.xplanbox.api.commons.v2.model.CrsStatusEnum;
import de.latlon.xplanbox.api.commons.v2.model.ImageFormatStatusEnum;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReport;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public final class ValidationReportUtils {

	private ValidationReportUtils() {
	}

	public static boolean isValid(ValidationReport validationReport) {
		if (validationReport.getSyntaktisch() != null && validationReport.getSyntaktisch().getValid() != null
				&& !validationReport.getSyntaktisch().getValid())
			return false;
		return validationReport.getPlans().stream().allMatch(plan -> plan.getValid() != null && plan.getValid());
	}

	public static List<String> retrieveMissingReferences(ValidationReport validationReport) {
		return validationReport.getPlans()
			.stream()
			.flatMap(plan -> plan.getExternalReferencesResult()
				.stream()
				.filter(externalReferenceResult -> externalReferenceResult
					.getStatus() == ExternalReferenceStatusEnum.MISSING)
				.map(ExternalReferenceResult::getName))
			.toList();
	}

	public static boolean hasMissingReferences(ValidationReport validationReport) {
		return !retrieveMissingReferences(validationReport).isEmpty();
	}

	public static List<String> retrieveInvalidRasterdata(ValidationReport validationReport) {
		return validationReport.getPlans()
			.stream()
			.flatMap(plan -> plan.getRasterEvaluationResult()
				.stream()
				.filter(rasterEvaluationResult -> rasterEvaluationResult.getCrsStatus() == CrsStatusEnum.UNSUPPORTED
						|| rasterEvaluationResult.getImageFormatStatus() == ImageFormatStatusEnum.UNSUPPORTED)
				.map(RasterEvaluationResult::getName))
			.toList();
	}

	public static boolean hasInvalidRasterdata(ValidationReport validationReport) {
		return !retrieveInvalidRasterdata(validationReport).isEmpty();
	}

}
