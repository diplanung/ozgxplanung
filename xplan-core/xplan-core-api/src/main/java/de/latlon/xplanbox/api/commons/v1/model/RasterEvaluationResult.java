/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons.v1.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.latlon.xplanbox.api.commons.v2.model.CrsStatusEnum;
import de.latlon.xplanbox.api.commons.v2.model.ImageFormatStatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RasterEvaluationResult {

	private @Valid String name;

	private @Valid @JsonInclude(JsonInclude.Include.NON_NULL) CrsStatusEnum crsStatus;

	private @Valid @JsonInclude(JsonInclude.Include.NON_NULL) ImageFormatStatusEnum imageFormatStatus;

	public void setName(String name) {
		this.name = name;
	}

	public RasterEvaluationResult name(String name) {
		this.name = name;
		return this;
	}

	@Schema(example = "stelling.png")
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public RasterEvaluationResult crsStatus(CrsStatusEnum crsStatus) {
		this.crsStatus = crsStatus;
		return this;
	}

	public void setCrsStatus(@Valid CrsStatusEnum crsStatus) {
		this.crsStatus = crsStatus;
	}

	@Schema(example = "SUPPORTED")
	@JsonProperty("crsStatus")
	public @Valid CrsStatusEnum getCrsStatus() {
		return crsStatus;
	}

	public RasterEvaluationResult imageFormatStatus(ImageFormatStatusEnum imageFormatStatus) {
		this.imageFormatStatus = imageFormatStatus;
		return this;
	}

	public void setImageFormatStatus(@Valid ImageFormatStatusEnum imageFormatStatus) {
		this.imageFormatStatus = imageFormatStatus;
	}

	@Schema(example = "SUPPORTED")
	@JsonProperty("imageFormatStatus")
	public @Valid ImageFormatStatusEnum getImageFormatStatus() {
		return imageFormatStatus;
	}

}
