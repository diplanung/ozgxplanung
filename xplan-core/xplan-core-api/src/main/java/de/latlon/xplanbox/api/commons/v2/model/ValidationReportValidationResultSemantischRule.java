/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons.v2.model;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class ValidationReportValidationResultSemantischRule {

	private @Valid String id;

	private @Valid String title;

	@JsonInclude(NON_EMPTY)
	private @Valid List<ValidationReportValidationResultSemantischRuleFinding> findings = new ArrayList<>();

	public ValidationReportValidationResultSemantischRule id(String id) {
		this.id = id;
		return this;
	}

	@Schema
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ValidationReportValidationResultSemantischRule title(@Valid String title) {
		this.title = title;
		return this;
	}

	public @Valid String getTitle() {
		return title;
	}

	public void setTitle(@Valid String title) {
		this.title = title;
	}

	public ValidationReportValidationResultSemantischRule findings(
			@Valid List<ValidationReportValidationResultSemantischRuleFinding> errors) {
		this.findings = errors;
		return this;
	}

	public @Valid List<ValidationReportValidationResultSemantischRuleFinding> getFindings() {
		return findings;
	}

	public void setFindings(@Valid List<ValidationReportValidationResultSemantischRuleFinding> findings) {
		this.findings = findings;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ValidationReportValidationResultSemantischRule that = (ValidationReportValidationResultSemantischRule) o;
		return Objects.equals(id, that.id) && Objects.equals(title, that.title)
				&& Objects.equals(findings, that.findings);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, title, findings);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ValidationReportValidationResultSemantischRule {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    title: ").append(toIndentedString(title)).append("\n");
		sb.append("    gmlIds: ").append(toIndentedString(findings)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces (except the
	 * first line).
	 */
	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
