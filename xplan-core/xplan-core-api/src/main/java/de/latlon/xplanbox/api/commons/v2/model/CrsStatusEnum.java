/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons.v2.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public enum CrsStatusEnum {

	// available in XPlanGML and same as configured crs
	SUPPORTED("SUPPORTED"),

	// available in XPlanGML but not the same as configured crs
	UNSUPPORTED("UNSUPPORTED"), MISSING("MISSING");

	private String value;

	CrsStatusEnum(String v) {
		value = v;
	}

	public static CrsStatusEnum fromResult(boolean configuredCrs, boolean crsSet) {
		if (crsSet && configuredCrs)
			return SUPPORTED;
		if (crsSet && !configuredCrs)
			return UNSUPPORTED;
		return MISSING;
	}

	public String value() {
		return value;
	}

	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static CrsStatusEnum fromValue(String value) {
		for (CrsStatusEnum b : CrsStatusEnum.values()) {
			if (b.value.equals(value)) {
				return b;
			}
		}
		throw new IllegalArgumentException("Unexpected value '" + value + "'");
	}

}
