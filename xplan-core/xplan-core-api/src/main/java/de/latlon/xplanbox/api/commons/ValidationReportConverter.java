/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons;

import static de.latlon.xplanbox.api.commons.v2.model.ValidationRuleLevel.ERROR;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationRuleLevel.WARNING;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.SKIPPED;
import static de.latlon.xplanbox.api.commons.v2.model.ValidationStatus.TERMINATED_WITH_ERRORS;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.latlon.xplan.validator.i18n.ValidationMessages;
import de.latlon.xplanbox.api.commons.v1.model.DocumentSummary;
import de.latlon.xplanbox.api.commons.v1.model.ExternalReferenceResult;
import de.latlon.xplanbox.api.commons.v1.model.PlanInfoBbox;
import de.latlon.xplanbox.api.commons.v1.model.RasterEvaluationResult;
import de.latlon.xplanbox.api.commons.v1.model.RulesMetadata;
import de.latlon.xplanbox.api.commons.v1.model.SemanticInvalidRuleResult;
import de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantischRules;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReport;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportPlan;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultGeometrisch;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultGeometrischRule;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultGeometrischRuleFinding;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultProfil;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultSemantisch;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultSemantischRule;
import de.latlon.xplanbox.api.commons.v2.model.ValidationReportValidationResultSyntaktisch;
import de.latlon.xplanbox.api.commons.v2.model.ValidationRuleLevel;
import de.latlon.xplanbox.api.commons.v2.model.ValidationStatus;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz</a>
 * @since 8.0
 */
public final class ValidationReportConverter {

	private ValidationReportConverter() {
	}

	public static de.latlon.xplanbox.api.commons.v1.model.ValidationReport convertToV1(
			ValidationReport validationReportV2) {
		de.latlon.xplanbox.api.commons.v1.model.ValidationReport validationReport = new de.latlon.xplanbox.api.commons.v1.model.ValidationReport();
		if (validationReportV2 != null) {
			List<ValidationReportPlan> plans = validationReportV2.getPlans();
			Date date = DateConverter.convertToDate(validationReportV2.getDate());
			validationReport.date(date)
				.name(validationReportV2.getValidationName())
				.documentSummary(plans.stream()
					.map(plan -> new DocumentSummary().name(plan.getName()).type(plan.getType()))
					.toList())
				.version(plans.stream().findFirst().map(ValidationReportPlan::getVersion).orElse(null))
				.valid(ValidationReportUtils.isValid(validationReportV2))
				.status(convertStatus(validationReportV2.getStatus()))
				.bbox(createBboxOfAllPlans(plans))
				.filename(validationReportV2.getFilename())
				.externalReferences(externalReferences(plans))
				.externalReferencesResult(externalReferencesResult(plans))
				.rasterEvaluationResult(rasterEvaluationResult(plans))
				.wmsUrl(validationReportV2.getWmsUrl())
				.rulesMetadata(plans.stream()
					.findFirst()
					.map(plan -> plan.getValidationResult().getSemantisch().getRulesMetadata())
					.orElse(null))
				.validationResult(convertResults(validationReportV2));
		}
		return validationReport;
	}

	private static de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResult convertResults(
			ValidationReport validationReportV2) {
		return new de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResult()
			.syntaktisch(convertSyntaktisch(validationReportV2.getSyntaktisch()))
			.semantisch(convertSemantisch(validationReportV2))
			.geometrisch(convertGeometrisch(validationReportV2))
			.profile(convertProfiles(validationReportV2.getPlans()));

	}

	private static de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSyntaktisch convertSyntaktisch(
			ValidationReportValidationResultSyntaktisch syntaktisch) {
		if (syntaktisch == null)
			return null;
		return new de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSyntaktisch()
			.valid(syntaktisch.getValid())
			.messages(syntaktisch.getErrors());
	}

	private static de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantisch convertSemantisch(
			ValidationReport validationReportV2) {
		if (TERMINATED_WITH_ERRORS.equals(validationReportV2.getStatus()))
			return new de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantisch()
				.valid(false);
		List<ValidationReportPlan> plans = validationReportV2.getPlans();
		if (plans.stream()
			.allMatch(plan -> plan.getValidationResult() != null && plan.getValidationResult().getSemantisch() != null
					&& SKIPPED.equals(plan.getValidationResult().getSemantisch().getStatus())))
			return null;
		return new de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantisch()
			.valid(plans.stream()
				.allMatch(plan -> plan.getValidationResult().getSemantisch() != null
						&& plan.getValidationResult().getSemantisch().getValid() != null
						&& plan.getValidationResult().getSemantisch().getValid()))
			.addRuleItems(plans.stream()
				.flatMap(plan -> plan.getValidationResult().getSemantisch().getRules().stream())
				.map(ValidationReportConverter::convertSemantischRule)
				.toList());
	}

	private static de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantisch convertSemantischProfil(
			ValidationReportValidationResultSemantisch semantisch) {
		if (semantisch == null || SKIPPED.equals(semantisch.getStatus()))
			return null;
		List<ValidationReportValidationResultSemantischRules> rules = new ArrayList<>(
				semantisch.getRules().stream().map(ValidationReportConverter::convertSemantischRule).toList());
		return new de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantisch()
			.valid(semantisch.getValid())
			.rules(rules);
	}

	private static de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultGeometrisch convertGeometrisch(
			ValidationReport validationReportV2) {
		if (TERMINATED_WITH_ERRORS.equals(validationReportV2.getStatus()))
			return new de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultGeometrisch()
				.valid(false);
		List<ValidationReportPlan> plans = validationReportV2.getPlans();
		if (plans.stream().allMatch(plan -> plan.getValidationResult().getGeometrisch() == null) || plans.stream()
			.allMatch(plan -> plan.getValidationResult() != null && plan.getValidationResult().getGeometrisch() != null
					&& SKIPPED.equals(plan.getValidationResult().getGeometrisch().getStatus())))
			return null;
		List<String> errors = new ArrayList<>();
		List<String> warnings = new ArrayList<>();
		for (ValidationReportPlan plan : plans) {
			addErrorsAndWarnings(plan.getValidationResult().getGeometrisch(), warnings, errors);
		}
		boolean isValid = plans.stream()
			.allMatch(plan -> plan.getValidationResult().getGeometrisch().getValid() != null
					&& plan.getValidationResult().getGeometrisch().getValid());
		return new de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultGeometrisch().valid(isValid)
			.errors(errors)
			.warnings(warnings);

	}

	private static void addErrorsAndWarnings(ValidationReportValidationResultGeometrisch geometrisch,
			List<String> warnings, List<String> errors) {
		if (geometrisch == null)
			return;
		for (ValidationReportValidationResultGeometrischRule rule : geometrisch.getRules()) {
			String id = rule.getId();
			for (ValidationReportValidationResultGeometrischRuleFinding finding : rule.getFindings()) {
				String msg = id + ": " + finding.getMessage();
				switch (finding.getLevel()) {
					case WARNING -> warnings.add(msg);
					case ERROR -> errors.add(msg);
				}
			}
		}
	}

	private static ValidationReportValidationResultSemantischRules convertSemantischRule(
			ValidationReportValidationResultSemantischRule rule) {
		List<SemanticInvalidRuleResult> erroredFeatures = collectRuleResults(rule, ERROR);
		List<SemanticInvalidRuleResult> warnedFeatures = collectRuleResults(rule, WARNING);
		return new ValidationReportValidationResultSemantischRules().name(rule.getId())
			.message(rule.getTitle())
			.isValid(erroredFeatures.isEmpty())
			.erroredFeatures(erroredFeatures)
			.warnedFeatures(warnedFeatures);
	}

	private static List<de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantischProfil> convertProfiles(
			List<ValidationReportPlan> plans) {
		List<de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantischProfil> profilesV1 = new ArrayList<>();
		for (ValidationReportPlan plan : plans) {
			List<ValidationReportValidationResultProfil> profilesV2 = plan.getValidationResult().getProfile();
			if (profilesV2 != null)
				profilesV1.addAll(profilesV2.stream().map(profile -> {
					RulesMetadata rulesMetadata = profile.getRulesMetadata();
					return new de.latlon.xplanbox.api.commons.v1.model.ValidationReportValidationResultSemantischProfil()
						.name(rulesMetadata != null ? rulesMetadata.getName() : null)
						.description(rulesMetadata != null ? rulesMetadata.getDescription() : null)
						.result(convertSemantischProfil(profile.getResult()));
				}).toList());
		}
		return profilesV1;
	}

	private static String convertStatus(ValidationStatus status) {
		switch (status) {
			case TERMINATED_REASON_UNKNOWN -> {
				return ValidationMessages.getMessage("status_unfinished");
			}
			case TERMINATED_WITH_ERRORS -> {
				return ValidationMessages.getMessage("status_skipped");
			}
		}
		return ValidationMessages.getMessage("status_finished");
	}

	private static List<SemanticInvalidRuleResult> collectRuleResults(
			ValidationReportValidationResultSemantischRule rule, ValidationRuleLevel level) {
		return rule.getFindings()
			.stream()
			.filter(finding -> level.equals(finding.getLevel()))
			.map(finding -> new SemanticInvalidRuleResult().message(finding.getMessage())
				.invalidGmlIds(finding.getGmlIds()))
			.toList();
	}

	private static PlanInfoBbox createBboxOfAllPlans(List<ValidationReportPlan> plans) {
		PlanInfoBbox bboxOfAll = null;
		for (ValidationReportPlan plan : plans) {
			PlanInfoBbox bbox = plan.getBbox();
			if (bbox != null) {
				if (bboxOfAll == null)
					bboxOfAll = new PlanInfoBbox().copy(bbox);
				else
					bboxOfAll.extend(bboxOfAll, bbox);
			}
		}
		return bboxOfAll;
	}

	private static List<String> externalReferences(List<ValidationReportPlan> plans) {
		return plans.stream()
			.flatMap(plan -> plan.getExternalReferencesResult().stream())
			.map(ExternalReferenceResult::getName)
			.toList();
	}

	private static List<ExternalReferenceResult> externalReferencesResult(List<ValidationReportPlan> plans) {
		return plans.stream().flatMap(plan -> plan.getExternalReferencesResult().stream()).toList();
	}

	private static List<RasterEvaluationResult> rasterEvaluationResult(List<ValidationReportPlan> plans) {
		return plans.stream().flatMap(plan -> plan.getRasterEvaluationResult().stream()).toList();
	}

}
