/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons;

import jakarta.ws.rs.core.MediaType;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class XPlanMediaType {

	public static final String APPLICATION_ZIP = "application/zip";

	public static final MediaType APPLICATION_ZIP_TYPE = new MediaType("application", "zip");

	public static final String APPLICATION_X_ZIP = "application/x-zip";

	public static final MediaType APPLICATION_X_ZIP_TYPE = new MediaType("application", "x-zip");

	public static final String APPLICATION_X_ZIP_COMPRESSED = "application/x-zip-compressed";

	public static final MediaType APPLICATION_X_ZIP_COMPRESSED_TYPE = new MediaType("application", "x-zip-compressed");

	public static final String APPLICATION_PDF = "application/pdf";

	public static final MediaType APPLICATION_PDF_TYPE = new MediaType("application", "pdf");

}
