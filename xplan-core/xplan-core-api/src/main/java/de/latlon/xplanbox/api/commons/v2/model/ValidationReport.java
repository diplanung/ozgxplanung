/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons.v2.model;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import de.latlon.xplan.validator.report.geojson.model.FeatureCollection;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidationReport {

	private @Valid String filename;

	private @Valid String validationName;

	private @Valid LocalDateTime date;

	@JsonInclude(NON_NULL)
	private @Valid ValidationStatus status;

	private @Valid @JsonInclude(JsonInclude.Include.NON_NULL) URI wmsUrl;

	private @Valid ValidationReportValidationResultSyntaktisch syntaktisch;

	private @Valid List<ValidationReportPlan> plans = new ArrayList<>();

	private @Valid FeatureCollection geomfindings;

	public ValidationReport filename(String filename) {
		this.filename = filename;
		return this;
	}

	@Schema(example = "xplan52-test.gml")
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public ValidationReport validationName(String validationName) {
		this.validationName = validationName;
		return this;
	}

	@Schema(example = "xplan52-test")
	public String getValidationName() {
		return validationName;
	}

	public void setValidationName(String validationName) {
		this.validationName = validationName;
	}

	public ValidationReport date(LocalDateTime date) {
		this.date = date;
		return this;
	}

	public @Valid LocalDateTime getDate() {
		return date;
	}

	public void setDate(@Valid LocalDateTime date) {
		this.date = date;
	}

	public ValidationReport status(ValidationStatus status) {
		this.status = status;
		return this;
	}

	public @Valid ValidationStatus getStatus() {
		return status;
	}

	public void setStatus(@Valid ValidationStatus status) {
		this.status = status;
	}

	/**
	 * @deprecated XPlanValidatorWMS is deprecated and will be removed in a future version
	 */
	@Deprecated
	public ValidationReport wmsUrl(URI wmsUrl) {
		this.wmsUrl = wmsUrl;
		return this;
	}

	/**
	 * @deprecated XPlanValidatorWMS is deprecated and will be removed in a future version
	 */
	@Deprecated
	@Schema(example = "https://xplanbox.lat-lon.de/xplan-validator-wms/services/wms?PLANWERK_MANAGERID=13",
			description = "deprecated: XPlanValidatorWMS will be removed in a future version", deprecated = true)
	public URI getWmsUrl() {
		return wmsUrl;
	}

	/**
	 * @deprecated XPlanValidatorWMS is deprecated and will be removed in a future version
	 */
	@Deprecated
	public void setWmsUrl(URI wmsUrl) {
		this.wmsUrl = wmsUrl;
	}

	public ValidationReport syntaktisch(ValidationReportValidationResultSyntaktisch syntaktisch) {
		this.syntaktisch = syntaktisch;
		return this;
	}

	public @Valid ValidationReportValidationResultSyntaktisch getSyntaktisch() {
		return syntaktisch;
	}

	public void setSyntaktisch(@Valid ValidationReportValidationResultSyntaktisch syntaktisch) {
		this.syntaktisch = syntaktisch;
	}

	public ValidationReport plans(List<ValidationReportPlan> plans) {
		this.plans = plans;
		return this;
	}

	public ValidationReport addPlanItem(ValidationReportPlan plan) {
		this.plans.add(plan);
		return this;
	}

	public @Valid List<ValidationReportPlan> getPlans() {
		return plans;
	}

	public void setPlans(@Valid List<ValidationReportPlan> plans) {
		this.plans = plans;
	}

	public ValidationReport geomfindings(FeatureCollection geomfindings) {
		this.geomfindings = geomfindings;
		return this;
	}

	public @Valid FeatureCollection getGeomfindings() {
		return geomfindings;
	}

	public void setGeomfindings(@Valid FeatureCollection geomfindings) {
		this.geomfindings = geomfindings;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ValidationReport {\n");
		sb.append("    filename: ").append(toIndentedString(filename)).append("\n");
		sb.append("    name: ").append(toIndentedString(validationName)).append("\n");
		sb.append("    date: ").append(toIndentedString(date)).append("\n");
		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("    wmsUrl: ").append(toIndentedString(wmsUrl)).append("\n");
		sb.append("    syntaktisch: ").append(toIndentedString(syntaktisch)).append("\n");
		sb.append("    plans: ").append(toIndentedString(plans)).append("\n");
		sb.append("    geomfindings: ").append(toIndentedString(geomfindings)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces (except the
	 * first line).
	 */
	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
