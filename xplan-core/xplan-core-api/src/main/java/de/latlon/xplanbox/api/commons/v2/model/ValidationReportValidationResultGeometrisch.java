/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons.v2.model;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidationReportValidationResultGeometrisch {

	private @Valid Boolean valid;

	@JsonInclude(NON_NULL)
	private @Valid ValidationStatus status;

	private @Valid List<ValidationReportValidationResultGeometrischRule> rules = new ArrayList<>();

	public ValidationReportValidationResultGeometrisch valid(Boolean valid) {
		this.valid = valid;
		return this;
	}

	@Schema(example = "false")
	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	public ValidationReportValidationResultGeometrisch status(ValidationStatus status) {
		this.status = status;
		return this;
	}

	@Schema(example = "COMPLETED",
			description = "The status of the validation. If missing the validation is not started yet.")
	public ValidationStatus getStatus() {
		return status;
	}

	public void setStatus(ValidationStatus status) {
		this.status = status;
	}

	public ValidationReportValidationResultGeometrisch rules(
			List<ValidationReportValidationResultGeometrischRule> rules) {
		this.rules = rules;
		return this;
	}

	@Schema
	public List<ValidationReportValidationResultGeometrischRule> getRules() {
		return rules;
	}

	public void setRules(List<ValidationReportValidationResultGeometrischRule> rules) {
		this.rules = rules;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ValidationReportValidationResultGeometrisch that = (ValidationReportValidationResultGeometrisch) o;
		return Objects.equals(valid, that.valid) && status == that.status && Objects.equals(rules, that.rules);
	}

	@Override
	public int hashCode() {
		return Objects.hash(valid, status, rules);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ValidationReportValidationResultGeometrisch {\n");

		sb.append("    valid: ").append(toIndentedString(valid)).append("\n");
		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("    rules: ").append(toIndentedString(rules)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces (except the
	 * first line).
	 */
	private String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
