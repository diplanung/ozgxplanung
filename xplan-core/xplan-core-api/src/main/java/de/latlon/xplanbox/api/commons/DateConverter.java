/*-
 * #%L
 * xplan-core-api - Modul zur Gruppierung der Kernmodule
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public final class DateConverter {

	private DateConverter() {
	}

	public static LocalDate convertToLocalDate(Date dateToConvert) {
		if (dateToConvert == null)
			return null;
		return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public static LocalDateTime convertToLocalDateTime(Date dateToConvert) {
		if (dateToConvert == null)
			return null;
		return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	public static ZonedDateTime convertToZonedDateTime(Date dateToConvert) {
		if (dateToConvert == null)
			return null;
		return dateToConvert.toInstant().atZone(ZoneId.systemDefault());
	}

	public static Date convertToDate(LocalDate dateToConvert) {
		if (dateToConvert == null)
			return null;
		return Date.from(dateToConvert.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	public static Date convertToDate(LocalDateTime dateToConvert) {
		if (dateToConvert == null)
			return null;
		return Date.from(dateToConvert.atZone(ZoneId.systemDefault()).toInstant());
	}

	public static Date convertToDate(ZonedDateTime dateToConvert) {
		if (dateToConvert == null)
			return null;
		return Date.from(dateToConvert.toInstant());
	}

}
