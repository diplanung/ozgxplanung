/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons.v1.model;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import java.net.URI;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * Datatype for Link. A AbstractLink to a resource related to the resource such as
 * XPlanWerkWMS or the resource itself.
 *
 * @since 4.0
 */
@Schema(description = "Link to a resource related to the resource such as XPlanWerkWMS or the resource itself")
@jakarta.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen",
		date = "2020-08-28T13:42:47.160+02:00[Europe/Berlin]")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AbstractLink {

	private @Valid URI href;

	@JsonInclude(NON_NULL)
	private @Valid String type;

	@JsonInclude(NON_NULL)
	private @Valid String hreflang;

	@JsonInclude(NON_NULL)
	private @Valid String title;

	@JsonInclude(NON_NULL)
	private @Valid Integer length;

	public AbstractLink href(URI href) {
		this.href = href;
		return this;
	}

	@Schema(example = "https://xplanbox.lat-lon.de/xmanager/api/v1/plan/123", required = true)
	@JsonProperty("href")
	@NotNull
	public URI getHref() {
		return href;
	}

	public void setHref(URI href) {
		this.href = href;
	}

	public AbstractLink type(String type) {
		this.type = type;
		return this;
	}

	@Schema(example = "application/json")
	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public AbstractLink hreflang(String hreflang) {
		this.hreflang = hreflang;
		return this;
	}

	@Schema(example = "en")
	@JsonProperty("hreflang")
	public String getHreflang() {
		return hreflang;
	}

	public void setHreflang(String hreflang) {
		this.hreflang = hreflang;
	}

	public AbstractLink title(String title) {
		this.title = title;
		return this;
	}

	@Schema(example = "Othmarschen 3, Hamburg")
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public AbstractLink length(Integer length) {
		this.length = length;
		return this;
	}

	@Schema
	@JsonProperty("length")
	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		AbstractLink link = (AbstractLink) o;
		return Objects.equals(this.href, link.href) && Objects.equals(this.type, link.type)
				&& Objects.equals(this.hreflang, link.hreflang) && Objects.equals(this.title, link.title)
				&& Objects.equals(this.length, link.length);
	}

	@Override
	public int hashCode() {
		return Objects.hash(href, type, hreflang, title, length);
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces (except the
	 * first line).
	 */
	protected String toIndentedString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
