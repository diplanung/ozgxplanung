/*-
 * #%L
 * xplan-manager-api - Software zur Verwaltung von XPlanGML Daten
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.api.commons.v2.model;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import java.net.URI;
import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import de.latlon.xplanbox.api.commons.v1.model.AbstractLink;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * Datatype for Link. A Link to a resource related to the resource such as XPlanWerkWMS or
 * the resource itself.
 *
 * @since 8.0
 */
@Schema(description = "Link to a resource related to the resource such as "
		+ "ValidationReport (as described ind #/components/schemas/ValidationReport) or "
		+ "PlanInfo (as described ind #/components/schemas/PlanInfo)")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseLink extends AbstractLink {

	public enum RelEnum {

		STATUS("status"), REPORT("report"), PLANINFO("planinfo");

		private final String value;

		RelEnum(String v) {
			value = v;
		}

		public String value() {
			return value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(value);
		}

		@JsonCreator
		public static RelEnum fromValue(String value) {
			for (RelEnum b : RelEnum.values()) {
				if (b.value.equals(value)) {
					return b;
				}
			}
			throw new IllegalArgumentException("Unexpected value '" + value + "'");
		}

	}

	private @Valid RelEnum rel;

	@JsonInclude(NON_NULL)
	private @Valid URI schema;

	@JsonInclude(NON_NULL)
	private Date expirationTime;

	public ResponseLink rel(RelEnum rel) {
		this.rel = rel;
		return this;
	}

	@Schema(example = "self")
	@JsonProperty("rel")
	public RelEnum getRel() {
		return rel;
	}

	public void setRel(RelEnum rel) {
		this.rel = rel;
	}

	public ResponseLink schema(URI schema) {
		this.schema = schema;
		return this;
	}

	@Schema(example = "https://xplanbox.lat-lon.de/xmanager/api/v1#/components/schemas/ValidationReport")
	@JsonProperty("schema")
	@NotNull
	public URI getSchema() {
		return schema;
	}

	public void setSchema(URI schema) {
		this.schema = schema;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	public ResponseLink expirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;
		ResponseLink link = (ResponseLink) o;
		return rel == link.rel;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), rel);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Link {\n");
		sb.append("    href: ").append(toIndentedString(getHref())).append("\n");
		sb.append("    rel: ").append(toIndentedString(rel)).append("\n");
		sb.append("    type: ").append(toIndentedString(getType())).append("\n");
		sb.append("    hreflang: ").append(toIndentedString(getHreflang())).append("\n");
		sb.append("    title: ").append(toIndentedString(getTitle())).append("\n");
		sb.append("    length: ").append(toIndentedString(getLength())).append("\n");
		sb.append("}");
		return sb.toString();
	}

}
