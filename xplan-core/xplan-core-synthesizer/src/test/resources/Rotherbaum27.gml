<?xml version='1.0' encoding='UTF-8'?>
<!--Zuletzt aktualisiert am: 10 Feb 2021 08:20:26 MEZ-->
<xplan:XPlanAuszug xmlns:xplan="http://www.xplanung.de/xplangml/5/2" xmlns:gml="http://www.opengis.net/gml/3.2" gml:id="Gml_D8AD7D58-D20D-4E21-A1B0-D88D7DBF453F">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>565463.986 5936206.747</gml:lowerCorner>
      <gml:upperCorner>565898.316 5936449.368</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_09F08185-5C1D-4475-95E1-80EDA5DEB7A2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565463.986 5936206.747</gml:lowerCorner>
          <gml:upperCorner>565898.316 5936449.368</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>Rotherbaum27</xplan:name>
      <xplan:beschreibung>Der Bebauungsplan Rotherbaum 27 für den Geltungsbereich Rothenbaumchaussee - Hallerstraße - Mittelweg - Bei St. Johannis - Heimhuder Straße - Turmweg (Bezirk Eimsbüttel, Ortsteil 312) wird festgestellt.</xplan:beschreibung>
      <xplan:technHerstellDatum>2014-09-03</xplan:technHerstellDatum>
      <xplan:raeumlicherGeltungsbereich>
        <!--Inlined geometry 'Gml_9ED35190-858E-40D6-8268-B15C6F5F805E'-->
        <gml:Polygon gml:id="Gml_9ED35190-858E-40D6-8268-B15C6F5F805E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565762.142 5936207.082 565898.316 5936207.367 565896.692 5936220.468 565893.615 5936239.881 565890.858 5936253.274 565887.312 5936266.667 565880.088 5936288.591 565858.948 5936346.278 565822.120 5936448.807 565820.771 5936449.368 565808.033 5936448.664 565599.581 5936436.686 565479.664 5936429.901 565467.314 5936429.202 565465.920 5936429.096 565465.788 5936422.385 565464.015 5936332.140 565463.986 5936250.911 565477.014 5936250.643 565597.587 5936248.162 565739.899 5936245.618 565751.267 5936245.375 565750.877 5936206.747 565762.142 5936207.082</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_E7B18656-8F6E-4BF7-8877-3C27A10A18F4"/>
      <xplan:texte xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_701E42E5-23F0-4B75-B11E-FF872CA73CB7"/>
      <xplan:texte xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_735F3E62-AE80-4B08-A62B-2D18DFCC351D"/>
      <xplan:texte xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_058F9594-D6F6-43C1-989F-F9AB9EFBB385"/>
      <xplan:texte xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_9F307243-4605-44E6-BD6F-89F2E6B469ED"/>
      <xplan:texte xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_5A29A71C-33B3-4D37-A6B3-798FDF226C53"/>
      <xplan:texte xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_B02177EB-D1F9-4F79-941B-9CA1F221C157"/>
      <xplan:texte xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_14529F22-C338-4D03-BD02-9CE91097DE3C"/>
      <xplan:texte xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_1A2A44AE-B6FD-41D9-892E-BE39AA705246"/>
      <xplan:texte xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_971BE4C4-E04B-4701-BEEF-9D7918C90196"/>
      <xplan:texte xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_953F73E1-9945-40EA-84AD-C61874F26A4C"/>
      <xplan:texte xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_50260B25-A589-4CD0-B152-9E034360F154"/>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>312</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2006-03-06</xplan:rechtsverordnungsDatum>
      <xplan:versionBauNVODatum>1990-01-01</xplan:versionBauNVODatum>
      <xplan:bereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565463.986 5936206.747</gml:lowerCorner>
          <gml:upperCorner>565898.316 5936449.368</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <!--Inlined geometry 'Gml_08AEDC7E-66C7-4EAE-B76B-BF77875CD49E'-->
        <gml:Polygon gml:id="Gml_08AEDC7E-66C7-4EAE-B76B-BF77875CD49E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565762.142 5936207.082 565898.316 5936207.367 565896.692 5936220.468 565893.615 5936239.881 565890.858 5936253.274 565887.312 5936266.667 565880.088 5936288.591 565858.948 5936346.278 565822.120 5936448.807 565820.771 5936449.368 565808.033 5936448.664 565599.581 5936436.686 565479.664 5936429.901 565467.314 5936429.202 565465.920 5936429.096 565465.788 5936422.385 565464.015 5936332.140 565463.986 5936250.911 565477.014 5936250.643 565597.587 5936248.162 565739.899 5936245.618 565751.267 5936245.375 565750.877 5936206.747 565762.142 5936207.082</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:rasterBasis xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#XPLAN_rasterBasis_2bfcdf18-be5e-4792-8ff1-f70b746a99a6"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_06621478-4969-4727-8F5F-1CB41C4AA868"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_5BBB6366-ABCC-443B-B06E-E13F5169AEBD"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A7A871FF-23B7-44EA-BA8D-C4A2BDA8FB41"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_CDBD0068-C1B5-4DE9-9EB3-1699EDCD4CC9"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_F338CCF1-CDA2-4F51-9A0F-F7882FA0E7B5"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_3441B3DE-04CA-4580-BFF3-6177FCB7C91C"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_D5DF1DA9-0C99-4D16-88B3-FB9109948C15"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_96EED429-9C51-42E2-AAD2-D4168755DF04"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A155962F-8CA9-41E5-9981-517C172E2D32"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_3CBFDC90-E513-4E03-B080-CA2063CED127"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_E34F3B9D-9BCB-4D70-9640-0F30D591C0AD"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_BEA37C0D-23EB-4EC6-8647-E9F925694749"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_5839EE24-FA42-4F79-ADD7-2A83E4302917"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_59E5D563-8773-4C5B-B5F5-D8C9377CF0CD"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_6B237DE5-6562-4C01-8B8D-FC57F8631F09"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_BA552C1A-9F28-4E9D-8676-285D57862702"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A009FB88-28FF-46FC-8EBE-0FF7BF4F1656"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_CC9BECD1-4E1B-4C2F-86D3-B0BDD5058CC4"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_23FA98F2-638A-437D-9179-24E5426988B0"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_C0449179-CB75-4AD6-8F37-2F9D759448F1"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_10D913B7-0B5F-4D6F-9064-14EADFDD5575"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_6AC26AFC-A93F-4D7A-A000-0A217A56A853"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_F86357A0-B4B1-4681-9B21-B83067B887D1"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_4E00A730-B542-449E-9D8A-E00C08A159D5"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_C2FFA4CF-EA9C-4C6E-97CC-27D8477D8631"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_414773CC-21EE-41C1-846B-909ED7006FE9"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_9ED07310-796D-4BDC-B3E4-9DB90BF19B8E"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_E3F8D510-128F-4E04-8A4D-797CE9512D40"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_BED06115-A953-4934-90A5-13591CF61BC9"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_60F5F4AC-5DF3-47FF-816A-83AC93A45E31"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_1D95AC59-79F5-4EF5-BC65-ACBE31F3AFF7"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_547B218F-E7E7-409E-833A-0BBB121413B7"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_0D5A6A40-750C-461A-9B90-BBCB5B69BB0F"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_059996D9-526B-4C71-9D54-F93FA23EE994"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_ADCAA559-FB8F-4E7F-9B81-453F49E314FD"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_CE6A3F46-FBB7-4943-8218-5938C3002F2B"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_F1A8E06B-079F-40EE-8686-7A39386FCBFF"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_021091DC-B56B-4819-93FF-E77B7B410A1C"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_56A5E24C-47FC-460A-967A-A286865C9FB2"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A0094ADA-2CAD-46D7-BBE0-9B11361DE8B0"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_0D3A70B7-04A7-4014-93A4-C1202B2138E4"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A39EBEB2-0ECB-4CEC-814D-75668D93C4BC"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_E79D3549-2E78-4930-AD42-27FCA1FBC3C1"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_9F39F0CC-6E16-4CFF-B25F-A828F231224A"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_76AC43C8-39AB-47A9-8AD7-49069D6514F9"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_4C487667-82F3-41E5-9D25-382DC0BFD729"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_82F1B925-7994-4273-ABA6-18FA85BB3CA9"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_76951865-48FA-4B11-836F-303B0DAC57E1"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_7BD970BB-64C1-4E15-AF23-EFECFE40D852"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_AD0AF193-5FDB-4598-A1D2-00B7C4922208"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_CF042D07-472B-49BC-A120-212ED26ADF73"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_36729A2E-E5E8-4B7D-9F46-4049D57032CB"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_5981E4D7-EFE7-4080-80E9-978E29EC0246"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_E5D41C1E-512A-444C-8B7E-A7E00AE7B917"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_13C5028C-16AC-4A35-B710-AFCFA20308F3"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_52F62D9D-A2E7-4417-9B16-8B267BF7A1B1"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_D9546777-8D4A-4620-83DA-2300781C527C"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A22C0643-3518-4891-844E-84A5E50DDEE4"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_CEAEB980-6ADE-4C40-9703-E552C2EF4C2B"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_05CA4B4E-A5BA-48C4-A7AD-DB7A54583D63"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A21447B3-E656-4088-B34F-BB78F95D316A"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_942EC9DD-CD6E-4BAE-9073-2067F8E24AD7"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_FC95B2D3-27F0-41DE-8346-3BAAF90BE0D7"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_957544E5-F206-4B02-B439-7F05901FEE28"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_FE685129-3891-4BB3-90CC-C79E636C48AA"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_4676DAF7-227C-4E16-A65B-05078E93F9C9"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_972C8B8F-C212-4BFA-B5B0-6A586D4429F4"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_CE0D5A54-484D-4BE4-867E-9300203D675C"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_7AFBD5D8-B895-4C8E-AE17-21F064D4737E"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_F283274E-6473-4739-A41F-4BA04BF51657"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_42E7A7A1-715C-4D73-BF6E-733CC2058EC8"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_5DC25FB5-D4A8-4B5F-B6FE-4DB0462E6EB0"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_68485B95-D68E-4CFF-8CEC-2180B743D42A"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_43AC2A8C-8F3D-4AD5-8C35-2F63BB145F3C"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_B9577D78-E86F-476D-9858-928139D8D626"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_FC440BE7-E152-4858-A895-1D0041405362"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_68A0E440-DEDF-4FD3-AEE5-8E16F371B2AB"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_766C86AE-AE24-47E1-8063-6E4D3C9561FD"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_047F1DC7-895B-4E82-BD80-B7AA4F8ECECC"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_D6A53EE3-0F0B-4BC4-8D6B-C7A0F5FA8FFD"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_48791A9B-3B7E-4C42-869F-ABB5B32F1C9D"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_CD14AF17-99A6-4C20-AB1E-FAECDDC195BF"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_C8AE3BB0-8A87-4DB0-BFBC-4055BF8C3A30"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A8432A4A-B586-4CBA-A935-2CC7C99516D1"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_C6057670-3B55-41E0-A0F9-D5677AE69AB3"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_C111F100-CAB6-449C-B992-F77DC2940AF0"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_CE70B1D2-0DB5-4429-BFD5-E0566C6F57AA"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_1E975086-9368-4DB4-845A-DCA1E2A5A934"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_94B40304-A770-413A-8034-4F477794FDC8"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_545B1BE8-A51F-4B3D-B4FD-809B60749251"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_9642D7F8-A40D-41A8-818B-8CF945AECF2D"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_8BE107D5-7116-411C-80B5-7227966E82A5"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_13AF40B1-5C32-4ECF-AB76-7905A37254AF"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_8794D064-E02A-4464-BA1E-A14E86725894"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_A15383F5-C92C-4BE4-8575-895FB8CAF141"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_B4DF08CC-E68D-46B7-A7CC-D074DD65C11C"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_CDF14878-00DD-4416-8C3E-56C7676DB3D0"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_242CCE94-3A6A-4F8B-8CBD-BDDDE20F5504"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_DC5F3F0C-C3D3-4D2B-9800-6C8A5CD99215"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_A14D1BC1-8020-48A5-8A8A-88270373030E"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_52851937-0BFB-4AE4-B2FE-C6792151010F"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_CB355D2E-90FA-4D68-9E5F-8B230EC36F62"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_08DA5927-E2A7-4E90-AC0B-EE23AAB58F07"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_15E6CF30-CE1E-444A-AA45-FA453505C51D"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_425F1CEB-6195-4AD5-B728-8F0D2F818192"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_B886623C-C4F9-47EA-A399-B5971A66D252"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_8A7CD17B-A230-40C3-B510-A0F8E9AF6847"/>
      <xplan:planinhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_65A9FACE-E0AC-409B-BE0C-069EEA10D43F"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_7552CC0B-43C4-47EC-B360-5F02C94B8FBB"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_2E2BDAEA-42B9-4B34-8C51-866DAEE17DD2"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_177ECE7D-22D3-485F-B08B-127234C3E5A8"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_B75A44BA-B8F8-4293-8E1C-32B354C9E4E7"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_1231631B-C3EB-4F28-8C09-E326E8BAB117"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_E6F1A34D-951C-4BE5-BEC8-18DD427E39E0"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_39872463-90B8-4D85-B736-A3A385271400"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_23DAC4BF-EFE6-485C-A136-D94B277C738C"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_BF17D42E-6269-4DF5-A623-4BBC01135F4D"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_2B8C6DDA-260B-42B4-B71F-683475FC566B"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_D990C42E-0151-45E1-9022-A8DEF677D2E9"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_FD6AC549-9651-48E4-AC89-0541EF6E139D"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_26938415-CDFF-4218-835E-35E6C32177B7"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_D991F9AB-A34B-4219-AE9D-C32552157BE7"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_2689AEDE-EA9D-4AB4-840F-604C48A3B4BC"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_5764FE87-F634-4686-89DA-0647DA2AB77D"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_83AED188-9B55-4040-8CCA-EFA4E2755E4B"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_F25329F2-FB16-4BFD-9512-D8D3E2BF1B70"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_703BDE29-FC56-43DC-9C14-014BF0191C6D"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_092CFCE3-7C70-4B54-8299-77B9532213A9"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_CBC3738C-8E02-425F-8651-DDC5A299B92C"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_9997689C-974F-4265-A1C5-109A35E05FFC"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_39E286A1-2688-43EB-8F0F-0830B187886A"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_207C00E5-89B0-4142-8ED5-0552FC538868"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_14BBC686-492C-48E7-8983-D59242799634"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_C141A31E-CF96-4265-995A-C3B2426FAAB2"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_50D8E4FE-5C02-48E9-BE26-28D67EF85A16"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_2733E85C-BC72-4566-B33F-A41704275314"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_E817A841-B0C3-4FC9-A19D-CD278301B6F8"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_958A4FD5-B621-4581-9B0B-27B98F429EE3"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_B41D95DC-8D48-4817-94E1-AD5D0771B5D9"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_57B1E7FA-DE70-4EA0-B8B0-DF028A809401"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_68D8DAF0-3B84-4541-9DB0-BCA166ECBB9A"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_B942DC31-CD79-43D3-8267-612BC60B5C61"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_EBE5A866-9513-47C7-A085-3A16ECB80C82"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_34997323-1121-4389-8032-C1E49B6B513E"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_3E269B23-98E8-4E0B-9ED1-B33DD0C29269"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_51684B85-A692-4544-BA4D-94AA8E321AE0"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_AA059669-A1E0-483A-8AB3-CEC84B562D69"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_2E4AA92F-D437-4B4D-B5CC-BFA4EC12089B"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_E03FE3AF-56C9-42C6-93B3-08B3B1B3C9C0"/>
      <xplan:praesentationsobjekt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_5FD36ACC-5957-438D-A71D-6CA3CC11D60B"/>
      <xplan:versionBauNVODatum>1990-01-01</xplan:versionBauNVODatum>
      <xplan:gehoertZuPlan xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_09F08185-5C1D-4475-95E1-80EDA5DEB7A2"/>
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_06621478-4969-4727-8F5F-1CB41C4AA868">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565840.900 5936216.181</gml:lowerCorner>
          <gml:upperCorner>565884.054 5936303.631</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_7552CC0B-43C4-47EC-B360-5F02C94B8FBB"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_2B8C6DDA-260B-42B4-B71F-683475FC566B"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_C81E5D10-F194-45FF-AF96-3676C1D3F48C'-->
        <gml:Polygon gml:id="Gml_C81E5D10-F194-45FF-AF96-3676C1D3F48C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_84D345CE-8175-425B-81A7-E845823EB04C" srsName="EPSG:25832">
                  <gml:posList>565874.620 5936265.427 565861.203 5936303.622</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FAF1E2BF-897F-4CD4-A03D-F49F81E6A8EC" srsName="EPSG:25832">
                  <gml:posList>565861.203 5936303.622 565847.615 5936303.631</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4C89994F-143B-4447-94DE-81EBE84D5320" srsName="EPSG:25832">
                  <gml:posList>565847.615 5936303.631 565847.485 5936299.553</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_51E3F3F6-9C0B-40B6-A391-BEDA598E9FF6" srsName="EPSG:25832">
                  <gml:posList>565847.485 5936299.553 565846.587 5936271.227</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_31752720-5EE8-4090-8436-81E1C9C8D931" srsName="EPSG:25832">
                  <gml:posList>565846.587 5936271.227 565846.382 5936254.426</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_84EE5052-3B65-463A-8192-6568E302E8D0" srsName="EPSG:25832">
                  <gml:posList>565846.382 5936254.426 565841.229 5936254.526</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_42183016-1B09-4FF8-A7AA-5764F201FFF8" srsName="EPSG:25832">
                  <gml:posList>565841.229 5936254.526 565840.901 5936230.026</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_CF203569-C007-4283-98CE-C81DC1659BFB" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc>
                      <gml:posList>565840.901 5936230.026 565844.642 5936220.605 565853.936 5936216.556</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DC037A0B-E923-4C01-8D18-A8CB0BF1CF0B" srsName="EPSG:25832">
                  <gml:posList>565853.936 5936216.556 565873.238 5936216.183</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_01261904-BE73-4195-AC8A-A76E18B3E2F5" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc>
                      <gml:posList>565873.238 5936216.183 565877.913 5936217.036 565881.890 5936219.636</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_212D3EF2-80B2-4B34-9ECD-E1F937352E9B" srsName="EPSG:25832">
                  <gml:posList>565881.890 5936219.636 565883.214 5936221.686</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_83A1521B-9601-41DC-8E16-D12A708FCB19" srsName="EPSG:25832">
                  <gml:posList>565883.214 5936221.686 565883.818 5936223.337</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B21046D1-DDA4-4269-8DAF-5D513DA304B3" srsName="EPSG:25832">
                  <gml:posList>565883.818 5936223.337 565884.054 5936226.489</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_74E29702-2B73-4469-8915-8AEA74F355FD" srsName="EPSG:25832">
                  <gml:posList>565884.054 5936226.489 565883.866 5936229.971</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ADEF3282-930B-4255-9752-0EA1FE69B806" srsName="EPSG:25832">
                  <gml:posList>565883.866 5936229.971 565874.620 5936265.427</gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1600</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:zugunstenVon>Freie und Hansestadt Hamburg</xplan:zugunstenVon>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7552CC0B-43C4-47EC-B360-5F02C94B8FBB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565857.527 5936247.687</gml:lowerCorner>
          <gml:upperCorner>565857.527 5936247.687</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_06621478-4969-4727-8F5F-1CB41C4AA868"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_97FC1DA8-C8FA-4160-AC22-B28B473A9731'-->
        <gml:Point gml:id="Gml_97FC1DA8-C8FA-4160-AC22-B28B473A9731" srsName="EPSG:25832">
          <gml:pos>565857.527 5936247.687</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_2B8C6DDA-260B-42B4-B71F-683475FC566B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565861.145 5936241.740</gml:lowerCorner>
          <gml:upperCorner>565861.145 5936241.740</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zugunstenVon</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_06621478-4969-4727-8F5F-1CB41C4AA868"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_4F850409-7CF4-4F3B-8766-CED4E7A3EBA8'-->
        <gml:Point gml:id="Gml_4F850409-7CF4-4F3B-8766-CED4E7A3EBA8" srsName="EPSG:25832">
          <gml:pos>565861.145 5936241.740</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_5BBB6366-ABCC-443B-B06E-E13F5169AEBD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565739.277 5936403.452</gml:lowerCorner>
          <gml:upperCorner>565824.603 5936430.892</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_2E2BDAEA-42B9-4B34-8C51-866DAEE17DD2"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_FD6AC549-9651-48E4-AC89-0541EF6E139D"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_FC416765-64A3-4C18-ABDB-AD68E3EC5EA7'-->
        <gml:Polygon gml:id="Gml_FC416765-64A3-4C18-ABDB-AD68E3EC5EA7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565821.089 5936413.043 565778.149 5936414.176 565749.244 5936429.667 565746.957 5936430.892 565743.464 5936430.681 565739.463 5936430.438 565739.277 5936430.426 565739.283 5936422.224 565739.295 5936406.305 565739.295 5936405.703 565746.224 5936405.521 565748.776 5936405.454 565769.908 5936404.896 565821.505 5936403.535 565824.603 5936403.452 565821.089 5936413.043</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:zugunstenVon>Freie und Hansestadt Hamburg</xplan:zugunstenVon>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2E2BDAEA-42B9-4B34-8C51-866DAEE17DD2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565787.185 5936410.669</gml:lowerCorner>
          <gml:upperCorner>565787.185 5936410.669</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_5BBB6366-ABCC-443B-B06E-E13F5169AEBD"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_887EAB6C-579E-4EB4-94F4-1A3E46FEB5A4'-->
        <gml:Point gml:id="Gml_887EAB6C-579E-4EB4-94F4-1A3E46FEB5A4" srsName="EPSG:25832">
          <gml:pos>565787.185 5936410.669</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_FD6AC549-9651-48E4-AC89-0541EF6E139D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565788.366 5936407.116</gml:lowerCorner>
          <gml:upperCorner>565788.366 5936407.116</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zugunstenVon</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_5BBB6366-ABCC-443B-B06E-E13F5169AEBD"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_F3A5B9F7-05C1-4BE6-84EB-A870C627B10F'-->
        <gml:Point gml:id="Gml_F3A5B9F7-05C1-4BE6-84EB-A870C627B10F" srsName="EPSG:25832">
          <gml:pos>565788.366 5936407.116</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_A7A871FF-23B7-44EA-BA8D-C4A2BDA8FB41">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565539.658 5936256.517</gml:lowerCorner>
          <gml:upperCorner>565675.219 5936289.374</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_14BBC686-492C-48E7-8983-D59242799634"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_C141A31E-CF96-4265-995A-C3B2426FAAB2"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_50D8E4FE-5C02-48E9-BE26-28D67EF85A16"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_2733E85C-BC72-4566-B33F-A41704275314"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_971BE4C4-E04B-4701-BEEF-9D7918C90196"/>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_735F3E62-AE80-4B08-A62B-2D18DFCC351D"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_E3588624-7471-4F30-825E-8B14261A5ECB'-->
        <gml:Polygon gml:id="Gml_E3588624-7471-4F30-825E-8B14261A5ECB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565674.745 5936256.561 565674.724 5936256.562 565675.219 5936282.140 565640.064 5936282.938 565640.218 5936287.438 565603.840 5936288.142 565584.822 5936288.510 565569.271 5936288.810 565565.473 5936288.884 565549.924 5936289.186 565540.244 5936289.374 565539.658 5936259.136 565564.887 5936258.648 565568.684 5936258.574 565654.126 5936256.916 565674.723 5936256.517 565674.744 5936256.517 565674.745 5936256.561</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:Zzwingend>5</xplan:Zzwingend>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_14BBC686-492C-48E7-8983-D59242799634">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565573.369 5936286.016</gml:lowerCorner>
          <gml:upperCorner>565573.369 5936286.016</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A7A871FF-23B7-44EA-BA8D-C4A2BDA8FB41"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_6B3E7041-E88C-470C-A7B8-DA1C1382426F'-->
        <gml:Point gml:id="Gml_6B3E7041-E88C-470C-A7B8-DA1C1382426F" srsName="EPSG:25832">
          <gml:pos>565573.369 5936286.016</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_C141A31E-CF96-4265-995A-C3B2426FAAB2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565581.756 5936285.698</gml:lowerCorner>
          <gml:upperCorner>565581.756 5936285.698</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A7A871FF-23B7-44EA-BA8D-C4A2BDA8FB41"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_1F63697E-26EA-493A-AEE6-79764467F82E'-->
        <gml:Point gml:id="Gml_1F63697E-26EA-493A-AEE6-79764467F82E" srsName="EPSG:25832">
          <gml:pos>565581.756 5936285.698</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_50D8E4FE-5C02-48E9-BE26-28D67EF85A16">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565588.873 5936285.619</gml:lowerCorner>
          <gml:upperCorner>565588.873 5936285.619</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A7A871FF-23B7-44EA-BA8D-C4A2BDA8FB41"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_57750E89-C9BD-40CB-A5ED-2BA9DF9415B4'-->
        <gml:Point gml:id="Gml_57750E89-C9BD-40CB-A5ED-2BA9DF9415B4" srsName="EPSG:25832">
          <gml:pos>565588.873 5936285.619</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_2733E85C-BC72-4566-B33F-A41704275314">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565598.949 5936285.474</gml:lowerCorner>
          <gml:upperCorner>565598.949 5936285.474</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A7A871FF-23B7-44EA-BA8D-C4A2BDA8FB41"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_417A2D92-41AA-4B3D-A583-F823A41BC23D'-->
        <gml:Point gml:id="Gml_417A2D92-41AA-4B3D-A583-F823A41BC23D" srsName="EPSG:25832">
          <gml:pos>565598.949 5936285.474</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_CDBD0068-C1B5-4DE9-9EB3-1699EDCD4CC9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565540.244 5936282.140</gml:lowerCorner>
          <gml:upperCorner>565675.994 5936385.447</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_092CFCE3-7C70-4B54-8299-77B9532213A9"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_CBC3738C-8E02-425F-8651-DDC5A299B92C"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_9997689C-974F-4265-A1C5-109A35E05FFC"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_39E286A1-2688-43EB-8F0F-0830B187886A"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_953F73E1-9945-40EA-84AD-C61874F26A4C"/>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_971BE4C4-E04B-4701-BEEF-9D7918C90196"/>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_735F3E62-AE80-4B08-A62B-2D18DFCC351D"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_BB133A51-C39D-40D0-B8CF-C3DE345D4C20'-->
        <gml:Polygon gml:id="Gml_BB133A51-C39D-40D0-B8CF-C3DE345D4C20" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565675.994 5936322.166 565670.722 5936326.226 565668.013 5936328.312 565666.959 5936328.912 565633.956 5936354.191 565621.756 5936363.535 565621.333 5936370.504 565616.097 5936370.602 565574.497 5936371.380 565573.260 5936371.403 565564.747 5936375.077 565547.231 5936382.635 565543.242 5936384.357 565542.106 5936385.447 565540.244 5936289.374 565549.924 5936289.186 565565.473 5936288.884 565569.271 5936288.810 565584.822 5936288.510 565603.840 5936288.142 565640.218 5936287.438 565640.064 5936282.938 565675.219 5936282.140 565675.994 5936322.166</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:Z>4</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_092CFCE3-7C70-4B54-8299-77B9532213A9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565589.019 5936303.809</gml:lowerCorner>
          <gml:upperCorner>565589.019 5936303.809</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_CDBD0068-C1B5-4DE9-9EB3-1699EDCD4CC9"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_1CF8CA20-07F1-4E32-8296-FF065A6B1C09'-->
        <gml:Point gml:id="Gml_1CF8CA20-07F1-4E32-8296-FF065A6B1C09" srsName="EPSG:25832">
          <gml:pos>565589.019 5936303.809</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_CBC3738C-8E02-425F-8651-DDC5A299B92C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565596.480 5936303.664</gml:lowerCorner>
          <gml:upperCorner>565596.480 5936303.664</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_CDBD0068-C1B5-4DE9-9EB3-1699EDCD4CC9"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_BE85F85C-2B3F-4E5B-AD47-597DDE554BB7'-->
        <gml:Point gml:id="Gml_BE85F85C-2B3F-4E5B-AD47-597DDE554BB7" srsName="EPSG:25832">
          <gml:pos>565596.480 5936303.664</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_9997689C-974F-4265-A1C5-109A35E05FFC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565602.433 5936303.584</gml:lowerCorner>
          <gml:upperCorner>565602.433 5936303.584</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_CDBD0068-C1B5-4DE9-9EB3-1699EDCD4CC9"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_41E27692-5E38-41F7-8C9F-E32BFE392650'-->
        <gml:Point gml:id="Gml_41E27692-5E38-41F7-8C9F-E32BFE392650" srsName="EPSG:25832">
          <gml:pos>565602.433 5936303.584</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_39E286A1-2688-43EB-8F0F-0830B187886A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565594.654 5936297.433</gml:lowerCorner>
          <gml:upperCorner>565594.654 5936297.433</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_CDBD0068-C1B5-4DE9-9EB3-1699EDCD4CC9"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_8C885A7B-5FC3-4BC2-A20B-EA3FC356CC4E'-->
        <gml:Point gml:id="Gml_8C885A7B-5FC3-4BC2-A20B-EA3FC356CC4E" srsName="EPSG:25832">
          <gml:pos>565594.654 5936297.433</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_F338CCF1-CDA2-4F51-9A0F-F7882FA0E7B5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565526.374 5936370.504</gml:lowerCorner>
          <gml:upperCorner>565621.333 5936423.059</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_3E269B23-98E8-4E0B-9ED1-B33DD0C29269"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_51684B85-A692-4544-BA4D-94AA8E321AE0"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_AA059669-A1E0-483A-8AB3-CEC84B562D69"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_2E4AA92F-D437-4B4D-B5CC-BFA4EC12089B"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_14529F22-C338-4D03-BD02-9CE91097DE3C"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_2CEEEED5-FB49-4E07-9E47-A4B2B87E5657'-->
        <gml:Polygon gml:id="Gml_2CEEEED5-FB49-4E07-9E47-A4B2B87E5657" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565618.142 5936423.059 565526.893 5936417.517 565526.374 5936385.106 565529.739 5936384.989 565536.387 5936385.371 565542.091 5936385.699 565542.106 5936385.447 565543.242 5936384.357 565547.231 5936382.635 565564.747 5936375.077 565573.260 5936371.403 565574.497 5936371.380 565616.097 5936370.602 565621.333 5936370.504 565618.142 5936423.059</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:Z>6</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1600</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_3E269B23-98E8-4E0B-9ED1-B33DD0C29269">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565560.165 5936412.036</gml:lowerCorner>
          <gml:upperCorner>565560.165 5936412.036</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_F338CCF1-CDA2-4F51-9A0F-F7882FA0E7B5"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_DF388D02-69E6-4206-AC6F-AAA4909DBD2C'-->
        <gml:Point gml:id="Gml_DF388D02-69E6-4206-AC6F-AAA4909DBD2C" srsName="EPSG:25832">
          <gml:pos>565560.165 5936412.036</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_51684B85-A692-4544-BA4D-94AA8E321AE0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565558.816 5936406.876</gml:lowerCorner>
          <gml:upperCorner>565558.816 5936406.876</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_F338CCF1-CDA2-4F51-9A0F-F7882FA0E7B5"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_8309A06D-FCF4-4BE0-9C65-0AE6DD7BA6B6'-->
        <gml:Point gml:id="Gml_8309A06D-FCF4-4BE0-9C65-0AE6DD7BA6B6" srsName="EPSG:25832">
          <gml:pos>565558.816 5936406.876</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_AA059669-A1E0-483A-8AB3-CEC84B562D69">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565563.317 5936406.846</gml:lowerCorner>
          <gml:upperCorner>565563.317 5936406.846</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_F338CCF1-CDA2-4F51-9A0F-F7882FA0E7B5"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_F7D7CE40-58B3-4603-B5BA-66682BC646AA'-->
        <gml:Point gml:id="Gml_F7D7CE40-58B3-4603-B5BA-66682BC646AA" srsName="EPSG:25832">
          <gml:pos>565563.317 5936406.846</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_2E4AA92F-D437-4B4D-B5CC-BFA4EC12089B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565560.307 5936402.415</gml:lowerCorner>
          <gml:upperCorner>565560.307 5936402.415</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_F338CCF1-CDA2-4F51-9A0F-F7882FA0E7B5"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_E273EBD4-654D-4B69-B8EC-2E21BA496C2D'-->
        <gml:Point gml:id="Gml_E273EBD4-654D-4B69-B8EC-2E21BA496C2D" srsName="EPSG:25832">
          <gml:pos>565560.307 5936402.415</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_3441B3DE-04CA-4580-BFF3-6177FCB7C91C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565495.758 5936259.136</gml:lowerCorner>
          <gml:upperCorner>565542.106 5936419.727</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_E817A841-B0C3-4FC9-A19D-CD278301B6F8"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_958A4FD5-B621-4581-9B0B-27B98F429EE3"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_B41D95DC-8D48-4817-94E1-AD5D0771B5D9"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_1A2A44AE-B6FD-41D9-892E-BE39AA705246"/>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_14529F22-C338-4D03-BD02-9CE91097DE3C"/>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_701E42E5-23F0-4B75-B11E-FF872CA73CB7"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_997647D6-3D7C-4FBF-8DAD-CB96B4F8F9C0'-->
        <gml:Polygon gml:id="Gml_997647D6-3D7C-4FBF-8DAD-CB96B4F8F9C0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565526.374 5936385.106 565526.893 5936417.517 565526.919 5936419.184 565498.866 5936419.727 565498.842 5936418.466 565498.213 5936386.075 565495.758 5936259.987 565533.936 5936259.247 565539.658 5936259.136 565540.244 5936289.374 565542.106 5936385.447 565542.091 5936385.699 565536.387 5936385.371 565529.739 5936384.989 565526.374 5936385.106</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1600</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_E817A841-B0C3-4FC9-A19D-CD278301B6F8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565506.058 5936291.249</gml:lowerCorner>
          <gml:upperCorner>565506.058 5936291.249</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_3441B3DE-04CA-4580-BFF3-6177FCB7C91C"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_20BF2395-C36D-4418-B7B0-9D4E85C9D3B2'-->
        <gml:Point gml:id="Gml_20BF2395-C36D-4418-B7B0-9D4E85C9D3B2" srsName="EPSG:25832">
          <gml:pos>565506.058 5936291.249</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_958A4FD5-B621-4581-9B0B-27B98F429EE3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565512.432 5936291.060</gml:lowerCorner>
          <gml:upperCorner>565512.432 5936291.060</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_3441B3DE-04CA-4580-BFF3-6177FCB7C91C"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_6A680C7D-0249-4B3D-99D9-5135B511761A'-->
        <gml:Point gml:id="Gml_6A680C7D-0249-4B3D-99D9-5135B511761A" srsName="EPSG:25832">
          <gml:pos>565512.432 5936291.060</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_B41D95DC-8D48-4817-94E1-AD5D0771B5D9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565507.140 5936286.166</gml:lowerCorner>
          <gml:upperCorner>565507.140 5936286.166</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_3441B3DE-04CA-4580-BFF3-6177FCB7C91C"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_205FCB2B-A70F-48A3-902F-C4D10E36FEB5'-->
        <gml:Point gml:id="Gml_205FCB2B-A70F-48A3-902F-C4D10E36FEB5" srsName="EPSG:25832">
          <gml:pos>565507.140 5936286.166</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinbedarfsFlaeche gml:id="GML_D5DF1DA9-0C99-4D16-88B3-FB9109948C15">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565762.569 5936217.216</gml:lowerCorner>
          <gml:upperCorner>565836.114 5936257.029</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_177ECE7D-22D3-485F-B08B-127234C3E5A8"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_D990C42E-0151-45E1-9022-A8DEF677D2E9"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_0EFEC8E7-D530-4A81-8C2A-36F7002A596C'-->
        <gml:Polygon gml:id="Gml_0EFEC8E7-D530-4A81-8C2A-36F7002A596C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_B049B821-F17D-4BE6-BCDF-1E6BE7F7538F" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc>
                      <gml:posList>565819.685 5936217.216 565828.572 5936219.739 565834.710 5936226.644</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_63FA41D8-EF86-4FBC-8B73-4C2BC741210B" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc>
                      <gml:posList>565834.710 5936226.644 565835.510 5936229.011 565835.804 5936231.493</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_55FDD6FA-7DCB-4EA9-ABD1-6A5E23350416" srsName="EPSG:25832">
                  <gml:posList>565835.804 5936231.493 565836.114 5936254.624</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6796DC3D-A908-48A8-B12E-F57F451C5FF9" srsName="EPSG:25832">
                  <gml:posList>565836.114 5936254.624 565807.066 5936255.184</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_36F2BE34-94D0-424D-98B5-155E1F627E67" srsName="EPSG:25832">
                  <gml:posList>565807.066 5936255.184 565806.102 5936256.201</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5813DF5E-255C-45D8-AC70-3177795FB376" srsName="EPSG:25832">
                  <gml:posList>565806.102 5936256.201 565763.113 5936257.029</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A9805747-42D2-4E31-A107-615AA0C1EBE4" srsName="EPSG:25832">
                  <gml:posList>565763.113 5936257.029 565762.657 5936233.472</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FC99FD48-ACF0-4569-B85E-3DB132F25EE2" srsName="EPSG:25832">
                  <gml:posList>565762.657 5936233.472 565762.569 5936228.920</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A65670B0-6CFE-4970-8B71-92F8F2D78D31" srsName="EPSG:25832">
                  <gml:posList>565762.569 5936228.920 565772.960 5936218.116</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_52E7C30F-9F5B-4E6E-A491-EF564A1053BC" srsName="EPSG:25832">
                  <gml:posList>565772.960 5936218.116 565819.685 5936217.216</gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:zweckbestimmung>1400</xplan:zweckbestimmung>
      <xplan:zugunstenVon>Ev. Luth. Kirchengem. St. Johannis/Harvestehude</xplan:zugunstenVon>
    </xplan:BP_GemeinbedarfsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_177ECE7D-22D3-485F-B08B-127234C3E5A8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565825.834 5936243.451</gml:lowerCorner>
          <gml:upperCorner>565825.834 5936243.451</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_D5DF1DA9-0C99-4D16-88B3-FB9109948C15"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_C20FA51E-6DEE-44F1-AE3F-AFAF898407B0'-->
        <gml:Point gml:id="Gml_C20FA51E-6DEE-44F1-AE3F-AFAF898407B0" srsName="EPSG:25832">
          <gml:pos>565825.834 5936243.451</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_D990C42E-0151-45E1-9022-A8DEF677D2E9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565800.131 5936227.997</gml:lowerCorner>
          <gml:upperCorner>565800.131 5936227.997</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zugunstenVon</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_D5DF1DA9-0C99-4D16-88B3-FB9109948C15"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_16ACA0F3-23AD-4B19-992B-0EC77CC4084A'-->
        <gml:Point gml:id="Gml_16ACA0F3-23AD-4B19-992B-0EC77CC4084A" srsName="EPSG:25832">
          <gml:pos>565800.131 5936227.997</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinbedarfsFlaeche gml:id="GML_96EED429-9C51-42E2-AAD2-D4168755DF04">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565763.113 5936254.624</gml:lowerCorner>
          <gml:upperCorner>565836.244 5936291.700</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:text>Kindertagesheim</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_B75A44BA-B8F8-4293-8E1C-32B354C9E4E7"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_2689AEDE-EA9D-4AB4-840F-604C48A3B4BC"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_5764FE87-F634-4686-89DA-0647DA2AB77D"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_83AED188-9B55-4040-8CCA-EFA4E2755E4B"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_83DCACBD-A6A9-40BA-BEAA-6D9893B763BE'-->
        <gml:Polygon gml:id="Gml_83DCACBD-A6A9-40BA-BEAA-6D9893B763BE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565810.790 5936290.978 565773.195 5936291.700 565772.283 5936265.629 565763.282 5936265.801 565763.113 5936257.029 565806.102 5936256.201 565807.066 5936255.184 565836.114 5936254.624 565836.244 5936264.398 565810.292 5936264.896 565810.790 5936290.978</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:zweckbestimmung>16000</xplan:zweckbestimmung>
      <xplan:zweckbestimmung>1600</xplan:zweckbestimmung>
      <xplan:zugunstenVon>Freie und Hansestadt Hamburg</xplan:zugunstenVon>
    </xplan:BP_GemeinbedarfsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_B75A44BA-B8F8-4293-8E1C-32B354C9E4E7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565794.084 5936276.524</gml:lowerCorner>
          <gml:upperCorner>565794.084 5936276.524</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>besondereZweckbestimmung</xplan:art>
      <xplan:art>text</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:index>2</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_96EED429-9C51-42E2-AAD2-D4168755DF04"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_D4BC7AB5-437B-4CF2-AA35-A97AB3EF8496'-->
        <gml:Point gml:id="Gml_D4BC7AB5-437B-4CF2-AA35-A97AB3EF8496" srsName="EPSG:25832">
          <gml:pos>565794.084 5936276.524</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_2689AEDE-EA9D-4AB4-840F-604C48A3B4BC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565794.622 5936258.829</gml:lowerCorner>
          <gml:upperCorner>565794.622 5936258.829</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zugunstenVon</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_96EED429-9C51-42E2-AAD2-D4168755DF04"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_D96339A0-6D0D-48CC-B2A2-0D8FDA8BDECE'-->
        <gml:Point gml:id="Gml_D96339A0-6D0D-48CC-B2A2-0D8FDA8BDECE" srsName="EPSG:25832">
          <gml:pos>565794.622 5936258.829</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="Gml_5764FE87-F634-4686-89DA-0647DA2AB77D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565791.553 5936262.321</gml:lowerCorner>
          <gml:upperCorner>565791.553 5936262.321</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>text</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_96EED429-9C51-42E2-AAD2-D4168755DF04"/>
      <xplan:horizontaleAusrichtung>zentrisch</xplan:horizontaleAusrichtung>
      <xplan:vertikaleAusrichtung>Mitte</xplan:vertikaleAusrichtung>
      <xplan:position>
        <!--Inlined geometry 'Gml_83A068BE-F57C-4C90-982C-8ADD577F938F'-->
        <gml:Point gml:id="Gml_83A068BE-F57C-4C90-982C-8ADD577F938F" srsName="EPSG:25832">
          <gml:pos>565791.553 5936262.321</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_83AED188-9B55-4040-8CCA-EFA4E2755E4B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565781.983 5936286.590</gml:lowerCorner>
          <gml:upperCorner>565781.983 5936286.590</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_96EED429-9C51-42E2-AAD2-D4168755DF04"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_A566274C-9AA6-42F0-A25E-2286DCE111DC'-->
        <gml:Point gml:id="Gml_A566274C-9AA6-42F0-A25E-2286DCE111DC" srsName="EPSG:25832">
          <gml:pos>565781.983 5936286.590</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinbedarfsFlaeche gml:id="GML_A155962F-8CA9-41E5-9981-517C172E2D32">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565618.142 5936216.556</gml:lowerCorner>
          <gml:upperCorner>565861.203 5936430.426</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_1231631B-C3EB-4F28-8C09-E326E8BAB117"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_26938415-CDFF-4218-835E-35E6C32177B7"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_D991F9AB-A34B-4219-AE9D-C32552157BE7"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_F25329F2-FB16-4BFD-9512-D8D3E2BF1B70"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_703BDE29-FC56-43DC-9C14-014BF0191C6D"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_A32E9EAE-491E-4293-B6C0-0A8F94F58BDE'-->
        <gml:Polygon gml:id="Gml_A32E9EAE-491E-4293-B6C0-0A8F94F58BDE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A35EA73B-6F46-47B0-9352-91199069D026" srsName="EPSG:25832">
                  <gml:posList>565852.669 5936327.918 565844.986 5936347.825</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_975C99DB-F048-40EF-A147-B34F374B5B5B" srsName="EPSG:25832">
                  <gml:posList>565844.986 5936347.825 565825.060 5936402.202</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9E1DD35C-CC82-48B4-BCA1-F8E47B916396" srsName="EPSG:25832">
                  <gml:posList>565825.060 5936402.202 565824.603 5936403.452</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B839A761-6EC0-4350-BCAC-5F71C83EB374" srsName="EPSG:25832">
                  <gml:posList>565824.603 5936403.452 565821.505 5936403.535</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6256909D-615C-4F57-9474-EC0E368E8C57" srsName="EPSG:25832">
                  <gml:posList>565821.505 5936403.535 565769.908 5936404.896</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0CF371EE-D321-41C2-A994-BD45E5CD0DAF" srsName="EPSG:25832">
                  <gml:posList>565769.908 5936404.896 565748.776 5936405.454</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_67E95B01-BF88-4C1C-B487-2211FD12375A" srsName="EPSG:25832">
                  <gml:posList>565748.776 5936405.454 565746.224 5936405.521</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FB0C0D69-80A8-4813-8115-2B88F1BBC5C5" srsName="EPSG:25832">
                  <gml:posList>565746.224 5936405.521 565739.295 5936405.703</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2002D808-83AE-4180-BC19-70DCC18B9F4C" srsName="EPSG:25832">
                  <gml:posList>565739.295 5936405.703 565739.295 5936406.305</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6454AA47-B22E-4344-9A31-12000A65F7B3" srsName="EPSG:25832">
                  <gml:posList>565739.295 5936406.305 565739.283 5936422.224</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A778263D-393A-453C-8B9B-A95D4182C4A6" srsName="EPSG:25832">
                  <gml:posList>565739.283 5936422.224 565739.277 5936430.426</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5EC481C1-149D-4F32-9237-490F198B3792" srsName="EPSG:25832">
                  <gml:posList>565739.277 5936430.426 565618.142 5936423.059</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5E95954B-20EC-4046-8D57-6C17C6DE0742" srsName="EPSG:25832">
                  <gml:posList>565618.142 5936423.059 565621.333 5936370.504</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_66681B4F-C256-452C-A944-8F56B16C4E1C" srsName="EPSG:25832">
                  <gml:posList>565621.333 5936370.504 565621.756 5936363.535</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EF42C7AA-7C5A-4C4A-B785-D1EAE8070D34" srsName="EPSG:25832">
                  <gml:posList>565621.756 5936363.535 565633.956 5936354.191</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BBFF8C06-295D-426D-8C13-041AAA19080F" srsName="EPSG:25832">
                  <gml:posList>565633.956 5936354.191 565666.959 5936328.912</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A9BD62A3-4E13-416C-A743-070A5B6B7146" srsName="EPSG:25832">
                  <gml:posList>565666.959 5936328.912 565668.013 5936328.312</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D50CCB10-A40C-47DE-B513-0D2D005E7C97" srsName="EPSG:25832">
                  <gml:posList>565668.013 5936328.312 565670.722 5936326.226</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F5F7A328-8E38-4D75-868D-502E3746D20A" srsName="EPSG:25832">
                  <gml:posList>565670.722 5936326.226 565675.994 5936322.166</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_14A239BA-7AD0-4F1E-A6AF-4DD7A57D877C" srsName="EPSG:25832">
                  <gml:posList>565675.994 5936322.166 565675.219 5936282.140</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_42D39717-0E3F-4CC5-A0AF-D9554D629E5C" srsName="EPSG:25832">
                  <gml:posList>565675.219 5936282.140 565674.724 5936256.562</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_427192DC-329B-4E72-8B30-A520F6C7AE2B" srsName="EPSG:25832">
                  <gml:posList>565674.724 5936256.562 565674.745 5936256.561</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_10A2CB56-5FC1-4D88-8671-B7EB9412B8E5" srsName="EPSG:25832">
                  <gml:posList>565674.745 5936256.561 565674.744 5936256.517</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_04130B23-6A5F-4E6E-B280-AB20DE046463" srsName="EPSG:25832">
                  <gml:posList>565674.744 5936256.517 565740.084 5936255.249</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6841C8CF-1301-46D1-865A-7EF1E8969913" srsName="EPSG:25832">
                  <gml:posList>565740.084 5936255.249 565740.297 5936266.246</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3FCF3120-8E43-4B1B-835A-AC2A90B50B02" srsName="EPSG:25832">
                  <gml:posList>565740.297 5936266.246 565763.282 5936265.801</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2530D860-C858-4586-8D87-DFD4A0414526" srsName="EPSG:25832">
                  <gml:posList>565763.282 5936265.801 565772.283 5936265.629</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6DFC4EA4-FEBD-4835-8FD4-B4E1FC8C147F" srsName="EPSG:25832">
                  <gml:posList>565772.283 5936265.629 565773.195 5936291.700</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CFACE715-E2BD-4395-8DB3-F22F02D9332D" srsName="EPSG:25832">
                  <gml:posList>565773.195 5936291.700 565810.790 5936290.978</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_875D1DC7-DBB7-4070-8BED-E97A3471DB06" srsName="EPSG:25832">
                  <gml:posList>565810.790 5936290.978 565810.292 5936264.896</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1D435CE5-E7B4-49F3-9FD7-6267162069F0" srsName="EPSG:25832">
                  <gml:posList>565810.292 5936264.896 565836.244 5936264.398</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9E41AA23-6752-46BA-9B84-FD144487522B" srsName="EPSG:25832">
                  <gml:posList>565836.244 5936264.398 565836.114 5936254.624</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_65405E45-413B-4D43-900C-118A1182BF9B" srsName="EPSG:25832">
                  <gml:posList>565836.114 5936254.624 565835.804 5936231.493</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_80115BCE-CB5E-4C42-B743-4D138662E1C7" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc>
                      <gml:posList>565835.804 5936231.493 565835.510 5936229.011 565834.710 5936226.644</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_13FDFDFF-41BD-4F31-8F20-FB1BA8294FA8" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc>
                      <gml:posList>565834.710 5936226.644 565828.572 5936219.739 565819.685 5936217.216</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C12D2712-B1D7-4995-9A3A-6D5FACF2350D" srsName="EPSG:25832">
                  <gml:posList>565819.685 5936217.216 565820.565 5936217.199</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4F44E583-2E2E-4B53-B658-93BE6E2266A0" srsName="EPSG:25832">
                  <gml:posList>565820.565 5936217.199 565853.936 5936216.556</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_39E2C2C6-45A1-40AB-B77D-34B2B4ABE650" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc>
                      <gml:posList>565853.936 5936216.556 565844.642 5936220.605 565840.901 5936230.026</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_92F6CC02-11F6-407A-BF41-E193F2494992" srsName="EPSG:25832">
                  <gml:posList>565840.901 5936230.026 565841.229 5936254.526</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5B88F7A6-A2EF-477F-8C31-5588018C09E9" srsName="EPSG:25832">
                  <gml:posList>565841.229 5936254.526 565846.382 5936254.426</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8E757786-5D19-42C2-B156-3246F68F51CB" srsName="EPSG:25832">
                  <gml:posList>565846.382 5936254.426 565846.587 5936271.227</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5E6F8A45-907D-4B2F-BFE8-874A28670A66" srsName="EPSG:25832">
                  <gml:posList>565846.587 5936271.227 565847.485 5936299.553</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9034DAB2-8F3F-45AC-9D0C-1F4EEFC86B05" srsName="EPSG:25832">
                  <gml:posList>565847.485 5936299.553 565847.615 5936303.631</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DD07BA4F-8351-4E24-85CD-BA942BFA60B0" srsName="EPSG:25832">
                  <gml:posList>565847.615 5936303.631 565861.203 5936303.622</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_728E333B-F247-4667-A180-FD63D916CD25" srsName="EPSG:25832">
                  <gml:posList>565861.203 5936303.622 565852.669 5936327.918</gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GFZ>1.2</xplan:GFZ>
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:zweckbestimmung>12000</xplan:zweckbestimmung>
      <xplan:zweckbestimmung>1200</xplan:zweckbestimmung>
      <xplan:zugunstenVon>Freie und Hansestadt Hamburg</xplan:zugunstenVon>
    </xplan:BP_GemeinbedarfsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1231631B-C3EB-4F28-8C09-E326E8BAB117">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565706.507 5936375.743</gml:lowerCorner>
          <gml:upperCorner>565706.507 5936375.743</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>besondereZweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A155962F-8CA9-41E5-9981-517C172E2D32"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_47DE9B16-B02B-43D4-BECB-6B579CA80343'-->
        <gml:Point gml:id="Gml_47DE9B16-B02B-43D4-BECB-6B579CA80343" srsName="EPSG:25832">
          <gml:pos>565706.507 5936375.743</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_26938415-CDFF-4218-835E-35E6C32177B7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565777.139 5936342.416</gml:lowerCorner>
          <gml:upperCorner>565777.139 5936342.416</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A155962F-8CA9-41E5-9981-517C172E2D32"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_672C1A38-EA97-4AF8-B3EF-3BCEA7E4621E'-->
        <gml:Point gml:id="Gml_672C1A38-EA97-4AF8-B3EF-3BCEA7E4621E" srsName="EPSG:25832">
          <gml:pos>565777.139 5936342.416</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_D991F9AB-A34B-4219-AE9D-C32552157BE7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565777.139 5936337.495</gml:lowerCorner>
          <gml:upperCorner>565777.139 5936337.495</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zugunstenVon</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A155962F-8CA9-41E5-9981-517C172E2D32"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_4E1920BA-78CA-4809-9DCD-3A339718F089'-->
        <gml:Point gml:id="Gml_4E1920BA-78CA-4809-9DCD-3A339718F089" srsName="EPSG:25832">
          <gml:pos>565777.139 5936337.495</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_F25329F2-FB16-4BFD-9512-D8D3E2BF1B70">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565669.085 5936361.156</gml:lowerCorner>
          <gml:upperCorner>565669.085 5936361.156</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A155962F-8CA9-41E5-9981-517C172E2D32"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_4BA391EB-80D7-4C51-A70A-04D797A94755'-->
        <gml:Point gml:id="Gml_4BA391EB-80D7-4C51-A70A-04D797A94755" srsName="EPSG:25832">
          <gml:pos>565669.085 5936361.156</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_703BDE29-FC56-43DC-9C14-014BF0191C6D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565669.396 5936356.621</gml:lowerCorner>
          <gml:upperCorner>565669.396 5936356.621</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GFZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A155962F-8CA9-41E5-9981-517C172E2D32"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_F9904D1C-9570-4F42-B496-CFD1E12ADACB'-->
        <gml:Point gml:id="Gml_F9904D1C-9570-4F42-B496-CFD1E12ADACB" srsName="EPSG:25832">
          <gml:pos>565669.396 5936356.621</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_3CBFDC90-E513-4E03-B080-CA2063CED127">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565487.665 5936206.747</gml:lowerCorner>
          <gml:upperCorner>565898.316 5936449.368</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_B01B144C-0132-451B-8D4B-8EAC248F007E'-->
        <gml:Polygon gml:id="Gml_B01B144C-0132-451B-8D4B-8EAC248F007E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E386B3C5-F120-4316-827A-C72988FA2936" srsName="EPSG:25832">
                  <gml:posList>565822.120 5936448.807 565820.771 5936449.368</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E35DF06B-EB33-4E5F-82C5-98094AD748DD" srsName="EPSG:25832">
                  <gml:posList>565820.771 5936449.368 565808.033 5936448.664</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DAD75002-5FD4-4E0D-A939-7903E7449BBA" srsName="EPSG:25832">
                  <gml:posList>565808.033 5936448.664 565599.581 5936436.686</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C1767AD6-8BC6-4038-8715-9B689765B674" srsName="EPSG:25832">
                  <gml:posList>565599.581 5936436.686 565499.081 5936430.999</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D6175148-0778-4F58-A2C8-792FBE8CD3A4" srsName="EPSG:25832">
                  <gml:posList>565499.081 5936430.999 565498.866 5936419.727</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D29675E4-EF18-49A0-B863-A72681BD3F89" srsName="EPSG:25832">
                  <gml:posList>565498.866 5936419.727 565526.919 5936419.184</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4597CEDD-1DD2-4016-A548-1468D86887BD" srsName="EPSG:25832">
                  <gml:posList>565526.919 5936419.184 565526.893 5936417.517</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BEFC1828-17B0-4803-AAD9-A88313D4AB0A" srsName="EPSG:25832">
                  <gml:posList>565526.893 5936417.517 565618.142 5936423.059</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A1BB78B2-CB27-4040-98DA-846B3C45EC47" srsName="EPSG:25832">
                  <gml:posList>565618.142 5936423.059 565739.277 5936430.426</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FBB81767-42CE-400A-B1C9-F9172ECDD7AB" srsName="EPSG:25832">
                  <gml:posList>565739.277 5936430.426 565739.463 5936430.438</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6F4DAA2E-9156-4609-843E-444DF8FC6F4F" srsName="EPSG:25832">
                  <gml:posList>565739.463 5936430.438 565743.464 5936430.681</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8B9C3653-54DA-4B03-BD92-913F14F48C91" srsName="EPSG:25832">
                  <gml:posList>565743.464 5936430.681 565746.957 5936430.892</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BE937EFB-D4CC-4968-B5D6-8B76B1B4B0B9" srsName="EPSG:25832">
                  <gml:posList>565746.957 5936430.892 565749.244 5936429.667</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D7FA9DDF-2929-4293-ABA7-E8D90960FFA3" srsName="EPSG:25832">
                  <gml:posList>565749.244 5936429.667 565778.149 5936414.176</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B3749708-48B7-4DB8-9C93-6E3DFC880111" srsName="EPSG:25832">
                  <gml:posList>565778.149 5936414.176 565821.089 5936413.043</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0F6750F1-8A7F-40AE-978E-4AABEEC5F408" srsName="EPSG:25832">
                  <gml:posList>565821.089 5936413.043 565824.603 5936403.452</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6363B1CE-4699-4C52-B1E0-362DF6C36351" srsName="EPSG:25832">
                  <gml:posList>565824.603 5936403.452 565825.060 5936402.202</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_65D77E43-1E59-4A4E-A65A-AF465B7E33B7" srsName="EPSG:25832">
                  <gml:posList>565825.060 5936402.202 565844.986 5936347.825</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FC9E6945-28A1-4000-B30E-B2D84FF1303B" srsName="EPSG:25832">
                  <gml:posList>565844.986 5936347.825 565852.669 5936327.918</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2E7FF2D1-D6FD-44D3-AAD6-4598BE7C76C0" srsName="EPSG:25832">
                  <gml:posList>565852.669 5936327.918 565861.203 5936303.622</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_986B6757-2BC8-4C77-BB9D-380558FA5D29" srsName="EPSG:25832">
                  <gml:posList>565861.203 5936303.622 565874.620 5936265.427</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6E360245-2860-425F-A605-D9841A57ADD7" srsName="EPSG:25832">
                  <gml:posList>565874.620 5936265.427 565883.866 5936229.971</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EBAEC7C4-5DF7-400E-A6BC-CC1870843EA8" srsName="EPSG:25832">
                  <gml:posList>565883.866 5936229.971 565884.054 5936226.489</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DB5F23E1-4058-437E-97ED-9FC61EF16A17" srsName="EPSG:25832">
                  <gml:posList>565884.054 5936226.489 565883.818 5936223.337</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_281AC673-917E-49B6-8016-F753D0752D22" srsName="EPSG:25832">
                  <gml:posList>565883.818 5936223.337 565883.214 5936221.686</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FFC72803-1F89-4FDE-9EB8-5BA0F5A840EE" srsName="EPSG:25832">
                  <gml:posList>565883.214 5936221.686 565881.890 5936219.636</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_DFF07E82-85B1-411C-88F2-7FFC20A7CADE" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc>
                      <gml:posList>565881.890 5936219.636 565877.913 5936217.036 565873.238 5936216.183</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A74D391B-0BB6-4C49-9432-3DF6AF7BA820" srsName="EPSG:25832">
                  <gml:posList>565873.238 5936216.183 565853.936 5936216.556</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_80D0001E-8EA9-40EC-BE5F-3C70EB68F10F" srsName="EPSG:25832">
                  <gml:posList>565853.936 5936216.556 565820.565 5936217.199</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C41569DB-FC12-47F9-B449-04615B8B6306" srsName="EPSG:25832">
                  <gml:posList>565820.565 5936217.199 565819.685 5936217.216</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_811DEA8D-88CE-40C0-B35F-629849D0F62D" srsName="EPSG:25832">
                  <gml:posList>565819.685 5936217.216 565772.960 5936218.116</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4BAC2BC0-9957-4E76-A0F2-57CCB79C9E95" srsName="EPSG:25832">
                  <gml:posList>565772.960 5936218.116 565762.569 5936228.920</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B0B29FFE-F257-4C5B-B652-3BD51933F83E" srsName="EPSG:25832">
                  <gml:posList>565762.569 5936228.920 565762.657 5936233.472</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7B61360D-0875-491A-8201-5D08BC3BC8DB" srsName="EPSG:25832">
                  <gml:posList>565762.657 5936233.472 565763.113 5936257.029</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8BDE93F3-F426-4AA0-ABA0-2CA61B10E9B3" srsName="EPSG:25832">
                  <gml:posList>565763.113 5936257.029 565763.282 5936265.801</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_82D3D718-3869-4FE1-81EE-CB52B88A3F48" srsName="EPSG:25832">
                  <gml:posList>565763.282 5936265.801 565740.297 5936266.246</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_70F60C78-B918-47A2-ABD9-0C7336AAC282" srsName="EPSG:25832">
                  <gml:posList>565740.297 5936266.246 565740.084 5936255.249</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F57B1AC4-4130-43D9-9838-5B9295FCA590" srsName="EPSG:25832">
                  <gml:posList>565740.084 5936255.249 565674.744 5936256.517</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_193119F0-EEDC-4A2D-98C9-345CA82039D8" srsName="EPSG:25832">
                  <gml:posList>565674.744 5936256.517 565674.723 5936256.517</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FFAB4123-70A0-4535-A9F9-FEFBEF026E75" srsName="EPSG:25832">
                  <gml:posList>565674.723 5936256.517 565654.126 5936256.916</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_391E2A4F-DC9D-430E-B723-3125CE70D9CE" srsName="EPSG:25832">
                  <gml:posList>565654.126 5936256.916 565568.684 5936258.574</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E226E2DA-3E3C-47F4-B009-DFB30F29A3B5" srsName="EPSG:25832">
                  <gml:posList>565568.684 5936258.574 565564.887 5936258.648</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DC7348FD-61A3-4964-A003-5D257363DD41" srsName="EPSG:25832">
                  <gml:posList>565564.887 5936258.648 565539.658 5936259.136</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_76F0BC7E-F35A-4545-A2AA-04102B951BC4" srsName="EPSG:25832">
                  <gml:posList>565539.658 5936259.136 565533.936 5936259.247</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_06B447D0-AB2D-4B94-9C46-2B19EFE8CCC9" srsName="EPSG:25832">
                  <gml:posList>565533.936 5936259.247 565495.758 5936259.987</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6227DDC2-6C14-438E-9055-59B8DE941706" srsName="EPSG:25832">
                  <gml:posList>565495.758 5936259.987 565497.336 5936341.047</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_082A9D0C-7124-48D6-B6FE-62502A2E4691" srsName="EPSG:25832">
                  <gml:posList>565497.336 5936341.047 565489.820 5936284.271</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A7139D77-28A4-4D27-8A88-84EE5F610DA3" srsName="EPSG:25832">
                  <gml:posList>565489.820 5936284.271 565488.371 5936269.539</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CFFCC15E-E62E-43EB-9412-5B180DFFBE1A" srsName="EPSG:25832">
                  <gml:posList>565488.371 5936269.539 565487.665 5936259.143</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0BAFE9B6-5727-478B-9017-4EDB5909FA57" srsName="EPSG:25832">
                  <gml:posList>565487.665 5936259.143 565487.666 5936250.424</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CCD949D7-E079-4B63-9EA9-BF1CA799C2F8" srsName="EPSG:25832">
                  <gml:posList>565487.666 5936250.424 565597.587 5936248.162</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5CE999A8-DA49-43CE-8B80-27C0F80AE571" srsName="EPSG:25832">
                  <gml:posList>565597.587 5936248.162 565739.899 5936245.618</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_894F0F6F-E696-44FC-AE58-E5195F6DC717" srsName="EPSG:25832">
                  <gml:posList>565739.899 5936245.618 565751.267 5936245.375</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_959F4101-0DEE-4E2F-9D88-7522367FF727" srsName="EPSG:25832">
                  <gml:posList>565751.267 5936245.375 565750.877 5936206.747</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6FF63148-9262-4E2A-8070-1D252D48D713" srsName="EPSG:25832">
                  <gml:posList>565750.877 5936206.747 565762.142 5936207.082</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D4335285-139A-43DE-BB40-C77F88C09390" srsName="EPSG:25832">
                  <gml:posList>565762.142 5936207.082 565898.316 5936207.367</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7179D6EF-4742-4938-98F8-BF94C218235F" srsName="EPSG:25832">
                  <gml:posList>565898.316 5936207.367 565896.692 5936220.468</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9985C2D6-119A-42FF-9597-FD03C78AE60E" srsName="EPSG:25832">
                  <gml:posList>565896.692 5936220.468 565893.615 5936239.881</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C1EE74CF-C897-49F6-B230-77CBCC4748C3" srsName="EPSG:25832">
                  <gml:posList>565893.615 5936239.881 565890.858 5936253.274</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_914CB3A1-81E9-499C-9C89-79A8E3DB92FC" srsName="EPSG:25832">
                  <gml:posList>565890.858 5936253.274 565887.312 5936266.667</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_40AF5ED4-7F5F-4CFB-9600-C3AE7669CDA3" srsName="EPSG:25832">
                  <gml:posList>565887.312 5936266.667 565880.088 5936288.591</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1C6AF496-73AC-4D02-887B-9B837669387F" srsName="EPSG:25832">
                  <gml:posList>565880.088 5936288.591 565858.948 5936346.278</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E91905FF-29CA-40F0-8B98-D3FA9C036DB9" srsName="EPSG:25832">
                  <gml:posList>565858.948 5936346.278 565822.120 5936448.807</gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_E34F3B9D-9BCB-4D70-9640-0F30D591C0AD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565463.986 5936250.573</gml:lowerCorner>
          <gml:upperCorner>565482.820 5936430.079</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_04942E0B-A61F-4756-A80C-A14EC87B91B0'-->
        <gml:Polygon gml:id="Gml_04942E0B-A61F-4756-A80C-A14EC87B91B0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565482.820 5936430.079 565479.664 5936429.901 565467.314 5936429.202 565465.920 5936429.096 565465.788 5936422.385 565464.015 5936332.140 565463.986 5936250.911 565477.014 5936250.643 565480.419 5936250.573 565482.820 5936430.079</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_BEA37C0D-23EB-4EC6-8647-E9F925694749">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565480.419 5936250.424</gml:lowerCorner>
          <gml:upperCorner>565499.081 5936430.999</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_D60CAEFC-9BA8-4CD6-B3DE-1D1AF21BEA35'-->
        <gml:Polygon gml:id="Gml_D60CAEFC-9BA8-4CD6-B3DE-1D1AF21BEA35" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565487.665 5936259.143 565488.371 5936269.539 565489.820 5936284.271 565497.336 5936341.047 565498.213 5936386.075 565498.842 5936418.466 565498.866 5936419.727 565499.081 5936430.999 565482.820 5936430.079 565480.419 5936250.573 565487.666 5936250.424 565487.665 5936259.143</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_5839EE24-FA42-4F79-ADD7-2A83E4302917">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565773.235 5936243.064</gml:lowerCorner>
          <gml:upperCorner>565773.235 5936243.064</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2000</xplan:hoehenbezug>
          <xplan:h uom="m">45</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_C1BD8990-5933-49EA-A463-7DD6ADDCAEB7'-->
        <gml:Point gml:id="Gml_C1BD8990-5933-49EA-A463-7DD6ADDCAEB7" srsName="EPSG:25832">
          <gml:pos>565773.235 5936243.064</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_59E5D563-8773-4C5B-B5F5-D8C9377CF0CD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565806.397 5936242.871</gml:lowerCorner>
          <gml:upperCorner>565806.397 5936242.871</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2000</xplan:hoehenbezug>
          <xplan:h uom="m">18</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_11C97862-CA8B-4905-98EE-5C73E0478E1D'-->
        <gml:Point gml:id="Gml_11C97862-CA8B-4905-98EE-5C73E0478E1D" srsName="EPSG:25832">
          <gml:pos>565806.397 5936242.871</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_6B237DE5-6562-4C01-8B8D-FC57F8631F09">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565540.244 5936282.140</gml:lowerCorner>
          <gml:upperCorner>565675.219 5936289.374</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_CE48BB3E-554C-446D-B2BD-7F8BA751B748'-->
        <gml:LineString gml:id="Gml_CE48BB3E-554C-446D-B2BD-7F8BA751B748" srsName="EPSG:25832">
          <gml:posList>565540.244 5936289.374 565549.924 5936289.186 565565.473 5936288.884 565569.271 5936288.810 565584.822 5936288.510 565603.840 5936288.142 565640.218 5936287.438 565640.064 5936282.938 565675.219 5936282.140</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_BA552C1A-9F28-4E9D-8676-285D57862702">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565526.374 5936384.989</gml:lowerCorner>
          <gml:upperCorner>565542.106 5936417.517</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_575BE0C5-48A4-4B4E-9C10-231729945667'-->
        <gml:LineString gml:id="Gml_575BE0C5-48A4-4B4E-9C10-231729945667" srsName="EPSG:25832">
          <gml:posList>565526.893 5936417.517 565526.374 5936385.106 565529.739 5936384.989 565536.387 5936385.371 565542.091 5936385.699 565542.106 5936385.447</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_A009FB88-28FF-46FC-8EBE-0FF7BF4F1656">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565763.113 5936217.199</gml:lowerCorner>
          <gml:upperCorner>565836.114 5936257.029</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_A16BC530-C8BA-4FFC-B546-A2A5B30ECD02'-->
        <gml:Curve gml:id="Gml_A16BC530-C8BA-4FFC-B546-A2A5B30ECD02" srsName="EPSG:25832">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565763.113 5936257.029 565806.102 5936256.201</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565806.102 5936256.201 565807.066 5936255.184</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565807.066 5936255.184 565836.114 5936254.624</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565836.114 5936254.624 565835.804 5936231.493</gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList>565835.804 5936231.493 565835.510 5936229.011 565834.710 5936226.644</gml:posList>
            </gml:ArcString>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList>565834.710 5936226.644 565828.572 5936219.739 565819.685 5936217.216</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565819.685 5936217.216 565820.565 5936217.199</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_CC9BECD1-4E1B-4C2F-86D3-B0BDD5058CC4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565763.282 5936254.624</gml:lowerCorner>
          <gml:upperCorner>565836.244 5936291.700</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_5E89917C-E5C9-4F21-9F46-2C8FEDD71BB5'-->
        <gml:LineString gml:id="Gml_5E89917C-E5C9-4F21-9F46-2C8FEDD71BB5" srsName="EPSG:25832">
          <gml:posList>565836.114 5936254.624 565836.244 5936264.398 565810.292 5936264.896 565810.790 5936290.978 565773.195 5936291.700 565772.283 5936265.629 565763.282 5936265.801</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_23FA98F2-638A-437D-9179-24E5426988B0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565554.800 5936288.895</gml:lowerCorner>
          <gml:upperCorner>565566.432 5936368.235</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_8329C673-263A-4D54-ACE7-91D10A2FEF5B'-->
        <gml:Polygon gml:id="Gml_8329C673-263A-4D54-ACE7-91D10A2FEF5B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565564.894 5936288.899 565566.432 5936368.040 565556.338 5936368.235 565554.800 5936289.096 565554.800 5936289.091 565564.894 5936288.895 565564.894 5936288.899</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_C0449179-CB75-4AD6-8F37-2F9D759448F1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565569.891 5936288.605</gml:lowerCorner>
          <gml:upperCorner>565581.524 5936367.943</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_EBA259C4-5CA1-401D-B364-246697A4FDAE'-->
        <gml:Polygon gml:id="Gml_EBA259C4-5CA1-401D-B364-246697A4FDAE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565581.524 5936367.748 565571.429 5936367.943 565569.891 5936288.802 565569.891 5936288.798 565579.986 5936288.605 565581.524 5936367.748</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_10D913B7-0B5F-4D6F-9064-14EADFDD5575">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565608.719 5936287.851</gml:lowerCorner>
          <gml:upperCorner>565620.108 5936354.548</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_A0745358-3CC5-4F07-B5DB-77E0969A7A7C'-->
        <gml:Polygon gml:id="Gml_A0745358-3CC5-4F07-B5DB-77E0969A7A7C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565620.108 5936354.459 565615.510 5936354.548 565615.432 5936350.502 565609.935 5936350.609 565608.719 5936288.048 565618.814 5936287.851 565620.108 5936354.459</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_6AC26AFC-A93F-4D7A-A000-0A217A56A853">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565623.811 5936287.560</gml:lowerCorner>
          <gml:upperCorner>565635.121 5936354.362</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_D01CF537-EB01-4EFA-8FF2-FC5B95387D20'-->
        <gml:Polygon gml:id="Gml_D01CF537-EB01-4EFA-8FF2-FC5B95387D20" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565635.121 5936350.119 565629.624 5936350.226 565629.702 5936354.273 565625.105 5936354.362 565623.811 5936287.754 565633.905 5936287.560 565635.121 5936350.119</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_F86357A0-B4B1-4681-9B21-B83067B887D1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565526.488 5936377.919</gml:lowerCorner>
          <gml:upperCorner>565615.631 5936422.112</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_B950C3C0-B7EA-4648-A002-97EF7ABDCFB6'-->
        <gml:Polygon gml:id="Gml_B950C3C0-B7EA-4648-A002-97EF7ABDCFB6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565611.705 5936415.483 565614.946 5936415.685 565611.704 5936422.112 565592.954 5936420.947 565554.068 5936418.532 565530.055 5936417.040 565530.095 5936416.392 565526.872 5936416.188 565526.488 5936392.236 565526.502 5936392.237 565565.048 5936394.618 565572.454 5936395.075 565589.228 5936396.111 565589.694 5936388.875 565589.504 5936388.895 565589.313 5936388.908 565589.122 5936388.914 565588.931 5936388.914 565588.740 5936388.907 565588.550 5936388.893 565588.360 5936388.873 565588.171 5936388.846 565587.982 5936388.813 565587.796 5936388.773 565587.610 5936388.727 565587.426 5936388.674 565587.245 5936388.615 565587.065 5936388.550 565586.888 5936388.479 565586.713 5936388.401 565586.542 5936388.317 565586.373 5936388.228 565586.207 5936388.132 565586.045 5936388.031 565585.887 5936387.925 565585.732 5936387.813 565585.581 5936387.695 565585.435 5936387.573 565585.292 5936387.445 565585.155 5936387.313 565585.022 5936387.175 565584.893 5936387.034 565584.770 5936386.888 565584.652 5936386.738 565584.539 5936386.583 565584.432 5936386.425 565584.330 5936386.264 565584.234 5936386.098 565584.144 5936385.930 565584.060 5936385.759 565583.981 5936385.584 565583.909 5936385.407 565583.843 5936385.228 565583.783 5936385.047 565583.730 5936384.863 565583.683 5936384.678 565583.642 5936384.491 565583.608 5936384.303 565583.581 5936384.114 565583.559 5936383.924 565583.545 5936383.734 565583.537 5936383.543 565583.537 5936383.352 565583.542 5936383.161 565583.554 5936382.970 565583.573 5936382.780 565583.598 5936382.591 565583.630 5936382.402 565583.669 5936382.215 565583.714 5936382.030 565583.765 5936381.845 565583.823 5936381.663 565583.887 5936381.483 565583.958 5936381.306 565584.034 5936381.131 565584.117 5936380.958 565584.204 5936380.790 565584.298 5936380.624 565584.397 5936380.463 565584.502 5936380.304 565584.612 5936380.149 565584.728 5936379.998 565584.849 5936379.852 565584.974 5936379.709 565585.105 5936379.571 565585.240 5936379.438 565585.380 5936379.309 565585.524 5936379.185 565585.672 5936379.066 565585.825 5936378.953 565585.981 5936378.845 565586.141 5936378.742 565586.304 5936378.645 565586.470 5936378.553 565586.640 5936378.468 565586.812 5936378.388 565586.988 5936378.314 565587.165 5936378.246 565587.345 5936378.185 565587.527 5936378.130 565587.710 5936378.081 565587.895 5936378.038 565588.082 5936378.002 565588.270 5936377.972 565588.458 5936377.949 565588.647 5936377.933 565588.837 5936377.923 565589.027 5936377.919 565589.217 5936377.922 565589.407 5936377.932 565589.596 5936377.948 565589.785 5936377.970 565589.973 5936378.000 565590.159 5936378.035 565590.344 5936378.077 565590.528 5936378.126 565590.710 5936378.181 565590.890 5936378.242 565591.068 5936378.309 565591.243 5936378.382 565591.416 5936378.462 565591.585 5936378.547 565591.752 5936378.638 565591.916 5936378.735 565592.076 5936378.837 565592.232 5936378.945 565592.385 5936379.058 565592.533 5936379.177 565592.678 5936379.300 565592.818 5936379.428 565592.953 5936379.561 565593.084 5936379.699 565593.210 5936379.841 565593.331 5936379.988 565593.447 5936380.138 565593.558 5936380.293 565593.663 5936380.451 565593.763 5936380.613 565593.857 5936380.778 565593.945 5936380.946 565594.028 5936381.117 565594.104 5936381.291 565594.175 5936381.468 565594.239 5936381.646 565594.297 5936381.827 565594.349 5936382.010 565594.394 5936382.195 565594.433 5936382.380 565594.466 5936382.568 565594.492 5936382.756 565594.512 5936382.945 565594.525 5936383.134 565594.531 5936383.324 565595.557 5936383.390 565595.171 5936389.376 565593.536 5936389.270 565592.178 5936389.183 565591.721 5936396.280 565591.739 5936396.281 565613.097 5936397.605 565615.631 5936404.711 565612.393 5936404.509 565611.705 5936415.483</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_4E00A730-B542-449E-9D8A-E00C08A159D5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565498.433 5936326.847</gml:lowerCorner>
          <gml:upperCorner>565526.872 5936416.188</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_B942DC31-CD79-43D3-8267-612BC60B5C61"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_27A5925B-DCCC-40C5-A2C6-BB2863D13E18'-->
        <gml:Polygon gml:id="Gml_27A5925B-DCCC-40C5-A2C6-BB2863D13E18" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565526.488 5936392.236 565526.872 5936416.188 565500.127 5936414.497 565498.675 5936339.726 565498.433 5936327.284 565520.920 5936326.847 565522.059 5936385.462 565526.378 5936385.378 565526.488 5936392.236</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>6</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_B942DC31-CD79-43D3-8267-612BC60B5C61">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565512.314 5936374.812</gml:lowerCorner>
          <gml:upperCorner>565512.314 5936374.812</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_4E00A730-B542-449E-9D8A-E00C08A159D5"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_E8E41A8F-8D03-45EF-BD37-BA6F7F9FE448'-->
        <gml:Point gml:id="Gml_E8E41A8F-8D03-45EF-BD37-BA6F7F9FE448" srsName="EPSG:25832">
          <gml:pos>565512.314 5936374.812</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_C2FFA4CF-EA9C-4C6E-97CC-27D8477D8631">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565520.920 5936326.584</gml:lowerCorner>
          <gml:upperCorner>565535.554 5936385.462</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_EBE5A866-9513-47C7-A085-3A16ECB80C82"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_6DD45488-B21B-4D24-BF35-BF67858177C0'-->
        <gml:Polygon gml:id="Gml_6DD45488-B21B-4D24-BF35-BF67858177C0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565526.374 5936385.106 565526.378 5936385.378 565522.059 5936385.462 565520.920 5936326.847 565534.413 5936326.584 565535.551 5936385.200 565535.554 5936385.323 565529.739 5936384.989 565526.374 5936385.106</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>5</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_EBE5A866-9513-47C7-A085-3A16ECB80C82">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565528.057 5936341.607</gml:lowerCorner>
          <gml:upperCorner>565528.057 5936341.607</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_C2FFA4CF-EA9C-4C6E-97CC-27D8477D8631"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_A4DD2975-E794-486E-A8BE-782D576C5BBF'-->
        <gml:Point gml:id="Gml_A4DD2975-E794-486E-A8BE-782D576C5BBF" srsName="EPSG:25832">
          <gml:pos>565528.057 5936341.607</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_414773CC-21EE-41C1-846B-909ED7006FE9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565534.413 5936326.545</gml:lowerCorner>
          <gml:upperCorner>565537.556 5936385.438</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_34997323-1121-4389-8032-C1E49B6B513E"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_D1534224-F4FB-4A17-A81F-F17E8C4B0A7F'-->
        <gml:Polygon gml:id="Gml_D1534224-F4FB-4A17-A81F-F17E8C4B0A7F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565537.550 5936385.161 565537.556 5936385.438 565536.387 5936385.371 565535.554 5936385.323 565535.551 5936385.200 565534.413 5936326.584 565536.412 5936326.545 565537.550 5936385.161</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>4</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_34997323-1121-4389-8032-C1E49B6B513E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565536.019 5936356.447</gml:lowerCorner>
          <gml:upperCorner>565536.019 5936356.447</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_414773CC-21EE-41C1-846B-909ED7006FE9"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_C5402FE1-258D-496F-80D5-BC8D76A7B6A2'-->
        <gml:Point gml:id="Gml_C5402FE1-258D-496F-80D5-BC8D76A7B6A2" srsName="EPSG:25832">
          <gml:pos>565536.019 5936356.447</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_9ED07310-796D-4BDC-B3E4-9DB90BF19B8E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565525.599 5936284.470</gml:lowerCorner>
          <gml:upperCorner>565536.307 5936321.343</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_68D8DAF0-3B84-4541-9DB0-BCA166ECBB9A"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_8ED18E17-1524-4167-B866-FE4FF639569F'-->
        <gml:Polygon gml:id="Gml_8ED18E17-1524-4167-B866-FE4FF639569F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565536.307 5936321.149 565526.312 5936321.343 565525.599 5936284.665 565535.594 5936284.470 565536.307 5936321.149</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>5</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_68D8DAF0-3B84-4541-9DB0-BCA166ECBB9A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565530.835 5936305.756</gml:lowerCorner>
          <gml:upperCorner>565530.835 5936305.756</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_9ED07310-796D-4BDC-B3E4-9DB90BF19B8E"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_DA4BD1AD-6248-4F15-86C2-B22C8321C331'-->
        <gml:Point gml:id="Gml_DA4BD1AD-6248-4F15-86C2-B22C8321C331" srsName="EPSG:25832">
          <gml:pos>565530.835 5936305.756</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_E3F8D510-128F-4E04-8A4D-797CE9512D40">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565497.205 5936263.383</gml:lowerCorner>
          <gml:upperCorner>565535.594 5936321.887</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_57B1E7FA-DE70-4EA0-B8B0-DF028A809401"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_9EF13946-7450-4865-9151-C5DEFB4B0DF3'-->
        <gml:Polygon gml:id="Gml_9EF13946-7450-4865-9151-C5DEFB4B0DF3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565535.594 5936284.470 565525.599 5936284.665 565526.312 5936321.343 565498.328 5936321.887 565497.205 5936264.122 565535.184 5936263.383 565535.594 5936284.470</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>6</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_57B1E7FA-DE70-4EA0-B8B0-DF028A809401">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565510.859 5936305.888</gml:lowerCorner>
          <gml:upperCorner>565510.859 5936305.888</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_E3F8D510-128F-4E04-8A4D-797CE9512D40"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_D8A91630-519C-4285-9DCF-1CF0BBDEFFFF'-->
        <gml:Point gml:id="Gml_D8A91630-519C-4285-9DCF-1CF0BBDEFFFF" srsName="EPSG:25832">
          <gml:pos>565510.859 5936305.888</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_BED06115-A953-4934-90A5-13591CF61BC9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565535.184 5936263.294</gml:lowerCorner>
          <gml:upperCorner>565540.147 5936284.470</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_E6F1A34D-951C-4BE5-BEC8-18DD427E39E0"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_394ECA72-4F5C-4492-B439-7EE8C7F0E409'-->
        <gml:Polygon gml:id="Gml_394ECA72-4F5C-4492-B439-7EE8C7F0E409" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565540.147 5936284.382 565535.594 5936284.470 565535.184 5936263.383 565539.739 5936263.294 565540.147 5936284.382</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_E6F1A34D-951C-4BE5-BEC8-18DD427E39E0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565537.666 5936273.882</gml:lowerCorner>
          <gml:upperCorner>565537.666 5936273.882</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_BED06115-A953-4934-90A5-13591CF61BC9"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_BB4419A2-E592-4954-A6E3-52D281DCD856'-->
        <gml:Point gml:id="Gml_BB4419A2-E592-4954-A6E3-52D281DCD856" srsName="EPSG:25832">
          <gml:pos>565537.666 5936273.882</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_60F5F4AC-5DF3-47FF-816A-83AC93A45E31">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565625.560 5936259.306</gml:lowerCorner>
          <gml:upperCorner>565739.405 5936422.224</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_39872463-90B8-4D85-B736-A3A385271400"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_40C1662F-5E1A-42B6-AA98-A917F92E37BD'-->
        <gml:Polygon gml:id="Gml_40C1662F-5E1A-42B6-AA98-A917F92E37BD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565739.283 5936422.224 565625.560 5936416.455 565628.874 5936365.042 565684.345 5936367.893 565682.174 5936260.376 565739.405 5936259.306 565739.283 5936422.224</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_39872463-90B8-4D85-B736-A3A385271400">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565710.944 5936340.312</gml:lowerCorner>
          <gml:upperCorner>565710.944 5936340.312</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_60F5F4AC-5DF3-47FF-816A-83AC93A45E31"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_9B1827B1-0F87-422A-A2AF-69E6D37D7525'-->
        <gml:Point gml:id="Gml_9B1827B1-0F87-422A-A2AF-69E6D37D7525" srsName="EPSG:25832">
          <gml:pos>565710.944 5936340.312</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_1D95AC59-79F5-4EF5-BC65-ACBE31F3AFF7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565749.776 5936271.505</gml:lowerCorner>
          <gml:upperCorner>565847.970 5936397.304</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_23DAC4BF-EFE6-485C-A136-D94B277C738C"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_0AE2E12F-B57C-4E64-8AA3-124E79FBF519'-->
        <gml:Polygon gml:id="Gml_0AE2E12F-B57C-4E64-8AA3-124E79FBF519" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565847.970 5936317.122 565842.417 5936317.176 565813.764 5936395.565 565751.471 5936397.304 565749.776 5936304.619 565822.064 5936303.315 565821.760 5936271.647 565836.468 5936271.505 565836.801 5936306.234 565847.798 5936306.128 565847.970 5936317.122</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_23DAC4BF-EFE6-485C-A136-D94B277C738C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565790.969 5936354.583</gml:lowerCorner>
          <gml:upperCorner>565790.969 5936354.583</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_1D95AC59-79F5-4EF5-BC65-ACBE31F3AFF7"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_4AAA3077-9383-4A7B-8C73-5D72B15D0B26'-->
        <gml:Point gml:id="Gml_4AAA3077-9383-4A7B-8C73-5D72B15D0B26" srsName="EPSG:25832">
          <gml:pos>565790.969 5936354.583</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_547B218F-E7E7-409E-833A-0BBB121413B7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565764.895 5936233.636</gml:lowerCorner>
          <gml:upperCorner>565814.350 5936255.377</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_77780681-B261-45C7-BDA5-F953D4FA7451'-->
        <gml:Polygon gml:id="Gml_77780681-B261-45C7-BDA5-F953D4FA7451" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565807.302 5936253.284 565805.611 5936255.036 565787.962 5936255.377 565786.096 5936253.713 565786.066 5936252.161 565771.250 5936252.448 565771.270 5936253.473 565769.622 5936255.262 565767.056 5936255.311 565764.895 5936252.662 565764.937 5936237.195 565766.800 5936235.020 565769.267 5936234.972 565771.167 5936236.894 565771.184 5936237.792 565785.823 5936237.509 565785.790 5936235.807 565787.365 5936233.980 565805.136 5936233.636 565806.988 5936235.327 565807.030 5936237.537 565809.625 5936237.487 565810.581 5936237.925 565813.762 5936240.908 565814.350 5936241.773 565814.239 5936246.303 565813.937 5936247.378 565810.719 5936250.724 565809.841 5936251.277 565807.264 5936251.328 565807.302 5936253.284</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_0D5A6A40-750C-461A-9B90-BBCB5B69BB0F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565780.050 5936264.737</gml:lowerCorner>
          <gml:upperCorner>565809.207 5936283.252</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_52A35943-657C-4B0C-B694-FDBCD761E949'-->
        <gml:Polygon gml:id="Gml_52A35943-657C-4B0C-B694-FDBCD761E949" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565809.207 5936282.729 565780.222 5936283.252 565780.050 5936265.260 565809.034 5936264.737 565809.207 5936282.729</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_059996D9-526B-4C71-9D54-F93FA23EE994">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565554.800 5936288.895</gml:lowerCorner>
          <gml:upperCorner>565566.432 5936368.235</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_1A296033-9CD0-4FEA-9B7C-80598ACE71B9'-->
        <gml:LineString gml:id="Gml_1A296033-9CD0-4FEA-9B7C-80598ACE71B9" srsName="EPSG:25832">
          <gml:posList>565566.432 5936368.040 565564.894 5936288.899 565564.894 5936288.895 565554.800 5936289.091 565554.800 5936289.096 565556.338 5936368.235 565566.432 5936368.040</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ADCAA559-FB8F-4E7F-9B81-453F49E314FD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565569.891 5936288.605</gml:lowerCorner>
          <gml:upperCorner>565581.524 5936367.943</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_C9C3BEB1-60D0-40C1-8F1E-22E7FD691500'-->
        <gml:LineString gml:id="Gml_C9C3BEB1-60D0-40C1-8F1E-22E7FD691500" srsName="EPSG:25832">
          <gml:posList>565581.524 5936367.748 565579.986 5936288.605 565569.891 5936288.798 565569.891 5936288.802 565571.429 5936367.943 565581.524 5936367.748</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_CE6A3F46-FBB7-4943-8218-5938C3002F2B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565608.719 5936287.851</gml:lowerCorner>
          <gml:upperCorner>565620.108 5936354.548</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_FCBB9183-6D37-4D2A-A548-652664A02705'-->
        <gml:LineString gml:id="Gml_FCBB9183-6D37-4D2A-A548-652664A02705" srsName="EPSG:25832">
          <gml:posList>565615.510 5936354.548 565620.108 5936354.459 565618.814 5936287.851 565608.719 5936288.048 565609.935 5936350.609 565615.432 5936350.502 565615.510 5936354.548</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_F1A8E06B-079F-40EE-8686-7A39386FCBFF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565623.811 5936287.560</gml:lowerCorner>
          <gml:upperCorner>565635.121 5936354.362</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_0D3CB108-6F5F-46DA-B2D9-D823F04673D3'-->
        <gml:LineString gml:id="Gml_0D3CB108-6F5F-46DA-B2D9-D823F04673D3" srsName="EPSG:25832">
          <gml:posList>565629.702 5936354.273 565629.624 5936350.226 565635.121 5936350.119 565633.905 5936287.560 565623.811 5936287.754 565625.105 5936354.362 565629.702 5936354.273</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_021091DC-B56B-4819-93FF-E77B7B410A1C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565568.865 5936261.839</gml:lowerCorner>
          <gml:upperCorner>565619.316 5936283.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_FF4EDF2C-DC70-4B5D-94A9-1AA54BCE0EE3'-->
        <gml:LineString gml:id="Gml_FF4EDF2C-DC70-4B5D-94A9-1AA54BCE0EE3" srsName="EPSG:25832">
          <gml:posList>565577.699 5936276.426 565585.645 5936276.271 565585.785 5936283.494 565602.725 5936283.165 565602.585 5936275.942 565610.530 5936275.787 565610.671 5936283.010 565619.316 5936282.842 565618.987 5936265.902 565614.664 5936265.986 565614.584 5936261.839 565597.993 5936262.161 565598.074 5936266.309 565589.778 5936266.470 565589.698 5936262.323 565573.107 5936262.645 565573.188 5936266.793 565568.865 5936266.877 565569.194 5936283.817 565577.840 5936283.649 565577.699 5936276.426</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_56A5E24C-47FC-460A-967A-A286865C9FB2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565622.785 5936260.790</gml:lowerCorner>
          <gml:upperCorner>565673.235 5936282.768</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_D28068F0-1020-4ED4-B449-C92A64319AAC'-->
        <gml:LineString gml:id="Gml_D28068F0-1020-4ED4-B449-C92A64319AAC" srsName="EPSG:25832">
          <gml:posList>565627.107 5936265.744 565622.785 5936265.828 565623.114 5936282.768 565631.759 5936282.600 565631.618 5936275.377 565639.564 5936275.223 565639.704 5936282.445 565656.645 5936282.116 565656.504 5936274.893 565658.685 5936274.851 565664.450 5936274.739 565664.590 5936281.961 565673.235 5936281.793 565672.906 5936264.853 565668.584 5936264.937 565668.503 5936260.790 565654.207 5936261.068 565651.912 5936261.113 565651.993 5936265.260 565643.698 5936265.422 565643.617 5936261.274 565627.027 5936261.597 565627.107 5936265.744</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_A0094ADA-2CAD-46D7-BBE0-9B11361DE8B0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565539.832 5936262.888</gml:lowerCorner>
          <gml:upperCorner>565565.376 5936284.382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_A7EC25F0-61F2-4764-A390-86201F8B89C4'-->
        <gml:LineString gml:id="Gml_A7EC25F0-61F2-4764-A390-86201F8B89C4" srsName="EPSG:25832">
          <gml:posList>565544.154 5936267.358 565539.832 5936267.442 565540.161 5936284.382 565548.806 5936284.213 565548.666 5936276.991 565556.611 5936276.836 565556.752 5936284.059 565565.376 5936283.891 565565.048 5936266.951 565560.745 5936267.035 565560.664 5936262.888 565544.074 5936263.210 565544.154 5936267.358</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_0D3A70B7-04A7-4014-93A4-C1202B2138E4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565526.488 5936377.919</gml:lowerCorner>
          <gml:upperCorner>565615.631 5936422.112</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_39F99E1E-304D-4462-ACFD-EE44B47E3120'-->
        <gml:LineString gml:id="Gml_39F99E1E-304D-4462-ACFD-EE44B47E3120" srsName="EPSG:25832">
          <gml:posList>565589.228 5936396.111 565572.454 5936395.075 565565.048 5936394.618 565526.502 5936392.237 565526.488 5936392.236 565526.872 5936416.188 565530.095 5936416.392 565530.055 5936417.040 565554.068 5936418.532 565592.954 5936420.947 565611.704 5936422.112 565614.946 5936415.685 565611.705 5936415.483 565612.393 5936404.509 565615.631 5936404.711 565613.097 5936397.605 565591.739 5936396.281 565591.721 5936396.280 565592.178 5936389.183 565593.536 5936389.270 565595.171 5936389.376 565595.557 5936383.390 565594.531 5936383.324 565594.525 5936383.134 565594.512 5936382.945 565594.492 5936382.756 565594.466 5936382.568 565594.433 5936382.380 565594.394 5936382.195 565594.349 5936382.010 565594.297 5936381.827 565594.239 5936381.646 565594.175 5936381.468 565594.104 5936381.291 565594.028 5936381.117 565593.945 5936380.946 565593.857 5936380.778 565593.763 5936380.613 565593.663 5936380.451 565593.558 5936380.293 565593.447 5936380.138 565593.331 5936379.988 565593.210 5936379.841 565593.084 5936379.699 565592.953 5936379.561 565592.818 5936379.428 565592.678 5936379.300 565592.533 5936379.177 565592.385 5936379.058 565592.232 5936378.945 565592.076 5936378.837 565591.916 5936378.735 565591.752 5936378.638 565591.585 5936378.547 565591.416 5936378.462 565591.243 5936378.382 565591.068 5936378.309 565590.890 5936378.242 565590.710 5936378.181 565590.528 5936378.126 565590.344 5936378.077 565590.159 5936378.035 565589.973 5936378.000 565589.785 5936377.970 565589.596 5936377.948 565589.407 5936377.932 565589.217 5936377.922 565589.027 5936377.919 565588.837 5936377.923 565588.647 5936377.933 565588.458 5936377.949 565588.270 5936377.972 565588.082 5936378.002 565587.895 5936378.038 565587.710 5936378.081 565587.527 5936378.130 565587.345 5936378.185 565587.165 5936378.246 565586.988 5936378.314 565586.812 5936378.388 565586.640 5936378.468 565586.470 5936378.553 565586.304 5936378.645 565586.141 5936378.742 565585.981 5936378.845 565585.825 5936378.953 565585.672 5936379.066 565585.524 5936379.185 565585.380 5936379.309 565585.240 5936379.438 565585.105 5936379.571 565584.974 5936379.709 565584.849 5936379.852 565584.728 5936379.998 565584.612 5936380.149 565584.502 5936380.304 565584.397 5936380.463 565584.298 5936380.624 565584.204 5936380.790 565584.117 5936380.958 565584.034 5936381.131 565583.958 5936381.306 565583.887 5936381.483 565583.823 5936381.663 565583.765 5936381.845 565583.714 5936382.030 565583.669 5936382.215 565583.630 5936382.402 565583.598 5936382.591 565583.573 5936382.780 565583.554 5936382.970 565583.542 5936383.161 565583.537 5936383.352 565583.537 5936383.543 565583.545 5936383.734 565583.559 5936383.924 565583.581 5936384.114 565583.608 5936384.303 565583.642 5936384.491 565583.683 5936384.678 565583.730 5936384.863 565583.783 5936385.047 565583.843 5936385.228 565583.909 5936385.407 565583.981 5936385.584 565584.060 5936385.759 565584.144 5936385.930 565584.234 5936386.098 565584.330 5936386.264 565584.432 5936386.425 565584.539 5936386.583 565584.652 5936386.738 565584.770 5936386.888 565584.893 5936387.034 565585.022 5936387.175 565585.155 5936387.313 565585.292 5936387.445 565585.435 5936387.573 565585.581 5936387.695 565585.732 5936387.813 565585.887 5936387.925 565586.045 5936388.031 565586.207 5936388.132 565586.373 5936388.228 565586.542 5936388.317 565586.713 5936388.401 565586.888 5936388.479 565587.065 5936388.550 565587.245 5936388.615 565587.426 5936388.674 565587.610 5936388.727 565587.796 5936388.773 565587.982 5936388.813 565588.171 5936388.846 565588.360 5936388.873 565588.550 5936388.893 565588.740 5936388.907 565588.931 5936388.914 565589.122 5936388.914 565589.313 5936388.908 565589.504 5936388.895 565589.694 5936388.875 565589.228 5936396.111</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_A39EBEB2-0ECB-4CEC-814D-75668D93C4BC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565498.433 5936326.847</gml:lowerCorner>
          <gml:upperCorner>565526.872 5936416.188</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_BD63101E-1174-4663-A3D2-630DABB76035'-->
        <gml:LineString gml:id="Gml_BD63101E-1174-4663-A3D2-630DABB76035" srsName="EPSG:25832">
          <gml:posList>565500.127 5936414.497 565526.872 5936416.188 565526.488 5936392.236 565526.378 5936385.378 565522.059 5936385.462 565520.920 5936326.847 565498.433 5936327.284 565498.675 5936339.726 565500.127 5936414.497</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_E79D3549-2E78-4930-AD42-27FCA1FBC3C1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565520.920 5936326.584</gml:lowerCorner>
          <gml:upperCorner>565535.554 5936385.462</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_CCD751D0-E6FA-4DC1-A214-690C6231C61F'-->
        <gml:LineString gml:id="Gml_CCD751D0-E6FA-4DC1-A214-690C6231C61F" srsName="EPSG:25832">
          <gml:posList>565520.920 5936326.847 565522.059 5936385.462 565526.378 5936385.378 565526.374 5936385.106 565529.739 5936384.989 565535.554 5936385.323 565535.551 5936385.200 565534.413 5936326.584 565520.920 5936326.847</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_9F39F0CC-6E16-4CFF-B25F-A828F231224A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565534.413 5936326.545</gml:lowerCorner>
          <gml:upperCorner>565537.556 5936385.438</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_2F8EC2BF-4786-4338-A982-ADF3E5D2D44A'-->
        <gml:LineString gml:id="Gml_2F8EC2BF-4786-4338-A982-ADF3E5D2D44A" srsName="EPSG:25832">
          <gml:posList>565536.412 5936326.545 565534.413 5936326.584 565535.551 5936385.200 565535.554 5936385.323 565536.387 5936385.371 565537.556 5936385.438 565537.550 5936385.161 565536.412 5936326.545</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_76AC43C8-39AB-47A9-8AD7-49069D6514F9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565525.599 5936284.470</gml:lowerCorner>
          <gml:upperCorner>565536.307 5936321.343</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_FBDB5DEC-E277-4C92-AA2C-4132FB4F6F1C'-->
        <gml:LineString gml:id="Gml_FBDB5DEC-E277-4C92-AA2C-4132FB4F6F1C" srsName="EPSG:25832">
          <gml:posList>565536.307 5936321.149 565535.594 5936284.470 565525.599 5936284.665 565526.312 5936321.343 565536.307 5936321.149</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_4C487667-82F3-41E5-9D25-382DC0BFD729">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565497.205 5936263.383</gml:lowerCorner>
          <gml:upperCorner>565535.594 5936321.887</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_06665D3E-06C4-487B-AC99-EE5A7F65CCC0'-->
        <gml:LineString gml:id="Gml_06665D3E-06C4-487B-AC99-EE5A7F65CCC0" srsName="EPSG:25832">
          <gml:posList>565498.328 5936321.887 565526.312 5936321.343 565525.599 5936284.665 565535.594 5936284.470 565535.184 5936263.383 565497.205 5936264.122 565498.328 5936321.887</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_82F1B925-7994-4273-ABA6-18FA85BB3CA9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565535.184 5936263.294</gml:lowerCorner>
          <gml:upperCorner>565540.147 5936284.470</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_A9CA6C1E-67D1-41B3-84DA-0915F5980157'-->
        <gml:LineString gml:id="Gml_A9CA6C1E-67D1-41B3-84DA-0915F5980157" srsName="EPSG:25832">
          <gml:posList>565539.739 5936263.294 565535.184 5936263.383 565535.594 5936284.470 565540.147 5936284.382 565539.739 5936263.294</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_76951865-48FA-4B11-836F-303B0DAC57E1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565625.560 5936259.306</gml:lowerCorner>
          <gml:upperCorner>565739.405 5936422.224</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_FFBDE32A-D084-4F9D-96F4-192374B8292F'-->
        <gml:LineString gml:id="Gml_FFBDE32A-D084-4F9D-96F4-192374B8292F" srsName="EPSG:25832">
          <gml:posList>565739.283 5936422.224 565739.405 5936259.306 565682.174 5936260.376 565684.345 5936367.893 565628.874 5936365.042 565625.560 5936416.455 565739.283 5936422.224</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_7BD970BB-64C1-4E15-AF23-EFECFE40D852">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565749.776 5936271.505</gml:lowerCorner>
          <gml:upperCorner>565847.970 5936397.304</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_5BF6F2F7-00A0-41A8-97C9-06A945FDA102'-->
        <gml:LineString gml:id="Gml_5BF6F2F7-00A0-41A8-97C9-06A945FDA102" srsName="EPSG:25832">
          <gml:posList>565813.764 5936395.565 565842.417 5936317.176 565847.970 5936317.122 565847.798 5936306.128 565836.801 5936306.234 565836.468 5936271.505 565821.760 5936271.647 565822.064 5936303.315 565749.776 5936304.619 565751.471 5936397.304 565813.764 5936395.565</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_AD0AF193-5FDB-4598-A1D2-00B7C4922208">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565764.895 5936233.636</gml:lowerCorner>
          <gml:upperCorner>565814.350 5936255.377</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_BCA56F8A-8854-4096-AF2E-9304BB35BB52'-->
        <gml:LineString gml:id="Gml_BCA56F8A-8854-4096-AF2E-9304BB35BB52" srsName="EPSG:25832">
          <gml:posList>565814.350 5936241.773 565813.762 5936240.908 565810.581 5936237.925 565809.625 5936237.487 565807.030 5936237.537 565806.988 5936235.327 565805.136 5936233.636 565787.365 5936233.980 565785.790 5936235.807 565785.823 5936237.509 565771.184 5936237.792 565771.167 5936236.894 565769.267 5936234.972 565766.800 5936235.020 565764.937 5936237.195 565764.895 5936252.662 565767.056 5936255.311 565769.622 5936255.262 565771.270 5936253.473 565771.250 5936252.448 565786.066 5936252.161 565786.096 5936253.713 565787.962 5936255.377 565805.611 5936255.036 565807.302 5936253.284 565807.264 5936251.328 565809.841 5936251.277 565810.719 5936250.724 565813.937 5936247.378 565814.239 5936246.303 565814.350 5936241.773</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_CF042D07-472B-49BC-A120-212ED26ADF73">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565780.050 5936264.737</gml:lowerCorner>
          <gml:upperCorner>565809.207 5936283.252</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_E2C22643-524E-4DA5-9EA0-0440C62F9019'-->
        <gml:LineString gml:id="Gml_E2C22643-524E-4DA5-9EA0-0440C62F9019" srsName="EPSG:25832">
          <gml:posList>565809.207 5936282.729 565809.034 5936264.737 565780.050 5936265.260 565780.222 5936283.252 565809.207 5936282.729</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_36729A2E-E5E8-4B7D-9F46-4049D57032CB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565495.758 5936216.181</gml:lowerCorner>
          <gml:upperCorner>565884.054 5936430.892</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_CD056D5E-4ABC-4F3F-BADA-416B3EB004D2'-->
        <gml:Curve gml:id="Gml_CD056D5E-4ABC-4F3F-BADA-416B3EB004D2" srsName="EPSG:25832">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565749.244 5936429.667 565746.957 5936430.892</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565746.957 5936430.892 565743.464 5936430.681</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565743.464 5936430.681 565739.463 5936430.438</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565739.463 5936430.438 565739.277 5936430.426</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565739.277 5936430.426 565618.142 5936423.059</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565618.142 5936423.059 565526.893 5936417.517</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565526.893 5936417.517 565526.919 5936419.184</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565526.919 5936419.184 565498.866 5936419.727</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565498.866 5936419.727 565498.842 5936418.466</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565498.842 5936418.466 565498.213 5936386.075</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565498.213 5936386.075 565495.758 5936259.987</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565495.758 5936259.987 565533.936 5936259.247</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565533.936 5936259.247 565539.658 5936259.136</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565539.658 5936259.136 565564.887 5936258.648</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565564.887 5936258.648 565568.684 5936258.574</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565568.684 5936258.574 565654.126 5936256.916</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565654.126 5936256.916 565674.723 5936256.517</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565674.723 5936256.517 565674.744 5936256.517</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565674.744 5936256.517 565740.084 5936255.249</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565740.084 5936255.249 565740.297 5936266.246</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565740.297 5936266.246 565763.282 5936265.801</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565763.282 5936265.801 565763.113 5936257.029</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565763.113 5936257.029 565762.657 5936233.472</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565762.657 5936233.472 565762.569 5936228.920</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565762.569 5936228.920 565772.960 5936218.116</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565772.960 5936218.116 565819.685 5936217.216</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565819.685 5936217.216 565820.565 5936217.199</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565820.565 5936217.199 565853.936 5936216.556</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565853.936 5936216.556 565873.238 5936216.183</gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList>565873.238 5936216.183 565877.913 5936217.036 565881.890 5936219.636</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565881.890 5936219.636 565883.214 5936221.686</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565883.214 5936221.686 565883.818 5936223.337</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565883.818 5936223.337 565884.054 5936226.489</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565884.054 5936226.489 565883.866 5936229.971</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565883.866 5936229.971 565874.620 5936265.427</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565874.620 5936265.427 565861.203 5936303.622</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565861.203 5936303.622 565852.669 5936327.918</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565852.669 5936327.918 565844.986 5936347.825</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565844.986 5936347.825 565825.060 5936402.202</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565825.060 5936402.202 565824.603 5936403.452</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565824.603 5936403.452 565821.089 5936413.043</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565821.089 5936413.043 565778.149 5936414.176</gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList>565778.149 5936414.176 565749.244 5936429.667</gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_5981E4D7-EFE7-4080-80E9-978E29EC0246">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565618.142 5936282.285</gml:lowerCorner>
          <gml:upperCorner>565681.566 5936423.483</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_BF17D42E-6269-4DF5-A623-4BBC01135F4D"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_E7B18656-8F6E-4BF7-8877-3C27A10A18F4"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_7AB93016-9A44-4A18-AF3A-EAC274C84884'-->
        <gml:Polygon gml:id="Gml_7AB93016-9A44-4A18-AF3A-EAC274C84884" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565680.852 5936283.952 565681.566 5936325.148 565679.592 5936329.676 565628.708 5936367.613 565627.058 5936393.217 565625.560 5936416.455 565625.107 5936423.483 565618.142 5936423.059 565618.426 5936396.523 565620.299 5936387.537 565621.333 5936370.504 565619.254 5936364.224 565675.970 5936320.922 565674.343 5936286.254 565676.248 5936282.285 565680.852 5936283.952</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
      <xplan:gegenstand>2000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_BF17D42E-6269-4DF5-A623-4BBC01135F4D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565665.710 5936334.390</gml:lowerCorner>
          <gml:upperCorner>565665.710 5936334.390</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>massnahme</xplan:art>
      <xplan:art>gegenstand</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_5981E4D7-EFE7-4080-80E9-978E29EC0246"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_24AEE4F3-9679-46BD-BE73-B92C83CB2709'-->
        <gml:Point gml:id="Gml_24AEE4F3-9679-46BD-BE73-B92C83CB2709" srsName="EPSG:25832">
          <gml:pos>565665.710 5936334.390</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_E5D41C1E-512A-444C-8B7E-A7E00AE7B917">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565576.051 5936382.867</gml:lowerCorner>
          <gml:upperCorner>565576.051 5936382.867</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_C026DDC2-74C2-4BE3-A16D-1C2B09EEDEBE'-->
        <gml:Point gml:id="Gml_C026DDC2-74C2-4BE3-A16D-1C2B09EEDEBE" srsName="EPSG:25832">
          <gml:pos>565576.051 5936382.867</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_13C5028C-16AC-4A35-B710-AFCFA20308F3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565745.787 5936292.594</gml:lowerCorner>
          <gml:upperCorner>565745.787 5936292.594</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_0A8A7DE0-84A5-475B-B41F-3BC736DD7D83'-->
        <gml:Point gml:id="Gml_0A8A7DE0-84A5-475B-B41F-3BC736DD7D83" srsName="EPSG:25832">
          <gml:pos>565745.787 5936292.594</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_52F62D9D-A2E7-4417-9B16-8B267BF7A1B1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565746.133 5936300.686</gml:lowerCorner>
          <gml:upperCorner>565746.133 5936300.686</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_9FA6C47E-2006-4649-9381-F006D809238E'-->
        <gml:Point gml:id="Gml_9FA6C47E-2006-4649-9381-F006D809238E" srsName="EPSG:25832">
          <gml:pos>565746.133 5936300.686</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_D9546777-8D4A-4620-83DA-2300781C527C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565746.285 5936309.131</gml:lowerCorner>
          <gml:upperCorner>565746.285 5936309.131</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_9A1D67EC-80B8-4B02-A7F9-7C962D441840'-->
        <gml:Point gml:id="Gml_9A1D67EC-80B8-4B02-A7F9-7C962D441840" srsName="EPSG:25832">
          <gml:pos>565746.285 5936309.131</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_A22C0643-3518-4891-844E-84A5E50DDEE4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565746.441 5936317.790</gml:lowerCorner>
          <gml:upperCorner>565746.441 5936317.790</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_FCF15605-0836-4C79-8CC1-3FA8BB492849'-->
        <gml:Point gml:id="Gml_FCF15605-0836-4C79-8CC1-3FA8BB492849" srsName="EPSG:25832">
          <gml:pos>565746.441 5936317.790</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_CEAEB980-6ADE-4C40-9703-E552C2EF4C2B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565746.487 5936325.879</gml:lowerCorner>
          <gml:upperCorner>565746.487 5936325.879</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_401C0570-F85A-4391-8734-EB8815C751AE'-->
        <gml:Point gml:id="Gml_401C0570-F85A-4391-8734-EB8815C751AE" srsName="EPSG:25832">
          <gml:pos>565746.487 5936325.879</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_05CA4B4E-A5BA-48C4-A7AD-DB7A54583D63">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565790.336 5936288.743</gml:lowerCorner>
          <gml:upperCorner>565790.336 5936288.743</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_62B7FD38-C027-42DB-9003-A0FE20819DF4'-->
        <gml:Point gml:id="Gml_62B7FD38-C027-42DB-9003-A0FE20819DF4" srsName="EPSG:25832">
          <gml:pos>565790.336 5936288.743</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_A21447B3-E656-4088-B34F-BB78F95D316A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565814.028 5936288.865</gml:lowerCorner>
          <gml:upperCorner>565814.028 5936288.865</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_48BFD00D-2F4B-47C9-B2EE-4837B0D1655A'-->
        <gml:Point gml:id="Gml_48BFD00D-2F4B-47C9-B2EE-4837B0D1655A" srsName="EPSG:25832">
          <gml:pos>565814.028 5936288.865</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_942EC9DD-CD6E-4BAE-9073-2067F8E24AD7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565812.987 5936274.080</gml:lowerCorner>
          <gml:upperCorner>565812.987 5936274.080</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_F2E9A610-579A-4781-8588-DF24BFE6DD0A'-->
        <gml:Point gml:id="Gml_F2E9A610-579A-4781-8588-DF24BFE6DD0A" srsName="EPSG:25832">
          <gml:pos>565812.987 5936274.080</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_FC95B2D3-27F0-41DE-8346-3BAAF90BE0D7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565800.740 5936268.800</gml:lowerCorner>
          <gml:upperCorner>565800.740 5936268.800</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_D467FBAB-D9D7-435F-8F1B-A979F432CC7E'-->
        <gml:Point gml:id="Gml_D467FBAB-D9D7-435F-8F1B-A979F432CC7E" srsName="EPSG:25832">
          <gml:pos>565800.740 5936268.800</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_957544E5-F206-4B02-B439-7F05901FEE28">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565777.247 5936268.676</gml:lowerCorner>
          <gml:upperCorner>565777.247 5936268.676</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_26859DAF-BE12-4A1F-BAB8-728CFA1CAF6F'-->
        <gml:Point gml:id="Gml_26859DAF-BE12-4A1F-BAB8-728CFA1CAF6F" srsName="EPSG:25832">
          <gml:pos>565777.247 5936268.676</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_FE685129-3891-4BB3-90CC-C79E636C48AA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565832.966 5936262.292</gml:lowerCorner>
          <gml:upperCorner>565832.966 5936262.292</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_7F18A1D9-0E96-4CC6-AF4D-EBAC46437CAA'-->
        <gml:Point gml:id="Gml_7F18A1D9-0E96-4CC6-AF4D-EBAC46437CAA" srsName="EPSG:25832">
          <gml:pos>565832.966 5936262.292</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_4676DAF7-227C-4E16-A65B-05078E93F9C9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565825.953 5936260.660</gml:lowerCorner>
          <gml:upperCorner>565825.953 5936260.660</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_C2ACCED4-D972-4DB9-9967-0A39C91C942D'-->
        <gml:Point gml:id="Gml_C2ACCED4-D972-4DB9-9967-0A39C91C942D" srsName="EPSG:25832">
          <gml:pos>565825.953 5936260.660</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_972C8B8F-C212-4BFA-B5B0-6A586D4429F4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565820.211 5936252.352</gml:lowerCorner>
          <gml:upperCorner>565820.211 5936252.352</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_7EA067B7-347D-4787-99EC-485AC6668E22'-->
        <gml:Point gml:id="Gml_7EA067B7-347D-4787-99EC-485AC6668E22" srsName="EPSG:25832">
          <gml:pos>565820.211 5936252.352</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_CE0D5A54-484D-4BE4-867E-9300203D675C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565837.271 5936238.417</gml:lowerCorner>
          <gml:upperCorner>565837.271 5936238.417</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_59A81137-59C1-4737-89B4-9EE21821436D'-->
        <gml:Point gml:id="Gml_59A81137-59C1-4737-89B4-9EE21821436D" srsName="EPSG:25832">
          <gml:pos>565837.271 5936238.417</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_7AFBD5D8-B895-4C8E-AE17-21F064D4737E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565834.490 5936229.872</gml:lowerCorner>
          <gml:upperCorner>565834.490 5936229.872</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_88ED1913-B808-4E14-8DFE-766F09EADB1F'-->
        <gml:Point gml:id="Gml_88ED1913-B808-4E14-8DFE-766F09EADB1F" srsName="EPSG:25832">
          <gml:pos>565834.490 5936229.872</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_F283274E-6473-4739-A41F-4BA04BF51657">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565757.728 5936233.935</gml:lowerCorner>
          <gml:upperCorner>565757.728 5936233.935</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_F3143018-6A5A-4E2E-ACBF-BE7DBE0A7903'-->
        <gml:Point gml:id="Gml_F3143018-6A5A-4E2E-ACBF-BE7DBE0A7903" srsName="EPSG:25832">
          <gml:pos>565757.728 5936233.935</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_42E7A7A1-715C-4D73-BF6E-733CC2058EC8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565758.581 5936241.181</gml:lowerCorner>
          <gml:upperCorner>565758.581 5936241.181</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_7E62ED29-F499-4ACD-9825-774C68028DD4'-->
        <gml:Point gml:id="Gml_7E62ED29-F499-4ACD-9825-774C68028DD4" srsName="EPSG:25832">
          <gml:pos>565758.581 5936241.181</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_5DC25FB5-D4A8-4B5F-B6FE-4DB0462E6EB0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565758.021 5936249.334</gml:lowerCorner>
          <gml:upperCorner>565758.021 5936249.334</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_A5F77DD0-36C6-4769-8C9F-D847E46A7D1E'-->
        <gml:Point gml:id="Gml_A5F77DD0-36C6-4769-8C9F-D847E46A7D1E" srsName="EPSG:25832">
          <gml:pos>565758.021 5936249.334</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_68485B95-D68E-4CFF-8CEC-2180B743D42A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565758.371 5936257.862</gml:lowerCorner>
          <gml:upperCorner>565758.371 5936257.862</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_F8292E28-5006-41E5-B4F6-8715461DD5BF'-->
        <gml:Point gml:id="Gml_F8292E28-5006-41E5-B4F6-8715461DD5BF" srsName="EPSG:25832">
          <gml:pos>565758.371 5936257.862</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_43AC2A8C-8F3D-4AD5-8C35-2F63BB145F3C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565745.505 5936274.867</gml:lowerCorner>
          <gml:upperCorner>565745.505 5936274.867</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_488E77FA-0E7A-4760-81E4-F06485F3EAA9'-->
        <gml:Point gml:id="Gml_488E77FA-0E7A-4760-81E4-F06485F3EAA9" srsName="EPSG:25832">
          <gml:pos>565745.505 5936274.867</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_B9577D78-E86F-476D-9858-928139D8D626">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565785.915 5936225.301</gml:lowerCorner>
          <gml:upperCorner>565785.915 5936225.301</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_17A9EB33-6E95-4A34-B951-58324F20F212'-->
        <gml:Point gml:id="Gml_17A9EB33-6E95-4A34-B951-58324F20F212" srsName="EPSG:25832">
          <gml:pos>565785.915 5936225.301</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_FC440BE7-E152-4858-A895-1D0041405362">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565764.120 5936221.466</gml:lowerCorner>
          <gml:upperCorner>565764.120 5936221.466</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_25003A42-F198-4EAF-A0F3-11105E8001DF'-->
        <gml:Point gml:id="Gml_25003A42-F198-4EAF-A0F3-11105E8001DF" srsName="EPSG:25832">
          <gml:pos>565764.120 5936221.466</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_68A0E440-DEDF-4FD3-AEE5-8E16F371B2AB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565822.443 5936218.179</gml:lowerCorner>
          <gml:upperCorner>565822.443 5936218.179</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_5728FD0A-6131-4B5E-B016-55CEC78C1304'-->
        <gml:Point gml:id="Gml_5728FD0A-6131-4B5E-B016-55CEC78C1304" srsName="EPSG:25832">
          <gml:pos>565822.443 5936218.179</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_766C86AE-AE24-47E1-8063-6E4D3C9561FD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565829.384 5936223.463</gml:lowerCorner>
          <gml:upperCorner>565829.384 5936223.463</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_2E82025A-A816-46FC-A8EF-4FFBCB991BBA'-->
        <gml:Point gml:id="Gml_2E82025A-A816-46FC-A8EF-4FFBCB991BBA" srsName="EPSG:25832">
          <gml:pos>565829.384 5936223.463</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_047F1DC7-895B-4E82-BD80-B7AA4F8ECECC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565807.732 5936214.320</gml:lowerCorner>
          <gml:upperCorner>565807.732 5936214.320</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_90FC5463-048C-4510-BE54-56D7B3801291'-->
        <gml:Point gml:id="Gml_90FC5463-048C-4510-BE54-56D7B3801291" srsName="EPSG:25832">
          <gml:pos>565807.732 5936214.320</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_D6A53EE3-0F0B-4BC4-8D6B-C7A0F5FA8FFD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565815.645 5936215.361</gml:lowerCorner>
          <gml:upperCorner>565815.645 5936215.361</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_F6C3B417-638B-4C5E-8A3E-5998C9FD6D4C'-->
        <gml:Point gml:id="Gml_F6C3B417-638B-4C5E-8A3E-5998C9FD6D4C" srsName="EPSG:25832">
          <gml:pos>565815.645 5936215.361</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_48791A9B-3B7E-4C42-869F-ABB5B32F1C9D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565810.754 5936220.845</gml:lowerCorner>
          <gml:upperCorner>565810.754 5936220.845</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_DCB2BEF2-601B-4CF5-9539-669946E46229'-->
        <gml:Point gml:id="Gml_DCB2BEF2-601B-4CF5-9539-669946E46229" srsName="EPSG:25832">
          <gml:pos>565810.754 5936220.845</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_CD14AF17-99A6-4C20-AB1E-FAECDDC195BF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565791.594 5936214.448</gml:lowerCorner>
          <gml:upperCorner>565791.594 5936214.448</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_6D95B1E1-9674-4D71-A25E-D7A40A25F3A2'-->
        <gml:Point gml:id="Gml_6D95B1E1-9674-4D71-A25E-D7A40A25F3A2" srsName="EPSG:25832">
          <gml:pos>565791.594 5936214.448</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_C8AE3BB0-8A87-4DB0-BFBC-4055BF8C3A30">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565776.256 5936214.816</gml:lowerCorner>
          <gml:upperCorner>565776.256 5936214.816</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_E4C97FB6-32FA-4B8D-AB7C-E29B0ED2BE76'-->
        <gml:Point gml:id="Gml_E4C97FB6-32FA-4B8D-AB7C-E29B0ED2BE76" srsName="EPSG:25832">
          <gml:pos>565776.256 5936214.816</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenFlaeche gml:id="GML_A8432A4A-B586-4CBA-A935-2CC7C99516D1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565498.255 5936257.362</gml:lowerCorner>
          <gml:upperCorner>565673.462 5936416.872</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_207C00E5-89B0-4142-8ED5-0552FC538868"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_C352801F-3C21-4E8A-A4C1-87E9790158F8'-->
        <gml:Polygon gml:id="Gml_C352801F-3C21-4E8A-A4C1-87E9790158F8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565581.524 5936367.748 565571.429 5936367.943 565571.439 5936368.437 565571.511 5936372.158 565571.659 5936379.750 565571.674 5936380.548 565551.143 5936380.947 565545.169 5936381.063 565545.217 5936383.504 565545.247 5936385.061 565542.451 5936385.115 565542.100 5936385.122 565539.650 5936385.170 565539.626 5936385.557 565539.501 5936387.576 565539.163 5936393.019 565537.682 5936416.872 565530.095 5936416.392 565526.872 5936416.188 565505.583 5936414.842 565505.378 5936404.310 565505.280 5936399.230 565505.191 5936394.665 565505.019 5936385.794 565503.980 5936332.348 565503.880 5936327.178 565503.775 5936321.781 565503.689 5936317.334 565499.291 5936317.419 565498.255 5936264.101 565513.967 5936263.796 565533.846 5936263.409 565533.936 5936259.247 565539.658 5936259.136 565539.739 5936263.294 565544.074 5936263.210 565556.148 5936262.975 565556.240 5936258.815 565564.887 5936258.648 565565.297 5936258.640 565568.294 5936258.582 565568.684 5936258.574 565577.300 5936258.407 565577.516 5936262.560 565582.963 5936262.454 565589.698 5936262.323 565597.993 5936262.161 565610.401 5936261.920 565610.281 5936257.767 565619.225 5936257.593 565622.223 5936257.535 565631.136 5936257.362 565631.242 5936261.515 565643.617 5936261.274 565651.912 5936261.113 565654.207 5936261.068 565668.503 5936260.790 565671.804 5936260.726 565672.826 5936260.706 565672.906 5936264.853 565673.116 5936275.671 565673.235 5936281.793 565673.243 5936282.185 565673.462 5936293.473 565672.438 5936293.491 565640.852 5936294.023 565641.160 5936309.475 565641.910 5936347.151 565635.064 5936347.212 565629.566 5936347.260 565629.624 5936350.226 565629.702 5936354.273 565629.748 5936356.611 565625.083 5936356.676 565625.083 5936358.475 565624.185 5936358.496 565620.409 5936358.584 565615.981 5936358.687 565616.080 5936367.602 565616.113 5936370.602 565616.299 5936387.351 565609.252 5936387.350 565608.988 5936370.735 565608.940 5936367.735 565608.931 5936367.219 565581.524 5936367.748</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>3200</xplan:zweckbestimmung>
    </xplan:BP_GemeinschaftsanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_207C00E5-89B0-4142-8ED5-0552FC538868">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565595.647 5936351.950</gml:lowerCorner>
          <gml:upperCorner>565595.647 5936351.950</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A8432A4A-B586-4CBA-A935-2CC7C99516D1"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_9E6B575C-90A4-4FF9-AF73-D4722A377A6B'-->
        <gml:Point gml:id="Gml_9E6B575C-90A4-4FF9-AF73-D4722A377A6B" srsName="EPSG:25832">
          <gml:pos>565595.647 5936351.950</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_C6057670-3B55-41E0-A0F9-D5677AE69AB3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565498.210 5936256.517</gml:lowerCorner>
          <gml:upperCorner>565676.066 5936397.111</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_E03FE3AF-56C9-42C6-93B3-08B3B1B3C9C0"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_058F9594-D6F6-43C1-989F-F9AB9EFBB385"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_68972A31-8EA8-40CB-95DD-78E29B90A21B'-->
        <gml:Polygon gml:id="Gml_68972A31-8EA8-40CB-95DD-78E29B90A21B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565541.870 5936385.686 565540.014 5936387.567 565526.417 5936387.793 565524.086 5936387.832 565504.801 5936394.806 565499.780 5936396.621 565498.427 5936397.111 565498.213 5936386.075 565498.210 5936385.926 565499.572 5936385.900 565522.059 5936385.462 565526.378 5936385.378 565526.374 5936385.106 565529.739 5936384.989 565535.554 5936385.323 565536.387 5936385.371 565537.556 5936385.438 565538.097 5936385.469 565541.645 5936381.778 565542.032 5936381.611 565567.467 5936370.636 565567.174 5936355.509 565566.289 5936309.819 565565.883 5936288.876 565565.422 5936265.118 565565.297 5936258.640 565568.294 5936258.582 565568.657 5936277.251 565568.882 5936288.818 565569.502 5936320.743 565570.359 5936364.837 565570.430 5936368.456 565572.614 5936368.415 565605.058 5936367.808 565616.420 5936367.595 565621.155 5936356.898 565620.620 5936329.363 565619.813 5936287.833 565619.752 5936284.718 565619.225 5936257.593 565622.223 5936257.535 565622.767 5936285.503 565622.811 5936287.775 565623.638 5936330.327 565624.246 5936361.628 565633.956 5936354.191 565666.959 5936328.912 565668.013 5936328.312 565670.722 5936326.226 565673.037 5936324.443 565672.220 5936282.208 565672.212 5936281.813 565673.235 5936281.793 565673.116 5936275.671 565672.906 5936264.853 565671.884 5936264.873 565671.724 5936256.575 565674.723 5936256.517 565674.744 5936256.517 565674.745 5936256.561 565674.724 5936256.562 565675.219 5936282.140 565675.451 5936294.112 565675.994 5936322.166 565676.066 5936325.884 565676.039 5936325.904 565673.109 5936328.139 565658.565 5936339.233 565627.057 5936363.265 565624.319 5936365.354 565621.516 5936367.491 565621.504 5936367.500 565621.363 5936367.608 565621.399 5936369.422 565621.333 5936370.504 565620.299 5936387.537 565617.304 5936387.351 565618.324 5936370.560 565617.471 5936370.576 565616.097 5936370.602 565574.497 5936371.380 565573.260 5936371.403 565570.510 5936372.590 565567.530 5936373.876 565564.747 5936375.077 565547.231 5936382.635 565543.242 5936384.357 565542.106 5936385.447 565541.870 5936385.686</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="Gml_E03FE3AF-56C9-42C6-93B3-08B3B1B3C9C0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565594.824 5936369.458</gml:lowerCorner>
          <gml:upperCorner>565594.824 5936369.458</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>typ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_C6057670-3B55-41E0-A0F9-D5677AE69AB3"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_64E3CC68-5CFC-408E-985B-0B94F17F92B9'-->
        <gml:Point gml:id="Gml_64E3CC68-5CFC-408E-985B-0B94F17F92B9" srsName="EPSG:25832">
          <gml:pos>565594.824 5936369.458</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_C111F100-CAB6-449C-B992-F77DC2940AF0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565609.252 5936387.350</gml:lowerCorner>
          <gml:upperCorner>565620.299 5936423.059</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:wirdDargestelltDurch xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_5FD36ACC-5957-438D-A71D-6CA3CC11D60B"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_9F307243-4605-44E6-BD6F-89F2E6B469ED"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_AC9EBE1F-DAF8-47FC-A73C-2FEA6927B60F'-->
        <gml:Polygon gml:id="Gml_AC9EBE1F-DAF8-47FC-A73C-2FEA6927B60F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565617.304 5936387.351 565620.299 5936387.537 565618.426 5936396.523 565618.142 5936423.059 565613.145 5936422.755 565613.183 5936419.179 565613.222 5936415.578 565613.339 5936404.568 565613.404 5936398.469 565613.424 5936396.646 565609.252 5936387.350 565617.304 5936387.351</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>5000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="Gml_5FD36ACC-5957-438D-A71D-6CA3CC11D60B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565615.157 5936390.929</gml:lowerCorner>
          <gml:upperCorner>565615.157 5936390.929</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>typ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:dientZurDarstellungVon xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_C111F100-CAB6-449C-B992-F77DC2940AF0"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_9021CBD5-E929-4CE8-A0A7-097457CD260B'-->
        <gml:Point gml:id="Gml_9021CBD5-E929-4CE8-A0A7-097457CD260B" srsName="EPSG:25832">
          <gml:pos>565615.157 5936390.929</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_CE70B1D2-0DB5-4429-BFD5-E0566C6F57AA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565612.393 5936397.605</gml:lowerCorner>
          <gml:upperCorner>565615.631 5936404.711</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hZwingend uom="m">6</xplan:hZwingend>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_2B84790C-961B-4B31-8BA8-4D75EA3DDD74'-->
        <gml:Polygon gml:id="Gml_2B84790C-961B-4B31-8BA8-4D75EA3DDD74" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565615.631 5936404.711 565612.393 5936404.509 565613.097 5936397.605 565615.631 5936404.711</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_1E975086-9368-4DB4-845A-DCA1E2A5A934">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565611.704 5936415.483</gml:lowerCorner>
          <gml:upperCorner>565614.946 5936422.112</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hZwingend uom="m">6</xplan:hZwingend>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_F420782C-986F-4ADB-B5A9-A02AEFA84ACD'-->
        <gml:Polygon gml:id="Gml_F420782C-986F-4ADB-B5A9-A02AEFA84ACD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565614.946 5936415.685 565611.704 5936422.112 565611.705 5936415.483 565614.946 5936415.685</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_94B40304-A770-413A-8034-4F477794FDC8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565499.572 5936385.378</gml:lowerCorner>
          <gml:upperCorner>565526.417 5936396.621</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:h uom="m">3.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_DABB82D0-ABDB-455A-BE12-DE54C50A9602'-->
        <gml:Polygon gml:id="Gml_DABB82D0-ABDB-455A-BE12-DE54C50A9602" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565526.417 5936387.793 565524.086 5936387.832 565499.780 5936396.621 565499.572 5936385.900 565522.059 5936385.462 565526.378 5936385.378 565526.417 5936387.793</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_545B1BE8-A51F-4B3D-B4FD-809B60749251">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565540.017 5936276.969</gml:lowerCorner>
          <gml:upperCorner>565548.666 5936276.991</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_70263A09-892A-485A-978C-44B447160DCA'-->
        <gml:LineString gml:id="Gml_70263A09-892A-485A-978C-44B447160DCA" srsName="EPSG:25832">
          <gml:posList>565548.666 5936276.991 565540.017 5936276.969</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_9642D7F8-A40D-41A8-818B-8CF945AECF2D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565556.611 5936276.620</gml:lowerCorner>
          <gml:upperCorner>565565.235 5936276.836</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_4AFA9D3B-0BD1-456E-A4DD-C73E5AA9F254'-->
        <gml:LineString gml:id="Gml_4AFA9D3B-0BD1-456E-A4DD-C73E5AA9F254" srsName="EPSG:25832">
          <gml:posList>565556.611 5936276.836 565565.235 5936276.620</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_8BE107D5-7116-411C-80B5-7227966E82A5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565569.052 5936276.426</gml:lowerCorner>
          <gml:upperCorner>565577.699 5936276.493</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_664CF4CE-DFDE-47BB-B165-992C6E360E25'-->
        <gml:LineString gml:id="Gml_664CF4CE-DFDE-47BB-B165-992C6E360E25" srsName="EPSG:25832">
          <gml:posList>565577.699 5936276.426 565569.052 5936276.493</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_13AF40B1-5C32-4ECF-AB76-7905A37254AF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565610.530 5936275.686</gml:lowerCorner>
          <gml:upperCorner>565619.177 5936275.787</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_26FEC05D-5D79-48EA-BCB1-A73D2C4AC4EF'-->
        <gml:LineString gml:id="Gml_26FEC05D-5D79-48EA-BCB1-A73D2C4AC4EF" srsName="EPSG:25832">
          <gml:posList>565610.530 5936275.787 565619.177 5936275.686</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_8794D064-E02A-4464-BA1E-A14E86725894">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565622.971 5936275.377</gml:lowerCorner>
          <gml:upperCorner>565631.618 5936275.395</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_D56AF8CD-F847-4B7D-B877-240B21CFC15F'-->
        <gml:LineString gml:id="Gml_D56AF8CD-F847-4B7D-B877-240B21CFC15F" srsName="EPSG:25832">
          <gml:posList>565631.618 5936275.377 565622.971 5936275.395</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_A15383F5-C92C-4BE4-8575-895FB8CAF141">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565556.611 5936276.620</gml:lowerCorner>
          <gml:upperCorner>565565.376 5936284.059</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gliederung1>(A)</xplan:gliederung1>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_50260B25-A589-4CD0-B152-9E034360F154"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_6558F601-79E2-455E-BFAB-C1FCAFE3D8B2'-->
        <gml:Polygon gml:id="Gml_6558F601-79E2-455E-BFAB-C1FCAFE3D8B2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565565.376 5936283.891 565556.752 5936284.059 565556.611 5936276.836 565565.235 5936276.620 565565.376 5936283.891</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_B4DF08CC-E68D-46B7-A7CC-D074DD65C11C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565540.017 5936276.969</gml:lowerCorner>
          <gml:upperCorner>565548.806 5936284.382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gliederung1>(A)</xplan:gliederung1>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_50260B25-A589-4CD0-B152-9E034360F154"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_9A907B69-B1FC-4449-96E9-D6F36959B55D'-->
        <gml:Polygon gml:id="Gml_9A907B69-B1FC-4449-96E9-D6F36959B55D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565548.666 5936276.991 565548.806 5936284.213 565540.161 5936284.382 565540.017 5936276.969 565548.666 5936276.991</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_CDF14878-00DD-4416-8C3E-56C7676DB3D0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565539.832 5936262.888</gml:lowerCorner>
          <gml:upperCorner>565565.235 5936276.991</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_801D99B1-08AD-4EB1-A279-2485F94F53B3'-->
        <gml:Polygon gml:id="Gml_801D99B1-08AD-4EB1-A279-2485F94F53B3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565565.235 5936276.620 565556.611 5936276.836 565548.666 5936276.991 565540.017 5936276.969 565539.832 5936267.442 565544.154 5936267.358 565544.074 5936263.210 565560.664 5936262.888 565560.745 5936267.035 565565.048 5936266.951 565565.235 5936276.620</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_242CCE94-3A6A-4F8B-8CBD-BDDDE20F5504">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565569.052 5936276.426</gml:lowerCorner>
          <gml:upperCorner>565577.840 5936283.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gliederung1>(A)</xplan:gliederung1>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_50260B25-A589-4CD0-B152-9E034360F154"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_5EA34067-C29E-4E8E-B14F-11FF976CFB92'-->
        <gml:Polygon gml:id="Gml_5EA34067-C29E-4E8E-B14F-11FF976CFB92" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565577.840 5936283.649 565569.194 5936283.817 565569.052 5936276.493 565577.699 5936276.426 565577.840 5936283.649</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_DC5F3F0C-C3D3-4D2B-9800-6C8A5CD99215">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565610.530 5936275.686</gml:lowerCorner>
          <gml:upperCorner>565619.316 5936283.010</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gliederung1>(A)</xplan:gliederung1>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_50260B25-A589-4CD0-B152-9E034360F154"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_3B95FE4A-F098-4BB7-A96E-74AD81526CA3'-->
        <gml:Polygon gml:id="Gml_3B95FE4A-F098-4BB7-A96E-74AD81526CA3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565619.316 5936282.842 565610.671 5936283.010 565610.530 5936275.787 565619.177 5936275.686 565619.316 5936282.842</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_A14D1BC1-8020-48A5-8A8A-88270373030E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565568.865 5936261.839</gml:lowerCorner>
          <gml:upperCorner>565619.177 5936283.494</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_EED54F39-60E8-4674-8E05-A78B8A71D82F'-->
        <gml:Polygon gml:id="Gml_EED54F39-60E8-4674-8E05-A78B8A71D82F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565602.725 5936283.165 565585.785 5936283.494 565585.645 5936276.271 565577.699 5936276.426 565569.052 5936276.493 565568.865 5936266.877 565573.188 5936266.793 565573.107 5936262.645 565589.698 5936262.323 565589.778 5936266.470 565598.074 5936266.309 565597.993 5936262.161 565614.584 5936261.839 565614.664 5936265.986 565618.987 5936265.902 565619.177 5936275.686 565610.530 5936275.787 565602.585 5936275.942 565602.725 5936283.165</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_52851937-0BFB-4AE4-B2FE-C6792151010F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565622.971 5936275.377</gml:lowerCorner>
          <gml:upperCorner>565631.759 5936282.768</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gliederung1>(A)</xplan:gliederung1>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Gml_50260B25-A589-4CD0-B152-9E034360F154"/>
      <xplan:position>
        <!--Inlined geometry 'Gml_A7A2FA02-BD7B-417D-B0F3-261376869F62'-->
        <gml:Polygon gml:id="Gml_A7A2FA02-BD7B-417D-B0F3-261376869F62" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565631.759 5936282.600 565623.114 5936282.768 565622.971 5936275.395 565631.618 5936275.377 565631.759 5936282.600</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_CB355D2E-90FA-4D68-9E5F-8B230EC36F62">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565622.785 5936260.790</gml:lowerCorner>
          <gml:upperCorner>565673.235 5936282.445</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_B72F06F8-4025-4B8E-ACC0-20477886323F'-->
        <gml:Polygon gml:id="Gml_B72F06F8-4025-4B8E-ACC0-20477886323F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565656.645 5936282.116 565639.704 5936282.445 565639.564 5936275.223 565631.618 5936275.377 565622.971 5936275.395 565622.785 5936265.828 565627.107 5936265.744 565627.027 5936261.597 565643.617 5936261.274 565643.698 5936265.422 565651.993 5936265.260 565651.912 5936261.113 565654.207 5936261.068 565668.503 5936260.790 565668.584 5936264.937 565672.906 5936264.853 565673.235 5936281.793 565664.590 5936281.961 565664.450 5936274.739 565658.685 5936274.851 565656.504 5936274.893 565656.645 5936282.116</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_08DA5927-E2A7-4E90-AC0B-EE23AAB58F07">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565841.477 5936284.528</gml:lowerCorner>
          <gml:upperCorner>565867.175 5936292.456</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_581FFA2D-6D67-497D-A93E-2E963BE4DE5B'-->
        <gml:LineString gml:id="Gml_581FFA2D-6D67-497D-A93E-2E963BE4DE5B" srsName="EPSG:25832">
          <gml:posList>565867.175 5936292.456 565841.477 5936284.528</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1800</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_15E6CF30-CE1E-444A-AA45-FA453505C51D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565749.985 5936266.013</gml:lowerCorner>
          <gml:upperCorner>565753.175 5936316.065</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_AD4A1757-35A7-496E-B6E9-07C85E40273C'-->
        <gml:LineString gml:id="Gml_AD4A1757-35A7-496E-B6E9-07C85E40273C" srsName="EPSG:25832">
          <gml:posList>565749.985 5936316.065 565752.173 5936298.835 565753.175 5936289.176 565752.360 5936266.013</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1800</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_425F1CEB-6195-4AD5-B728-8F0D2F818192">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565743.060 5936256.279</gml:lowerCorner>
          <gml:upperCorner>565743.789 5936293.276</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_13208814-E926-44CC-B9A3-FC0F34DA03EF'-->
        <gml:LineString gml:id="Gml_13208814-E926-44CC-B9A3-FC0F34DA03EF" srsName="EPSG:25832">
          <gml:posList>565743.060 5936256.279 565743.789 5936293.276</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1600</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_B886623C-C4F9-47EA-A399-B5971A66D252">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565480.419 5936250.424</gml:lowerCorner>
          <gml:upperCorner>565499.081 5936430.999</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>09F08185-5C1D-4475-95E1-80EDA5DEB7A2</xplan:uuid>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_9AF8D5D1-6E42-49AE-BBDC-88A27DDCF023'-->
        <gml:Polygon gml:id="Gml_9AF8D5D1-6E42-49AE-BBDC-88A27DDCF023" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565487.665 5936259.143 565488.371 5936269.539 565489.820 5936284.271 565497.336 5936341.047 565498.213 5936386.075 565498.842 5936418.466 565498.866 5936419.727 565499.081 5936430.999 565482.820 5936430.079 565480.419 5936250.573 565487.666 5936250.424 565487.665 5936259.143</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Denkmalschutzrecht gml:id="Gml_8A7CD17B-A230-40C3-B510-A0F8E9AF6847">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565764.895 5936233.636</gml:lowerCorner>
          <gml:upperCorner>565814.350 5936255.377</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_D6E47860-528C-4AE7-B330-14147238C61C'-->
        <gml:Polygon gml:id="Gml_D6E47860-528C-4AE7-B330-14147238C61C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565807.302 5936253.284 565805.611 5936255.036 565787.962 5936255.377 565786.096 5936253.713 565786.066 5936252.161 565771.250 5936252.448 565771.270 5936253.473 565769.622 5936255.262 565767.056 5936255.311 565764.895 5936252.662 565764.937 5936237.195 565766.800 5936235.020 565769.267 5936234.972 565771.167 5936236.894 565771.184 5936237.792 565785.823 5936237.509 565785.790 5936235.807 565787.365 5936233.980 565805.136 5936233.636 565806.988 5936235.327 565807.030 5936237.537 565809.625 5936237.487 565810.581 5936237.925 565813.762 5936240.908 565814.350 5936241.773 565814.239 5936246.303 565813.937 5936247.378 565810.719 5936250.724 565809.841 5936251.277 565807.264 5936251.328 565807.302 5936253.284</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1100</xplan:artDerFestlegung>
    </xplan:SO_Denkmalschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="Gml_65A9FACE-E0AC-409B-BE0C-069EEA10D43F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565495.758 5936256.517</gml:lowerCorner>
          <gml:upperCorner>565675.994 5936423.059</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Umgrenzung der Grundstücke, für die Gemeinschaftstiefgaragen bestimmt sind</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#GML_A5AE3104-5AB9-4729-A9A9-94D9AC061828"/>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <!--Inlined geometry 'Gml_C35182DE-0FE9-482B-A00E-F4A67C57D2DD'-->
        <gml:Polygon gml:id="Gml_C35182DE-0FE9-482B-A00E-F4A67C57D2DD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList>565618.142 5936423.059 565526.893 5936417.517 565526.919 5936419.184 565498.866 5936419.727 565498.842 5936418.466 565498.213 5936386.075 565495.758 5936259.987 565533.936 5936259.247 565539.658 5936259.136 565564.887 5936258.648 565568.684 5936258.574 565654.126 5936256.916 565674.723 5936256.517 565674.744 5936256.517 565674.745 5936256.561 565674.724 5936256.562 565675.219 5936282.140 565675.994 5936322.166 565670.722 5936326.226 565668.013 5936328.312 565666.959 5936328.912 565633.956 5936354.191 565621.756 5936363.535 565621.333 5936370.504 565618.142 5936423.059</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_50260B25-A589-4CD0-B152-9E034360F154">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Auf den mit „(A)" bezeichneten Flächen des allgemeinen Wohngebiets am Turmweg sind im Erdgeschoss und im ersten Obergeschoss nur nicht störende Gewerbebetriebe, Anlagen für Verwaltungen und Räume für freie Berufe im Sinne des § 13 der Baunutzungsverordnung in der Fassung vom 23. Januar 1990 (BGBl. I S. 133), zuletzt geändert am 22. April 1993 (BGBl. I S. 466, 479), zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_953F73E1-9945-40EA-84AD-C61874F26A4C">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Im reinen Wohngebiet sind die viergeschossigen Baukörper jeweils zu den festgesetzten Gehrechten hin auf mindestens 40 vom Hundert der jeweiligen Gebäudelänge auf bis zu zwei Vollgeschosse abzustaffeln.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_971BE4C4-E04B-4701-BEEF-9D7918C90196">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>Im reinen Wohngebiet können zu den Gehrechten hin Überschreitungen der Baugrenzen durch Erker und Balkone bis zu 1,5 m oberhalb einer lichten Höhe von 6 m sowie zur Erschließung erforderliche Brücken oberhalb einer lichten Höhe von 4m über den Gehrechten zugelassen werden. Im allgemeinen Wohngebiet am Turmweg können
Überschreitungen der Baugrenzen durch Terrassen, Balkone und Fahrstuhlschächte bis zu 4m zugelassen werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_1A2A44AE-B6FD-41D9-892E-BE39AA705246">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>Im Kerngebiet entlang der Rothenbaumchaussee sind die Aufenthaltsräume durch geeignete Grundrissgestaltung den lärmabgewandten Gebäudeseiten zuzuordnen. Soweit die Anordnung der in Satz 1 genannten Räume an den lärmabgewandten Gebäudeseiten nicht möglich ist, muss für diese Räume ein ausreichender Lärmschutz durch bauliche Maßnahmen an Außentüren, Fenstern, Außenwänden und Dächern der Gebäude geschaffen werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_14529F22-C338-4D03-BD02-9CE91097DE3C">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>In den Kerngebieten sind Spielhallen und ähnliche Unternehmen im Sinne von § 33 i der Gewerbeordnung, die der Aufstellung von Spielgeräten mit oder ohne Gewinnmöglichkeiten dienen, sowie Vorführ- und Geschäftsräume, deren Zweck auf Darstellungen oder auf Handlungen mit sexuellem Charakter ausgerichtet ist, unzulässig. Tankstellen im Zusammenhang mit Parkhäusern und Großgaragen sind unzulässig. Ausnahmen für Tankstellen nach § 7 Absatz 3 Nummer 1 der Baunutzungsverordnung werden ausgeschlossen. Einkaufszentren und großflächige Handels- und Einzelhandelsbetriebe nach § 11 Absatz 3 der Baunutzungsverordnung sind unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_B02177EB-D1F9-4F79-941B-9CA1F221C157">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Auf den privaten Grundstücksflächen sind Fahr- und Gehwege in wasser- und luftdurchlässigem Aufbau herzustellen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_5A29A71C-33B3-4D37-A6B3-798FDF226C53">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>Die nicht überbauten Flächen von Tiefgaragen sind mit einem mindestens 0,5 m starken durchwurzelbaren Substrataufbau herzustellen. Soweit Bäume angepflanzt werden, muss auf einer Fläche von 12 m  je Baum die Schichtstärke mindestens 1 m betragen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_9F307243-4605-44E6-BD6F-89F2E6B469ED">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Das auf dem Flurstück 1632 der Gemarkung Rotherbaum festgesetzte Geh-, Fahr- und Leitungsrecht umfasst die Befugnis, für den Anschluss des Flurstücks 1638 an die Hallerstraße eine Zufahrt anzulegen und zu unterhalten sowie die Befugnis, der Hamburger Gaswerke GmbH, der Hamburger Wasserwerke GmbH, der Hamburgischen Electricitäts-Werke AG und der Deutschen Telekom AG, unterirdische Leitungen zu verlegen und zu unterhalten. Geringfügige Abweichungen von dem festgesetzten Geh-, Fahr- und Leitungsrecht können zugelassen werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_058F9594-D6F6-43C1-989F-F9AB9EFBB385">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Die auf den Flurstücken 1629, 1631, 1632, 1637, 1639, 1641 und 1654 der Gemarkung Rotherbaum festgesetzten Gehrechte umfassen die Befugnis der Freien und Hansestadt Hamburg, allgemein zugängliche Wege anzulegen und zu unterhalten. Geringfügige Abweichungen von den festgesetzten Gehrechten können zugelassen werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_735F3E62-AE80-4B08-A62B-2D18DFCC351D">
      <xplan:schluessel>§2 Nr.10</xplan:schluessel>
      <xplan:text>Im reinen und allgemeinen Wohngebiet sind die Dächer der Gebäude extensiv zu begrünen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_701E42E5-23F0-4B75-B11E-FF872CA73CB7">
      <xplan:schluessel>§2 Nr.11</xplan:schluessel>
      <xplan:text>Im Kerngebiet an der Rothenbaumchaussee kann eine Überschreitung der Baugrenzen bis zu 5 m durch untergeordnete Bauteile wie Stützen, Sonnenschutz- und Dachkonstruktionen zugelassen werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_E7B18656-8F6E-4BF7-8877-3C27A10A18F4">
      <xplan:schluessel>§2 Nr.12</xplan:schluessel>
      <xplan:text>Für die nach der Planzeichnung zu erhaltenden Bäume und Baumreihen sind bei Abgang Ersatzpflanzungen vorzunehmen. Dabei sind Bäume mit einem Stammumfang von mindestens 16 cm, in 1 m Höhe über dem Erdboden gemessen, zu verwenden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_Rasterdarstellung gml:id="XPLAN_rasterBasis_2bfcdf18-be5e-4792-8ff1-f70b746a99a6">
      <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>Rotherbaum27.pgw</xplan:georefURL>
          <xplan:referenzURL>Rotherbaum27.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
    </xplan:XP_Rasterdarstellung>
  </gml:featureMember>
</xplan:XPlanAuszug>
