﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_DB4B0B33-C4A4-46E2-B2B6-EE0AE6148129" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/2 http://www.xplanungwiki.de/upload/XPlanGML/5.2/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/5/2">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>574390.3164 5947249.5282</gml:lowerCorner>
      <gml:upperCorner>574702.9458 5947436.7213</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="Gml_0B3B78A5-4945-4D5B-83C9-9BECF7679262">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574390.3164 5947249.5282</gml:lowerCorner>
          <gml:upperCorner>574702.9458 5947436.7213</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>Test PPO Index -1</xplan:name>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_6DDCF403-79FC-465F-BD44-A8AEAF4BF6C2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574391.5188 5947436.7213 574390.3164 5947251.5505 574701.7434 5947249.5282 
574702.9458 5947434.6991 574391.5188 5947436.7213 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:bereich xlink:href="#Gml_EA1C4DE8-FD45-4EB2-8547-FF2011BAB486" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="Gml_EA1C4DE8-FD45-4EB2-8547-FF2011BAB486">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574390.3164 5947249.5282</gml:lowerCorner>
          <gml:upperCorner>574702.9458 5947436.7213</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_53F501BF-EA14-4A3B-B055-78C7B5597883" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574391.5188 5947436.7213 574390.3164 5947251.5505 574701.7434 5947249.5282 
574702.9458 5947434.6991 574391.5188 5947436.7213 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#Gml_3C239C51-10F8-4C2C-94F9-E04420BB6CE4" />
      <xplan:praesentationsobjekt xlink:href="#Gml_4DCCBF0D-BFDA-45B8-92E4-F438E0117F9E" />
      <xplan:gehoertZuPlan xlink:href="#Gml_0B3B78A5-4945-4D5B-83C9-9BECF7679262" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_3C239C51-10F8-4C2C-94F9-E04420BB6CE4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574390.3164 5947249.5282</gml:lowerCorner>
          <gml:upperCorner>574702.9458 5947436.7213</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_EA1C4DE8-FD45-4EB2-8547-FF2011BAB486" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_4DCCBF0D-BFDA-45B8-92E4-F438E0117F9E" />
      <xplan:rechtscharakter>9998</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_00977E59-3241-4FB4-92DA-E2804FCE626D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">574391.5188 5947436.7213 574390.3164 5947251.5505 574701.7434 5947249.5282 
574702.9458 5947434.6991 574391.5188 5947436.7213 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_4DCCBF0D-BFDA-45B8-92E4-F438E0117F9E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>574476.8471 5947349.4086</gml:lowerCorner>
          <gml:upperCorner>574476.8471 5947349.4086</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>-1</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_EA1C4DE8-FD45-4EB2-8547-FF2011BAB486" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_3C239C51-10F8-4C2C-94F9-E04420BB6CE4" />
      <xplan:position>
        <gml:Point gml:id="Gml_F5AB61E1-B498-4596-9AEA-775714C7EB84" srsName="EPSG:25832">
          <gml:pos>574476.8471 5947349.4086</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
</xplan:XPlanAuszug>
