<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_64990359-A27D-4182-8832-48CACC97B2F8" xsi:schemaLocation="http://www.xplanung.de/xplangml/4/1 http://www.xplanungwiki.de/upload/XPlanGML/4.1-Kernmodell/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/4/1">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>563607.4505 5938236.6243</gml:lowerCorner>
      <gml:upperCorner>564077.522 5938709.4651</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_A3DC5063-81C0-48AF-B1D4-801A496B8E85">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563607.4505 5938236.6243</gml:lowerCorner>
          <gml:upperCorner>564077.522 5938709.4651</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan003_4-1</xplan:name>
      <xplan:technHerstellDatum>2014-08-21</xplan:technHerstellDatum>
      <xplan:xPlanGMLVersion>4.1</xplan:xPlanGMLVersion>
      <xplan:raeumlicherGeltungsbereich>
        <gml:MultiSurface gml:id="Gml_2D1F9C5B-F54A-45BB-80AE-6A7C83D9042F" srsName="EPSG:25832">
          <gml:surfaceMember>
            <gml:Polygon gml:id="Gml_665EDB8E-9AAB-4CAE-9E0F-2A4D48B1F4BA" srsName="EPSG:25832">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsName="EPSG:25832">564045.2081 5938259.3484 564077.522 5938264.299 564066.8745 5938331.7459 
564036.391 5938326.477 563950.062 5938313.521 563951.173 5938308.66 
563888.755 5938294.819 563879.8731 5938292.8493 563882.4376 5938278.0897 
563873.156 5938276.7435 563878.4212 5938240.8079 563879.0341 5938236.6243 
564045.2081 5938259.3484 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
          <gml:surfaceMember>
            <gml:Polygon gml:id="Gml_63ADD830-261D-4A47-8CEE-C8464AE57A3B" srsName="EPSG:25832">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsName="EPSG:25832">564020.158 5938587.286 564013.744 5938620.627 564010.85 5938635.539 
564009.564 5938640.635 564009.306 5938642.623 564008.811 5938646.417 
564008.476 5938650.932 564000.843 5938700.213 563999.7622 5938709.4651 
563972.1473 5938703.4914 563954.7126 5938699.4406 563892.7278 5938685.7438 
563791.5299 5938662.4922 563642.0759 5938627.7179 563612.9173 5938620.7889 
563607.4505 5938619.6665 563623.905 5938591.0696 563703.6068 5938455.5578 
563711.963 5938457.685 563736.907 5938464.076 563764.965 5938471.264 
563787.664 5938477.079 563805.993 5938481.775 563822.457 5938485.993 
563825.537 5938486.782 563841.84 5938490.959 563864.242 5938496.7 
563871.9 5938498.662 563881.166 5938501.037 563896.633 5938505.007 
563911.141 5938508.725 563932.438 5938514.182 563951.808 5938519.146 
563967.061 5938523.054 563982.315 5938526.963 563973.505 5938561.637 
563972.2243 5938566.8187 563992.9524 5938571.9216 564007.9233 5938575.7484 
564010.0664 5938565.0328 564014.3725 5938534.7829 564020.0478 5938502.8979 
564024.1754 5938477.8709 564028.7394 5938450.9865 564031.7325 5938434.7517 
564049.368 5938437.904 564047.096 5938449.734 564036.924 5938501.658 
564032.229 5938525.641 564028.405 5938545.299 564025.294 5938561.085 
564020.158 5938587.286 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#Gml_267100B3-4C86-4DB6-A7E5-1B2932CB7948" />
      <xplan:texte xlink:href="#Gml_91A1D0E6-546E-4E95-A760-D4994CE80CA9" />
      <xplan:begruendungsTexte xlink:href="#Gml_4BC7A96E-C31E-4A85-B5AC-1E2E28E93A3B" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>317</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>1972-03-07</xplan:rechtsverordnungsDatum>
      <xplan:staedtebaulicherVertrag>false</xplan:staedtebaulicherVertrag>
      <xplan:erschliessungsVertrag>false</xplan:erschliessungsVertrag>
      <xplan:durchfuehrungsVertrag>false</xplan:durchfuehrungsVertrag>
      <xplan:bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_267100B3-4C86-4DB6-A7E5-1B2932CB7948">
      <xplan:schluessel>§2 Nr1</xplan:schluessel>
      <xplan:text>Die nicht überbaubaren Grundstücksteile sind als Garagen unter Erdgleiche nutzbar, wenn Wohnruhe und Gartenanlagen nicht erheblich beeinträchtigt werden.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_91A1D0E6-546E-4E95-A760-D4994CE80CA9">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Auf dem Flurstück 1103 der Gemarkung Lokstedt ist eine Tankstelle zulässig.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_BegruendungAbschnitt gml:id="Gml_4BC7A96E-C31E-4A85-B5AC-1E2E28E93A3B">
      <xplan:refText>
        <xplan:XP_ExterneReferenz>
          <xplan:beschreibung>Der Bebauungsplan BPlan003_4-1 für den Geltungsbereich Grandweg — Behrkampsweg — Lokstedter Steindamm — Veilchenweg (Bezirk Eimsbüttels Ortsteil 317) wird festgestellt.</xplan:beschreibung>
        </xplan:XP_ExterneReferenz>
      </xplan:refText>
    </xplan:XP_BegruendungAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_D614C448-56DD-459F-8393-83A1B45A77BC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563607.4505 5938236.6243</gml:lowerCorner>
          <gml:upperCorner>564077.522 5938709.4651</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:MultiSurface gml:id="Gml_9FAA7506-63AD-479D-83B4-1ABF7388C6B8" srsName="EPSG:25832">
          <gml:surfaceMember>
            <gml:Polygon gml:id="Gml_F8FB696C-BB48-42ED-A6EA-07BB1A7473BE" srsName="EPSG:25832">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsName="EPSG:25832">564045.2081 5938259.3484 564077.522 5938264.299 564066.8745 5938331.7459 
564036.391 5938326.477 563950.062 5938313.521 563951.173 5938308.66 
563888.755 5938294.819 563879.8731 5938292.8493 563882.4376 5938278.0897 
563873.156 5938276.7435 563878.4212 5938240.8079 563879.0341 5938236.6243 
564045.2081 5938259.3484 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
          <gml:surfaceMember>
            <gml:Polygon gml:id="Gml_000A6503-CE84-49D2-8102-E443A5E21B2F" srsName="EPSG:25832">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsName="EPSG:25832">564020.158 5938587.286 564013.744 5938620.627 564010.85 5938635.539 
564009.564 5938640.635 564009.306 5938642.623 564008.811 5938646.417 
564008.476 5938650.932 564000.843 5938700.213 563999.7622 5938709.4651 
563972.1473 5938703.4914 563954.7126 5938699.4406 563892.7278 5938685.7438 
563791.5299 5938662.4922 563642.0759 5938627.7179 563612.9173 5938620.7889 
563607.4505 5938619.6665 563623.905 5938591.0696 563703.6068 5938455.5578 
563711.963 5938457.685 563736.907 5938464.076 563764.965 5938471.264 
563787.664 5938477.079 563805.993 5938481.775 563822.457 5938485.993 
563825.537 5938486.782 563841.84 5938490.959 563864.242 5938496.7 
563871.9 5938498.662 563881.166 5938501.037 563896.633 5938505.007 
563911.141 5938508.725 563932.438 5938514.182 563951.808 5938519.146 
563967.061 5938523.054 563982.315 5938526.963 563973.505 5938561.637 
563972.2243 5938566.8187 563992.9524 5938571.9216 564007.9233 5938575.7484 
564010.0664 5938565.0328 564014.3725 5938534.7829 564020.0478 5938502.8979 
564024.1754 5938477.8709 564028.7394 5938450.9865 564031.7325 5938434.7517 
564049.368 5938437.904 564047.096 5938449.734 564036.924 5938501.658 
564032.229 5938525.641 564028.405 5938545.299 564025.294 5938561.085 
564020.158 5938587.286 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:geltungsbereich>
      <xplan:nachrichtlich xlink:href="#GML_C2E1C009-F1F4-497E-9BDB-DDB53F7CFFB8" />
      <xplan:nachrichtlich xlink:href="#GML_E37BC97D-097D-4691-9709-D50598DAFAAB" />
      <xplan:praesentationsobjekt xlink:href="#GML_91BFBC42-1056-47AD-976F-302821DDF113" />
      <xplan:praesentationsobjekt xlink:href="#GML_CC494B1B-57C2-41FD-BB42-ECF78CDF7BEB" />
      <xplan:praesentationsobjekt xlink:href="#GML_4A07DC7D-95AC-477F-AADB-F0E0263A6AF6" />
      <xplan:praesentationsobjekt xlink:href="#GML_D10C8104-84B4-420A-B952-ED6140003749" />
      <xplan:praesentationsobjekt xlink:href="#GML_C86FC9B1-9BB0-4EA5-ADD9-B43146AAF720" />
      <xplan:praesentationsobjekt xlink:href="#GML_284069E1-10BA-4BA1-87AE-D958B8B3CAF5" />
      <xplan:praesentationsobjekt xlink:href="#GML_8BDD12E5-9369-4DEC-9AA8-CCD05076817C" />
      <xplan:praesentationsobjekt xlink:href="#GML_CF5992D8-52BE-4E9A-A162-E9E19C51ED8B" />
      <xplan:praesentationsobjekt xlink:href="#GML_19CBA11A-C184-40BD-B21F-4599999C52D3" />
      <xplan:praesentationsobjekt xlink:href="#GML_A509F13B-AEDA-49C3-88A1-B9A9C7339C3F" />
      <xplan:praesentationsobjekt xlink:href="#GML_D560018D-1F60-49A0-A5C2-E27138491FA5" />
      <xplan:praesentationsobjekt xlink:href="#GML_94A54459-653D-4708-9912-F2C7FF162F25" />
      <xplan:rasterBasis xlink:href="#Gml_758104A5-BBF2-4411-BD92-A16D3EA35749" />
      <xplan:versionBauNVO>1000</xplan:versionBauNVO>
      <xplan:gehoertZuPlan xlink:href="#GML_A3DC5063-81C0-48AF-B1D4-801A496B8E85" />
      <xplan:inhaltBPlan xlink:href="#GML_F003EA97-3361-4351-9C5F-6F1B175B587D" />
      <xplan:inhaltBPlan xlink:href="#GML_A023D8C1-609B-4697-B779-6DF33CA061D6" />
      <xplan:inhaltBPlan xlink:href="#GML_352428B1-6C8C-4FAE-A191-5EA58CB52595" />
      <xplan:inhaltBPlan xlink:href="#GML_B33D55D6-6772-4A05-B0FB-0132289328DA" />
      <xplan:inhaltBPlan xlink:href="#GML_BF15D119-6287-4FE1-81B3-9CC413E0097F" />
      <xplan:inhaltBPlan xlink:href="#GML_C6D8ACB2-A1B8-4903-BCD1-FA61FB7CEF49" />
      <xplan:inhaltBPlan xlink:href="#GML_6D597255-163B-4FFB-8D6D-B9DDFD7E1A07" />
      <xplan:inhaltBPlan xlink:href="#GML_F964C670-465B-45E6-BD8E-02860CC7C104" />
      <xplan:inhaltBPlan xlink:href="#GML_4B39E838-835E-439B-B862-6FB0386DF6EA" />
      <xplan:inhaltBPlan xlink:href="#GML_07A7E0CE-A650-4745-B81D-E8E9E2ED5A01" />
      <xplan:inhaltBPlan xlink:href="#GML_5F332328-CB0E-425E-A228-816164777ED6" />
      <xplan:inhaltBPlan xlink:href="#GML_7FD2503A-041F-44A6-B0E7-D20440C97478" />
      <xplan:inhaltBPlan xlink:href="#GML_5C4594A8-0886-4516-B645-D12EA63E8210" />
      <xplan:inhaltBPlan xlink:href="#GML_51457177-7038-4F01-B6B1-8E1848D7039E" />
      <xplan:inhaltBPlan xlink:href="#GML_BC162E09-C565-4A08-AB6D-872B4D75D456" />
      <xplan:inhaltBPlan xlink:href="#GML_D6692A33-3307-443E-83EB-A8B22088E52F" />
      <xplan:inhaltBPlan xlink:href="#GML_8F951356-99FD-4170-A004-E855ED2CA892" />
      <xplan:inhaltBPlan xlink:href="#GML_DDBDB2E8-40FD-43C8-B463-7BA9B6AE24B6" />
      <xplan:inhaltBPlan xlink:href="#GML_6310559C-9C76-41DB-BF43-3077DA54BA7E" />
      <xplan:inhaltBPlan xlink:href="#GML_0272CE30-3269-4FC8-82E8-0DE302AF8B1A" />
      <xplan:inhaltBPlan xlink:href="#GML_0B557087-A310-49C0-A897-7040AF67BF33" />
      <xplan:inhaltBPlan xlink:href="#GML_7A344658-217C-4644-AECB-6050290C78C8" />
      <xplan:inhaltBPlan xlink:href="#GML_D818C653-7D8F-44B5-9DCB-64C00E5A4BC1" />
      <xplan:inhaltBPlan xlink:href="#GML_502F4C02-9388-4E4D-83FC-FC7EBE975871" />
      <xplan:inhaltBPlan xlink:href="#GML_40ACDD03-F118-41D3-9C66-975AC8948FB2" />
      <xplan:inhaltBPlan xlink:href="#GML_9D4996D0-A8EB-4F5C-A504-5BAD248CC378" />
      <xplan:inhaltBPlan xlink:href="#GML_C11942A6-EA49-4213-A785-557590B2EA0A" />
      <xplan:inhaltBPlan xlink:href="#GML_24421451-7209-4094-BFE3-7B288B8FA4EA" />
      <xplan:inhaltBPlan xlink:href="#GML_D0574A6E-E0DF-4301-8F2E-9D5F053EDCB5" />
      <xplan:inhaltBPlan xlink:href="#GML_853695D1-0AA6-4436-81A0-51C6FB450932" />
      <xplan:inhaltBPlan xlink:href="#GML_292D9FB0-4738-4E03-9C0A-C98C2FAB9B3F" />
      <xplan:inhaltBPlan xlink:href="#GML_F05E81ED-8034-4251-BBBE-6965F8DE2B32" />
      <xplan:inhaltBPlan xlink:href="#GML_2DAA2CB6-600F-4BC6-B58F-5FD09EAED3F4" />
      <xplan:inhaltBPlan xlink:href="#GML_24E72DBD-22F2-4068-BD21-2E3F08FE1F0C" />
      <xplan:inhaltBPlan xlink:href="#GML_440C031F-C426-48A2-838B-324EB264A6B6" />
      <xplan:inhaltBPlan xlink:href="#GML_2AF1A83D-9AF3-4190-81EB-33F046472BBE" />
      <xplan:inhaltBPlan xlink:href="#GML_65E3CC09-481F-4FAD-B134-1F509FED8657" />
      <xplan:inhaltBPlan xlink:href="#GML_3F1BCA68-3E9D-4B5E-BAA7-2D711C939886" />
      <xplan:inhaltBPlan xlink:href="#GML_0505475B-90A1-4DD7-90DB-EFF094411921" />
      <xplan:inhaltBPlan xlink:href="#GML_24F1C887-253A-4AFB-A195-E631D9E887BD" />
      <xplan:inhaltBPlan xlink:href="#GML_4BF18F6E-0652-48AD-9E41-7FCAFE668DB9" />
      <xplan:inhaltBPlan xlink:href="#GML_D77479E6-AA07-4B9A-89E9-22827ADBB860" />
      <xplan:inhaltBPlan xlink:href="#GML_38D78975-58ED-4E63-99B9-438693A59C90" />
      <xplan:inhaltBPlan xlink:href="#GML_A4713798-1F7B-4375-8892-10F63BF19403" />
      <xplan:inhaltBPlan xlink:href="#GML_D61B537A-E474-4AD4-BF2A-F6CC560521E7" />
      <xplan:inhaltBPlan xlink:href="#GML_EAA62AC7-A903-4D21-B75D-651014777E06" />
      <xplan:inhaltBPlan xlink:href="#GML_1B80AD6D-18F2-47A1-94D3-E1E1199150B6" />
      <xplan:inhaltBPlan xlink:href="#GML_18D8BE3C-F1BE-441C-9AE5-67F3AE0B91FD" />
      <xplan:inhaltBPlan xlink:href="#GML_0D40A5C9-7E8B-43E9-8AA2-E889E5614DDD" />
      <xplan:inhaltBPlan xlink:href="#GML_741A93D9-88C5-4E46-82B2-CC14C5CA3346" />
      <xplan:inhaltBPlan xlink:href="#GML_3F61F21D-3ADD-45CD-B287-BE8243F6EB78" />
      <xplan:inhaltBPlan xlink:href="#GML_ADD3996C-DF0B-4F1A-8D23-8F348079FCB5" />
      <xplan:inhaltBPlan xlink:href="#GML_CCC36E84-8CA4-4FBA-B89E-04616BB04F42" />
      <xplan:inhaltBPlan xlink:href="#GML_D3A075B2-ED6C-49AB-89F7-0637D85D0F3A" />
      <xplan:inhaltBPlan xlink:href="#GML_D9A97FDA-CE7D-4D6F-B1C3-36E3300A35C4" />
      <xplan:inhaltBPlan xlink:href="#GML_B64D109F-9251-464B-8DE4-6F99DB175F73" />
      <xplan:inhaltBPlan xlink:href="#GML_8C942AEA-A7BA-4130-8D5A-22557CEE8CE6" />
      <xplan:inhaltBPlan xlink:href="#GML_EA7E09B4-A62C-4A77-B664-4D8240A92298" />
      <xplan:inhaltBPlan xlink:href="#GML_581CFA29-49E9-4FA8-8B6C-AB6AD630F81A" />
      <xplan:inhaltBPlan xlink:href="#GML_8048ACEE-4D83-447D-BA7F-7EB994B6C8D1" />
      <xplan:inhaltBPlan xlink:href="#GML_4BAB4B89-19C7-40EE-A3FA-A6307C95640D" />
      <xplan:inhaltBPlan xlink:href="#GML_9CEDC62D-5545-4010-A86C-97C2C797FB01" />
      <xplan:inhaltBPlan xlink:href="#GML_2A627C81-18C6-4577-8441-E91788270011" />
      <xplan:inhaltBPlan xlink:href="#GML_D8BDCB0B-9D36-45DA-9A2D-8AD3F243D088" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_F003EA97-3361-4351-9C5F-6F1B175B587D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563873.156 5938240.8079</gml:lowerCorner>
          <gml:upperCorner>564044.476 5938326.477</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_91BFBC42-1056-47AD-976F-302821DDF113" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6C3B8299-E9E1-41FD-BBC8-DDF2E3A2E5E9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563963.851 5938253.214 564044.476 5938264.923 564036.391 5938326.477 
563950.062 5938313.521 563951.173 5938308.66 563888.755 5938294.819 
563879.8731 5938292.8493 563882.4376 5938278.0897 563873.156 5938276.7435 
563878.4212 5938240.8079 563963.851 5938253.214 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1400</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_91BFBC42-1056-47AD-976F-302821DDF113">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564001.646 5938289.9827</gml:lowerCorner>
          <gml:upperCorner>564001.646 5938289.9827</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_F003EA97-3361-4351-9C5F-6F1B175B587D" />
      <xplan:position>
        <gml:Point gml:id="Gml_7895E096-3AF2-4298-A78F-04B15C0C7C71" srsName="EPSG:25832">
          <gml:pos>564001.646 5938289.9827</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_A023D8C1-609B-4697-B779-6DF33CA061D6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564054.3732 5938262.346</gml:lowerCorner>
          <gml:upperCorner>564077.522 5938331.7459</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2CC4825D-7D2D-4F9C-941B-F97CF5F6D8F3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">564077.522 5938264.299 564066.8745 5938331.7459 564054.3732 5938329.5851 
564064.7742 5938262.346 564077.522 5938264.299 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_352428B1-6C8C-4FAE-A191-5EA58CB52595">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563878.4212 5938236.6243</gml:lowerCorner>
          <gml:upperCorner>564053.6314 5938327.6686</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_310154CB-890F-4937-BC82-E249C16F78D4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">564045.2081 5938259.3484 564053.6314 5938260.6389 564043.2851 5938327.6686 
564036.391 5938326.477 564044.476 5938264.923 563963.851 5938253.214 
563878.4212 5938240.8079 563879.0341 5938236.6243 564045.2081 5938259.3484 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_B33D55D6-6772-4A05-B0FB-0132289328DA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564043.2851 5938260.6389</gml:lowerCorner>
          <gml:upperCorner>564064.7742 5938329.5851</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_137C16E3-3BB6-4CB2-89CC-68237E34075A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">564064.7742 5938262.346 564054.3732 5938329.5851 564043.2851 5938327.6686 
564053.6314 5938260.6389 564064.7742 5938262.346 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_BF15D119-6287-4FE1-81B3-9CC413E0097F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563695.3526 5938457.685</gml:lowerCorner>
          <gml:upperCorner>563764.965 5938506.087</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_CC494B1B-57C2-41FD-BB42-ECF78CDF7BEB" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4D2DA8A5-3F15-4E41-BD97-B4BBF909BB28" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563736.907 5938464.076 563764.965 5938471.264 563756.1 5938506.087 
563728.017 5938498.914 563699.5181 5938491.6338 563696.9053 5938490.9663 
563695.3526 5938484.6319 563711.963 5938457.685 563736.907 5938464.076 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:Z>2</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_CC494B1B-57C2-41FD-BB42-ECF78CDF7BEB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563738.3809 5938486.9965</gml:lowerCorner>
          <gml:upperCorner>563738.3809 5938486.9965</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>allgArtDerBaulNutzung</xplan:art>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>bauweise</xplan:art>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:index>2</xplan:index>
      <xplan:index>3</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_BF15D119-6287-4FE1-81B3-9CC413E0097F" />
      <xplan:position>
        <gml:Point gml:id="Gml_CE027639-1F75-492B-A3A7-34672AE47E76" srsName="EPSG:25832">
          <gml:pos>563738.3809 5938486.9965</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_C6D8ACB2-A1B8-4903-BCD1-FA61FB7CEF49">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563756.1 5938471.264</gml:lowerCorner>
          <gml:upperCorner>563982.315 5938561.637</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_4A07DC7D-95AC-477F-AADB-F0E0263A6AF6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BFEDAF25-5D73-43C6-9507-1A8FBE1F83B5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563787.664 5938477.079 563805.993 5938481.775 563822.457 5938485.993 
563825.537 5938486.782 563841.84 5938490.959 563864.242 5938496.7 
563871.9 5938498.662 563881.166 5938501.037 563896.633 5938505.007 
563911.141 5938508.725 563932.438 5938514.182 563951.808 5938519.146 
563967.061 5938523.054 563982.315 5938526.963 563973.505 5938561.637 
563958.237 5938557.736 563942.969 5938553.835 563923.593 5938548.885 
563902.28 5938543.439 563887.748 5938539.727 563872.287 5938535.769 
563855.327 5938531.435 563833.015 5938525.735 563813.603 5938520.777 
563797.141 5938516.571 563778.074 5938511.7 563756.1 5938506.087 
563764.965 5938471.264 563787.664 5938477.079 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:Z>2</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
      <xplan:bebauungsArt>4000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_4A07DC7D-95AC-477F-AADB-F0E0263A6AF6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563869.1911 5938518.7466</gml:lowerCorner>
          <gml:upperCorner>563869.1911 5938518.7466</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>allgArtDerBaulNutzung</xplan:art>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>bebauungsArt</xplan:art>
      <xplan:art>bauweise</xplan:art>
      <xplan:art>Z</xplan:art>
      <xplan:art>MaxZahlWohnungen</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:index>2</xplan:index>
      <xplan:index>3</xplan:index>
      <xplan:index>4</xplan:index>
      <xplan:index>5</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_C6D8ACB2-A1B8-4903-BCD1-FA61FB7CEF49" />
      <xplan:position>
        <gml:Point gml:id="Gml_213A7F11-72FF-46CC-8173-C5D3A20E15FD" srsName="EPSG:25832">
          <gml:pos>563869.1911 5938518.7466</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_6D597255-163B-4FFB-8D6D-B9DDFD7E1A07">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563619.736 5938567.349</gml:lowerCorner>
          <gml:upperCorner>563708.572 5938625.027</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_D10C8104-84B4-420A-B952-ED6140003749" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F24F1FAE-D277-4E25-A6B0-E32CBD427E0E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563654.722 5938625.027 563622.652 5938617.56 563619.736 5938613.482 
563629.972 5938595.971 563637.327 5938583.364 563641.983 5938575.364 
563646.65 5938567.349 563680.228 5938575.201 563708.572 5938581.831 
563705.361 5938593.373 563703.912 5938598.582 563702.595 5938603.161 
563691.373 5938600.932 563676.815 5938598.041 563667.418 5938596.175 
563661.08 5938606.48 563659.219 5938606.121 563654.722 5938625.027 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:Z>2</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_D10C8104-84B4-420A-B952-ED6140003749">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563674.0473 5938590.1446</gml:lowerCorner>
          <gml:upperCorner>563674.0473 5938590.1446</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>allgArtDerBaulNutzung</xplan:art>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>bauweise</xplan:art>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:index>2</xplan:index>
      <xplan:index>3</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6D597255-163B-4FFB-8D6D-B9DDFD7E1A07" />
      <xplan:position>
        <gml:Point gml:id="Gml_69262B8E-CF2F-4838-B971-8216DE129F30" srsName="EPSG:25832">
          <gml:pos>563674.0473 5938590.1446</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_F964C670-465B-45E6-BD8E-02860CC7C104">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563654.722 5938581.831</gml:lowerCorner>
          <gml:upperCorner>563766.6094 5938647.754</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_C86FC9B1-9BB0-4EA5-ADD9-B43146AAF720" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D6046B22-17BF-41D0-9829-04BF367514AE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563766.6094 5938607.3132 563766.107 5938609.28 563757.326 5938643.654 
563753.184 5938647.754 563739.2189 5938644.5585 563731.713 5938642.841 
563721.045 5938640.357 563710.309 5938637.858 563698.805 5938635.158 
563683.978 5938631.751 563669.335 5938628.386 563654.722 5938625.027 
563659.219 5938606.121 563661.08 5938606.48 563667.418 5938596.175 
563676.815 5938598.041 563691.373 5938600.932 563702.595 5938603.161 
563703.912 5938598.582 563705.361 5938593.373 563708.572 5938581.831 
563723.7 5938585.389 563730.997 5938587.105 563731.519 5938587.228 
563734.458 5938587.919 563742.278 5938589.758 563745.161 5938590.436 
563747.642 5938591.02 563766.541 5938595.466 563765.104 5938601.092 
563764.6899 5938602.7116 563766.6094 5938607.3132 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:Z>2</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
      <xplan:bebauungsArt>4000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_C86FC9B1-9BB0-4EA5-ADD9-B43146AAF720">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563735.563 5938608.004</gml:lowerCorner>
          <gml:upperCorner>563735.563 5938608.004</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>allgArtDerBaulNutzung</xplan:art>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>bebauungsArt</xplan:art>
      <xplan:art>bauweise</xplan:art>
      <xplan:art>Z</xplan:art>
      <xplan:art>MaxZahlWohnungen</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:index>2</xplan:index>
      <xplan:index>3</xplan:index>
      <xplan:index>4</xplan:index>
      <xplan:index>5</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_F964C670-465B-45E6-BD8E-02860CC7C104" />
      <xplan:position>
        <gml:Point gml:id="Gml_454EDEC5-E8F0-419E-A6B5-5D6E8A292F37" srsName="EPSG:25832">
          <gml:pos>563735.563 5938608.004</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_4B39E838-835E-439B-B862-6FB0386DF6EA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563646.65 5938536.156</gml:lowerCorner>
          <gml:upperCorner>563688.039 5938575.201</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_284069E1-10BA-4BA1-87AE-D958B8B3CAF5" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_14DC511D-F1ED-4FD2-B935-1809DC286BFA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563681.357 5938539.994 563688.039 5938541.517 563681.817 5938568.35 
563680.5 5938574.031 563680.228 5938575.201 563646.65 5938567.349 
563662.1067 5938540.376 563664.525 5938536.156 563681.357 5938539.994 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:Z>2</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_284069E1-10BA-4BA1-87AE-D958B8B3CAF5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563669.2848 5938560.3789</gml:lowerCorner>
          <gml:upperCorner>563669.2848 5938560.3789</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>allgArtDerBaulNutzung</xplan:art>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>bauweise</xplan:art>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:index>2</xplan:index>
      <xplan:index>3</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_4B39E838-835E-439B-B862-6FB0386DF6EA" />
      <xplan:position>
        <gml:Point gml:id="Gml_CB618A40-D064-4721-A4D5-79DA93559E43" srsName="EPSG:25832">
          <gml:pos>563669.2848 5938560.3789</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_07A7E0CE-A650-4745-B81D-E8E9E2ED5A01">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563664.525 5938501.375</gml:lowerCorner>
          <gml:upperCorner>563750.284 5938585.389</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_8BDD12E5-9369-4DEC-9AA8-CCD05076817C" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_5C4F10CC-A310-4470-BAE1-22603325C278" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563723.7 5938585.389 563708.572 5938581.831 563680.228 5938575.201 
563680.5 5938574.031 563681.817 5938568.35 563688.039 5938541.517 
563681.357 5938539.994 563664.525 5938536.156 563675.012 5938517.441 
563682.5375 5938505.1783 563689.117 5938501.375 563750.284 5938516.989 
563741.789 5938553.773 563731.9177 5938551.5221 563723.7 5938585.389 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:Z>2</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_8BDD12E5-9369-4DEC-9AA8-CCD05076817C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563699.4473 5938538.9476</gml:lowerCorner>
          <gml:upperCorner>563699.4473 5938538.9476</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>allgArtDerBaulNutzung</xplan:art>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>bauweise</xplan:art>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:index>2</xplan:index>
      <xplan:index>3</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_07A7E0CE-A650-4745-B81D-E8E9E2ED5A01" />
      <xplan:position>
        <gml:Point gml:id="Gml_198A0FFD-3046-457D-BE22-A7E067334873" srsName="EPSG:25832">
          <gml:pos>563699.4473 5938538.9476</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_5F332328-CB0E-425E-A228-816164777ED6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563723.7 5938551.5221</gml:lowerCorner>
          <gml:upperCorner>563818.896 5938606.131</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_CF5992D8-52BE-4E9A-A162-E9E19C51ED8B" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0F868A43-E801-405F-BDE9-959B90AF900F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563811.879 5938606.131 563809.544 5938605.582 563804.043 5938604.288 
563800.508 5938603.456 563798.561 5938602.998 563799.706 5938598.133 
563743.234 5938584.8492 563742.278 5938589.758 563734.458 5938587.919 
563731.519 5938587.228 563730.997 5938587.105 563723.7 5938585.389 
563731.9177 5938551.5221 563741.789 5938553.773 563756.685 5938557.163 
563773.187 5938560.916 563789.865 5938564.711 563803.981 5938567.922 
563818.896 5938571.317 563811.879 5938606.131 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
      <xplan:bebauungsArt>7000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_CF5992D8-52BE-4E9A-A162-E9E19C51ED8B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563772.0756 5938580.6195</gml:lowerCorner>
          <gml:upperCorner>563772.0756 5938580.6195</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>allgArtDerBaulNutzung</xplan:art>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>bebauungsArt</xplan:art>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:index>2</xplan:index>
      <xplan:index>3</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_5F332328-CB0E-425E-A228-816164777ED6" />
      <xplan:position>
        <gml:Point gml:id="Gml_9B4866E8-D7CB-4C7B-AE06-97D746EC928A" srsName="EPSG:25832">
          <gml:pos>563772.0756 5938580.6195</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_7FD2503A-041F-44A6-B0E7-D20440C97478">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563925.957 5938570.689</gml:lowerCorner>
          <gml:upperCorner>563991.002 5938696.935</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_19CBA11A-C184-40BD-B21F-4599999C52D3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F3201978-235B-4D5F-B8EB-A696330E3419" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563985.795 5938617.434 563984.9968 5938622.6056 563980.786 5938649.888 
563974.385 5938691.365 563967.447 5938696.935 563959.6546 5938695.1443 
563950.293 5938692.993 563935.442 5938689.581 563942.599 5938658.109 
563925.957 5938654.396 563932.827 5938623.58 563931.853 5938623.314 
563936.794 5938601.747 563950.816 5938606.235 563950.991 5938605.56 
563953.806 5938594.723 563960.05 5938570.689 563987.884 5938577.725 
563991.002 5938583.694 563985.795 5938617.434 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_19CBA11A-C184-40BD-B21F-4599999C52D3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563945.9072 5938625.8634</gml:lowerCorner>
          <gml:upperCorner>563945.9072 5938625.8634</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>allgArtDerBaulNutzung</xplan:art>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:index>2</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7FD2503A-041F-44A6-B0E7-D20440C97478" />
      <xplan:position>
        <gml:Point gml:id="Gml_3CBE782C-4B85-4B71-B35B-2B1C036B298E" srsName="EPSG:25832">
          <gml:pos>563945.9072 5938625.8634</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_5C4594A8-0886-4516-B645-D12EA63E8210">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563922.105 5938563.174</gml:lowerCorner>
          <gml:upperCorner>563960.05 5938606.235</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_A509F13B-AEDA-49C3-88A1-B9A9C7339C3F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_ED3C2017-AA23-4F85-929F-3299E940C73E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563960.05 5938570.689 563953.806 5938594.723 563950.991 5938605.56 
563950.816 5938606.235 563936.794 5938601.747 563930.159 5938599.475 
563922.434 5938597.207 563922.105 5938597.111 563930.749 5938563.174 
563960.05 5938570.689 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:Z>2</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_A509F13B-AEDA-49C3-88A1-B9A9C7339C3F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563939.5572 5938588.9539</gml:lowerCorner>
          <gml:upperCorner>563939.5572 5938588.9539</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>allgArtDerBaulNutzung</xplan:art>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>bauweise</xplan:art>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:index>2</xplan:index>
      <xplan:index>3</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_5C4594A8-0886-4516-B645-D12EA63E8210" />
      <xplan:position>
        <gml:Point gml:id="Gml_6C3960A9-5108-402C-9648-A69383C22BDA" srsName="EPSG:25832">
          <gml:pos>563939.5572 5938588.9539</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_51457177-7038-4F01-B6B1-8E1848D7039E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563741.789 5938516.989</gml:lowerCorner>
          <gml:upperCorner>563942.599 5938689.581</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_D560018D-1F60-49A0-A5C2-E27138491FA5" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_179CCA57-E838-44E0-8612-9C4E3C4F6299" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563931.853 5938623.314 563932.827 5938623.58 563925.957 5938654.396 
563942.599 5938658.109 563935.442 5938689.581 563903.204 5938682.173 
563890.974 5938679.363 563878.775 5938676.56 563864.249 5938673.221 
563852.908 5938670.614 563844.5683 5938668.697 563841.545 5938668.002 
563830.426 5938665.447 563819.325 5938662.897 563808.946 5938660.512 
563797.461 5938657.884 563786.627 5938655.405 563776.1948 5938653.018 
563770.608 5938646.2046 563778.1243 5938616.7022 563779.9913 5938613.2835 
563782.2138 5938610.8494 563787.0061 5938608.3939 563789.136 5938600.781 
563798.561 5938602.998 563800.508 5938603.456 563804.043 5938604.288 
563809.544 5938605.582 563811.879 5938606.131 563818.896 5938571.317 
563803.981 5938567.922 563789.865 5938564.711 563773.187 5938560.916 
563756.685 5938557.163 563741.789 5938553.773 563750.284 5938516.989 
563765.792 5938520.95 563782.182 5938525.135 563798.735 5938529.363 
563812.783 5938532.95 563828.291 5938536.912 563838.8248 5938539.603 
563842.802 5938540.619 563852.81 5938543.22 563867.307 5938546.988 
563884.207 5938551.278 563899.718 5938555.215 563915.217 5938559.19 
563930.749 5938563.174 563922.105 5938597.111 563922.434 5938597.207 
563930.159 5938599.475 563936.794 5938601.747 563931.853 5938623.314 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:Z>2</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
      <xplan:bebauungsArt>4000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_D560018D-1F60-49A0-A5C2-E27138491FA5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563857.007 5938612.7665</gml:lowerCorner>
          <gml:upperCorner>563857.007 5938612.7665</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>allgArtDerBaulNutzung</xplan:art>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>bebauungsArt</xplan:art>
      <xplan:art>bauweise</xplan:art>
      <xplan:art>Z</xplan:art>
      <xplan:art>MaxZahlWohnungen</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>1</xplan:index>
      <xplan:index>2</xplan:index>
      <xplan:index>3</xplan:index>
      <xplan:index>4</xplan:index>
      <xplan:index>5</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_51457177-7038-4F01-B6B1-8E1848D7039E" />
      <xplan:position>
        <gml:Point gml:id="Gml_368387C2-CAB7-44C7-974F-758F3FB6FCBA" srsName="EPSG:25832">
          <gml:pos>563857.007 5938612.7665</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_BC162E09-C565-4A08-AB6D-872B4D75D456">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563607.4505 5938455.5578</gml:lowerCorner>
          <gml:upperCorner>564005.8443 5938705.1007</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_50F0B8ED-C8AF-4CC9-BC12-FF296EE5D5E1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563979.5868 5938705.1007 563972.1473 5938703.4914 563954.7126 5938699.4406 
563892.7278 5938685.7438 563791.5299 5938662.4922 563642.0759 5938627.7179 
563612.9173 5938620.7889 563607.4505 5938619.6665 563623.905 5938591.0696 
563703.6068 5938455.5578 563711.963 5938457.685 563695.3526 5938484.6319 
563696.9053 5938490.9663 563699.5181 5938491.6338 563728.017 5938498.914 
563756.1 5938506.087 563778.074 5938511.7 563797.141 5938516.571 
563813.603 5938520.777 563833.015 5938525.735 563855.327 5938531.435 
563872.287 5938535.769 563887.748 5938539.727 563902.28 5938543.439 
563923.593 5938548.885 563942.969 5938553.835 563958.237 5938557.736 
563973.505 5938561.637 563972.2243 5938566.8187 563992.9524 5938571.9216 
564005.8443 5938575.217 563984.7968 5938667.2225 563980.9603 5938667.2225 
563977.2656 5938692.3283 563981.2249 5938693.0194 563979.5868 5938705.1007 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563776.1948 5938653.018 563786.627 5938655.405 563797.461 5938657.884 
563808.946 5938660.512 563819.325 5938662.897 563830.426 5938665.447 
563841.545 5938668.002 563844.5683 5938668.697 563852.908 5938670.614 
563864.249 5938673.221 563878.775 5938676.56 563890.974 5938679.363 
563903.204 5938682.173 563935.442 5938689.581 563950.293 5938692.993 
563959.6546 5938695.1443 563967.447 5938696.935 563974.385 5938691.365 
563980.786 5938649.888 563984.9968 5938622.6056 563985.795 5938617.434 
563991.002 5938583.694 563987.884 5938577.725 563960.05 5938570.689 
563930.749 5938563.174 563915.217 5938559.19 563899.718 5938555.215 
563884.207 5938551.278 563867.307 5938546.988 563852.81 5938543.22 
563842.802 5938540.619 563838.8248 5938539.603 563828.291 5938536.912 
563812.783 5938532.95 563798.735 5938529.363 563782.182 5938525.135 
563765.792 5938520.95 563750.284 5938516.989 563689.117 5938501.375 
563682.5375 5938505.1783 563675.012 5938517.441 563664.525 5938536.156 
563662.1067 5938540.376 563646.65 5938567.349 563641.983 5938575.364 
563637.327 5938583.364 563629.972 5938595.971 563619.736 5938613.482 
563622.652 5938617.56 563654.722 5938625.027 563669.335 5938628.386 
563683.978 5938631.751 563698.805 5938635.158 563710.309 5938637.858 
563721.045 5938640.357 563731.713 5938642.841 563739.2189 5938644.5585 
563753.184 5938647.754 563757.326 5938643.654 563766.107 5938609.28 
563766.6094 5938607.3132 563764.6899 5938602.7116 563765.104 5938601.092 
563766.541 5938595.466 563747.642 5938591.02 563745.161 5938590.436 
563742.278 5938589.758 563743.234 5938584.8492 563799.706 5938598.133 
563798.561 5938602.998 563789.136 5938600.781 563787.0061 5938608.3939 
563782.2138 5938610.8494 563779.9913 5938613.2835 563778.1243 5938616.7022 
563770.608 5938646.2046 563776.1948 5938653.018 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_D6692A33-3307-443E-83EB-A8B22088E52F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563997.0912 5938435.8786</gml:lowerCorner>
          <gml:upperCorner>564049.368 5938709.4651</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3ABE3ABC-6DB5-47F8-ABAA-F748AD86A8B2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">564000.843 5938700.213 563999.7622 5938709.4651 563997.0912 5938708.8873 
563998.6875 5938695.7975 564000.275 5938695.7975 564003.45 5938669.736 
564002.2593 5938669.6037 564007.706 5938631.2819 564038.0367 5938435.8786 
564049.368 5938437.904 564047.096 5938449.734 564036.924 5938501.658 
564032.229 5938525.641 564028.405 5938545.299 564025.294 5938561.085 
564020.158 5938587.286 564013.744 5938620.627 564010.85 5938635.539 
564009.564 5938640.635 564009.306 5938642.623 564008.811 5938646.417 
564008.476 5938650.932 564000.843 5938700.213 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_8F951356-99FD-4170-A004-E855ED2CA892">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563977.2656 5938434.7517</gml:lowerCorner>
          <gml:upperCorner>564038.0367 5938708.8873</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3818CCC4-140E-4AEC-B242-D49B226EE097" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563997.0912 5938708.8873 563979.5868 5938705.1007 563981.2249 5938693.0194 
563977.2656 5938692.3283 563980.9603 5938667.2225 563984.7968 5938667.2225 
564005.8443 5938575.217 564007.9233 5938575.7484 564010.0664 5938565.0328 
564014.3725 5938534.7829 564020.0478 5938502.8979 564024.1754 5938477.8709 
564028.7394 5938450.9865 564031.7325 5938434.7517 564038.0367 5938435.8786 
564007.706 5938631.2819 564002.2593 5938669.6037 564003.45 5938669.736 
564000.275 5938695.7975 563998.6875 5938695.7975 563997.0912 5938708.8873 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_DDBDB2E8-40FD-43C8-B463-7BA9B6AE24B6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564059.0817 5938266.3268</gml:lowerCorner>
          <gml:upperCorner>564059.0817 5938266.3268</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:text>Oberkante Tunnel</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">13.48</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Point gml:id="Gml_0B608B63-1DB1-4895-9F74-A2A3747B0AE0" srsName="EPSG:25832">
          <gml:pos>564059.0817 5938266.3268</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_6310559C-9C76-41DB-BF43-3077DA54BA7E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564005.5931 5938612.9299</gml:lowerCorner>
          <gml:upperCorner>564005.5931 5938612.9299</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:text>Oberkante Tunnel</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">16.41</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Point gml:id="Gml_570129AD-74B7-44A4-8934-D267347B74DB" srsName="EPSG:25832">
          <gml:pos>564005.5931 5938612.9299</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_0272CE30-3269-4FC8-82E8-0DE302AF8B1A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563986.9929 5938704.079</gml:lowerCorner>
          <gml:upperCorner>563986.9929 5938704.079</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:text>Straßenhöhe</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>          
          <xplan:h uom="m">20.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Point gml:id="Gml_8790A26B-BADC-4333-AA81-403E19519F36" srsName="EPSG:25832">
          <gml:pos>563986.9929 5938704.079</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_0B557087-A310-49C0-A897-7040AF67BF33">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564003.5252 5938574.6242</gml:lowerCorner>
          <gml:upperCorner>564003.5252 5938574.6242</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:text>Straßenhöhe</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>         
          <xplan:h uom="m">21.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Point gml:id="Gml_13CE181F-7A10-4410-B29D-BF8E63944EE8" srsName="EPSG:25832">
          <gml:pos>564003.5252 5938574.6242</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_7A344658-217C-4644-AECB-6050290C78C8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563756.1 5938471.264</gml:lowerCorner>
          <gml:upperCorner>563764.965 5938506.087</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_84F37C2A-9045-45F9-98A7-A902A0C916A1" srsName="EPSG:25832">
          <gml:posList>563756.1 5938506.087 563764.965 5938471.264 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_D818C653-7D8F-44B5-9DCB-64C00E5A4BC1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563646.65 5938567.349</gml:lowerCorner>
          <gml:upperCorner>563708.572 5938625.027</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_12D316B5-5E10-48AE-9462-422B8A84B2AF" srsName="EPSG:25832">
          <gml:posList>563654.722 5938625.027 563659.219 5938606.121 563661.08 5938606.48 
563667.418 5938596.175 563676.815 5938598.041 563691.373 5938600.932 
563702.595 5938603.161 563703.912 5938598.582 563705.361 5938593.373 
563708.572 5938581.831 563680.228 5938575.201 563646.65 5938567.349 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_502F4C02-9388-4E4D-83FC-FC7EBE975871">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563708.572 5938581.831</gml:lowerCorner>
          <gml:upperCorner>563742.278 5938589.758</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_7EC0F48A-A769-4300-B716-0B941E6A5CC3" srsName="EPSG:25832">
          <gml:posList>563742.278 5938589.758 563734.458 5938587.919 563731.519 5938587.228 
563730.997 5938587.105 563723.7 5938585.389 563708.572 5938581.831 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_40ACDD03-F118-41D3-9C66-975AC8948FB2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563723.7 5938516.989</gml:lowerCorner>
          <gml:upperCorner>563750.284 5938585.389</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_74BDA0E0-E261-4893-8DF8-2C758A5F5A6B" srsName="EPSG:25832">
          <gml:posList>563723.7 5938585.389 563731.9177 5938551.5221 563741.789 5938553.773 
563750.284 5938516.989 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_9D4996D0-A8EB-4F5C-A504-5BAD248CC378">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563741.789 5938553.773</gml:lowerCorner>
          <gml:upperCorner>563818.896 5938606.131</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_DACDF209-40B1-4C96-964B-0B26C39B97A1" srsName="EPSG:25832">
          <gml:posList>563798.561 5938602.998 563800.508 5938603.456 563804.043 5938604.288 
563809.544 5938605.582 563811.879 5938606.131 563818.896 5938571.317 
563803.981 5938567.922 563789.865 5938564.711 563773.187 5938560.916 
563756.685 5938557.163 563741.789 5938553.773 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_C11942A6-EA49-4213-A785-557590B2EA0A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563925.957 5938570.689</gml:lowerCorner>
          <gml:upperCorner>563960.05 5938689.581</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_7465244A-EC99-441E-BD47-C7828496BA0F" srsName="EPSG:25832">
          <gml:posList>563935.442 5938689.581 563942.599 5938658.109 563925.957 5938654.396 
563932.827 5938623.58 563931.853 5938623.314 563936.794 5938601.747 
563950.816 5938606.235 563950.991 5938605.56 563953.806 5938594.723 
563960.05 5938570.689 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_24421451-7209-4094-BFE3-7B288B8FA4EA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563922.105 5938563.174</gml:lowerCorner>
          <gml:upperCorner>563936.794 5938601.747</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_77B59C66-3654-4A8D-A268-CCCF71FEDD73" srsName="EPSG:25832">
          <gml:posList>563936.794 5938601.747 563930.159 5938599.475 563922.434 5938597.207 
563922.105 5938597.111 563930.749 5938563.174 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_D0574A6E-E0DF-4301-8F2E-9D5F053EDCB5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563707.5628 5938470.0771</gml:lowerCorner>
          <gml:upperCorner>563975.9972 5938555.1601</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6DF9FF8D-E9E4-4DD7-8960-288465AA7A45" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563975.9972 5938537.6473 563971.8275 5938555.1601 563707.5628 5938487.6407 
563711.5339 5938470.0771 563975.9972 5938537.6473 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_853695D1-0AA6-4436-81A0-51C6FB450932">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563749.1729 5938522.6803</gml:lowerCorner>
          <gml:upperCorner>563954.3961 5938591.5051</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E41F61BE-A03C-42B9-8DAC-A4A41D9E8E22" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563954.3961 5938573.9038 563950.5732 5938591.5051 563749.1729 5938540.0928 
563753.7357 5938522.6803 563954.3961 5938573.9038 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_292D9FB0-4738-4E03-9C0A-C98C2FAB9B3F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563967.844 5938580.548</gml:lowerCorner>
          <gml:upperCorner>563985.069 5938611.141</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_24E0B620-53F5-4390-A046-3C0679B55FE0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563985.069 5938582.758 563979.842 5938611.141 563967.844 5938608.931 
563970.871 5938592.495 563972.928 5938581.324 563973.071 5938580.548 
563985.069 5938582.758 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>3</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_F05E81ED-8034-4251-BBBE-6965F8DE2B32">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563963.555 5938579.2004</gml:lowerCorner>
          <gml:upperCorner>563973.071 5938592.495</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C8B8DCB1-3013-4C5E-9387-A453581D5157" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563973.071 5938580.548 563972.928 5938581.324 563970.871 5938592.495 
563963.555 5938591.147 563965.612 5938579.976 563965.7548 5938579.2004 
563973.071 5938580.548 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>2</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_2DAA2CB6-600F-4BC6-B58F-5FD09EAED3F4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563952.09 5938647.4314</gml:lowerCorner>
          <gml:upperCorner>563974.092 5938688.558</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A03EAAFB-FFB9-4FB9-9A89-891F0779782C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563968.415 5938688.558 563952.09 5938685.297 563954.781 5938673.783 
563956.997 5938674.287 563958.193 5938673.552 563960.055 5938660.954 
563963.1894 5938647.4314 563974.092 5938648.9533 563968.415 5938688.558 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>3</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_24E72DBD-22F2-4068-BD21-2E3F08FE1F0C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563932.632 5938629.075</gml:lowerCorner>
          <gml:upperCorner>563965.9658 5938660.954</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3754BCBC-44DE-43C1-90A1-BE7FD05B1BA6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563965.9658 5938635.4535 563964.5514 5938641.5552 563963.1894 5938647.4314 
563960.055 5938660.954 563958.5 5938660.575 563958.51 5938660.534 
563932.632 5938654.4017 563938.7186 5938629.075 563965.9658 5938635.4535 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>2</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_440C031F-C426-48A2-838B-324EB264A6B6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563955.103 5938612.7811</gml:lowerCorner>
          <gml:upperCorner>563975.5608 5938630.4328</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_5F03407B-CB62-4E58-89FA-FA239D620AC0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563975.5608 5938617.0357 563972.7546 5938630.4328 563955.103 5938626.2688 
563958.0902 5938612.7811 563975.5608 5938617.0357 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>3</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_2AF1A83D-9AF3-4190-81EB-33F046472BBE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563781.1797 5938625.9747</gml:lowerCorner>
          <gml:upperCorner>563935.3174 5938673.4903</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6F82375E-F0EE-4EB2-A122-346917FD6F55" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563924.6217 5938653.2972 563924.0625 5938656.2879 563935.3174 5938658.5128 
563932.0725 5938673.4903 563906.505 5938667.5887 563902.9448 5938666.7669 
563898.2844 5938665.6912 563894.2894 5938664.7691 563890.7612 5938663.9547 
563886.2596 5938662.9156 563882.1013 5938661.9558 563867.5962 5938658.6077 
563864.526 5938657.899 563856.256 5938656.004 563847.393 5938653.974 
563847.3995 5938653.9458 563844.9022 5938653.3694 563842.0143 5938652.7028 
563842.005 5938652.743 563833.784 5938650.852 563833.7947 5938650.8055 
563825.613 5938648.917 563822.7044 5938648.2456 563820.3551 5938647.7034 
563812.3287 5938645.8507 563804.7365 5938644.0982 563801.5093 5938643.3533 
563799.652 5938650.0197 563799.5737 5938650.3009 563792.429 5938648.626 
563781.1797 5938645.9889 563785.0177 5938628.4006 563804.4 5938632.8538 
563806.3579 5938625.9747 563924.6217 5938653.2972 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_65E3CC09-481F-4FAD-B134-1F509FED8657">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563739.0061 5938567.3631</gml:lowerCorner>
          <gml:upperCorner>563812.066 5938593.5469</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_94A54459-653D-4708-9912-F2C7FF162F25" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_05BB2331-0E9F-4FA1-A37F-C2AF5842F6BD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563809.6394 5938593.5469 563793.7769 5938589.7207 563793.1676 5938592.151 
563777.3938 5938588.5216 563777.7468 5938587.1276 563762.1783 5938583.4963 
563762.3849 5938582.6045 563739.0061 5938577.0751 563741.3905 5938567.3631 
563764.6422 5938572.8624 563764.5126 5938573.4219 563764.4355 5938573.7544 
563780.202 5938577.4318 563780.0277 5938578.1201 563779.8491 5938578.8253 
563795.5612 5938582.4404 563796.1606 5938580.0088 563800.9918 5938581.1742 
563812.0336 5938583.8376 563812.066 5938583.8454 563809.6394 5938593.5469 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>2</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_94A54459-653D-4708-9912-F2C7FF162F25">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563775.348 5938581.1897</gml:lowerCorner>
          <gml:upperCorner>563775.348 5938581.1897</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:stylesheetId>Default</xplan:stylesheetId>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_65E3CC09-481F-4FAD-B134-1F509FED8657" />
      <xplan:position>
        <gml:Point gml:id="Gml_877737E1-F569-4E64-9A5E-DBCF65E3AADF" srsName="EPSG:25832">
          <gml:pos>563775.348 5938581.1897</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_3F1BCA68-3E9D-4B5E-BAA7-2D711C939886">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563741.3905 5938558.6224</gml:lowerCorner>
          <gml:upperCorner>563814.25 5938583.8454</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0341ABE2-E799-4552-9C25-8300240D3D33" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563812.066 5938583.8454 563812.0336 5938583.8376 563800.9918 5938581.1742 
563796.1606 5938580.0088 563795.5612 5938582.4404 563779.8491 5938578.8253 
563780.0277 5938578.1201 563780.202 5938577.4318 563764.4355 5938573.7544 
563764.5126 5938573.4219 563764.6422 5938572.8624 563741.3905 5938567.3631 
563743.5364 5938558.6224 563766.6737 5938564.0946 563766.467 5938564.9866 
563782.4116 5938568.7056 563782.0589 5938570.0985 563797.7154 5938573.7009 
563798.3145 5938571.2703 563814.25 5938575.1141 563812.066 5938583.8454 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>1</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_0505475B-90A1-4DD7-90DB-EFF094411921">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563676.212 5938504.502</gml:lowerCorner>
          <gml:upperCorner>563699.215 5938536.573</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_68534C8F-AD27-4C85-98DB-89E75C3455F9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563687.899 5938536.573 563682.529 5938535.246 563676.212 5938533.684 
563679.904 5938518.753 563683.841 5938519.726 563687.609 5938504.502 
563699.215 5938507.756 563695.549 5938522.604 563691.593 5938521.627 
563687.899 5938536.573 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_24F1C887-253A-4AFB-A195-E631D9E887BD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563661.362 5938541.56</gml:lowerCorner>
          <gml:upperCorner>563682.197 5938569.015</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C1B9A49A-20DC-4701-9063-67F4F1ABAA11" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563672.881 5938569.015 563661.362 5938566.352 563664.626 5938552.208 
563667.598 5938552.894 563670.21 5938541.56 563682.197 5938544.326 
563679.582 5938555.663 563676.146 5938554.869 563672.881 5938569.015 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_4BF18F6E-0652-48AD-9E41-7FCAFE668DB9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563696.212 5938512.766</gml:lowerCorner>
          <gml:upperCorner>563728.572 5938568.2965</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BB424D55-94EF-4408-8286-F976251B182A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563707.9056 5938568.2965 563696.212 5938565.5793 563699.509 5938553.0663 
563699.8558 5938551.7504 563702.811 5938552.5109 563704.6792 5938545.3108 
563709.5299 5938526.6162 563713.42 5938527.4076 563716.9623 5938512.766 
563728.572 5938515.81 563724.929 5938530.868 563721.043 5938530.024 
563716.384 5938547.98 563714.4325 5938555.5013 563711.4774 5938554.7409 
563707.9056 5938568.2965 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_D77479E6-AA07-4B9A-89E9-22827ADBB860">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563626.2608 5938570.5125</gml:lowerCorner>
          <gml:upperCorner>563666.0814 5938615.6745</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A1C6B3EC-2D13-426E-8023-AED54A1B0E54" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563641.6961 5938615.6745 563626.2608 5938612.4295 563650.3457 5938570.5125 
563666.0814 5938579.2565 563657.934 5938594.2909 563654.4654 5938593.6019 
563641.6961 5938615.6745 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_38D78975-58ED-4E63-99B9-438693A59C90">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563660.9987 5938602.7357</gml:lowerCorner>
          <gml:upperCorner>563759.5443 5938641.7251</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_412C79FF-99DD-4B66-9CC7-D960387C028C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563759.5443 5938624.215 563755.3699 5938641.7251 563660.9987 5938620.1877 
563663.7777 5938606.9873 563665.4279 5938602.7357 563759.5443 5938624.215 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_A4713798-1F7B-4375-8892-10F63BF19403">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563707.5628 5938470.0771</gml:lowerCorner>
          <gml:upperCorner>563975.9972 5938555.1601</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_FD459A67-57C6-4A8F-BC49-D99143511245" srsName="EPSG:25832">
          <gml:posList>563971.8275 5938555.1601 563975.9972 5938537.6473 563711.5339 5938470.0771 
563707.5628 5938487.6407 563971.8275 5938555.1601 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_D61B537A-E474-4AD4-BF2A-F6CC560521E7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563749.1729 5938522.6803</gml:lowerCorner>
          <gml:upperCorner>563954.3961 5938591.5051</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_10A1C9AE-79BC-4908-9664-3BF1C5A9519A" srsName="EPSG:25832">
          <gml:posList>563753.7357 5938522.6803 563749.1729 5938540.0928 563950.5732 5938591.5051 
563954.3961 5938573.9038 563753.7357 5938522.6803 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_EAA62AC7-A903-4D21-B75D-651014777E06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563967.844 5938580.548</gml:lowerCorner>
          <gml:upperCorner>563985.069 5938611.141</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_D0283035-E7F7-4590-9A15-FB656AAE8FF5" srsName="EPSG:25832">
          <gml:posList>563973.071 5938580.548 563972.928 5938581.324 563970.871 5938592.495 
563967.844 5938608.931 563979.842 5938611.141 563985.069 5938582.758 
563973.071 5938580.548 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_1B80AD6D-18F2-47A1-94D3-E1E1199150B6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563963.555 5938579.2004</gml:lowerCorner>
          <gml:upperCorner>563973.071 5938592.495</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_59930C94-0FE7-4A8A-BFEF-24BA9F2B655A" srsName="EPSG:25832">
          <gml:posList>563973.071 5938580.548 563965.7548 5938579.2004 563965.612 5938579.976 
563963.555 5938591.147 563970.871 5938592.495 563972.928 5938581.324 
563973.071 5938580.548 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_18D8BE3C-F1BE-441C-9AE5-67F3AE0B91FD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563952.09 5938647.4314</gml:lowerCorner>
          <gml:upperCorner>563974.092 5938688.558</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_32EDFFC3-916A-4C45-B009-DC1711CE1F76" srsName="EPSG:25832">
          <gml:posList>563968.415 5938688.558 563974.092 5938648.9533 563963.1894 5938647.4314 
563960.055 5938660.954 563958.193 5938673.552 563956.997 5938674.287 
563954.781 5938673.783 563952.09 5938685.297 563968.415 5938688.558 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_0D40A5C9-7E8B-43E9-8AA2-E889E5614DDD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563932.632 5938629.075</gml:lowerCorner>
          <gml:upperCorner>563965.9658 5938660.954</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_17547819-221C-49C6-A28D-7332C9F49794" srsName="EPSG:25832">
          <gml:posList>563960.055 5938660.954 563963.1894 5938647.4314 563964.5514 5938641.5552 
563965.9658 5938635.4535 563938.7186 5938629.075 563932.632 5938654.4017 
563958.51 5938660.534 563958.5 5938660.575 563960.055 5938660.954 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_741A93D9-88C5-4E46-82B2-CC14C5CA3346">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563955.103 5938612.7811</gml:lowerCorner>
          <gml:upperCorner>563975.5608 5938630.4328</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_E416C529-8360-4E8D-B53D-0D578E8EDDFA" srsName="EPSG:25832">
          <gml:posList>563975.5608 5938617.0357 563958.0902 5938612.7811 563955.103 5938626.2688 
563972.7546 5938630.4328 563975.5608 5938617.0357 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_3F61F21D-3ADD-45CD-B287-BE8243F6EB78">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563781.1797 5938625.9747</gml:lowerCorner>
          <gml:upperCorner>563935.3174 5938673.4903</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_4A0AFEEE-6402-4857-A282-A85B9A1221E9" srsName="EPSG:25832">
          <gml:posList>563781.1797 5938645.9889 563792.429 5938648.626 563799.5737 5938650.3009 
563799.652 5938650.0197 563801.5093 5938643.3533 563804.7365 5938644.0982 
563812.3287 5938645.8507 563820.3551 5938647.7034 563822.7044 5938648.2456 
563825.613 5938648.917 563833.7947 5938650.8055 563833.784 5938650.852 
563842.005 5938652.743 563842.0143 5938652.7028 563844.9022 5938653.3694 
563847.3995 5938653.9458 563847.393 5938653.974 563856.256 5938656.004 
563864.526 5938657.899 563867.5962 5938658.6077 563882.1013 5938661.9558 
563886.2596 5938662.9156 563890.7612 5938663.9547 563894.2894 5938664.7691 
563898.2844 5938665.6912 563902.9448 5938666.7669 563906.505 5938667.5887 
563932.0725 5938673.4903 563935.3174 5938658.5128 563924.0625 5938656.2879 
563924.6217 5938653.2972 563806.3579 5938625.9747 563804.4 5938632.8538 
563785.0177 5938628.4006 563781.1797 5938645.9889 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ADD3996C-DF0B-4F1A-8D23-8F348079FCB5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563739.0061 5938567.3631</gml:lowerCorner>
          <gml:upperCorner>563812.066 5938593.5469</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_061C4BF3-E635-49CF-9244-90BBA39270BC" srsName="EPSG:25832">
          <gml:posList>563793.1676 5938592.151 563793.7769 5938589.7207 563809.6394 5938593.5469 
563812.066 5938583.8454 563812.0336 5938583.8376 563800.9918 5938581.1742 
563796.1606 5938580.0088 563795.5612 5938582.4404 563779.8491 5938578.8253 
563780.0277 5938578.1201 563780.202 5938577.4318 563764.4355 5938573.7544 
563764.5126 5938573.4219 563764.6422 5938572.8624 563741.3905 5938567.3631 
563739.0061 5938577.0751 563762.3849 5938582.6045 563762.1783 5938583.4963 
563777.7468 5938587.1276 563777.3938 5938588.5216 563793.1676 5938592.151 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_CCC36E84-8CA4-4FBA-B89E-04616BB04F42">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563741.3905 5938558.6224</gml:lowerCorner>
          <gml:upperCorner>563814.25 5938583.8454</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_A1C37D29-E68D-484A-8732-6CD603B2AC4D" srsName="EPSG:25832">
          <gml:posList>563795.5612 5938582.4404 563796.1606 5938580.0088 563800.9918 5938581.1742 
563812.0336 5938583.8376 563812.066 5938583.8454 563814.25 5938575.1141 
563798.3145 5938571.2703 563797.7154 5938573.7009 563782.0589 5938570.0985 
563782.4116 5938568.7056 563766.467 5938564.9866 563766.6737 5938564.0946 
563743.5364 5938558.6224 563741.3905 5938567.3631 563764.6422 5938572.8624 
563764.5126 5938573.4219 563764.4355 5938573.7544 563780.202 5938577.4318 
563780.0277 5938578.1201 563779.8491 5938578.8253 563795.5612 5938582.4404 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_D3A075B2-ED6C-49AB-89F7-0637D85D0F3A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563676.212 5938504.502</gml:lowerCorner>
          <gml:upperCorner>563699.215 5938536.573</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_6878C211-81C1-4131-BB83-1F6805F3C6C1" srsName="EPSG:25832">
          <gml:posList>563687.609 5938504.502 563683.841 5938519.726 563679.904 5938518.753 
563676.212 5938533.684 563682.529 5938535.246 563687.899 5938536.573 
563691.593 5938521.627 563695.549 5938522.604 563699.215 5938507.756 
563687.609 5938504.502 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_D9A97FDA-CE7D-4D6F-B1C3-36E3300A35C4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563661.362 5938541.56</gml:lowerCorner>
          <gml:upperCorner>563682.197 5938569.015</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_A7834FFF-533F-4D68-AD07-51070371EC3C" srsName="EPSG:25832">
          <gml:posList>563672.881 5938569.015 563676.146 5938554.869 563679.582 5938555.663 
563682.197 5938544.326 563670.21 5938541.56 563667.598 5938552.894 
563664.626 5938552.208 563661.362 5938566.352 563672.881 5938569.015 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_B64D109F-9251-464B-8DE4-6F99DB175F73">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563696.212 5938512.766</gml:lowerCorner>
          <gml:upperCorner>563728.572 5938568.2965</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_A3D5382A-07CE-4407-93ED-B7342FA80AE4" srsName="EPSG:25832">
          <gml:posList>563728.572 5938515.81 563716.9623 5938512.766 563713.42 5938527.4076 
563709.5299 5938526.6162 563704.6792 5938545.3108 563702.811 5938552.5109 
563699.8558 5938551.7504 563699.509 5938553.0663 563696.212 5938565.5793 
563707.9056 5938568.2965 563711.4774 5938554.7409 563714.4325 5938555.5013 
563716.384 5938547.98 563721.043 5938530.024 563724.929 5938530.868 
563728.572 5938515.81 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_8C942AEA-A7BA-4130-8D5A-22557CEE8CE6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563626.2608 5938570.5125</gml:lowerCorner>
          <gml:upperCorner>563666.0814 5938615.6745</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_F3A2DDF4-01A9-4BBD-AF1D-FA6DE9D42B83" srsName="EPSG:25832">
          <gml:posList>563650.3457 5938570.5125 563626.2608 5938612.4295 563641.6961 5938615.6745 
563654.4654 5938593.6019 563657.934 5938594.2909 563666.0814 5938579.2565 
563650.3457 5938570.5125 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_EA7E09B4-A62C-4A77-B664-4D8240A92298">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563660.9987 5938602.7357</gml:lowerCorner>
          <gml:upperCorner>563759.5443 5938641.7251</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_E930BCF8-90D1-4064-AA95-CE2935D4E4FE" srsName="EPSG:25832">
          <gml:posList>563755.3699 5938641.7251 563759.5443 5938624.215 563665.4279 5938602.7357 
563663.7777 5938606.9873 563660.9987 5938620.1877 563755.3699 5938641.7251 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_581CFA29-49E9-4FA8-8B6C-AB6AD630F81A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563878.4212 5938240.8079</gml:lowerCorner>
          <gml:upperCorner>564044.476 5938326.477</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_06E6C55B-639A-4450-9D6D-C00978ACEDC9" srsName="EPSG:25832">
          <gml:posList>563878.4212 5938240.8079 563963.851 5938253.214 564044.476 5938264.923 
564036.391 5938326.477 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_8048ACEE-4D83-447D-BA7F-7EB994B6C8D1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564066.8745 5938264.299</gml:lowerCorner>
          <gml:upperCorner>564077.522 5938331.7459</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_8FE9FC36-3C54-4FC4-8C64-CE33A911D4CB" srsName="EPSG:25832">
          <gml:posList>564066.8745 5938331.7459 564077.522 5938264.299 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_4BAB4B89-19C7-40EE-A3FA-A6307C95640D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563619.736 5938501.375</gml:lowerCorner>
          <gml:upperCorner>563991.002 5938696.935</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_831DA514-0E8D-4941-A1A0-78D39EAB1C95" srsName="EPSG:25832">
          <gml:posList>563776.1948 5938653.018 563770.608 5938646.2046 563778.1243 5938616.7022 
563779.9913 5938613.2835 563782.2138 5938610.8494 563787.0061 5938608.3939 
563789.136 5938600.781 563798.561 5938602.998 563799.706 5938598.133 
563743.234 5938584.8492 563742.278 5938589.758 563745.161 5938590.436 
563747.642 5938591.02 563766.541 5938595.466 563765.104 5938601.092 
563764.6899 5938602.7116 563766.6094 5938607.3132 563766.107 5938609.28 
563757.326 5938643.654 563753.184 5938647.754 563739.2189 5938644.5585 
563731.713 5938642.841 563721.045 5938640.357 563710.309 5938637.858 
563698.805 5938635.158 563683.978 5938631.751 563669.335 5938628.386 
563654.722 5938625.027 563622.652 5938617.56 563619.736 5938613.482 
563629.972 5938595.971 563637.327 5938583.364 563641.983 5938575.364 
563646.65 5938567.349 563662.1067 5938540.376 563664.525 5938536.156 
563675.012 5938517.441 563682.5375 5938505.1783 563689.117 5938501.375 
563750.284 5938516.989 563765.792 5938520.95 563782.182 5938525.135 
563798.735 5938529.363 563812.783 5938532.95 563828.291 5938536.912 
563838.8248 5938539.603 563842.802 5938540.619 563852.81 5938543.22 
563867.307 5938546.988 563884.207 5938551.278 563899.718 5938555.215 
563915.217 5938559.19 563930.749 5938563.174 563960.05 5938570.689 
563987.884 5938577.725 563991.002 5938583.694 563985.795 5938617.434 
563984.9968 5938622.6056 563980.786 5938649.888 563974.385 5938691.365 
563967.447 5938696.935 563959.6546 5938695.1443 563950.293 5938692.993 
563935.442 5938689.581 563903.204 5938682.173 563890.974 5938679.363 
563878.775 5938676.56 563864.249 5938673.221 563852.908 5938670.614 
563844.5683 5938668.697 563841.545 5938668.002 563830.426 5938665.447 
563819.325 5938662.897 563808.946 5938660.512 563797.461 5938657.884 
563786.627 5938655.405 563776.1948 5938653.018 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_9CEDC62D-5545-4010-A86C-97C2C797FB01">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564028.405 5938449.734</gml:lowerCorner>
          <gml:upperCorner>564047.096 5938545.299</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_7E74CBCA-B774-49D7-8668-BB06C1A58695" srsName="EPSG:25832">
          <gml:posList>564028.405 5938545.299 564032.229 5938525.641 564036.924 5938501.658 
564047.096 5938449.734 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_2A627C81-18C6-4577-8441-E91788270011">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563999.7622 5938561.085</gml:lowerCorner>
          <gml:upperCorner>564025.294 5938709.4651</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_3EE33825-DD8C-4FB9-8704-0E597292C50F" srsName="EPSG:25832">
          <gml:posList>563999.7622 5938709.4651 564000.843 5938700.213 564008.476 5938650.932 
564008.811 5938646.417 564009.306 5938642.623 564009.564 5938640.635 
564010.85 5938635.539 564013.744 5938620.627 564020.158 5938587.286 
564025.294 5938561.085 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_D8BDCB0B-9D36-45DA-9A2D-8AD3F243D088">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563695.3526 5938457.685</gml:lowerCorner>
          <gml:upperCorner>563973.505 5938561.637</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:LineString gml:id="Gml_8E1DBA65-763A-43DB-A0CD-176CAA09E879" srsName="EPSG:25832">
          <gml:posList>563973.505 5938561.637 563958.237 5938557.736 563942.969 5938553.835 
563923.593 5938548.885 563902.28 5938543.439 563887.748 5938539.727 
563872.287 5938535.769 563855.327 5938531.435 563833.015 5938525.735 
563813.603 5938520.777 563797.141 5938516.571 563778.074 5938511.7 
563756.1 5938506.087 563728.017 5938498.914 563699.5181 5938491.6338 
563696.9053 5938490.9663 563695.3526 5938484.6319 563711.963 5938457.685 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_C2E1C009-F1F4-497E-9BDB-DDB53F7CFFB8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564043.2851 5938260.6389</gml:lowerCorner>
          <gml:upperCorner>564064.7742 5938329.5851</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_979957EA-5732-4C62-BC41-2DC7F7E45E56" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">564064.7742 5938262.346 564054.3732 5938329.5851 564043.2851 5938327.6686 
564053.6314 5938260.6389 564064.7742 5938262.346 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_E37BC97D-097D-4691-9709-D50598DAFAAB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>563977.2656 5938434.7517</gml:lowerCorner>
          <gml:upperCorner>564038.0367 5938708.8873</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:uuid>A3DC5063-81C0-48AF-B1D4-801A496B8E85</xplan:uuid>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#GML_D614C448-56DD-459F-8393-83A1B45A77BC" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E736AAD2-D8DC-4358-8571-5B56309FAEAB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">563997.0912 5938708.8873 563979.5868 5938705.1007 563981.2249 5938693.0194 
563977.2656 5938692.3283 563980.9603 5938667.2225 563984.7968 5938667.2225 
564005.8443 5938575.217 564007.9233 5938575.7484 564010.0664 5938565.0328 
564014.3725 5938534.7829 564020.0478 5938502.8979 564024.1754 5938477.8709 
564028.7394 5938450.9865 564031.7325 5938434.7517 564038.0367 5938435.8786 
564007.706 5938631.2819 564002.2593 5938669.6037 564003.45 5938669.736 
564000.275 5938695.7975 563998.6875 5938695.7975 563997.0912 5938708.8873 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_RasterplanBasis gml:id="Gml_758104A5-BBF2-4411-BD92-A16D3EA35749">
      <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>BPlan003_4-1.pgw</xplan:georefURL>
          <xplan:referenzURL>BPlan003_4-1.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
    </xplan:XP_RasterplanBasis>
  </gml:featureMember>
</xplan:XPlanAuszug>
