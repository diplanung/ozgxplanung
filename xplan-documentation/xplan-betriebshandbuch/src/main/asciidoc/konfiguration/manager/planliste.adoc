[[planliste]]
=== Planliste

Die angezeigten Spalten der Planliste auf der Hauptseite können in der Datei _<XPLANBOX_CONFIG>/managerWebConfiguration.properties_ konfiguriert werden.

Im folgenden Beispiel werden die Spalten 'Nummer' und 'Rechtsstand' aus der Planliste entfernt:

----
hiddenColumns=NUMBER,LEGISLATIONSTATUS
----

Mehrere Spalten müssen komma-separiert angegeben werden. Der folgenden Tabelle kann die Zuordnung des Spaltentitels und der Angabe in der Konfiguration entnommen werden:

[width="100%",cols="50%,50%",options="header"]
|===
|*Titel der Spalte in der Oberfläche* |*Angabe in der Konfiguration*
|Name |NAME
|ID |ID
|Nummer |NUMBER
|Bezirk/Gemeinde |COMMUNITY
|XPlanGML-Version |VERSION
|Planart |TYPE
|sonstige Planart |ADDITIONALTYPE
|Rechtsstand |LEGISLATIONSTATUS
|Datum Veröffentlichung |RELEASEDATE
|Datum Import |IMPORTDATE
|Planstatus |PLANSTATUS
|Gültigkeit|VALIDITIYPERIOD
|===