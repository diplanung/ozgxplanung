[[installationskomponenten]]
=== Überblick der Installationskomponenten

Die xPlanBox umfasst die folgenden Komponenten, die zusammengenommen die Liefereinheit darstellen. Speichern Sie die Distributionsdatei in einem lokalen Verzeichnis und entpacken Sie die ZIP-Datei. Prüfen Sie die Vollständigkeit der Distributionsdatei anhand der folgenden Auflistung.

[width="100%",cols="30%,40%,30%",options="header"]
|===
|*Komponente* |*Konfigurationen* |*Beschreibung*
|xplan-dokumente-api.war |xplan-dokumente-config.zip|<<xplandokumente-api>>
|xplan-manager-web.war |xplan-manager-config-default.zip, xplan-manager-workspace.zip|<<xplanmanager-web>>
|xplan-manager-api.war |xplan-manager-config-default.zip, xplan-manager-workspace.zip|<<xplandokumente-api>>
|xplan-services-wfs-syn.war |xplan-services-wfs-syn-workspace.zip|<<xplanwfs>>
|xplan-services-wfs.war |xplan-services-wfs-workspace.zip|<<xplanwfs>>
|xplan-services-wms.war |xplan-services-wms-workspace.zip|<<xplanwms>>
|xplan-validator-web.war |xplan-validator-config.zip, xplan-validator-workspace.zip|<<xplanvalidator-web>>
|xplan-validator-api.war |xplan-validator-config.zip, xplan-validator-workspace.zip|<<xplanvalidator-api>>
|xplan-validator-executor.jar |-|<<xplanvalidator-executor>>
|xplan-webpages-default.war |-|<<xplanresources>>
|xplan-webservices-inspireplu.war |xplan-webservices-inspireplu-workspace.zip|<<xplaninspirepluwfs>>,<<xplaninspirepluwms>>
|xplan-webservices-validator-wms.war |xplan-webservices-validator-wms-workspace.zip|<<xplanvalidator-wms>>
|xplan-cli.zip | - |<<xplanclitools>>
|externe Komponente |xplan-mapserver-config.zip | Konfiguration für <<mapserver,MapServer>>
|externe Komponente |xplan-mapproxy-config.zip | Konfiguration für <<mapproxy,MapProxy>>
|externe Komponente |xplan-database-scripts.jar | Liquibase Changelog-Dateien für <<xplandb,XPlanDB>>
|xplan-benutzerhandbuch-html.zip | - | Benutzerhandbuch für die Komponenten der xPlanBox in HTML-Format
|xplan-benutzerhandbuch-pdf.zip | - | Benutzerhandbuch für die Komponenten der xPlanBox
|xplan-betriebshandbuch-html.zip | - | Betriebshandbuch für die Komponenten der xPlanBox in HTML-Format
|xplan-betriebshandbuch-pdf.zip | - | Betriebshandbuch für die Komponenten der xPlanBox in PDF-Format
|===

NOTE: Stellen Sie vor Beginn der Installation sicher, dass die Distributionsdatei vollständig ist. Bitte kontaktieren Sie https://www.lat-lon.de[lat/lon GmbH], per E-Mail an info@lat-lon.de, wenn Installationskomponenten fehlen.