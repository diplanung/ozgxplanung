[[konfiguration-der-datenbank]]
=== Erstellen der Datenbank XPlanDB

[[konfiguration-xplandb]]
==== Vorbereitung

Für das Erstellen der Datenbank XPlanDB ist ein Datenbankwerkzeug, wie z. B. https://www.pgadmin.org[pgAdmin] oder das Kommandozeilentool `psql` erforderlich.

Die folgenden Schritte müssen ausgeführt werden:

- Verbindung zum PostgreSQL-Server als Super User (Rolle _postgres_) herstellen
- Anlegen der Datenbank und der Datenbankbenutzer `DB_INIT_USER` (1) mit Berechtigungen für DDL und DCL sowie `DB_USER` (2) mit Berechtigungen für DQL und DML:
[source,sql]
----
CREATE USER "$DB_INIT_USER" PASSWORD '$DB_INIT_PASSWORD';  <1>
CREATE USER "$DB_USER" PASSWORD '$DB_PASSWORD';  <2>
CREATE DATABASE "$DB_NAME" OWNER '$DB_INIT_USER';
----
- Installation der PostGIS-Erweiterung für die neu erstellte Datenbank.
.. Dazu als Super User (_postgres_) mit der neuen Database `$DB_NAME` verbinden und folgendes SQL-Statement ausführen:
[source,sql]
----
CREATE EXTENSION IF NOT EXISTS postgis;
----

NOTE: Weitere Informationen zu der Erstellung der XPlanDB im Anhang <<appendix_xplandb_skript>>.

[[konfiguration-xplandb-liquibase]]
==== Aufruf von Liquibase

Die Erstellung der XPlanDB wird dann mit dem Ausführen der Liquibase-Skripte abgeschlossen.

[source,bash]
----
$LIQUIBASE_HOME/liquibase
      --driver=org.postgresql.Driver \ <1>
      --classpath=changelog:/liquibase/classpath/xplan-database-scripts.jar:./org/postgresql/postgresql/42.7.3/postgresql-42.7.3.jar \ <2>
      --search-path=changelog \ <3>
      --changelog-file=changelog_xplandb.yaml \ <4>
      --url=$XPLAN_JDBC_URL \ <5>
      --username=$XPLAN_DB_INIT_USER \ <6>
      --password=$XPLAN_DB_INIT_PASSWORD \ <7>
      update <8>
      -Dxplan.db.user=$XPLAN_DB_USER <9>
      -Dxplan.srid=$XPLAN_SERVICES_DEFAULT_CRS_SRID <10>
----
<1> JDBC-Treiber für PostgreSQL ist in Liquibase enthalten (optional)
<2> Pfad zu den gepackten Liquibase-Dateien und Pfad zu JDBC-Treiber getrennt durch `:` unter LINUX/UNIX oder `;` unter Windows (optional)
<3> Pfad zu den Liquibase Changelog-Dateien (optional)
<4> Liquibase-Datei für die XPlanDB _changelog_xplandb.yaml_
<5> JDBC URL, z.B. `jdbc:postgresql://localhost:5432/xplanbox`
<6> Benutzername, `$DB_INIT_USER` aus <<konfiguration-xplandb>>
<7> Passwort für den Benutzer, `$DB_INIT_PASSWORD` aus <<konfiguration-xplandb>>
<8> Liquibase Kommando https://docs.liquibase.com/commands/update/update.html[Update]
<9> Benutzername, `$DB_USER` aus <<konfiguration-xplandb>>
<10> PostGIS SRID in dem die Daten gespeichert werden, z.B. `25832`

==== Datenbankverbindungen anpassen

- Anpassen der Datenbank-Verbindungen in den XPlanDiensten und im XPlanManagerWorkspace unter Verwendung der Rolle `$DB_USER`:
 .. _<DEEGREE_WORKSPACE_ROOT>/xplan-manager-workspace/jdbc/xplan.xml_
 .. _<DEEGREE_WORKSPACE_ROOT>/xplan-services-wfs-workspace/jdbc/xplan.xml_
 .. _<DEEGREE_WORKSPACE_ROOT>/xplan-services-wfs-syn-workspace/jdbc/xplan.xml_
 .. _<DEEGREE_WORKSPACE_ROOT>/xplan-services-wms-workspace/jdbc/xplan.xml_
. Anpassen der Datenbank-Verbindungen im XPlanValidatorWMS, wenn die persistente Datenhaltung verwendet wird (siehe <<konfiguration-xplanvalidatorwms>>)
.. _<DEEGREE_WORKSPACE_ROOT>/xplan-webservices-validator-wms-sql-workspace/jdbc/xplan.xml_
. Anpassen der Datenbank-Verbindungen in den XPlanDiensten und im XPlanManagerWorkspace, wenn die Bereitstellung für INSPIRE PLU erfolgen soll:
.. _<DEEGREE_WORKSPACE_ROOT>/xplan-webservices-inspireplu-workspace/jdbc/inspireplu.xml_
.. _<DEEGREE_WORKSPACE_ROOT>/xplan-manager-workspace/jdbc/inspireplu.xml_

==== Weiterführende Informationen zur Konfiguration des Datenbankzugriffs

In den Dateien __xplan.xml__ werden die ConnectionsPools für den Zugriff auf die Datenbank konfiguriert. Diese beinhalten neben den Verbindungsdetails wie die URL, den Nutzernamen und das Passwort weitere Details, die ggf. bei einer Installation zu berücksichtigen sind. Darunter:

* `initialSize`: Die Anzahl der initialen Verbindungen, die beim Start geöffnet werden.
* `maxTotal`: Die maximale Anzahl von offenen Verbindungen, die von Pool zeitgleich verwendet werden können.
* `maxIdle`: Die maximale Anzahl der Verbindungen, die sich ungenutzt im Pool befinden können.

Da die Anzahl der zugelassenen Verbindungen des Datenbankservers begrenzt sein kann, ist es abhängig von der Installation der xPlanBox, der Anzahl der Nutzer und gegebenenfalls weiterer Faktoren sinnvoll, die vordefinierten Werte an die Installationsumgebung anzupassen.

Alternativ zur Konfiguration von ConnectionsPools für den Zugriff auf die Datenbank kann auch eine JNDI DataSource konfiguriert werden. Details hierzu befinden sich im Handbuch von https://download.deegree.org/documentation/current/html/#anchor-configuration-jdbc[deegree webservices].