[[konfiguration-der-applikationsserver]]
=== Einrichtung der Applikationsserver

Um den Betrieb der verschiedenen im Abschnitt
<<systemarchitektur-und-schnittstellen,Systemarchitektur und Schnittstellen>> beschriebenen
Komponenten zu gewährleisten, ist eine Aufteilung der Komponenten auf mehrere Tomcat-Instanzen erforderlich. Im Folgenden ist beschrieben, wie eine Installation der Komponenten auf drei Tomcat-Instanzen erfolgen kann. Werden drei Tomcat-Instanzen auf einem Server betrieben, dann sollte der Server die im Kapitel <<empfohlene-systemkonfiguration>> beschriebenen Anforderungen erfüllen.

Für den Betrieb der xPlanBox sind getrennte Tomcat-Instanzen erforderlich:
der _Anwendungs-Tomcat_, _Dienste-Tomcat_ und _API-Tomcat_. Im Folgenden wird die
Konfiguration der drei Tomcat-Instanzen beschrieben.

[[anwendungs-tomcat]]
==== Anwendungs-Tomcat

Im _Anwendungs-Tomcat_ muss folgende Konfiguration vorgenommen werden:

. Für die Komponenten XPlanManager und XPlanValidator muss die Umgebungsvariable _XPLANBOX_CONFIG_ gesetzt werden. Das in dieser Variable gesetzte Verzeichnis muss die Konfigurationsdateien _managerConfiguration.properties_, _managerWebConfiguration.properties_ und _validatorConfiguration.properties_ enthalten (siehe Kapitel <<vorbereitung-der-installation>> und  <<anwendung-installieren>>).
. Das Verzeichnis mit den deegree Workspaces ist über die Umgebungsvariable _DEEGREE_WORKSPACE_ROOT_ gesetzt (siehe Kapitel <<vorbereitung-der-installation>>).
. Die Bibliothek deegree muss über die Variable `javax.xml.transform.TransformerFactory=net.sf.saxon.TransformerFactoryImpl` konfiguriert werden.
. Die Bibliothek JTS muss über die Variable `jts.overlay=ng` konfiguriert werden.


TIP: Es wird empfohlen die Zeitzone im _Anwendungs-Tomcat_ auf "Europe/Berlin" zu konfigurieren, wenn dies nicht bereits auf Ebene des Betriebssystems gesetzt ist. Andernfalls kann es beim Editieren eines Plans veränderten Datumsangaben (z. B. Herstellungsdatum) kommen. Für die Konfiguration im Tomcat muss folgende Option zusätzlich in den `CATALINA_OPTS` ergänzt werden: `-Duser.timezone=Europe/Berlin`.

[[anwendungs-tomcat-linux]]
===== Linux

Unter Linux muss im Verzeichnis _<CATALINA_HOME>/bin_ der Instanz _Anwendungs-Tomcat_ ggf. eine neue Datei mit dem Namen _setenv.sh_ angelegt werden.

In dieser Datei wird die Variable `CATALINA_OPTS` wie folgt erweitert:

----
export CATALINA_OPTS='-DXPLANBOX_CONFIG=/opt/xplanbox/xplan-manager-config -DDEEGREE_WORKSPACE_ROOT=/opt/deegree -Djts.overlay=ng -Djavax.xml.transform.TransformerFactory=net.sf.saxon.TransformerFactoryImpl -Duser.timezone=Europe/Berlin'
----

Falls bereits ein Export von `CATALINA_OPTS` in dieser Datei vorhanden ist, muss die Variable `CATALINA_OPTS` erweitert werden.

[[anwendungs-tomcat-windows]]
===== Windows

Unter Windows muss im Verzeichnis _<CATALINA_HOME>/bin_ der Instanz _Anwendungs-Tomcat_ ggf. eine neue Datei mit dem Namen _setenv.bat_ angelegt werden.

In dieser Datei wird die Variable `CATALINA_OPTS` wie folgt erweitert:

----
export CATALINA_OPTS='-DXPLANBOX_CONFIG=C:\xplanbox\xplan-manager-config -DDEEGREE_WORKSPACE_ROOT=C:\deegree -Djts.overlay=ng -Djavax.xml.transform.TransformerFactory=net.sf.saxon.TransformerFactoryImpl -Duser.timezone=Europe/Berlin'
----

Falls bereits ein Export von `CATALINA_OPTS` in dieser Datei vorhanden ist, muss die Variable  `CATALINA_OPTS` erweitert werden.

IMPORTANT: Der _Anwendungs-Tomcat_ muss mindestens über 4GB Arbeitsspeicher verfügen, dies kann durch Setzen der Umgebungsvariable: `export JAVA_OPTS='-Xmx4096m'` erfolgen.

[[dienste-tomcat]]
==== Dienste-Tomcat

Für den Zugriff des XPlanManager auf die REST-API des XPlanWerkWMS, muss zur Authentifizierung ein API-Key eingerichtet werden.

Für die Instanz _Dienste-Tomcat_ muss die Datei _<DEEGREE_WORKSPACE_ROOT>/config.apikey_ angelegt werden, in dieser wird der API-Key für die REST-Schnittstelle aller XPlanDienste eingetragen.

.Beispiel für den Inhalt der Datei config.apikey:
----
xplanbox
----

Der API-Key wird beim Start der XPlanDienste automatisch angelegt und ein generierter Key gesetzt. Dieser kann nachträglich angepasst werden. Weitere Informationen finden sich dazu im Handbuch von https://download.deegree.org/documentation/current/html/#%5Fsetting%5Fup%5Fthe%5Finterface[deegree webservices].
Der API-Key muss auch in die _managerConfiguration.properties_ eingetragen werden (s. Kapitel <<automatischer-workspace-reload>>)!

Für den _Dienste-Tomcat_ muss auch die Variable `CATALINA_OPTS` wie folgt erweitert werden:

----
export CATALINA_OPTS='-DDEEGREE_WORKSPACE_ROOT=/opt/deegree -Djavax.xml.transform.TransformerFactory=net.sf.saxon.TransformerFactoryImpl'
----

IMPORTANT: Der _Dienste-Tomcat_ muss mindestens über 4GB Arbeitsspeicher verfügen,
dies kann durch Setzen der Umgebungsvariable: `export JAVA_OPTS='-Xmx4096m'` erfolgen.

[[api-tomcat]]
==== API-Tomcat

Für den _API-Tomcat_ muss die Variable `CATALINA_OPTS` wie auch für den <<dienste-tomcat,_Dienste-Tomcat_>> wie folgt erweitert werden:

----
export CATALINA_OPTS='-DXPLANBOX_CONFIG=/opt/xplanbox/xplan-manager-config -DDEEGREE_WORKSPACE_ROOT=/opt/deegree -Djts.overlay=ng -Djavax.xml.transform.TransformerFactory=net.sf.saxon.TransformerFactoryImpl -Duser.timezone=Europe/Berlin'
----

IMPORTANT: Der _API-Tomcat_ muss mindestens über 4GB Arbeitsspeicher verfügen,
dies kann durch Setzen der Umgebungsvariable: `export JAVA_OPTS='-Xmx4096m'` erfolgen.

==== Absicherung der Tomcat-Instanzen

Wenn die xPlanBox in einer produktiven Umgebung betrieben wird, sollten die Apache Tomcat-Instanzen abgesichert werden. Dazu sind die allgemeinen Empfehlungen aus der https://tomcat.apache.org/tomcat-10.0-doc/security-howto.html[Dokumentation von Apache Tomcat zum Thema Sicherheit] zu beachten.

