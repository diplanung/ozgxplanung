[[systemarchitektur-und-schnittstellen]]
== Systemarchitektur und Schnittstellen

Die folgende Abbildung zeigt die vereinfachte Darstellung der zentralen Komponenten der xPlanBox.

.Vereinfachte Darstellung der Architektur der xPlanBox
image::Architektur_xPlanBox.png[xPlanBox Architektur]

Das Gesamtsystem setzt sich aus folgenden Komponenten zusammen:

* <<xplanmanager-web,XPlanManagerWeb>>
* <<xplanmanager-api,XPlanManagerAPI>>
* <<xplanvalidator-web,XPlanValidatorWeb>> inkl. <<xplanvalidator-wms,XPlanValidatorWMS>>
* <<xplanvalidator-api,XPlanValidatorAPI>>
* <<xplandokumente-api,XPlanDokumenteAPI>>
* <<xplanvalidator-executor,XPlanValidatorExecutor>>
* <<xplanwms,XPlanWMS, XPlanArtWMS und XPlanWerkWMS>>
* <<xplanmapserver,XPlanMapServer>>
* <<xplanwfs,XPlanWFS>>
* <<xplansynwfs,XPlanSynWFS>>
* <<xplanmapproxy,XPlanMapProxy>>
* <<xplaninspirepluwms,XPlanInspirePluWMS>>
* <<xplaninspirepluwfs,XPlanInspirePluWFS>>
* <<xplanclitools,XPlanCLI Tools>>
* <<xplanresources,XPlanRessourcen>>
* <<xplandb,XPlanDB>>

[[xplanmanager-web]]
=== XPlanManagerWeb

Die Komponente XPlanManagerWeb ist eine Web-Oberfläche, die dem
Fachadministrator der xPlanBox ermöglicht,
die Datenhaltung <<XPlanDB>> über einen Browser zu verwalten.

[[xplanmanager-api]]
=== XPlanManagerAPI

Die Komponente XPlanManagerAPI stellt eine REST-Schnittstelle bereit, über die
die Datenhaltung <<XPlanDB>> verwaltet werden kann.

[[xplanvalidator-web]]
=== XPlanValidatorWeb

Die Komponente XPlanValidatorWeb ist eine Web-Oberfläche, welche dem
Fachadministrator der xPlanBox ermöglicht,
XPlanGML-Dokumente über den Browser zu validieren.

[[xplanvalidator-wms]]
==== XPlanValidatorWMS

Bei der Komponente XPlanValidatorWMS handelt es sich um eine Subkomponente
des XPlanValidatorWeb. Validierte Plänen können über den XPlanValidatorWMS in der Kartenvorschau angezeigt werden, ohne das ein Plan in die Datenhaltung der <<xplandb>> importiert wird.

IMPORTANT: Der XPlanValidatorWMS ist veraltet und wird in zukünftigen Versionen der xPlanBox entfernt!

[[xplanvalidator-api]]
=== XPlanValidatorAPI

Die Komponente XPlanValidatorAPI stellt eine REST-Schnittstelle bereit, über die XPlanGML-Dokumente validiert werden können.

[[xplanvalidator-executor]]
==== XPlanValidatorExecutor

Die Komponente XPlanValidatorExecutor ist die zentrale Komponente innerhalb der xPlanBox, die die Validierung der Pläne durchführt und wird von den Komponenten XPlanValidatorAPI und XPlanManagerAPI verwendet.

[[xplandokumente-api]]
=== XPlanDokumenteAPI

Die optionale Komponente XPlanDokumenteAPI stellt eine REST-Schnittstelle bereit, über die zu einem Plan zugehörigen Dokumente abgerufen werden können.

[[xplanwms]]
=== XPlanWMS, XPlanArtWMS und XPlanWerkWMS

Der XPlanWMS, XPlanArtWMS und XPlanWerkWMS sind auf dem Standard Web Map Service
(Version 1.1.1 und 1.3.0) des Open Geospatial Consortium (OGC)
basierende Kartendienste. Diese bieten die Möglichkeit,
Visualisierungen von Plandaten sowie Sachinformationsabfragen zu
einzelnen Planinhalten abzufragen.

Die Plandaten liegen in einer separaten Datenhaltung (siehe Abbildung 2) vor, d.h. je nachdem in welche Datenhaltung ein Plan
importiert wurde, ist dieser über den entsprechenden XPlanWMS-Endpoint abrufbar. Die folgende Tabelle zeigt die vom XPlanWMS
bereitgestellten Daten nach Planstatus.

[width="100%",cols="30%,40%,30%",options="header"]
|===
|Endpoint
|Planstatus
|Postfix für Schema und Endpoint
|XPlanWMSInAufstellung
|In Aufstellung
|_pre_
|XPlanWMSFestgestellt
|Festgestellt
|-
|XPlanWMSArchiviert
|Archiviert
|_archive_
|===

NOTE: Gleiches gilt für den XPlanWerkWMS, XPlanArtWMS, XPlanWFS und XPlanSynWFS

Der XPlanWMS greift auf dasselbe Datenbankschema wie der <<xplansynwfs>> in der <<xplandb>> zu. Rasterdaten werden über XPlanMapServer bereitgestellt. Die Verzeichnisstruktur des Workspace <<appendix_xplanwms-workspace>> ist im Anhang dokumentiert.

Während der XPlanWMS planübergreifend arbeitet, beschränkt sich der
XPlanWerkWMS auf einzelne Planwerke. Der XPlanArtWMS stellt je einen Endpunkt für jede Planart wie z. B. BPlan oder FPlan zur Verfügung.

[[xplanmapserver]]
=== XPlanMapServer

Der XPlanMapServer ist auf dem Standard Web Map Service
(Version 1.1.1 und 1.3.0) des Open Geospatial Consortium (OGC)
basierender Kartendienst. Dieser bietet die Möglichkeit die Rasterdaten zu einem Planwerk abzufragen.

[[xplanwfs]]
=== XPlanWFS

Der XPlanWFS ist ein auf dem Standard Web Feature Service
(Version 1.1.0 und 2.0.0) des Open Geospatial Consortium (OGC)
basierender Dienst zur Abfrage von Vektordaten und stellt Endpunkte für jede XPlanGML-Version bereit. Zu jeder unterstützten XPlanGML-Version stellt die xPlanBox einen XPlanWFS-Endpoint bereit. Jeder Dienst greift auf das zu der XPlanGML-Version passende Datenbankschema in der <<xplandb>> zu. Die Zuordnung der einzelnen Endpunkte zu den Datenbankschemata ist im Kapitel <<datenzugriff-xplandb>> dokumentiert.

[[xplansynwfs]]
=== XPlanSynWFS

Der XPlanSynWFS dient der Abbildung des synthetisierten
XPlanGML-Anwendungsschemas (XPlanSynGML). Dieses stellt eine
vereinfachte und zusammenfassende Form der verschiedenen XPlanGML
Versionen dar. Der XPlanSynWFS greift auf das synthetisierte Datenbankschema in der <<xplandb>> zu.

[[xplanmapproxy]]
=== XPlanMapProxy

Der XPlanMapProxy ist auf dem Standard Web Map Service
(Version 1.1.1 und 1.3.0) und Web Map Tiling Service 1.0.0 des Open Geospatial Consortium (OGC)
basierender Kartendienst. Dieser bietet die Möglichkeiten kaskadierend auf die Inhalte des XPlanWMS und XPlanMapServer mit Cache-Funktion zuzugreifen.

[[xplaninspirepluwms]]
=== XPlanInspirePluWMS

Der XPlanInspirePluWMS ist ein INSPIRE View Service für die
Bereitstellung importierter Pläne im INSPIRE Datenthema Planned Land Use (PLU).

[[xplaninspirepluwfs]]
=== XPlanInspirePluWFS

Der XPlanInspirePluWFS ist ein INSPIRE Download Service für die
Bereitstellung importierter Pläne im INSPIRE Datenthema Planned Land Use (PLU).

[[xplanclitools]]
=== XPlanCLI Tools

Das Kommandozeilenwerkzeug XPlanCLI unterstützt mehrere Kommandos zur Validierung und Verwaltung von Planwerken, sowie Kommandos zur Erstellung eines Auswerteschemas oder Generierung von Metadaten.

[[xplanresources]]
=== XPlanRessourcen

Die optionale Komponente XPlanRessourcen bietet eine
Einstiegsseite zu den einzelnen Komponenten der xPlanBox und stellt
Testdaten sowie die Dokumente mit den Konformitätsbedingungen des Standards XPlanung bereit.

[[xplandb]]
=== XPlanDB

Die Komponente stellt die zentrale Datenhaltungskomponente für die alle Komponenten der xPlanBox bereit und wird durch eine <<datenbank,PostgreSQL/PostGIS Datenbank>> realisiert. Die Einrichtung der Datenbank ist im Kapitel <<konfiguration-der-datenbank>>, die Datenbankstruktur im Anhang <<appendix_xplandb>> sowie der Datenzugriff im Kapitel <<datenzugriff-xplandb>> beschrieben.