== Support und bekannte Probleme
[[support]]
=== Support

Für professionellen Support per Telefon oder E-Mail kontaktieren Sie bitte die https://www.lat-lon.de[lat/lon GmbH].

[[fehler-melden]]
=== Fehler melden

Für den Fall, dass Sie einen Fehler in der xPlanBox finden, erstellen Sie bitte einen  Fehlerbericht unter https://gitlab.opencode.de/diplanung/ozgxplanung/-/issues[Open CoDE GitLab Issues].

[[bekannte-probleme]]
=== Bekannte Probleme

==== XPlanValidator - Anzeige der Zeilennummern im Validierungsbericht

Die Ausgabe der Zeilennummer bei semantischen Validierungsfehlern kann je nach Formatierung des XML-Dokuments von der Zeile des verursachenden Elements abweichen.
Gute und exakte Ergebnisse bei der Ausgabe der Zeilennummer des Elements lassen sich dann erzielen, wenn das XML-Dokument ohne Formatierung der Attribute erfolgt, insbesondere der Deklaration der Namensräume im Wurzelelement. Die Zeilen werden entsprechend der https://www.w3.org/TR/REC-xml/#sec-line-ends[XML-Spezifikation] gezählt und stellen nur eine Annäherung an die Zeilennummer der Dokumententität oder der externen geparsten Entität dar, in der das Element erscheint, das das Ereignis auslöst.

==== XPlanManagerWeb - Anzeige der Pläne auf der letzten Seite

Die Ansicht der Pläne im XPlanManagerWeb zeigt auf der letzten Seite immer die letzten 15 Pläne an. Dieses Verhalten tritt sowohl mit oder ohne Auswahl eines Filters auf (siehe dazu die Funktionsbeschreibung in <<xplanmanager-web-auflistung>>).

==== XPlanManagerWeb - Transformation in das INSPIRE Planned Land Use Datenschema

Bei der Transformation von XPlanGML in das INSPIRE Planned Land Use Datenschema können in der vorliegenden Version der xPlanBox die Daten nicht vollständig transformiert werden.
So fehlen u.a. Transformationsregeln für Werte aus Codelisten als auch konfigurierbare Abbildungsregeln für Elemente wie z. B. der INSPIRE ID.
Zugesichert werden kann, dass das über den XPlanInspirePluWFS abgegebene GML gegen das GML-Applikationsschema validiert.
Nicht zugesichert werden kann, dass das GML die Vorgaben aus den Technical Guidelines für das INSPIRE Annex III Datenthema Land Use vollständig erfüllt, sowie dass die Daten aus XPlanGML vollständig in das Zielschema INSPIRE PLU transformiert werden.

==== XPlanManagerWeb - Löschen von einzelnen Plänen aus dem InspirePLU Diensten

Aktuell kann über den XPlanManager ein XPlanArchiv nur aus der XPlanDB-Datenhaltung entfernt werden. Wird ein Plan von XPlanGML nach INSPIRE PLU transformiert und in die InspirePLUDB-Datenhaltung übertragen, kann der transformierte Datensatz nicht über den XPlanManager aus der InspirePLUDB-Datenhaltung entfernt werden.

Eine Umgehung ist das Löschen des Datensatzes aus der InspirePLUDB-Datenhaltung direkt in der Datenbank oder über die WFS-T Schnittstelle des InspirePLUWFS. Zum Löschen eines oder mehrerer Datensätze muss an den InspirePLUWFS eine WFS-T 2.0.0 DELETE Aktion geschickt werden.

==== XPlanManagerWeb - Änderungen der Rasterbasis über die Editorfunktion im XPlanManagerWeb werden nicht übernommen

Bei dem Hinzufügen einer Rasterbasis wird ohne Angabe des Typs ("Keine Auswahl" statt "Scan") der Eintrag nicht in das XPlanGML-Instanzdokument übernommen. Die Referenz auf die Rasterbasis wird beim Speichern nicht in das XPlanGML geschrieben, befindet sich aber in der ZIP-Datei.

==== XPlanManagerWeb - Hinzufügen von einer Datei xplan.gml über die Editorfunktion im XPlanManagerWeb resultiert in korrupten XPlanArchiv

Wird ein Text, Dokument oder eine Rasterbasis mit dem Namen _xplan.gml_ über die Editorfunktion des XPlanManagerWeb hinzugefügt und gespeichert, kann dieses XPlanArchiv danach nicht mehr geöffnet werden. Beim nochmaligen Aufruf der Editorfunktion zeigt das System dann den HTTP-Statusfehler "500" an.

IMPORTANT: Der Fehler kann nicht in der xPlanBox behoben werden! Das XPlanArchiv muss aus der Datenhaltung entfernt, korrigiert und dann erneut über den XPlanManager importiert werden.

==== XPlanManagerWeb - Texteingaben über Editorfunktion im XPlanManagerWeb können nicht gespeichert werden

Bei der Verarbeitung von Texteingaben über die Editorfunktion werden die Zeichen und Zeichenketten aus Sicherheitsgründen von der Anwendung geprüft. Die zugelassenen Zeichen unterscheiden sich von denen, die im XML zugelassen sind und können dazu führen, dass eine Änderung an einem bereits erfolgreich validierten und importieren Plan nicht übernommen werden kann. In diesem Fall müssen allen Sonderzeichen aus der Texteingabe entfernt werden. Alternativ muss die Änderung direkt im XPlanGML vorgenommen werden und der Plan erneut importiert werden.

==== XPlanManagerWeb - Hinzufügen einer weiteren Datei mit gleichem Namen über die Editorfunktion führt zu Fehler beim Abspeichern

Wird ein XPlanArchiv mit Begleitdokumenten oder Rasterdaten importiert und über die Editorfunktion eine weitere Datei mit bereits verwendetem Dateinamen hinzugefügt, dann kommt es beim Abspeichern zu einer Fehlermeldung.

==== XPlanManagerWeb - Überspringen des Imports von Rasterdaten

Wird beim Import eines Plans mit Rasterdaten die Option "Weiter ohne Rasterdaten" im Dialog "Analyse der Rasterdaten" ausgewählt, kann es in Folge zu Fehlern im XPlanMapServer und XPlanMapProxy kommen.

==== XPlanManagerWeb - Überspringen des Imports von Rasterdaten

Wenn ein Plan mit Rasterdaten über den XPlanManagerWeb mit aktivierter Übernahme der Rasterdaten importiert wird, werden die Rasterdaten übernommen und in der Kartenvorschau angezeigt. Wird der Plan erneut importiert, aber mit aktivierter Option "ohne Rasterdaten importieren", führt dies dazu, dass die Rasterdaten auch bei dem zuvor importierten Plan nicht mehr in der Kartenvorschau angezeigt werden.

==== XPlanManagerWeb/XPlanManagerAPI - Fehler beim Editieren: Multiple representations of the same entity

Wird ein XPlanArchiv mit Anhängen z. B. Begleitdokumenten importiert und sind diese Dateien nicht oder durch eine absolute URL im XPlanGML referenziert, dann kommt es beim Hinzufügen einer Datei mit dem gleichen Namen über die Editierfunktion im XPlanManagerWeb oder entsprechende Funktion in der XPlanManagerAPI zu einem Fehler.
Ein solches XPlanArchiv kann entstehen, wenn ein XPlanArchiv über den XPlanManagerWeb oder die XPlanManagerAPI exportiert wird und die XPlanDokumenteAPI aktiviert ist. Dann werden die externen Referenzen von der xPlanBox automatisch angepasst. Das exportierte XPlanArchiv beinhaltet alle Anhänge des Plans sowie die absoluten Referenzen auf die XPlanDokumenteAPI im XPlanGML.

==== XPlanDokumenteAPI - Abruf des World-Files bei GeoTIFF nicht möglich

Bei Import eines Plans mit einer im XPlanGML referenzierten GeoTIFF-Datei als Rasterbasis und zusätzlich auch noch einem referenzierten World-File (tfw-Datei), dann ist die tfw-Datei nicht über die XPlanDokumenteAPI abfragbar. Wird die im exportierten XPlanGML angegebene URL aufgerufen, kann die tfw-Datei nicht abgerufen werden und ein HTTP-Statusfehler "404 - Not Found" wird von der XPlanDokumenteAPI zurückgegeben.

==== XPlanDokumenteAPI/XPlanManagerAPI/XPlanManagerWeb - Re-Import von exportierten Plänen mit Rasterdaten bei aktivierter XPlanDokumenteAPI nicht möglich

Wenn die XPlanDokumenteAPI aktiviert ist und ein XPlanArchiv mit Rasterdaten importiert wurde, dann werden die Referenzen auf die Rasterdaten von der xPlanBox automatisch angepasst. Wird das XPlanGML-Instanzdokument über den XPlanManagerAPI oder -Web wieder exportiert, dann müssen vor einem erneuten Import in die xPlanBox die Referenzen manuell angepasst werden.

==== XPlanValidatorAPI - Verwendung von vollqualifizierten Pfaden im HTTP-Header "X-Filename"

Wird der HTTP-Header "X-Filename" mit einem vollqualifizierten Pfad angegeben, kommt es bei der Anfrage eines Reports im Format PDF zu einem HTTP-Statusfehler "500".

Der Fehler kann dadurch umgangen werden, dass im HTTP-Header nur der Dateiname angegeben wird. Der Fehler tritt nicht auf, wenn über den HTTP-Header "Accept" Json oder XML angefragt werden.

==== XPlanWMS - Darstellungsvorschriften für Raumordnungspläne

Die Darstellungsvorschriften für Raumordnungspläne sind zum Teil unvollständig. Durch den XPlanWMS werden die betroffenen Ebenen daher nur in der Standarddarstellung ausgegeben.

==== XPlanWMS - Umsetzung von Präsentationsobjekte

Im XPlanWMS ist der Umfang der Darstellung von Präsentationsobjekten nur eingeschränkt implementiert. Über die folgenden Layer werden diese angezeigt:

 * Für die Planart BP_Plan:
 ** bp_xp_fpo
 ** bp_xp_lpo
 ** bp_xp_lto
 ** bp_xp_ppo
 ** bp_xp_pto
 * Für die Planart FP_Plan:
 ** fp_xp_fpo
 ** fp_xp_lpo
 ** fp_xp_lto
 ** fp_xp_ppo
 ** fp_xp_pto
 * Für die Planart LP_Plan:
 ** lp_xp_fpo
 ** lp_xp_lpo
 ** lp_xp_lto
 ** lp_xp_ppo
 ** lp_xp_pto
 * Für die Planart RP_Plan:
 ** rp_xp_fpo
 ** rp_xp_lpo
 ** rp_xp_lto
 ** rp_xp_ppo
 ** rp_xp_pto
 * Für die Planart SO_Plan:
 ** so_xp_fpo
 ** so_xp_lpo
 ** so_xp_lto
 ** so_xp_ppo
 ** so_xp_pto

Derzeit werden die folgenden Attribute bei der Visualisierung berücksichtigt:

 * XP_LTO
 ** schriftinhalt
 ** position
 * XP_PTO
 ** schriftinhalt
 ** skalierung
 ** drehwinkel
 ** horizontaleAusrichtung
 ** vertikaleAusrichtung
 ** position
 * XP_FPO
 ** Polygon wird mit grauem Umring dargestellt
 ** position
 * XP_LPO
 ** Linie wird grau dargestellt
 ** position
 * XP_PPO
 ** Darstellung erfolgt als Kreis mit grauem Umring
 ** position
