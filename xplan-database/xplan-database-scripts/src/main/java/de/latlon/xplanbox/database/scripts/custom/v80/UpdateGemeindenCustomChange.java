/*-
 * #%L
 * xplan-database-scripts - Liquibase Changelogs zum Aufsetzen/Aktualisieren der Datenhaltung.
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.database.scripts.custom.v80;

import static de.latlon.xplanbox.database.scripts.custom.v80.GemeindeParser.parseGemeinden;

import javax.xml.stream.XMLStreamException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.zip.GZIPInputStream;

import liquibase.Scope;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.DatabaseException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class UpdateGemeindenCustomChange implements CustomTaskChange {

	@Override
	public void execute(Database database) throws CustomChangeException {
		JdbcConnection dbConn = (JdbcConnection) database.getConnection();
		String sql = "SELECT plan, data FROM xplanmgr.artefacts WHERE artefacttype = 'XPLANGML'";
		try {
			PreparedStatement psSelect = dbConn.prepareStatement(sql);
			ResultSet rs = psSelect.executeQuery();
			while (rs.next()) {
				int planId = rs.getInt("plan");
				byte[] data = rs.getBytes("data");

				Scope.getCurrentScope().getLog(getClass()).info("Update plan with id " + planId);
				try (ByteArrayInputStream bis = new ByteArrayInputStream(data);
						GZIPInputStream is = new GZIPInputStream(bis)) {
					List<GemeindeParser.Gemeinde> gemeinden = parseGemeinden(is);
					for (GemeindeParser.Gemeinde gemeinde : gemeinden) {
						String insertSql = "INSERT INTO xplanmgr.gemeinden (plan,ags,rs,gemeindename,ortsteilname) VALUES (?,?,?,?,?)";
						PreparedStatement psInsert = dbConn.prepareStatement(insertSql);
						psInsert.setInt(1, planId);
						psInsert.setString(2, gemeinde.ags());
						psInsert.setString(3, gemeinde.rs());
						psInsert.setString(4, gemeinde.gemeindeName());
						psInsert.setString(5, gemeinde.ortsteilName());
						psInsert.executeUpdate();
					}
				}
			}

		}
		catch (SQLException | DatabaseException | IOException | XMLStreamException e) {
			throw new CustomChangeException(e);
		}
	}

	@Override
	public String getConfirmationMessage() {
		return "Updated gemeinden";
	}

	@Override
	public void setUp() throws SetupException {

	}

	@Override
	public void setFileOpener(ResourceAccessor resourceAccessor) {

	}

	@Override
	public ValidationErrors validate(Database database) {
		return new ValidationErrors();
	}

}
