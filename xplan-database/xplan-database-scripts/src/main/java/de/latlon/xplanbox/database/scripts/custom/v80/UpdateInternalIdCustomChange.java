/*-
 * #%L
 * xplan-database-scripts - Liquibase Changelogs zum Aufsetzen/Aktualisieren der Datenhaltung.
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.database.scripts.custom.v80;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import liquibase.Scope;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.DatabaseException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;

/**
 * Insert internalId into xplanmgr.plans.
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0.1
 */
public class UpdateInternalIdCustomChange implements CustomTaskChange {

	@Override
	public void execute(Database database) throws CustomChangeException {
		JdbcConnection dbConn = (JdbcConnection) database.getConnection();
		try {
			Scope.getCurrentScope().getLog(getClass()).info("Insert in xplanmgr.plans.");
			String updateSql = "UPDATE xplanmgr.plans as p set internalId = s.xplan_internalid FROM (SELECT xplan_mgr_planid, xplan_internalid FROM "
					// bp
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansyn.xplan_bp_plan) bpfix "
					+ " NATURAL FULL JOIN "
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansynpre.xplan_bp_plan) bppre "
					+ " NATURAL FULL JOIN "
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansynarchive.xplan_bp_plan) bparchive "
					+ " NATURAL FULL JOIN "
					// fp
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansyn.xplan_fp_plan) fpfix "
					+ " NATURAL FULL JOIN "
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansynpre.xplan_fp_plan) fppre "
					+ " NATURAL FULL JOIN "
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansynarchive.xplan_fp_plan) fparchive "
					+ " NATURAL FULL JOIN "
					// lp
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansyn.xplan_lp_plan) lpfix "
					+ " NATURAL FULL JOIN "
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansynpre.xplan_lp_plan) lppre "
					+ " NATURAL FULL JOIN "
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansynarchive.xplan_lp_plan) lparchive "
					+ " NATURAL FULL JOIN "
					// rp
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansyn.xplan_rp_plan) rpfix "
					+ " NATURAL FULL JOIN "
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansynpre.xplan_rp_plan) rppre "
					+ " NATURAL FULL JOIN "
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansynarchive.xplan_rp_plan) rparchive "
					+ " NATURAL FULL JOIN "
					// so
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansyn.xplan_so_plan) sofix "
					+ " NATURAL FULL JOIN "
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansynpre.xplan_so_plan) sopre "
					+ " NATURAL FULL JOIN "
					+ " (SELECT xplan_mgr_planid, xplan_internalid FROM xplansynarchive.xplan_so_plan) soarchive "
					+ " ) as s WHERE p.internalId is null and p.id = s.xplan_mgr_planid;";
			PreparedStatement psUpdate = dbConn.prepareStatement(updateSql);
			int noOfUpdated = psUpdate.executeUpdate();
			Scope.getCurrentScope()
				.getLog(getClass())
				.info("Updated " + noOfUpdated + " internalIds in xplanmgr.plans.");
		}
		catch (SQLException | DatabaseException e) {
			throw new CustomChangeException(e);
		}
	}

	@Override
	public String getConfirmationMessage() {
		return "Updated xplanmgr.plans.internalID";
	}

	@Override
	public void setUp() throws SetupException {

	}

	@Override
	public void setFileOpener(ResourceAccessor resourceAccessor) {

	}

	@Override
	public ValidationErrors validate(Database database) {
		return new ValidationErrors();
	}

}
