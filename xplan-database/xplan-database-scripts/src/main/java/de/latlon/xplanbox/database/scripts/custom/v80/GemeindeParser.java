/*-
 * #%L
 * xplan-database-scripts - Liquibase Changelogs zum Aufsetzen/Aktualisieren der Datenhaltung.
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.database.scripts.custom.v80;

import static javax.xml.stream.XMLStreamConstants.END_DOCUMENT;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class GemeindeParser {

	public record Gemeinde(String ags, String rs, String gemeindeName, String ortsteilName) {
	}

	public static List<Gemeinde> parseGemeinden(InputStream is) throws IOException, XMLStreamException {
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLStreamReader reader = null;
		try {
			reader = inputFactory.createXMLStreamReader(is);
			return readDocument(reader);
		}
		finally {
			if (reader != null) {
				reader.close();
			}
		}
	}

	private static List<Gemeinde> readDocument(XMLStreamReader reader) throws XMLStreamException {
		List<Gemeinde> gemeinden = new ArrayList<>();
		boolean foundPlan = moveReaderToFirstMatch(reader, "(BP|FP|LP|SO)_Plan");
		if (foundPlan) {
			while (moveReaderToFirstMatch(reader, "gemeinde")) {
				gemeinden.add(readGemeinde(reader));
			}
		}
		return gemeinden;
	}

	private static Gemeinde readGemeinde(XMLStreamReader reader) throws XMLStreamException {
		moveReaderToFirstMatch(reader, "XP_Gemeinde");
		String ags = null;
		String rs = null;
		String gemeindeName = null;
		String ortsteilName = null;
		while (reader.hasNext()) {
			int eventType = reader.next();
			switch (eventType) {
				case XMLStreamReader.START_ELEMENT:
					String elementName = reader.getLocalName();
					if (elementName.equals("ags"))
						ags = readCharacters(reader);
					else if (elementName.equals("rs"))
						rs = readCharacters(reader);
					else if (elementName.equals("gemeindeName"))
						gemeindeName = readCharacters(reader);
					else if (elementName.equals("ortsteilName"))
						ortsteilName = readCharacters(reader);
					break;
				case XMLStreamReader.END_ELEMENT:
					// end of XP_Gemeinde, return parsed gemeinde
					return new Gemeinde(ags, rs, gemeindeName, ortsteilName);
			}
		}
		return null;
	}

	private static String readCharacters(XMLStreamReader reader) throws XMLStreamException {
		StringBuilder result = new StringBuilder();
		while (reader.hasNext()) {
			int eventType = reader.next();
			switch (eventType) {
				case XMLStreamReader.CHARACTERS:
				case XMLStreamReader.CDATA:
					result.append(reader.getText());
					break;
				case XMLStreamReader.END_ELEMENT:
					return result.toString();
			}
		}
		return null;
	}

	private static boolean moveReaderToFirstMatch(XMLStreamReader reader, String name) throws XMLStreamException {
		boolean hasMoreElements = true;
		do {
			if (reader.isStartElement() && reader.getName().getLocalPart().matches(name)) {
				return true;
			}
			try {
				nextElement(reader);
			}
			catch (NoSuchElementException e) {
				// end of file
				hasMoreElements = false;
			}

		}
		while (hasMoreElements);
		return false;
	}

	private static void nextElement(XMLStreamReader xmlReader) throws XMLStreamException, NoSuchElementException {
		xmlReader.next();
		while (xmlReader.getEventType() != END_DOCUMENT && !xmlReader.isStartElement() && !xmlReader.isEndElement()) {
			xmlReader.next();
		}
		if (xmlReader.getEventType() == END_DOCUMENT) {
			throw new NoSuchElementException();
		}
	}

}
