/*-
 * #%L
 * xplan-database-scripts - Liquibase Changelogs zum Aufsetzen/Aktualisieren der Datenhaltung.
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.database.scripts.custom.v80;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import liquibase.Scope;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.DatabaseException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;

/**
 * Insert simplified and transformed (WGS894) geltungsbereich into
 * xplanmgr.geltungsbereich.
 *
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 * @since 8.0
 */
public class UpdateGeltungsbereichCustomChange implements CustomTaskChange {

	@Override
	public void execute(Database database) throws CustomChangeException {
		JdbcConnection dbConn = (JdbcConnection) database.getConnection();
		try {
			Scope.getCurrentScope().getLog(getClass()).info("Insert in xplanmgr.geltungsbereich.");
			String insertSql = "INSERT INTO xplanmgr.geltungsbereich (id,geltungsbereichwgs84) "
					+ "SELECT xplan_mgr_planid as id, ST_Transform(xplan_raeumlichergeltungsbereich,4326) as geltungsbereich "
					// BP
					+ "FROM"
					+ "  (SELECT f.xplan_mgr_planid,f.xplan_raeumlichergeltungsbereich FROM xplansyn.xplan_bp_plan f) bpfix "
					+ "NATURAL FULL JOIN"
					+ "  (SELECT p.xplan_mgr_planid,p.xplan_raeumlichergeltungsbereich FROM xplansynpre.xplan_bp_plan p) bppre "
					+ "NATURAL FULL JOIN"
					+ "  (SELECT a.xplan_mgr_planid,a.xplan_raeumlichergeltungsbereich FROM xplansynarchive.xplan_bp_plan a) bparchive "
					// FP
					+ "NATURAL FULL JOIN"
					+ "  (SELECT f.xplan_mgr_planid,f.xplan_raeumlichergeltungsbereich FROM xplansyn.xplan_fp_plan f) fpfix "
					+ "NATURAL FULL JOIN"
					+ "  (SELECT p.xplan_mgr_planid,p.xplan_raeumlichergeltungsbereich FROM xplansynpre.xplan_fp_plan p) fppre "
					+ "NATURAL FULL JOIN"
					+ "  (SELECT a.xplan_mgr_planid,a.xplan_raeumlichergeltungsbereich FROM xplansynarchive.xplan_fp_plan a) fparchive "
					// RP
					+ "NATURAL FULL JOIN"
					+ "  (SELECT f.xplan_mgr_planid,f.xplan_raeumlichergeltungsbereich FROM xplansyn.xplan_rp_plan f) rpfix "
					+ "NATURAL FULL JOIN"
					+ "  (SELECT p.xplan_mgr_planid,p.xplan_raeumlichergeltungsbereich FROM xplansynpre.xplan_rp_plan p) rppre "
					+ "NATURAL FULL JOIN"
					+ "  (SELECT a.xplan_mgr_planid,a.xplan_raeumlichergeltungsbereich FROM xplansynarchive.xplan_rp_plan a) rparchive "
					// LP
					+ "NATURAL FULL JOIN"
					+ "  (SELECT f.xplan_mgr_planid,f.xplan_raeumlichergeltungsbereich FROM xplansyn.xplan_lp_plan f) lpfix "
					+ "NATURAL FULL JOIN"
					+ "  (SELECT p.xplan_mgr_planid,p.xplan_raeumlichergeltungsbereich FROM xplansynpre.xplan_lp_plan p) lppre "
					+ "NATURAL FULL JOIN"
					+ "  (SELECT a.xplan_mgr_planid,a.xplan_raeumlichergeltungsbereich FROM xplansynarchive.xplan_lp_plan a) lparchive "
					// SO
					+ "NATURAL FULL JOIN"
					+ "  (SELECT f.xplan_mgr_planid,f.xplan_raeumlichergeltungsbereich FROM xplansyn.xplan_so_plan f) sofix "
					+ "NATURAL FULL JOIN"
					+ "  (SELECT p.xplan_mgr_planid,p.xplan_raeumlichergeltungsbereich FROM xplansynpre.xplan_so_plan p) sopre "
					+ "NATURAL FULL JOIN"
					+ "  (SELECT a.xplan_mgr_planid,a.xplan_raeumlichergeltungsbereich FROM xplansynarchive.xplan_so_plan a) soarchive "
					+ "ON CONFLICT ON CONSTRAINT id_unique DO NOTHING";
			PreparedStatement psInsert = dbConn.prepareStatement(insertSql);
			int noOfInserted = psInsert.executeUpdate();
			Scope.getCurrentScope()
				.getLog(getClass())
				.info("Inserted " + noOfInserted + " in xplanmgr.geltungsbereich.");
		}
		catch (SQLException | DatabaseException e) {
			throw new CustomChangeException(e);
		}
	}

	@Override
	public String getConfirmationMessage() {
		return "Updated geltungsbereich";
	}

	@Override
	public void setUp() throws SetupException {

	}

	@Override
	public void setFileOpener(ResourceAccessor resourceAccessor) {

	}

	@Override
	public ValidationErrors validate(Database database) {
		return new ValidationErrors();
	}

}
