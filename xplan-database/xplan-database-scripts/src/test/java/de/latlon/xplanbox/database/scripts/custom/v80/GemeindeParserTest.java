/*-
 * #%L
 * xplan-database-scripts - Liquibase Changelogs zum Aufsetzen/Aktualisieren der Datenhaltung.
 * %%
 * Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package de.latlon.xplanbox.database.scripts.custom.v80;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:goltz@lat-lon.de">Lyn Goltz </a>
 */
public class GemeindeParserTest {

	@Test
	void verify_parseGemeinde() throws XMLStreamException, IOException {
		InputStream is = getClass().getResourceAsStream("/BPlan001_5-2_Gemeinden.gml");
		List<GemeindeParser.Gemeinde> gemeinden = GemeindeParser.parseGemeinden(is);

		assertEquals(2, gemeinden.size());

	}

	@Test
	void verify_parseGemeinden_noGemeindenAvailable() throws XMLStreamException, IOException {
		InputStream is = getClass().getResourceAsStream("/BPlan001_5-2_NoGemeinden.gml");
		List<GemeindeParser.Gemeinde> gemeinden = GemeindeParser.parseGemeinden(is);

		assertEquals(0, gemeinden.size());

	}

}
