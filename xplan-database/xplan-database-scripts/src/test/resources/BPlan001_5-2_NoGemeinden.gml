﻿<?xml version="1.0" encoding="utf-8"?>
<!--
  #%L
  xplan-database-scripts - Liquibase Changelogs zum Aufsetzen/Aktualisieren der Datenhaltung.
  %%
  Copyright (C) 2008 - 2025 Freie und Hansestadt Hamburg, developed by lat/lon gesellschaft für raumbezogene Informationssysteme mbH
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->

<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_1EE271CF-56BA-4C7B-82BC-C26862404C1A" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/2 http://www.xplanungwiki.de/upload/XPlanGML/5.2/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/5/2">
  <!-- Testplan mit zwei BP_Bereich Objekten und Rasterdaten -->
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>576416.125 5942410.13</gml:lowerCorner>
      <gml:upperCorner>576662.68 5942588.473</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_1c23cbae-b6e9-4f21-b9c2-f3fc6df46545">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576416.125 5942410.13</gml:lowerCorner>
          <gml:upperCorner>576662.68 5942588.473</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan001_5-2 Bereiche</xplan:name>
      <xplan:beschreibung>Der vorhabenbezogene Bebauungsplan BPlan001_5-2 für das Gebiet südlich Schierenberg, westlich Saseler Straße (Bezirk Wandsbek, Ortsteil 526) wird festgestellt. Das Plangebiet wird wie folgt begrenzt: Schierenberg – Saseler Straße – Südostgrenze des Flurstücks 129, über das Flur-stück 5687, Südgrenzen der Flurstücke 5687 und 5684, Ostgrenzen der Flurstücke 4402 und 4403, über die Flurstücke 4403 und 4401 und Westgrenzen der Flurstücke 4401 und 4400 der Gemarkung Meiendorf.</xplan:beschreibung>
      <xplan:technHerstellDatum>2016-07-21</xplan:technHerstellDatum>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_B44A56A5-756B-4732-A2F4-57356F2A7CD9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576599.891 5942440.018 576598.756 5942468.151 576611.81 5942468.677
                576626.513 5942487.073 576625.447 5942487.931 576652.088 5942522.116
                576656.335 5942527.565 576655.825 5942531.79 576662.68 5942540.274
                576601.22 5942588.473 576588.349 5942584.209 576416.125 5942527.154
                576416.434 5942514.7 576419.617 5942443.127 576419.938 5942435.927
                576421.087 5942410.13 576423.587 5942410.205 576480.297 5942411.883
                576480.165 5942424.848 576485.589 5942430.139 576505.565 5942430.669
                576512.841 5942426.038 576516.148 5942426.171 576516.545 5942413.603
                576593.045 5942416.37 576600.834 5942416.652 576599.891 5942440.018
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#GML_b4003c7b-9bc5-4d29-8de8-ddc80de9f251" />
      <xplan:texte xlink:href="#GML_51a21b00-c38b-4ff7-bc86-766994a242f1" />
      <xplan:texte xlink:href="#GML_c61d212b-e6b2-45c8-8d13-640d1550668e" />
      <xplan:texte xlink:href="#GML_ad4a57ec-d094-4fb7-bf50-96dd5537d474" />
      <xplan:texte xlink:href="#GML_8d149ef7-4b94-40d9-b994-6b222dd4734e" />
      <xplan:texte xlink:href="#GML_abd5458e-92a4-4620-b6ec-944a997c3fcf" />
      <xplan:texte xlink:href="#GML_6c87c405-f456-4a52-ae02-d345333bc7c2" />
      <xplan:texte xlink:href="#GML_28cf5e05-9279-4205-946b-aad02be217dd" />
      <xplan:texte xlink:href="#GML_78445473-8bfb-4234-bcfa-9675e3c39fe7" />
      <xplan:texte xlink:href="#GML_e54b2640-a9f8-4b60-be2b-11345fb076b4" />
      <xplan:texte xlink:href="#GML_da22f094-ff79-47c3-905d-57125ad15bbe" />
      <xplan:texte xlink:href="#GML_0b4c3abf-323c-4369-bba0-fdaf90a05e18" />
      <xplan:texte xlink:href="#GML_ba86d815-a484-4530-83fe-ad2003191bdf" />
      <xplan:texte xlink:href="#GML_1ed1108a-2204-47f8-822f-08e3ddc697e2" />
      <xplan:texte xlink:href="#GML_854a644c-3f65-4596-8362-c6eb77cb747d" />
      <xplan:texte xlink:href="#GML_bd96f489-ace2-4a87-8731-9ef82e38c8c1" />
      <xplan:texte xlink:href="#GML_5060fe64-2557-4954-af7c-0f79ca0811dd" />
      <xplan:texte xlink:href="#GML_7c360c93-0ccf-44bd-85cd-3c9c54647cc8" />
      <xplan:texte xlink:href="#GML_c9dc8fb4-db8d-411f-9974-ebd078dc055a" />
      <xplan:texte xlink:href="#GML_ea29c620-e827-4e8c-8fc1-f7f0e605904e" />
      <xplan:texte xlink:href="#GML_a7b715e5-c683-4ac9-ae8c-f6b3960810d9" />
      <xplan:texte xlink:href="#GML_b63ca453-1e1e-4f09-b99f-bc4dec515188" />
      <xplan:texte xlink:href="#GML_c984e887-2e6f-4cfd-a569-3f54d81c29e6" />
      <xplan:texte xlink:href="#GML_a3c27752-304e-4220-8eed-a2d91f2531d7" />
      <xplan:planArt>3000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2016-06-21</xplan:rechtsverordnungsDatum>
      <xplan:durchfuehrungsVertrag>true</xplan:durchfuehrungsVertrag>
      <xplan:versionBauNVOText>Version vom 11.06.2013</xplan:versionBauNVOText>
      <xplan:bereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:bereich xlink:href="#Gml_1A345FC1-C010-43E1-8E55-3755CC94C54E" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_b4003c7b-9bc5-4d29-8de8-ddc80de9f251">
      <xplan:schluessel>§2 Nr.15</xplan:schluessel>
      <xplan:text>Für festgesetzte Einzelbäume sowie auf den Flächen für
        die Erhaltung und zum Anpflanzen von Bäumen und
        Sträuchern sind bei Abgang gleichwertige Ersatzpflanzungen
        vorzunehmen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_51a21b00-c38b-4ff7-bc86-766994a242f1">
      <xplan:schluessel>§2 Nr.14</xplan:schluessel>
      <xplan:text>Auf der mit „(E)“ bezeichneten Fläche für die Erhaltung
        und zum Anpflanzen von Bäumen und Sträuchern sind
        Lücken mit Bäumen und Sträuchern so zu schließen sowie
        Wall-Aufsetzarbeiten so durchzuführen, dass der Charakter
        und Aufbau eines Knicks erhalten bleibt.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c61d212b-e6b2-45c8-8d13-640d1550668e">
      <xplan:schluessel>§2 Nr.23</xplan:schluessel>
      <xplan:text>Auf der mit „(F)“ bezeichneten Fläche für Maßnahmen
        zum Schutz, zur Pflege und zur Entwicklung von Boden,
        Natur und Landschaft ist eine mindestens 250 m² große
        Hochstaudenflur herzustellen. Die Fläche ist allseitig einzuzäunen
        und zu dem mit „MI 1“ bezeichneten Mischgebiet zusätzlich mit einer vorgelagerten, dichtwachsenden
        zweireihigen Strauchhecke abzupflanzen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ad4a57ec-d094-4fb7-bf50-96dd5537d474">
      <xplan:schluessel>§2 Nr.10</xplan:schluessel>
      <xplan:text>Einseitig zu den mit „(B)“ gekennzeichneten Gebäudeseiten
        ausgerichtete Wohnungen sind unzulässig. Zu den mit
        „(C)“ gekennzeichneten Gebäudeseiten ausgerichtete
        offene Außenwohnbereiche (zum Beispiel Balkone, Loggien,
        Terrassen) sind unzulässig. An den mit „(C)“ gekennzeichneten
        Gebäudeseiten sind entweder vor den Aufenthaltsräumen
        verglaste Vorbauten wie zum Beispiel Doppelfassaden,
        verglaste Loggien, Wintergärten, verglaste
        Laubengänge oder in ihrer Wirkung vergleichbare Maßnahmen
        vorzusehen oder in den Aufenthaltsräumen durch
        geeignete bauliche Schallschutzmaßnahmen wie zum Beispiel
        Doppelfassaden, verglaste Vorbauten, besondere
        Fensterkonstruktionen oder in ihrer Wirkung vergleichbare
        Maßnahmen sicherzustellen, dass durch diese baulichen
        Maßnahmen insgesamt eine Schallpegeldifferenz
        erreicht wird, die es ermöglicht, dass in Aufenthaltsräumen
        ein lnnenraumpegel von 40 dB(A) während der Tagzeit
        und 30 dB(A) während der Nachtzeit bei teilgeöffneten
        Fenstern nicht überschritten wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_8d149ef7-4b94-40d9-b994-6b222dd4734e">
      <xplan:schluessel>§2 Nr.24</xplan:schluessel>
      <xplan:text>Auf den mit „(G)“ bezeichneten Flächen für Maßnahmen
        zum Schutz, zur Pflege und zur Entwicklung von Boden,
        Natur und Landschaft ist jeweils eine dichtwachsende
        zweireihige Strauchhecke aus dornenbewehrten Arten
        anzupflanzen und auf der Südseite einzuzäunen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_abd5458e-92a4-4620-b6ec-944a997c3fcf">
      <xplan:schluessel>§2 Nr.18</xplan:schluessel>
      <xplan:text>In den allgemeinen Wohngebieten und in dem mit „MI 1“
        bezeichneten Mischgebiet sind Standplätze für Abfallbehälter
        außerhalb von Gebäuden mit Sträuchern oder
        Hecken einzugrünen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_6c87c405-f456-4a52-ae02-d345333bc7c2">
      <xplan:schluessel>§2 Nr.19</xplan:schluessel>
      <xplan:text>In den allgemeinen Wohngebieten und in dem mit „MI 1“
        bezeichneten Mischgebiet sind die Dachflächen von
        Gebäuden und Carports mit einem mindestens 8 cm starken
        durchwurzelbaren Substrataufbau zu versehen und
        mindestens extensiv zu begrünen. Ausnahmen für erforderliche
        befestigte Flächen und anderweitige Nutzungen
        können zugelassen werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_28cf5e05-9279-4205-946b-aad02be217dd">
      <xplan:schluessel>§2 Nr.20</xplan:schluessel>
      <xplan:text>Nicht überbaute Tiefgaragen sind mit einem mindestens
        50 cm starken durchwurzelbaren Substrataufbau zu versehen
        und dauerhaft zu begrünen. Ausnahmen für erforderliche
        befestigte Flächen können zugelassen werden.
        Für anzupflanzende Bäume auf Tiefgaragen muss auf einer
        Fläche von mindestens 12 m² je Baum die Schichtstärke
        des durchwurzelbaren Substrataufbaus mindestens 1 m
        betragen. Tiefgaragenzufahrten sind baulich einzufassen
        oder mit Rankgerüsten oder Pergolen zu überstellen und
        mit Schling- oder Kletterpflanzen zu begrünen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_78445473-8bfb-4234-bcfa-9675e3c39fe7">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>In den Mischgebieten sind Gartenbaubetriebe und Tankstellen
        unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_e54b2640-a9f8-4b60-be2b-11345fb076b4">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Das festgesetzte Gehrecht auf dem Flurstück 4402 der
        Gemarkung Meiendorf umfasst die Befugnis der Freien
        und Hansestadt Hamburg zu verlangen, dass die bezeichnete
        Fläche dem allgemeinen Fußgängerverkehr als Gehweg
        zur Verfügung gestellt wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_da22f094-ff79-47c3-905d-57125ad15bbe">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Eine Überschreitung der Baugrenzen durch Balkone um
        bis zu 1,5 m, durch Wintergärten um bis zu 1,75 m sowie
        durch ebenerdige Terrassen um bis zu 3 m, kann zugelassen
        werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_0b4c3abf-323c-4369-bba0-fdaf90a05e18">
      <xplan:schluessel>§2 Nr.13</xplan:schluessel>
      <xplan:text>Auf der mit „(D)“ bezeichneten Fläche für die Erhaltung
        und zum Anpflanzen von Bäumen und Sträuchern sind
        Lücken mit Bäumen und Sträuchern so zu schließen, dass
        der Charakter und Aufbau des dichtwachsenden, gestuften
        Schutzgrüns wieder hergestellt und dauerhaft erhalten
        wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ba86d815-a484-4530-83fe-ad2003191bdf">
      <xplan:schluessel>§2 Nr.21</xplan:schluessel>
      <xplan:text>Auf den privaten Grundstücksflächen sind ebenerdige,
        nicht überdachte Stellplätze in wasser- und luftdurchlässigem
        Aufbau herzustellen. Feuerwehrumfahrten und -aufstellflächen
        auf zu begrünenden Flächen sind in vegetationsfähigem
        Aufbau (Schotterrasen) herzustellen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_1ed1108a-2204-47f8-822f-08e3ddc697e2">
      <xplan:schluessel>§2 Nr.22</xplan:schluessel>
      <xplan:text>Drainagen oder sonstige bauliche oder technische Maßnahmen,
        die zu einer dauerhaften Absenkung des vegetationsverfügbaren
        Grundwasserspiegels beziehungsweise
        von Staunässe führen, sind unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_854a644c-3f65-4596-8362-c6eb77cb747d">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>In den allgemeinen Wohngebieten sind Stellplätze nur in
        Tiefgaragen zulässig. Tiefgaragen sind auch außerhalb der
        Baugrenzen zulässig. Für Tiefgaragen und deren Zufahrten
        kann die festgesetzte Grundflächenzahl bis zu einer
        Grundflächenzahl von 0,8 in dem mit „WA 1“ und bis zu
        einer Grundflächenzahl von 0,7 in dem mit „WA 2“
        bezeichneten Wohngebiet überschritten werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_bd96f489-ace2-4a87-8731-9ef82e38c8c1">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>In den allgemeinen Wohngebieten und in dem mit „MI 1“
        bezeichneten Mischgebiet sind Staffelgeschosse unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_5060fe64-2557-4954-af7c-0f79ca0811dd">
      <xplan:schluessel>§2 Nr.17</xplan:schluessel>
      <xplan:text>In den Baugebieten sind an Straßenverkehrsflächen
        angrenzende Einfriedigungen nur in Form von Hecken
        oder durchbrochenen Zäunen in Verbindung mit Hecken
        zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_7c360c93-0ccf-44bd-85cd-3c9c54647cc8">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Im Rahmen der festgesetzten Nutzungen sind innerhalb
        des Vorhabengebiets nur solche Vorhaben zulässig, zu
        deren Durchführung sich der Vorhabenträger im Durchführungsvertrag
        verpflichtet.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c9dc8fb4-db8d-411f-9974-ebd078dc055a">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>In den allgemeinen Wohngebieten werden Ausnahmen für
        Gartenbaubetriebe und Tankstellen ausgeschlossen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ea29c620-e827-4e8c-8fc1-f7f0e605904e">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Auf der mit „(A)“ bezeichneten Fläche des Mischgebiets
        ist der dort vorhandene Tischlereibetrieb zulässig. Änderungen
        und Erneuerungen der betrieblichen Anlagen können
        ausnahmsweise zugelassen werden, wenn durch geeignete
        Maßnahmen und bauliche Vorkehrungen, wie zum
        Beispiel Einhausungen, sichergestellt wird, dass sich die
        vom Betrieb ausgehenden Emissionen nicht erhöhen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_a7b715e5-c683-4ac9-ae8c-f6b3960810d9">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>In den gewerblich geprägten Teilen der Mischgebiete sind
        Vergnügungsstätten, insbesondere Spielhallen, Wettbüros
        und ähnliche Unternehmen im Sinne von § 1 Absatz 2 des
        Hamburgischen Spielhallengesetzes vom 4. Dezember
        2012 (HmbGVBl. S. 505), die der Aufstellung von Spielgeräten
        mit oder ohne Gewinnmöglichkeiten dienen und
        Vorführ- und Geschäftsräume, deren Zweck auf Darstellungen
        oder auf Handlungen mit sexuellem Charakter ausgerichtet
        ist, unzulässig. In den übrigen Teilen der Mischgebiete
        werden Ausnahmen für Vergnügungsstätten ausgeschlossen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_b63ca453-1e1e-4f09-b99f-bc4dec515188">
      <xplan:schluessel>§2 Nr.11</xplan:schluessel>
      <xplan:text>Außerhalb von öffentlichen Straßenverkehrsflächen sind
        Geländeaufhöhungen und Abgrabungen sowie Ablagerungen
        im Kronenbereich zu erhaltender Bäume unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c984e887-2e6f-4cfd-a569-3f54d81c29e6">
      <xplan:schluessel>§2 Nr.16</xplan:schluessel>
      <xplan:text>In den allgemeinen Wohngebieten und in den Mischgebieten
        ist für je angefangene 250 m² der nicht überbaubaren
        Grundstücksfläche, einschließlich der zu begrünenden
        unterbauten Flächen, ein kleinkroniger Baum oder für je
        angefangene 500 m² mindestens ein großkroniger Baum zu
        pflanzen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_a3c27752-304e-4220-8eed-a2d91f2531d7">
      <xplan:schluessel>§2 Nr.12</xplan:schluessel>
      <xplan:text>Für festgesetzte Anpflanzungen und für Ersatzpflanzungen
        von Bäumen, Sträuchern und Hecken sind standortgerechte,
        einheimische Laubgehölzarten zu verwenden
        und dauerhaft zu erhalten. Ausnahmen können zugelassen
        werden. Bäume müssen einen Stammumfang von mindestens
        18 cm, in 1 m Höhe über dem Erdboden gemessen,
        aufweisen. Im Kronenbereich jedes Baumes ist eine offene
        Vegetationsfläche von mindestens 12 m² anzulegen und zu
        begrünen. Sträucher und Heckenpflanzen müssen mindestens
        folgende Qualität aufweisen: Zweimal verpflanzt,
        Höhe mindestens 60 cm.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576416.125 5942410.13</gml:lowerCorner>
          <gml:upperCorner>576662.68 5942588.473</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:bedeutung>1600</xplan:bedeutung>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_42F0B741-8812-464A-B458-486949C7E924" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576655.825 5942531.79 576662.68 5942540.274 576601.22 5942588.473
                576588.349 5942584.209 576416.125 5942527.154 576416.434 5942514.7
                576419.617 5942443.127 576419.938 5942435.927 576421.087 5942410.13
                576423.587 5942410.205 576480.297 5942411.883 576480.165 5942424.848
                576485.589 5942430.139 576505.565 5942430.669 576512.841 5942426.038
                576516.148 5942426.171 576516.545 5942413.603 576593.045 5942416.37
                576600.834 5942416.652 576599.891 5942440.018 576598.756 5942468.151
                576597.1439 5942508.047 576594.615 5942570.631 576609.257 5942567.967
                576639.6672 5942544.3423 576655.825 5942531.79 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>BPlan001_5-2_Bereich0.pgw</xplan:georefURL>
          <xplan:referenzURL>BPlan001_5-2_Bereich0.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
      <xplan:planinhalt xlink:href="#GML_0a7293a1-8897-42a2-8f7d-8e652a2f3a04" />
      <xplan:planinhalt xlink:href="#GML_f58a0678-d7ea-409c-94ba-a3c3c199f8e3" />
      <xplan:planinhalt xlink:href="#GML_4a804a8a-3125-455e-a89a-bf3783b92da1" />
      <xplan:planinhalt xlink:href="#GML_2aa72a5e-4e29-4258-9969-aca657c6774e" />
      <xplan:planinhalt xlink:href="#GML_8f0e7555-075d-4dd0-957a-bc93c5a42c1e" />
      <xplan:planinhalt xlink:href="#GML_3981919b-339d-47be-8361-fa38881dcb29" />
      <xplan:planinhalt xlink:href="#GML_49ddaa03-f4e7-4847-a4b6-275295694227" />
      <xplan:planinhalt xlink:href="#GML_7f7168b3-920f-4672-85de-df3b8a712c6f" />
      <xplan:planinhalt xlink:href="#GML_c662dafa-b6fc-4115-a0a0-3e53c1094a99" />
      <xplan:planinhalt xlink:href="#GML_59d9124b-f014-4c8b-a9d1-d11638a73bca" />
      <xplan:planinhalt xlink:href="#GML_7a1a96af-d38b-4325-a0bd-f1e1e4ce9680" />
      <xplan:planinhalt xlink:href="#GML_85090559-3c6e-4f29-bb6e-418f2f7bff9c" />
      <xplan:planinhalt xlink:href="#GML_627b957f-ad28-472d-a7c8-5aeb75e823a1" />
      <xplan:planinhalt xlink:href="#GML_6faad647-0a64-405e-a0be-515184414780" />
      <xplan:planinhalt xlink:href="#GML_4c252228-adbf-4027-83ef-4a7373cf8ad9" />
      <xplan:planinhalt xlink:href="#GML_2f681dc9-7be4-43bd-8542-e801008aab39" />
      <xplan:planinhalt xlink:href="#GML_4edc6426-2d9d-4408-bbf6-0865c0271e08" />
      <xplan:planinhalt xlink:href="#GML_508ca656-eafb-48ff-9ee4-8cef746898ee" />
      <xplan:planinhalt xlink:href="#GML_df45a20e-be9e-4cb6-8053-21df2169c36b" />
      <xplan:planinhalt xlink:href="#GML_3951a89c-a3f9-4065-aa6d-767e9549235a" />
      <xplan:planinhalt xlink:href="#GML_842bad69-eb2b-43f4-bd1b-45dba4c33ef7" />
      <xplan:planinhalt xlink:href="#GML_038d8a87-7958-4bc0-b9a5-93f725ee77c9" />
      <xplan:planinhalt xlink:href="#GML_432bd289-8860-4325-88de-64896e887095" />
      <xplan:planinhalt xlink:href="#GML_c07fcf83-6309-40dd-86d0-b9ebc8f44055" />
      <xplan:planinhalt xlink:href="#GML_1e89c3a6-8e10-4354-abd2-a518e932c6fb" />
      <xplan:planinhalt xlink:href="#GML_3647946c-2a30-4078-909d-35f765227756" />
      <xplan:planinhalt xlink:href="#GML_ac8bcc7c-996b-403c-beda-0c44e62e5526" />
      <xplan:planinhalt xlink:href="#GML_1d7081c1-9315-478a-b33a-bba36110dd12" />
      <xplan:planinhalt xlink:href="#GML_3bbebe5a-71f8-4d11-b1e9-b8802aa0a2ec" />
      <xplan:planinhalt xlink:href="#GML_dad385e7-637b-40c7-8d96-ca50c85d4c6d" />
      <xplan:planinhalt xlink:href="#GML_631c2328-7244-4952-9fcd-9f7c993901b5" />
      <xplan:planinhalt xlink:href="#GML_f9aebab9-62a1-45e0-bcca-7998ec7156c2" />
      <xplan:planinhalt xlink:href="#GML_1f6bab62-0bb3-4fec-881e-b0cd25c51168" />
      <xplan:planinhalt xlink:href="#GML_47fb016d-dab5-4213-a4c1-deed9fb7dd94" />
      <xplan:planinhalt xlink:href="#GML_1a82a070-de61-4fbe-ac33-6891f7702b34" />
      <xplan:planinhalt xlink:href="#GML_3b8a6af5-457e-4e0f-9e12-60b936cadfdf" />
      <xplan:planinhalt xlink:href="#GML_8047a192-d634-4414-b38a-950438ea177c" />
      <xplan:planinhalt xlink:href="#GML_3f91fd67-0c0b-4b1a-aa98-8753c8199ba8" />
      <xplan:planinhalt xlink:href="#GML_0e8dfd5a-603d-4c56-b846-5f5c1573c528" />
      <xplan:planinhalt xlink:href="#GML_b1826698-ecc5-4cb0-9c88-d5afe1f31b5d" />
      <xplan:planinhalt xlink:href="#GML_60fb20fd-8cd3-4173-ac56-880f4451e4d8" />
      <xplan:planinhalt xlink:href="#GML_11901099-8d74-4850-8177-6541547ab30b" />
      <xplan:planinhalt xlink:href="#GML_42c7fef5-324b-4253-ada5-725c99cb27cf" />
      <xplan:planinhalt xlink:href="#GML_438cb8ae-be20-4cce-b7fa-6d5b7326a357" />
      <xplan:planinhalt xlink:href="#GML_ba300100-99e7-4741-8fc0-323cbfe283f3" />
      <xplan:planinhalt xlink:href="#GML_269bef7b-7d34-40e6-b48d-611a5001ab73" />
      <xplan:planinhalt xlink:href="#GML_e3d2f053-c8ae-498f-aab5-b497490ec636" />
      <xplan:planinhalt xlink:href="#GML_f352bbb5-f0e0-4059-8c86-61f393a9c732" />
      <xplan:planinhalt xlink:href="#GML_6bd85817-ed09-4b54-8ffa-d1faa982d616" />
      <xplan:planinhalt xlink:href="#GML_1ce434d3-ca84-4476-8cb2-18cd42647531" />
      <xplan:planinhalt xlink:href="#GML_c94e422a-b3ed-4d92-95d5-8dc4c189650e" />
      <xplan:planinhalt xlink:href="#GML_96a7bd77-f199-4cea-84d3-fdbabfe3f643" />
      <xplan:planinhalt xlink:href="#GML_a95a1007-a70f-474c-b791-ffb675af4015" />
      <xplan:planinhalt xlink:href="#GML_bff9a682-a559-4db3-a3b5-4fabc70907a6" />
      <xplan:planinhalt xlink:href="#GML_b021ad8f-e4d0-498b-896a-2db5e334ff4d" />
      <xplan:planinhalt xlink:href="#GML_3264fd82-1958-4a67-8673-42cb63036f17" />
      <xplan:planinhalt xlink:href="#GML_109fb3fb-2e1f-45a1-86af-2bb1dcc6d8ed" />
      <xplan:planinhalt xlink:href="#GML_1a6c9d6a-6e93-4854-9229-bd579e4d0df6" />
      <xplan:planinhalt xlink:href="#GML_a8fcb5cd-7364-46b6-8ad9-beb227979325" />
      <xplan:praesentationsobjekt xlink:href="#GML_8ded082b-f799-4b8c-ba49-a1aa23a63d8f" />
      <xplan:praesentationsobjekt xlink:href="#GML_4ebd4bb6-0bf4-460b-a23a-bdd149ebef6f" />
      <xplan:praesentationsobjekt xlink:href="#GML_00f7eff9-1faf-4448-8ec7-1f0ef6c16d6d" />
      <xplan:praesentationsobjekt xlink:href="#GML_5ba9de57-cd5c-4658-a2af-afd65f5b25bf" />
      <xplan:praesentationsobjekt xlink:href="#GML_1f29d226-6c44-4dd2-ac8e-d109edb17354" />
      <xplan:praesentationsobjekt xlink:href="#GML_05afd08d-622c-4587-829b-6e25b25c7e97" />
      <xplan:praesentationsobjekt xlink:href="#GML_71de5645-d06c-4eb8-82f7-97b8bf8a00e9" />
      <xplan:praesentationsobjekt xlink:href="#GML_c50c09c9-84d0-48ad-8c29-0a823b223514" />
      <xplan:praesentationsobjekt xlink:href="#GML_05d2bf60-cc10-4a38-ab83-d3af0057d8f7" />
      <xplan:praesentationsobjekt xlink:href="#GML_77b72a48-372d-482a-8355-c966829968ba" />
      <xplan:praesentationsobjekt xlink:href="#GML_b651ba20-7173-4b57-a0f7-cd61928b563b" />
      <xplan:praesentationsobjekt xlink:href="#GML_bc8efcd2-efba-4a41-bd7f-6d3b26f272de" />
      <xplan:praesentationsobjekt xlink:href="#GML_c233cc65-4339-4252-934c-24fb7eb2ad14" />
      <xplan:praesentationsobjekt xlink:href="#GML_5e9681f5-4c33-46f4-8113-b287cbe57b3a" />
      <xplan:praesentationsobjekt xlink:href="#GML_6a527b3a-3287-42fe-8705-d1e28dcbb3d7" />
      <xplan:praesentationsobjekt xlink:href="#GML_b4c0ed8b-3299-4bc4-bfd6-72a53a6957b8" />
      <xplan:praesentationsobjekt xlink:href="#GML_6bfad91a-e310-49d9-93ac-2677f9578e6d" />
      <xplan:praesentationsobjekt xlink:href="#GML_150032b9-df25-4c7a-abf2-b05693dcccba" />
      <xplan:praesentationsobjekt xlink:href="#GML_83094468-6c4d-4e1a-ad90-8a86710d3363" />
      <xplan:praesentationsobjekt xlink:href="#GML_33707095-3440-490b-9d83-c362569a0ef4" />
      <xplan:praesentationsobjekt xlink:href="#GML_0ab64645-e5c3-4661-b57e-ae29122cf610" />
      <xplan:praesentationsobjekt xlink:href="#GML_7796ee9b-c35f-4e4d-9309-269006f6ccf8" />
      <xplan:praesentationsobjekt xlink:href="#GML_ac19c4a5-51ea-4718-b2e3-98cff0c05273" />
      <xplan:praesentationsobjekt xlink:href="#GML_5c143714-4150-4f49-8d5a-d2b826e7958d" />
      <xplan:praesentationsobjekt xlink:href="#GML_cf666f15-e87c-4193-b484-68ccd5e72343" />
      <xplan:praesentationsobjekt xlink:href="#GML_94de2016-50e2-4e56-9819-3e80454a2a36" />
      <xplan:praesentationsobjekt xlink:href="#GML_c2a44c83-eb3f-46df-ba27-96f0ad9ab3c8" />
      <xplan:praesentationsobjekt xlink:href="#GML_cf3fbe32-bf2e-4dc1-8386-7ec5c0532f1b" />
      <xplan:praesentationsobjekt xlink:href="#GML_916188a0-f5b0-4f17-942f-c25a0d915bd9" />
      <xplan:praesentationsobjekt xlink:href="#GML_0bdc76ef-35d5-4c75-9d5b-fa3ea30ba4d1" />
      <xplan:praesentationsobjekt xlink:href="#GML_a5d113d2-aae0-4d37-be0d-86c438e4381f" />
      <xplan:praesentationsobjekt xlink:href="#GML_d0723b30-7904-45b2-9b6a-8b55bf18b911" />
      <xplan:praesentationsobjekt xlink:href="#GML_15890cb4-3da8-43fc-ab37-2c9ecc3cd74c" />
      <xplan:praesentationsobjekt xlink:href="#GML_231f5f5d-30e9-4114-a1ce-e88cd7e3d2b2" />
      <xplan:versionBauNVOText>Version vom 11.06.2013</xplan:versionBauNVOText>
      <xplan:gehoertZuPlan xlink:href="#GML_1c23cbae-b6e9-4f21-b9c2-f3fc6df46545" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="GML_0a7293a1-8897-42a2-8f7d-8e652a2f3a04">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576419.618 5942410.13</gml:lowerCorner>
          <gml:upperCorner>576480.297 5942443.668</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_8FC74F80-50EF-486D-8307-D490AF0ACE15" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576429.724 5942428.829 576436.04 5942429.111 576436.179 5942434.919
                576431.773 5942443.668 576419.618 5942443.127 576419.938 5942435.927
                576421.087 5942410.13 576423.587 5942410.205 576480.297 5942411.883
                576480.266 5942414.884 576430.41 5942413.408 576429.724 5942428.829
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>9999</xplan:klassifizMassnahme>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_f58a0678-d7ea-409c-94ba-a3c3c199f8e3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576429.409 5942439.738</gml:lowerCorner>
          <gml:upperCorner>576456.937 5942513.277</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_94de2016-50e2-4e56-9819-3e80454a2a36" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_8B032B00-5C41-4665-AEBB-D70CD93CF481" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576456.937 5942440.135 576455.999 5942471.816 576442.12 5942513.277
                576429.409 5942509.053 576442.564 5942469.424 576443.446 5942439.738
                576456.937 5942440.135 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_94de2016-50e2-4e56-9819-3e80454a2a36">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576448.798 5942469.435</gml:lowerCorner>
          <gml:upperCorner>576448.798 5942469.435</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f58a0678-d7ea-409c-94ba-a3c3c199f8e3" />
      <xplan:position>
        <gml:Point gml:id="Gml_0EFFA883-ADA4-47E1-BC24-0D01C2CA1E3A" srsName="EPSG:25832">
          <gml:pos>576448.798 5942469.435</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_4a804a8a-3125-455e-a89a-bf3783b92da1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576572.789 5942443.581</gml:lowerCorner>
          <gml:upperCorner>576586.817 5942462.063</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_59E6236F-03B6-42C7-823D-500C0FB971AF" srsName="EPSG:25832">
          <gml:posList>576572.789 5942461.663 576573.324 5942443.581 576586.817 5942443.98
            576586.283 5942462.063 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_2aa72a5e-4e29-4258-9969-aca657c6774e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576591.66 5942440.947</gml:lowerCorner>
          <gml:upperCorner>576591.66 5942440.947</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">41.07</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_4A08737B-AB65-4E71-870D-CCF9770964B7" srsName="EPSG:25832">
          <gml:pos>576591.66 5942440.947</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_8f0e7555-075d-4dd0-957a-bc93c5a42c1e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576492.223 5942512.76</gml:lowerCorner>
          <gml:upperCorner>576510.74 5942534.189</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_C7C8333A-6BFA-4EA5-90D8-201E4A9BC1C1" srsName="EPSG:25832">
          <gml:posList>576510.74 5942517.017 576505.034 5942534.189 576492.223 5942529.932
            576497.929 5942512.76 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_3981919b-339d-47be-8361-fa38881dcb29">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576527.157 5942509.662</gml:lowerCorner>
          <gml:upperCorner>576577.155 5942537.673</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_7262C208-27C2-40E5-825A-2B6F908C00CB" srsName="EPSG:25832">
          <gml:posList>576531.415 5942509.662 576577.155 5942524.862 576572.897 5942537.673
            576527.157 5942522.473 576531.415 5942509.662 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_49ddaa03-f4e7-4847-a4b6-275295694227">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576416.434 5942474.449</gml:lowerCorner>
          <gml:upperCorner>576655.825 5942571.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_05FE9CA9-8375-4C41-9A09-82CF68E49E69" srsName="EPSG:25832">
          <gml:posList>576655.825 5942531.79 576609.257 5942567.967 576594.615 5942570.631
            576588.214 5942571.795 576585.605 5942570.928 576580.138 5942569.112
            576578.266 5942565.328 576584.585 5942546.633 576586.698 5942540.648
            576584.613 5942538.256 576585.286 5942536.147 576587.281 5942530.022
            576590.82 5942519.15 576591.61 5942515.589 576592.25 5942494.172
            576591.693 5942485.065 576590.737 5942484.009 576589.163 5942482.269
            576586.072 5942480.109 576582.576 5942478.696 576578.853 5942478.1
            576461.588 5942474.628 576455.548 5942474.449 576442.293 5942514.335
            576446.184 5942515.628 576444.447 5942520.855 576440.666 5942522.762
            576416.434 5942514.7 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="GML_7f7168b3-920f-4672-85de-df3b8a712c6f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576516.45 5942413.603</gml:lowerCorner>
          <gml:upperCorner>576593.045 5942419.368</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_84050227-8FC6-4370-931D-9671FAA1B42E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576593.045 5942416.37 576592.923 5942419.368 576516.45 5942416.602
                576516.545 5942413.603 576593.045 5942416.37 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NebenanlagenFlaeche gml:id="GML_c662dafa-b6fc-4115-a0a0-3e53c1094a99">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576418.023 5942507.169</gml:lowerCorner>
          <gml:upperCorner>576446.184 5942522.771</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_cf3fbe32-bf2e-4dc1-8386-7ec5c0532f1b" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_48BA4436-BD00-446D-9B35-C3F613165F85" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576442.293 5942514.335 576446.184 5942515.628 576444.447 5942520.855
                576440.649 5942522.771 576418.023 5942515.238 576420.704 5942507.169
                576442.293 5942514.335 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_NebenanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_cf3fbe32-bf2e-4dc1-8386-7ec5c0532f1b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576435.799 5942516.668</gml:lowerCorner>
          <gml:upperCorner>576435.799 5942516.668</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c662dafa-b6fc-4115-a0a0-3e53c1094a99" />
      <xplan:position>
        <gml:Point gml:id="Gml_10F8E369-D6F9-43BF-A2CF-D948919422D5" srsName="EPSG:25832">
          <gml:pos>576435.799 5942516.668</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="GML_59d9124b-f014-4c8b-a9d1-d11638a73bca">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576416.434 5942410.13</gml:lowerCorner>
          <gml:upperCorner>576600.834 5942571.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_D54DDA9F-AF71-431C-8205-1779FB76F6D5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576599.891 5942440.018 576598.756 5942468.151 576594.615 5942570.631
                576588.214 5942571.795 576585.605 5942570.928 576580.138 5942569.112
                576566.153 5942564.472 576456.312 5942527.947 576440.649 5942522.771
                576416.434 5942514.7 576419.618 5942443.127 576419.938 5942435.927
                576421.087 5942410.13 576423.587 5942410.205 576480.297 5942411.883
                576480.266 5942414.884 576480.165 5942424.848 576485.589 5942430.139
                576505.565 5942430.669 576512.841 5942426.038 576516.148 5942426.171
                576516.45 5942416.602 576516.545 5942413.603 576593.045 5942416.37
                576600.834 5942416.652 576599.891 5942440.018 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>8000</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_7a1a96af-d38b-4325-a0bd-f1e1e4ce9680">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576501.302 5942537.538</gml:lowerCorner>
          <gml:upperCorner>576501.302 5942537.538</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_77E2EAA8-5C2E-49C5-9583-3CC7E59C3435" srsName="EPSG:25832">
          <gml:pos>576501.302 5942537.538</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
      <xplan:kronendurchmesser uom="m">1</xplan:kronendurchmesser>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_85090559-3c6e-4f29-bb6e-418f2f7bff9c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576569.222 5942517.447</gml:lowerCorner>
          <gml:upperCorner>576584.731 5942558.997</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e54b2640-a9f8-4b60-be2b-11345fb076b4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_CF65598F-B8B1-4084-A0B3-278D15BEF5AA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576584.731 5942518.078 576571.12 5942558.997 576569.222 5942558.366
                576582.833 5942517.447 576584.731 5942518.078 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_627b957f-ad28-472d-a7c8-5aeb75e823a1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576429.724 5942413.408</gml:lowerCorner>
          <gml:upperCorner>576592.923 5942439.776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(E)</xplan:gliederung1>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1f29d226-6c44-4dd2-ac8e-d109edb17354" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_231f5f5d-30e9-4114-a1ce-e88cd7e3d2b2" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_51a21b00-c38b-4ff7-bc86-766994a242f1" />
      <xplan:refTextInhalt xlink:href="#GML_0b4c3abf-323c-4369-bba0-fdaf90a05e18" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2FF2D69E-EC33-4A9A-90DF-FAC9BDC31189" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576592.094 5942439.776 576462.797 5942435.779 576462.798 5942435.749
                576436.179 5942434.919 576436.04 5942429.111 576429.724 5942428.829
                576430.41 5942413.408 576480.266 5942414.884 576480.165 5942424.848
                576485.589 5942430.139 576505.565 5942430.669 576512.841 5942426.038
                576516.148 5942426.171 576516.45 5942416.602 576592.923 5942419.368
                576592.094 5942439.776 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1f29d226-6c44-4dd2-ac8e-d109edb17354">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576568.679 5942428.219</gml:lowerCorner>
          <gml:upperCorner>576568.679 5942428.219</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_627b957f-ad28-472d-a7c8-5aeb75e823a1" />
      <xplan:position>
        <gml:Point gml:id="Gml_E52F6BF7-2108-4AF5-A631-56D9347EF0EF" srsName="EPSG:25832">
          <gml:pos>576568.679 5942428.219</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_231f5f5d-30e9-4114-a1ce-e88cd7e3d2b2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576469.912 5942419.198</gml:lowerCorner>
          <gml:upperCorner>576469.912 5942419.198</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_627b957f-ad28-472d-a7c8-5aeb75e823a1" />
      <xplan:schriftinhalt>(E)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_1BA42B7E-4759-46A6-9C81-77DAD3858EA6" srsName="EPSG:25832">
          <gml:pos>576469.912 5942419.198</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6faad647-0a64-405e-a0be-515184414780">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576521.453 5942522.473</gml:lowerCorner>
          <gml:upperCorner>576539.969 5942543.897</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_F74A371C-2259-48CC-9B2E-26F408477805" srsName="EPSG:25832">
          <gml:posList>576539.969 5942526.73 576534.264 5942543.897 576521.453 5942539.64
            576527.157 5942522.473 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_4c252228-adbf-4027-83ef-4a7373cf8ad9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576572.789 5942443.581</gml:lowerCorner>
          <gml:upperCorner>576586.817 5942462.063</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7796ee9b-c35f-4e4d-9309-269006f6ccf8" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_FD35F040-8775-4DC1-9250-378758802639" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576586.817 5942443.98 576586.283 5942462.063 576572.789 5942461.663
                576573.324 5942443.581 576586.817 5942443.98 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7796ee9b-c35f-4e4d-9309-269006f6ccf8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576579.803 5942452.822</gml:lowerCorner>
          <gml:upperCorner>576579.803 5942452.822</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_4c252228-adbf-4027-83ef-4a7373cf8ad9" />
      <xplan:position>
        <gml:Point gml:id="Gml_48C89054-C333-4A7D-AF36-555D000321D0" srsName="EPSG:25832">
          <gml:pos>576579.803 5942452.822</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_2f681dc9-7be4-43bd-8542-e801008aab39">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576537.705 5942460.636</gml:lowerCorner>
          <gml:upperCorner>576586.283 5942475.557</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_E8B43C28-CCEA-409A-B4C4-41BC69BB5FE2" srsName="EPSG:25832">
          <gml:posList>576586.283 5942462.063 576585.884 5942475.557 576537.705 5942474.13
            576538.104 5942460.636 576586.283 5942462.063 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_4edc6426-2d9d-4408-bbf6-0865c0271e08">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576454.397 5942483.772</gml:lowerCorner>
          <gml:upperCorner>576585.945 5942564.472</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_88E0D2BE-19B0-4819-ACE4-91EDCC4574F2" srsName="EPSG:25832">
          <gml:posList>576454.397 5942524.162 576465.108 5942491.997 576466.066 5942489.137
            576467.009 5942487.194 576468.029 5942485.955 576468.991 5942485.155
            576470.103 5942484.509 576471.162 5942484.101 576472.593 5942483.814
            576473.672 5942483.772 576476.148 5942483.813 576574.305 5942486.719
            576577.618 5942487.564 576580.728 5942489.289 576582.388 5942490.772
            576583.807 5942492.573 576585.186 5942495.361 576585.945 5942499.069
            576585.446 5942515.928 576569.937 5942562.554 576566.153 5942564.472
            576456.312 5942527.947 576454.397 5942524.162 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_508ca656-eafb-48ff-9ee4-8cef746898ee">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576526.139 5942547.483</gml:lowerCorner>
          <gml:upperCorner>576526.139 5942547.483</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_A77464E8-3EEA-4563-89AA-E2F6C5AE9CDC" srsName="EPSG:25832">
          <gml:pos>576526.139 5942547.483</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
      <xplan:kronendurchmesser uom="m">1</xplan:kronendurchmesser>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_df45a20e-be9e-4cb6-8053-21df2169c36b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576432.12 5942433.33</gml:lowerCorner>
          <gml:upperCorner>576432.12 5942433.33</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">41</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_538055B4-EC7C-44D1-AA88-29EC09D6278C" srsName="EPSG:25832">
          <gml:pos>576432.12 5942433.33</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_3951a89c-a3f9-4065-aa6d-767e9549235a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576496.67 5942438.659</gml:lowerCorner>
          <gml:upperCorner>576496.67 5942438.659</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">41.3</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_BD3ED782-9557-4BFF-A568-E292BDE2BA89" srsName="EPSG:25832">
          <gml:pos>576496.67 5942438.659</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_842bad69-eb2b-43f4-bd1b-45dba4c33ef7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576459.295 5942501.817</gml:lowerCorner>
          <gml:upperCorner>576477.811 5942523.238</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_372990D9-6C48-43BC-8C3F-06B2F1BFC3A8" srsName="EPSG:25832">
          <gml:posList>576477.811 5942506.074 576472.107 5942523.238 576459.295 5942518.984
            576465 5942501.817 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_038d8a87-7958-4bc0-b9a5-93f725ee77c9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576472.633 5942440.616</gml:lowerCorner>
          <gml:upperCorner>576486.662 5942459.097</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4E0FB011-5E3E-475A-8808-2E858BC8591A" srsName="EPSG:25832">
          <gml:posList>576472.633 5942458.698 576473.168 5942440.616 576486.662 5942441.015
            576486.123 5942459.097 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_432bd289-8860-4325-88de-64896e887095">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576429.724 5942413.408</gml:lowerCorner>
          <gml:upperCorner>576592.924 5942439.776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_AB7517F1-F72F-4904-BB4C-DEB195F3FF14" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576592.094 5942439.776 576462.797 5942435.779 576462.798 5942435.749
                576436.179 5942434.919 576436.04 5942429.111 576429.724 5942428.829
                576430.41 5942413.408 576480.266 5942414.884 576480.165 5942424.848
                576485.589 5942430.139 576505.565 5942430.669 576512.841 5942426.038
                576516.148 5942426.171 576516.45 5942416.602 576592.924 5942419.348
                576592.094 5942439.776 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:massnahme>3000</xplan:massnahme>
      <xplan:gegenstand>2000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c07fcf83-6309-40dd-86d0-b9ebc8f44055">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576465 5942489.005</gml:lowerCorner>
          <gml:upperCorner>576514.998 5942517.017</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_05d2bf60-cc10-4a38-ab83-d3af0057d8f7" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_FB6D84EF-CB63-49B8-BE50-BBE94261D7F0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576514.998 5942504.206 576510.74 5942517.017 576465 5942501.817
                576469.253 5942489.005 576514.998 5942504.206 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_05d2bf60-cc10-4a38-ab83-d3af0057d8f7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576489.998 5942503.011</gml:lowerCorner>
          <gml:upperCorner>576489.998 5942503.011</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c07fcf83-6309-40dd-86d0-b9ebc8f44055" />
      <xplan:position>
        <gml:Point gml:id="Gml_D2B10062-54FC-49BE-A1AD-3ECACA066190" srsName="EPSG:25832">
          <gml:pos>576489.998 5942503.011</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_1e89c3a6-8e10-4354-abd2-a518e932c6fb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576465 5942489.005</gml:lowerCorner>
          <gml:upperCorner>576514.998 5942517.017</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_36546E5E-F74F-4EA3-B4EC-2188D0F68F08" srsName="EPSG:25832">
          <gml:posList>576469.253 5942489.005 576514.998 5942504.206 576510.74 5942517.017
            576465 5942501.817 576469.253 5942489.005 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_3647946c-2a30-4078-909d-35f765227756">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576527.157 5942509.662</gml:lowerCorner>
          <gml:upperCorner>576577.155 5942537.673</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b4c0ed8b-3299-4bc4-bfd6-72a53a6957b8" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_1E33130F-1378-41A7-BA51-66631721DF2F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576577.155 5942524.862 576572.897 5942537.673 576527.157 5942522.473
                576531.415 5942509.662 576577.155 5942524.862 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b4c0ed8b-3299-4bc4-bfd6-72a53a6957b8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576552.156 5942523.668</gml:lowerCorner>
          <gml:upperCorner>576552.156 5942523.668</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3647946c-2a30-4078-909d-35f765227756" />
      <xplan:position>
        <gml:Point gml:id="Gml_7AD69712-5ECA-428D-8D9A-FADAC7315A2C" srsName="EPSG:25832">
          <gml:pos>576552.156 5942523.668</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ac8bcc7c-996b-403c-beda-0c44e62e5526">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576420.864 5942475.609</gml:lowerCorner>
          <gml:upperCorner>576439.57 5942509.053</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_150032b9-df25-4c7a-abf2-b05693dcccba" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_C62C38AA-BD20-4007-A00B-861AFD857AD1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576439.57 5942478.445 576429.409 5942509.053 576420.864 5942506.213
                576431.033 5942475.609 576439.57 5942478.445 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_150032b9-df25-4c7a-abf2-b05693dcccba">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576430.217 5942492.331</gml:lowerCorner>
          <gml:upperCorner>576430.217 5942492.331</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ac8bcc7c-996b-403c-beda-0c44e62e5526" />
      <xplan:position>
        <gml:Point gml:id="Gml_A399277E-1234-4438-8C33-41806021341A" srsName="EPSG:25832">
          <gml:pos>576430.217 5942492.331</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_1d7081c1-9315-478a-b33a-bba36110dd12">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576516.45 5942413.603</gml:lowerCorner>
          <gml:upperCorner>576593.045 5942419.348</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(G)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_8d149ef7-4b94-40d9-b994-6b222dd4734e" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3B8A8F35-90FD-4683-8E92-35C39857C37A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576593.045 5942416.37 576592.924 5942419.348 576516.45 5942416.602
                576516.545 5942413.603 576593.045 5942416.37 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_3bbebe5a-71f8-4d11-b1e9-b8802aa0a2ec">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576416.125 5942474.449</gml:lowerCorner>
          <gml:upperCorner>576662.68 5942588.473</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_251385C4-5861-48CD-9839-0E2E85DB1397" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576601.22 5942588.473 576588.349 5942584.209 576416.125 5942527.154
                576416.434 5942514.7 576440.649 5942522.771 576444.447 5942520.855
                576446.184 5942515.628 576442.293 5942514.335 576455.548 5942474.449
                576461.588 5942474.628 576578.853 5942478.1 576582.576 5942478.696
                576586.072 5942480.109 576589.163 5942482.269 576590.737 5942484.009
                576591.693 5942485.065 576592.25 5942494.172 576591.61 5942515.589
                576590.82 5942519.15 576587.281 5942530.022 576585.286 5942536.147
                576584.613 5942538.256 576586.701 5942540.649 576585.643 5942543.641
                576584.585 5942546.633 576578.266 5942565.328 576580.138 5942569.112
                576585.605 5942570.928 576588.214 5942571.795 576594.615 5942570.631
                576609.257 5942567.967 576655.825 5942531.79 576662.68 5942540.274
                576601.22 5942588.473 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576454.397 5942524.162 576456.312 5942527.947 576566.153 5942564.472
                576569.937 5942562.554 576585.446 5942515.928 576585.945 5942499.069
                576585.186 5942495.361 576583.807 5942492.573 576582.388 5942490.772
                576580.728 5942489.289 576577.618 5942487.564 576574.305 5942486.719
                576476.148 5942483.813 576473.672 5942483.772 576472.593 5942483.814
                576471.162 5942484.101 576470.103 5942484.509 576468.991 5942485.155
                576468.029 5942485.955 576467.009 5942487.194 576466.066 5942489.137
                576465.108 5942491.997 576454.397 5942524.162 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_dad385e7-637b-40c7-8d96-ca50c85d4c6d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576419.618 5942410.13</gml:lowerCorner>
          <gml:upperCorner>576436.179 5942443.668</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(F)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_ac19c4a5-51ea-4718-b2e3-98cff0c05273" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c61d212b-e6b2-45c8-8d13-640d1550668e" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F9FBEDA1-0783-4D6F-9FDB-28C621E759F0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576423.587 5942410.205 576430.543 5942410.41 576430.41 5942413.408
                576429.724 5942428.829 576436.04 5942429.111 576436.179 5942434.919
                576431.773 5942443.668 576419.618 5942443.127 576421.087 5942410.13
                576423.587 5942410.205 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_ac19c4a5-51ea-4718-b2e3-98cff0c05273">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576422.832 5942424.648</gml:lowerCorner>
          <gml:upperCorner>576422.832 5942424.648</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_dad385e7-637b-40c7-8d96-ca50c85d4c6d" />
      <xplan:schriftinhalt>(F)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_7F489B65-AC1C-43A2-8D8E-8524501D75F7" srsName="EPSG:25832">
          <gml:pos>576422.832 5942424.648</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_631c2328-7244-4952-9fcd-9f7c993901b5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576569.937 5942517.281</gml:lowerCorner>
          <gml:upperCorner>576584.996 5942562.554</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_00809F0C-E950-4223-81B5-49FD42F623CD" srsName="EPSG:25832">
          <gml:posList>576584.996 5942517.281 576569.937 5942562.554 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_f9aebab9-62a1-45e0-bcca-7998ec7156c2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576571.627 5942441.909</gml:lowerCorner>
          <gml:upperCorner>576588.92 5942477.592</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(C)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_ad4a57ec-d094-4fb7-bf50-96dd5537d474" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_916222ED-7F34-4594-85A5-CFCAA77E28C5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576588.92 5942477.592 576579.633 5942476.632 576579.633 5942472.712
                576583.156 5942472.712 576583.756 5942465.671 576571.867 5942465.511
                576572.107 5942459.351 576584.056 5942459.191 576584.357 5942445.989
                576571.627 5942445.989 576571.627 5942441.909 576587.719 5942441.909
                576588.92 5942441.909 576588.92 5942477.592 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_1f6bab62-0bb3-4fec-881e-b0cd25c51168">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576492.223 5942512.76</gml:lowerCorner>
          <gml:upperCorner>576510.74 5942534.189</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0bdc76ef-35d5-4c75-9d5b-fa3ea30ba4d1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_EFDA03AE-47DE-427C-9BF8-ECFD3AD38986" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576510.74 5942517.017 576505.034 5942534.189 576492.223 5942529.932
                576497.929 5942512.76 576510.74 5942517.017 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0bdc76ef-35d5-4c75-9d5b-fa3ea30ba4d1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576501.481 5942523.475</gml:lowerCorner>
          <gml:upperCorner>576501.481 5942523.475</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1f6bab62-0bb3-4fec-881e-b0cd25c51168" />
      <xplan:position>
        <gml:Point gml:id="Gml_37C69BB2-B214-4D97-B0CF-8296141053D3" srsName="EPSG:25832">
          <gml:pos>576501.481 5942523.475</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_47fb016d-dab5-4213-a4c1-deed9fb7dd94">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576472.234 5942458.698</gml:lowerCorner>
          <gml:upperCorner>576520.812 5942473.618</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b651ba20-7173-4b57-a0f7-cd61928b563b" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_EAF51293-F93B-478F-9894-8EA3DFFAB36E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576520.812 5942460.124 576520.412 5942473.618 576472.234 5942472.192
                576472.633 5942458.698 576520.812 5942460.124 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b651ba20-7173-4b57-a0f7-cd61928b563b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576496.523 5942466.158</gml:lowerCorner>
          <gml:upperCorner>576496.523 5942466.158</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_47fb016d-dab5-4213-a4c1-deed9fb7dd94" />
      <xplan:position>
        <gml:Point gml:id="Gml_8690DCEF-EF7D-4B22-A4F8-B0455989EFB8" srsName="EPSG:25832">
          <gml:pos>576496.523 5942466.158</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_1a82a070-de61-4fbe-ac33-6891f7702b34">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576454.397 5942483.772</gml:lowerCorner>
          <gml:upperCorner>576585.945 5942564.472</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>1</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0ab64645-e5c3-4661-b57e-ae29122cf610" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c2a44c83-eb3f-46df-ba27-96f0ad9ab3c8" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_d0723b30-7904-45b2-9b6a-8b55bf18b911" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_abd5458e-92a4-4620-b6ec-944a997c3fcf" />
      <xplan:refTextInhalt xlink:href="#GML_6c87c405-f456-4a52-ae02-d345333bc7c2" />
      <xplan:refTextInhalt xlink:href="#GML_854a644c-3f65-4596-8362-c6eb77cb747d" />
      <xplan:refTextInhalt xlink:href="#GML_bd96f489-ace2-4a87-8731-9ef82e38c8c1" />
      <xplan:refTextInhalt xlink:href="#GML_5060fe64-2557-4954-af7c-0f79ca0811dd" />
      <xplan:refTextInhalt xlink:href="#GML_c9dc8fb4-db8d-411f-9974-ebd078dc055a" />
      <xplan:refTextInhalt xlink:href="#GML_c984e887-2e6f-4cfd-a569-3f54d81c29e6" />
      <xplan:refTextInhalt xlink:href="#GML_ba86d815-a484-4530-83fe-ad2003191bdf" />
      <xplan:refTextInhalt xlink:href="#GML_da22f094-ff79-47c3-905d-57125ad15bbe" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_CFCD78E6-138F-460F-AA2C-7C2A61BAF851" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576476.148 5942483.813 576574.305 5942486.719 576577.618 5942487.564
                576580.728 5942489.289 576582.388 5942490.772 576583.807 5942492.573
                576585.186 5942495.361 576585.945 5942499.069 576585.446 5942515.928
                576569.937 5942562.554 576566.153 5942564.472 576456.312 5942527.947
                576454.397 5942524.162 576465.108 5942491.997 576466.066 5942489.137
                576467.009 5942487.194 576468.029 5942485.955 576468.991 5942485.155
                576470.103 5942484.509 576471.162 5942484.101 576472.593 5942483.814
                576473.672 5942483.772 576476.148 5942483.813 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung>
        <xplan:BP_Dachgestaltung>
          <xplan:dachform>1000</xplan:dachform>
        </xplan:BP_Dachgestaltung>
      </xplan:dachgestaltung>
      <xplan:dachform>1000</xplan:dachform>
      <xplan:GRZ>0.35</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0ab64645-e5c3-4661-b57e-ae29122cf610">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576537.524 5942497.207</gml:lowerCorner>
          <gml:upperCorner>576537.524 5942497.207</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1a82a070-de61-4fbe-ac33-6891f7702b34" />
      <xplan:position>
        <gml:Point gml:id="Gml_C01C0BCC-5AFC-480F-8780-764C4BB8FD7E" srsName="EPSG:25832">
          <gml:pos>576537.524 5942497.207</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c2a44c83-eb3f-46df-ba27-96f0ad9ab3c8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576540.684 5942503.522</gml:lowerCorner>
          <gml:upperCorner>576540.684 5942503.522</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1a82a070-de61-4fbe-ac33-6891f7702b34" />
      <xplan:position>
        <gml:Point gml:id="Gml_083851FF-8E62-494E-A7A7-29CCC8915CC6" srsName="EPSG:25832">
          <gml:pos>576540.684 5942503.522</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d0723b30-7904-45b2-9b6a-8b55bf18b911">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576536.662 5942492.615</gml:lowerCorner>
          <gml:upperCorner>576536.662 5942492.615</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>dachgestaltung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1a82a070-de61-4fbe-ac33-6891f7702b34" />
      <xplan:position>
        <gml:Point gml:id="Gml_78955109-C799-4576-9EF9-7B3CB67ACF7D" srsName="EPSG:25832">
          <gml:pos>576536.662 5942492.615</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_3b8a6af5-457e-4e0f-9e12-60b936cadfdf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576429.409 5942439.738</gml:lowerCorner>
          <gml:upperCorner>576456.937 5942513.277</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_55B65877-EA96-4C99-AAC8-C31598279E48" srsName="EPSG:25832">
          <gml:posList>576443.446 5942439.738 576456.937 5942440.135 576455.999 5942471.816
            576442.12 5942513.277 576429.409 5942509.053 576442.564 5942469.424
            576443.446 5942439.738 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_8047a192-d634-4414-b38a-950438ea177c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576538.104 5942442.549</gml:lowerCorner>
          <gml:upperCorner>576552.134 5942461.036</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_83094468-6c4d-4e1a-ad90-8a86710d3363" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_3ED3D0A4-3AE8-4568-937B-E008681823E1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576552.134 5942442.955 576551.598 5942461.036 576538.104 5942460.636
                576538.64 5942442.549 576552.134 5942442.955 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_83094468-6c4d-4e1a-ad90-8a86710d3363">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576545.119 5942451.794</gml:lowerCorner>
          <gml:upperCorner>576545.119 5942451.794</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_8047a192-d634-4414-b38a-950438ea177c" />
      <xplan:position>
        <gml:Point gml:id="Gml_8659C23D-57CF-4268-9D72-0550868F655E" srsName="EPSG:25832">
          <gml:pos>576545.119 5942451.794</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_3f91fd67-0c0b-4b1a-aa98-8753c8199ba8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576461.588 5942435.749</gml:lowerCorner>
          <gml:upperCorner>576599.891 5942571.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>2</xplan:gliederung1>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5e9681f5-4c33-46f4-8113-b287cbe57b3a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_6bfad91a-e310-49d9-93ac-2677f9578e6d" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_cf666f15-e87c-4193-b484-68ccd5e72343" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_abd5458e-92a4-4620-b6ec-944a997c3fcf" />
      <xplan:refTextInhalt xlink:href="#GML_6c87c405-f456-4a52-ae02-d345333bc7c2" />
      <xplan:refTextInhalt xlink:href="#GML_da22f094-ff79-47c3-905d-57125ad15bbe" />
      <xplan:refTextInhalt xlink:href="#GML_ba86d815-a484-4530-83fe-ad2003191bdf" />
      <xplan:refTextInhalt xlink:href="#GML_854a644c-3f65-4596-8362-c6eb77cb747d" />
      <xplan:refTextInhalt xlink:href="#GML_bd96f489-ace2-4a87-8731-9ef82e38c8c1" />
      <xplan:refTextInhalt xlink:href="#GML_5060fe64-2557-4954-af7c-0f79ca0811dd" />
      <xplan:refTextInhalt xlink:href="#GML_c9dc8fb4-db8d-411f-9974-ebd078dc055a" />
      <xplan:refTextInhalt xlink:href="#GML_c984e887-2e6f-4cfd-a569-3f54d81c29e6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_448471AF-0B82-4D7D-B51D-1E95579AC5D7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576592.043 5942439.774 576599.891 5942440.018 576598.756 5942468.151
                576594.615 5942570.631 576588.214 5942571.795 576585.605 5942570.928
                576580.138 5942569.112 576578.266 5942565.328 576584.585 5942546.633
                576585.643 5942543.641 576586.701 5942540.649 576584.613 5942538.256
                576585.286 5942536.147 576587.281 5942530.022 576590.82 5942519.15
                576591.61 5942515.589 576592.25 5942494.172 576591.693 5942485.065
                576590.737 5942484.009 576589.163 5942482.269 576586.072 5942480.109
                576582.576 5942478.696 576578.853 5942478.1 576461.588 5942474.628
                576462.798 5942435.749 576592.043 5942439.774 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung>
        <xplan:BP_Dachgestaltung>
          <xplan:dachform>1000</xplan:dachform>
        </xplan:BP_Dachgestaltung>
      </xplan:dachgestaltung>
      <xplan:dachform>1000</xplan:dachform>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5e9681f5-4c33-46f4-8113-b287cbe57b3a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576524.665 5942454.124</gml:lowerCorner>
          <gml:upperCorner>576524.665 5942454.124</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>dachgestaltung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3f91fd67-0c0b-4b1a-aa98-8753c8199ba8" />
      <xplan:position>
        <gml:Point gml:id="Gml_E0A2C857-23D9-4F3D-99F7-9CFB8A853B5F" srsName="EPSG:25832">
          <gml:pos>576524.665 5942454.124</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_6bfad91a-e310-49d9-93ac-2677f9578e6d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576526.983 5942464.84</gml:lowerCorner>
          <gml:upperCorner>576526.983 5942464.84</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3f91fd67-0c0b-4b1a-aa98-8753c8199ba8" />
      <xplan:position>
        <gml:Point gml:id="Gml_4BDECC78-9255-4034-AF3B-3233AD6A8704" srsName="EPSG:25832">
          <gml:pos>576526.983 5942464.84</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_cf666f15-e87c-4193-b484-68ccd5e72343">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576525.824 5942459.048</gml:lowerCorner>
          <gml:upperCorner>576525.824 5942459.048</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3f91fd67-0c0b-4b1a-aa98-8753c8199ba8" />
      <xplan:position>
        <gml:Point gml:id="Gml_4526C7BB-45F1-43D3-875E-24D436872EA9" srsName="EPSG:25832">
          <gml:pos>576525.824 5942459.048</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_0e8dfd5a-603d-4c56-b846-5f5c1573c528">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576429.724 5942413.408</gml:lowerCorner>
          <gml:upperCorner>576600.834 5942440.018</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Schutzgrün</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_916188a0-f5b0-4f17-942f-c25a0d915bd9" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_ba86d815-a484-4530-83fe-ad2003191bdf" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_83A4903D-3A87-4548-843E-766827D3CE61" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576599.891 5942440.018 576592.043 5942439.774 576462.798 5942435.749
                576436.179 5942434.919 576436.04 5942429.111 576429.724 5942428.829
                576430.41 5942413.408 576480.266 5942414.884 576480.165 5942424.848
                576485.589 5942430.139 576505.565 5942430.669 576512.841 5942426.038
                576516.148 5942426.171 576516.45 5942416.602 576592.923 5942419.368
                576593.045 5942416.37 576600.834 5942416.652 576599.891 5942440.018
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_916188a0-f5b0-4f17-942f-c25a0d915bd9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576444.052 5942422.671</gml:lowerCorner>
          <gml:upperCorner>576444.052 5942422.671</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>text</xplan:art>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_0e8dfd5a-603d-4c56-b846-5f5c1573c528" />
      <xplan:position>
        <gml:Point gml:id="Gml_423526DF-A419-4FB5-A137-415F016AF445" srsName="EPSG:25832">
          <gml:pos>576444.052 5942422.671</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b1826698-ecc5-4cb0-9c88-d5afe1f31b5d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576538.104 5942442.549</gml:lowerCorner>
          <gml:upperCorner>576552.134 5942461.036</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_45BA466C-BAEF-4B9F-88AC-FA52A0B79002" srsName="EPSG:25832">
          <gml:posList>576538.104 5942460.636 576538.64 5942442.549 576552.134 5942442.955
            576551.598 5942461.036 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_60fb20fd-8cd3-4173-ac56-880f4451e4d8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576543.221 5942428.491</gml:lowerCorner>
          <gml:upperCorner>576543.221 5942428.491</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">48.4</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_A50B9AF6-B9E6-43E7-9822-B2A5C0B4575E" srsName="EPSG:25832">
          <gml:pos>576543.221 5942428.491</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_11901099-8d74-4850-8177-6541547ab30b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576585.605 5942419.368</gml:lowerCorner>
          <gml:upperCorner>576600.713 5942571.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E9B5ABE5-D758-43B5-8F6F-B67DE8D0FE97" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576600.713 5942419.649 576599.891 5942440.018 576598.756 5942468.151
                576594.615 5942570.631 576588.214 5942571.795 576585.605 5942570.928
                576587.244 5942530.136 576590.82 5942519.15 576591.61 5942515.589
                576592.25 5942494.172 576591.693 5942485.065 576590.737 5942484.009
                576590.328 5942483.557 576592.094 5942439.776 576592.923 5942419.368
                576600.713 5942419.649 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:massnahme>3000</xplan:massnahme>
      <xplan:gegenstand>2000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_42c7fef5-324b-4253-ada5-725c99cb27cf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576416.434 5942434.919</gml:lowerCorner>
          <gml:upperCorner>576462.798 5942522.771</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>1</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_00f7eff9-1faf-4448-8ec7-1f0ef6c16d6d" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_05afd08d-622c-4587-829b-6e25b25c7e97" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c233cc65-4339-4252-934c-24fb7eb2ad14" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_b4003c7b-9bc5-4d29-8de8-ddc80de9f251" />
      <xplan:refTextInhalt xlink:href="#GML_6c87c405-f456-4a52-ae02-d345333bc7c2" />
      <xplan:refTextInhalt xlink:href="#GML_bd96f489-ace2-4a87-8731-9ef82e38c8c1" />
      <xplan:refTextInhalt xlink:href="#GML_78445473-8bfb-4234-bcfa-9675e3c39fe7" />
      <xplan:refTextInhalt xlink:href="#GML_5060fe64-2557-4954-af7c-0f79ca0811dd" />
      <xplan:refTextInhalt xlink:href="#GML_a7b715e5-c683-4ac9-ae8c-f6b3960810d9" />
      <xplan:refTextInhalt xlink:href="#GML_c984e887-2e6f-4cfd-a569-3f54d81c29e6" />
      <xplan:refTextInhalt xlink:href="#GML_ba86d815-a484-4530-83fe-ad2003191bdf" />
      <xplan:refTextInhalt xlink:href="#GML_da22f094-ff79-47c3-905d-57125ad15bbe" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C2C76A25-90BC-4E17-988F-5E559A540A58" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576442.293 5942514.335 576446.184 5942515.628 576444.447 5942520.855
                576440.649 5942522.771 576416.434 5942514.7 576419.618 5942443.127
                576431.773 5942443.668 576436.179 5942434.919 576462.798 5942435.749
                576461.588 5942474.628 576455.548 5942474.449 576442.293 5942514.335
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung>
        <xplan:BP_Dachgestaltung>
          <xplan:dachform>1000</xplan:dachform>
        </xplan:BP_Dachgestaltung>
      </xplan:dachgestaltung>
      <xplan:dachform>1000</xplan:dachform>
      <xplan:GRZ>0.5</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_00f7eff9-1faf-4448-8ec7-1f0ef6c16d6d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576427.154 5942469.442</gml:lowerCorner>
          <gml:upperCorner>576427.154 5942469.442</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_42c7fef5-324b-4253-ada5-725c99cb27cf" />
      <xplan:position>
        <gml:Point gml:id="Gml_09CD11D1-86C9-40C5-BB5C-E6477518ED4A" srsName="EPSG:25832">
          <gml:pos>576427.154 5942469.442</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_05afd08d-622c-4587-829b-6e25b25c7e97">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576427.882 5942462.534</gml:lowerCorner>
          <gml:upperCorner>576427.882 5942462.534</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_42c7fef5-324b-4253-ada5-725c99cb27cf" />
      <xplan:position>
        <gml:Point gml:id="Gml_CD229C09-23ED-4116-AD4C-17EC5C8613D0" srsName="EPSG:25832">
          <gml:pos>576427.882 5942462.534</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c233cc65-4339-4252-934c-24fb7eb2ad14">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576426.972 5942457.081</gml:lowerCorner>
          <gml:upperCorner>576426.972 5942457.081</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>dachgestaltung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_42c7fef5-324b-4253-ada5-725c99cb27cf" />
      <xplan:position>
        <gml:Point gml:id="Gml_B525CF40-F0EC-4618-A4B5-D90F7294B62A" srsName="EPSG:25832">
          <gml:pos>576426.972 5942457.081</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Immissionsschutz gml:id="GML_438cb8ae-be20-4cce-b7fa-6d5b7326a357">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576422.504 5942410.205</gml:lowerCorner>
          <gml:upperCorner>576593.045 5942439.776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2000</xplan:hoehenbezug>
          <xplan:bezugspunkt>3000</xplan:bezugspunkt>
          <xplan:h uom="m">9</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_71de5645-d06c-4eb8-82f7-97b8bf8a00e9" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_6a527b3a-3287-42fe-8705-d1e28dcbb3d7" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_15890cb4-3da8-43fc-ab37-2c9ecc3cd74c" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_F5AFE5F1-1B38-44C5-AD1B-FCC51BF90F23" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576592.094 5942439.776 576422.504 5942434.534 576423.587 5942410.205
                576430.543 5942410.41 576480.297 5942411.883 576480.165 5942424.848
                576485.589 5942430.139 576505.565 5942430.669 576512.841 5942426.038
                576516.148 5942426.171 576516.545 5942413.603 576593.045 5942416.37
                576592.923 5942419.368 576592.094 5942439.776 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:nutzung>Fläche für besondere Anlagen</xplan:nutzung>
      <xplan:typ>2000</xplan:typ>
      <xplan:technVorkehrung>10002</xplan:technVorkehrung>
      <xplan:detaillierteTechnVorkehrung>Schutzwall mit Schutzwand</xplan:detaillierteTechnVorkehrung>
    </xplan:BP_Immissionsschutz>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_71de5645-d06c-4eb8-82f7-97b8bf8a00e9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576532.471 5942420.745</gml:lowerCorner>
          <gml:upperCorner>576532.471 5942420.745</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_438cb8ae-be20-4cce-b7fa-6d5b7326a357" />
      <xplan:position>
        <gml:Point gml:id="Gml_B1B165EE-577A-4C64-95D1-4BDBE2982187" srsName="EPSG:25832">
          <gml:pos>576532.471 5942420.745</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">1.73</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_6a527b3a-3287-42fe-8705-d1e28dcbb3d7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576526.885 5942431.723</gml:lowerCorner>
          <gml:upperCorner>576526.885 5942431.723</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_438cb8ae-be20-4cce-b7fa-6d5b7326a357" />
      <xplan:schriftinhalt>Schutzwall mit Schutzwand</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_986BFC9C-9F25-4AE6-ABFF-55D439D66163" srsName="EPSG:25832">
          <gml:pos>576526.885 5942431.723</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">1.36</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_15890cb4-3da8-43fc-ab37-2c9ecc3cd74c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576523.225 5942422.286</gml:lowerCorner>
          <gml:upperCorner>576523.225 5942422.286</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_438cb8ae-be20-4cce-b7fa-6d5b7326a357" />
      <xplan:position>
        <gml:Point gml:id="Gml_1544F56F-5AFF-403C-870B-423DF4BCE2F1" srsName="EPSG:25832">
          <gml:pos>576523.225 5942422.286</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ba300100-99e7-4741-8fc0-323cbfe283f3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576554.382 5942533.416</gml:lowerCorner>
          <gml:upperCorner>576572.897 5942554.84</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_A2BFA3B1-2597-414F-BB16-42BF09149D59" srsName="EPSG:25832">
          <gml:posList>576572.897 5942537.673 576567.193 5942554.84 576554.382 5942550.583
            576560.087 5942533.416 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_269bef7b-7d34-40e6-b48d-611a5001ab73">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576521.453 5942522.473</gml:lowerCorner>
          <gml:upperCorner>576539.969 5942543.897</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a5d113d2-aae0-4d37-be0d-86c438e4381f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_86CA0A7F-1439-43B9-B28B-332C6D6BF23E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576539.969 5942526.73 576534.264 5942543.897 576521.453 5942539.64
                576527.157 5942522.473 576539.969 5942526.73 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a5d113d2-aae0-4d37-be0d-86c438e4381f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576530.711 5942533.185</gml:lowerCorner>
          <gml:upperCorner>576530.711 5942533.185</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_269bef7b-7d34-40e6-b48d-611a5001ab73" />
      <xplan:position>
        <gml:Point gml:id="Gml_4B749696-FBE8-46FF-BBB4-FBB2B6FF741B" srsName="EPSG:25832">
          <gml:pos>576530.711 5942533.185</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_e3d2f053-c8ae-498f-aab5-b497490ec636">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576625.089 5942561.859</gml:lowerCorner>
          <gml:upperCorner>576625.089 5942561.859</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">40.6</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_5E20D2F4-BC0B-4861-A740-189B030A4E38" srsName="EPSG:25832">
          <gml:pos>576625.089 5942561.859</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_f352bbb5-f0e0-4059-8c86-61f393a9c732">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576476.841 5942424.932</gml:lowerCorner>
          <gml:upperCorner>576476.841 5942424.932</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">48.2</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_2F7B240E-C36F-46BF-871B-C22FA919CD2D" srsName="EPSG:25832">
          <gml:pos>576476.841 5942424.932</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_6bd85817-ed09-4b54-8ffa-d1faa982d616">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576537.705 5942460.636</gml:lowerCorner>
          <gml:upperCorner>576586.283 5942475.557</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c50c09c9-84d0-48ad-8c29-0a823b223514" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_A4A13CE9-607A-47FF-9744-07380B8A4CCF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576586.283 5942462.063 576585.884 5942475.557 576537.705 5942474.13
                576538.104 5942460.636 576586.283 5942462.063 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c50c09c9-84d0-48ad-8c29-0a823b223514">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576561.994 5942468.096</gml:lowerCorner>
          <gml:upperCorner>576561.994 5942468.096</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6bd85817-ed09-4b54-8ffa-d1faa982d616" />
      <xplan:position>
        <gml:Point gml:id="Gml_E89E7982-1495-4818-8010-FEB219E2A2F2" srsName="EPSG:25832">
          <gml:pos>576561.994 5942468.096</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_1ce434d3-ca84-4476-8cb2-18cd42647531">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576585.605 5942419.368</gml:lowerCorner>
          <gml:upperCorner>576600.713 5942571.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(E)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_4ebd4bb6-0bf4-460b-a23a-bdd149ebef6f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_33707095-3440-490b-9d83-c362569a0ef4" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_51a21b00-c38b-4ff7-bc86-766994a242f1" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6661044E-F178-4304-999B-350A12931C85" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576600.713 5942419.649 576599.891 5942440.018 576598.756 5942468.151
                576594.615 5942570.631 576588.214 5942571.795 576585.605 5942570.928
                576587.244 5942530.136 576590.82 5942519.15 576591.61 5942515.589
                576592.25 5942494.172 576591.693 5942485.065 576590.737 5942484.009
                576590.328 5942483.557 576592.094 5942439.776 576592.923 5942419.368
                576600.713 5942419.649 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_4ebd4bb6-0bf4-460b-a23a-bdd149ebef6f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576593.85 5942426.013</gml:lowerCorner>
          <gml:upperCorner>576593.85 5942426.013</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1ce434d3-ca84-4476-8cb2-18cd42647531" />
      <xplan:schriftinhalt>(E)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_DAA4094B-66C4-4EA0-8CCA-08156FD0C908" srsName="EPSG:25832">
          <gml:pos>576593.85 5942426.013</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_33707095-3440-490b-9d83-c362569a0ef4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576588.391 5942549.915</gml:lowerCorner>
          <gml:upperCorner>576588.391 5942549.915</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1ce434d3-ca84-4476-8cb2-18cd42647531" />
      <xplan:position>
        <gml:Point gml:id="Gml_0D04349C-0851-4988-A134-916A84BD89BE" srsName="EPSG:25832">
          <gml:pos>576588.391 5942549.915</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c94e422a-b3ed-4d92-95d5-8dc4c189650e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576472.633 5942440.616</gml:lowerCorner>
          <gml:upperCorner>576486.662 5942459.097</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5c143714-4150-4f49-8d5a-d2b826e7958d" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_86CFF1EA-9CD4-44F8-893A-84AC2524D58A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576486.662 5942441.015 576486.123 5942459.097 576472.633 5942458.698
                576473.168 5942440.616 576486.662 5942441.015 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5c143714-4150-4f49-8d5a-d2b826e7958d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576479.647 5942449.856</gml:lowerCorner>
          <gml:upperCorner>576479.647 5942449.856</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c94e422a-b3ed-4d92-95d5-8dc4c189650e" />
      <xplan:position>
        <gml:Point gml:id="Gml_9E604246-64EE-4B39-8960-6075D6DF1E1F" srsName="EPSG:25832">
          <gml:pos>576479.647 5942449.856</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_96a7bd77-f199-4cea-84d3-fdbabfe3f643">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576420.864 5942475.609</gml:lowerCorner>
          <gml:upperCorner>576439.57 5942509.053</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_BF5A027E-8523-4803-9E49-2A930DEAF507" srsName="EPSG:25832">
          <gml:posList>576429.409 5942509.053 576420.864 5942506.213 576431.033 5942475.609
            576439.57 5942478.445 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a95a1007-a70f-474c-b791-ffb675af4015">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576554.382 5942533.416</gml:lowerCorner>
          <gml:upperCorner>576572.897 5942554.84</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_bc8efcd2-efba-4a41-bd7f-6d3b26f272de" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_F028F04C-B735-4219-88E3-6BB8361773F8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576572.897 5942537.673 576567.193 5942554.84 576554.382 5942550.583
                576560.087 5942533.416 576572.897 5942537.673 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_bc8efcd2-efba-4a41-bd7f-6d3b26f272de">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576563.64 5942544.128</gml:lowerCorner>
          <gml:upperCorner>576563.64 5942544.128</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_a95a1007-a70f-474c-b791-ffb675af4015" />
      <xplan:position>
        <gml:Point gml:id="Gml_D1A4B981-C2ED-4A8E-9DE2-2096B4A89B4D" srsName="EPSG:25832">
          <gml:pos>576563.64 5942544.128</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_bff9a682-a559-4db3-a3b5-4fabc70907a6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576507.318 5942441.643</gml:lowerCorner>
          <gml:upperCorner>576521.347 5942460.124</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_0FACB72B-CF29-4B20-8D51-169C6A33252B" srsName="EPSG:25832">
          <gml:posList>576507.318 5942459.725 576507.853 5942441.643 576521.347 5942442.042
            576520.812 5942460.124 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b021ad8f-e4d0-498b-896a-2db5e334ff4d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576472.234 5942458.698</gml:lowerCorner>
          <gml:upperCorner>576520.812 5942473.618</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_897356A8-AA23-4516-876B-8478DC890765" srsName="EPSG:25832">
          <gml:posList>576520.812 5942460.124 576520.412 5942473.618 576472.234 5942472.192
            576472.633 5942458.698 576520.812 5942460.124 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_3264fd82-1958-4a67-8673-42cb63036f17">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576430.41 5942410.41</gml:lowerCorner>
          <gml:upperCorner>576480.297 5942414.884</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(G)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_8ded082b-f799-4b8c-ba49-a1aa23a63d8f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_8d149ef7-4b94-40d9-b994-6b222dd4734e" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_661BD052-7B0B-4F02-9D56-E73365408ACC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576480.297 5942411.883 576480.266 5942414.884 576430.41 5942413.408
                576430.543 5942410.41 576480.297 5942411.883 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_8ded082b-f799-4b8c-ba49-a1aa23a63d8f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576438.813 5942411.191</gml:lowerCorner>
          <gml:upperCorner>576438.813 5942411.191</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3264fd82-1958-4a67-8673-42cb63036f17" />
      <xplan:schriftinhalt>(G)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_4011D498-541E-4E02-ADA3-4DCA81C0B306" srsName="EPSG:25832">
          <gml:pos>576438.813 5942411.191</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_109fb3fb-2e1f-45a1-86af-2bb1dcc6d8ed">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576507.318 5942441.643</gml:lowerCorner>
          <gml:upperCorner>576521.347 5942460.124</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_77b72a48-372d-482a-8355-c966829968ba" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_DCC44A0A-4326-475E-AA91-27E1A91DACBB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576521.347 5942442.042 576520.812 5942460.124 576507.318 5942459.725
                576507.853 5942441.643 576521.347 5942442.042 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_77b72a48-372d-482a-8355-c966829968ba">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576514.333 5942450.884</gml:lowerCorner>
          <gml:upperCorner>576514.333 5942450.884</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_109fb3fb-2e1f-45a1-86af-2bb1dcc6d8ed" />
      <xplan:position>
        <gml:Point gml:id="Gml_828F95B0-3615-43E8-87E5-E416611D22F6" srsName="EPSG:25832">
          <gml:pos>576514.333 5942450.884</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_1a6c9d6a-6e93-4854-9229-bd579e4d0df6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576418.558 5942521.081</gml:lowerCorner>
          <gml:upperCorner>576418.558 5942521.081</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">40.8</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_DA0366E5-D4A2-46F4-8230-D92E29C03B7F" srsName="EPSG:25832">
          <gml:pos>576418.558 5942521.081</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a8fcb5cd-7364-46b6-8ad9-beb227979325">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576459.295 5942501.817</gml:lowerCorner>
          <gml:upperCorner>576477.811 5942523.238</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5ba9de57-cd5c-4658-a2af-afd65f5b25bf" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7ACB1384-E59D-49D7-8A29-4BDBC2D5C149" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576477.811 5942506.074 576472.107 5942523.238 576459.295 5942518.984
                576465 5942501.817 576477.811 5942506.074 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5ba9de57-cd5c-4658-a2af-afd65f5b25bf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576468.553 5942512.528</gml:lowerCorner>
          <gml:upperCorner>576468.553 5942512.528</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_a8fcb5cd-7364-46b6-8ad9-beb227979325" />
      <xplan:position>
        <gml:Point gml:id="Gml_7E18C026-8913-4867-AE06-C38408B722A2" srsName="EPSG:25832">
          <gml:pos>576468.553 5942512.528</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_35e891fa-16a4-4ac3-a5a5-c449ad78d271">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576620.053 5942520.948</gml:lowerCorner>
          <gml:upperCorner>576620.053 5942520.948</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_1A345FC1-C010-43E1-8E55-3755CC94C54E" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bfa7ceb2-40d5-40ac-9023-7960473efb78" />
      <xplan:position>
        <gml:Point gml:id="Gml_71204A2B-FC75-41DC-92D7-8D3C84E8B0E8" srsName="EPSG:25832">
          <gml:pos>576620.053 5942520.948</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="Gml_1A345FC1-C010-43E1-8E55-3755CC94C54E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576594.615 5942468.151</gml:lowerCorner>
          <gml:upperCorner>576656.335 5942570.631</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>1</xplan:nummer>
      <xplan:bedeutung>1600</xplan:bedeutung>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_01CD3DC3-47FA-4C69-B17E-D1758B7E5586" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576655.825 5942531.79 576639.6672 5942544.3423 576609.257 5942567.967
                576594.615 5942570.631 576597.1439 5942508.047 576598.756 5942468.151
                576611.81 5942468.677 576626.513 5942487.073 576625.447 5942487.931
                576652.088 5942522.116 576656.335 5942527.565 576655.825 5942531.79
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>BPlan001_5-2_Bereich1.pgw</xplan:georefURL>
          <xplan:referenzURL>BPlan001_5-2_Bereich1.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
      <xplan:planinhalt xlink:href="#GML_bfa7ceb2-40d5-40ac-9023-7960473efb78" />
      <xplan:planinhalt xlink:href="#GML_f20bca86-bba8-47dc-b32c-2149f1816e55" />
      <xplan:planinhalt xlink:href="#GML_14188ee0-916a-4355-bafc-da98d2825c05" />
      <xplan:planinhalt xlink:href="#GML_e36ff7bb-13fd-4216-9ae6-f8b39c1ccdfc" />
      <xplan:planinhalt xlink:href="#GML_bbe87b33-5e0d-44cb-8e1c-79078f6c3bf0" />
      <xplan:praesentationsobjekt xlink:href="#GML_e1769717-420a-4fc2-9537-6edb0fd69083" />
      <xplan:praesentationsobjekt xlink:href="#GML_35e891fa-16a4-4ac3-a5a5-c449ad78d271" />
      <xplan:praesentationsobjekt xlink:href="#GML_91d05bc0-9d71-4382-88e4-d1c02c48cdaa" />
      <xplan:praesentationsobjekt xlink:href="#GML_0bf9f998-bd70-4fea-9cbc-2f2668f5839a" />
      <xplan:versionBauNVOText>Version vom 11.06.2013</xplan:versionBauNVOText>
      <xplan:gehoertZuPlan xlink:href="#GML_1c23cbae-b6e9-4f21-b9c2-f3fc6df46545" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_bfa7ceb2-40d5-40ac-9023-7960473efb78">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576594.615 5942468.151</gml:lowerCorner>
          <gml:upperCorner>576656.335 5942570.631</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>2</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#Gml_1A345FC1-C010-43E1-8E55-3755CC94C54E" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_35e891fa-16a4-4ac3-a5a5-c449ad78d271" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e1769717-420a-4fc2-9537-6edb0fd69083" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_91d05bc0-9d71-4382-88e4-d1c02c48cdaa" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_ea29c620-e827-4e8c-8fc1-f7f0e605904e" />
      <xplan:refTextInhalt xlink:href="#GML_5060fe64-2557-4954-af7c-0f79ca0811dd" />
      <xplan:refTextInhalt xlink:href="#GML_a7b715e5-c683-4ac9-ae8c-f6b3960810d9" />
      <xplan:refTextInhalt xlink:href="#GML_c984e887-2e6f-4cfd-a569-3f54d81c29e6" />
      <xplan:refTextInhalt xlink:href="#GML_ba86d815-a484-4530-83fe-ad2003191bdf" />
      <xplan:refTextInhalt xlink:href="#GML_da22f094-ff79-47c3-905d-57125ad15bbe" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_274C2988-8D2E-434D-A18F-773B257BEF77" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576611.81 5942468.677 576626.513 5942487.073 576625.447 5942487.931
                576652.088 5942522.116 576656.335 5942527.565 576655.825 5942531.79
                576609.257 5942567.967 576594.615 5942570.631 576598.756 5942468.151
                576611.81 5942468.677 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>2</xplan:Z>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e1769717-420a-4fc2-9537-6edb0fd69083">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576620.962 5942527.931</gml:lowerCorner>
          <gml:upperCorner>576620.962 5942527.931</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_1A345FC1-C010-43E1-8E55-3755CC94C54E" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bfa7ceb2-40d5-40ac-9023-7960473efb78" />
      <xplan:position>
        <gml:Point gml:id="Gml_555D03D5-C571-4D96-B7C9-6055E33D52B0" srsName="EPSG:25832">
          <gml:pos>576620.962 5942527.931</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_91d05bc0-9d71-4382-88e4-d1c02c48cdaa">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576622.145 5942515.494</gml:lowerCorner>
          <gml:upperCorner>576622.145 5942515.494</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_1A345FC1-C010-43E1-8E55-3755CC94C54E" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bfa7ceb2-40d5-40ac-9023-7960473efb78" />
      <xplan:position>
        <gml:Point gml:id="Gml_21AAF525-E3BA-4255-AB43-719D06AF803A" srsName="EPSG:25832">
          <gml:pos>576622.145 5942515.494</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_f20bca86-bba8-47dc-b32c-2149f1816e55">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576602.018 5942468.433</gml:lowerCorner>
          <gml:upperCorner>576652.088 5942561.023</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_1A345FC1-C010-43E1-8E55-3755CC94C54E" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_FB5AD614-1127-47DA-B5DF-0EC0DEBFB49A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576611.81 5942468.677 576626.513 5942487.073 576625.447 5942487.931
                576652.088 5942522.116 576602.018 5942561.023 576605.75 5942468.433
                576611.81 5942468.677 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>2</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_14188ee0-916a-4355-bafc-da98d2825c05">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576602.018 5942468.433</gml:lowerCorner>
          <gml:upperCorner>576652.088 5942561.023</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#Gml_1A345FC1-C010-43E1-8E55-3755CC94C54E" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_19C015BE-B3C3-497A-89E7-6635318FC179" srsName="EPSG:25832">
          <gml:posList>576605.75 5942468.433 576611.81 5942468.677 576626.513 5942487.073
            576625.434 5942487.914 576652.088 5942522.116 576602.018 5942561.023
            576605.75 5942468.433 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_0bf9f998-bd70-4fea-9cbc-2f2668f5839a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576609.908 5942485.248</gml:lowerCorner>
          <gml:upperCorner>576609.908 5942485.248</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_1A345FC1-C010-43E1-8E55-3755CC94C54E" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bbe87b33-5e0d-44cb-8e1c-79078f6c3bf0" />
      <xplan:schriftinhalt>(A)</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_8907FAFE-54F5-4F51-AB0A-F33B8DB526C7" srsName="EPSG:25832">
          <gml:pos>576609.908 5942485.248</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_bbe87b33-5e0d-44cb-8e1c-79078f6c3bf0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576597.144 5942468.151</gml:lowerCorner>
          <gml:upperCorner>576656.335 5942544.342</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(A)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#Gml_1A345FC1-C010-43E1-8E55-3755CC94C54E" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0bf9f998-bd70-4fea-9cbc-2f2668f5839a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_ea29c620-e827-4e8c-8fc1-f7f0e605904e" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_72584846-FFF5-4D02-8638-9C1108BA41F1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">576652.088 5942522.116 576656.335 5942527.565 576655.825 5942531.79
                576639.667 5942544.342 576605.88 5942500.937 576605.337 5942501.379
                576597.144 5942508.047 576598.756 5942468.151 576611.81 5942468.677
                576626.513 5942487.073 576625.447 5942487.931 576652.088 5942522.116
              </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_e36ff7bb-13fd-4216-9ae6-f8b39c1ccdfc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576597.149 5942500.937</gml:lowerCorner>
          <gml:upperCorner>576639.667 5942544.342</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Sonstige Abgrenzung</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#Gml_1A345FC1-C010-43E1-8E55-3755CC94C54E" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4EB3DC53-BBEC-4D5A-8960-C7AF1579E367" srsName="EPSG:25832">
          <gml:posList>576639.667 5942544.342 576605.88 5942500.937 576597.149 5942508.043
          </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
</xplan:XPlanAuszug>
