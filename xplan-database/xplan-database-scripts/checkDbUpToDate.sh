#!/bin/bash

# Checks if the database is up-to-date or not
# Paramaters
# - --wait (optional): wait until the db is up-to-date
# Return codes:
# - 0: database is up-to-date
# - 1: database is not up-to-date

set -e

export PGPASSWORD=$XPLAN_DB_PASSWORD
export LC_ALL=en_US.UTF-8

function readLastTagWithGitRev() {
	psql --username=$XPLAN_DB_USER --dbname=$XPLAN_DB_NAME --host=$XPLAN_DB_HOSTNAME --port=$XPLAN_DB_PORT -c "select tag from databasechangelog where id like 'lastGitRev%' order by orderexecuted desc limit 1" -qtA
}

if [ "$1" == '--wait' ]; then
	while [ "$(readLastTagWithGitRev)" != "$XPLAN_GIT_REVISION" ]; do
		echo 'Waiting for 10s'; 
		sleep 10s; 
	done; 
	echo 'DB up-to-date'
elif [ "$(readLastTagWithGitRev)" != "$XPLAN_GIT_REVISION" ]; then 
	echo 'DB update needed';
	exit 1
else 
	echo 'DB up-to-date'; 
fi;

